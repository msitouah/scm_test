define pgmid=&1
define load_date=&2
define err_file=&3
spool &&err_file append
set serveroutput on
--
whenever sqlerror exit sql.sqlcode;
--
DECLARE
  P_Program_Id  INTEGER       := TO_NUMBER( '&&pgmid' );
  P_Load_Date   DATE          := TO_Date( '&&load_Date', 'dd-mon-yyyy' );
  V_File_Type   Ops_Scripts_Parms.Parm_Value%TYPE  := NULL;
  P_Success     BOOLEAN       := NULL;
  P_Message     VARCHAR2(255) := NULL;
  --
  V_Child       INTEGER;
  --
BEGIN
  --
  Extracted_Values.Fix_bid_Ask( P_Program_Id
                              , P_Load_Date
                              , 500
                              , P_Success 
                              , P_Message 
                              );
  IF P_Success
  THEN
    DBMS_OUTPUT.PUT_LINE( 'Success' );
  ELSE
    DBMS_OUTPUT.PUT_LINE( 'Failure.  Once more and you''re history!' );
  END IF;
  DBMS_OUTPUT.PUT_LINE( nvl( P_Message, 'No Message' ) ) ;
  IF NOT P_Success
  THEN
    RAISE Program_Error;
  END IF;
END;
/
SPOOL OFF
EXIT
