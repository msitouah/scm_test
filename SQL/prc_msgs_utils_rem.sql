-- Insert a Message in Pricing_Msgs 
-- using Oasis_Messages
-- Richard (23 Jun 2005)
-- Based on prc_msgs_utils.sql
-- Include a remark that can be set to null
-- JSL (15 nov 2005)
-- OASIS-3910 (An update to Product_Running_Parms is also required)
-- Richard (24Nov2014)
--
set term off
set verify off
--
define pgm_id   = &1
define Msg_Type = &2
define Msg_No   = &3
define Remarks  = &4
--
declare
   V_Del_Flag   Oasis_Messages.Delete_Flag%Type;
   V_Message    Oasis_Messages.Text_E%Type;
   V_Message_2  Oasis_Messages.Text_E%Type;
   V_Msg        Oasis_Messages.Code%Type;
   V_Msg_2      Oasis_Messages.Code%Type;
   V_Remarks    Pricing_Msgs.Remarks%Type;
   V_Msg_No     Integer(6);
   V_Msg_Root   Varchar2(6);
   V_Msg_Type   Varchar2(12);
   V_Pgm_ID     Ops_Scripts.Program_ID%Type;
   V_Start_Date Date;
   V_Success    Boolean;
begin
   V_Del_Flag   := 'N';
   V_Msg_No     := to_number('&&Msg_No');
   V_Msg_Root   := 'UTL-';
   V_Msg_Type   := '&&Msg_Type';
   V_Pgm_ID     := to_number('&&pgm_id');
   V_Start_Date := sysdate;
   V_Remarks    := '&&remarks';
   IF UPPER(V_Remarks) = 'NULL'
   THEN
     V_Remarks  := NULL;
   END IF;
   -- If Message # is greater than 10000
   -- This is not a Success (Richard - 23 Jun 2005)
   V_Success    := True;
   If V_Msg_No > 10000
   Then 
     V_Msg_No   := V_Msg_No - 10000;
     V_Success  := False;
   End IF;
   --
   -- Insert a Pricing Messages
   --
   V_Msg   := V_Msg_Root || V_Msg_Type || '-' ||
            substr(to_char(v_msg_No, '009'),2,3);
   V_Msg_2 := V_Msg_Root || V_Msg_Type || '-' ||
            substr(to_char(v_msg_No+5, '009'),2,3);
   --
   Manage_Messages.Get_Oasis_Message(
       V_Msg,
       V_Del_Flag,
       V_Message);
   Manage_Messages.Get_Oasis_Message(
       V_Msg_2,
       V_Del_Flag,
       V_Message_2);
   insert
     into pricing_msgs
      (date_of_msg, program_id, date_of_prices,
       msg_text,    msg_text_2, successfull_flag,
       remarks,     last_user,  last_stamp)
   select
       V_Start_Date, V_Pgm_ID,    Constants.get_site_date,
       V_Message,    V_Message_2, 'N',
       V_Remarks,    Constants.get_user_id, sysdate
     from Dual;
   --
   Commit;
   --
   IF V_Success
   Then
     V_Msg   := V_Msg_Root || V_Msg_Type || '-' ||
                substr(to_char(v_msg_No+10, '009'),2,3);
     --
     Manage_Messages.Get_Oasis_Message(
         V_Msg,
         V_Del_Flag,
         V_Message);
     --
     -- Added by Richard (24Nov2014)
     -- Required by TMS
     Update product_running_parms
        set last_run_date = sysdate
      Where Program_Id  = V_Pgm_ID;
     -- End Added by Richard (24Nov2014)
    
     Update Pricing_Msgs
        set msg_text         = V_Message,
            successfull_flag = 'Y',
            last_stamp       = sysdate
      Where Program_Id  = V_Pgm_ID
        and date_of_msg = V_Start_Date;
   Else
     --
     Update Pricing_Msgs
        set Err_text         = 'Job Failed',
            successfull_flag = 'N',
            last_stamp       = sysdate
      Where Program_Id  = V_Pgm_ID
        and date_of_msg = V_Start_Date;
   --
   End IF;
   --
   Commit;
   --
end;
/
exit
