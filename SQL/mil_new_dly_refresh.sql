set serveroutput on
set timing on
set time   on
define ref_log_file=&1
define spool_file=&2
spool &spool_file

WHENEVER SQLERROR EXIT FAILURE

DECLARE
   C_Program_Id              OPS_Scripts.Program_ID%TYPE:= 3115; -- MILM/UNV
   C_Ext_Program_Id          OPS_Scripts.Program_ID%TYPE:= 6537; -- MILM/SVS
   C_Client_Code             Clients.Code%TYPE:='MILM';
--
   V_Log_File                VARCHAR2(80);
   V_Site_Date               DATE:=Constants.Get_Site_Date;
   V_Next_Run_Date           DATE;
   V_Success                 BOOLEAN:=FALSE;
   V_Msg                     VARCHAR2(512):=NULL;
   V_DB_Mode    Databases.Database_Mode_Code%Type:=Constants.Get_DB_Mode;
   V_DB_Date    Date;
   V_Check_Next_Run          VARCHAR2(1);
   --
   Process_Error             EXCEPTION;
   --
   CURSOR Cur_Get_Next_Run_Date(I_PgmID OPS_Scripts.Program_ID%TYPE) IS
   SELECT Next_Run_Date
   FROM   Product_Running_Parms
   WHERE  Program_Id = I_PgmID;
   --
BEGIN
   V_Success    := FALSE;
   V_Log_File   := '&ref_log_file';
   V_Next_Run_Date := NULL;
   V_Check_Next_Run := 'Y';
--
   IF V_DB_Mode = 'PROD'
   Then
     V_DB_Date := trunc(Sysdate);
   Else
     V_DB_Date := V_Site_Date;
   END IF;
-------------------------------------------
-- Daily Refresh using MILM Client Universe
-------------------------------------------
--
-- Do we need to check for next run date from parameter CHK_RUN in Break_Points?
--
   V_Check_Next_Run := NVL(Scripts.Get_Arg_Value(C_Program_Id,'CHK_RUN'),
                           V_Check_Next_Run);
--
   IF V_Check_Next_Run='Y'
   THEN
      OPEN Cur_Get_Next_Run_Date(C_Ext_Program_Id);
      FETCH Cur_Get_Next_Run_Date INTO V_Next_Run_Date;
      CLOSE Cur_Get_Next_Run_Date;
   --
   -- Skip the Daily Refresh if not to be run
   --
      IF V_Next_Run_Date IS NULL OR
         V_Next_Run_date <> V_DB_Date
      THEN
         GOTO Get_Out;
      END IF;
   END IF;
   --
   Manage_MIL_Prices.Maintenance(C_Program_Id
                                ,C_Client_Code
                                ,V_Log_File
                                ,V_Success
                                ,V_Msg);
   IF NOT V_Success
   THEN
      RAISE Process_Error;
   END IF;
<<GET_OUT>>
   NULL;
EXCEPTION
   WHEN OTHERS THEN
   RAISE;
END;
/
spool off
EXIT
