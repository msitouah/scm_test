set serveroutput on
-- Will generate the warrant underlying securities Report
--
-- Mourad - 02Jan2016
--
define pgm_id=&1
--
WHENEVER SQLERROR EXIT FAILURE

declare
   p_pgm_id        ops_scripts.program_id%type;
   v_running_date  date;
   v_success       boolean;
   v_message       varchar2(1024);
   --
begin
   --
   p_pgm_id        := to_number('&&pgm_id');
   v_running_date  := constants.get_site_date;

   --
   warning_warrants_rpt.generate_report( p_pgm_id,
                                         v_running_date,
                                         v_success,
                                         v_message);
   --
end;
/
exit
