-- Will generate the Stale CAF Report
--
-- Miglena-12Dec2014
--
define pgm_id      = &1
--
declare
   p_pgm_id        ops_scripts.program_id%type;
   v_running_date  date;
   v_success       boolean;
   v_message       varchar2(1024);
   --
begin
   --
   p_pgm_id        := to_number('&&pgm_id');
   v_running_date  := constants.get_previous_site_date;

   --
   stale_caf_rep.display_stale_ca
                              ( p_pgm_id,
                                v_running_date,
                                v_success,
                                v_message);
   --
end;
/
exit
