set timing on
set time   on
set serveroutput on
define pgmid=&1
define clt_code=&2
define out_file=&3
define log_file=&4
define spool_file=&5
define fri_clt=&6
define product=&7
define sub_product=&8
define cmd_file=&9
define cmd_rec=&10
define prc_date=&11
spool &spool_file

WHENEVER SQLERROR EXIT FAILURE

BEGIN
   dbms_output.put_line('****************************************************');
   dbms_output.put_line('*** This extract is time consuming               ***');
   dbms_output.put_line('*** Please be patient                            ***');
   dbms_output.put_line('*** Starting time is            : ' ||
                       TO_CHAR(SYSDATE,                 'HH24:MI          ***'));
   dbms_output.put_line('*** Estimated finishing time is : ' || 
                       TO_CHAR(SYSDATE+45/60/24,        'HH24:MI          ***'));
   dbms_output.put_line('***                                              ***');
   dbms_output.put_line('*** CALL PROGRAMMING ONLY IF STILL RUNNING AFTER ***');
   dbms_output.put_line('*** THE ESTIMATED FINISHING TIME                 ***');
   dbms_output.put_line('****************************************************');
END;
/

-- SSI 18SEP2015 - OASIS-5616 - Set default end date to be previous site date
--
DECLARE
--
   C_Saturday      VARCHAR2(1):='7';
   C_Sunday        VARCHAR2(1):='1';
   V_Num_Years     NUMBER:=5;
--
   V_Program_Id    OPS_Scripts.Program_Id%TYPE;
   V_Clt_Code      Clients.Code%TYPE;
   V_Out_File      VARCHAR2(80);
   V_Log_File      VARCHAR2(80);
   V_Clt_Univ      Clients.Code%TYPE;
   V_Start_Date    DATE;
   V_End_Date      DATE;
   V_Success       BOOLEAN:=FALSE;
   V_Msg           VARCHAR2(512):=NULL;
   V_Dir_Name      VARCHAR2(60):=Constants.Get_Dir_Name;
   V_Cmd_File      VARCHAR2(80):='&&cmd_file';
   V_Rec           VARCHAR2(80):='&&cmd_rec';
   V_Fri_Client    Clients.Fri_Client%TYPE:=&&fri_clt;
   V_Product       Products.Code%TYPE:='&&product';
   V_Sub_Product   Sub_Products.Sub_Product_Code%TYPE:='&&sub_product';
   V_File_Handle   Utl_File.File_Type;
   V_Dated_File    VARCHAR2(80);
   V_Price_Date    DATE;
   V_Autoflush     BOOLEAN:= TRUE;
--
   Process_Error   EXCEPTION;
--
   CURSOR Cur_Get_Clt_Univ
   IS
   SELECT Code
   FROM   Clients
   WHERE  Fri_Client = (Select NVL(Fri_Client_Universe,Fri_Client)
                        FROM   Clients
                        WHERE  Code = V_CLt_Code);
--
BEGIN
   V_Success    := FALSE;
   V_Program_id := &pgmid;
   V_Clt_Code   := '&clt_code';
   V_Out_File   := '&out_file';
   V_Log_File   := '&log_file';
--
   V_Price_Date := TO_DATE('&&prc_date','DD-MON-YYYY');
--
   V_Start_Date := Scripts.Get_Break_Arg_Value(V_Program_Id,'DATE1');
   V_End_Date   := Scripts.Get_Break_Arg_Value(V_Program_Id,'DATE2');

--
   V_Clt_Univ  := Scripts.Get_Arg_Value(V_Program_Id,'CLT');
--
   IF V_Clt_Univ IS NULL THEN
      OPEN Cur_Get_Clt_Univ;
      FETCH Cur_Get_Clt_Univ INTO V_Clt_Univ;
      CLOSE Cur_Get_Clt_Univ;
   END IF;
   IF V_Clt_Univ IS NULL
   THEN
      V_Msg :='No client universe is specified, program abort';
      RAISE process_Error;
   END IF;
--    
-- If no defined dates, default extract of number of years in V_Num_Years
--
   IF V_End_Date IS NULL
   THEN
--    SSI 18SEP2015 - Set default end date to be previous site date
--    V_End_Date := Constants.Get_Site_Date;
      V_End_Date := Constants.Get_Previous_Site_Date;
   END IF;
   IF V_Start_Date IS NULL
   THEN
      V_Start_Date := ADD_MONTHS(V_End_Date,-60);
--    Adjust to previous week day if it is a Saturday or Sunday
      IF TO_CHAR(V_Start_Date,'D')=C_Saturday
      THEN
         V_Start_Date := V_Start_Date - 1;
      ELSIF TO_CHAR(V_Start_Date,'D')=C_Sunday
      THEN
         V_Start_Date := V_Start_Date - 2;
      END IF;
   END IF;
--
   DBMS_Output.Put_Line('Extracting historical prices');
   DBMS_Output.Put_Line('Client :'  || V_Clt_Univ);
   DBMS_Output.Put_Line('Start_Date :' || TO_CHAR(V_Start_Date));
   DBMS_Output.Put_Line('End   Date :' || TO_CHAR(V_End_Date));
   DBMS_Output.Put_Line('Out_File:' || V_Out_File);
   DBMS_Output.Put_Line('Log_File:' || V_Log_File);
--
   Manage_mil_Prices.Extract_Prices(V_Program_Id
                                   ,V_Clt_Univ  -- Client universe to be extracted
                                   ,V_Start_Date
                                   ,V_End_Date  
                                   ,V_Out_File
                                   ,V_Log_File
                                   ,V_Success
                                   ,V_Msg);
--
   IF V_Success
   THEN
      DBMS_Output.Put_Line('Extraction of historical prices successfull');
      DBMS_Output.Put_Line('Get name for output file in zip');
      V_File_Handle := UTL_FILE.FOPEN( V_Dir_Name, V_Cmd_File, 'W');
--
      Product_Component.Get_Dated_File_Name(V_Fri_Client,
                                      V_product,
                                      V_sub_product,
                                      'P',
                                      V_Price_Date,
                                      V_Dated_File,
                                      V_Success,
                                      V_Msg
                                     ); 
      IF V_Success
      THEN
         V_Dated_File := V_Dated_File;
         V_Rec := REPLACE(V_Rec,'%%%%%',V_Dated_File);
         V_Rec := V_Rec || ' export ' || SUBSTR(V_Rec,1,INSTR(V_Rec,'=',1)-1);
         Utl_File.Put_Line( V_File_handle, V_Rec , V_Autoflush);
         Utl_File.Fclose( V_File_Handle );
      ELSE
         DBMS_Output.Put_Line('Error Copy_dated_File_Name  ' || V_Msg);
         RAISE Process_Error;
      END IF;
--
   ELSE
      DBMS_Output.Put_Line('Error Extracting: ' || V_Msg);
      RAISE Process_Error;
   END IF;
EXCEPTION
   WHEN OTHERS THEN
   dbms_output.put_line('MIL_Hist failed');
   dbms_output.put_line(V_Msg);
   RAISE;
END;
/
spool off
EXIT
