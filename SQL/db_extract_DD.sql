define system='OASIS_NEW'
set term off
host cp $DASSIST/constraints.OASIS_NEW $DASSIST/constraints.OASIS_OLD
host cp $DASSIST/grants.OASIS_NEW      $DASSIST/grants.OASIS_OLD
host cp $DASSIST/indexes.OASIS_NEW     $DASSIST/indexes.OASIS_OLD
host cp $DASSIST/indexes_col.OASIS_NEW $DASSIST/indexes_col.OASIS_OLD
host cp $DASSIST/links.OASIS_NEW       $DASSIST/links.OASIS_OLD
host cp $DASSIST/objects.OASIS_NEW     $DASSIST/objects.OASIS_OLD
host cp $DASSIST/roles.OASIS_NEW       $DASSIST/roles.OASIS_OLD
host cp $DASSIST/synonyms.OASIS_NEW    $DASSIST/synonyms.OASIS_OLD
host cp $DASSIST/sysroles.OASIS_NEW    $DASSIST/sysroles.OASIS_OLD
host cp $DASSIST/tables.OASIS_NEW      $DASSIST/tables.OASIS_OLD
host cp $DASSIST/tables_col.OASIS_NEW  $DASSIST/tables_col.OASIS_OLD
host cp $DASSIST/tsp.OASIS_NEW         $DASSIST/tsp.OASIS_OLD
host cp $DASSIST/tspcnt.OASIS_NEW      $DASSIST/tspcnt.OASIS_OLD
host cp $DASSIST/tspind.OASIS_NEW      $DASSIST/tspind.OASIS_OLD
host cp $DASSIST/tsptab.OASIS_NEW      $DASSIST/tsptab.OASIS_OLD
host cp $DASSIST/users.OASIS_NEW       $DASSIST/users.OASIS_OLD
@$ORADHOME/sql/list.cons
@$ORADHOME/sql/list.grn
@$ORADHOME/sql/list.ind
@$ORADHOME/sql/list.indcol
@$ORADHOME/sql/list.lnk
@$ORADHOME/sql/list.obj
@$ORADHOME/sql/list.role
@$ORADHOME/sql/list.syn
@$ORADHOME/sql/list.sysrole
@$ORADHOME/sql/list.tab
@$ORADHOME/sql/list.tabcol
@$ORADHOME/sql/list.tsp
@$ORADHOME/sql/list.tspcnt
@$ORADHOME/sql/list.users
set term on
select sysdate from dual;
exit
