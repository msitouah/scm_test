rem  SQL*Plus FRI login startup file.
rem
rem  This is the FRI login file for SQL*Plus.
rem  Add any sqlplus commands here that are to be 
rem  executed when a user invokes sqlplus
rem prompt
rem prompt |              C  O  R  P  O  R  A  T  I  O  N                | 
rem prompt |            ----------    ---------        ----              |
rem prompt |            ----------    ------------     ----              |
rem prompt |            ----          ----    -----    ----              |
rem prompt |            **********    ****  *****      ****              |
rem prompt |            ****          ****  ****       ****              |
rem prompt |            ****          ****   ****      ****              |
rem prompt |            ****          ****    ****     ****              |
rem prompt |                                   ****                      |
rem prompt |              C  O  R  P  O  R  A  T  I  O  N                | 
define _editor=vi;
rem !echo "Editor is vi"
!echo "Directory is " `pwd`
show user
set heading off
set pause off
select 'Welcome to the [' || database_code || '] Database. ' || 
       'Oasis Site Date is: ' ||
       to_char(Constants.Get_Site_Date, 'DD-Mon-YYYY')
  from site_controls;
-- !echo "Welcome to the [${ORACLE_SID}] Database."
set heading on;
set pause "                                       Press <RTN> to continue ..."
set pause on
set pagesize 24
