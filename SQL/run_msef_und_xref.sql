-- Will generate the Undefined Values in XREF tables for MSEF 
--
-- Miglena-09Jun2015
--
define pgm_id      = &1
--
declare
   p_pgm_id        ops_scripts.program_id%type;
   v_running_date  date;
   v_success       boolean;
   v_message       varchar2(1024);
   --
begin
   --
   p_pgm_id        := to_number('&&pgm_id');
   v_running_date  := constants.get_site_date;

   --
   undefined_msef_xref.display_und_msef_xref
                                  ( p_pgm_id,
                                    v_running_date,
                                    v_success,
                                    v_message);
   --
end;
/
exit
