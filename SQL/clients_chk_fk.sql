set term off
-- Script to call the procedure that validates
-- Fri_Clients_Universe and Master_Fri_Client
-- in the table Clients. Is the Referential Integrity preserved?
-- Richard (15 Dec 2005)
--
DECLARE
   V_Success    BOOLEAN;
   V_MESSAGE    VARCHAR2(100);
   V_PgmID      Number := 101;
begin
    Client.Validate_Clients_FK(V_PgmID,
                               V_Success,
                               V_Message);
    -- IF V_Success THEN
    --    dbms_output.put_line('Success : YES');
    -- ELSE
    --    dbms_output.put_line(V_Message);
    --    dbms_output.put_line('Success : NO');
    -- END IF;
end;
/
Exit;
