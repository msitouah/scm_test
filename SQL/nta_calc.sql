-- Will calculate NTA prices from yields
--
-- Miglena-17Jun2015
--
define pgm_id      = &1
--
declare
   p_pgm_id        ops_scripts.program_id%type;
   v_running_date  date;
   v_freq          number := 1;
   v_success       boolean;
   v_message       varchar2(1024);
   
   --
begin
   --
   p_pgm_id        := to_number('&&pgm_id');
   
   
   v_running_date  := constants.get_site_date;

   --
   gen_prices_from_yields.calc_prices( p_pgm_id
                                     , v_running_date
                                     , v_freq
                                     , v_success     
                                     , v_message );
   --
end;
/
exit