-- Will generate the DEJA MF Prc Report
--
-- Miglena-02Nov2015
--
define pgm_id      = &1
--
declare
   p_pgm_id        ops_scripts.program_id%type;
   v_running_date  date;
   v_success       boolean;
   v_message       varchar2(1024);
   --
begin
   --
   p_pgm_id        := to_number('&&pgm_id');
   v_running_date  := constants.get_site_date;

   --
   deja_mf_prc.display_deja_mf_prc
                              ( p_pgm_id,
                                v_running_date,
                                v_success,
                                v_message);
   --
end;
/
exit