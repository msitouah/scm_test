-- set serveroutput ON
--
-- Si des procedures ou fonctions sont invalides
-- des messages seront envoyes dans l'agenda du DBA
-- Richard (23 sept 1997)
--
set term   off
set verify off
--
define Pgm_ID = &1
--
declare
  V_Pgm_ID     OPS_Scripts.Program_ID%Type := to_Number('&&Pgm_ID');
  V_Status     number;
  V_Status_msg varchar2(60);
begin
  db_utilities.invalid_objects_upd_agenda
     (V_Pgm_ID, V_Status, V_Status_msg);
end;
/
exit;
