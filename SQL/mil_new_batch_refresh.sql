set serveroutput on
set time on
set timing on
WHENEVER SQLERROR EXIT FAILURE
DECLARE
    C_Program_Id          OPS_Scripts.Program_Id%TYPE:=224;
--  SSI - 27NOV2013
    C_Client_Code         Clients.Code%TYPE:='MILM';
    V_Log_File            VARCHAR2(80);
    V_Success             BOOLEAN;
    V_Message             VARCHAR2(512);
--
    Process_Error         Exception;
--
BEGIN
--
   V_Log_File := 'mil_batch_refresh.log';
--
-- Manage_MIL_Prices.Batch_Refresh (C_Program_Id
--                                 ,V_Log_File
--                                 ,V_Success
--                                 ,V_Message);
--
   Manage_MIL_Prices.Maintenance_To_Recalc(C_Program_Id
                                          ,C_Client_Code
                                          ,V_Log_File
                                          ,V_Success
                                          ,V_Message);
   IF NOT V_Success
   THEN
      dbms_output.Put_Line('Refresh unsuccessfull');
      dbms_output.Put_Line(V_Message);
      RAISE Process_Error;
   ELSE
      dbms_output.Put_Line('Refresh successfully completed');
   END IF;
EXCEPTION
   WHEN OTHERS THEN
      dbms_output.Put_Line('Refresh successfully completed');
      dbms_output.Put_Line(SUBSTR(SQLERRM(SQLCODE),1,255));
      RAISE;
END;
/
EXIT
