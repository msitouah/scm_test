set serveroutput on size 60000
define program_id=&1
define spool_file=&2
spool &&spool_file

prompt '********************************'
prompt '**                            **'
prompt '** This load may take a while **'
prompt '**                            **'
prompt '**         BE PATIENT         **'
prompt '**         **********         **'
prompt '**                            **'
prompt '********************************'

whenever sqlerror exit sql.sqlcode;

set serveroutput on 

DECLARE
BEGIN
  --
  Tick_changes.Deactivate_Money_Markets( TO_NUMBER( '&&Program_Id' )
                                       );
END;
/
exit;
