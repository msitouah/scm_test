set serveroutput on size 60000
define program_id=&1
define cf_program_id=&2
define Delivery_Date=&3
define Delivery_Time=&4
define Data_Supplier_Code=&5
define date_of_msg=&6
define msg_text=&7
define msg_text_2=&8
define spool_file=&9
spool &&spool_file

prompt '********************************'
prompt '**                            **'
prompt '** This load may take a while **'
prompt '**                            **'
prompt '**         BE PATIENT         **'
prompt '**         **********         **'
prompt '**                            **'
prompt '********************************'

whenever sqlerror exit sql.sqlcode;

set serveroutput on 

declare
--
-- Modified :
--
-- JSL - 30 jan 2004 - Accept code 140 if the failure flag is N
--                     This means that someone already acknowledged
--                     the problem and is doing a restart.
-- JSL - 01 mar 2004 - Validate the delivery date : can only be current
--                     or previous.
--                   - Supplier may not require a carry forward,
--                     e.g. dividends.
-- JSL - 18 mar 2008 - Added the carry forward on GICS.
-- JSL - 29 apr 2009 - Added the carry forward on Yields.
-- RV  - 04 Jun 2012 - V_Commit_Count is now 8192 instead of 100
-- JSL - 06 mar 2014 - OASIS-3491
--                   - Added the carry forward on Exchange Rates
-- JSL - 17 sep 2014 - OASIS-4238
--                   - Add processing to treat only carry forward
--
   pgmloc           INTEGER;
-- V_Commit_Count   INTEGER := 100;
   V_Commit_Count   INTEGER := 8192;
   V_Delivery_Date  Date;
   V_system_date    Date;
   V_Program_Id     ops_Scripts.Program_Id%TYPE;
   V_CF_Program_Id  ops_Scripts.Program_Id%TYPE;
   V_Delivery_Time  data_supply_schedule.Delivery_Time%TYPE;
   V_Data_Supplier_Code Data_Suppliers.Code%TYPE;
   V_Carry_Forward  BOOLEAN;
   V_Frestart       Yes_No.Code%TYPE;
   -- ... Added by Richard (30 Jan 2004)
   -- ... New Argument in Pricing.get_loading_status
   V_Failure        Successfull_Flags.Failure_Flag%Type;
   -- End Added by Richard (30 Jan 2004)
   V_Status         number := NULL;
   V_Message        Varchar2(200) := NULL;
-- Ajout - JSL - 17 septembre 2014
   B_Carry_Forward_Only BOOLEAN;
-- Fin d'ajout - JSL - 17 septembre 2014
--
   Pricing_Msgs_Record Pricing_Msgs%ROWTYPE;
   Break_Points_Record Break_Points%ROWTYPE;
--
Cursor Get_break_Points (
       I_Program_Id Ops_Scripts.Program_Id%Type
     ) IS
select *
  from break_points B
 where B.program_id = I_Program_Id;
--
-- Added - JSL - 01 mar 2004
Cursor Get_Data_Types (
       I_Program_Id Ops_Scripts.Program_Id%Type
     ) IS
select DT.*
  from Data_Types DT
     , Data_Supply_Schedule DSS
 where DSS.program_id = I_Program_Id
   and DT.code        = DSS.Data_Type_Code;
--
DT_Record          Data_Types%ROWTYPE;
-- Fin d'ajout - JSL - 01 mar 2004
--
begin
--
   pgmloc := 10;
   V_Program_Id := TO_NUMBER('&&Program_Id');
   V_CF_Program_Id := TO_NUMBER('&&CF_Program_Id');
   V_Delivery_Date := TO_DATE('&&Delivery_Date', 'DD-MON-YYYY');
   V_Delivery_Time := '&&Delivery_Time';
   V_Data_Supplier_Code := UPPER('&&Data_Supplier_Code');
   V_System_Date := TO_DATE('&&date_of_msg', 'DDMonYYYYHH24miss');
   V_Carry_Forward := FALSE;
   V_Frestart      := 'N';
-- Added - JSL - 01 mar 2004
   IF  V_Delivery_Date != Constants.Get_Site_Date
   AND V_Delivery_Date != Constants.Get_Previous_Site_Date
   THEN
     Manage_Messages.Get_Oasis_Message( 'PROC-CHECK_LOAD-010'
                                      , 'N'
                                      , TO_CHAR( V_Delivery_Date )
                                      , TO_CHAR( Constants.Get_Site_Date )
                                      , TO_CHAR( Constants.Get_Previous_Site_Date )
                                      , V_Message
                                      );
     raise Program_Error;
   END IF;
   --
   DT_Record.CFW_Required_Flag := 'Y';
   pgmloc := 20;
   OPEN  Get_Data_Types( V_Program_Id );
   pgmloc := 30;
   FETCH Get_Data_Types INTO DT_record;
   pgmloc := 40;
   CLOSE Get_Data_Types;
-- Fin d'ajout - JSL - 01 mar 2004
   --
   pgmloc := 50;
   pricing.get_loading_status( V_Program_ID
                             , V_Delivery_Date
   -- ... Added by Richard (30 Jan 2004)
                             , V_Failure
   -- End Added by Richard (30 Jan 2004)
                             , V_Status
                             , V_Message
                             );
dbms_output.put_line('V_Status : '||v_status);
dbms_output.put_line('V_Message : '||v_message);
   --
-- Ajout - JSL - 17 septembre 2014
   Pricing_Msgs_Record.Msg_Text   := REPLACE('&&Msg_Text','!',' ');
   IF INSTR( Pricing_Msgs_Record.Msg_Text, 'CFW' ) <> 0
   THEN
      B_Carry_Forward_Only := TRUE;
   ELSE
      B_Carry_Forward_Only := FALSE;
-- Fin d'ajout - JSL - 17 septembre 2014
     --
     pgmloc := 60;
     Global_File_Updates.Initialize_Pricing_Msgs( V_Program_Id
                                                , V_System_Date
                                                , V_Delivery_Date
                                                , FALSE
                                                , Pricing_Msgs_Record
                                                );
     --
     pgmloc := 70;
     Pricing_Msgs_Record.Msg_Text   := REPLACE('&&Msg_Text','!',' ');
     Pricing_Msgs_Record.Msg_Text_2 := REPLACE('&&Msg_Text_2','!',' ');
     --
     pgmloc := 80;
     Global_File_Updates.Update_Pricing_Msgs( V_Program_Id
                                            , V_System_Date
                                            , FALSE
                                            , Pricing_Msgs_Record
                                            );
     --
     pgmloc := 90;
     COMMIT;
-- Ajout - JSL - 17 septembre 2014
   END IF;
-- Fin d'ajout - JSL - 17 septembre 2014
   --
   pgmloc := 100;
-- Added 140 - JSl - 30 jan 2004
   IF V_Status in ( 0, 110, 120, 130, 140 )
   THEN
      pgmloc := 110;
      OPEN GET_Break_Points ( V_Program_ID );
      pgmloc := 120;
      FETCH Get_Break_Points INTO Break_Points_Record;
      IF Get_Break_Points%NOTFOUND
      THEN
         V_Message := 'Cannot access Break_Points';
         raise program_error;
      END IF;
      pgmloc := 130;
      CLOSE Get_Break_Points;
      IF Break_Points_Record.Status = 'FRESTART'
      THEN
         V_Frestart := 'Y';
      END IF;
   ELSE
      Raise program_error;
   END IF;
   --
   pgmloc := 140;
   IF V_Status = 110
   THEN
      V_Carry_Forward := TRUE;
   ELSIF V_Status = 120
   THEN
      IF V_Frestart = 'Y'
      THEN
         V_Carry_Forward := TRUE;
      ELSE
         Raise program_error;
      END IF;
-- Added - JSL - 30 jan 2004
   ELSIF V_Status = 140
     AND V_Failure <> 'N'
   THEN
      Raise program_error;
-- End of add - JSL - 30 jan 2004
   ELSE
      V_Carry_Forward := FALSE;
   END IF;
   --
-- Added - JSL - 30 jan 2004
   IF V_Frestart = 'Y'
   THEN
      V_Carry_Forward := TRUE;
   END IF;
-- End of add - JSL - 30 jan 2004
   --
   IF V_Carry_Forward
-- Modification - JSL - 17 septembre 2014
-- -- Added - JSL - 01 mar 2004
--    AND DT_Record.CFW_Required_Flag = 'Y'
-- -- Fin d'ajout - JSL - 01 mar 2004
-- THEN --{
-- Ajout - JSL - 18 mars 2008
   THEN
      IF DT_Record.CFW_Required_Flag = 'Y'
      THEN --{
-- Fin de modification - JSL - 17 septembre 2014
      IF DT_Record.Code = 'GIC'
      THEN --{
        pgmloc := 150;
        Carry_FWD.On_Gics( V_CF_Program_Id
                         , V_Delivery_date
                         , V_Data_Supplier_Code
                         , V_delivery_time
                         , V_commit_count
                         , V_Frestart
                         , V_Delivery_date
                         , 'N'
                         );
-- Ajout - JSL - 29 avril 2009
      ELSIF DT_Record.Code = 'YLD'
      THEN --}{
        pgmloc := 160;
        Carry_FWD.CFW_Yield_Curves( V_Data_Supplier_Code
                                  , V_Delivery_date
                                  , V_delivery_time
                                  , V_commit_count
                                  , V_Frestart
                                  , V_Delivery_date
                                  , 'N'
                                  );
-- Fin d'ajout - JSL - 29 avril 2009
-- Ajout - JSL - 06 mars 2014
      ELSIF DT_Record.Code = 'EXR'
      THEN --}{
        pgmloc := 161;
        Carry_FWD.On_Exch_Rates( V_CF_Program_Id
                                   , V_Delivery_date
                                   , V_Data_Supplier_Code
                                   , V_delivery_time
                                   , V_commit_count
                                   , V_Frestart
                                   , V_Delivery_date
                                   , 'N'
                                   );
-- Fin d'ajout - JSL - 06 mars 2014
      ELSE --}{ DT_Record.Code = 'PRI'
-- Fin d'ajout - JSL - 18 mars 2008
        pgmloc := 170;
        Carry_FWD.On_Security_Prices( V_CF_Program_Id
                                  , V_Delivery_date
                                  , V_Data_Supplier_Code
                                  , V_delivery_time
                                  , V_commit_count
                                  , V_Frestart
                                  , V_Delivery_date
                                  , V_program_id
                                  , 'N'
                                  );
      END IF; --}
-- Ajout - JSL - 17 septembre 2014
      END IF; --} IF DT_Record.CFW_Required_Flag = 'Y'
   ELSE
      IF B_Carry_Forward_Only
      THEN --{
         -- Insert a pricing mesage to show that something was done.
         pgmloc := 180;
         Global_File_Updates.Initialize_Pricing_Msgs( V_CF_Program_Id
                                                    , V_System_Date
                                                    , V_Delivery_Date
                                                    , FALSE
                                                    , Pricing_Msgs_Record
                                                    );
         --
         pgmloc := 190;
         Pricing_Msgs_Record.Msg_Text   := REPLACE('&&Msg_Text','!',' ');
         Pricing_Msgs_Record.Msg_Text_2 := REPLACE('&&Msg_Text_2','!',' ');
         Pricing_Msgs_Record.Remarks    := 'Already done';
         --
         pgmloc := 200;
         Global_File_Updates.Update_Pricing_Msgs( V_CF_Program_Id
                                                , V_System_Date
                                                , FALSE
                                                , Pricing_Msgs_Record
                                                );
         --
         pgmloc := 210;
         COMMIT;
      END IF; --}
-- Fin d'ajout - JSL - 17 septembre 2014
   END IF; --}

   --
Exception
   When program_error
   then
      Pricing_Msgs_Record.Err_Text := substr(V_Message,1,60);
      Global_File_Updates.Update_Pricing_Msgs(
                           V_Program_ID
                         , V_system_Date
                         , FALSE
                         , Pricing_Msgs_Record
                         );
      COMMIT;
      raise_application_error( -20001
                             , V_Message||'  '||V_Status
                             , FALSE
                             );
   when others
   then
      dbms_output.put_line('Error in check_load.  Pgmloc '||pgmloc);
      dbms_output.put_line('Call programming.');
      raise;
end;
/
exit;
