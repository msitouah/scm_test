define pgmid=&1
define err_file=&2
spool &&err_file
set serveroutput on
--
whenever sqlerror exit sql.sqlcode;
--
DECLARE
  P_Program_Id  INTEGER       := TO_NUMBER( '&&pgmid' );
  P_Load_Date   DATE          := Constants.Get_Site_Date;
  P_Success     BOOLEAN       := NULL;
  P_Message     VARCHAR2(255) := NULL;
  --
  --
BEGIN
  --
  Status_Reset.Apply( P_Program_Id
                    , P_Load_Date
                    , 100         -- commit frequency
                    , P_Success 
                    , P_Message 
                    );
  IF P_Success
  THEN
    DBMS_OUTPUT.PUT_LINE( 'Success' );
  ELSE
    DBMS_OUTPUT.PUT_LINE( 'Failure.  Once more and you''re history!' );
  END IF;
  DBMS_OUTPUT.PUT_LINE( nvl( P_Message, 'No Message' ) ) ;
  IF NOT P_Success
  THEN
    RAISE Program_Error;
  END IF;
END;
/
SPOOL OFF
EXIT
