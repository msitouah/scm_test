-- Will run program 259 to verify number of inserted record FOREIGN_SECURITIES Price Flows Implied
--
-- Miglena-22Jan2015
--
define pgm_id      = &1
--
declare
   p_pgm_id        ops_scripts.program_id%type;
   v_running_date  date;
   v_success       boolean;
   v_message       varchar2(1024);
   v_commit_freq   number := 1;
   --
begin
   --
   p_pgm_id        := to_number('&&pgm_id');
   v_running_date  := constants.get_site_date;

   --
   price_flows.chk_pf_imp_daily_exec
                              ( p_pgm_id,
                                v_running_date,
                                v_commit_freq,
                                v_success,
                                v_message);
   --
end;
/
exit