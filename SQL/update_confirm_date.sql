set serveroutput on
define program_id=&1

set term ON 
set echo ON 
set verify ON 
set feedback ON 

whenever sqlerror exit sql.sqlcode;

Declare
--
--------------------------------------------------------------------------
--
-- Modification
-- JSL - 29jan2014 - oasis-2536
--                 - Accept that there are more than one submit done
--                   Client can send their maintenance file more than once
--                   and very close to each other. 
-- JSL - 22jun2015 - OASIS-5496
--                 - Get the entries from the queue having the most recent
--                   site date (for entries schedules on next business date)
--
--------------------------------------------------------------------------
--
  V_Success  BOOLEAN;
  V_Message  VARCHAR2(250);
  v_cnt      INTEGER;
-- Ajout - JSL - 25 mai 2011
  V_Dummy    Dual.Dummy%TYPE;
-- Fin d'ajout - JSL - 25 mai 2011
Begin
--
-- Modification - JSL - 22 juin 2015
--UPDATE Oasis_Queue
--SET Confirm_Date = SYSDATE
--   ,Last_User   = Constants.Get_User_Id
--   ,Last_Stamp  = SYSDATE
--WHERE Confirm_Date IS NULL
--and program_id = to_Number('&&program_Id')
--and site_date = constants.get_site_date
--and submit_date <= sysdate
--;
  UPDATE Oasis_Queue OQ
     SET Confirm_Date = SYSDATE
       , Last_User    = Constants.Get_User_Id
       , Last_Stamp   = SYSDATE
   WHERE OQ.Confirm_Date IS NULL
     and OQ.program_id  = to_Number('&&program_Id')
     and OQ.site_date  <= (
         SELECT MAX( OQ1.Site_Date )
           FROM Oasis_Queue OQ1
          WHERE OQ1.Program_Id = OQ.Program_Id
            AND OQ1.Submit_Date <= Sysdate
            AND OQ1.Confirm_Date IS NULL
            AND OQ1.Site_Date <= Constants.Get_Site_Date
         )
     and OQ.submit_date <= sysdate
       ;
-- Fin de modification - JSL - 22 juin 2015
  v_cnt := sql%rowcount;
  case v_cnt
  when 1 then 
    commit;
    Dbms_Output.Put_Line( 'SUCCESS' );
  when 0 then 
-- Ajout - JSL - 25 mai 2011
    -- Check if this was submitted through TMS
    V_Dummy := NULL;
    BEGIN
      SELECT Dummy
        INTO V_Dummy
        FROM DUAL
       WHERE EXISTS (
             SELECT 1
               FROM TMS_Running_Parms
              WHERE Program_Id = &&Program_Id
                AND Active_Flag = 'Y'
                AND Prod_Flag = 'Y'
                AND Prod_Date <= SYSDATE
             );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN NULL;
    END;
    IF V_Dummy IS NULL
    THEN
      NULL;
    ELSE
-- Fin d'ajout - JSL - 25 mai 2011
      Dbms_Output.Put_Line( 'FAILURE' );
      Dbms_Output.Put_Line( 'Unable to update oasis_queue, 0 row updated...!!!');
      RAISE Program_Error;
-- Ajout - JSL - 25 mai 2011
    END IF;
-- Fin d'ajout - JSL - 25 mai 2011
  else
-- Modification - JSL - 29 janvier 2014
--  rollback;
--  Dbms_Output.Put_Line( 'FAILURE' );
--  Dbms_Output.Put_Line( 'Unable to update oasis_queue, TOO MANY ROW UPDATED: '
--                     ||v_cnt||' !!!'
--                      );
--  RAISE Program_Error;
    commit;
    Dbms_Output.Put_Line( 'SUCCESS' );
-- Fin de modification - JSL - 29 janvier 2014
  end case;
END;
/
exit
