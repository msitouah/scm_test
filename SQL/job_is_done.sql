--
-- JSL - 31Aug2015 - OASIS-5916
--                 - Set a pricing message as done.
--
define program_id  =  &1
define System_Date =  &2
define Msg_Text    = '&3'
define Msg_Text_2  = '&4'
--
set pause off
set verify off
--
WHENEVER SQLERROR EXIT FAILURE
declare
   Pricing_Msg_Rec     Pricing_Msgs%ROWTYPE;
   P_program_id        Pricing_Msgs.program_id%TYPE;
   P_system_date       DATE;
   Cursor Get_Site_Date
        ( P_Program_Id ops_scripts.program_id%TYPE
        ) is
   Select min( site_date )
     from tms_queue
    where program_id = P_Program_Id
      and running_date >= constants.Get_Site_Date + 0.25
        ;
   V_Site_Date         Date;
begin
   p_program_id   := &&program_id;
   p_system_date  := to_date('&&system_date', 'DDMonYYYYHH24miss');
   --
   -- Get the site date from tms_queue
   V_Site_Date    := null;
   open  get_Site_Date( p_program_id );
   fetch get_site_Date into V_Site_Date;
   close get_site_date;
   V_Site_Date := NVL( V_Site_Date, Constants.Get_Site_Date );
--
-- Appel de la procedure Global_File_Updates
--
   Global_File_Updates.Initialize_Pricing_Msgs( P_Program_Id
                                              , P_System_Date
                                              , V_Site_Date
                                              , FALSE   -- Update break points
                                              , Pricing_Msg_Rec
                                              );
--
   Pricing_Msg_Rec.Msg_Text         := REPLACE('&&Msg_Text','!',' ');
   Pricing_Msg_Rec.Msg_Text_2       := REPLACE('&&Msg_Text_2','!',' ');
   Pricing_Msg_Rec.SuccessFull_Flag := 'Y';
--
   Global_File_Updates.Update_Pricing_Msgs( P_Program_Id
                                          , P_System_Date
                                          , TRUE   -- Update break points
                                          , Pricing_Msg_Rec
                                          );
--
   COMMIT;
end;
/
exit
