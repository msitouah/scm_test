CREATE OR REPLACE PACKAGE BODY &&owner..LOAD_DUAL_SOURCES IS
----------------------------------------------------------------------
--
-- mlz - __sept2011 - based on load_official_prices
-- JSL - 08mar2012 - Let the database trigger set the date bc.
-- JSL - 16mar2012 - In validate_record, consider only current values.
-- JSL - 04apr2013 - OASIS-2255
--                 - change the source code only on price (default on insert)
-- mlz - 02jul2013 - oasis-2527 - fix: use manage_prc_msgs_det.handle_t
-- mlz - 12jul2013 - oasis-2566 - minor change to official_prices.get_ds_price
-- mlz - 17mar2014 - oasis-3526 - added new logfile for non blocking errors
--                 - new param: LOGMON
-- mlz - 17may2014 - oasis-3817 - do not fail when there's no schedule
-- mlz - 29may2014 - oasis-3838 - email the logmon if errors occurred
-- mlz - 01jun2014 - oasis-3838 - no pgmid hardcode --> parameter
-- JSL - 24oct2014 - OASIS-4447
--                 - Tracking of restricted price supplier.
-- JSL - 06apr2015 - OASIS-5241
--                 - Accept prices even if source is deemed closed.
-- mlz - 15nov2015 - oasis-6222 - performance tuning
--
-------------------------------------------------------------------------------
--
--
  c_oasis_message_gen   constant varchar2(30) := 'PROC-GEN-';
  C_Oasis_Message       CONSTANT VARCHAR2(30) := 'PROC-LOAD_DATA_PRICES-';
  C_LSR_Oasis_Message   CONSTANT VARCHAR2(30) := 'PROC-LOAD_SUPPLIER_RULES-';
  --C_CFW_Oasis_Message  CONSTANT VARCHAR2(30) := 'PROC-CARRY_FORWARD-';
  C_MOP_Oasis_Message   CONSTANT VARCHAR2(30) := 'PROC-MANAGE_OFF_PRICES-';
  C_Date_Format         CONSTANT VARCHAR2(20) := 'DD-MON-RR';
  C_Round_String        CONSTANT VARCHAR2(10) := 'NN';
--C_Warning             CONSTANT VARCHAR2(1)  := 'W';
--C_Error               CONSTANT VARCHAR2(1)  := 'E';
--C_File_Type           CONSTANT VARCHAR2(16) := 'OFFICIAL';
  C_FRI_ID              CONSTANT Identification_Systems.Code%TYPE := 'X';
  C_Old_Date            CONSTANT DATE := TO_DATE( '01-jan-1901', 'dd-mon-yyyy' );

  c_parm_subst_del_time constant varchar2(20) := 'SUBST_DEL_TIME';
  c_parm_subst_supplier constant varchar2(20) := 'SUBST_SUPPLIER';
  c_parm_log_all        constant varchar2(7)  := 'LOG_ALL';
  c_parm_log_to_table   constant varchar2(12) := 'LOG_TO_TABLE';
  c_sep                 constant varchar2(1) := chr(9);

  fatal_error          exception;  --breaks execution
  handled_error        exception;  --only logged, no stop


  G_Data_Supplier_Code  Data_Suppliers.Code%TYPE;
  G_Delivery_Time       Data_Supply_Schedule.Delivery_Time%TYPE;
  g_Pricing_Msgs_Rec    Pricing_Msgs%ROWTYPE;
  g_user                varchar2(30);
  g_log_all             boolean;
  g_log_to_table          boolean;
  g_Log_Fh              Utl_File.File_Type;
  --mlz - 02jul2013
  g_lh  manage_prc_msgs_det.handle_t;
  --endmlz - 02jul2013  

--substitution supplier/del time: to use the existing rules of official_prices
--  stored in table: official_prc_nations
  G_Subst_Data_Supplier_Code  Data_Suppliers.Code%TYPE;
  G_Subst_Delivery_Time       Data_Supply_Schedule.Delivery_Time%TYPE;

  type reason_row_t is record
  (
    status    pricing_msgs_det.status%type,
    --mlz - 17may2014
    --text      varchar2(100)
    text      varchar2(1000)
    --endmlz - 17may2014
  );
  type reason_t is table of reason_row_t;

--C_Adjusted            CONSTANT VARCHAR2(20) := 'Ajdusted';
--C_Unadjusted          CONSTANT VARCHAR2(20) := 'Unadjusted';
--G_Adj_Treatment               VARCHAR2(20) := NULL;

  C_Reuter              CONSTANT Data_Suppliers.Code%TYPE := 'REUT';
  C_London              CONSTANT Sources.Code%TYPE        := 'L';
  C_Milan               CONSTANT Sources.Code%TYPE        := 'MI';

  G_End_Of_File                 BOOLEAN      := FALSE;
  GR_Security_Prices            Security_Prices%ROWTYPE;
--GR_Security_Prices_Adj        Security_Prices%ROWTYPE;


--G_File_Type                   VARCHAR2(16);
--G_Delivery_Date               DATE;
  Pgmloc                        INTEGER;
  G_Header_Processed            BOOLEAN;

--translated from ops_script parameter #price_type
--see Official_Prices package header for constants
--  g_price_type                integer;

--mlz - 12jul2013
g_program_id  ops_scripts.program_id%type;
--endmlz - 12jul2013

  --mlz - 17mar2014
  g_load_date             date;
  g_log_file              utl_file.file_Type;

  g_logmon                utl_file.file_type;
  c_logmon_sep            constant varchar2(1) := chr(9);
  c_oradir                constant varchar2(30) := 'XMLDIR';
  --endmlz - 17mar2014

  --mlz - 29may2014
  g_logmon_filename       varchar2(1000);
  g_cnt_logmon            binary_integer;
  --endmlz - 29may2014

  TYPE TT_Mid_From_Sources IS TABLE OF Sources.Code%TYPE INDEX BY VARCHAR2(4);
  G_Mid_From_Sources TT_Mid_From_Sources;

-- Ajout - JSL - 24 octobre 2014
  C_Parm_Track_Suppliers  CONSTANT Ops_Scripts_Parms.Parm_Name%TYPE := 'TRACK_SUPPLIERS';
  G_Track_Suppliers       String_Utils.T_Varchar;
-- Fin d'ajout - JSL - 24 octobre 2014


  --
  -- main cursor: get all securities active at one time on a source
  -- >> (1 line per source )
  -- no check if active for load_date (date param only to exclude posterior statuses)
  -- >> will be done in get_data_line
  --
  CURSOR cur_securities( p_load_date in date ) IS
    --mlz - 15nov2015
    --select sts.fri_ticker
    select /*+ index( sts security_trading_status_x_01 ) */
           sts.fri_ticker
    --endmlz - 15nov2015
         , sfr.nation_code as nation_m
         , s.security_type_code
         --
         , s.Trading_Status_Code
         , s.date_of_status
    from securities s
       , security_trading_status sts
       , sources_for_revolution sfr
       , trading_status ts
       , supplier_xref_sec_types x  --to filter security types
    where s.fri_ticker = sts.fri_ticker
    and sts.source_code = sfr.source_code
    and sts.date_of_status <= p_load_date --eliminates active sources in the future
    and sts.trading_status_code = ts.code
    and ts.active_flag = 'Y'
    and s.security_type_code = x.security_type_code
    and x.data_supplier_code = g_data_supplier_code --STPD
-- Modification - JSL - 15 mai 2012
--  and exists ( select 1
--                 from dual_fri_tickers dft
--                where dft.fri_ticker = s.fri_ticker
--      )
-- Fin de modification - JSL - 15 mai 2012
    group by sts.fri_ticker
           , sfr.nation_code
           , s.security_type_code
           , s.Trading_Status_Code
           , s.date_of_status
    order by fri_ticker, nation_code
    ;
  GR_Securities  cur_securities%ROWTYPE;

--mlz - 17mar2014
procedure raise_fatal_error is
begin
  --workaround needed as long as
  --  dbms_utility.format_error_stack is unable to give the
  --  correct error line number
  raise fatal_error;
end;
--endmlz - 17mar2014

--------------------------------------------------------------------------------
function get_oasis_msg( p_code in number
                      , p_1    in varchar2 default null
                      , p_2    in varchar2 default null
                      , p_3    in varchar2 default null
                      )
  return varchar2
is
  l_code varchar2(3);
  l_message varchar2(1000);
begin
  l_code := to_char(p_code, 'fm000');
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||l_code
                                   , 'N'
                                   , p_1
                                   , p_2
                                   , p_3
                                   , l_message
                                   );
  return l_message;
exception when others then
  return 'Err in get_msg for code: '||p_code||' //'
    ||dbms_utility.format_error_stack
    ||dbms_utility.format_error_backtrace
    ;
end;


--------------------------------------------------------------------------------
function get_error_msg(p_message in varchar2)
  return varchar2
is
  v_tmp_message varchar2(4000);
begin
  --custom error supposed to be in: p_message
  --but some procedures put it in: pricing_msgs.err_text instead
  if p_message is not null then
    v_tmp_message := substr(p_message, 1, 4000);

  elsif g_pricing_msgs_rec.err_text is not null then
    v_tmp_message := g_pricing_msgs_rec.err_text;

  else
    v_tmp_message := substr(
      dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      , 1, 4000
      )
      ;
  end if;

  if instr(v_tmp_message, 'ORA-') = 0 then
    v_tmp_message := substr(
      v_tmp_message
      ||chr(10)||dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      , 1, 4000
      )
      ;
  --else: error stack alreadu there...
  end if;

  return v_tmp_message;
end;

procedure concat_before( p_buf in out nocopy varchar2, p_new_str in varchar2)
is
begin
  if trim( p_new_str ) is null then
    return;
  end if;

  if p_buf is not null then
    p_buf := p_new_str || ' //' || p_buf;
  else
    p_buf := p_new_str;
  end if;
end;
procedure concat_after( p_buf in out nocopy varchar2, p_new_str in varchar2)
is
begin
  if trim( p_new_str ) is null then
    return;
  end if;

  if p_buf is not null then
    p_buf := p_buf || ' //' ||p_new_str;
  else
    p_buf := p_new_str;
  end if;
end;


procedure log_header
is
begin
  manage_prc_msgs_det.write_log(
    --mlz - 02jul2013
    --g_pricing_msgs_rec
    --, g_log_fh
    --, c_sep
    --, g_log_to_table
    g_lh
    --endmlz - 02jul2013
    --
    ,
    'NATION_M'               ||c_sep||
    'REASON'                 ||c_sep||
    'FRI_TICKER'             ||c_sep||
    'SOURCE_CODE'            ||c_sep||
    'PRICE_CURRENCY'         ||c_sep||
    'DELIVERY_DATE'          ||c_sep||
    'DATA_SUPPLIER_CODE'     ||c_sep||
    'PRICE_DATE'             ||c_sep||
    'PRICE'                  ||c_sep||
    'BID_DATE'               ||c_sep||
    'BID'                    ||c_sep||
    'ASK_DATE'               ||c_sep||
    'ASK'                    ||c_sep||
    'MID_DATE'               ||c_sep||
    'MID'                    ||c_sep
    ,'LOG'         --context
    ,'STATUS'      --status
    ,null          --line_nr
    );
end;

procedure log_price( p_sec_prc in security_prices%rowtype
                   , p_nation_m in sources_for_revolution.nation_code%type
                   , p_reason1 in varchar2
                   , p_reason2 in varchar2
                   , p_status in varchar2
                   )
is
  l_reason varchar2(4000);
begin
  l_reason := trim(p_reason1);
  if l_reason is not null
    and trim(p_reason2) is not null
  then
    l_reason := l_reason ||'// ';
  end if;
  if trim(p_reason2) is not null then
    l_reason := l_reason || p_reason2;
  end if;

  l_reason := replace( l_reason, chr(10), ' ');

  manage_prc_msgs_det.write_log(
    --mlz - 02jul2013
    --g_pricing_msgs_rec
    --, g_log_fh
    --, c_sep
    --, g_log_to_table
    g_lh
    --endmlz - 02jul2013
    --
    ,p_nation_m                       ||c_sep||
    l_reason                         ||c_sep||
    p_sec_prc.fri_ticker             ||c_sep||
    p_sec_prc.source_code            ||c_sep||
    p_sec_prc.price_currency         ||c_sep||
    p_sec_prc.delivery_date          ||c_sep||
    p_sec_prc.data_supplier_code     ||c_sep||
    p_sec_prc.price_date             ||c_sep||
    p_sec_prc.price                  ||c_sep||
    p_sec_prc.bid_date               ||c_sep||
    p_sec_prc.bid                    ||c_sep||
    p_sec_prc.ask_date               ||c_sep||
    p_sec_prc.ask                    ||c_sep||
    p_sec_prc.mid_date               ||c_sep||
    p_sec_prc.mid                    ||c_sep
    ,'LOG'         --context
    ,p_status      --status
    ,null          --line_nr
    );
end;

procedure log_reason( p_fri_ticker in tickers.fri_ticker%type
                    , p_nation_m in sources_for_revolution.nation_code%type
                    , p_reason in varchar2
                    , p_status in varchar2
                    )
is
  c_sep varchar2(1) := chr(9);
begin
  manage_prc_msgs_det.write_log(
    --mlz - 02jul2013
    --g_pricing_msgs_rec
    --, g_log_fh
    --, c_sep
    --, g_log_to_table
    g_lh
    --endmlz - 02jul2013
    --
    ,p_nation_m                       ||c_sep||
    replace( p_reason, chr(10), ' ') ||c_sep||
    p_fri_ticker                     ||c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep||
    c_sep
    ,'LOG'         --context
    ,p_status      --status
    ,null          --line_nr
    );
end;
procedure log_step( p_msg in varchar2, p_to_both in boolean := false )
is
  l_tmp_success boolean;
begin
  load_supplier_Rules.Write_file( g_Log_fh
                                , to_char(sysdate, 'yyyy/mm/dd hh24:mi:ss')
                                  ||' - '|| p_msg
                                , g_Pricing_Msgs_Rec
                                , l_tmp_success
                                );
  IF (NOT l_tmp_success) THEN
    raise_fatal_error;
  END IF;

  --detailed and main log are mixed...
  --
  --if g_log_all or g_log_counts then
  --  if p_to_both then
  --    manage_prc_msgs_det.write_log(
  --      g_lh
  --      --
  --      , p_msg
  --      , 'LOG_STEP'     --context
  --      , null           --status
  --      , null           --line_nr
  --      );
  --  end if;
  --end if;
end;



--mlz - 29may2014
--------------------------------------------------------------------------------
procedure log_close
is
begin
  if utl_file.is_open(g_log_file) then
    utl_file.fclose(g_Log_File);
  end if;
exception when others then null;
end;
--endmlz - 29may2014



--mlz - 17mar2014
function get_oasis_msg_gen( p_code in number
                          , p_1    in varchar2 default null
                          , p_2    in varchar2 default null
                          , p_3    in varchar2 default null
                          )
  return varchar2
is
  l_code varchar2(3);
  l_message varchar2(1000);
begin
  l_code := to_char(p_code, 'fm000');
  manage_messages.get_oasis_message( c_oasis_message_gen||l_code
                                   , 'N'
                                   , p_1
                                   , p_2
                                   , p_3
                                   , l_message
                                   );
  return l_message;
exception when others then
  return 'Err in get_msg for code: '||p_code||' //'
    ||dbms_utility.format_error_stack
    ||dbms_utility.format_error_backtrace
    ;
end;
--endmlz - 17mar2014

--mlz - 17mar2014
procedure logmon_open(p_errmsg out nocopy varchar2)
is
  --mlz - 29may2014 - moved global
  --l_filename varchar2(100);
  --endmlz - 29may2014
  
  l_exi boolean;
  l_len number;
  l_blk binary_integer;

  --level 1 ....................................................................  
  function translate_file_name (
      p_file_name            in  varchar2,
      p_sitedate_char_string in  varchar2,
      p_date_value           in  date,
      p_sysdate_char_string  in varchar2
    )
    return varchar2
  as
    l_tmp varchar2(1000);
  begin
    l_tmp := manage_file.translate_file_name(P_File_Name,P_SiteDate_Char_String,P_Date_Value,P_Sysdate_Char_String);

    --added some custom replacements
    l_tmp := replace(l_tmp, P_SiteDate_Char_String||'PGMID'||P_SiteDate_Char_String, g_program_id);
    --l_tmp := replace(l_tmp, P_SiteDate_Char_String||'COUNTRY_ID'||P_SiteDate_Char_String, g_country_id);
    --l_tmp := replace(l_tmp, P_SiteDate_Char_String||'FILE_TYPE'||P_SiteDate_Char_String, g_file_type);

    return l_tmp;
  end;
  --level 1 ....................................................................
  procedure get_parm_logmon
  is
    l_parm ops_scripts_parms.parm_name%type := 'LOGMON';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);
    if l_tmp is null then
      l_tmp := 'logmon-%%%PGMID%%%_%%%YYYY.MM.DD%%%.log';
      p_errmsg := get_oasis_msg_gen(210, l_parm); --Parameter [%%%%%] not found (ops_scripts/_parms)
      log_step(p_errmsg);
      raise_fatal_error;
    end if;

    --mlz - 29may2014
    --l_filename := translate_file_name(l_tmp, '%%%', g_load_date, null);
    g_logmon_filename := translate_file_name(l_tmp, '%%%', g_load_date, null);
    --log_step('['||l_parm||'] is ['||l_filename||']');
    log_step('['||l_parm||'] is ['||g_logmon_filename||']');
    --endmlz - 29may2014
  end;
--level 0 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
begin

  get_parm_logmon;
  --mlz - 29may2014
  --utl_file.fgetattr(c_oradir, l_filename, l_exi, l_len, l_blk);
  utl_file.fgetattr(c_oradir, g_logmon_filename, l_exi, l_len, l_blk);
  --endmlz - 29may2014

  --mlz - 29may2014
  --g_logmon := utl_file.fopen(c_oradir, l_filename, 'A', 32767);
  g_logmon := utl_file.fopen(c_oradir, g_logmon_filename, 'A', 32767);
  --endmlz - 29may2014
  
  if l_exi = false then
    utl_file.put_line(g_logmon, 
      'date' ||c_logmon_sep||
      'msg' ||c_logmon_sep||
      'fri_ticker' ||c_logmon_sep||
      'source' ||c_logmon_sep||
      'currency' ||c_logmon_sep||
      'deliv_date' ||c_logmon_sep||
      'supplier'
      );
  end if;
    
  --mlz - 29may2014
  g_cnt_logmon := 0;
  --endmlz - 29may2014

end;

procedure logmon_write( 
  p_message in varchar2, 
  p_frit in security_prices.fri_ticker%type,
  p_src in security_prices.source_code%type,
  p_curr in security_prices.price_currency%type,
  p_deld in security_prices.delivery_date%type,
  p_suppl in security_prices.data_supplier_code%type
  )
is
  l_prefix varchar2(30) := to_char(sysdate, 'yyyy/mm/dd hh24:mi:ss');
begin
  if utl_file.is_open(g_logmon) then
    utl_file.put_line(g_logmon, 
      l_prefix
      ||c_logmon_sep||replace(p_message, chr(10), ' ')
      ||c_logmon_sep||p_frit
      ||c_logmon_sep||p_src
      ||c_logmon_sep||p_curr
      ||c_logmon_sep||to_char(p_deld, 'yyyy/mm/dd hh24:mi:ss')
      ||c_logmon_sep||p_suppl
      );
  end if;

  --mlz - 29may2014
  g_cnt_logmon := g_cnt_logmon + 1;
  --endmlz - 29may2014
end;

procedure logmon_write( p_message in varchar2)
is
begin
  logmon_write(p_message, null, null, null, null, null);
end;

procedure logmon_close
is
  --mlz - 29may2014
  l_tmp_success boolean;
  l_tmp_message varchar2(1000);
  --endmlz - 29may2014
  --mlz - 01jun2014
  l_email_pgmid number := scripts.get_arg_value( g_program_id, 'LOGMON_EMAIL_PGM');
  --endmlz - 01jun2014  
begin
  if utl_file.is_open(g_logmon) then
    utl_file.fclose(g_logmon);
  end if;
  --mlz - 29may2014
  if g_cnt_logmon > 0 then
    --mlz - 01jun2014
    --tms_utilities.send_tms_email(328, g_logmon_filename, c_oradir, l_tmp_success, l_tmp_message);
    tms_utilities.send_tms_email(l_email_pgmid, g_logmon_filename, c_oradir, l_tmp_success, l_tmp_message);
    --endmlz - 01jun2014
  end if;
  --endmlz - 29may2014
end;
--endmlz - 17mar2014

--------------------------------------------------------------------------------
function is_active_on_nation_m(
  p_fri_ticker in securities.fri_ticker%type
  , p_nation_m in sources_for_revolution.nation_code%type
  , p_date     in date
  )
  return boolean
is
--  cursor cur_sec is
--    select ts.active_flag, s.date_of_status
--    from securities     s
--       , trading_status ts
--    where s.fri_ticker        = p_fri_ticker
--    and s.trading_status_code = ts.code
--    ;
--  r_sec  cur_sec%rowtype;

  --
  -- if active on 1 of the sources of the nation_m, whatever the currency
  -- >> considered as active
  -- (official_prices.get_ont_price_multi will retrieve a price for all available currencies)
  --
  -- actually, this is only used to avoid unuseful queries on security_prices
  --
  cursor cur_t_st is
    --mlz - 15nov2015
    --select ts.active_flag
    select /*+ index( sts security_trading_status_x_01 ) */
      ts.active_flag
    --endmlz - 15nov2015
    from sources_for_revolution sfr
       , security_trading_status sts
       , trading_status ts
    where sfr.nation_code = p_nation_m
    and sfr.source_code = sts.source_code
    and sts.trading_status_code = ts.code
    and sts.date_of_status =
      --mlz - 15nov2015
      --(select max(sts2.date_of_status)
      (select /*+ index( sts2 security_trading_status_x_01 ) */ 
        max(sts2.date_of_status)
      --endmlz - 15nov2015
      from security_trading_status sts2
      where sts2.fri_ticker    = sts.fri_ticker
      and sts2.source_code     = sts.source_code
      and sts2.price_currency  = sts.price_currency
--    and sts2.date_of_status <= to_date('2011-09-01', 'yyyy/mm/dd')
      and sts2.date_of_status <= p_date
      )
    and sts.fri_ticker = p_fri_ticker
    and ts.active_flag = 'Y'
    ;
  r_t_st  cur_t_st%rowtype;
begin
  --
  -- 1. check against security flag
  --
--  open cur_sec;
--  fetch cur_sec into r_sec;
--  close cur_sec;
--
--  if r_sec.date_of_status <= p_date
--    and r_sec.active_flag = 'N'
--  then
--    return false;
--  end if;


  --
  -- 2. look for sec_t_st
  --
  open cur_t_st;
  fetch cur_t_st into r_t_st;
  if cur_t_st%found then
    close cur_t_st;

    --if r_t_st.active_flag = 'Y' then
      return true;
    --end if;
    --return false;
  end if;
  close cur_t_st;

  return false;
end;

function is_active_on_nation_m2(
  p_fri_ticker in securities.fri_ticker%type
  , p_nation_m in sources_for_revolution.nation_code%type
  , p_date     in date
  )
  return varchar2
is
begin
  if is_active_on_nation_m( p_fri_ticker, p_nation_m, p_date) = true then
    return 'Y';
  else
    return 'N';
  end if;
end;



--
-----------------------------------------------------------------------
-- To fetch the record from the table 'Pricing_Calc_Schedule'
-----------------------------------------------------------------------
--
--PROCEDURE Get_Ticker_Valuation
--        ( P_Fri_Ticker IN     Securities.Fri_Ticker%TYPE
--        , P_Load_Date  IN     DATE
--        , p_nation_m   in     nations_for_revolution.code%type
--        , P_Prc_Msgs   IN OUT NOCOPY Pricing_Msgs%ROWTYPE
--        , P_Success       OUT BOOLEAN
--        , P_Price_Rec     OUT Security_Prices%ROWTYPE
--        , P_Price_Rec_Adj OUT Security_Prices%ROWTYPE
--        ) IS --{
----
--V_Source     Sources.Code%TYPE;
--V_Source_Adj Sources.Code%TYPE;
--V_Currency   Currencies.Currency%TYPE;
----
--V_Active_Flag_O Yes_No.Code%TYPE;
--V_Active_Flag_P Yes_No.Code%TYPE;
----
--CURSOR Get_Security_Desc
--     ( I_Fri_Ticker Securities.Fri_Ticker%TYPE
--     ) IS
--SELECT *
--  FROM Securities
-- WHERE Fri_Ticker = I_Fri_Ticker;
----
--R_Securities Securities%ROWTYPE;
--
--  v_prices_tab  official_prices.sec_prc_t;
--
--BEGIN --{
--  Pgmloc := 1180;
--  P_Success := TRUE;
--  Pgmloc := 1190;
--
--  P_Price_Rec := null;
--  Pgmloc := 1200;
--
--  P_Price_Rec_Adj := null;
--  Pgmloc := 1210;
--
--  OPEN  Get_Security_Desc( P_Fri_Ticker );
--  Pgmloc := 1220;
--  R_Securities.Fri_Ticker := NULL;
--  FETCH Get_Security_Desc INTO R_Securities;
--  Pgmloc := 1230;
--  CLOSE Get_Security_Desc;
--
--  IF R_Securities.Fri_Ticker IS NULL
--  THEN
--    -- Securities does not exists
--    GOTO Get_Out;
--  END IF;
--  --
--  V_Source     := NULL;
--  V_Source_Adj := NULL;
--  V_Currency   := NULL;
--  --
--  Pgmloc := 1250;
----  official_prices.Get_Primaries(  P_Load_Date
----                                , R_Securities
----                                , V_Currency
----                                , V_Source
----                                , P_Prc_Msgs
----                                , P_Success
----                                );
----  IF NOT P_Success THEN GOTO Get_Out; END IF;
--  --
----  Pgmloc := 1260;
----  IF V_Source   IS NULL
----  OR V_Currency IS NULL
----  THEN
----    GOTO Get_Out;
----  END IF;
--  --
--  Pgmloc := 1270;
--
--  -- Get the adjusted source, if exists
----  SELECT Abs_Adjust_Source_Code
----    INTO V_Source_Adj
----    FROM SOURCES
----   WHERE Code = V_Source;
----  Pgmloc := 1280;
--
--  --V_Source := Official_Prices.Source_Synonym( V_Source );
--
--
--  --
--  -- Obtenir le prix selon les priorites
--  --
--  Pgmloc := 1290;
--  official_prices.Get_One_Price_multi( P_Load_Date
--                                     , R_Securities.Fri_Ticker
--                                     , p_nation_m
--                                     , v_prices_tab
--                                     , P_Prc_Msgs
--                                     , P_Success
--                                     );
--  IF NOT P_Success THEN GOTO Get_Out; END IF;
--
----  IF P_Price_Rec.Source_Code IS NOT NULL
----  THEN
----
----
----
------ Garder la source, sinon il y a risque de duplicat
----    P_Price_Rec.Data_Supplier_Code := DSS_Rec.Data_Supplier_Code;
----    P_Price_Rec.Delivery_Date
----      := TO_DATE( TO_CHAR( P_Price_Rec.Delivery_Date
----                         , 'DD-MON-YYYY'
----                         )
----               || G_Delivery_Time
----                , 'DD-MON-YYYYHH24MI'
----                );
----    IF Official_Prices.Is_Source_a_Synonym( P_Price_Rec.Source_Code ) = 'Y'
----    THEN
----      Pgmloc := 1300;
----      -- Get the source that is already present for Statpro
----      BEGIN
----        Pgmloc := 1310;
----        V_Source := NULL;
----        SELECT MIN(SP.SOURCE_CODE)
----          INTO V_Source
----          FROM Security_Prices SP
----         WHERE SP.Data_Supplier_Code = P_Price_Rec.Data_Supplier_Code
----           AND SP.Delivery_Date      = P_Price_Rec.Delivery_Date
----           AND SP.Fri_Ticker         = P_Price_Rec.Fri_Ticker
----           AND Official_Prices.Source_Synonym( SP.Source_Code )
----               = P_Price_Rec.Source_Code;
----        IF V_Source = P_Price_Rec.Source_Code
----        THEN --{
----          NULL;
----        ELSE --}{
----          -- Not same source, Is the one that we have is inactive
----          -- and the other is active?
----          V_Active_Flag_O := 'N';
----          V_Active_Flag_P := 'N';
----          Pgmloc := 1320;
----          SELECT TS.Active_Flag
----            INTO V_Active_Flag_O
----            FROM Security_Trading_Status STS
----               , Trading_Status           TS
----           WHERE STS.Fri_Ticker     = P_Price_Rec.Fri_Ticker
----             AND STS.Source_Code    = V_Source
----             AND STS.Price_Currency = P_Price_Rec.Price_Currency
----             AND STS.Date_Of_Status = (
----                 SELECT MAX( STS1.Date_Of_Status )
----                   FROM Security_Trading_Status STS1
----                  WHERE STS1.Fri_Ticker      = STS.Fri_Ticker
----                    AND STS1.Source_Code     = STS.Source_Code
----                    AND STS1.Price_Currency  = STS.Price_Currency
----                    AND STS1.Date_of_Status <= P_Load_Date
----                 )
----             AND TS.Code = STS.Trading_Status_Code
----             ;
----          IF V_Active_Flag_O = 'Y'
----          THEN --{
----            -- The source we already have is active, keep it!
----            P_Price_Rec.Source_Code        := V_Source;
----          ELSE --}{
----            -- The source in STP price is inactive
----            -- Is the other one active?
----            Pgmloc := 1330;
----            SELECT TS.Active_Flag
----              INTO V_Active_Flag_P
----              FROM Security_Trading_Status STS
----                 , Trading_Status           TS
----             WHERE STS.Fri_Ticker     = P_Price_Rec.Fri_Ticker
----               AND STS.Source_Code    = P_Price_Rec.Source_Code
----               AND STS.Price_Currency = P_Price_Rec.Price_Currency
----               AND STS.Date_Of_Status = (
----                   SELECT MAX( STS1.Date_Of_Status )
----                     FROM Security_Trading_Status STS1
----                    WHERE STS1.Fri_Ticker      = STS.Fri_Ticker
----                      AND STS1.Source_Code     = STS.Source_Code
----                      AND STS1.Price_Currency  = STS.Price_Currency
----                      AND STS1.Date_of_Status <= P_Load_Date
----                   )
----               AND TS.Code = STS.Trading_Status_Code
----               ;
----            IF V_Active_Flag_P = 'Y'
----            THEN
----              NULL; -- we have to delete the one in STP.
----            ELSE
----              -- Both are inactive - keep the one we already have in STP.
----              P_Price_Rec.Source_Code        := V_Source;
----            END IF;
----          END IF; --}
----        END IF; --}
----      EXCEPTION
----      WHEN NO_DATA_FOUND THEN
----        NULL;
----      END;
----    END IF;
----  END IF;
----  --
----  IF V_Source_Adj IS NOT NULL
----  THEN
----    Pgmloc := 1340;
----    official_prices.Get_One_Price( P_Load_Date
----                 , R_Securities.Fri_Ticker
----                 , V_Source_Adj
----                 , V_Currency
----                 , P_Price_Rec_Adj
----                 , P_Prc_Msgs
----                 , P_Success
----                 );
----    IF NOT P_Success THEN GOTO Get_Out; END IF;
----    IF P_Price_Rec_Adj.Source_Code IS NOT NULL
----    THEN
----      P_Price_Rec_Adj.Data_Supplier_Code := DSS_Rec.Data_Supplier_Code;
----      P_Price_Rec_Adj.Delivery_Date
----        := TO_DATE( TO_CHAR( P_Price_Rec_Adj.Delivery_Date
----                           , 'DD-MON-YYYY'
----                           )
----                 || G_Delivery_Time
----                  , 'DD-MON-YYYYHH24MI'
----                  );
----    END IF;
----  END IF;
--  --
--<< Get_Out >>
--  NULL;
--  --
--END Get_Ticker_Valuation; --}}
--
--

---------------------------------------------------------------------------------
PROCEDURE Get_Data_Line(
          p_File_Handle    IN OUT NOCOPY Utl_File.File_Type
        , p_File_Date      IN     DATE
        , DSS_Rec          IN OUT NOCOPY Data_Supply_Schedule%ROWTYPE
--        , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_prices_tab     IN OUT NOCOPY official_prices.sec_prc_t
        , P_prices_tab_ori IN OUT NOCOPY official_prices.sec_prc_t
        , p_reason_tab     in out nocopy reason_t
        , p_nation_m          OUT NOCOPY sources_for_revolution.nation_code%type
        , p_Record_Count      OUT NUMBER
        , p_Success           OUT BOOLEAN
        , p_End_of_File       OUT BOOLEAN
        ) IS
--
  V_Symbol              Tickers.Ticker%TYPE;
--  R_Security_Prices     Security_Prices%ROWTYPE;
--V_Previous_Date       DATE;
--
--  V_Currency                   Currencies.Currency%TYPE;
  V_Source                     Sources.Code%TYPE;
--  V_Source_Adj                 Sources.Code%TYPE;
  i    binary_integer;
  v_tmp_message                varchar2(4000);
  b_active_on_nation_m         boolean;
--
-- Ajout - JSL - 24 octobre 2014
  Orig_Data_Supplier_Code      Data_Suppliers.Code%TYPE;
  Orig_Delivery_Time           VARCHAR2(4);
-- Fin d'ajout - JSL - 24 octobre 2014
--
BEGIN
  --{
  pgmloc := 1350;
  p_Success := false;
  p_End_of_File    := FALSE;
  --
  DSS_Rec.Identification_System_Code := C_FRI_ID;
  --
  WHILE NOT p_End_of_File
  LOOP --{
    --
    --------------------------------------------
    --  Fetch the Pricing Calc Schedule Security
    --------------------------------------------
    IF NOT cur_securities%ISOPEN
    THEN
      Pgmloc := 1380;
      OPEN cur_securities( p_File_Date );
    END IF;


--    IF G_Adj_Treatment = C_Adjusted
--    THEN --{
--      G_Adj_Treatment := C_Unadjusted;

    -- set 1 elem for 1st tests
    -- (will be adjusted/enlarged after prices are found)
    --
    p_reason_tab := reason_t();
    p_reason_tab.extend( 1 );

    Pgmloc := 1390;
    FETCH cur_securities INTO GR_Securities;
    IF cur_securities%NOTFOUND
    THEN
      Pgmloc := 1400;
      p_End_of_File := TRUE;
      p_Success     := TRUE;
      CLOSE cur_securities;
      GOTO GET_OUT;
    END IF;


    g_Pricing_Msgs_Rec.Value_01 := NVL( g_Pricing_Msgs_Rec.Value_01, 0 ) + 1;

    -- Security not active at price date
    Pgmloc := 1410;
    IF  GR_Securities.Trading_Status_Code != 'A'
    AND GR_Securities.Date_of_Status      <= p_File_Date
    THEN
--        G_Adj_Treatment := C_Adjusted;
      p_reason_tab(1).status := 'SKIP_INA';
      p_reason_tab(1).text := 'Security is inactive';
      GOTO Get_Next;
    END IF;


    Pgmloc := 1410;
    b_active_on_nation_m :=
      is_active_on_nation_m( gr_securities.fri_ticker
                            , gr_securities.nation_m
                            , p_File_Date
                            )
      ;
    if b_active_on_nation_m = false
    THEN
--        G_Adj_Treatment := C_Adjusted;
      p_reason_tab(1).status := 'SKIP_INA';
      p_reason_tab(1).text := 'Security is inactive on nation_m: '||gr_securities.nation_m;
      GOTO Get_Next;
    END IF;


    --mlz - jan2011
    if official_prices.process_security( DSS_Rec.Data_Supplier_Code
                                       , g_delivery_time
                                       , GR_Securities.security_type_code
                                       )
                       = false
    then
--        G_Adj_Treatment := C_Adjusted;
      p_reason_tab(1).status := 'SKIP_NOTP';
      p_reason_tab(1).text := 'Security not processed';
      GOTO Get_Next; ------>>>>
    END IF;
    --endmlz - jan2011

      Pgmloc := 1460;
--      Get_Ticker_Valuation( GR_Securities.Fri_Ticker
--                          , p_File_Date
--                          , gr_securities.nation_m
--                          , g_Pricing_Msgs_Rec
--                          , p_Success
--                          , GR_Security_Prices
--                          , GR_Security_Prices_Adj
--                          );
--      IF NOT p_Success THEN GOTO Get_Out; END IF;
      --
    --out param
    p_nation_m := gr_securities.nation_m;

    p_prices_tab     := official_prices.sec_prc_t();

    official_prices.Get_ds_Prices(  p_File_Date
                                  , GR_Securities.Fri_Ticker
                                  --mlz - 12jul2013
                                  , GR_Securities.security_type_code
                                  --endmlz - 12jul2013
                                  , gr_securities.nation_m
                                  , p_prices_tab
                                  , g_Pricing_Msgs_Rec
                                  , P_Success
                                  );
    --mlz - 17may2014
    --IF NOT P_Success THEN GOTO Get_Out; END IF;
    if not p_success then
      p_reason_tab(1).status := 'SKIP_ERR';
      p_reason_tab(1).text := g_Pricing_Msgs_Rec.Err_Text;
      --reset error
      g_Pricing_Msgs_Rec.Err_Text := null;
      GOTO Get_Next; ------>>>>
    end if;
    --endmlz - 17may2014

    if p_prices_tab.count = 0 then
      p_reason_tab(1).status := 'SKIP_NOPRC';
      p_reason_tab(1).text := 'No price to retrieve';
      GOTO Get_Next; ------>>>>
    END IF;

    --
    -- keep a copy to log precisely what price was chosen and why
    p_prices_tab_ori := p_prices_tab;

    -- separate tab for reason text... must match ori tab
    -- >> discard 1st init and get a new size
    p_reason_tab := reason_t();
    p_reason_tab.extend( p_prices_tab_ori.count );

    for i in nvl(p_prices_tab.first, 1)..nvl(p_prices_tab.last, 0)
    loop
      exit when i > p_prices_tab.last;

--        V_Currency   := p_prices_tab(i).Price_Currency;
      V_Source     := p_prices_tab(i).Source_Code;
--      V_Source_Adj := GR_Security_Prices_Adj.Source_Code;
      --
      Pgmloc := 1470;
      IF p_prices_tab(i).Price_Currency IS NULL
      OR p_prices_tab(i).Source_Code    IS NULL
      THEN
--        G_Adj_Treatment := C_Adjusted;
        --GOTO Get_Next;
        p_reason_tab(i).status := 'SKIP_ERR';
        p_reason_tab(i).text := 'price_currency or source_code null';
        p_prices_tab.delete(i);
        continue;
      END IF;
      --
      -- pas de prix courant, laisser faire le carry forward! Get next!
      --V_Previous_Date := File_Date;
      IF p_prices_tab(i).Price_Date = p_File_Date
      OR p_prices_tab(i).Bid_Date   = p_File_Date
      OR p_prices_tab(i).Ask_Date   = p_File_Date
      OR p_prices_tab(i).Mid_Date   = p_File_Date
      THEN --{
        -- Have a current price
        NULL;
      ELSE --}{
        -- Let the carry forward in.
        -- we should not have an adjusted price without unadjusted.
--        G_Adj_Treatment := C_Adjusted;
        --GOTO Get_Next;

        p_reason_tab(i).status := 'SKIP_CFW';
        p_reason_tab(i).text := 'Price not of today, CFW brought it --> skip';
        p_prices_tab.delete(i);
        continue;
      END IF; --}
      --





--    ELSE --}{
--      G_Adj_Treatment := C_Adjusted;
--      V_Source := V_Source_Adj;
--    END IF; --}





      Pgmloc := 1550;
      IF p_prices_tab(i).Source_Code    IS NULL
      OR p_prices_tab(i).Price_Currency IS NULL
      THEN
  --      G_Adj_Treatment := C_Adjusted;
        --GOTO Get_Next;

          p_reason_tab(i).status := 'SKIP_ERR';
          p_reason_tab(i).text := 'price_currency or source_code null';
          p_prices_tab.delete(i);
          continue;
      END IF;


      --
      -->>>> Nous pouvons avoir des prix meme si la source est dite fermee!
      --
      -- Si fete -> laisser faire le carry forward! Get next!
      --IF Holiday.Is_It_Open( p_File_Date, 'SOURCE', p_prices_tab(i).Source_Code ) IS NOT NULL
      --THEN
      --  G_Adj_Treatment := C_Adjusted;
        --GOTO Get_Next;
      --
      --  p_reason_tab(i).status := 'SKIP_HOLYD';
      --  p_reason_tab(i).text := 'date is a holiday';
      --  p_prices_tab.delete(i);
      --  continue;
      --END IF;


      --
      -- Obtenir le prix selon les priorites
      --
--    Pgmloc := 1560;
--    IF G_Adj_Treatment = C_Adjusted
--    THEN
--      R_Security_Prices := GR_Security_Prices_Adj;
--    ELSE
--      R_Security_Prices := GR_Security_Prices;
--    END IF;


      Pgmloc := 1570;
      IF p_prices_tab(i).Fri_Ticker IS NULL
      THEN
        --GOTO Get_Next;

          p_reason_tab(i).status := 'SKIP_TNULL';
          p_reason_tab(i).text := 'fri_ticker null';
          p_prices_tab.delete(i);
          continue;
      END IF;
      --

-- Ajout - JSL - 24 octobre 2014
      Orig_Data_Supplier_Code := p_prices_tab(i).Data_Supplier_Code;
      Orig_Delivery_Time      := TO_CHAR( p_prices_tab(i).Delivery_Date, 'HH24MI' );
-- Fin d'ajout - JSL - 24 octobre 2014

      p_prices_tab(i).Data_Supplier_Code := DSS_Rec.Data_Supplier_Code;
      p_prices_tab(i).Delivery_Date
        := TO_DATE( TO_CHAR( p_File_Date, 'DD-MON-YYYY' )
                 || DSS_Rec.Delivery_Time
                  , 'DD-MON-YYYYHH24MI'
                  );
      p_prices_tab(i).Price_Origin_Code   := 'L';
-- Modification - JSL - 08 mars 2012
-- Laisser faire le database trigger pour la date bc.
--    p_prices_tab(i).Delivery_Date_BC
--      := TO_CHAR( 999999999999
--                - TO_NUMBER( TO_CHAR( p_prices_tab(i).Delivery_Date
--                                    , 'YYYYMMDDHH24MI' ) ) );
-- Modification - JSL - 08 mars 2012
      p_prices_tab(i).Last_User          := g_user; --Constants.Get_User_Id;
      p_prices_tab(i).Last_Stamp         := SYSDATE;
      --


      IF p_prices_tab(i).Price_Date = p_File_Date
      THEN
        NULL;
      ELSE
        p_prices_tab(i).Price_Date            := NULL;
        p_prices_tab(i).Price                 := NULL;
        p_prices_tab(i).Volume                := NULL;
        p_prices_tab(i).High                  := NULL;
        p_prices_tab(i).Low                   := NULL;
        p_prices_tab(i).Open_Price            := NULL;
        p_prices_tab(i).Yield                 := NULL;
        p_prices_tab(i).Dirty_Price           := NULL;
        p_prices_tab(i).Price_Mod_Duration    := NULL;
        p_prices_tab(i).Board_Lot_Flag        := NULL;
        p_prices_tab(i).Last_Trade_Time       := NULL;
        p_prices_tab(i).Number_Of_Trans       := NULL;
        p_prices_tab(i).Accrued_Interest      := NULL;
        p_prices_tab(i).Term_In_Years         := NULL;
        p_prices_tab(i).Settlement_Date       := NULL;
        p_prices_tab(i).Maturity_Date         := NULL;
        p_prices_tab(i).Maturity_Type         := NULL;
        p_prices_tab(i).Index_Ratio           := NULL;
        p_prices_tab(i).Rpb_Factor            := NULL;
        p_prices_tab(i).Effective_Date_Factor := NULL;
        p_prices_tab(i).Next_Chg_Date_Factor  := NULL;
        p_prices_tab(i).Benchmark_Code        := NULL;
        p_prices_tab(i).Spread                := NULL;
        p_prices_tab(i).Spread_From_Generic   := NULL;
        p_prices_tab(i).Spread_From_Libor     := NULL;
        p_prices_tab(i).Convexity             := NULL;
        p_prices_tab(i).Pvbp                  := NULL;
        p_prices_tab(i).Average_Life_Date     := NULL;
        p_prices_tab(i).Market_Cap            := NULL;
        p_prices_tab(i).IFRS_Code             := NULL;
      END IF;
      --
      IF p_prices_tab(i).Bid_Date = p_File_Date
      THEN
        NULL;
      ELSE
        p_prices_tab(i).Bid_Date         := NULL;
        p_prices_tab(i).Bid              := NULL;
        p_prices_tab(i).Bid_Yield        := NULL;
        p_prices_tab(i).Bid_Dirty_Price  := NULL;
        p_prices_tab(i).Bid_Mod_Duration := NULL;
      END IF;
      --
      IF p_prices_tab(i).Ask_Date = p_File_Date
      THEN
        NULL;
      ELSE
        p_prices_tab(i).Ask_Date         := NULL;
        p_prices_tab(i).Ask              := NULL;
        p_prices_tab(i).Ask_Yield        := NULL;
        p_prices_tab(i).Ask_Dirty_Price  := NULL;
        p_prices_tab(i).Ask_Mod_Duration := NULL;
      END IF;
      --
      IF p_prices_tab(i).Mid_Date = p_File_Date
      THEN
        NULL;
      ELSIF G_Mid_From_Sources.EXISTS( p_prices_tab(i).Source_Code )
      THEN
        p_prices_tab(i).Mid_Date         := NULL;
        p_prices_tab(i).Mid              := NULL;
        p_prices_tab(i).Mid_Yield        := NULL;
        p_prices_tab(i).Mid_Dirty_Price  := NULL;
        p_prices_tab(i).Mid_Mod_Duration := NULL;
      ELSIF p_prices_tab(i).Bid_Date = p_File_Date
        AND p_prices_tab(i).Ask_Date = p_File_Date
      THEN
        p_prices_tab(i).Mid_Date := p_File_Date;
        p_prices_tab(i).Mid      := ( p_prices_tab(i).Bid
                                      + p_prices_tab(i).Ask
                                      ) / 2.0 ;
      ELSE
        p_prices_tab(i).Mid_Date         := NULL;
        p_prices_tab(i).Mid              := NULL;
        p_prices_tab(i).Mid_Yield        := NULL;
        p_prices_tab(i).Mid_Dirty_Price  := NULL;
        p_prices_tab(i).Mid_Mod_Duration := NULL;
      END IF;
      --
      --


      IF p_prices_tab(i).Price_Date IS NOT NULL
      OR p_prices_tab(i).Bid_Date   IS NOT NULL
      OR p_prices_tab(i).Ask_Date   IS NOT NULL
      OR p_prices_tab(i).Mid_Date   IS NOT NULL
      THEN
        --P_SP := R_Security__Prices;
        --p_Success := TRUE;
        --GOTO Get_Out;
        p_reason_tab(i).status := '';
        p_reason_tab(i).text := '';

-- Ajout - JSL - 24 octobre 2014
-- Keep track of prices if from some restricted supplier
        if g_track_suppliers.count > 0 then
          if g_track_suppliers.exists( Orig_Data_Supplier_Code )
          then
            manage_Prices_Available.Insert_Record( DSS_Rec.Data_Supplier_Code
                                                 , DSS_Rec.Delivery_Time
                                                 , P_File_Date
                                                 , Orig_Data_Supplier_Code
                                                 , Orig_Delivery_Time
                                                 , p_prices_tab(i).Fri_Ticker
                                                 , p_nation_m
                                                 , p_prices_tab(i).Source_Code
                                                 , p_prices_tab(i).Price_Currency
                                                 , p_prices_tab(i).Price_Date
                                                 , p_prices_tab(i).Price
                                                 , p_prices_tab(i).Bid_Date
                                                 , p_prices_tab(i).Bid
                                                 , P_Success
                                                 , V_Tmp_Message
                                                 );
            if not p_success then raise_fatal_error; end if;
          end if;
        end if;
-- Fin d'ajout - JSL - 24 octobre 2014

      END IF;
    end loop;




    if p_prices_tab.count > 0 then
      --tab is not empty --> there is some prices to process
      --> exit read loop
      GOTO Get_Out;
    end if;


    --
<< Get_Next >>
    --
    IF MOD( g_Pricing_Msgs_Rec.Value_01, 250) = 0
    THEN
      Load_Supplier_Rules.Update_Prc_Msgs( g_Pricing_Msgs_Rec.Program_Id
                                         , g_Pricing_Msgs_Rec.Date_of_Msg
                                         , g_Pricing_Msgs_Rec
                                         , p_Success
                                         );
    END IF;
    --
    --check if no prices were retrieved, if there is a reason to log
    if p_reason_tab.count > 0 then
      if g_log_all then
        log_reason( GR_Securities.Fri_Ticker
                  , GR_Securities.nation_m
                  , p_reason_tab(1).text
                  , p_reason_tab(1).status
                  );
      end if;
    end if;
  END LOOP; --}




--
<<GET_OUT>>
  IF NOT p_Success
  THEN
    p_End_of_File := TRUE;
  END IF;
  NULL;
--
EXCEPTION
  WHEN OTHERS
  THEN
    v_tmp_message := get_error_msg(v_tmp_message);
    log_step(v_tmp_message);
    logmon_write(v_tmp_message
      , GR_Security_Prices.Fri_Ticker
      , GR_Security_Prices.source_code
      , GR_Security_Prices.price_currency
      , to_char( GR_Security_Prices.delivery_date, 'yyyy/mm/dd hh24:mi:ss')
      , g_Data_Supplier_Code
      );

    IF cur_securities%ISOPEN
    THEN
      CLOSE cur_securities;
      p_End_of_File := TRUE;
    END IF;
    --
    v_tmp_message := '@'||pgmloc
      ||' (frit:'||GR_Securities.Fri_Ticker
      ||' /nat_m:'||GR_Securities.nation_m
      ||') '||v_tmp_message
      ;
    g_Pricing_Msgs_Rec.Err_Text := substr(v_tmp_message,1,255);

--    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'100'
--                                     , 'N'
--                                     , TO_CHAR( SQLCODE )
--                                     , 'Get_Data_Line At Pgmloc'
--                                       ||TO_CHAR( Pgmloc )
--                                       || ' '''
--                                       || GR_Securities.nation_m
--                                       --|| p_prices_tab(i).Source_Code
--                                       || ''' '
--                                       || GR_Securities.Fri_Ticker
--                                     , g_Pricing_Msgs_Rec.Err_Text
--                                     );
  p_success := false;
  --}
END;
--
--
---------------------------------------------------------------------
--  Procedure To Validate The Data Record If Reject Or Not
----------------------------------------------------------------------
--
PROCEDURE Validate_Record
        ( P_Handle        IN     Utl_File.File_Type
        , DSS             IN     Data_Supply_Schedule%ROWTYPE
        , IDS             IN     Identification_Systems%ROWTYPE
        , P_Date_Fmt      IN     VARCHAR2
        , P_Curr_Ct       IN     NUMBER
        , P_Src_Ct        IN     NUMBER
        , P_Round_Pricing IN     NUMBER
        , P_Round_Yield   IN     NUMBER
--        , Pricing_Msg_rec IN OUT Pricing_Msgs%ROWTYPE
        , P_SP            IN OUT Security_Prices%ROWTYPE
        , p_nation_m      in     sourceS_for_revolution.nation_code%type
        , p_Success       OUT    BOOLEAN
        , p_message       out nocopy varchar2
        )
IS --{
--
  V_Fri_Xref_Cy        VARCHAR2(3) := NULL;
  V_Fri_Xref_Artm_Code VARCHAR2(2) := NULL;
  V_Fri_Xref_Conv_No   NUMBER := NULL;
  V_Fri_Ticker         Tickers.Fri_Ticker%TYPE := NULL;
  V_Fri_Source         Sources.Code%TYPE := NULL;
  V_Destination        Price_Destinations.Code%TYPE := NULL;
  W_Fri_Source         Sources.Code%TYPE := NULL;
  V_Active_flag        VARCHAR2(1) := NULL;
  V_Active_flag_T      VARCHAR2(1) := NULL;
  V_Source_Open        Holiday_Classes.Code%TYPE;
  V_Data_Approved_Flag Unapp_Security_Prices.Data_Approved_Flag%TYPE;
  Sec_Prc_Rec          ds_Security_Prices%ROWTYPE;
  --W_Sec_Prc_Rec        Security_Prices%ROWTYPE;
  Curr_Xref_Rec        Supplier_Xref_Currencies%ROWTYPE;
  V_Error_Code         VARCHAR2(32);
  V_Error_Message      VARCHAR2(1024);

  V_Price_Changed      BOOLEAN;
  V_Bid_Changed        BOOLEAN;
  V_Ask_Changed        BOOLEAN;
  V_Mid_Changed        BOOLEAN;

  V_Cfw_Msg            VARCHAR2(150);
  V_Cfw_Count          INTEGER;
  V_Cfw_Status         INTEGER;

  V_Orig_Source_Code   Ld_Suppliers_Prc_Messages.Source%TYPE;

  v_tmp_message       varchar2(4000);

--
--CURSOR Cur_Get_Trd_Sts
--     ( I_Fri_Ticker     Securities.Fri_Ticker%TYPE
--     , I_Source_Code    Sources.Code%TYPE
--     , I_Price_Currency Currencies.Currency%TYPE
--     , I_Date           Date
--     ) IS
--SELECT T.Active_Flag
--  FROM Security_Trading_Status S
--     , Trading_Status T
-- WHERE S.Fri_Ticker     = I_Fri_Ticker
--   AND S.Source_Code    = I_Source_Code
--   AND S.Price_Currency = I_Price_Currency
--   AND S.Trading_Status_Code = T.Code
--   AND S.Date_Of_Status = (
--       SELECT MAX( P.DATE_OF_STATUS )
--         FROM Security_Trading_Status P
--        WHERE P.Fri_Ticker     = S.Fri_Ticker
--          AND P.Source_Code    = S.Source_Code
--          AND P.Price_Currency = S.Price_Currency
--          AND P.Date_Of_Status <= I_Date
--       );
--
CURSOR Cur_Upd_Sec_Prices
     ( I_Fri_Ticker     Securities.Fri_Ticker%TYPE
     --, I_Source_Code    Sources.Code%TYPE
     , I_Nation_Code    nations.Code%TYPE
     , I_Price_Currency Currencies.Currency%TYPE
     , I_Data_Supplier  Data_Suppliers.Code%TYPE
     , I_Delivery_Date  DATE
     ) IS
SELECT *
  FROM ds_SECURITY_PRICES
 WHERE Fri_Ticker         = I_Fri_Ticker
   --AND Official_Prices.Source_Synonym( Source_Code ) = I_Source_Code
   and nation_code        = I_Nation_Code
   AND Price_Currency     = I_Price_Currency
   AND Delivery_Date      = I_Delivery_Date
   AND Data_Supplier_Code = I_Data_Supplier
     ;
--   FOR UPDATE;
--
BEGIN --{
-- DBMS_OUTPUT.PUT_LINE('Entering validate record');
  --
  Pgmloc := 2210;
  p_Success := FALSE;
  --
  --  Get the FRI Trading Status (Active or not)
  Pgmloc := 2220;
--  OPEN Cur_Get_Trd_Sts ( P_SP.Fri_Ticker
--                       , P_SP.Source_Code
--                       , P_SP.Price_Currency
--                       , TRUNC( P_SP.Delivery_Date )
--                       );
--  V_Active_Flag := NULL;
--  Pgmloc := 2230;
--  FETCH Cur_Get_Trd_Sts
--   INTO V_Active_flag;
--  --
--  -- Treat PINK_SHEET and NASDAQ BULLETIN BOARD as One Source
--  -- Treat NYBE and NYSE as One Source
--  --
--  IF Cur_Get_Trd_Sts%NOTFOUND
--  THEN
--    V_Active_Flag := NULL;
--  END IF;
--  Pgmloc := 2240;
--  CLOSE Cur_Get_Trd_sts;
--  IF V_Active_Flag = 'Y'
--  THEN --{
--    NULL;
--  ELSE --}{
--    Pgmloc := 2250;
--    W_Fri_Source := Official_Prices.Source_Other_Code( P_SP.Source_Code );
---- Fin de modification - JSL - 28 septembre 2010
--    IF W_Fri_Source IS NOT NULL
--    THEN --{
--      Pgmloc := 2260;
--      OPEN Cur_Get_Trd_Sts ( P_SP.Fri_Ticker
--                           , W_Fri_Source
--                           , P_SP.Price_Currency
--                           , TRUNC( P_SP.Delivery_Date )
--                           );
--      V_Active_Flag_T := NULL;
--      Pgmloc := 2270;
--      FETCH Cur_Get_Trd_Sts
--       INTO V_Active_flag_T;
--      IF Cur_Get_Trd_Sts%NOTFOUND THEN V_Active_Flag_T := NULL; END IF;
--      Pgmloc := 2280;
--      CLOSE Cur_Get_Trd_sts;
--      IF V_Active_Flag_T = 'Y'
--      THEN
--        P_SP.Source_Code := W_Fri_Source;
--      END IF;
--    END IF; --}
--  END IF; --}
--  --
--  --
--  Pgmloc := 2290;
--  IF Cur_Upd_Sec_Prices%ISOPEN
--  THEN
--    Pgmloc := 2300;
--    CLOSE Cur_Upd_Sec_Prices;
--  END IF;
  Pgmloc := 2310;
  OPEN  Cur_Upd_Sec_Prices
     ( P_SP.Fri_Ticker
     --, Official_Prices.Source_Synonym( P_SP.Source_Code )
     , p_nation_m
     , P_SP.Price_Currency
     , P_SP.Data_Supplier_Code
     , P_SP.Delivery_Date
     );
  Pgmloc := 2320;
  FETCH Cur_Upd_Sec_Prices
   INTO Sec_Prc_Rec;
  --
  IF Cur_Upd_Sec_Prices%NOTFOUND
  THEN --{
    -- Create a new record that will be inserted later on.
    Pgmloc := 2330;
    Sec_Prc_Rec := null;
    Pgmloc := 2340;
    Sec_Prc_rec.FRI_TICKER          := P_SP.Fri_Ticker;
    Sec_Prc_rec.SOURCE_CODE         := P_SP.Source_Code;
    Sec_Prc_rec.PRICE_CURRENCY      := P_SP.Price_Currency;
    Sec_Prc_rec.DELIVERY_DATE       := P_SP.Delivery_Date;
    Sec_Prc_rec.DATA_SUPPLIER_CODE  := P_SP.Data_Supplier_Code;
  END IF; --}



  sec_prc_rec.nation_code := p_nation_m;

  Pgmloc := 2350;
  V_Price_Changed                 := FALSE;
  V_Bid_Changed                   := FALSE;
  V_Ask_Changed                   := FALSE;
  V_Mid_Changed                   := FALSE;
-- Ajout - JSL - 16 mars 2012
-- Validate only current values
  IF P_SP.Price_Date <> TRUNC( P_SP.Delivery_Date )
  THEN
    P_SP.Price_Date         := NULL;
    P_SP.Price              := NULL;
    P_SP.High               := NULL;
    P_SP.Low                := NULL;
    P_SP.Open_Price         := NULL;
    P_SP.Yield              := NULL;
    P_SP.Dirty_Price        := NULL;
    P_SP.Price_Mod_Duration := NULL;
    P_SP.High_52W           := NULL;
    P_SP.Low_52W            := NULL;
    P_SP.Board_Lot_Flag     := NULL;
    P_SP.Last_Trade_Time    := NULL;
    P_SP.Number_Of_Trans    := NULL;
    P_SP.Accrued_Interest   := NULL;
    P_SP.Market_Cap         := NULL;
  END IF;
  IF P_SP.Bid_Date <> TRUNC( P_SP.Delivery_Date )
  THEN
    P_SP.Bid_Date         := NULL;
    P_SP.Bid              := NULL;
    P_SP.Bid_Yield        := NULL;
    P_SP.Bid_Dirty_Price  := NULL;
    P_SP.Bid_Mod_Duration := NULL;
  END IF;
  IF P_SP.Ask_Date <> TRUNC( P_SP.Delivery_Date )
  THEN
    P_SP.Ask_Date         := NULL;
    P_SP.Ask              := NULL;
    P_SP.Ask_Yield        := NULL;
    P_SP.Ask_Dirty_Price  := NULL;
    P_SP.Ask_Mod_Duration := NULL;
  END IF;
  IF P_SP.Mid_Date <> TRUNC( P_SP.Delivery_Date )
  THEN
    P_SP.Mid_Date         := NULL;
    P_SP.Mid              := NULL;
    P_SP.Mid_Yield        := NULL;
    P_SP.Mid_Dirty_Price  := NULL;
    P_SP.Mid_Mod_Duration := NULL;
  END IF;
-- Fin d'ajout - JSL - 16 mars 2012
  --
  IF P_SP.Price_Date IS NOT NULL
  THEN
    Sec_Prc_rec.Price_Origin_Code     := 'L';
    Sec_Prc_rec.SOURCE_CODE           := P_SP.Source_Code;
    Sec_Prc_Rec.Price_Date            := P_SP.Price_Date;
    Sec_Prc_Rec.Price                 := P_SP.Price;
    Sec_Prc_Rec.Volume                := P_SP.Volume;
    Sec_Prc_Rec.High                  := P_SP.High;
    Sec_Prc_Rec.Low                   := P_SP.Low;
    Sec_Prc_Rec.Open_Price            := P_SP.Open_Price;
    Sec_Prc_Rec.Yield                 := P_SP.Yield;
    Sec_Prc_Rec.Dirty_Price           := P_SP.Dirty_Price;
    Sec_Prc_Rec.Price_Mod_Duration    := P_SP.Price_Mod_Duration;
    Sec_Prc_Rec.Board_Lot_Flag        := P_SP.Board_Lot_Flag;
    Sec_Prc_Rec.Last_Trade_Time       := P_SP.Last_Trade_Time;
    Sec_Prc_Rec.Number_Of_Trans       := P_SP.Number_Of_Trans;
    Sec_Prc_Rec.Accrued_Interest      := P_SP.Accrued_Interest;
    Sec_Prc_Rec.Term_In_Years         := P_SP.Term_In_Years;
    Sec_Prc_Rec.Settlement_Date       := P_SP.Settlement_Date;
    Sec_Prc_Rec.Maturity_Date         := P_SP.Maturity_Date;
    Sec_Prc_Rec.Maturity_Type         := P_SP.Maturity_Type;
    Sec_Prc_Rec.Index_Ratio           := P_SP.Index_Ratio;
    Sec_Prc_Rec.Rpb_Factor            := P_SP.Rpb_Factor;
    Sec_Prc_Rec.Effective_Date_Factor := P_SP.Effective_Date_Factor;
    Sec_Prc_Rec.Next_Chg_Date_Factor  := P_SP.Next_Chg_Date_Factor;
    Sec_Prc_Rec.Benchmark_Code        := P_SP.Benchmark_Code;
    Sec_Prc_Rec.Spread                := P_SP.Spread;
    Sec_Prc_Rec.Spread_From_Generic   := P_SP.Spread_From_Generic;
    Sec_Prc_Rec.Spread_From_Libor     := P_SP.Spread_From_Libor;
    Sec_Prc_Rec.Convexity             := P_SP.Convexity;
    Sec_Prc_Rec.Pvbp                  := P_SP.Pvbp;
    Sec_Prc_Rec.Average_Life_Date     := P_SP.Average_Life_Date;
    Sec_Prc_Rec.Market_Cap            := P_SP.Market_Cap;
    Sec_Prc_Rec.IFRS_Code             := P_SP.IFRS_Code;
  END IF;
  IF P_SP.Bid_Date IS NOT NULL
  THEN
    Sec_Prc_rec.Price_Origin_Code     := 'L';
-- Modification - JSL - 04 avril 2013
--  Sec_Prc_rec.SOURCE_CODE           := P_SP.Source_Code;
-- Fin de modification - JSL - 04 avril 2013
    Sec_Prc_Rec.Bid_Date         := P_SP.Bid_Date;
    Sec_Prc_Rec.Bid              := P_SP.Bid;
    Sec_Prc_Rec.Bid_Yield        := P_SP.Bid_Yield;
    Sec_Prc_Rec.Bid_Dirty_Price  := P_SP.Bid_Dirty_Price;
    Sec_Prc_Rec.Bid_Mod_Duration := P_SP.Bid_Mod_Duration;
  END IF;
  IF P_SP.Ask_Date IS NOT NULL
  THEN
    Sec_Prc_rec.Price_Origin_Code     := 'L';
-- Modification - JSL - 04 avril 2013
--  Sec_Prc_rec.SOURCE_CODE           := P_SP.Source_Code;
-- Fin de modification - JSL - 04 avril 2013
    Sec_Prc_Rec.Ask_Date         := P_SP.Ask_Date;
    Sec_Prc_Rec.Ask              := P_SP.Ask;
    Sec_Prc_Rec.Ask_Yield        := P_SP.Ask_Yield;
    Sec_Prc_Rec.Ask_Dirty_Price  := P_SP.Ask_Dirty_Price;
    Sec_Prc_Rec.Ask_Mod_Duration := P_SP.Ask_Mod_Duration;
  END IF;
  IF P_SP.Mid_Date IS NOT NULL
  THEN
    Sec_Prc_rec.Price_Origin_Code     := 'L';
-- Modification - JSL - 04 avril 2013
--  Sec_Prc_rec.SOURCE_CODE           := P_SP.Source_Code;
-- Fin de modification - JSL - 04 avril 2013
    Sec_Prc_Rec.Mid_Date         := P_SP.Mid_Date;
    Sec_Prc_Rec.Mid              := P_SP.Mid;
    Sec_Prc_Rec.Mid_Yield        := P_SP.Mid_Yield;
    Sec_Prc_Rec.Mid_Dirty_Price  := P_SP.Mid_Dirty_Price;
    Sec_Prc_Rec.Mid_Mod_Duration := P_SP.Mid_Mod_Duration;
  END IF;
  Sec_Prc_Rec.Updated_Flag          := P_SP.Updated_Flag;
  Sec_Prc_Rec.Delivery_Date_Bc      := P_SP.Delivery_Date_Bc;
  Sec_Prc_Rec.User_Key_Code         := P_SP.User_Key_Code;
  Sec_Prc_Rec.Price_Origin_Code     := P_SP.Price_Origin_Code;
  Sec_Prc_Rec.Last_User             := P_SP.Last_User;
  Sec_Prc_Rec.Last_Stamp            := SYSDATE;


  V_Data_Approved_Flag := 'Y';
-- Retrait - JSL - 06 avril 2015
--   Pgmloc := 2360;
--   Holiday.Is_It_Open_Proc( TRUNC(Sec_Prc_Rec.Delivery_Date)
--                          , 'SOURCE'
--                          , Sec_Prc_Rec.Source_Code
--                          , NULL
--                          , V_Source_Open
--                          );
--   IF V_Source_Open IS NOT NULL
--   THEN
--     Pgmloc := 2370;
--     V_Data_Approved_Flag := 'NH';
--     Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'080'
--                                      , 'N'
--                                      , Sec_Prc_Rec.Source_Code
--                                      , V_Source_Open
--                                      , V_Error_Message
--                                      );
--     v_tmp_message := V_Error_Message;
--   END IF;
-- Fin de retrait - JSL - 06 avril 2015


  Pgmloc := 2380;
  DECLARE --{
    --V_Message      VARCHAR2(1024) := NULL;
    V_Error_Column All_Tab_Columns.Column_Name%TYPE := NULL;
  BEGIN --{
    --
    IF V_Data_approved_Flag = 'Y'
    THEN --{
      IF Cur_Upd_Sec_Prices%FOUND
      THEN --{
        Pgmloc := 2390;
        UPDATE ds_SECURITY_PRICES
           SET Price_date            = Sec_Prc_Rec.price_date
             , Price                 = Sec_Prc_Rec.Price
             , Volume                = Sec_Prc_Rec.Volume
             , High                  = Sec_Prc_Rec.High
             , Low                   = Sec_Prc_Rec.Low
             , Open_Price            = Sec_Prc_Rec.Open_Price
             , Yield                 = Sec_Prc_Rec.Yield
             , Dirty_Price           = Sec_Prc_Rec.Dirty_Price
             , Price_Mod_Duration    = Sec_Prc_Rec.Price_Mod_Duration
             , Board_Lot_flag        = Sec_Prc_Rec.Board_Lot_flag
             , Last_trade_time       = Sec_Prc_Rec.Last_trade_time
             , number_of_Trans       = Sec_Prc_Rec.Number_of_trans
             , Bid_date              = Sec_Prc_Rec.Bid_date
             , Bid                   = Sec_Prc_Rec.Bid
             , Bid_Yield             = Sec_Prc_Rec.Bid_Yield
             , Bid_Dirty_Price       = Sec_Prc_Rec.Bid_Dirty_Price
             , Bid_Mod_Duration      = Sec_Prc_Rec.Bid_Mod_Duration
             , Ask_date              = Sec_Prc_Rec.Ask_date
             , Ask                   = Sec_Prc_Rec.Ask
             , Ask_Yield             = Sec_Prc_Rec.Ask_Yield
             , Ask_Dirty_Price       = Sec_Prc_Rec.Ask_Dirty_Price
             , Ask_Mod_Duration      = Sec_Prc_Rec.Ask_Mod_Duration
             , Mid_date              = Sec_Prc_Rec.Mid_date
             , Mid                   = Sec_Prc_Rec.Mid
             , Mid_Yield             = Sec_Prc_Rec.Mid_Yield
             , Mid_Dirty_Price       = Sec_Prc_Rec.Mid_Dirty_Price
             , Mid_Mod_Duration      = Sec_Prc_Rec.Mid_Mod_Duration
             , Price_Origin_Code     = Sec_Prc_Rec.Price_Origin_Code
             , Updated_Flag          = Sec_Prc_Rec.Updated_Flag
             , Accrued_Interest      = Sec_Prc_Rec.Accrued_Interest
             , Term_in_Years         = Sec_Prc_Rec.Term_in_Years
             , Settlement_Date       = Sec_Prc_Rec.Settlement_Date
             , Maturity_Date         = Sec_Prc_Rec.Maturity_Date
             , Maturity_Type         = Sec_Prc_Rec.Maturity_Type
             , Index_Ratio           = Sec_Prc_Rec.Index_Ratio
             , RPB_Factor            = Sec_Prc_Rec.RPB_Factor
             , Effective_Date_Factor = Sec_Prc_Rec.Effective_Date_Factor
             , Next_Chg_Date_Factor  = Sec_Prc_Rec.Next_Chg_Date_Factor
             , Benchmark_Code        = Sec_Prc_Rec.Benchmark_Code
             , Spread                = Sec_Prc_Rec.Spread
             , Spread_From_Generic   = Sec_Prc_Rec.Spread_From_Generic
             , Spread_From_Libor     = Sec_Prc_Rec.Spread_From_Libor
             , Convexity             = Sec_Prc_Rec.Convexity
             , PVBP                  = Sec_Prc_Rec.PVBP
             , Average_Life_Date     = Sec_Prc_Rec.Average_Life_Date
             , Market_Cap            = Sec_Prc_Rec.Market_Cap
             , IFRS_Code             = Sec_Prc_Rec.IFRS_Code
             , Last_User             = Sec_Prc_Rec.Last_User
             , Last_Stamp            = Sec_Prc_Rec.Last_Stamp
-- Modification - JSL - 11 juin 2012
--           , nation_code           = Sec_Prc_Rec.nation_code
             , Source_Code           = Sec_Prc_Rec.Source_Code
-- Fin de modification - JSL - 11 juin 2012
         WHERE Fri_Ticker            = Sec_Prc_Rec.Fri_Ticker
-- Modification - JSL - 11 juin 2012
--         AND Source_Code           = Sec_Prc_Rec.Source_Code
           AND Nation_Code           = Sec_Prc_Rec.Nation_Code
-- Fin de modification - JSL - 11 juin 2012
           AND Price_Currency        = Sec_Prc_Rec.Price_Currency
           AND Delivery_Date         = Sec_Prc_Rec.Delivery_Date
           AND Data_Supplier_Code    = Sec_Prc_Rec.Data_Supplier_Code
             ;
--        WHERE CURRENT OF Cur_Upd_Sec_Prices;
        Pgmloc := 2400;
        IF Sec_Prc_rec.Price_Origin_Code <> 'X'
        THEN
          g_Pricing_Msgs_Rec.Value_03 := NVL(g_Pricing_Msgs_Rec.Value_03,0)
                                    + 1;
        ELSE
          g_Pricing_Msgs_Rec.Value_09 := NVL(g_Pricing_Msgs_Rec.Value_09,0)
                                    + 1;
        END IF;
      ELSE --}{
        Pgmloc := 2410;
        INSERT
          INTO ds_Security_Prices
             ( Fri_Ticker         , Source_Code
             , Price_Currency     , Delivery_Date
             , Data_Supplier_Code , Price_Date
             , Price              , Volume
             , High               , Low
             , Open_Price         , Yield
             , Board_Lot_Flag     , Last_Trade_Time
             , Number_Of_Trans    , Bid_Date
             , Bid                , Ask_Date
             , Ask                , Mid_Date
             , Mid                , Updated_Flag
             , Delivery_Date_Bc   , User_Key_Code
             , Price_Origin_Code  , Last_User
             , Last_Stamp
             , Bid_Yield
             , Ask_Yield
             , Mid_Yield
             , Dirty_Price        , Price_Mod_Duration
             , Bid_Dirty_Price    , Bid_Mod_Duration
             , Ask_Dirty_Price    , Ask_Mod_Duration
             , Mid_Dirty_Price    , Mid_Mod_Duration
             , Accrued_Interest
             , Settlement_Date
             , Maturity_Date
             , Maturity_Type
             , Index_Ratio
             , Rpb_Factor
             , Effective_Date_Factor
             , Next_Chg_Date_Factor
             , Benchmark_Code
             , Spread
             , Spread_From_Generic
             , Spread_From_Libor
             , Convexity
             , PVBP
             , Average_Life_Date
             , Market_Cap
             , IFRS_Code
             , nation_code
             )
        VALUES
-- Modification - JSL - 04 avril 2013
--           ( Sec_Prc_Rec.Fri_Ticker         , Sec_Prc_Rec.Source_Code
             ( Sec_Prc_Rec.Fri_Ticker         , NVL( Sec_Prc_Rec.Source_Code, P_SP.Source_Code )
-- Fin de modification - JSL - 04 avril 2013
             , Sec_Prc_Rec.Price_Currency     , Sec_Prc_Rec.Delivery_Date
             , Sec_Prc_Rec.Data_Supplier_Code , Sec_Prc_Rec.Price_Date
             , Sec_Prc_Rec.Price              , Sec_Prc_Rec.Volume
             , Sec_Prc_Rec.High               , Sec_Prc_Rec.Low
             , Sec_Prc_Rec.Open_Price         , Sec_Prc_Rec.Yield
             , Sec_Prc_Rec.Board_Lot_Flag     , Sec_Prc_Rec.Last_Trade_Time
             , Sec_Prc_Rec.Number_Of_Trans    , Sec_Prc_Rec.Bid_Date
             , Sec_Prc_Rec.Bid                , Sec_Prc_Rec.Ask_Date
             , Sec_Prc_Rec.Ask                , Sec_Prc_Rec.Mid_Date
             , Sec_Prc_Rec.Mid                , Sec_Prc_Rec.Updated_Flag
             , Sec_Prc_Rec.Delivery_Date_Bc   , Sec_Prc_Rec.User_Key_Code
             , Sec_Prc_Rec.Price_Origin_Code  , Sec_Prc_Rec.Last_User
             , Sec_Prc_Rec.Last_Stamp
             , Sec_Prc_Rec.Bid_Yield
             , Sec_Prc_Rec.Ask_Yield
             , Sec_Prc_Rec.Mid_Yield
             , Sec_Prc_Rec.Dirty_Price        , Sec_Prc_Rec.Price_Mod_Duration
             , Sec_Prc_Rec.Bid_Dirty_Price    , Sec_Prc_Rec.Bid_Mod_Duration
             , Sec_Prc_Rec.Ask_Dirty_Price    , Sec_Prc_Rec.Ask_Mod_Duration
             , Sec_Prc_Rec.Mid_Dirty_Price    , Sec_Prc_Rec.Mid_Mod_Duration
             , Sec_Prc_Rec.Accrued_Interest
             , Sec_Prc_Rec.Settlement_Date
             , Sec_Prc_Rec.Maturity_Date
             , Sec_Prc_Rec.Maturity_Type
             , Sec_Prc_Rec.Index_Ratio
             , Sec_Prc_Rec.Rpb_Factor
             , Sec_Prc_Rec.Effective_Date_Factor
             , Sec_Prc_Rec.Next_Chg_Date_Factor
             , Sec_Prc_Rec.Benchmark_Code
             , Sec_Prc_Rec.Spread
             , Sec_Prc_Rec.Spread_From_Generic
             , Sec_Prc_Rec.Spread_From_Libor
             , Sec_Prc_Rec.Convexity
             , Sec_Prc_Rec.PVBP
             , Sec_Prc_Rec.Average_Life_Date
             , Sec_Prc_Rec.Market_Cap
             , Sec_Prc_Rec.IFRS_Code
             , Sec_Prc_Rec.nation_code
             );
        g_Pricing_Msgs_Rec.Value_02 := NVL(g_Pricing_Msgs_Rec.Value_02,0)
                                  + 1;
      END IF; --}
    END IF; --}
    p_Success := TRUE;

  EXCEPTION
    WHEN OTHERS
    THEN
      --mlz - 17mar2014
      v_tmp_message := get_error_msg(v_tmp_message);
      log_step( replace(v_tmp_message,chr(10),' ') );
      logmon_write( v_tmp_message, Sec_Prc_Rec.fri_ticker, Sec_Prc_Rec.Source_Code, Sec_Prc_Rec.Price_Currency, Sec_Prc_Rec.Delivery_Date, Sec_Prc_Rec.Data_Supplier_Code );
      
      --concat_before( v_tmp_message, get_error_msg('') );
      --
      --V_Data_Approved_Flag := 'N';
      --Load_Supplier_Rules.Write_File( P_Handle
      --                              , v_tmp_message
      --                              , g_Pricing_Msgs_Rec
      --                              , p_Success
      --                              );
      --endmlz - 17mar2014
  END; --}}
  --
  Pgmloc := 2480;
  CLOSE Cur_Upd_Sec_Prices;
  --
  --mlz - 29may2014 - same end as in: load_official_prices
  --p_message := v_tmp_message;
  p_Success := TRUE;
  --endmlz - 29may2014
  --
<<GET_OUT>>
  NULL;
--
EXCEPTION
  WHEN OTHERS
  THEN
    --mlz - 17mar2014
    v_tmp_message := get_error_msg(v_tmp_message);
    log_step( replace(v_tmp_message,chr(10),' ') );
    --mlz - 29may2014
    logmon_write( replace(v_tmp_message,chr(10),' ') );
    --endmlz - 29may2014
    
    --concat_before( v_tmp_message, get_error_msg('') );
    --
    g_Pricing_Msgs_Rec.Err_Text := substr(v_tmp_message,1,255);
    --Manage_Messages.Get_Oasis_Message( C_LSR_Oasis_Message||'010'
    --                                 , 'N'
    --                                 , TO_CHAR( SQLCODE )
    --                                 , 'Validate_Record'
    --                                 , g_Pricing_Msgs_Rec.Err_Text
    --                                 );
    --Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'999'
    --                                 , 'N'
    --                                 , Pgmloc
    --                                 , 'Validate_Record'
    --                                 , g_Pricing_Msgs_Rec.Msg_Text
    --                                 );
    --Load_Supplier_Rules.Write_File( P_Handle
    --                              , g_Pricing_Msgs_Rec.Err_Text
    --                              , g_Pricing_Msgs_Rec
    --                              , p_Success
    --                              );
    --Load_Supplier_Rules.Write_File( P_Handle
    --                              , g_Pricing_Msgs_Rec.Msg_Text
    --                              , g_Pricing_Msgs_Rec
    --                              , p_Success
    --                              );
    --Load_Supplier_Rules.Write_File( P_Handle
    --                              , 'Fri_Ticker is : '||P_SP.Fri_Ticker
    --                              , g_Pricing_Msgs_Rec
    --                              , p_Success
    --                              );
    --endmlz - 17mar2014
    
    p_Success := FALSE;
    p_message := v_tmp_message;
  --
END Validate_Record; --}}
--
--
-----------------------------------------------------------------------
-- Get the most recent pricing message, may be from Linux.
-----------------------------------------------------------------------
--
--
PROCEDURE Get_Pricing_Msgs
        ( P_Program_Id     IN     Ops_Scripts.Program_Id%TYPE
        , P_Load_Date      IN     DATE
--        , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , p_Success             OUT BOOLEAN
        ) IS --{
--
V_System_Date        DATE;
V_On_Server          BOOLEAN := CONSTANTS.On_Server;
--
BEGIN --{
  --
  p_success := false;
  Pgmloc := 1580;
  V_System_Date    := SYSDATE;
  IF V_On_Server
  THEN --{
    Pgmloc := 1590;
    BEGIN --{
      Pgmloc := 1600;
      SELECT *
        INTO g_Pricing_Msgs_Rec
        FROM Pricing_Msgs PM
       WHERE PM.Program_Id = P_Program_Id
         AND PM.Date_of_Prices = P_Load_Date
         AND PM.Successfull_Flag = 'N'
         AND PM.Terminal_Id = NVL( Constants.Get_Terminal_Id, 'N/A' )
         AND PM.Date_Of_Msg = (
             SELECT MAX( PM1.Date_Of_Msg )
               FROM Pricing_Msgs PM1
              WHERE PM.Program_Id     = PM1.Program_Id
                AND PM.Date_Of_Prices = PM1.Date_Of_Prices
                AND PM.Terminal_Id    = PM1.Terminal_Id
             );
      V_System_Date := g_Pricing_Msgs_Rec.Date_Of_Msg;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        Pgmloc := 1610;
        Load_Supplier_Rules.Initialize_Prc_Msgs( P_Program_Id
                                               , V_System_date
                                               , P_Load_Date
                                               , g_Pricing_Msgs_Rec
                                               , p_Success
                                               );
        IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
    END; --}
  ELSE --}{
    ----------------------------------------
    -- Initalization of the pricing messages
    ----------------------------------------
    Pgmloc := 1620;
    load_supplier_rules.Initialize_Prc_Msgs( P_Program_Id
                                           , V_System_date
                                           , P_Load_Date
                                           , g_Pricing_Msgs_Rec
                                           , p_Success
                                           );
    IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  END IF; --}
  Pgmloc := 1630;
  g_pricing_msgs_rec.last_user := nvl( g_pricing_msgs_rec.last_user
                                     , constants.get_user_id );
  g_Pricing_Msgs_Rec.Successfull_Flag := 'N';
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'010'
                                   , 'N'
                                   , P_Program_Id
                                   , g_Pricing_Msgs_Rec.Msg_Text
                                   );
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'020'
                                   , 'N'
                                   , P_Program_Id
                                   , g_Pricing_Msgs_Rec.Msg_Text_2
                                   );
  g_Pricing_Msgs_Rec.Value_01         := 0;
  g_Pricing_Msgs_Rec.Value_02         := 0;
  g_Pricing_Msgs_Rec.Value_03         := 0;
  g_Pricing_Msgs_Rec.Value_04         := 0;
  g_Pricing_Msgs_Rec.Value_05         := 0;
  g_Pricing_Msgs_Rec.Value_06         := 0;
  g_Pricing_Msgs_Rec.Value_07         := 0;
  g_Pricing_Msgs_Rec.Value_08         := 0;
  g_Pricing_Msgs_Rec.Value_15         := 0;
  Load_Supplier_Rules.Update_Prc_Msgs( P_Program_Id
                                     , V_System_date
                                     , g_Pricing_Msgs_Rec
                                     , p_Success
                                     );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 1640;
  UPDATE Break_Points
     SET Status = 'PROG'
       , Information = TO_CHAR( V_System_Date, 'dd-mon-yyyy hh24:mi:ss')
       , Nb_Records = g_Pricing_Msgs_Rec.Value_01
       , Last_User = g_user --Constants.Get_User_Id
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  COMMIT;
  --
  p_Success := TRUE;
  --
<< Get_Out >>
  NULL;
END Get_Pricing_Msgs; --}}
--
-----------------------------------------------------------------------
-- Do the Housekeeping of starting a new load
-----------------------------------------------------------------------
--
--
PROCEDURE Process_Begin
        ( P_Program_Id     IN     Ops_Scripts.Program_Id%TYPE
        , P_Log_File       IN OUT NOCOPY Utl_File.File_Type
        , P_Delivery_Time  IN OUT NOCOPY Data_Supply_Schedule.Delivery_Time%TYPE
        , P_DSS_Rec        IN OUT NOCOPY Data_Supply_Schedule%ROWTYPE
        , P_IDS_Rec        IN OUT NOCOPY Identification_Systems%ROWTYPE
        , P_E_DSS_Rec      IN OUT NOCOPY Data_Supply_Schedule%ROWTYPE
        , P_E_IDS_Rec      IN OUT NOCOPY Identification_Systems%ROWTYPE
        , P_Round_Pricing     OUT NUMBER
        , P_Round_Yield       OUT NUMBER
--        , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , p_Success             OUT BOOLEAN
        ) IS --{
--
E_Program_Id         Ops_Scripts.Program_Id%TYPE;
Data_File            Utl_File.File_Type;
Copy_File            Utl_File.File_Type;
Dir_Name             VARCHAR2(100);
Data_File_name       Supplier_Running_Parms.File_Name%TYPE;
Log_file_name        Supplier_Running_Parms.File_Name%TYPE;
Copy_File_name       Supplier_Running_Parms.File_Name%TYPE;
V_Identification_System_Code Identification_Systems.Code%TYPE := NULL;
V_Round_String       VARCHAR2(10) := NULL;
V_Message            VARCHAR2(255):= NULL;
--
BEGIN --{
  Pgmloc := 1650;
  p_Success := FALSE;
  --
  Pgmloc := 1660;
  -------------------------------------------------------
  --  Get the Supplier information from the rules package
  -------------------------------------------------------
  Load_Supplier_Rules.Get_Supplier_Info( P_Program_id
                                       , g_Pricing_Msgs_Rec
                                       , P_DSS_Rec
                                       , P_IDS_Rec
                                       , p_Success
                                       );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  Pgmloc := 1670;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'030'
                                   , 'N'
                                   , P_DSS_Rec.Data_Supplier_Code
                                   , P_DSS_Rec.Delivery_Time
                                   , g_Pricing_Msgs_Rec.Msg_Text
                                   );
  Pgmloc := 1680;
  -------------------------------------------------------
  --  Get the name of both files (supp file and log file)
  -------------------------------------------------------
  P_Delivery_Time := Scripts.Get_Arg_Value( P_Program_id, 'TIME' );
  IF P_Delivery_Time IS NULL
  THEN
    P_Delivery_Time := P_DSS_Rec.Delivery_Time;
  END IF;
  Pgmloc := 1690;
  Load_Supplier_Rules.Get_File_Names( P_DSS_Rec.Data_Supplier_Code
                                    , P_Delivery_Time
                                    , g_Pricing_Msgs_Rec
                                    , Data_File_Name
                                    , Log_File_Name
                                    , Copy_File_Name
                                    , Dir_Name
                                    , p_Success
                                    );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 1700;
  ---------------------------------------
  --  Open the Log file in the Write mode
  ---------------------------------------
  Load_Supplier_Rules.Open_File( Log_File_Name
                               , Dir_Name
                               , 'W'
                               , g_Pricing_Msgs_Rec
                               , P_Log_File
                               , p_Success
                               );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  Pgmloc := 1710;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'010'
                                   , 'N'
                                   , P_Program_Id
                                   , V_Message
                                   );
  load_supplier_Rules.Write_file( P_Log_File
                                , V_Message
                                , g_Pricing_Msgs_Rec
                                , p_Success
                                );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  Pgmloc := 1720;
  load_supplier_Rules.Write_file( P_Log_File
                                , g_Pricing_Msgs_Rec.Msg_Text
                                , g_Pricing_Msgs_Rec
                                , p_Success
                                );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  Pgmloc := 1730;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'040'
                                   , 'N'
                                   , TO_CHAR( g_Pricing_Msgs_Rec.Date_of_Msg
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  load_supplier_Rules.Write_file( P_Log_File
                                , V_Message
                                , g_Pricing_Msgs_Rec
                                , p_Success
                                );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  Pgmloc := 1740;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'050'
                                   , 'N'
                                   , V_Message
                                   );
  load_supplier_Rules.Write_file( P_Log_File
                                , V_Message
                                , g_Pricing_Msgs_Rec
                                , p_Success
                                );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  Pgmloc := 1770;
  E_Program_Id := NULL;
  BEGIN
    E_Program_Id := TO_NUMBER( Scripts.Get_Arg_Value( P_Program_id, 'EMUL' ) );
  EXCEPTION WHEN OTHERS THEN NULL;
  END;
  IF E_program_id IS NULL
  THEN
    E_Program_Id := P_Program_Id;
  END IF;
  --
  Pgmloc := 1780;
  -------------------------------------------------------
  --  Get the Supplier information from the rules package
  -------------------------------------------------------
  Load_Supplier_Rules.Get_Supplier_Info( E_Program_id
                                       , g_Pricing_Msgs_Rec
                                       , P_E_DSS_Rec
                                       , P_E_IDS_Rec
                                       , p_Success
                                       );
  IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  IF E_Program_Id <> P_Program_Id
  THEN
     Pgmloc := 1790;
     Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'070'
                                      , 'N'
                                      , P_DSS_Rec.Identification_System_Code
                                      , V_Message
                                      );
     load_supplier_Rules.Write_file( P_Log_File
                                   , V_Message
                                   , g_Pricing_Msgs_Rec
                                   , p_Success
                                   );
     IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
     Pgmloc := 1800;
     Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'080'
                                      , 'N'
                                      , P_E_DSS_Rec.Data_Supplier_Code
                                      , P_E_DSS_Rec.Delivery_Time
                                      , V_Message
                                      );
     Pgmloc := 1810;
     load_supplier_Rules.Write_file( P_Log_File
                                   , V_Message
                                   , g_Pricing_Msgs_Rec
                                   , p_Success
                                   );
     IF ( NOT p_Success ) THEN GOTO GET_OUT; END IF;
  END IF;
  --
  ---------------------------------------------------
  --For Getting the Rounding Factor for Price, Yield
  ---------------------------------------------------
  Pgmloc := 1820;
  V_Round_String   := Scripts.Get_Arg_Value( P_Program_id, 'RND' );
  IF V_Round_String IS NULL
  THEN
    V_Round_String := C_Round_String;
  END IF;
  --
  Pgmloc := 1830;
  load_supplier_Rules.Get_Round_Values( V_Round_String
                                      , g_Pricing_Msgs_Rec
                                      , P_Round_Pricing
                                      , P_Round_Yield
                                      , p_Success
                                      );
  IF (NOT p_Success) THEN GOTO GET_OUT; END IF;
  --
  p_Success := TRUE;
  --
<< Get_Out >>
  NULL;
  --
END Process_Begin; --}}
--
-----------------------------------------------------------------------
-- Do the price load.
-----------------------------------------------------------------------
--
--
PROCEDURE Load_Prices( P_Program_Id   IN  Ops_Scripts.Program_Id%TYPE
                     , P_Load_Date    IN  DATE
                     , P_Commit_Freq  IN  INTEGER
                     , P_Success      OUT BOOLEAN
                     , P_Message      OUT VARCHAR2
                     ) IS
  DSS_Rec              Data_Supply_Schedule%ROWTYPE;
  --SP_Rec               Security_Prices%ROWTYPE;
  IDS_Rec              Identification_Systems%ROWTYPE;
  E_Program_Id         Ops_Scripts.Program_Id%TYPE;
  E_DSS_Rec            Data_Supply_Schedule%ROWTYPE;
  E_IDS_Rec            Identification_Systems%ROWTYPE;
  Success              BOOLEAN;

  Data_File            Utl_File.File_Type;
  Log_File             Utl_File.File_Type;
  Copy_File            Utl_File.File_Type;
  file_Date            DATE := NULL;
  Dir_Name             VARCHAR2(100);

  V_Xref_Curr          NUMBER := 0;
  V_Xref_Src           NUMBER := 0;
  End_Of_File          BOOLEAN := FALSE;

  Data_File_name       Supplier_Running_Parms.File_Name%TYPE;
  Log_file_name        Supplier_Running_Parms.File_Name%TYPE;
  Copy_File_name       Supplier_Running_Parms.File_Name%TYPE;

  V_Round_Pricing      NUMBER := NULL;
  V_Round_Yield        NUMBER := NULL;
  Commit_Count         NUMBER;
  Prev_count           NUMBER;
  V_User               VARCHAR2(4);
  V_Delivery_Date      DATE := Constants.Get_Site_Date;
  V_Delivery_Date_Time DATE;
  V_File_Type          VARCHAR2(16);
  P_Date_fmt           VARCHAR2(16);
  V_Rec_Count          NUMBER := 0;
  V_Load_Date          DATE;
--
  V_Delivery_time      DSS_Rec.Delivery_Time%TYPE := NULL;
  V_Message            VARCHAR2(255) := NULL;
--
  V_Source_Code        Sources.Code%TYPE;
--
  v_prices_tab         official_prices.sec_prc_t;
  v_prices_tab_ori     official_prices.sec_prc_t;
  v_reason_tab         reason_t;
  i                    binary_integer;
  v_nation_m           sources_for_revolution.nation_code%type;
  l_tmp_message        varchar2(4000);
--
---------------------------
--  Actual Start of Process
---------------------------
BEGIN
  --
  Pgmloc := 2490;
  V_Load_Date := NVL( P_Load_Date, Constants.Get_Site_Date );
  g_user := Constants.Get_User_Id;
  
  --mlz - 17mar2014
  v_load_date := trunc(v_load_date);
  g_load_date := v_load_date;
  --endmlz - 17mar2014
  
  --mlz - 12jul2013
  g_program_id := p_program_id;
  --endmlz - 12jul2013
  Pgmloc := 2500;
  Get_Pricing_Msgs( P_Program_Id
                  , V_Load_Date
                  --, g_Pricing_Msgs_Rec
                  , Success
                  );
  IF NOT SUCCESS THEN GOTO Get_Out; END IF;
  --
  Pgmloc := 2510;
  Process_Begin ( P_Program_Id
                , g_Log_fh
                , V_Delivery_Time
                , DSS_Rec
                , IDS_Rec
                , E_DSS_Rec
                , E_IDS_Rec
                , V_Round_Pricing
                , V_Round_Yield
                --, g_Pricing_Msgs_Rec
                , Success
                );
  IF NOT SUCCESS THEN GOTO Get_Out; END IF;
  --
  Pgmloc := 2520;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'050'
                                   , 'N'
                                   , V_Message
                                   );
  Load_Supplier_Rules.Write_file( g_Log_fh
                                , V_Message
                                , g_Pricing_Msgs_Rec
                                , Success
                                );
  IF NOT SUCCESS THEN GOTO GET_OUT; END IF;
  --

  --mlz - 17mar2014
  logmon_open(l_tmp_message);
  --endmlz - 17mar2014

  FOR R_Mid_From_Sources IN ( SELECT * FROM Mid_From_Sources )
  LOOP
    V_Source_Code := R_Mid_From_Sources.Source_Code;
    G_Mid_From_Sources( V_Source_Code ) := V_Source_Code;
  END LOOP;
  --
-- Ajout - JSL - 28 janvier 2010


  G_Data_Supplier_Code := DSS_Rec.Data_Supplier_Code;  --for global cursor
  G_delivery_time := V_Delivery_Time; --V? or P_DSS_Rec?

  --mlz - 29may2014 - reduce noise in the file
  -- --mlz - 17mar2014
  -- logmon_write('--------------------------------------------------------------------------------');
  -- logmon_write('LOAD DUAL - '||G_Data_Supplier_Code||' '||G_delivery_time);
  -- logmon_write('--------------------------------------------------------------------------------');
  -- --endmlz - 17mar2014
  --endmlz - 29may2014

  --mlz - 02jul2013
  g_lh := manage_prc_msgs_det.get_log_to_file_handle(
    g_Log_fh,
    c_sep
    );
  --endmlz - 02jul2013

  --
  -- parameter: SUBST_SUPPLIER
  --
  -- substitution supplier for all OFFICIAL_PRICES calls (to use STP2000 parms)
  --> if not set -->> use current
  --
  G_Subst_Data_Supplier_Code := scripts.get_arg_value( p_program_id, c_parm_subst_supplier);

  if G_Subst_Data_Supplier_Code is null then
    G_Subst_Data_Supplier_Code := G_Data_Supplier_Code;
  end if;



  --
  -- parameter: SUBST_DEL_TIME
  --
  -- substitution delivery time for all OFFICIAL_PRICES calls
  -- (to use STP2000 parms)
  --> if not set -->> use current
  --
  G_Subst_Delivery_Time := scripts.get_arg_value( p_program_id, c_parm_subst_del_time);

  if G_Subst_Delivery_Time is null then
    G_Subst_Delivery_Time := G_Delivery_Time;
  end if;

  official_prices.set_supplier_delivery_info( G_subst_Data_Supplier_Code
                                            , G_subst_delivery_time
                                            );


  --
  -- parameter: LOG_ALL
  --
  declare
    tmp OPS_SCRIPTS.SCRIPT_NAME%type;
  begin
    tmp :=  UPPER( Scripts.Get_Arg_Value( p_Program_Id, c_parm_log_all ) );
    if tmp in ('Y','YES') then
      g_log_all := true;
    else
      g_log_all := false;
    end if;
  end;


  --
  -- parameter: LOG_TO_TABLE
  --  default: N --> file (way faster)
  --
  declare
    tmp OPS_SCRIPTS.SCRIPT_NAME%type;
  begin
    tmp :=  UPPER( Scripts.Get_Arg_Value( p_Program_Id, c_parm_log_to_table ) );
    if tmp in ('Y','YES') then
      g_log_to_table := true;
    else
      g_log_to_table := false;
    end if;
  end;

  if g_log_to_table = false then
    log_header;
  end if;

-- Ajout - JSL - 24 octobre 2014
  G_Track_Suppliers.delete;
  DECLARE
    tmp      ops_scripts_parms.parm_value%type;
    all_null boolean;
    success  boolean;
  begin
    tmp := UPPER( Scripts.Get_Arg_Value( P_Program_Id, c_parm_track_suppliers ) );
    String_Utils.To_Table( tmp, String_Utils.C_Argument_Separator, G_Track_Suppliers, all_null, success );
  end;
-- Fin d'ajout - JSL - 24 octobre 2014


-- Fin d'ajout - JSL - 28 janvier 2010
  P_Date_Fmt         := C_Date_Format ;
  G_Header_Processed := FALSE;
  prev_count         := 0;
  Commit_Count       := 0;

  Pgmloc := 1380;
  OPEN cur_securities( V_Load_Date );

  -----------------------------------------------
  --  Start reading file till the end of the file
  -----------------------------------------------
  WHILE NOT End_Of_File
  LOOP --{

    pgmloc := 2300;
-- Emulation d'un fournisseur
-- Ne se fait pas a l'obtention du titre, mais au chargement.
    Get_Data_Line( g_Log_fh
                 , V_Load_Date
                 , DSS_Rec
--                 , g_Pricing_Msgs_Rec
                 --, SP_Rec
                 , v_prices_tab -->> prices (tab)
                 , v_prices_tab_ori
                 , v_reason_tab
                 , v_nation_m   -->> for nation_m
                 , V_Rec_Count
                 , Success
                 , End_of_File
                 );
    IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
    EXIT WHEN End_Of_File;
    --
    ----------------------------------------
    --  Validate the data record for rejects
    ----------------------------------------
    Pgmloc := 2400;


    i := v_prices_tab_ori.first;
    loop
      exit when i is null;
      exit when i > v_prices_tab_ori.last;

      if v_prices_tab.exists(i) then
        --the price was kept --> process it

        Validate_Record( g_Log_fh
                       , E_DSS_Rec
                       , E_IDS_Rec
                       , P_Date_Fmt
                       , V_Xref_Curr
                       , V_Xref_Src
                       , V_Round_Pricing
                       , V_Round_Yield
  --                     , g_Pricing_Msgs_Rec
                       --, SP_Rec
                       , v_prices_tab(i)
                       , v_nation_m
                       , Success
                       , l_tmp_message
                       );
        IF (NOT SUCCESS) THEN
          --fatal error
          log_price( v_prices_tab_ori(i)
                   , v_nation_m
                   , l_tmp_message
                   , v_reason_tab(i).text
                   , 'ERROR'
                   );
          GOTO GET_OUT;
        END IF;

        if l_tmp_message is not null then
          --non blocking error
          log_price( v_prices_tab_ori(i)
                   , v_nation_m
                   , l_tmp_message
                   , v_reason_tab(i).text
                   , 'ERROR'
                   );
        else
          if g_log_all then
            log_price( v_prices_tab_ori(i)
                     , v_nation_m
                     , l_tmp_message
                     , v_reason_tab(i).text
                     , 'PROCESSED'
                     );
          end if;
        end if;

        Commit_Count := Commit_Count + 1;
      else
        --the price was discarded --> just log the original one with the reason
        if g_log_all then
          log_price( v_prices_tab_ori(i)
                   , v_nation_m
                   , v_reason_tab(i).text
                   , null           --reason2
                   , nvl(v_reason_tab(i).status, 'NOT_PRC')
                   );
        end if;
      end if;

      i := v_prices_tab_ori.next(i);
    end loop;

    ------------------------
    --  Commit if necessary.
    ------------------------
    IF Commit_Count >= P_Commit_Freq
    THEN
      pgmloc := 2450;
      Load_Supplier_Rules.Update_Prc_Msgs( P_Program_ID
                                         , g_Pricing_Msgs_Rec.Date_of_Msg
                                         , g_Pricing_Msgs_Rec
                                         , Success
                                         );
      IF (NOT SUCCESS) THEN  GOTO GET_OUT; END IF;
      Prev_count := Commit_count + Prev_count;
      Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'090'
                                       , 'N'
                                       , Prev_Count
                                       , V_Message
                                       );
      Load_Supplier_Rules.Write_file( g_Log_fh
                                    , V_Message
                                    , g_Pricing_Msgs_Rec
                                    , Success
                                    );
      IF (NOT SUCCESS) THEN  GOTO GET_OUT; END IF;
      Commit_Count := 0;
    END IF;
  --}
  END LOOP;




  Pgmloc := 2740;
  Load_Supplier_Rules.Update_Prc_Msgs( P_Program_ID
                                     , g_Pricing_Msgs_Rec.Date_of_Msg
                                     , g_Pricing_Msgs_Rec
                                     , Success
                                     );
  IF (NOT SUCCESS) THEN  GOTO GET_OUT; END IF;

  Prev_count := Commit_count + Prev_count;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'090'
                                   , 'N'
                                   , Prev_Count
                                   , V_Message
                                   );
  Load_Supplier_Rules.Write_file( g_Log_fh
                                , V_Message
                                , g_Pricing_Msgs_Rec
                                , Success
                                );
  IF (NOT SUCCESS) THEN  GOTO GET_OUT; END IF;
  --
  Pgmloc := 2750;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'110'
                                   , 'N'
                                   , TO_CHAR( SYSDATE, 'dd-mon-yyyy hh24:mi:ss' )
                                   , g_Pricing_Msgs_Rec.Remarks
                                   );
  Pgmloc := 2760;
  Load_Supplier_Rules.Write_file( g_Log_fh
                                , g_Pricing_Msgs_Rec.Remarks
                                , g_Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;

  --
  -------------------------------------
  --  Closing both the files at the end
  -------------------------------------
  --mlz - 29may2014 - closing logs at real end
  -- --mlz - 17mar2014
  -- logmon_close;
  -- --endmlz - 17mar2014
  -- Pgmloc := 2770;
  -- Load_Supplier_Rules.File_Close( g_Log_fh
  --                               , g_Pricing_Msgs_Rec
  --                               , Success
  --                               );
  -- IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --endmlz - 29may2014
  --
  Pgmloc := 2780;
  UPDATE Break_Points
     SET Status = 'DONE'
       , Nb_Records = g_Pricing_Msgs_Rec.Value_01
       , Last_User = g_user --Constants.Get_User_Id
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  Pgmloc := 2790;
  UPDATE Supplier_Running_Parms
     SET Last_Run_Date = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  -- Do not update Last_Stamp/User in Supplier_Running_Parms
  -- as this is not a modification but only a status of execution.
  --
  Pgmloc := 2800;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'120'
                                   , 'N'
                                   , g_Pricing_Msgs_Rec.Msg_Text
                                   );
  Pgmloc := 2810;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'130'
                                   , 'N'
                                   , g_Pricing_Msgs_Rec.Msg_Text_2
                                   );
  g_Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  GOTO Upd_Prc_Msg;
  --
<<GET_OUT>>
  Pgmloc := 2820;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'140'
                                   , 'N'
                                   , g_Pricing_Msgs_Rec.Msg_Text
                                   );
  Pgmloc := 2830;
  Manage_Messages.Get_Oasis_Message( C_MOP_Oasis_Message||'150'
                                   , 'N'
                                   , g_Pricing_Msgs_Rec.Msg_Text_2
                                   );
  g_Pricing_Msgs_Rec.Successfull_Flag := 'N';
--
<<Upd_Prc_Msg>>
  --mlz - 29may2014 - close logs here
  logmon_close; --send email if errors occurred
  log_close;
  --endmlz - 29may2014
  
  IF cur_securities%ISOPEN
  THEN
    CLOSE cur_securities;
    End_Of_File := TRUE;
  END IF;
  --
  P_Success := Success;
  P_Message := g_Pricing_Msgs_Rec.Err_Text;
  --
  Pgmloc := 2840;
  Load_Supplier_Rules.Update_Prc_Msgs( P_Program_ID
                                     , g_Pricing_Msgs_Rec.Date_of_Msg
                                     , g_Pricing_Msgs_Rec
                                     , Success
                                     );
  Pgmloc := 2850;
  COMMIT;
--
EXCEPTION
  WHEN OTHERS
  THEN
    --mlz - 17mar2014
    l_tmp_message := get_error_msg(l_tmp_message);
    log_step( replace(l_tmp_message,chr(10),' ') );
    
    --mlz - 29may2014
    logmon_close;
    log_close;
    --endmlz - 29may2014
    
    g_Pricing_Msgs_Rec.Err_Text := substr(l_tmp_message,1,255);
    Load_Supplier_Rules.Update_Prc_Msgs( g_Program_ID
                                       , g_Pricing_Msgs_Rec.Date_of_Msg
                                       , g_Pricing_Msgs_Rec
                                       , p_Success
                                       );
    --endmlz - 17mar2014
    
    IF cur_securities%ISOPEN
    THEN
      CLOSE cur_securities;
      End_Of_File := TRUE;
    END IF;

    p_message := substr('@'||pgmloc||' '||l_tmp_message,1,255);
    P_Success := FALSE;
--
END Load_Prices;
----------------------------------------------------------------------
END LOAD_DUAL_SOURCES;
/
show error
