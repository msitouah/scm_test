CREATE OR REPLACE PACKAGE BODY &&owner..Index_Reports
AS
----------------------------------------------------------------------
-- SSI - 12-OCT-2007 - Creation
-- SSI - 30-OCT-2007 - New Procedure - Sub_Series_Value
--                   - Addition of call to AFT files.
--                   - Dispatcher procedure For_Client.
-- SSI - 26-NOV-2007 - All files are also copied to datestamped files
-- JSL - 27-nov-2007 - Addition of the possibility to only do the AFT
--                     without doing any calculation for some specific
--                     clients.  Other clients can still do the AFT.
-- SSI - 10-DEC-2007 - New report for ticker changes.
--                     New procedure Ticker_Changes.
-- ACB - 31-DEC-2007 - New Procedure Send_Compressed_Files
--                     New Procedure Create_Job
-- SSI - 08-JAN-2008 - File are now identified by product_components and
--                     driven by the table Client_File_Definitions.
--                   - On given run date, generate the period to be run
--                   - Populate the Client_Output_Files for output files
-- JSL - 11-jan-2008 - Preproduction fixes and integration.
--                     Not individually identified.
-- SSI - 15-JAN-2008 - For_Client
--                   - Dispatch by V_Mode (XTR or SEND) instead of by client
-- JSL - 16-jan-2008 - Other preproduction fixes due to the dispatching.
--                   - in send_compressed_file, v_pricing_msgs_rec is
--                     changed to the global record pricing_msg_rec.
-- JSL - 17-jan-2008 - Added drop_job at end of processing as the job has been
--                     executed and will not be required anymore.
--
-- SSI - 03-APR-2008 - Procedure Ticker_Changes
--                     - Work date for which to check for ticker change is
--                       the same as file date.
--
-- SSI - 15-MAY-2008 - New filed sub_product_code in Client_File_Definitions
--                     and Client_Output_Files.
--
-- SSI - 26-MAY-2008 - Send_Compressed_File is made public and generalized.
--
-- SSI - 12-SEP-2008 - Initialize all ICB sectors text.
--                   - Temporary fix for missing ICB supersector - 8900
--                   - DO NOT output J254 securities in Detail Benchmark
--                     file.
--
-- SSI - 17-SEP-2008 - ICB code exception for 8980 and 8990.
--
-- SSI - 29-OCT-2008 - While getting ISIN, parameters source and currency
--                     are null
--
-- SSI - 08-JAN-2009 - New procedure Gen_Included_Files: Read in new table 
--                     Client_Included_Files and generate to Client_Output_Files
--                     files to be included
--                   - For Send only clients (JSE index), add file to be
--                     included in zip file.
--
-- SSI - 16-JUN-2009 - Check if successfull after call to Send_Compressed_Files.
--
-- SSI - 17-JUN-2009 - Do not submit job through DBMS_SCHEDULER.
--                   - Remove also call to AFT.
--                   - Pricing_Msgs created and updated if run on server.
--                   - Remove Update to Last_Run_Date. It is to be taken care of
--                     by Global_File_Updates.Update_Pricing_Msgs.
-- 
-- SSI - 19-JUN-2009 - Add condition to reset rerun date only if run from server
--
-- SSI - 18-JUN-2009 - Add share outstanding in detail benchmark file.
--
-- SSI - 19-NOV-2009 - Get_SuperSector - Change exception for ICB code '898%' and '899%'.
--
-- SSI - 27-OCT-2010 - Security_Characteristic - Add currency in characteristic file.
--
-- APX - 20-JAN-2011 - Make generation of Date.dat file optional.
--
-- SSI - 25-JAN-2011 - Send_Compressed_File - Set successfull in Pricing_MSgs only when it
--                                            is a child process. Otherwise the rerun date
--                                            in Produc_running_Parms will be erased by
--                                            call to Update of Pricing_MSgs.
--
-- SSI - 25-SEP-2015 - IDX-2750 - For_Client - Remove check on V_Rerun_Date check for starting and ending date in
--                                             Break_Points. It should always be checked.
--
-- SSI - 07DEC2015   - IDX-3397 - Parameters are reversed in call to Holiday.Is_It_Open using Holiday class.
--
----------------------------------------------------------------------
--
Pgmloc             INTEGER;
C_Oasis_Message    VARCHAR2(50) := 'PROC-INDEX_REPORTS-';
C_Gen_Message      VARCHAR2(50) := 'PROC-GEN-';
C_Commit_Freq      NUMBER:= 30;
C_Date_Format      VARCHAR2(20):='DD/MM/YYYY';
C_Delim            VARCHAR2(1):=',';
C_Text_Delim       VARCHAR2(1):=NULL;
--
C_ICB_Len          NUMBER:=4;
C_Grp_Len          NUMBER:=1;
C_SubSec_Len       NUMBER:=4;
C_Sector_Len       NUMBER:=3;
C_SprSec_Len       NUMBER:=2;
C_Indtry_Len       NUMBER:=1;
--
-- SSI - 26MAY2008
C_Zip_Component    Product_Components.Component_Code%TYPE:='O';
--
G_Comp_Index_Rec   Composite_Indexes%ROWTYPE;
G_Comp_Index       Tickers.Fri_Ticker%TYPE:=NULL;
G_Site_Date        Date;
--
V_FileName         VARCHAR2(80);
--
File_Type          Utl_File.File_Type;
Out_File_Path      VARCHAR2(50):=Constants.Get_Dir_Name;
G_Dir_Seperator    VARCHAR2(1) := Constants.Get_Dir_Seperator;
--
G_Program_ID       OPS_Scripts.Program_Id%TYPE;
G_System_Date      DATE;
G_Commit_Freq      NUMBER;
Pricing_Msg_Rec    Pricing_Msgs%ROWTYPE;
--
-- SSI - 08JAN2008
TYPE File_Def_Typ IS TABLE OF Client_File_Definitions%ROWTYPE INDEX BY Product_Components.Component_Code%TYPE;
TYPE File_Names IS TABLE OF VARCHAR2(60) INDEX BY BINARY_INTEGER;
G_File_Def         File_Def_Typ;
G_File_Def_Init    File_Def_Typ;
G_Comp             Product_Components.Component_Code%TYPE;
G_Out_Files        File_Names;
G_Init_Files       File_Names;
G_Num_Del          NUMBER;
G_Num_Files        NUMBER;
G_User             VARCHAR2(4):=Constants.Get_User_ID;
--
-- SSI - 30OCT2008
TYPE t_ICB_Rec IS RECORD (Code       Category_Classes.Class_Code%TYPE
                         ,Text       Category_Classes.Text_E%TYPE);
TYPE t_ICB_Tab IS TABLE OF t_ICB_Rec INDEX BY BINARY_INTEGER;
--
G_ICB_Code         Category_Classes.Class_Code%TYPE;
G_ICB_Tab          t_ICB_Tab;
G_Loading          BOOLEAN:=TRUE;
-- SSI - 22JAN2009
TYPE t_Flag IS TABLE OF VARCHAR2(1) INDEX BY BINARY_INTEGER;
G_Send_Files       t_Flag;
-- SSI 17JUN2009
G_On_Server           BOOLEAN     := Constants.On_Server;
--------------------------------------------------------------------------------
PROCEDURE Detail_Benchmark
        ( P_Composite_index    IN     Tickers.Fri_Ticker%TYPE
        , P_Data_Supplier_Code IN     Data_Suppliers.Code%TYPE
        , P_Del_Time           IN     VARCHAR2
        , P_From_Date          IN     DATE
        , P_To_Date            IN     DATE
        , P_Date_Format        IN     VARCHAR2
        , P_Filename           IN     VARCHAR2
        , P_Copy_Filename      IN     VARCHAR2
        , P_Success               OUT BOOLEAN
        , P_Message               OUT VARCHAR2
        ) IS --{
--
-- Report for current day load records on current day structure
-- Should not include any record added from tracker file the day after for calc.
-- Note these are not in the structure for the day yet
--
    V_Date            DATE;
    W_Date            DATE;
    V_FileName        VARCHAR2(80);
    V_Copy_FileName   VARCHAR2(80);
    V_Formatted_Date  VARCHAR2(16);
    V_Holiday         BOOLEAN;
    V_Delivery_Date   Date;
    V_Success         BOOLEAN;
    V_Message         VARCHAR2(512);
--
    CURSOR Cur1 IS
     select Security.get_sec_id(I.composite_index_fri_Ticker,I.source_Code,I.Price_Currency,'E') Series_Code
      ,REPLACE(REPLACE(security.get_sec_name(I.Composite_Index_fri_ticker,120,'S2'),C_Delim,' '),'  ',' ') Series_Name
      ,I.price_date
      ,security.get_hist_sec_id(I.Fri_Ticker,I.source_code,I.price_currency,'A',W_Date) CONS_CODE
      ,security.get_hist_sec_id(I.Fri_Ticker,I.source_code,NULL,'E',W_Date) ALPHA_CODE
-- SSI - 29OCT2008
--    ,NVL(security.get_hist_sec_id(I.Fri_Ticker,I.source_code,I.price_currency,'O',W_Date)
--        ,security.get_hist_sec_id(I.Fri_Ticker,I.source_code,I.price_currency,'Z',W_Date)) ISIN
      ,NVL(security.get_hist_sec_id(I.Fri_Ticker,NULL,NULL,'O',W_Date)
          ,security.get_hist_sec_id(I.Fri_Ticker,NULL,NULL,'Z',W_Date)) ISIN
      ,I.market_capitalization_Adj
      ,I.weight_factor
      ,I.Price
      ,I.Security_Return
      ,REPLACE(REPLACE(security.get_sec_name(I.fri_ticker,120,'S2'),C_Delim,' '),'  ',' ') security_name
      ,C.ISO_Currency
-- SSI - 18SEP2009
      , I.OUTSTANDING_SHARES      
from index_perf_items I
    ,Currencies C
    ,Composite_Index_Structures S
where S.Composite_index_fri_Ticker=NVL(P_Composite_index
                                      ,S.Composite_Index_Fri_Ticker)
and   S.Fri_Ticker_Parent IS NOT NULL
and   W_Date BETWEEN S.Effective_Date AND NVL(S.Effective_End_Date,W_Date)
and   I.Composite_index_fri_Ticker = S.Composite_index_fri_Ticker
and   I.Fri_Ticker = S.Fri_Ticker
and   I.Data_Supplier_Code = P_Data_Supplier_Code
and   I.delivery_date = V_Delivery_Date
and   C.Currency = I.Price_Currency
order by 3,1,4;
--
Out_Rec   CUR1%ROWTYPE;
V_Files   NUMBER;
--
BEGIN
    --{
    pgmloc := 1000;
    V_Success := FALSE;
    V_Message := NULL;
    P_Success := FALSE;
    P_Message := '';
    V_FileName := NULL;
    G_Site_Date := Constants.Get_Site_date;
-- SSI - 08JAN2008
    G_Out_Files := G_Init_Files;
    V_Files     := 0;
--
    V_Date := P_From_Date;
    W_Date := Holiday.Get_Actual_Last_Open_Date( V_Date
                                               , 'SUPPLIER'
                                               , P_Data_Supplier_Code
                                               , P_Del_Time );
    WHILE V_Date <= P_To_Date AND
          V_Date <= G_Site_Date
    LOOP --{
       pgmloc := 1010;
-- If weekend or a holiday
-- Update my work date
       IF TO_CHAR( V_Date, 'D' ) IN ( 1, 7 ) OR
          Holiday.Is_It_Open( V_Date
                            , 'SUPPLIER'
                            , P_Data_Supplier_Code
                            , P_Del_Time ) IS NOT NULL
       THEN
           V_Holiday := TRUE;
       ELSE
           W_Date := V_Date;
           V_Holiday := FALSE;
       END IF;
       pgmloc := 1020;
       V_Delivery_Date := TO_DATE(TO_CHAR(W_Date,P_Date_Format)||P_Del_Time,
                                  P_Date_Format || 'HH24MI');
       dbms_output.put_line( V_Date );
       V_Formatted_Date := TO_CHAR( V_Date, P_Date_Format );
       V_Filename := P_FileName;
-- SSI - 08JAN2008
--     IF INSTR( P_FileName, '%%%%%' ) > 0
--     THEN
--        V_FileName := REPLACE( P_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
--     IF INSTR( P_Copy_FileName, '%%%%%' ) > 0
--     THEN
--        V_Copy_FileName := REPLACE( P_Copy_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
--
       V_Copy_FileName := Manage_Messages.Replace_Text(P_Copy_Filename,V_Formatted_Date);
-- SSI - 08JAN2008 - End modif.
       pgmloc := 1030;
       File_Type   := Utl_File.Fopen(Out_File_Path, V_FileName, 'W');
       dbms_output.put_line(V_Date || ' Generating Report :' || V_FileName);
       pgmloc := 1040;
       Utl_File.Put_Line(File_Type,
                         V_Formatted_Date);
       Utl_File.Put_Line(File_Type,
                         'Series_Code' || C_Delim ||
                         'Series_Name' || C_Delim ||
                         'Ending_Date' || C_Delim ||
                         'CONS_CODE'     || C_Delim ||
                         'ALPHA_CODE'     || C_Delim ||
                         'ISIN'     || C_Delim ||
                         'Market_Capitalization'           || C_Delim ||
                         'Weight'           || C_Delim ||
                         'Price'            || C_Delim ||
                         'Security_Return'         || C_Delim ||
                         'Security_name'                || C_Delim ||
-- SSI - 18SEP2009
--                         'ISO_Currency' );
                         'ISO_Currency'     || C_Delim ||
                         'Outstanding_Shares'
                         );
       Utl_File.Put_Line(File_Type,
                         'JSE'     || C_Delim ||
                         'StatPro' || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro' || C_Delim ||
                         'StatPro' || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro' || C_Delim ||
                         'StatPro' || C_Delim ||
-- SSI - 18SEP2009
                         'JSE'     || C_Delim ||
                         'JSE');

       FOR Out_Rec IN Cur1
       LOOP
          pgmloc := 1050;
          IF V_Holiday
          THEN
             Out_Rec.Security_Return := 0;
          END IF;
-- SSI - 16SEP2008
          IF Out_Rec.Series_Code<>'J254'
          THEN
          Utl_File.Put_Line(File_Type,
                            Out_Rec.Series_Code || C_Delim ||
                            C_Text_Delim ||
                            Out_Rec.Series_Name ||
                            C_Text_Delim ||  C_Delim ||
                            TO_CHAR(V_Date,C_Date_Format) || C_Delim ||
                            Out_Rec.CONS_CODE     || C_Delim ||
                            Out_Rec.ALPHA_CODE     || C_Delim ||
                            Out_Rec.ISIN     || C_Delim ||
                            NVL(Out_Rec.market_capitalization_Adj,0)  || C_Delim ||
                            NVL(Out_Rec.weight_factor,0)           || C_Delim ||
                            NVL(Out_Rec.Price,0)           || C_Delim ||
                            NVL(Out_Rec.Security_Return,0)         || C_Delim ||
                            C_Text_Delim ||
                            Out_Rec.Security_name ||
                            C_Text_Delim ||  C_Delim ||
-- SSI - 18SEP2009                            
--                            Out_Rec.ISO_Currency );
                            Out_Rec.ISO_Currency  || C_Delim ||
                            Out_Rec.OUTSTANDING_SHARES
                            );  
          END IF;  -- not J254
       END LOOP;   -- Each output record
       Utl_File.Fclose(File_Type);
       IF V_Copy_FileName IS NOT NULL
       THEN
          Pgmloc := 1060;
          Manage_Utl_File.Copy_File ( Out_File_Path
                                     ,V_FileName
                                     ,Out_File_Path
                                     ,V_Copy_FileName
                                     ,1
                                     ,NULL
                                     ,V_Success
                                     ,V_Message);
          IF ( NOT V_SUCCESS )
          THEN
            GOTO GET_OUT;
          END IF;
          V_Files := V_Files + 1;
          G_Out_Files(V_Files) := V_Copy_FileName;
       END IF;
--
<<Next_Date>>
      pgmloc := 1070;
      V_Date := V_Date + 1;
    END LOOP; --} WHILE TRUE (on date)
-- SSI - 26NOV2007
--  P_Success := TRUE;
    V_Success := TRUE;
<<get_Out>>
-- 26NOV2007
    P_Success := V_Success;
    P_Message := V_Message;
-- SSI  - 08JAN2008
    G_Num_Files := G_Num_Files + V_Files;
    NULL;
    --
EXCEPTION
   WHEN OTHERS
   THEN
     P_Message := pgmloc || ' ' ||SUBSTR(SQLERRM(SQLCODE),1,255);
--
END Detail_Benchmark;
------------------------------------------------------------------------
PROCEDURE Complete_Detail_Benchmark
        ( P_Composite_index    IN     Tickers.Fri_Ticker%TYPE
        , P_Data_Supplier_Code IN     Data_Suppliers.Code%TYPE
        , P_Del_Time           IN     VARCHAR2
        , P_From_Date          IN     DATE
        , P_To_Date            IN     DATE
        , P_Date_Format        IN     VARCHAR2
        , P_Filename           IN     VARCHAR2
        , P_Copy_Filename      IN     VARCHAR2
        , P_Success               OUT BOOLEAN
        , P_Message               OUT VARCHAR2
        ) IS --{
--
    V_Date            DATE;
    W_Date            DATE;
    V_FileName        VARCHAR2(80);
    V_Holiday         BOOLEAN;
    V_Delivery_Date   DATE;
-- SSI - 26NOV2007
    V_Copy_FileName   VARCHAR2(80);
    V_Formatted_Date  VARCHAR2(16);
    V_Success         BOOLEAN;
    V_Message         VARCHAR2(512);
--
-- Note here we don't join with the composite index structures as there would
-- have new securities added in next day
    CURSOR Cur1 IS
select Security.get_sec_id(I.composite_index_fri_Ticker,I.source_Code,I.Price_Currency,'E') Series
      ,REPLACE(REPLACE(security.get_sec_name(I.Composite_Index_fri_ticker,120,'S2'),C_Delim,' '),'  ',' ') series_name
      ,I.price_date
      ,security.get_hist_sec_id(I.Fri_Ticker,I.source_code,I.price_currency,'A',W_Date) CONS_CODE
      ,security.get_hist_sec_id(I.Fri_Ticker,I.source_code,NULL,'E',W_Date) Alpha_Code
-- SSI - 29OCT2008
--    ,NVL(security.get_hist_sec_id(I.Fri_Ticker,I.source_code,I.price_currency,'O',W_Date)
--        ,security.get_hist_sec_id(I.Fri_Ticker,I.source_code,I.price_currency,'Z',W_Date)) ISIN
      ,NVL(security.get_hist_sec_id(I.Fri_Ticker,NULL,NULL,'O',W_Date)
          ,security.get_hist_sec_id(I.Fri_Ticker,NULL,NULL,'Z',W_Date)) ISIN
      ,market_capitalization_Adj
      ,market_capitalization_Adj_Comp
      ,weight_factor
      ,Computed_Weight
      ,Price
      ,Security_Return
--    ,REPLACE(REPLACE(SUBSTR(security.get_sec_name(I.fri_ticker,30,'S2'),1,30),C_Delim,' '),'  ',' ') sec_name
      ,REPLACE(REPLACE(security.get_sec_name(I.fri_ticker,120,'S2'),C_Delim,' '),'  ',' ') sec_name
      ,C.ISO_Currency
-- SSI - 18SEP2009
      , I.OUTSTANDING_SHARES
from index_perf_items I
    ,Currencies C
where Composite_index_fri_Ticker=NVL(P_Composite_index
                                    ,I.Composite_Index_Fri_Ticker)
and   I.fri_ticker <> I.composite_index_fri_ticker
and   I.Data_Supplier_Code = 'FTS'
and   I.delivery_date = V_Delivery_Date
and   C.Currency = I.Price_Currency
order by 3,1,4;
--
Out_Rec   CUR1%ROWTYPE;
V_Files   NUMBER;
--
BEGIN
    --{
    pgmloc := 1080;
    P_Success := FALSE;
    P_Message := '';
    V_Success := FALSE;
    V_Message := '';
    V_FileName := NULL;
    G_Site_Date := Constants.Get_Site_date;
-- SSI - 08JAN2008
    G_Out_Files := G_Init_Files;
    V_Files     := 0;
    --
    V_Date := P_From_Date;
--  Latest open date
    W_Date := Holiday.Get_Actual_Last_Open_Date( V_Date
                                               , 'SUPPLIER'
                                               , P_Data_Supplier_Code
                                               , P_Del_Time );
--
-- SSI - 08JAN2008
-- The period to extract is always the from last open date to end_date-1
--  IF P_From_Date = G_Site_Date
--  THEN
--     V_Date := W_Date;
--  END IF;
--
    V_Date := W_Date;
--  WHILE V_Date <= P_To_Date AND
    WHILE V_Date < P_To_Date AND
          V_Date < G_Site_Date
    LOOP --{
       pgmloc := 1090;
-- If weekend or a holiday
-- Update my work date
       IF TO_CHAR( V_Date, 'D' ) IN ( 1, 7 ) OR
          Holiday.Is_It_Open( V_Date
                            , 'SUPPLIER'
                            , P_Data_Supplier_Code
                            , P_Del_Time ) IS NOT NULL
       THEN
           V_Holiday := TRUE;
       ELSE
           W_Date := V_Date;
           V_Holiday := FALSE;
       END IF;
       pgmloc := 1100;
       dbms_output.put_line( V_Date );
       V_Delivery_Date := TO_DATE(TO_CHAR(W_Date,P_Date_Format)||P_Del_Time,
                                  P_Date_Format || 'HH24MI');
       V_Formatted_Date := TO_CHAR( V_Date, P_Date_Format );
       V_Filename := P_FileName;
-- SSI - 08JAN2008
--     IF INSTR( P_FileName, '%%%%%' ) > 0
--     THEN
--        V_FileName := REPLACE( P_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
--     IF INSTR( P_Copy_FileName, '%%%%%' ) > 0
--     THEN
--        V_Copy_FileName := REPLACE( P_Copy_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
       V_Copy_FileName := Manage_Messages.Replace_Text(P_Copy_Filename,V_Formatted_Date);
-- SSI - 08JAN2008 - End modif.
       pgmloc := 1110;
       File_Type   := Utl_File.Fopen(Out_File_Path, V_FileName, 'W');
       dbms_output.put_line(V_Date || ' Generating Report :' || V_FileName);
       pgmloc := 1120;
       Utl_File.Put_Line(File_Type,
                         V_Formatted_Date);
       Utl_File.Put_Line(File_Type,
                         'Series' || C_Delim ||
                         'Series_Name' || C_Delim ||
                         'Ending_Date' || C_Delim ||
                         'CONS_CODE'     || C_Delim ||
                         'ALPHA_CODE'     || C_Delim ||
                         'ISIN'     || C_Delim ||
                         'Market_Capitalization'           || C_Delim ||
                         'Market_Capitalization_StatPro'           || C_Delim ||
                         'Weight'           || C_Delim ||
                         'Weight_StatPro'           || C_Delim ||
                         'Price'           || C_Delim ||
                         'Security_Return'         || C_Delim ||
                         'Security_name'                || C_Delim ||
-- SSI - 18SEP2009
--                         'ISO_Currency' );
                         'ISO_Currency'     || C_Delim ||
                         'Outstanding_Shares'
                         );
       Utl_File.Put_Line(File_Type,
                         'JSE'     || C_Delim ||
                         'StatPro' || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro' || C_Delim ||
                         'StatPro' || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro' || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro' || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro' || C_Delim ||
                         'StatPro' || C_Delim ||
-- SSI - 18SEP2009
                         'JSE'     || C_Delim ||
                         'JSE');
       FOR Out_Rec IN Cur1
       LOOP
          pgmloc := 1130;
          IF V_Holiday
          THEN
             Out_Rec.Security_Return := 0;
          END IF;
          pgmloc := 1140;
          Utl_File.Put_Line(File_Type,
                            Out_Rec.Series || C_Delim ||
                            C_Text_Delim ||
                            Out_Rec.Series_Name ||
                            C_Text_Delim ||  C_Delim ||
                            TO_CHAR(V_Date,C_Date_Format) || C_Delim ||
                            Out_Rec.CONS_Code     || C_Delim ||
                            Out_Rec.ALPHA_CODE     || C_Delim ||
                            Out_Rec.ISIN     || C_Delim ||
                            NVL(Out_Rec.market_capitalization_Adj,0) || C_Delim ||
                            NVL(Out_Rec.market_capitalization_Adj_Comp,0) || C_Delim ||
                            NVL(Out_Rec.weight_factor,0)           || C_Delim ||
                            NVL(Out_Rec.Computed_Weight,0)             || C_Delim ||
                            NVL(Out_Rec.Price,0)           || C_Delim ||
                            NVL(Out_Rec.Security_Return,0)         || C_Delim ||
                            C_Text_Delim ||
                            Out_Rec.Sec_name ||
                            C_Text_Delim ||  C_Delim ||
-- SSI - 18SEP2009                            
--                          Out_Rec.ISO_Currency );
                            Out_Rec.ISO_Currency  || C_Delim ||
                            Out_Rec.OUTSTANDING_SHARES
                            );                              
       END LOOP;   -- Each output record
       Utl_File.Fclose(File_Type);
       IF V_Copy_FileName IS NOT NULL
       THEN
          Pgmloc := 1150;
          Manage_Utl_File.Copy_File ( Out_File_Path
                                     ,V_FileName
                                     ,Out_File_Path
                                     ,V_Copy_FileName
                                     ,1
                                     ,NULL
                                     ,V_Success
                                     ,V_Message);
          IF ( NOT V_SUCCESS )
          THEN
            GOTO GET_OUT;
          END IF;
          V_Files := V_Files + 1;
          G_Out_Files(V_Files) := V_Copy_FileName;
       END IF;
--
<<Next_Date>>
      pgmloc := 1160;
      V_Date := V_Date + 1;
    END LOOP; --} WHILE TRUE (on date)
-- SSI 26NOV2007
--  P_Success := TRUE;
    V_Success := TRUE;
<<get_Out>>
-- SSI 26NOV2007
    P_Success := V_Success;
    P_Message := V_Message;
-- SSI  - 08JAN2008
    G_Num_Files := G_Num_Files + V_Files;
    NULL;
    --
EXCEPTION
   WHEN OTHERS
   THEN
     P_Message := pgmloc || ' ' ||SUBSTR(SQLERRM(SQLCODE),1,255);
--
END Complete_Detail_Benchmark;
------------------------------------------------------------------------
PROCEDURE Load_ICB_Tab
        ( P_ICB_Code           IN     VARCHAR2
        , P_Message               OUT VARCHAR2
        , P_Success               OUT BOOLEAN
        )
IS
BEGIN
   pgmloc := 1170;
   P_Success := FALSE;
   P_Message := NULL;
   G_Loading := TRUE;
   pgmloc := 1180;
-- SubSector
   G_ICB_Tab(1).Code := Get_SubSector(P_ICB_Code);
   G_ICB_Tab(1).Text := Get_ICB_Text(G_ICB_Tab(1).Code);
-- Sector
   pgmloc := 1190;
   G_ICB_Tab(2).Code := Get_Sector(P_ICB_Code);
   G_ICB_Tab(2).Text := Get_ICB_Text(G_ICB_Tab(2).Code);
-- SuperSector
   pgmloc := 1200;
   G_ICB_Tab(3).Code := Get_SuperSector(P_ICB_Code);
   G_ICB_Tab(3).Text := Get_ICB_Text(G_ICB_Tab(3).Code);
-- Industry
   pgmloc := 1210;
   G_ICB_Tab(4).Code := Get_Industry(P_ICB_Code);
   G_ICB_Tab(4).Text := Get_ICB_Text(G_ICB_Tab(4).Code);
--
   G_ICB_Code:= P_ICB_Code;
--
   G_Loading := FALSE;
   P_Success := TRUE;
EXCEPTION
   WHEN OTHERS
   THEN
     P_Message := pgmloc || ' ' ||SUBSTR(SQLERRM(SQLCODE),1,255);
END Load_ICB_Tab;
--------------------------------------------------------------------------------
Function Get_SubSector (P_ICB_Code VARCHAR2)
RETURN VARCHAR2 IS
   V_SubSector_Code         VARCHAR2(12);
   V_ICB_Code               VARCHAR2(12);
   V_Message                VARCHAR2(512);
   V_Success                BOOLEAN;
BEGIN
   pgmloc := 1220;
   IF P_ICB_Code IS NULL
   THEN
      RETURN NULL;
   ELSIF G_Loading
   THEN
-- This is actually the whole ICB code
     pgmloc := 1230;
     V_ICB_Code := LPAD(TRIM(P_ICB_Code),C_ICB_Len,'0');
     V_SubSector_Code := V_ICB_Code;
     RETURN V_SubSector_Code;
-- If same as last ICB code stored
   ELSIF G_ICB_Code = P_ICB_Code
   THEN
      pgmloc := 1240;
      RETURN G_ICB_Tab(1).Code;
   ELSE 
      pgmloc := 1250;
      Load_ICB_Tab(P_ICB_Code
                  ,V_Message
                  ,V_Success);
      IF V_Success
      THEN
         RETURN G_ICB_Tab(1).Code;
      ELSE
         RETURN NULL;
      END IF;
   END IF;
/*
-- This is actually the whole ICB code
   V_ICB_Code := LPAD(TRIM(P_ICB_Code),C_ICB_Len,'0');
   V_SubSector_Code := V_ICB_Code;
   RETURN V_SubSector_Code;
*/
END Get_Subsector;
--------------------------------------------------------------------------------
Function Get_Sector (P_ICB_Code VARCHAR2)
RETURN VARCHAR2 IS
   V_Sector_Code            VARCHAR2(12);
   V_ICB_Code               VARCHAR2(12);
   V_Message                VARCHAR2(512);
   V_Success                BOOLEAN;
BEGIN
-- V_ICB_Code := LPAD(TRIM(P_ICB_Code),C_ICB_Len,'0');
-- V_Sector_Code := RPAD(SUBSTR(V_ICB_Code,1,C_Sector_Len),C_ICB_Len,'0');
-- RETURN V_Sector_Code;
   pgmloc := 1260;
   IF P_ICB_Code IS NULL
   THEN
      RETURN NULL;
   ELSIF G_Loading
   THEN
-- This is actually the whole ICB code
     pgmloc := 1270;
     V_ICB_Code := LPAD(TRIM(P_ICB_Code),C_ICB_Len,'0');
     V_Sector_Code := RPAD(SUBSTR(V_ICB_Code,1,C_Sector_Len),C_ICB_Len,'0');
     RETURN V_Sector_Code;
-- If same as last ICB code stored
   ELSIF G_ICB_Code = P_ICB_Code
   THEN
      pgmloc := 1280;
      RETURN G_ICB_Tab(2).Code;
   ELSE 
      pgmloc := 1290;
      Load_ICB_Tab(P_ICB_Code
                  ,V_Message
                  ,V_Success);
      IF V_Success
      THEN
         RETURN G_ICB_Tab(2).Code;
      ELSE
         RETURN NULL;
      END IF;
   END IF;
END Get_Sector;
--------------------------------------------------------------------------------
Function Get_SuperSector (P_ICB_Code VARCHAR2)
RETURN VARCHAR2 IS
   V_SuperSector_Code       VARCHAR2(12);
   V_ICB_Code               VARCHAR2(12);
   V_Message                VARCHAR2(512);
   V_Success                BOOLEAN;
BEGIN
-- SSI - 30OCT2008
-- V_ICB_Code := LPAD(TRIM(P_ICB_Code),C_ICB_Len,'0');
-- V_SuperSector_Code := RPAD(SUBSTR(V_ICB_Code,1,C_SprSec_Len),C_ICB_Len,'0');
-- RETURN V_SuperSector_Code;
--
   IF P_ICB_Code IS NULL
   THEN
      RETURN NULL;
   ELSIF G_Loading
   THEN
      V_ICB_Code := LPAD(TRIM(P_ICB_Code),C_ICB_Len,'0');
      pgmloc := 1300;
-- SSI - 19NOV2009
--    IF V_ICB_Code LIKE '898%'
      IF V_ICB_Code LIKE '89%'
      THEN
         V_SuperSector_Code := '8700';
-- SSI - 19NOV2009
--    ELSIF V_ICB_Code LIKE '899%'
--    THEN
--       V_SuperSector_Code := NULL;
      ELSE
         V_SuperSector_Code := RPAD(SUBSTR(V_ICB_Code,1,C_SprSec_Len),C_ICB_Len,'0');
      END IF;
      RETURN V_SuperSector_Code;
-- If sam as last ICB code stored
   ELSIF G_ICB_Code = P_ICB_Code
   THEN
      pgmloc := 1310;
      RETURN G_ICB_Tab(3).Code;
   ELSE 
      pgmloc := 1320;
      Load_ICB_Tab(P_ICB_Code
                  ,V_Message
                  ,V_Success);
      IF V_Success
      THEN
         RETURN G_ICB_Tab(3).Code;
      ELSE
         RETURN NULL;
      END IF;
   END IF;
END Get_SuperSector;
--------------------------------------------------------------------------------
Function Get_Industry (P_ICB_Code VARCHAR2)
RETURN VARCHAR2 IS
   V_Industry_Code          VARCHAR2(12);
   V_ICB_Code               VARCHAR2(12);
   V_Message                VARCHAR2(512);
   V_Success                BOOLEAN;
BEGIN
-- SSI 30OCT2008
-- V_ICB_Code := LPAD(TRIM(P_ICB_Code),C_ICB_Len,'0');
-- V_Industry_Code := RPAD(SUBSTR(V_ICB_Code,1,C_Indtry_Len),C_ICB_Len,'0');
-- IF V_Industry_Code = '0000'
-- THEN
--    V_Industry_Code := '0001';
-- END IF;
-- RETURN V_Industry_Code;
--
   IF P_ICB_Code IS NULL
   THEN
      RETURN NULL;
   ELSIF G_Loading
   THEN
      pgmloc := 1330;
      V_ICB_Code := LPAD(TRIM(P_ICB_Code),C_ICB_Len,'0');
      V_Industry_Code := RPAD(SUBSTR(V_ICB_Code,1,C_Indtry_Len),C_ICB_Len,'0');
      IF V_Industry_Code = '0000'
      THEN
         V_Industry_Code := '0001';
-- Exeption
      ELSIF V_ICB_Code LIKE '899%'
      THEN
         V_Industry_Code := NULL;
      END IF;
      pgmloc := 1340;
      RETURN V_Industry_Code;
-- If same as last ICB code stored
   ELSIF G_ICB_Code = P_ICB_Code
   THEN
      pgmloc := 1350;
      RETURN G_ICB_Tab(4).Code;
   ELSE 
      pgmloc := 1360;
      Load_ICB_Tab(P_ICB_Code
                  ,V_Message
                  ,V_Success);
      IF V_Success
      THEN
         RETURN G_ICB_Tab(4).Code;
      ELSE
         RETURN NULL;
      END IF;
   END IF;
END Get_Industry;
--------------------------------------------------------------------------------
Function Get_ICB_Text (P_ICB_SubCode VARCHAR2)
RETURN VARCHAR2 IS
   V_Text      VARCHAR2(100);
--
   CURSOR Get_Class_Text(I_Class_Code VARCHAR2) IS
   SELECT Text_E
   FROM   Category_Classes
   WHERE  Category_Code = 'ICB'
   and    Class_Code = I_Class_Code;
BEGIN
-- If same as last ICB code stored
   IF P_ICB_SubCode IS NULL
   THEN
      RETURN NULL;
   ELSIF NVL(G_ICB_Tab.Count,0) > 0 THEN
      IF G_ICB_Tab(1).Code = P_ICB_SubCode
      THEN
         pgmloc := 1370;
         RETURN G_ICB_Tab(1).Text;
      ELSIF G_ICB_Tab(2).Code = P_ICB_SubCode
      THEN
         pgmloc := 1380;
--       dbms_output.put_line('Get level 1');
         RETURN G_ICB_Tab(2).Text;
--       dbms_output.put_line('Get level 2');
      ELSIF G_ICB_Tab(3).Code = P_ICB_SubCode
      THEN
         pgmloc := 1390;
         RETURN G_ICB_Tab(3).Text;
--       dbms_output.put_line('Get level 3');
      ELSIF G_ICB_Tab(4).Code = P_ICB_SubCode
      THEN
         pgmloc := 1400;
--       dbms_output.put_line('Get level 4');
         RETURN G_ICB_Tab(4).Text;
      END IF;
   ELSE 
-- Initialize text for ICB sectors
--    dbms_output.put_line('othr');
      V_Text := NULL;
--
      pgmloc := 1410;
      OPEN Get_Class_Text(P_ICB_SubCode);
      pgmloc := 1420;
      FETCH Get_Class_Text INTO V_Text;
      pgmloc := 1430;
      CLOSE Get_Class_Text;
--
      RETURN V_Text;
   END IF;
END Get_ICB_Text;
--------------------------------------------------------------------------------
PROCEDURE Security_Characteristic
        ( P_Composite_index    IN     Tickers.Fri_Ticker%TYPE
        , P_Data_Supplier_Code IN     Data_Suppliers.Code%TYPE
        , P_Del_Time           IN     VARCHAR2
        , P_From_Date          IN     DATE
        , P_To_Date            IN     DATE
        , P_Date_Format        IN     VARCHAR2
        , P_Filename           IN     VARCHAR2
        , P_Copy_Filename      IN     VARCHAR2
        , P_Success               OUT BOOLEAN
        , P_Message               OUT VARCHAR2
        ) IS --{
--
-- Report for current day load records on current day structure
-- Should not include any record added from tracker file the day after for calc.
-- Note these are not in the structure for the day yet
--
    V_Date            DATE;
    W_Date            DATE;
    V_FileName        VARCHAR2(80);
    V_Copy_FileName   VARCHAR2(80);
    V_Formatted_Date  VARCHAR2(16);
    V_Holiday         BOOLEAN;
    V_Delivery_Date   DATE;
    V_Success         BOOLEAN;
    V_Message         VARCHAR2(512);
--
    V_SubSec          VARCHAR2(4);
    V_Sector          VARCHAR2(4);
    V_SprSec          VARCHAR2(4);
    V_Indtry          VARCHAR2(4);
    V_SubSec_Text     Category_Classes.Text_E%TYPE;
    V_Sector_Text     Category_Classes.Text_E%TYPE;
    V_SprSec_Text     Category_Classes.Text_E%TYPE;
    V_Indtry_Text     Category_Classes.Text_E%TYPE;
--
    V_ICB_Code        VARCHAR2(12);
    V_Prev_ICB_Code   VARCHAR2(12);
--
    CURSOR Cur1 IS
-- SSI 29OCT2007
--   select Security.get_sec_id(S.composite_index_fri_Ticker,C.source_Code,C.Price_Currency,'E') Series_Code
--    ,security.get_sec_name(S.Composite_Index_fri_ticker,60,'S2') Series_Name
--    ,security.get_hist_sec_id(S.Fri_Ticker,C.source_code,C.price_currency,'A',V_Date) CONS_CODE
     Select Distinct
       security.get_hist_sec_id(S.Fri_Ticker,C.source_code,C.price_currency,'A',V_Date) CONS_CODE
      ,security.get_hist_sec_id(S.Fri_Ticker,C.source_code,NULL,'E',V_Date) ALPHA_CODE
-- SSI - 29OCT2008
--    ,NVL(security.get_hist_sec_id(S.Fri_Ticker,C.source_code,C.price_currency,'O',V_Date)
--        ,security.get_hist_sec_id(S.Fri_Ticker,C.source_code,C.price_currency,'Z',V_Date)) ISIN
      ,NVL(security.get_hist_sec_id(S.Fri_Ticker,NULL,NULL,'O',V_Date)
          ,security.get_hist_sec_id(S.Fri_Ticker,NULL,NULL,'Z',V_Date)) ISIN
      ,REPLACE(REPLACE(security.get_sec_name(S.fri_ticker,120,'S2'),C_Delim,' '),'  ',' ') sec_name
      ,S.GICS_Code  ICB_Code
-- SSI 27OCT2010
     ,Cur.ISO_Currency
from Composite_Indexes C
    ,Composite_Index_Structures S
    ,Index_Perf_Items I
-- SSI 27OCT2010
    ,Currencies Cur
where S.Composite_index_fri_Ticker=NVL(P_Composite_index
                                      ,S.Composite_Index_Fri_Ticker)
and   S.Fri_Ticker_Parent IS NOT NULL
and   W_Date BETWEEN S.Effective_Date AND NVL(S.Effective_End_Date,W_Date)
and   C.fri_Ticker = S.Composite_index_fri_Ticker
and   I.Composite_index_fri_Ticker = S.Composite_index_fri_Ticker
and   I.Fri_Ticker = S.Fri_Ticker
and   I.Data_Supplier_Code = P_Data_Supplier_Code
and   I.delivery_date = V_Delivery_Date
-- SSI 27OCT2010
and   Cur.Currency = I.Price_Currency
-- SSI 29OCT2007
-- order by 1,5;  -- by series and ICB code
order by 2;  -- Order by alpha code
--
Out_Rec   CUR1%ROWTYPE;
V_Files   NUMBER;
--
-- SSI - 30OCT2008
-- Moved to new function Get_ICB_Text
CURSOR Get_Class_Text(I_Class_Code VARCHAR2) IS
SELECT Text_E
FROM   Category_Classes
WHERE  Category_Code = 'ICB'
and    Class_Code = I_Class_Code;
--
BEGIN
    --{
    pgmloc := 1440;
    P_Success := FALSE;
    P_Message := '';
    V_Success := FALSE;
    V_Message := '';
    V_FileName := NULL;
    G_Site_Date := Constants.Get_Site_Date;
-- SSI - 08JAN2008
    G_Out_Files := G_Init_Files;
    V_Files     := 0;
    --
    V_Date := P_From_Date;
--  Latest open date
    W_Date := Holiday.Get_Actual_Last_Open_Date( V_Date
                                               , 'SUPPLIER'
                                               , P_Data_Supplier_Code
                                               , P_Del_Time );
    V_Prev_ICB_Code := NULL;
    WHILE V_Date <= P_To_date AND
          V_Date <= G_Site_date
    LOOP --{
       pgmloc := 1450;
-- If weekend or a holiday
-- Update my work date
       IF TO_CHAR( V_Date, 'D' ) IN ( 1, 7 ) OR
          Holiday.Is_It_Open( V_Date
                            , 'SUPPLIER'
                            , P_Data_Supplier_Code
                            , P_Del_Time ) IS NOT NULL
       THEN
           V_Holiday := TRUE;
       ELSE
           W_Date := V_Date;
           V_Holiday := FALSE;
       END IF;
       dbms_output.put_line( V_Date );
       V_Delivery_Date := TO_DATE(TO_CHAR(W_Date,P_Date_Format)||P_Del_Time,
                                  P_Date_Format || 'HH24MI');
       V_Formatted_Date := TO_CHAR( V_Date, P_Date_Format );
       V_Filename := P_FileName;
-- SSI - 08JAN2008
--     IF INSTR( P_FileName, '%%%%%' ) > 0
--     THEN
--        V_FileName := REPLACE( P_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
--     IF INSTR( P_Copy_FileName, '%%%%%' ) > 0
--     THEN
--        V_Copy_FileName := REPLACE( P_Copy_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
       V_Copy_FileName := Manage_Messages.Replace_Text(P_Copy_Filename,V_Formatted_Date);
-- SSI - 08JAN2008 - End modif.
       pgmloc := 1460;
       File_Type   := Utl_File.Fopen(Out_File_Path, V_FileName, 'W');
       dbms_output.put_line(V_Date || ' Generating Report :' || V_FileName);
       pgmloc := 1470;
       Utl_File.Put_Line(File_Type,
                         V_Formatted_Date);
       Utl_File.Put_Line(File_Type,
--                       'Series_Code' || C_Delim ||
--                       'Series_Name' || C_Delim ||
                         'Ending_Date' || C_Delim ||
                         'CONS_CODE'     || C_Delim ||
                         'ALPHA_CODE'     || C_Delim ||
                         'ISIN'     || C_Delim ||
--                   -- SSI - 07NOV2007 - new field
                         'Security_name'       || C_Delim ||
                         'JSE Subsector ID'    || C_Delim ||
                         'JSE Subsector'       || C_Delim ||
                         'JSE Sector ID'    || C_Delim ||
                         'JSE Sector'    || C_Delim ||
                         'JSE Supersector ID'    || C_Delim ||
                         'JSE Supersector'    || C_Delim ||
                         'JSE Industry ID'    || C_Delim ||
-- SSI - 27OCT2010
--                       'JSE Industry');
                         'JSE Industry'       || C_Delim ||
                         'Currency');
       Utl_File.Put_Line(File_Type,
--                       'JSE'     || C_Delim ||
--                       'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro'     || C_Delim ||
                         'StatPro' || C_Delim ||
--                    -- SSI - 07NOV2007 - New field
                         'StatPro' || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
-- SSI - 27OCT2010
                         'JSE'     || C_Delim ||
                         'JSE' );
--
       FOR Out_Rec IN Cur1
       LOOP
          pgmloc := 1480;
          V_ICB_Code := Out_Rec.ICB_Code;
          IF V_Prev_ICB_Code is NULL OR
             V_ICB_Code <> V_Prev_ICB_Code
          THEN
             V_SubSec := Get_SubSector(V_ICB_Code);
             V_Sector := Get_Sector(V_ICB_Code);
             V_SprSec := Get_SuperSector(V_ICB_Code);
             V_Indtry := Get_Industry(V_ICB_Code);
-- SSI - 16SEP2008
-- Temporary fix for missing supersector '8900'
-- We're not sure if this supersector ever exists - no description from JSE
--           IF V_SprSec='8900'
--           THEN
--              V_SprSec := NULL;
--           END IF;
-- SSI - 30OCT2008
-- Calling new function - Coding are moved to a new function
--
             V_SubSec_Text := Get_ICB_Text(V_SubSec);
             V_Sector_Text := Get_ICB_Text(V_Sector);
             V_SprSec_Text := Get_ICB_Text(V_SprSec);
             V_Indtry_Text := Get_ICB_Text(V_Indtry);
--         
/*
-- *****************************************************************************
-- SSI - 17SEP2008
-- Exception for V_Sector '8980' '8990'
-- *****************************************************************************
             IF V_Sector = '8980'
             THEN
                V_SprSec := '8700';
          --    V_Indtry follows the norm, as found -- '8000'
             ELSIF V_Sector = '8990'
             THEN
                V_SprSec := NULL;
                V_Indtry := NULL;
             END IF;
--
-- SSI - 12SEP2008
-- Initialize text for ICB sectors
-- Do not initialized with the code, initialize with NULL
--           V_SubSec_Text := V_SubSec;
--           V_Sector_Text := V_Sector;
--           V_SprSec_Text := V_SprSec;
--           V_Indtry_Text := V_Indtry;
--
             V_SubSec_Text := NULL;
             V_Sector_Text := NULL;
             V_SprSec_Text := NULL;
             V_Indtry_Text := NULL;
--
             OPEN Get_Class_Text(V_SubSec);
             FETCH Get_Class_Text INTO V_SubSec_Text;
             CLOSE Get_Class_Text;
             V_SubSec_Text := REPLACE(REPLACE(V_SubSec_Text,C_Delim,' ')
                                      ,'  ',' ');
--
             OPEN Get_Class_Text(V_Sector);
             FETCH Get_Class_Text INTO V_Sector_Text;
             CLOSE Get_Class_Text;
             V_Sector_Text := REPLACE(REPLACE(V_Sector_Text,C_Delim,' ')
                                      ,'  ',' ');
--
             OPEN Get_Class_Text(V_SprSec);
             FETCH Get_Class_Text INTO V_SprSec_Text;
             CLOSE Get_Class_Text;
             V_SprSec_Text := REPLACE(REPLACE(V_SprSec_Text,C_Delim,' ')
                                      ,'  ',' ');
--
             OPEN Get_Class_Text(V_Indtry);
             FETCH Get_Class_Text INTO V_Indtry_Text;
             CLOSE Get_Class_Text;
             V_Indtry_Text := REPLACE(REPLACE(V_Indtry_Text,C_Delim,' ')
                                      ,'  ',' ');
*/
          END IF;
--
          Utl_File.Put_Line(File_Type,
--                          Out_Rec.Series_Code || C_Delim ||
--                          C_Text_Delim ||
--                          Out_Rec.Series_Name ||
--                          C_Text_Delim ||  C_Delim ||
                            TO_CHAR(V_Date,C_Date_Format) || C_Delim ||
                            Out_Rec.CONS_CODE     || C_Delim ||
                            Out_Rec.ALPHA_CODE    || C_Delim ||
                            Out_Rec.ISIN     || C_Delim ||
--                      -- SSI - New field - Security name
                            C_Text_Delim ||
                            Out_Rec.sec_name         ||
                            C_Text_Delim ||  C_Delim ||
                            C_Text_Delim ||
--                       -- SSI - 15SEP2008
--                       -- Get_SubSector(V_ICB_Code) ||
                            V_SubSec                  ||
                            C_Text_Delim ||  C_Delim ||
                            C_Text_Delim || V_SubSec_Text || C_Text_Delim ||
                             C_Delim ||
                            C_Text_Delim ||
--                      -- SSI - 15SEP2008
--                      --  Get_Sector(V_ICB_Code)  ||
                            V_Sector                ||
                            C_Text_Delim ||  C_Delim ||
                            C_Text_Delim || V_Sector_Text || C_Text_Delim ||
                             C_Delim ||
                            C_Text_Delim ||
--                      -- SSI - 15SEP2008
--                      --  Get_SuperSector(V_ICB_Code)  || 
                            V_SprSec                 ||
                            C_Text_Delim ||  C_Delim ||
                            C_Text_Delim || V_SprSec_Text || C_Text_Delim ||
                             C_Delim ||
                            C_Text_Delim ||
--                      -- SSI - 15SEP2008
--                      --  Get_Industry(V_ICB_Code)  ||
                            V_Indtry                 ||
                            C_Text_Delim ||  C_Delim ||
                            C_Text_Delim || V_Indtry_Text || C_Text_Delim
-- SSI - 27OCT2010
                            || C_Delim ||
                            Out_rec.iso_Currency 
-- SSI - 27OCT2010 - end of modif
                            );
          V_Prev_ICB_Code := V_ICB_Code;
       END LOOP;   -- Each output record
       Utl_File.Fclose(File_Type);
       IF V_Copy_FileName IS NOT NULL
       THEN
          Pgmloc := 1490;
          Manage_Utl_File.Copy_File ( Out_File_Path
                                     ,V_FileName
                                     ,Out_File_Path
                                     ,V_Copy_FileName
                                     ,1
                                     ,NULL
                                     ,V_Success
                                     ,V_Message);
          IF ( NOT V_SUCCESS )
          THEN
            GOTO GET_OUT;
          END IF;
          V_Files := V_Files + 1;
          G_Out_Files(V_Files) := V_Copy_FileName;
       END IF;
--
<<Next_Date>>
      pgmloc := 1500;
      V_Date := V_Date + 1;
    END LOOP; --} WHILE TRUE (on date)
-- SSI - 26NOV2007
--  P_Success := TRUE;
    V_Success := TRUE;
<<get_Out>>
-- SSI - 26NOV2007
    P_Success := V_Success;
    P_Message := V_Message;
-- SSI  - 08JAN2008
    G_Num_Files := G_Num_Files + V_Files;
    NULL;
    --
EXCEPTION
   WHEN OTHERS
   THEN
     P_Message := pgmloc || ' ' ||SUBSTR(SQLERRM(SQLCODE),1,255);
--
END Security_Characteristic;
------------------------------------------------------------------------
PROCEDURE Series_Value
        ( P_Composite_index    IN     Tickers.Fri_Ticker%TYPE
        , P_Data_Supplier_Code IN     Data_Suppliers.Code%TYPE
        , P_Del_Time           IN     VARCHAR2
        , P_From_Date          IN     DATE
        , P_To_Date            IN     DATE
        , P_Date_Format        IN     VARCHAR2
        , P_Filename           IN     VARCHAR2
        , P_Copy_Filename      IN     VARCHAR2
        , P_Success               OUT BOOLEAN
        , P_Message               OUT VARCHAR2
        ) IS --{
--
    V_Date            DATE;
    W_Date            DATE;
    V_Formatted_Date  VARCHAR2(16);
    V_Holiday         BOOLEAN;
    V_Delivery_Date   DATE;
    V_FileName        VARCHAR2(80);
    V_Copy_FileName   VARCHAR2(80);
    V_Success         BOOLEAN;
    V_Message         VARCHAR2(512);
--
    CURSOR Cur1 IS
    select Security.get_sec_id(I.composite_index_fri_Ticker,I.source_Code,I.Price_Currency,'E') Series
      ,I.price_date
      ,I.Total_Return
      ,REPLACE(REPLACE(security.get_sec_name(I.Composite_Index_fri_ticker,120,'S2'),C_Delim,' '),'  ',' ') Series_Name
from index_perf_items I
    ,Composite_Index_Structures S
where S.Composite_index_fri_Ticker=NVL(P_Composite_index
                                      ,S.Composite_Index_Fri_Ticker)
and   S.Fri_Ticker_Parent IS NULL
and   W_Date BETWEEN S.Effective_Date AND NVL(S.Effective_End_Date,W_Date)
and   I.Composite_index_fri_Ticker = S.Composite_index_fri_Ticker
and   I.Fri_Ticker = S.Fri_Ticker
and   I.Data_Supplier_Code = P_Data_Supplier_Code
and   I.delivery_date = V_Delivery_date
order by 2,1;
Out_Rec   CUR1%ROWTYPE;
V_Files   NUMBER;
--
BEGIN
    --{
    pgmloc := 1510;
    P_Success := FALSE;
    P_Message := '';
    V_Success := FALSE;
    V_Message := '';
    V_FileName := NULL;
    G_Site_Date := Constants.Get_Site_date;
-- SSI - 08JAN2008
    G_Out_Files := G_Init_Files;
    V_Files     := 0;
    --
--
    V_Date := P_From_Date;
    W_Date := Holiday.Get_Actual_Last_Open_Date( V_Date
                                               , 'SUPPLIER'
                                               , P_Data_Supplier_Code
                                               , P_Del_Time );
    WHILE V_Date <= P_To_Date AND
          V_Date <= G_Site_Date
    LOOP --{
       pgmloc := 1520;
-- If weekend or a holiday
-- Update my work date
       IF TO_CHAR( V_Date, 'D' ) IN ( 1, 7 ) OR
          Holiday.Is_It_Open( V_Date
                            , 'SUPPLIER'
                            , P_Data_Supplier_Code
                            , P_Del_Time ) IS NOT NULL
       THEN
           V_Holiday := TRUE;
       ELSE
           W_Date := V_Date;
           V_Holiday := FALSE;
       END IF;
       pgmloc := 1530;
       dbms_output.put_line( V_Date );
       V_Delivery_Date := TO_DATE(TO_CHAR(W_Date,P_Date_Format)||P_Del_Time,
                                  P_Date_Format || 'HH24MI');
       V_Formatted_Date := TO_CHAR( V_Date, P_Date_Format );
       V_Filename := P_FileName;
-- SSI - 08JAN2008
--     IF INSTR( P_FileName, '%%%%%' ) > 0
--     THEN
--        V_FileName := REPLACE( P_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
--     IF INSTR( P_Copy_FileName, '%%%%%' ) > 0
--     THEN
--        V_Copy_FileName := REPLACE( P_Copy_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
       V_Copy_FileName := Manage_Messages.Replace_Text(P_Copy_Filename,V_Formatted_Date);
-- SSI - 08JAN2008 - End modif.
       pgmloc := 1540;
       File_Type   := Utl_File.Fopen(Out_File_Path, V_FileName, 'W');
       dbms_output.put_line(V_Date || ' Generating Report :' || V_FileName);
       pgmloc := 1550;
       Utl_File.Put_Line(File_Type,
                         V_Formatted_Date);
       Utl_File.Put_Line(File_Type,
                         'Series' || C_Delim ||
                         'Ending_Date' || C_Delim ||
                         'Series_Value'     || C_Delim ||
                         'Series_Name');
       Utl_File.Put_Line(File_Type,
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro');
       FOR Out_Rec IN Cur1
       LOOP
          pgmloc := 1560;
          Utl_File.Put_Line(File_Type,
                            Out_Rec.Series || C_Delim ||
                            TO_CHAR(V_Date,C_Date_Format) || C_Delim ||
                            Out_Rec.Total_Return  || C_Delim ||
                            C_Text_Delim ||
                            Out_Rec.Series_Name ||
                            C_Text_Delim);
       END LOOP;   -- Each output record
       Utl_File.Fclose(File_Type);
-- SSI - 26NOV2007
       IF V_Copy_FileName IS NOT NULL
       THEN
          Pgmloc := 1570;
          Manage_Utl_File.Copy_File ( Out_File_Path
                                     ,V_FileName
                                     ,Out_File_Path
                                     ,V_Copy_FileName
                                     ,1
                                     ,NULL
                                     ,V_Success
                                     ,V_Message);
          IF ( NOT V_SUCCESS )
          THEN
            GOTO GET_OUT;
          END IF;
          V_Files := V_Files + 1;
          G_Out_Files(V_Files) := V_Copy_FileName;
       END IF;
--
<<Next_Date>>
      pgmloc := 1580;
      V_Date := V_Date + 1;
    END LOOP; --} WHILE TRUE (on date)
-- SSI - 26NOV2007
--  P_Success := TRUE;
    V_Success := TRUE;
<<get_Out>>
-- SSI - 26NOV2007
    P_Success := V_Success;
    P_Message := V_Message;
-- SSI  - 08JAN2008
    G_Num_Files := G_Num_Files + V_Files;
    NULL;
    --
EXCEPTION
   WHEN OTHERS
   THEN
     P_Message := pgmloc || ' ' ||SUBSTR(SQLERRM(SQLCODE),1,255);
--
END Series_Value;
------------------------------------------------------------------------
PROCEDURE Sub_Series_Value
        ( P_Composite_index    IN     Tickers.Fri_Ticker%TYPE
        , P_Data_Supplier_Code IN     Data_Suppliers.Code%TYPE
        , P_Del_Time           IN     VARCHAR2
        , P_From_Date          IN     DATE
        , P_To_Date            IN     DATE
        , P_Date_Format        IN     VARCHAR2
        , P_Filename           IN     VARCHAR2
        , P_Copy_Filename      IN     VARCHAR2
        , P_Success               OUT BOOLEAN
        , P_Message               OUT VARCHAR2
        ) IS --{
--
    V_Date            DATE;
    W_Date            DATE;
    V_Formatted_Date  VARCHAR2(16);
    V_Holiday         BOOLEAN;
    V_Delivery_Date   DATE;
    V_FileName        VARCHAR2(80);
    V_Copy_FileName   VARCHAR2(80);
    V_Success         BOOLEAN;
    V_Message         VARCHAR2(512);
--
-- These are the index that we only load the index file and not loading in the
-- structure
--
    CURSOR Cur1 IS
    select Security.get_sec_id(I.composite_index_fri_Ticker,I.source_Code,I.Price_Currency,'E') Series
      ,I.price_date
      ,I.Total_Return
      ,REPLACE(REPLACE(security.get_sec_name(I.Composite_Index_fri_ticker,120,'S2'),C_Delim,' '),'  ',' ') Series_Name
from index_perf_items I
Where I.Fri_Ticker = I.Composite_Index_Fri_Ticker
and   I.Data_Supplier_Code = P_Data_Supplier_Code
and   I.delivery_date = V_Delivery_date
and   NOT EXISTS (Select 1
                  From Composite_Index_Structures C
                  Where C.Composite_Index_Fri_Ticker =
                           I.Composite_Index_Fri_Ticker
                  And   C.Fri_Ticker = I.Fri_Ticker)
order by 2,1;
Out_Rec   CUR1%ROWTYPE;
V_Files   NUMBER;
--
BEGIN
    --{
    pgmloc := 1590;
    P_Success := FALSE;
    P_Message := '';
    V_Success := FALSE;
    V_Message := '';
    V_FileName := NULL;
    G_Site_Date := Constants.Get_Site_date;
-- SSI - 08JAN2008
    G_Out_Files := G_Init_Files;
    V_Files     := 0;
    --
--
    V_Date := P_From_Date;
    W_Date := Holiday.Get_Actual_Last_Open_Date( V_Date
                                               , 'SUPPLIER'
                                               , P_Data_Supplier_Code
                                               , P_Del_Time );
    WHILE V_Date <= P_To_Date AND
          V_Date <= G_Site_Date
    LOOP --{
       pgmloc := 1600;
-- If weekend or a holiday
-- Update my work date
       IF TO_CHAR( V_Date, 'D' ) IN ( 1, 7 ) OR
          Holiday.Is_It_Open( V_Date
                            , 'SUPPLIER'
                            , P_Data_Supplier_Code
                            , P_Del_Time ) IS NOT NULL
       THEN
           V_Holiday := TRUE;
       ELSE
           W_Date := V_Date;
           V_Holiday := FALSE;
       END IF;
       pgmloc := 1610;
       dbms_output.put_line( V_Date );
       V_Delivery_Date := TO_DATE(TO_CHAR(W_Date,P_Date_Format)||P_Del_Time,
                                  P_Date_Format || 'HH24MI');
       dbms_output.put_line( '*** Delivery_Date ' ||
                           TO_CHAR(V_Delivery_Date,'DDMONYYYY HH24MI') );
       V_Formatted_Date := TO_CHAR( V_Date, P_Date_Format );
       V_Filename := P_FileName;
-- SSI - 08JAN2008
--     IF INSTR( P_FileName, '%%%%%' ) > 0
--     THEN
--        V_FileName := REPLACE( P_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
--     IF INSTR( P_Copy_FileName, '%%%%%' ) > 0
--     THEN
--        V_Copy_FileName := REPLACE( P_Copy_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
       V_Copy_FileName := Manage_Messages.Replace_Text(P_Copy_Filename,V_Formatted_Date);
-- SSI - 08JAN2008 - End modif.
       pgmloc := 1620;
       File_Type   := Utl_File.Fopen(Out_File_Path, V_FileName, 'W');
       dbms_output.put_line(V_Date || ' Generating Report :' || V_FileName);
       pgmloc := 1630;
       Utl_File.Put_Line(File_Type,
                         V_Formatted_Date);
       Utl_File.Put_Line(File_Type,
                         'Series' || C_Delim ||
                         'Ending_Date' || C_Delim ||
                         'Series_Value'     || C_Delim ||
                         'Series_Name');
       Utl_File.Put_Line(File_Type,
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'JSE'     || C_Delim ||
                         'StatPro');
       FOR Out_Rec IN Cur1
       LOOP
          pgmloc := 1640;
          Utl_File.Put_Line(File_Type,
                            Out_Rec.Series || C_Delim ||
                            TO_CHAR(V_Date,C_Date_Format) || C_Delim ||
                            Out_Rec.Total_Return  || C_Delim ||
                            C_Text_Delim ||
                            Out_Rec.Series_Name ||
                            C_Text_Delim);
       END LOOP;   -- Each output record
       Utl_File.Fclose(File_Type);
       IF V_Copy_FileName IS NOT NULL
       THEN
          Pgmloc := 1650;
          Manage_Utl_File.Copy_File ( Out_File_Path
                                     ,V_FileName
                                     ,Out_File_Path
                                     ,V_Copy_FileName
                                     ,1
                                     ,NULL
                                     ,V_Success
                                     ,V_Message);
          IF ( NOT V_SUCCESS )
          THEN
            GOTO GET_OUT;
          END IF;
          V_Files := V_Files + 1;
          G_Out_Files(V_Files) := V_Copy_FileName;
       END IF;
--
<<Next_Date>>
      pgmloc := 1660;
      V_Date := V_Date + 1;
    END LOOP; --} WHILE TRUE (on date)
-- SSI - 26NOV2007
--  P_Success := TRUE;
    V_Success := TRUE;
<<get_Out>>
-- SSI - 26NOV2007
    P_Success := V_Success;
    P_Message := V_Message;
-- SSI  - 08JAN2008
    G_Num_Files := G_Num_Files + V_Files;
    NULL;
    --
EXCEPTION
   WHEN OTHERS
   THEN
     P_Message := pgmloc || ' ' ||SUBSTR(SQLERRM(SQLCODE),1,255);
--
END Sub_Series_Value;
--------------------------------------------------------------------------------
PROCEDURE Ticker_Changes
        ( P_Composite_index    IN     Tickers.Fri_Ticker%TYPE
        , P_Data_Supplier_Code IN     Data_Suppliers.Code%TYPE
        , P_Del_Time           IN     VARCHAR2
        , P_From_Date          IN     DATE
        , P_To_Date            IN     DATE
        , P_Date_Format        IN     VARCHAR2
        , P_Filename           IN     VARCHAR2
        , P_Copy_Filename      IN     VARCHAR2
        , P_Success               OUT BOOLEAN
        , P_Message               OUT VARCHAR2
        ) IS --{
--
-- Report for current day load records on current day structure
-- Should not include any record added from tracker file the day after for calc.
-- Note these are not in the structure for the day yet
--
    V_Date            DATE;
    W_Date            DATE;
    V_FileName        VARCHAR2(80);
    V_Copy_FileName   VARCHAR2(80);
    V_Formatted_Date  VARCHAR2(16);
    V_Holiday         BOOLEAN;
    V_Delivery_Date   DATE;
    V_Success         BOOLEAN;
    V_Message         VARCHAR2(512);
--
    CURSOR Cur1 IS
     Select Distinct
            DECODE(T.Identification_System_Code,'Z','ISIN','O','ISIN','A','CONS_CODE','E','ALPHA_CODE') SYMBOL
           ,T.Old_Ticker
           ,T.New_Ticker
from Composite_Indexes C
    ,Composite_Index_Structures S
    ,Index_Perf_Items P
    ,Ticker_Changes T
    ,Identification_Systems I
where S.Composite_index_fri_Ticker=NVL(P_Composite_index
                                      ,S.Composite_Index_Fri_Ticker)
and   S.Fri_Ticker_Parent IS NOT NULL
and   W_Date BETWEEN S.Effective_Date AND NVL(S.Effective_End_Date,W_Date)
and   C.fri_Ticker = S.Composite_index_fri_Ticker
and   P.Composite_index_fri_Ticker = S.Composite_index_fri_Ticker
and   P.Fri_Ticker = S.Fri_Ticker
and   P.Data_Supplier_Code = P_Data_Supplier_Code
and   P.delivery_date = V_Delivery_Date
and   T.Fri_Ticker = S.Fri_Ticker
and   T.Identification_System_Code = I.Code
and   T.Identification_System_Code IN ('A','O','Z','E')
and   T.Source_Code = DECODE(I.Source_Dependence_Flag,'Y',P.Source_Code,T.Source_Code)
and   T.Effective_Date = W_Date
order by 1;  -- Order by Symbol
--
Out_Rec   CUR1%ROWTYPE;
V_Files   NUMBER;
--
BEGIN
    --{
    pgmloc := 1670;
    P_Success := FALSE;
    P_Message := '';
    V_Success := FALSE;
    V_Message := '';
    V_FileName := NULL;
    G_Site_Date := Constants.Get_Site_Date;
-- SSI - 08JAN2008
    G_Out_Files := G_Init_Files;
    V_Files     := 0;
    --
    V_Date := P_From_Date;
    W_Date := V_Date;
    WHILE V_Date <= P_To_date AND
          V_Date <= G_Site_date
    LOOP --{
       pgmloc := 1680;
-- Skip weekend
       IF TO_CHAR( V_Date, 'D' ) IN ( 1, 7 )
       THEN
          GOTO NEXT_DATE;
       END IF;
-- SSI - 03APR2008
--     Work date for which to check for ticker change is the same as file date.
       W_Date := V_Date;
--
       dbms_output.put_line( V_Date );
       V_Delivery_Date := TO_DATE(TO_CHAR(W_Date,P_Date_Format)||P_Del_Time,
                                  P_Date_Format || 'HH24MI');
       V_Formatted_Date := TO_CHAR( V_Date, P_Date_Format );
       V_Filename := P_FileName;
-- SSI - 08JAN2008
--     IF INSTR( P_FileName, '%%%%%' ) > 0
--     THEN
--        V_FileName := REPLACE( P_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
--     IF INSTR( P_Copy_FileName, '%%%%%' ) > 0
--     THEN
--        V_Copy_FileName := REPLACE( P_Copy_FileName, '%%%%%', V_Formatted_Date);
--     END IF;
       V_Copy_FileName := Manage_Messages.Replace_Text(P_Copy_Filename,V_Formatted_Date);
-- SSI - 08JAN2008 - End modif.
       pgmloc := 1690;
       File_Type   := Utl_File.Fopen(Out_File_Path, V_FileName, 'W');
       dbms_output.put_line(V_Date || ' Generating Report :' || V_FileName);
       pgmloc := 1700;
       Utl_File.Put_Line(File_Type,
                         V_Formatted_Date);
       Utl_File.Put_Line(File_Type,
                         'Ending_Date' || C_Delim ||
                         'SYMBOL'     || C_Delim ||
                         'OLD_CODE'     || C_Delim ||
                         'NEW_CODE');
       Utl_File.Put_Line(File_Type,
                         'StatPro'     || C_Delim ||
                         'StatPro'     || C_Delim ||
                         'StatPro'     || C_Delim ||
                         'StatPro');
       FOR Out_Rec IN Cur1
       LOOP
          pgmloc := 1710;
          Utl_File.Put_Line(File_Type,
                            TO_CHAR(V_Date,C_Date_Format) || C_Delim ||
                            Out_Rec.Symbol        || C_Delim ||
                            Out_Rec.Old_Ticker    || C_Delim ||
                            Out_Rec.New_Ticker
                            );
       END LOOP;   -- Each output record
       Utl_File.Fclose(File_Type);
       IF V_Copy_FileName IS NOT NULL
       THEN
          Pgmloc := 1720;
          Manage_Utl_File.Copy_File ( Out_File_Path
                                     ,V_FileName
                                     ,Out_File_Path
                                     ,V_Copy_FileName
                                     ,1
                                     ,NULL
                                     ,V_Success
                                     ,V_Message);
          IF ( NOT V_SUCCESS )
          THEN
            GOTO GET_OUT;
          END IF;
          V_Files := V_Files + 1;
          G_Out_Files(V_Files) := V_Copy_FileName;
       END IF;
--
<<Next_Date>>
      pgmloc := 1730;
      V_Date := V_Date + 1;
    END LOOP; --} WHILE TRUE (on date)
-- SSI - 26NOV2007
--  P_Success := TRUE;
    V_Success := TRUE;
<<get_Out>>
-- SSI - 26NOV2007
    P_Success := V_Success;
    P_Message := V_Message;
-- SSI  - 08JAN2008
    G_Num_Files := G_Num_Files + V_Files;
    NULL;
    --
EXCEPTION
   WHEN OTHERS
   THEN
     P_Message := pgmloc || ' ' ||SUBSTR(SQLERRM(SQLCODE),1,255);
--
END Ticker_Changes;
------------------------------------------------------------------------
PROCEDURE Clean_Client_Output
           (P_Fri_Client      IN     Clients.Fri_Client%TYPE,
            P_Product         IN     Products.Code%TYPE,
            P_Sub_Product     IN     Sub_Products.Sub_Product_Code%TYPE,
            P_Component       IN     Product_Components.Component_Code%TYPE,
            P_Date            IN     DATE,
            P_Success            OUT BOOLEAN,
            P_Message            OUT VARCHAR2)
AS
BEGIN
   pgmloc := 1740;
   P_Success                   := FALSE;
   P_Message                   := NULL;
-- Clean up the records for the client/product/date
   pgmloc := 1750;
   Delete Client_Output_Files
   WHERE Fri_Client = P_Fri_Client
   AND   Product_Code = P_Product
-- SSI - 15MAY2008 - Add Sub product code
   AND   Sub_Product_Code = P_Sub_Product
   AND   Component_Code = NVL(P_Component,Component_Code)
   AND   Date_Of_Prices = P_Date;
--
   G_Num_Del := G_Num_Del + SQL%ROWCOUNT;
   Pricing_Msg_Rec.Value_02 := G_Num_Del;
--
   P_Success := TRUE;
EXCEPTION
WHEN OTHERS
THEN
   Manage_Messages.Get_Oasis_Message( C_GEN_Message||'100'
                                    , 'N'
                                    , pgmloc
                                    , SUBSTR( SQLERRM( SQLCODE ),1,512 )
                                    ,'Clean_Client_Output'
                                    , P_Message
                                   );
END Clean_Client_Output;
------------------------------------------------------------------------
PROCEDURE Populate_Client_Output
           (P_Fri_Client      IN     Clients.Fri_Client%TYPE,
            P_Product         IN     Products.Code%TYPE,
            P_Sub_Product     IN     Sub_Products.Sub_Product_Code%TYPE,
            P_Component       IN     Product_Components.Component_Code%TYPE,
            P_Date            IN     DATE,
            P_Success            OUT BOOLEAN,
            P_Message            OUT VARCHAR2)
AS
   Out_rec         Client_Output_files%ROWTYPE;
   i               NUMBER;
--
BEGIN
   pgmloc                      := 1760;
   P_Success                   := FALSE;
   P_Message                   := NULL;
   Out_Rec.Fri_Client          := P_Fri_Client;
   Out_Rec.Product_Code        := P_Product;
-- SSI - 15MAY2008
   Out_Rec.Sub_Product_Code    := P_Sub_Product;
   Out_Rec.Component_Code      := P_Component;
   Out_Rec.Date_Of_Prices      := P_Date;
-- Out_Rec.Send_To_Client_Flag := G_File_Def(P_Component).Send_To_Client_Flag;
   IF P_Component <> 'F'
   THEN
      Out_Rec.Send_To_Client_Flag := G_File_Def(P_Component).Send_To_Client_Flag;
   END IF;
   Out_Rec.Last_User           := G_User;
   Out_Rec.Last_Stamp          := SYSDATE;
--
-- dbms_output.put_line(G_Out_Files.COUNT || ' files to be written to Client_Output_FIles');
   FOR i IN G_Out_Files.FIRST .. G_Out_Files.LAST
   LOOP
      pgmloc                      := 1770;
      Out_Rec.Output_File_Name := G_Out_Files(i);
      IF P_Component = 'F'
      THEN
         Out_Rec.Send_To_Client_Flag := G_Send_Files(i);
      END IF;
      Pricing_Msg_Rec.Msg_Text_2 := 'Report file ' || P_Component || ' ' || Out_Rec.Output_File_Name;
      pgmloc                      := 1780;
-- dbms_output.put_line('**** Inserting **** ' || Out_Rec.date_of_prices || ' ' || Out_Rec.Output_File_Name);
      INSERT INTO Client_Output_Files
      VALUES Out_Rec;
   END LOOP;
   --
   Pgmloc := 1790;
   P_Success := TRUE;
EXCEPTION
WHEN OTHERS
THEN
   Manage_Messages.Get_Oasis_Message( C_GEN_Message||'100'
                                    , 'N'
                                    , pgmloc
                                    , SUBSTR( SQLERRM( SQLCODE ),1,512 )
                                    ,'Populate_Client_Output'
                                    , P_Message
                                   );
END Populate_Client_Output;
------------------------------------------------------------------------
PROCEDURE Gen_Included_Files
               (P_Program_Id       IN            Ops_Scripts.Program_Id%TYPE,
                P_Run_Date         IN            DATE,
                P_Fri_Client       IN            CLIENTS.Fri_Client%TYPE,
                P_Product          IN            PRODUCTS.Code%TYPE,
                P_Sub_Product      IN            SUB_PRODUCTS.Sub_Product_Code%TYPE,
                Pricing_Msgs_Rec   IN OUT        Pricing_Msgs%ROWTYPE,
                P_Inc_Files_Rec       OUT NOCOPY t_Inc_Files,
                P_Not_Found_Rec       OUT NOCOPY t_Inc_Files,
                P_Success             OUT NOCOPY BOOLEAN)
AS
   C_Component                     VARCHAR2(1):='F';
   V_Supplier                      Data_Suppliers.Code%TYPE;
   V_Open                          BOOLEAN;
   V_Exists                        BOOLEAN;
   V_File_Name                     VARCHAR2(60);
   V_File_length                   NUMBER;
   V_Blk_Size                      NUMBER;
   idx                             INTEGER;
   Num_Not_Found                   INTEGER;
   V_Message                       VARCHAR2(512);
--
   CURSOR Get_Files IS
   SELECT I.* 
   FROM  Client_Included_Files   I
       , Client_File_Definitions D
   WHERE D.Fri_Client = P_fri_Client
   AND   D.Product_Code = P_Product
   AND   D.Sub_Product_Code = P_Sub_Product
   AND   D.Component_Code = C_Component
   AND   D.Send_To_Client_Flag = 'Y'
   AND   D.Active_Flag         = 'Y'
   AND   I.Fri_Client = D.Fri_Client
   AND   I.Product_Code = D.Product_Code
   AND   I.Sub_Product_Code = D.Sub_Product_Code
   AND   I.Component_Code = D.Component_Code
   AND   I.Send_To_Client_Flag = 'Y' 
   AND   I.Active_Flag         = 'Y';
--
   CURSOR Get_Supplier IS
   SELECT DISTINCT Data_Supplier_Code
   FROM   Client_Index_Suppliers
   WHERE  Fri_Client = p_Fri_Client
   AND    Product_Code = P_Product
   AND    Sub_Product_Code = P_Sub_Product
   AND    Active_Flag = 'Y';
--
BEGIN
   Pgmloc := 1800;
   P_Success := FALSE;
--
-- Get the required data_supplier
   IF P_Sub_Product = 'NA'
   THEN
      V_Supplier := 'FTS';
   ELSE --{
      Pgmloc := 1810;
      OPEN Get_Supplier;
      Pgmloc := 1820;
      FETCH Get_Supplier INTO V_Supplier;
      Pgmloc := 1830;
      IF Get_Supplier%NOTFOUND
      THEN
         Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'090'
                                          , 'N'
                                          , V_Message
                                         );
         Pricing_Msgs_Rec.Err_Text := SUBSTR(V_Message, 1, 255);
         CLOSE Get_Supplier;
         GOTO Out_Inc_Files; 
      END IF;
      CLOSE Get_Supplier;
   END IF;  -- }
--
   Pgmloc := 1840;
   idx := 0;
   Num_Not_Found := 0;
   V_File_Name := NULL;
   P_Inc_Files_Rec.DELETE;
   P_Not_Found_Rec.DELETE;
   For Files_Rec IN Get_Files
   LOOP  -- {
-- SSI - Verify if it is open according to holiday class of the file
      Pgmloc := 1850;
      IF (TO_CHAR(P_Run_Date,'D') NOT IN (1,7) AND
                Holiday.Is_It_Open(P_Run_Date
                           ,'HOLCLASS'
-- SSI - 07DEC2015
--                         ,Files_Rec.Holiday_Class_Code
--                         ,NULL) IS NULL)
                           ,NULL
                           ,Files_Rec.Holiday_Class_Code) IS NULL)
      THEN
         Pgmloc := 1852;
         V_Open := TRUE;
      ELSE
         Pgmloc := 1854;
         V_Open := FALSE;
      END IF;
--       IBOX - Month end is always OPEN
      IF V_Supplier = 'IBOX' AND
         P_Run_Date = LAST_DAY(P_Run_Date)
      THEN
         Pgmloc := 1860;
         V_Open := TRUE;
      END IF;
--
      IF V_Open
      THEN --{
         Pgmloc := 1870;
         IF Files_Rec.Dated_File_Name_Format IS NULL AND
            Files_Rec.Date_Format            IS NULL AND
            Files_Rec.Date_Keyword_Code      IS NULL
         THEN  -- {Constant file name
            Pgmloc := 1880;
            V_File_Name := Files_Rec.Constant_File_Name;
         ELSIF Files_Rec.Dated_File_Name_Format IS NOT NULL AND
               Files_Rec.Date_Format            IS NOT NULL AND
               Files_Rec.Date_Keyword_Code      IS NOT NULL
         THEN -- }{ Dated File Name
--    -- Get dated file name by keyword and holiday class
            pgmloc := 1890;
            Product_Component.Get_Dated_File_Name
                   (Files_Rec.Fri_Client         
                   ,Files_Rec.Product_Code       
                   ,Files_Rec.Sub_Product_Code   
                   ,Files_Rec.Component_Code     
                   ,Files_Rec.Constant_File_Name 
                   ,P_Run_Date               
                   ,V_File_Name          
                   ,P_Success            
                   ,V_Message            
                   );
            IF NOT P_Success THEN Goto Out_Inc_Files; END IF;
         END IF;  --}
--       Check if the file exists and mandatory
         Pgmloc := 1910;
         UTL_FILE.FGETATTR( Out_File_Path,
                            V_File_Name,
                            V_Exists,
                            V_File_length,
                            V_Blk_Size);
         IF V_Exists
         THEN
            Pgmloc := 1920;
            idx := idx + 1;
            P_Inc_Files_Rec(idx) := Files_Rec;
            P_Inc_Files_Rec(idx).Dated_File_Name_Format := V_File_Name;
--   --  Save those mandatory but not found
         ELSIF Files_Rec.Mandatory_Flag='Y'
         THEN
            Pgmloc := 1930;
            Num_Not_Found := Num_Not_Found + 1;
            P_Not_Found_Rec(Num_Not_Found) := Files_Rec;
            P_Not_Found_Rec(Num_Not_Found).Dated_File_Name_Format := V_File_Name;
         END IF;
      END IF;  --} If Open
   END LOOP;   --}
   Pgmloc := 1940;
   P_Success := TRUE;
-- dbms_output.put_line('# Files found ' || P_Inc_Files_Rec.COUNT);
-- dbms_output.put_line('# Files  not found ' || P_Not_Found_Rec.COUNT);
--
<<Out_Inc_Files>>
   IF V_Message IS NOT NULL
   THEN
      Pricing_Msgs_Rec.Err_Text := SUBSTR(V_Message, 1, 255);
   END IF;
--
EXCEPTION
WHEN OTHERS
THEN
   Manage_Messages.Get_Oasis_Message( C_GEN_Message||'100'
                                    , 'N'
                                    , pgmloc
                                    , SUBSTR( SQLERRM( SQLCODE ),1,200 )
                                    ,'Gen_Included_Files'
                                    , Pricing_Msgs_rec.Err_Text
                                   );
END Gen_Included_Files;
--
-- SEND_COMPRESSED_FILES
-- New procedure - ACB - 31dec2008
--
PROCEDURE Send_Compressed_Files(
    p_program_id       IN           Ops_Scripts.Program_Id%TYPE,
    p_run_date         IN           DATE,
    p_pricing_msg_rec  IN OUT       Pricing_Msgs%ROWTYPE,
    p_success          OUT          boolean,
    p_message          OUT NOCOPY   VARCHAR2
) --{
AS
    LOGICAL_ERROR           EXCEPTION;
--
    v_job_name              VARCHAR2(64);
    v_job_time              TIMESTAMP WITH TIME ZONE;
--
    v_client                clients.code%TYPE;
    v_fri_client            clients.fri_client%TYPE;
    v_product               products.code%TYPE;
    v_sub_product           sub_products.sub_product_code%TYPE;
    v_date                  DATE;
    v_AFT_Flag              VARCHAR2(10);
--
    v_Command_file          UTL_FILE.file_type;
    v_log_file              UTL_FILE.file_type;
    V_Execution_File        UTL_FILE.file_type;
    V_Date_Format           VARCHAR2(60);
    v_Command_file_name     client_file_definitions.dated_file_name_format%TYPE;
    v_log_file_name         client_file_definitions.dated_file_name_format%TYPE;
    v_execution_file_name   client_file_definitions.dated_file_name_format%TYPE;
    v_output_file_name      client_file_definitions.dated_file_name_format%TYPE;
    V_Date_File_Name        client_file_definitions.dated_file_name_format%TYPE;
    W_File_Name             client_file_definitions.dated_file_name_format%TYPE;
--  SSI 17JUN2009
    V_Post_File             UTL_FILE.file_type;
    V_Post_File_Name        client_file_definitions.dated_file_name_format%TYPE;
    V_Dated_OutFile_Name    client_file_definitions.dated_file_name_format%TYPE;
--
    v_run_date              DATE;
    v_message_code          VARCHAR2(5)   := NULL;
    v_message_val1          VARCHAR2(500) := NULL;
    v_message_val2          VARCHAR2(500) := NULL;
    v_message_val3          VARCHAR2(500) := NULL;
    v_oasis_message         VARCHAR2(1000):= NULL;
    v_Num_Error             INTEGER := 0;
    --
    V_End_Of_File           BOOLEAN;
    v_line                  VARCHAR2(1000):= NULL;
-- SSI - 26MAY2008
    v_program_id            NUMBER;
    v_log_mode              VARCHAR2(2);
--
BEGIN
    --{
    pgmloc := 1950;
    v_Num_Error   := 0;
-- Mise en commentaire - JSL - 16 janvier 2008
--  g_system_date := sysdate;
-- Fin de mise en commentaire - JSL - 16 janvier 2008
    p_success     := true;
-- Modification - JSL - 16 janvier 2008
-- The initialization of pricing messages is done in the dispatcher.
--  v_run_date    := nvl(p_run_date, Constants.Get_Site_Date);
    v_run_date         := Pricing_Msg_Rec.Date_of_Prices;
--
-- SSI - 26MAY2006
-- If this is a child program_id, then initialize a new pricing_msgs
   v_program_id := p_program_id;
   IF p_program_id >= 10000
   THEN
       v_run_date    := p_run_date;
--     get the parent program id
       v_program_id  := TRUNC(p_program_id/100);
       load_supplier_rules.Initialize_Prc_Msgs( p_program_id,
                                                SYSDATE,
                                                v_run_date,
                                                Pricing_Msg_Rec,
                                                p_success
                                               );
       IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
   ELSE
       Pricing_Msg_Rec := P_Pricing_Msg_Rec;
   END IF;
   g_system_date := Pricing_Msg_Rec.Date_Of_Msg;
-- Fin de mise en commentaire - JSL - 16 janvier 2008
-- Fin de modification - JSL - 16 janvier 2008
--
-- Mise en commentaire - JSL - 16 janvier 2008
--     load_supplier_rules.Initialize_Prc_Msgs( p_program_id,
--                                              g_system_date,
--                                              v_run_date,
--                                              Pricing_Msg_Rec,
--                                              p_success
--                                             );
--     IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
-- Fin de mise en commentaire - JSL - 16 janvier 2008
--
    pgmloc := 1960;
    Pricing_Msg_Rec.Successfull_Flag := 'N';
    Pricing_Msg_Rec.Msg_Text         := 'Program_id is '
                                                || to_char(p_program_id);
    Pricing_Msg_Rec.Msg_Text_2       := 'Program in progress';
    Pricing_Msg_Rec.err_text         := null;
    Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
                                        g_system_date,
                                        Pricing_Msg_Rec,
                                        p_success
                                       );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
--
    -- Get PRODUCT_RUNNING_PARMS data
    pgmloc := 1970;
-- SSI - 26MAY2008
--  Extract_Info.Get_Running_Info(p_program_id,
    Extract_Info.Get_Running_Info(v_program_id,
                                  v_client,
                                  v_fri_client,
                                  v_product,
                                  v_sub_product,
                                  v_date,
                                  v_aft_flag,
                                  Pricing_Msg_Rec,
                                  p_success
    );
    if (not p_success) then
        v_message_code := null;
        v_oasis_message := Pricing_Msg_Rec.err_text;
        raise LOGICAL_ERROR;
    end if;
--
    Pricing_Msg_Rec.remarks := 'Client ' || v_client;
--
--    
    -- Get the log file name
    pgmloc := 1980;
    Product_Component.Get_Constant_File_Name(v_fri_client,
                                            v_product,
--                                      -- SSI - 15MAY2008
                                            v_sub_product,
                                            'L',
                                            v_log_file_name,
                                            p_success,
                                            p_message
                                           );
    IF (NOT p_success) THEN
        v_message_code := NULL;
        v_oasis_message := p_message;
        RAISE LOGICAL_ERROR;
    END IF;
    --
    -- Create log file
    --
-- SSI - 26MAY2008
    IF p_program_id <> v_program_id
    THEN
        v_log_file_name := v_log_file_name || TRIM(TO_CHAR(MOD(p_program_id,100)));
    END IF;
--
    pgmloc := 1990;
    Load_Supplier_Rules.Open_File(v_log_file_name,
                                  Out_File_Path,
                                  'W',
                                  Pricing_Msg_Rec,
                                  v_log_file,
                                  p_success
                                 );
    IF (NOT p_success) THEN
        v_message_code := '450';
        v_message_val1 := v_log_file_name;
        RAISE LOGICAL_ERROR;
    END IF;
    pgmloc := 2000;
-- SSI - 26MAY2008
--  v_message_code := '340';
    v_message_code := '440';
    v_message_val1 := v_log_file_name;
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Program Send_Compressed_Files started at '
                                   || to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'),
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Program_id ' || to_char(p_program_id),
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
--
    pgmloc := 2010;
    Pricing_Msg_Rec.Remarks := 'Log file name: '
                                 || Out_File_Path
                                 || G_Dir_Seperator
                                 || v_log_file_name;
--
    pgmloc := 2020;
    FOR Client_Required IN ( SELECT PC.Component_Code
                                  , PC.Text_E
                               FROM Product_Components PC
                                  , Client_Requirements CR
                              WHERE PC.Product_Code    = CR.Product_Code
                                AND PC.Component_Code  = CR.Component_Code
                                AND CR.Product_Code    = V_Product
                                AND CR.Fri_Client      = V_Fri_Client
                                AND CR.Active_Flag     = 'Y'
                                AND CR.Component_Code IN ('A', 'X', 'O', 'Z')
                           )
    LOOP --{
        pgmloc := 2030;
        Product_Component.Get_Constant_File_Name( V_Fri_Client
                                                , V_Product
--                                      -- SSI - 15MAY2008
                                                , V_Sub_Product
                                                , Client_Required.Component_Code
                                                , W_File_Name
                                                , P_Success
                                                , P_Message
                                                );
        IF (NOT p_success) THEN
            v_message_code := NULL;
            v_oasis_message := p_message;
            RAISE LOGICAL_ERROR;
        END IF;
        --
        IF    Client_Required.Component_Code = 'A'
        THEN
            V_Date_File_Name      := W_File_Name;
        ELSIF Client_Required.Component_Code = 'O'
        THEN
            V_Output_File_Name    := W_File_Name;
--      --  SSI - 17JUN2009
            Product_Component.Get_Dated_File_Name( V_Fri_Client
                                                , V_Product
                                                , V_Sub_Product
                                                , Client_Required.Component_Code
                                                , V_Date
                                                , V_Dated_OutFile_Name
                                                , P_Success
                                                , P_Message
                                                );
            IF (NOT p_success) THEN
                v_message_code := NULL;
                v_oasis_message := p_message;
                RAISE LOGICAL_ERROR;
            END IF;
--      --  SSI - 17JUN2009 - end of modif.
        ELSIF Client_Required.Component_Code = 'X'
        THEN
            V_Execution_File_Name := W_File_Name;
        ELSIF Client_Required.Component_Code = 'Z'
        THEN
            V_Command_File_Name   := W_File_Name;
        END IF;
        --
        pgmloc := 2040;
        Load_Supplier_Rules.Write_File( v_log_file
                                      , Client_Required.Text_E
                                     || ' file name : '
                                     || Out_File_Path
                                     || G_Dir_Seperator
                                     || W_File_Name
                                      , Pricing_Msg_Rec
                                      , P_Success
                                      );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
        --
    END LOOP; --} For client required components
    --
    -- Create the Command file
    --
    pgmloc := 2050;
    Load_Supplier_Rules.Open_File(v_command_file_name,
                                  Out_File_Path,
                                  'W',
                                  Pricing_Msg_Rec,
                                  v_Command_file,
                                  p_success
                                 );
    IF (NOT p_success) THEN
        v_message_code := '450';
        v_message_val1 := pgmloc||v_command_file_name;
        RAISE LOGICAL_ERROR;
    END IF;
    --
    -- Get the date format for the date file
    pgmloc := 2060;
-- SSI - 26MAY2008
--  V_Date_Format := NVL( Scripts.Get_Arg_Value( P_Program_ID
    V_Date_Format := NVL( Scripts.Get_Arg_Value( V_Program_ID
                                               , 'Date_Format' )
                        , 'MMDDYYYY' );
    --
    -- Build the Command file
    --
    pgmloc := 2070;
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Start building the Command file '
                                       || 'for the client ' || v_client,
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
--
    pgmloc := 2080;
    FOR Msg IN ( SELECT Code
                   FROM Oasis_Messages
                  WHERE Code >= C_Oasis_Message || '100'
                    AND Code <  C_Oasis_Message || '200'
                  ORDER BY Code
               )
    LOOP --{
        IF    Msg.Code = C_Oasis_Message || '120'
        THEN
            pgmloc := 2090;
            Manage_Messages.Get_Oasis_Message( Msg.Code
                                             , 'N'
                                             , Constants.Get_Dir_Name
                                             , V_Oasis_Message
                                             );
        ELSIF Msg.Code = C_Oasis_Message || '130'
        THEN
            pgmloc := 2100;
            Manage_Messages.Get_Oasis_Message( Msg.Code
                                             , 'N'
                                             , V_Execution_File_Name
                                             , V_Oasis_Message
                                             );
        ELSIF Msg.Code = C_Oasis_Message || '140'
        THEN
            pgmloc := 2110;
            Manage_Messages.Get_Oasis_Message( Msg.Code
                                             , 'N'
                                             , V_Output_File_Name
                                             , V_Oasis_Message
                                             );
        ELSE
            pgmloc := 2120;
            Manage_Messages.Get_Oasis_Message( Msg.Code
                                             , 'N'
                                             , V_Oasis_Message
                                             );
        END IF;
        pgmloc := 2130;
        Load_Supplier_Rules.Write_file( v_Command_file
                                      , v_oasis_message
                                      , Pricing_Msg_Rec
                                      , p_success
                                      );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    END LOOP; --} Msg between 100 and 200 - preparation
    --
    pgmloc := 2140;
    FOR reg in ( SELECT cof.output_file_name
                   FROM client_output_files  cof
                      , client_list_products cp
                  WHERE cof.fri_client          = cp.include_fri_client
                    AND cof.product_code        = cp.product_code
-- SSI - 15MAY2008
                    AND cof.sub_product_code    = v_sub_product
                    AND cp.active_flag          = 'Y'
                    AND cof.send_to_client_flag = 'Y'
                    AND cof.date_of_prices      = p_run_date
                    AND cp.fri_client           = v_fri_client
                    AND cp.product_code         = v_product
-- SSI - 26MAY2008
                    AND cof.component_code <> C_Zip_Component              
-- SSI - 26MAY2008 -- End of modif.
-- APX 20JAN2011 Evaluated active_flag to create date file
--                 UNION
--                 SELECT V_Date_File_Name Output_File_Name
--                   FROM DUAL
                 UNION
                 SELECT V_Date_File_Name Output_File_Name
                   FROM Product_Components PC, Client_Requirements CR, Client_File_Definitions CF
                  WHERE PC.Product_Code = V_Product
                    AND PC.Component_Code = 'A'
                    AND CF.fri_client = V_Fri_Client  
                    AND CF.product_code = PC.Product_Code
                    AND CF.sub_product_code = v_sub_product
                    AND CF.Component_Code  = PC.Component_Code
                    AND CF.active_flag = 'Y'
                    AND CF.Send_To_Client_flag = 'Y'
                    AND CR.Product_Code = CF.Product_Code
                    AND CR.Fri_Client = CF.fri_client
                    AND CR.Active_Flag = 'Y'
                    AND CR.Component_Code = CF.Component_Code
-- SSI - 26MAY2008
                 UNION
                 SELECT cof.output_file_name
                   FROM client_output_files  cof
                  WHERE cof.fri_client          = v_fri_client
                    AND cof.product_code        = v_Product
                    AND cof.sub_product_code    = v_sub_product
                    AND cof.send_to_client_flag = 'Y'
                    AND cof.date_of_prices      = p_run_date
                    AND cof.component_code <> C_Zip_Component              
-- SSI - 26MAY2008 -- End of modif.
               )
    LOOP --{
        Pricing_Msg_Rec.Value_02 := NVL( Pricing_Msg_Rec.Value_02, 0 ) + 1;
        pgmloc := 2150;
        Load_Supplier_Rules.Write_file(v_log_file,
                                       '    File ' || reg.output_file_name,
                                       Pricing_Msg_Rec,
                                       p_success
                                      );
        pgmloc := 2160;
        FOR Msg IN ( SELECT Code
                       FROM Oasis_Messages
                      WHERE Code >= C_Oasis_Message || '200'
                        AND Code <  C_Oasis_Message || '300'
                      ORDER BY Code
                   )
        LOOP --{
            IF    Msg.Code = C_Oasis_Message || '220'
            THEN
                pgmloc := 2170;
                Manage_Messages.Get_Oasis_Message( Msg.Code
                                                 , 'N'
                                                 , Reg.Output_File_Name
                                                 , V_Oasis_Message
                                                 );
            ELSIF Msg.Code = C_Oasis_Message || '230'
            THEN
                pgmloc := 2180;
                Manage_Messages.Get_Oasis_Message( Msg.Code
                                                 , 'N'
                                                 , TO_CHAR( V_Date
                                                          , V_Date_Format )
                                                 , V_Oasis_Message
                                                 );
            ELSE
                pgmloc := 2190;
                Manage_Messages.Get_Oasis_Message( Msg.Code
                                                 , 'N'
                                                 , V_Oasis_Message
                                                 );
            END IF;
            --
            -- APX 20JAN2011 Since date file is not required anymore V_Date_File_Name could be null now 
            IF  Msg.Code = C_Oasis_Message || '230'
--          AND Reg.Output_File_Name != V_Date_File_Name
            AND Reg.Output_File_Name != NVL(V_Date_File_Name,'X')
            THEN
                NULL;
            ELSE
                pgmloc := 2200;
                Load_Supplier_Rules.Write_file( v_Command_file
                                              , v_oasis_message
                                              , Pricing_Msg_Rec
                                              , p_success
                                              );
                IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
            END IF;
        END LOOP; --} Msg between 200 and 300 - individual files
    END LOOP; --} file names
    --
    pgmloc := 2210;
    FOR Msg IN ( SELECT Code
                   FROM Oasis_Messages
                  WHERE Code >= C_Oasis_Message || '300'
                    AND Code <  C_Oasis_Message || '400'
                  ORDER BY Code
               )
    LOOP --{
        pgmloc := 2220;
        Manage_Messages.Get_Oasis_Message( Msg.Code
                                         , 'N'
                                         , V_Oasis_Message
                                         );
        pgmloc := 2230;
        Load_Supplier_Rules.Write_file( v_Command_file
                                      , v_oasis_message
                                      , Pricing_Msg_Rec
                                      , p_success
                                      );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    END LOOP; --} Msg between 100 and 200 - preparation
    --
    -- END of creation of the Command file
    --
    pgmloc := 2240;
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Command file created with success.',
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
--
    Pricing_Msg_Rec.remarks := Pricing_Msg_Rec.remarks
                            || ' with '
                            || Pricing_Msg_Rec.Value_02
                            || ' file(s)';
    pgmloc := 2250;
    Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
                                        g_system_date,
                                        Pricing_Msg_Rec,
                                        p_success
                                       );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    -- Close the Command file
    pgmloc := 2260;
    Load_Supplier_Rules.File_Close(v_command_file,
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    --
    -- Create job to do the command file
    --
    pgmloc := 2270;
-- SSI 16SEP2008
-- SSI - 17JUN2009 
--  v_job_name := dbms_scheduler.generate_job_name( 'JSE_' );
--  v_job_name := dbms_scheduler.generate_job_name( v_client || '_' );
--  pgmloc := 2280;
--  Manage_Messages.Get_Oasis_Message(c_Oasis_Message || '410',
--                                    'N',
--                                    v_job_name,
--                                    v_oasis_message);
--  pgmloc := 2290;
--  Load_Supplier_Rules.Write_file(v_log_file,
--                                 v_oasis_message,
--                                 Pricing_Msg_Rec,
--                                 p_success
--                                );
--  pgmloc := 2300;
--  Pricing_Msg_Rec.Msg_Text := v_oasis_message;
--  Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                      G_System_Date,
--                                      Pricing_Msg_Rec,
--                                      p_success
--                                     );
--  IF (NOT p_success) THEN
--      v_oasis_message := Pricing_Msg_Rec.err_text;
--      RAISE LOGICAL_ERROR;
--  END IF;
--
--  pgmloc := 2310;
--  v_job_time := to_timestamp_tz( '2099-12-3123:59:59-05:00'
--                           , 'yyyy-mm-ddhh24:mi:ssTZH:TZM'
--                           );
--  pgmloc := 2320;
--  Manage_Messages.Get_Oasis_Message(c_Oasis_Message || '430',
--                                    'N',
--                                    v_job_time,
--                                    v_oasis_message);
--  pgmloc := 2330;
--  Load_Supplier_Rules.Write_file(v_log_file,
--                                 v_oasis_message,
--                                 Pricing_Msg_Rec,
--                                 p_success
--                                );
--  pgmloc := 2340;
--  Pricing_Msg_Rec.Msg_Text := v_oasis_message;
--  Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                      G_System_Date,
--                                      Pricing_Msg_Rec,
--                                      p_success
--                                     );
--  IF (NOT p_success) THEN
--      v_oasis_message := Pricing_Msg_Rec.err_text;
--      RAISE LOGICAL_ERROR;
--  END IF;
--
--  pgmloc := 2350;
--  dbms_scheduler.create_job (job_name => v_job_name,
--                             job_type => 'executable',
--                             job_action => Out_File_Path || G_Dir_Seperator
--                                              ||v_command_file_name,
--                             start_date => v_job_time,
--                             comments => 'Zip daily'
--                            );
--  pgmloc := 2360;
--  Manage_Messages.Get_Oasis_Message(c_Oasis_Message || '410',
--                                    'N',
--                                    v_job_name,
--                                    v_oasis_message);
--  pgmloc := 2370;
--  Load_Supplier_Rules.Write_file(v_log_file,
--                                 v_oasis_message,
--                                 Pricing_Msg_Rec,
--                                 p_success
--                                );
--  pgmloc := 2380;
--  Pricing_Msg_Rec.Msg_Text := v_oasis_message;
--  Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                      G_System_Date,
--                                      Pricing_Msg_Rec,
--                                      p_success
--                                     );
--  IF (NOT p_success) THEN
--      v_oasis_message := Pricing_Msg_Rec.err_text;
--      RAISE LOGICAL_ERROR;
--  END IF;
--
--  pgmloc := 2390;
--  dbms_scheduler.enable(v_job_name);
--
--  pgmloc := 2400;
--  dbms_scheduler.run_job(v_job_name, TRUE);
--  pgmloc := 2410;
--  Manage_Messages.Get_Oasis_Message(c_Oasis_Message || '420',
--                                    'N',
--                                    v_job_name,
--                                    v_oasis_message);
--  pgmloc := 2420;
--  Load_Supplier_Rules.Write_file(v_log_file,
--                                 v_oasis_message,
--                                 Pricing_Msg_Rec,
--                                 p_success
--                                );
--  pgmloc := 2430;
--  Pricing_Msg_Rec.Msg_Text := v_oasis_message;
--  Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                      G_System_Date,
--                                      Pricing_Msg_Rec,
--                                      p_success
--                                     );
--  IF (NOT p_success) THEN
--      v_oasis_message := Pricing_Msg_Rec.err_text;
--      RAISE LOGICAL_ERROR;
--  END IF;
--  --
-- Ajout - JSL - 17 janvier 2008
--  pgmloc := 2440;
--  dbms_scheduler.drop_job( job_name => v_job_name
--                         , force    => TRUE
--                         );
-- Fin d'ajout - JSL - 17 janvier 2008
--  --
--  -- Open the execution file generated
--  --     in order to find if some problem happened
--  --
--  pgmloc := 2450;
--  Load_Supplier_Rules.Write_file(v_log_file,
--                                 'Validating the zip file',
--                                 Pricing_Msg_Rec,
--                                 p_success
--                                );
--
--  pgmloc := 2460;
--  Load_Supplier_Rules.Open_File(v_execution_file_name,
--                                Out_File_Path,
--                                'r',
--                                Pricing_Msg_Rec,
--                                v_execution_file,
--                                p_success
--                               );
--  IF (NOT p_success) THEN
--      v_message_code := '450';
--      v_message_val1 := pgmloc||v_execution_file_name;
--      RAISE LOGICAL_ERROR;
--  END IF;
--  pgmloc := 2470;
--  WHILE not (v_end_of_file)
--  LOOP
--      BEGIN
--          UTL_FILE.get_line(v_execution_file, v_line);
--          IF ( REGEXP_LIKE(v_line, '^\*\*\*\*\*') ) then
--             v_num_error := v_num_error + 1;
--             IF V_Num_Error = 1
--             THEN
--                 Pricing_Msg_Rec.Err_Text := V_Line;
--             ELSIF V_Num_Error = 2
--             THEN
--                 Pricing_Msg_Rec.Remarks  := V_Line;
--             END IF;
--          END IF;
--      EXCEPTION
--          when NO_DATA_FOUND then
--              v_end_of_file := true;
--      END;
--  END LOOP; -- WHILE not (v_end_of_file)
--  pgmloc := 2480;
--  -- Close the execution file
--  Load_Supplier_Rules.File_Close(v_execution_file,
--                                 Pricing_Msg_Rec,
--                                 p_success
--                                );
--  IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
--  --
--  IF ( V_Num_Error = 0 )
--  THEN
--      pgmloc := 2490;
--      W_File_Name := Product_Component.Copy_Dated_File_Name( V_Fri_Client
--                                                           , V_Product
--                                                      -- SSI - 15MAY2008
--                                                           , V_Sub_Product
--                                                           , 'O'
--                                                           , V_Date
--                                                           );
--      Pricing_Msg_Rec.Value_01 := 1;
--      pgmloc := 2500;
--      -- aft the file!
-- SSI - 26MAY2008
-- Check also if on prod system
--      IF V_AFT_Flag = 'Y'
--      AND Constants.Get_Db_Mode  = 'PROD'
--      THEN --{
--          Pgmloc := 2510;
-- SSI - 26MAY2008
--          AFT_Process.AFTMAKE( Pricing_Msg_Rec.Program_Id
--          AFT_Process.AFTMAKE( V_Program_Id
--                             , P_Success
--                             , Pricing_Msg_Rec.Err_Text
--                             );
--          IF P_Success THEN
--             Pricing_Msg_Rec.Value_15 := 1;
--          END IF;
--      END IF; --} V_Aft_Flag
--  END IF;
--
--  Create post_processing file to execute the command file to zip files and to copy to dated file name
    V_post_File_Name := V_Client || '_post.run';
--
    pgmloc := 1990;
    Load_Supplier_Rules.Open_File(v_post_file_name,
                                  Out_File_Path,
                                  'W',
                                  Pricing_Msg_Rec,
                                  v_post_file,
                                  p_success
                                 );
    IF (NOT p_success) THEN
        v_message_code := '450';
        v_message_val1 := v_post_file_Name;
        RAISE LOGICAL_ERROR;
    END IF;
    Load_Supplier_Rules.Write_file(v_post_file,
                                   '. ' || V_Command_File_Name ,
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    Load_Supplier_Rules.Write_file(v_post_file,
                                   'cp ' || Out_File_Path || G_Dir_Seperator || V_Output_File_Name || ' ' ||
                                            Out_File_Path || G_Dir_Seperator || V_Dated_OutFile_Name, 
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    pgmloc := 2260;
    Load_Supplier_Rules.File_Close(v_post_file,
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
-- SSI - 17JUN2009 
    -- Final update in Pricing_Msgs record
-- SSI - 24JAN2011
-- Set successfull for child program only
--  Pricing_Msg_Rec.Msg_Text         := 'Program Successfully Completed.';
--  Pricing_Msg_Rec.Successfull_Flag := 'Y';
    IF p_program_id >= 10000
    THEN
       Pricing_Msg_Rec.Msg_Text         := 'Program Successfully Completed.';
       Pricing_Msg_Rec.Successfull_Flag := 'Y';
    END IF;
    IF (v_num_error <> 0) then
        Pricing_Msg_Rec.Msg_Text_2 := 'Problem in generating the zip file.';
--      Pricing_Msg_Rec.err_text := v_line;
        Load_Supplier_Rules.Write_file(v_log_file,
                                   v_line,
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
        p_success := FALSE;
    ELSE
        p_success := TRUE;
        Pricing_Msg_Rec.Msg_Text_2 := 'Zip file generated with success.';
        Load_Supplier_Rules.Write_file(v_log_file,
                                   Pricing_Msg_Rec.Msg_Text_2,
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    END IF;
    pgmloc := 2520;
    Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
                                        G_System_Date,
                                        Pricing_Msg_Rec,
                                        p_success
                                       );
    pgmloc := 2530;
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'End of Send_Compressed_Files at '
                                   || to_char(sysdate, 'mm/dd/yyyy hh24:mi:ss'),
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    IF p_program_id <> v_program_id THEN GOTO End_Process; END IF;
    pgmloc := 2540;
    Load_Supplier_Rules.File_Close(v_log_file,
                                   Pricing_Msg_Rec,
                                   p_success
                                  );
    IF (NOT p_success) THEN
        v_oasis_message := Pricing_Msg_Rec.err_text;
        RAISE LOGICAL_ERROR;
    END IF;
<<End_Process>>
    NULL;
EXCEPTION
    WHEN LOGICAL_ERROR THEN
        if (v_message_code IS NOT NULL) then
            Manage_Messages.Get_Oasis_Message(c_Oasis_Message || v_message_code,
                                             'N',
                                             v_message_val1,
                                             v_message_val2,
                                             v_message_val3,
                                             v_oasis_message);
        end if;
        Pricing_Msg_Rec.Msg_Text  := 'Send_Compressed_Files aborted!';
        Pricing_Msg_Rec.Msg_Text_2:= 'Error in generating the command file.';
        Pricing_Msg_Rec.Successfull_Flag := 'N';
        Pricing_Msg_Rec.err_text         := '@' ||pgmloc || ' ' ||v_oasis_message;
        Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
                                            g_system_date,
                                            Pricing_Msg_Rec,
                                            p_success);
--
        load_supplier_Rules.Write_file(v_log_file,
                                       'Send_Compressed_Files aborted - pgmloc '
                                          || pgmloc,
                                       Pricing_Msg_Rec,
                                       p_success
                                      );
        p_message := v_oasis_message;
        p_success := false;
        utl_file.fclose_all;
    WHEN OTHERS THEN
        Manage_Messages.Get_Oasis_Message(C_GEN_Message || '100',
                                          'N',
                                          pgmloc,
                                          substr(sqlerrm, 1, 150),
                                          'Send_Compressed_Files',
                                          v_oasis_message);
        Pricing_Msg_Rec.Msg_Text  := 'Send_Compressed_Files aborted!';
        Pricing_Msg_Rec.Msg_Text_2:= 'Error in generating the command file.';
        Pricing_Msg_Rec.Successfull_Flag := 'N';
        Pricing_Msg_Rec.err_text         := v_oasis_message;
        Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
                                            g_system_date,
                                            Pricing_Msg_Rec,
                                            p_success);
--
        load_supplier_Rules.Write_file(v_log_file,
                                       'Send_Compressed_Files aborted - pgmloc '
                                          || pgmloc,
                                       Pricing_Msg_Rec,
                                       p_success
                                      );
        p_message := v_oasis_message;
        p_success := false;
        utl_file.fclose_all;
--}}
END Send_Compressed_Files;
--
-- End of addition - ACB - 31 dec 2007
--------------------------------------------------------------------------------
PROCEDURE For_Client
           (P_Program_Id   IN      Ops_Scripts.Program_Id%TYPE,
            P_Dir_Name     IN OUT  VARCHAR2,
            P_Out_File     IN OUT  VARCHAR2,
            P_Commit_Freq  IN      NUMBER,
            P_Success      OUT     BOOLEAN,
            P_Message      OUT     VARCHAR2)
AS
--
 C_Data_Supplier_Code        Data_Suppliers.Code%TYPE:='FTS';
 C_Gen_Naming                VARCHAR2(10):='GEN';
 C_Fmt_Naming                VARCHAR2(10):='FMT';
 C_XTR                       VARCHAR2(5):='XTR';
 C_SEND                      VARCHAR2(5):='SEND';
--
--  TYPE File_Names IS TABLE OF VARCHAR2(60) INDEX BY BINARY_INTEGER;
--  V_Out_Files                 File_Names;
--  V_Init_Files                File_Names;
--
 V_Date_Format               VARCHAR2(8):='MMDDYYYY';
--
 V_Clt                       Clients.Code%TYPE;
 V_Client_Code               Clients.Code%TYPE;
 V_Fri_Client                Clients.Fri_Client%TYPE;
 V_Product                   Products.Code%TYPE;
 V_Sub_Product               Sub_Products.Sub_Product_Code%TYPE;
 V_Rerun_Date                DATE;
 V_AFT_Flag                  Product_Running_Parms.AFT_Flag%TYPE;
 V_Num_Files                 NUMBER;
 V_Success                   BOOLEAN;
 V_Message                   VARCHAR2(512);
 V_Site_Date                 DATE;
 V_Status                    VARCHAR2(1);
 V_File_Name                 Product_Running_Parms.File_Name%TYPE;
-- SSI - 26NOV2007
 V_Copy_Name                 Product_Running_Parms.File_Name%TYPE;
 V_Run_Day                   VARCHAR2(20);
 V_Sub_Series_Flag           VARCHAR2(1);
 V_File_Naming               VARCHAR2(10):=NULL;
 V_Index_Family              VARCHAR2(4):=NULL;
 V_IV_Del_Time               VARCHAR2(4);
 V_CO_Del_Time               VARCHAR2(4);
--
 V_Last_Prc_Date             DATE;
 V_Last_Run_Date             DATE;
 V_Start_Date                DATE;
 V_End_Date                  DATE;
 V_Date_Rest                 VARCHAR2(30);
 V_Parm                      VARCHAR2(30);
 V_Supp_Found                NUMBER;
 C_Infinite_Date             DATE:='31-DEC-2100';
-- SSI - 15JAN2008
 V_Mode                      VARCHAR2(5);
--
-- Ajout - JSL - 27 novembre 2007
 V_Aft_Only                  Yes_No.Code%TYPE;
-- Fin d'ajout - JSL - 27 novembre 2007
--
 Abnormal_End                EXCEPTION;
 Invalid_File_Name           EXCEPTION;
 Invalid_Client              EXCEPTION;
 Problem_Aft                 EXCEPTION;
--
-- The table is required to store all the file names
-- to do aft at the end of the program.(Phebe,30Sep03).
--
TYPE File_Name_Typ IS TABLE OF VARCHAR2(30)
    INDEX BY BINARY_INTEGER;
--
T_File_Name                  File_Name_Typ;
--
CURSOR Valid_Supp (I_Supp      VARCHAR2
                  ,I_Del_Time  VARCHAR2) IS
SELECT Count(1)
FROM   Data_Supply_Schedule
WHERE  Data_Supplier_Code = I_Supp
AND    Delivery_Time = I_Del_Time;
--
-- SSI - 31DEC2007
CURSOR Get_File_Def
IS
SELECT *
FROM Client_File_Definitions
WHERE Fri_Client = V_Fri_Client
AND   Product_Code = V_Product
-- SSI - 15MAY2008
-- Sub product Code added
AND   Sub_Product_Code = V_Sub_Product
AND   Active_Flag = 'Y';
--
-- SSI - 08JAN2008
-- Get the latest successfull run prior to a given date
CURSOR Get_Last_Success_Run (I_Date    DATE)
IS
SELECT Date_Of_Prices
FROM   Pricing_Msgs M
WHERE  Program_Id = P_Program_ID
AND    Successfull_Flag = 'Y'
AND    Date_Of_Prices = (SELECT MAX(Date_Of_Prices)
                         FROM   Pricing_Msgs M1
                         WHERE M1.Program_Id = M.Program_Id
                         AND   M1.Date_Of_Prices < I_Date)
AND    Date_OF_Msg    = (SELECT MAX(Date_Of_Msg)
                         FROM   Pricing_Msgs M1
                         WHERE M1.Program_Id = M.Program_Id
                         AND   M1.Date_Of_Prices = M.Date_Of_Prices);
--
-- SSI - 17JUN2009
--
CURSOR Get_Prc_Msgs (prog_id       NUMBER,
                     i_price_date  DATE)
IS
SELECT *
FROM pricing_msgs P
WHERE program_id = prog_id
AND date_of_prices = i_price_date
AND successfull_flag = 'N'
AND terminal_id = Constants.Get_Terminal_Id
AND date_of_msg = (SELECT max(P1.date_of_msg)
                   FROM pricing_msgs P1
                   WHERE P.program_id     = P1.program_id
                   AND P.date_of_prices = P1.date_of_prices
                   AND P.terminal_id    = P1.terminal_id
                  );
--
-- SSI - 22JAN2009
idx                    NUMBER;
V_Inc_Files_Rec        t_Inc_Files;
V_Not_Found_Rec        t_Inc_Files;
-- SSI - end of modif.
--
V_Client_Def           Client_File_Definitions%ROWTYPE;
V_Date                 DATE;
--
BEGIN
    Pgmloc  := 2550;
    G_Program_ID    :=  P_Program_ID;
    G_System_Date   :=  SYSDATE;
    Out_File_Path   :=  NVL(P_Dir_Name,Constants.Get_Dir_Name);
--  V_Out_Files     :=  V_Init_Files;
    G_Commit_Freq   :=  P_Commit_Freq;
    P_Success       :=  FALSE;
    P_Message       :=  NULL;
    V_Success       :=  FALSE;
    V_Message       := NULL;
    --
--  V_Num_Files     :=  0;
    --
    G_File_Def      := G_File_Def_Init;
    G_Comp          := NULL;
    G_Num_Del       := 0;
    G_Num_Files     := 0;
--
    Pgmloc  := 2560;
    IF G_Commit_Freq IS NULL OR
       G_Commit_Freq = 0
    THEN
       G_Commit_Freq := 2500;
    END IF;
    ----------------------------
    /* Get Client Information.*/
    ----------------------------
    Pgmloc  := 2570;
    Extract_Info.Get_Running_Info(
             G_Program_ID,
             V_Client_Code,
             V_Fri_Client,
             V_Product,
             V_Sub_Product,
             V_Rerun_Date,
             V_AFT_Flag,
             Pricing_Msg_Rec,
             V_Success);
    IF NOT V_Success THEN
       GOTO Get_Out;
    END IF;
    V_Date := NVL(V_Rerun_date,Constants.Get_Site_Date);
    --
    ----------------------------
    -- Initializing Pricing_Msgs
    ----------------------------
--  SSI - 17JUN2009 
    IF G_On_Server THEN  --{
       pgmloc := 1082;
       OPEN Get_Prc_Msgs (G_Program_Id,
                          V_Date);
       FETCH Get_Prc_Msgs INTO Pricing_Msg_Rec;
       IF (Get_Prc_Msgs%Found) THEN  -- {
           pgmloc := 1084;
           G_System_Date := Pricing_Msg_Rec.Date_Of_Msg;
           CLOSE Get_Prc_Msgs;
           GOTO Skip_Init_Msgs;
       END IF;   --}
       CLOSE Get_Prc_Msgs;
    END IF; --}
--  SSI - 17JUN2009 - end of modif
    Pgmloc  := 2580;
    Global_File_Updates.Initialize_Pricing_Msgs
--    (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
      (G_Program_Id, G_System_Date, V_Date, TRUE, Pricing_Msg_Rec);
    --
    Commit;
<<Skip_Init_Msgs>>
    --
    ----------------------------
    /*   Update pricing_msgs..*/
    ----------------------------
    Pgmloc  := 2590;
    Pricing_Msg_Rec.Msg_Text        := 'Extraction for Client: '||
                                        V_Client_Code || ' for ' || V_Rerun_date;
    Pricing_Msg_Rec.Msg_Text_2      := 'Start of Program';
    Pricing_Msg_Rec.Successfull_Flag:= 'N';
    Pgmloc  := 2600;
    Global_File_Updates.Update_Pricing_Msgs
             (G_Program_Id,
              G_System_Date,
              TRUE,
              Pricing_Msg_Rec);
    Commit;
-- SSI - 31DEC2007
-- Code moved up
    ----------------------------
    /* Get Client Information.*/
    ----------------------------
--  Pgmloc  := 2610;
--  Extract_Info.Get_Running_Info(
--           G_Program_ID,
--           V_Client_Code,
--           V_Fri_Client,
--           V_Product,
--           V_Sub_Product,
--           V_Rerun_Date,
--           V_AFT_Flag,
--           Pricing_Msg_Rec,
--           V_Success);
--  IF NOT V_Success THEN
--     GOTO Get_Out;
--  END IF;
    --
-- SSI - 15JAN2008
    Pgmloc  := 2620;
    V_Mode := UPPER(TRIM(NVL(Scripts.Get_Arg_Value(G_Program_ID,'MODE'),C_XTR)));
-- Modification - JSL - 16 janvier 2008
-- Code transpose de plus bas
--  IF V_Mode NOT IN (C_XTR,C_SEND)
--  THEN
    IF    V_Mode = C_XTR
    THEN
       NULL;
    ELSIF V_Mode = C_SEND
    THEN
-- SSI - 22JAN2009
-- Include any other files in CLIENT_INCLUDED_FILES
       Pgmloc  := 2625;
       Clean_Client_Output (V_Fri_Client
                           ,V_Product
                           ,V_Sub_Product
                           ,NULL
                           ,V_Date
                           ,V_Success
                           ,V_Message);
       IF NOT V_Success THEN GOTO Get_Out; END IF;
--
       G_Num_Files := 0;
       G_Out_Files.DELETE;
       G_Send_Files.DELETE;
       Gen_Included_Files( P_Program_Id
                         , V_Date
                         , V_Fri_Client
                         , V_Product
                         , V_Sub_Product
                         , Pricing_Msg_Rec
                         , V_Inc_Files_Rec
                         , V_Not_Found_Rec
                         , V_Success
                         );
       IF NOT V_Success THEN 
         V_Message := Pricing_Msg_Rec.Err_Text;
         GOTO Get_Out;
       END IF;
-- Add to Output file list
-- SSI - to verified
       FOR idx IN 1 .. V_Inc_Files_Rec.COUNT
       LOOP
          G_Num_Files := G_Num_Files + 1;
          G_Out_Files(G_Num_Files) := V_Inc_Files_Rec(idx).Dated_File_Name_Format;
          G_Send_Files(G_Num_Files) := V_Inc_Files_Rec(idx).Send_To_Client_Flag;
       END LOOP;
--
       IF NVL(G_Num_Files,0) > 0
       THEN
          Populate_Client_Output(V_Fri_Client
                                ,V_Product
                                ,V_Sub_Product
                                ,'F'    -- Component_Code for Included files
                                ,V_Date
                                ,V_Success
                                ,V_Message);
          IF NOT V_Success THEN GOTO Get_Out; END IF;
       END IF;
--
-- Check if there's any missing file, do not AFT is so
       IF V_Not_Found_Rec.COUNT > 0
       THEN
          V_Aft_Flag := 'N';
       END IF;
-- SSI - 22JAN2009 -- End of Modif.
--
       Send_Compressed_Files( P_Program_Id
                            , V_Date
-- SSI - 26MAY2008
                            , Pricing_Msg_Rec
                            , V_Success
                            , V_Message
                            );
-- SSI - 16JUN2009
-- Check if successfull
       IF (NOT V_success) THEN GOTO Get_Out; End IF;
       G_Num_Files := Pricing_Msg_Rec.Value_01;
       G_Num_Del   := Pricing_Msg_Rec.Value_02;
--
-- SSI - 22JAN2009
-- Check if there's any missing file and report
       IF V_Not_Found_Rec.COUNT > 0
       THEN
          Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'080'
                                           , 'N'
                                           , V_Not_Found_Rec.COUNT
                                           , V_Message
                                       );
          V_Success := FALSE;
       END IF;
--
       GOTO GET_OUT;
-- SSI - 22JAN2009 - End of Modif.
-- Fin de modification - JSL - 16 janvier 2008
    ELSE
       Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'070'
                                        , 'N'
                                        , NVL(V_MODE,'NULL')
                                        , V_Message
                                       );
       V_Success := FALSE;
       GOTO Get_Out;
    END IF;
--
-- Ajout - JSL - 27 novembre 2007
    Pgmloc  := 2630;
    V_Aft_Only := UPPER(SUBSTR(NVL(Scripts.Get_Arg_Value(G_Program_ID
                                                   ,'AFT_ONLY') , 'N') , 1, 1));
    IF V_Aft_Only = 'Y'
    THEN --{
       IF V_AFT_Flag = 'Y'
       THEN --{
          Pgmloc := 2640;
          AFT_Process.AFTMAKE(G_Program_Id
                             ,V_Success
                             ,V_Message);
          IF NOT V_Success THEN
             Pricing_Msg_Rec.Err_Text := V_Message;
          ELSE
             Pricing_Msg_Rec.Value_01 := 1;
          END IF;
       END IF; --} V_Aft_Flag
       GOTO Get_Out;
    END IF; --} V_Aft_Only
-- Fin d'ajout - JSL - 27 novembre 2007
    /*********************************/
    /*  Extraction for each client   */
    /*********************************/
    Pgmloc := 2650;
-- SSI - 15JAN2008
-- Dispatch by V_Mode, not client
--  IF V_Client_Code = 'STP1'   OR
--     V_Client_Code = 'STP2'   OR
--     V_Client_Code = 'STP3'   OR
--     V_Client_Code = 'STP4'   OR
--     V_Client_Code = 'STP5'   OR
--     V_Client_Code = 'STP6'   OR
--     V_Client_Code = 'STP7'   OR
--     V_Client_Code = 'STP8'   OR
--     V_Client_Code = 'STP9'   OR
--     V_Client_Code = 'ST11'   OR
--     V_Client_Code = 'ST12'   OR
--     V_Client_Code = 'ST13'   OR
--     V_Client_Code = 'ST14'   OR
--     V_Client_Code = 'ST15'   OR
--     V_Client_Code = 'ST16'   OR
--     V_Client_Code = 'ST17'   OR
--     V_Client_Code = 'ST18'   OR
--     V_Client_Code = 'ST19'   OR
--     V_Client_Code = 'ST21'   OR
--     V_Client_Code = 'ST22'   OR
--     V_Client_Code = 'ST23'   OR
--     V_Client_Code = 'ST24'   OR
--     V_Client_Code = 'ST25'   OR
--     V_Client_Code = 'ST26'   OR
--     V_Client_Code = 'ST27'   OR
--     V_Client_Code = 'ST28'   OR
--     V_Client_Code = 'ST29'   OR
--     V_Client_Code = 'ST31'   OR
--     V_Client_Code = 'ST32'   OR
--     V_Client_Code = 'ST33'   OR
--     V_Client_Code = 'ST34'   OR
--     V_Client_Code = 'ST35'   OR
--     V_Client_Code = 'ST36'   OR
--     V_Client_Code = 'ST37'   OR
--     V_Client_Code = 'ST38'   OR
--     V_Client_Code = 'ST39'
-- Modification - JSL - 16 janvier 2008
-- Si nous sommes ici c'est que le mode est C_XTR!
--  IF V_Mode = 'XTR'
--  THEN  -- {}
-- Fin de modification - JSL - 16 janvier 2008
--
       Pgmloc  := 2660;
       V_IV_Del_Time := Scripts.Get_Arg_Value(G_Program_ID,'IVDEL');
       V_CO_Del_Time := Scripts.Get_Arg_Value(G_Program_ID,'CODEL');
       V_Index_Family:= UPPER(Scripts.Get_Arg_Value(G_Program_ID,'FAM'));
       V_Sub_Series_Flag:=NVL(UPPER(Scripts.Get_Arg_Value(G_Program_ID,'SUBSER')),'N');
       V_File_Naming := NVL(UPPER(Scripts.Get_Arg_Value(G_Program_ID,'NAMING'))
                           ,C_GEN_Naming);
       V_Run_Day     := UPPER(Scripts.Get_Arg_Value(G_Program_ID,'RUNDAY'));
       V_Clt         := LOWER(V_Client_Code);
--
-- Validations
--
       Pgmloc  := 2670;
       OPEN Valid_Supp (C_Data_Supplier_Code,V_IV_Del_Time);
       Pgmloc  := 2680;
       FETCH Valid_Supp INTO V_Supp_Found;
       Pgmloc  := 2690;
       CLOSE Valid_Supp;
       IF V_Supp_Found <=0
       THEN
          Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'010'
                                           , 'N'
                                           , C_Data_Supplier_Code
                                           , V_IV_Del_Time
                                           , V_Message
                                          );
-- SSI - 17JUN2009 
          V_Success := FALSE;
          GOTO Get_Out;
       END IF;
--
       Pgmloc  := 2700;
       OPEN Valid_Supp (C_Data_Supplier_Code,V_CO_Del_Time);
       Pgmloc  := 2710;
       FETCH Valid_Supp INTO V_Supp_Found;
       Pgmloc  := 2720;
       CLOSE Valid_Supp;
       Pgmloc  := 2730;
       IF V_Supp_Found <=0
       THEN
          Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'010'
                                           , 'N'
                                           , C_Data_Supplier_Code
                                           , V_CO_Del_Time
                                           , V_Message
                                          );
-- SSI - 17JUN2009 
          V_Success := FALSE;
          GOTO Get_Out;
       END IF;
--
-- SSI -- 08JAN2008
-- File name is driven by table now
--     Pgmloc  := 2740;
--     IF V_File_Naming = C_Fmt_Naming AND
--        V_Index_Family IS NULL
--     THEN
--        Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'020'
--                                         , 'N'
--                                         , V_Message
--                                        );
--     END IF;
--
-- Start processing
--
--     Pgmloc  := 2750;
--     V_Start_Date := TRUNC(NVL(V_Rerun_Date,Constants.Get_Site_Date));
-- SSI - 31DEC2007
-- Assign start date and end date if given in Break_Points
--     V_End_Date   := TRUNC(NVL(V_Rerun_Date,Constants.Get_Site_Date));
--
--     V_Start_Date  := NVL(Scripts.Get_Break_Arg_Value(G_Program_ID,'DATE1')
--                          ,V_Start_Date);
--
--     V_End_Date    := NVL(Scripts.Get_Break_Arg_Value(G_Program_ID,'DATE2')
--                         ,V_End_Date);
--
-- SSI - 31DEC2007
-- Comment out check on V_Run_Day
-- File date is now defined in Client_File_Definition
--
--     IF V_Run_Day IS NULL
--     THEN
--        Pgmloc  := 2760;
--        V_Start_Date  := NVL(Scripts.Get_Break_Arg_Value(G_Program_ID,'DATE1')
--                             ,V_Start_Date);
--
--        V_End_Date    := NVL(Scripts.Get_Break_Arg_Value(G_Program_ID,'DATE2')
--                            ,V_Start_Date);
--     ELSIF V_Run_Day = 'HOLIDAY'
--     THEN
--     -- Get last holiday
--        Pgmloc  := 2770;
--        WHILE Holiday.Is_It_Open( V_Start_Date
--                                , 'SUPPLIER'
--                                , C_Data_Supplier_Code
--                                , V_CO_Del_Time ) IS NULL
--        LOOP
--           Pgmloc  := 2780;
--           V_Start_date := V_Start_Date - 1;
--        END LOOP;
--        Pgmloc  := 2790;
--        V_End_Date   := V_Start_Date;
--     ELSIF V_Run_Day IN ('SATURDAY','SUNDAY')
--     THEN
--        Pgmloc  := 2800;
--     -- Need a TRIM, it's returning a CHAR(9)
--        WHILE TRIM(TO_CHAR(V_Start_Date,'DAY')) <> V_Run_day
--        LOOP
--           Pgmloc  := 2810;
--           V_Start_Date := V_Start_Date - 1;
--        END LOOP;
--        V_End_Date := V_Start_Date;
--     END IF;
--
--     Pgmloc  := 2820;
--     IF V_Start_Date <> V_End_Date AND
--        V_File_Naming = C_Gen_Naming
--     THEN
--        Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'030'
--                                         , 'N'
--                                         , V_Message
--                                        );
--        GOTO Get_Out;
--     END IF;
--
--     Define period of extraction
--
--     Find the last run date of given run date
       Pgmloc  := 2830;
       OPEN Get_Last_Success_Run(V_Date);
       Pgmloc  := 2840;
       FETCH Get_Last_Success_Run INTO V_Last_Prc_Date;
       Pgmloc  := 2850;
       IF Get_Last_Success_Run%NOTFOUND
       THEN
          V_Last_Prc_Date := V_Date-1;
       END IF;
--
       Pgmloc  := 2860;
       V_Start_Date := V_Last_Prc_Date+1;
       V_End_Date   := V_Date;
--     Is there a period defined in Break_Points
--     IF V_Rerun_Date IS NULL
--     THEN
          Pgmloc  := 2870;
          V_Start_Date  := NVL(Scripts.Get_Break_Arg_Value(G_Program_ID,'DATE1')
                               ,V_Start_Date);

          V_End_Date    := NVL(Scripts.Get_Break_Arg_Value(G_Program_ID,'DATE2')
                              ,V_End_Date);
--     END IF;
--
       Pgmloc  := 2880;
       Pricing_Msg_Rec.Remarks := 'Extracting data from ' || V_Start_date ||
                                   ' to ' || V_End_date;
--
--     Clean the output file table
--
       Pgmloc  := 2890;
       Clean_Client_Output (V_Fri_Client
                           ,V_Product
-- SSI - 15MAY2008
                           ,V_Sub_Product
                           ,NULL
                           ,V_Date
                           ,V_Success
                           ,V_Message);
       IF NOT V_Success THEN GOTO Get_Out; END IF;
-- SSI - 31DEC2007
-- Get file definition
--
       pgmloc := 2900;
       OPEN Get_File_Def;
       LOOP
          pgmloc := 2910;
          FETCH Get_File_Def INTO V_Client_Def;
          pgmloc := 2920;
          EXIT WHEN Get_File_Def%NOTFOUND;
          G_File_Def(V_Client_Def.Component_Code) := V_Client_Def;
       END LOOP;
       CLOSE Get_File_Def;
--
       Pgmloc  := 2930;
       G_Comp := 'B';
       IF G_File_Def.EXISTS(G_Comp)
       THEN  --{
          Pgmloc  := 2940;
          V_File_Name := NULL;
          V_Copy_Name := NULL;
-- SSI - 08JAN2008
--        IF V_File_Naming = C_Gen_Naming
--        THEN
--           V_File_Name :=V_Clt || '_detail_benchmark.dat';
-- SSI - 26NOV2007
--           V_Copy_Name :='JSE_' || V_Index_Family || '_Constituents1_%%%%%.cn1';
--        ELSIF V_File_Naming = C_Fmt_Naming AND
--              V_Index_Family IS NOT NULL
--        THEN
--           V_File_Name :='JSE_' || V_Index_Family || '_Constituents1_%%%%%.cn1';
--        END IF;
-- Filename :='JSE_' || V_Index_Family || '_Constituents1_%%%%%.cn1';
-- V_File_Name :=V_Clt || '_detail_benchmark.dat';
--
          V_File_Name := G_File_Def(G_Comp).Constant_File_Name;
          V_Copy_Name := G_File_Def(G_Comp).Dated_File_name_Format;
          V_Date_Format := G_File_Def(G_Comp).Date_Format;
-- SSI - 08JAN2008 - End modif.
          Pgmloc  := 2950;
          Index_Reports.Detail_Benchmark
                  ( NULL
                  , C_Data_Supplier_Code
                  , V_CO_Del_Time
                  , V_Start_Date
                  , V_End_Date
                  , V_Date_Format
                  , V_File_Name
-- SSI - 26NOV2007
                  , V_Copy_Name
                  , V_Success
                  , V_Message);
          IF V_Success THEN
             Pgmloc  := 2960;
--           V_Num_Files := V_Num_Files + 1;
--           V_Out_Files(V_Num_Files) := V_File_Name;
--           Pricing_Msg_Rec.Value_01 := V_Num_Files;
             Pricing_Msg_Rec.Value_01 := G_Num_Files;
          ELSE
             V_Message := 'Error generating Detail Benchmark ' || V_Message;
             GOTO GET_OUT;
          END IF;
          Pgmloc  := 2970;
          Populate_Client_Output(V_Fri_Client
                                ,V_Product
--                          -- SSI - 15MAY2008
                                ,V_Sub_Product
                                ,G_Comp
                                ,V_Date
                                ,V_Success
                                ,V_Message);
          IF NOT V_Success THEN GOTO GET_OUT; END IF;
          Pgmloc  := 2980;
       END IF; --}
--
       G_Comp := 'D';
       IF G_File_Def.EXISTS(G_Comp)
       THEN  --{
       Pgmloc  := 2990;
       V_Copy_Name := NULL;
--     IF V_File_Naming = C_Gen_Naming
--     THEN
--        V_File_Name :=V_Clt || '_compl_detail_benchmark.dat';
-- SSI - 26NOV2007
--        V_Copy_Name :='JSE_' || V_Index_Family || '_Constituents2_%%%%%.cn2';
--     ELSIF V_File_Naming = C_Fmt_Naming AND
--           V_Index_Family IS NOT NULL
--     THEN
--        V_File_Name :='JSE_' || V_Index_Family || '_Constituents2_%%%%%.cn2';
--     END IF;
-- Filename :='JSE_' || V_Index_Family || '_Constituents2_%%%%%.cn2';
-- V_File_Name :=V_Clt || '_compl_detail_benchmark.dat';
--
          V_File_Name := G_File_Def(G_Comp).Constant_File_Name;
          V_Copy_Name := G_File_Def(G_Comp).Dated_File_name_Format;
          V_Date_Format := G_File_Def(G_Comp).Date_Format;
-- SSI - 08JAN2008 - End modif.
          Pgmloc  := 3000;
          Index_Reports.Complete_Detail_Benchmark
                  ( NULL
                  , C_Data_Supplier_Code
                  , V_CO_Del_Time
                  , V_Start_Date
                  , V_End_Date
                  , V_Date_Format
                  , V_File_Name
-- SSI - 26NOV2007
                  , V_Copy_Name
                  , V_Success
                  , V_Message);
          IF V_Success THEN
             Pgmloc  := 3010;
--           V_Num_Files := V_Num_Files + 1;
--           V_Out_Files(V_Num_Files) := V_File_Name;
--           Pricing_Msg_Rec.Value_01 := V_Num_Files;
             Pricing_Msg_Rec.Value_01 := G_Num_Files;
          ELSE
             V_Message := 'Error generating Complete detail Benchmark ' || V_Message;
             GOTO GET_OUT;
          END IF;
          Pgmloc  := 3020;
          Populate_Client_Output(V_Fri_Client
                                ,V_Product
--                          -- SSI - 15MAY2008
                                ,V_Sub_Product
                                ,G_Comp
                                ,V_Date
                                ,V_Success
                                ,V_Message);
          IF NOT V_Success THEN GOTO GET_OUT; END IF;
          Pgmloc  := 3030;
       END IF; --}
--
       G_Comp := 'C';
       IF G_File_Def.EXISTS(G_Comp)
       THEN  --{
          Pgmloc  := 3040;
--        V_Copy_Name := NULL;
--        IF V_File_Naming = C_Gen_Naming
--        THEN
--           V_File_Name :=V_Clt || '_sec_characteristics.dat';
-- SSI - 26NOV2007
--           V_Copy_Name :='JSE_' || V_Index_Family || '_Characteristics_%%%%%.sec';
--        ELSIF V_File_Naming = C_Fmt_Naming AND
--              V_Index_Family IS NOT NULL
--        THEN
--           V_File_Name :='JSE_' || V_Index_Family || '_Characteristics_%%%%%.sec';
--        END IF;
    -- Filename :='JSE_' || V_Index_Family || '_Characteristics_%%%%%.sec';
    -- V_File_Name :=V_Clt || '_sec_characteristics.dat';
          V_File_Name := G_File_Def(G_Comp).Constant_File_Name;
          V_Copy_Name := G_File_Def(G_Comp).Dated_File_name_Format;
          V_Date_Format := G_File_Def(G_Comp).Date_Format;
-- SSI - 08JAN2008 - End modif.
          Pgmloc  := 3050;
          Index_RePorts.Security_Characteristic
                  ( NULL
                  , C_Data_Supplier_Code
                  , V_CO_Del_Time
                  , V_Start_Date
                  , V_End_Date
                  , V_Date_Format
                  , V_File_Name
-- SSI - 26NOV2007
                  , V_Copy_Name
                  , V_Success
                  , V_Message);
          IF V_Success THEN
             Pgmloc  := 3060;
--           V_Num_Files := V_Num_Files + 1;
--           V_Out_Files(V_Num_Files) := V_File_Name;
--           Pricing_Msg_Rec.Value_01 := V_Num_Files;
             Pricing_Msg_Rec.Value_01 := G_Num_Files;
          ELSE
             V_Message := 'Error generating Characteristics ' || V_Message;
             GOTO GET_OUT;
          END IF;
          Pgmloc  := 3070;
          Populate_Client_Output(V_Fri_Client
                                ,V_Product
--                          -- SSI - 15MAY2008
                                ,V_Sub_Product
                                ,G_Comp
                                ,V_Date
                                ,V_Success
                                ,V_Message);
          IF NOT V_Success THEN GOTO GET_OUT; END IF;
          Pgmloc  := 3080;
       END IF; --}
--
       G_Comp := 'V';
       IF G_File_Def.EXISTS(G_Comp)
       THEN  --{
          Pgmloc  := 3090;
--        V_Copy_Name := NULL;
--        IF V_File_Naming = C_Gen_Naming
--        THEN
--           V_File_Name := V_Clt || '_series_val.dat';
-- SSI - 26NOV2007
--           V_Copy_Name :='JSE_' || V_Index_Family || '_Series_%%%%%.ser';
--        ELSIF V_File_Naming = C_Fmt_Naming AND
--              V_Index_Family IS NOT NULL
--        THEN
--           V_File_Name :='JSE_' || V_Index_Family || '_Series_%%%%%.ser';
--        END IF;
    -- Filename :='JSE_' || V_Index_Family || '_Series_%%%%%.ser';
    -- V_File_Name :=V_Clt || '_series_val.dat';
          V_File_Name := G_File_Def(G_Comp).Constant_File_Name;
          V_Copy_Name := G_File_Def(G_Comp).Dated_File_name_Format;
          V_Date_Format := G_File_Def(G_Comp).Date_Format;
-- SSI - 08JAN2008 - End modif.
          Pgmloc  := 3100;
          Index_Reports.Series_Value
                  ( NULL
                  , C_Data_Supplier_Code
                  , V_IV_Del_Time
                  , V_Start_Date
                  , V_End_Date
                  , V_Date_Format
                  , V_File_Name
-- SSI - 26NOV2007
                  , V_Copy_Name
                  , V_Success
                  , V_Message);
          IF V_Success THEN
             Pgmloc  := 3110;
--           V_Num_Files := V_Num_Files + 1;
--           V_Out_Files(V_Num_Files) := V_File_Name;
--           Pricing_Msg_Rec.Value_01 := V_Num_Files;
             Pricing_Msg_Rec.Value_01 := G_Num_Files;
          ELSE
             V_Message := 'Error generating Series_Value-' || V_Message;
             GOTO GET_OUT;
          END IF;
          Pgmloc  := 3120;
          Populate_Client_Output(V_Fri_Client
                                ,V_Product
--                          -- SSI - 15MAY2008
                                ,V_Sub_Product
                                ,G_Comp
                                ,V_Date
                                ,V_Success
                                ,V_Message);
          IF NOT V_Success THEN GOTO GET_OUT; END IF;
          Pgmloc  := 3130;
       END IF; --}
--
       Pgmloc  := 3140;
--     IF V_Sub_Series_Flag='Y'
       G_Comp := 'S';
       IF G_File_Def.EXISTS(G_Comp)
       THEN
--        V_Copy_Name := NULL;
--        IF V_File_Naming = C_Gen_Naming
--        THEN
--           V_File_Name :=V_Clt || '_sub_series_val.dat';
-- SSI - 26NOV2007
--           V_Copy_Name :='JSE_' || V_Index_Family || '_Sub_Series_%%%%%.ser';
--        ELSIF V_File_Naming = C_Fmt_Naming AND
--              V_Index_Family IS NOT NULL
--        THEN
--           V_File_Name :='JSE_' || V_Index_Family || '_Sub_Series_%%%%%.ser';
--        END IF;
    --
          V_File_Name := G_File_Def(G_Comp).Constant_File_Name;
          V_Copy_Name := G_File_Def(G_Comp).Dated_File_name_Format;
          V_Date_Format := G_File_Def(G_Comp).Date_Format;
-- SSI - 08JAN2008 - End modif.
          Pgmloc  := 3150;
          Index_Reports.Sub_Series_Value
                  ( NULL
                  , C_Data_Supplier_Code
                  , V_IV_Del_Time
                  , V_Start_Date
                  , V_End_Date
                  , V_Date_Format
                  , V_File_Name
-- SSI - 26NOV2007
                  , V_Copy_Name
                  , V_Success
                  , V_Message);
          IF V_Success THEN
             Pgmloc  := 3160;
--           V_Num_Files := V_Num_Files + 1;
--           V_Out_Files(V_Num_Files) := V_File_Name;
--           Pricing_Msg_Rec.Value_01 := V_Num_Files;
             Pricing_Msg_Rec.Value_01 := G_Num_Files;
          ELSE
             V_Message := 'Error generating Sub_Series_Value-' || V_Message;
             GOTO GET_OUT;
          END IF;
          Pgmloc  := 3170;
          Populate_Client_Output(V_Fri_Client
                                ,V_Product
--                          -- SSI - 15MAY2008
                                ,V_Sub_Product
                                ,G_Comp
                                ,V_Date
                                ,V_Success
                                ,V_Message);
          IF NOT V_Success THEN GOTO GET_OUT; END IF;
          Pgmloc  := 3180;
       END IF;
--
       G_Comp := 'T';
       IF G_File_Def.EXISTS(G_Comp)
       THEN  --{
          Pgmloc  := 3190;
--        V_Copy_Name := NULL;
--        IF V_File_Naming = C_Gen_Naming
--        THEN
--           V_File_Name := V_Clt || '_code_change.dat';
--           V_Copy_Name :='JSE_' || V_Index_Family || '_Code_Change_%%%%%.not';
--        ELSIF V_File_Naming = C_Fmt_Naming AND
--              V_Index_Family IS NOT NULL
--        THEN
--           V_File_Name :='JSE_' || V_Index_Family || '_Code_Change_%%%%%.not';
--        END IF;
--        IF   V_Start_Date = V_End_Date
--         AND TO_CHAR( V_Start_Date, 'D' ) IN ( 1, 7 )
--        THEN
--           GOTO Skip_Rpt;
--        END IF;
          V_File_Name := G_File_Def(G_Comp).Constant_File_Name;
          V_Copy_Name := G_File_Def(G_Comp).Dated_File_name_Format;
          V_Date_Format := G_File_Def(G_Comp).Date_Format;
-- SSI - 08JAN2008 - End modif.
          Pgmloc  := 3200;
          Index_Reports.Ticker_Changes
                  ( NULL
                  , C_Data_Supplier_Code
                  , V_CO_Del_Time
                  , V_Start_Date
                  , V_End_Date
                  , V_Date_Format
                  , V_File_Name
                  , V_Copy_Name
                  , V_Success
                  , V_Message);
          IF V_Success THEN
             Pgmloc  := 3210;
--           V_Num_Files := V_Num_Files + 1;
--           V_Out_Files(V_Num_Files) := V_File_Name;
--           Pricing_Msg_Rec.Value_01 := V_Num_Files;
             Pricing_Msg_Rec.Value_01 := G_Num_Files;
          ELSE
             V_Message := 'Error generating File for Ticker changes ' || V_Message;
             GOTO GET_OUT;
          END IF;
          Pgmloc  := 3220;
          Populate_Client_Output(V_Fri_Client
                                ,V_Product
--                          -- SSI - 15MAY2008
                                ,V_Sub_Product
                                ,G_Comp
                                ,V_Date
                                ,V_Success
                                ,V_Message);
          IF NOT V_Success THEN GOTO GET_OUT; END IF;
          Pgmloc  := 3230;
       END IF; --}
<<Skip_Rpt>>
-- <<GET_OUT>>
       NULL;
-- Modification - JSL - 16 janvier 2008
-- Code deplace des que nous obtenons le mode
-- -- SSI - 15JAN2008
--    ELSIF V_Mode = 'SEND'
--    THEN
--       Send_Compressed_Files(P_Program_Id
--                            ,V_Date
--                            ,V_Success
--                            ,V_Message);
--       IF NOT V_Success THEN GOTO GET_OUT; END IF;
--    ELSE
--       Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'040'
--                                        , 'N'
--                                        , V_Client_Code
--                                        , V_Message
--                                        );
--       V_Success := FALSE;
--       GOTO Get_Out;
--    END IF;  --{}
-- Fin de modification - JSL - 16 janvier 2008
-- -- SSI - 08JAN2008
-- Remove AFT process
--
--  Call AFT_Process.AFTMake
--
-- Pgmloc  := 3240;
-- IF V_AFT_Flag='Y'
--    AND Constants.Get_DB_Mode = 'PROD'
--    AND V_Num_Files >= 1
-- THEN
--   Pgmloc  := 3250;
--   FOR i IN 1 .. V_Num_Files
--   LOOP
--      --To Check that file name already doesn't have path in it
--      IF instr(V_Out_Files(i), Constants.Get_Dir_Seperator) = 0 THEN
--         Pgmloc := 3260;
--         AFT_Process.AFTMAKE('FILE'
--                         , Out_File_Path || G_Dir_Seperator || V_Out_Files(i)
--                         ,V_Success
--                         ,V_Message);
--      ELSE
--         Pgmloc  := 3270;
--         AFT_Process.AFTMAKE('FILE'
--                           ,V_Out_Files(i)
--                           ,V_Success
--                           ,V_Message);
--      END IF;
--      IF NOT V_Success THEN
--        Pricing_Msg_Rec.Err_Text := 'Problem in AFT file ' || V_Out_Files(i)
--                                     || ' ' || V_Message;
--        GOTO Get_Out;
--      END IF;
--   END LOOP;
-- END IF;
   V_Success := TRUE;
   ----------------------------
   /*   Update pricing_msgs..*/
   ----------------------------
<<GET_OUT>>
-- SSI - 30OCT2008
-- Reset rerun date and set last run date
-- SSI - 17JUN2009 
-- UPDATE product_running_parms
-- SET last_run_date  = sysdate,
--     rerun_date = NULL
-- WHERE program_id = p_program_id;
-- SSI - 17JUN2009 
--
   IF V_Success
   THEN
      Pricing_Msg_Rec.Msg_Text_2      := 'Successfully completed ';
--    SSI - 17JUN2009
--    Pricing_Msg_Rec.Successfull_Flag:= 'Y';
      IF NOT G_On_Server THEN  --{
         Pricing_Msg_Rec.Successfull_Flag := 'Y';
      END IF;
--    SSI - 17JUN2009 - end of modif
   ELSE
      Pricing_Msg_Rec.Err_Text := '@pgmloc ' || pgmloc || '-' || V_Message;
   END IF;
   Pricing_Msg_Rec.Value_01 := G_Num_Files;
   Pricing_Msg_Rec.Value_02 := G_Num_Del;
-- SSI - 09JAN2009
   Pricing_Msg_Rec.Value_03 := V_Not_Found_Rec.COUNT;
-- SSI - 09JAN2009 - End of modif.
   P_Success := V_Success;
   P_Message := V_Message;
   Global_File_Updates.Update_Pricing_Msgs
            (G_Program_Id,
             G_System_Date,
             TRUE,
             Pricing_Msg_Rec);
   Commit;
--
-- SSI - 17JUN2009 
--  Reset only rerun date and after update of Pricing_Msgs
-- SSI - 19JUN2009 
-- Add condition to reset rerun date only if run from server
   IF NOT G_On_Server THEN
      UPDATE product_running_parms
      SET rerun_date = NULL
      WHERE program_id = p_program_id
      AND    Rerun_Date IS NOT NULL;
   END IF;
   Commit;
-- SSI - 17JUN2009 - end of modif.
EXCEPTION
WHEN OTHERS
THEN
   V_Message := SUBSTR('Error @pgmloc ' || pgmloc || '-' ||
                 SUBSTR( SQLERRM( SQLCODE ), 1, 512 ) , 1 , 512);
END For_Client;
--------------------------------------------------------------------------------
END Index_Reports;
/
show errors
