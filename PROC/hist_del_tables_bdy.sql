CREATE OR REPLACE PACKAGE BODY &&owner..Hist_Del_Tables IS
------------------------------------------------------------------------
-- Created By : Sekhar Aka (15-MAR-2004)
--
-- This package keeps the Historical tables for the given n number of
-- days and drops the rest of the tables.
--
-- Modified:
-- JSL - 20 apr 2004 - Change from user_objects to all_objects.
-- JSL - 26 may 2005 - Use version of initialize message with date of prices
-- JSL - 17 apr 2008 - Keep the table datestamp as run date, without counting
--                     them as number of days.  We need to keep the universe
--                     as it was BEFORE the first maintenance of the day, not
--                     as it was before the last maintenance of the day.  The
--                     screen to rerun the dividend deletes the tables that
--                     are to be recreated.  The maintenance script already
--                     have provision for table that are existing.
-- JSL - 31 Oct 2015 - OASIS-6221
--                   - Add the purge option when dropping a table.
------------------------------------------------------------------------
Pgmloc           NUMBER;
Success          BOOLEAN       := TRUE;
G_System_Date    DATE;
C_Oasis_Message  VARCHAR2(30)  := 'PROC-HIST_DEL_TABLES-';
--
TYPE Date_Rec IS RECORD ( Table_Name     All_Objects.Object_Name%TYPE
                        , Prc_Msgs_Value NUMBER
                        , Price_Date     DATE
                        , Keep_Flag      VARCHAR2(1)
                        );
--
TYPE Price_Date_Tab IS TABLE OF Date_Rec INDEX BY BINARY_INTEGER;
Price_Date_Table    Price_Date_Tab;
--
------------------------------------------------------------------------
FUNCTION Get_Max_Date( P_DATE IN DATE
                     ) RETURN DATE IS
--
V_Temp_Date     DATE;
V_Index         BINARY_INTEGER;
V_Date          DATE;
--
BEGIN
  --{
  V_Temp_Date  := NULL;
  V_Index := Price_Date_Table.FIRST;
  --
  WHILE ( V_Index IS NOT NULL )
  LOOP
    --{
    V_Date := Price_Date_Table(V_Index).Price_Date;
    --
    IF V_Date < P_Date
    THEN
      IF V_Temp_Date IS NULL
      THEN
        V_Temp_Date := V_Date;
      ELSIF V_Date > V_Temp_Date
      THEN
        V_Temp_Date := V_Date;
      END IF;
    END IF;
    --
    V_Index := Price_Date_Table.NEXT(V_Index);
    --}
  END LOOP;
  --
  RETURN V_Temp_Date;
  --}
END;
--
------------------------------------------------------------------------
PROCEDURE Delete_Tables ( P_Program_Id        IN  NUMBER
                        , P_Histo_Program_Id  IN  NUMBER
                        , P_Alt_Client_Code   IN  VARCHAR2
                        , P_Table_Date_Fmt    IN  VARCHAR2
                        , P_Max_Days          IN  INTEGER
                        , P_Log_File_Name     IN  VARCHAR2
                        , P_Run_Date          IN  DATE
                        , P_Success           OUT BOOLEAN
                        , P_Message           OUT VARCHAR2
                        ) IS

--
CURSOR Get_Wrk_Tab_Patterns( inp_prd_code IN VARCHAR2
                           , inp_clt_code IN VARCHAR2  ) IS
SELECT WTP.Prc_Msgs_Value
     , LENGTH(inp_clt_code||'_'||WTP.Code||'_%') Plength
     , UO.Object_Name
  FROM WORKING_TABLES_PATTERNS WTP
     , ALL_OBJECTS UO
 WHERE PRODUCT_CODE = inp_prd_code
   AND OBJECT_NAME LIKE inp_clt_code||'_'||WTP.Code||'_%'
   AND OWNER = USER
   AND OBJECT_TYPE = 'TABLE';
--
CURSOR Get_Prd_Running_Parms ( inp_pgm_id IN NUMBER ) IS
SELECT *
  FROM PRODUCT_RUNNING_PARMS
 WHERE PROGRAM_ID = inp_pgm_id;
--
Pricing_Msgs_Rec     Pricing_Msgs%ROWTYPE;
Wrk_Tab_Pattern_Rec  Working_Tables_Patterns%ROWTYPE;
Prd_Running_Parm_Rec Product_Running_Parms%ROWTYPE;
--
V_Client_Code        Clients.Code%TYPE;
V_Fri_Client         Clients.Fri_Client%TYPE;
V_Table_Name         VARCHAR2(50);
V_Price_Date         DATE;
V_Index              INTEGER;
V_Curr_Index         BINARY_INTEGER;
V_Last_Index         BINARY_INTEGER;
V_Max_Price_Date     DATE;
V_Max_Count          INTEGER;
V_Max_Days           INTEGER;
V_Sql_Stmt           VARCHAR2(512);
V_Run_Date           DATE;
V_Temp_Date          DATE;
V_Days               INTEGER;
V_Tab_Length         INTEGER;
Log_File             Utl_File.File_Type;
Friday_Found         BOOLEAN;
Monthend_Found       BOOLEAN;
Update_Break_Pts     BOOLEAN;
--
BEGIN
  --{
  pgmloc := 05;
  --
  V_Client_Code    := NULL;
  P_Message        := NULL;
  P_Success        := TRUE;
  Update_Break_Pts := FALSE;
  G_System_Date    := SYSDATE;
  V_Run_Date       := P_Run_Date;
  V_Max_Days       := P_Max_Days;
  V_Curr_Index     := NULL;
  V_Max_Count      := 0;
  --
  Global_File_Updates.Initialize_Pricing_Msgs ( P_Histo_Program_Id,
                                                G_System_Date,
                                             -- Added - JSL - 26 may 2005
                                                P_Run_Date,
                                             -- End of addition
                                                Update_Break_Pts,
                                                Pricing_Msgs_Rec );
  ---------------------------------------
  --  Open the Log file in the Write mode
  ---------------------------------------
  pgmloc := 10;
  Load_Supplier_Rules.Open_File( P_Log_File_Name
                               , Constants.Get_Dir_Name
                               , 'W'
                               , Pricing_Msgs_Rec
                               , Log_File
                               , Success
                               );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 15;
  load_supplier_Rules.Write_file( Log_File
                                , 'Started at '
                                || TO_CHAR( G_System_Date, 'dd-mon-yyyy hh24:mi:ss' )
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  pgmloc := 20;
  load_supplier_Rules.Write_file( Log_File
                                , 'Program Id:'||TO_CHAR(P_Program_id)
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 25;
  OPEN Get_Prd_Running_Parms(P_Program_Id);
  FETCH Get_Prd_Running_Parms INTO Prd_Running_Parm_Rec;
  pgmloc := 15;
  CLOSE Get_Prd_Running_Parms;
  --
  IF P_Alt_Client_Code IS NOT NULL
  THEN
    --{
    V_Client_Code := P_Alt_Client_Code;
    --
    pgmloc := 30;
    SELECT FRI_CLIENT
      INTO V_Fri_Client
      FROM CLIENTS
     WHERE CODE = V_Client_Code;
  ELSE --}{
    pgmloc := 35;
    SELECT CODE
      INTO V_Client_Code
      FROM CLIENTS
     WHERE FRI_CLIENT = Prd_Running_Parm_Rec.Fri_Client;
    --
    V_Fri_Client := Prd_Running_Parm_Rec.Fri_Client;
    --}
  END IF;
  --
  pgmloc := 45;
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
  Pricing_Msgs_Rec.Msg_Text         := 'HIST_DEL_TABLES '
                                    || V_Client_Code
                                    || ' ( Keep '
                                    || P_Max_Days      ||' Days )';
  Pricing_Msgs_Rec.Msg_Text_2       := 'Program in progress';
  --
  IF V_Run_Date > Constants.Get_Site_Date
  THEN
    -- 'Cannot simulate future execution';
    pgmloc := 50;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'010'
                                     , 'N'
                                     , TO_CHAR(V_Run_Date,'DD-MON-YYYY')
                                     , Pricing_Msgs_Rec.Err_Text
                                     );
    Success := FALSE;
    pgmloc := 55;
    load_supplier_Rules.Write_file( Log_File
                                  , Pricing_Msgs_Rec.Err_Text
                                  , Pricing_Msgs_Rec
                                  , Success
                                  );
    IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
    GOTO GET_OUT;
  END IF;
  --
  pgmloc := 60;
  Global_File_Updates.Update_Pricing_Msgs ( P_Histo_Program_Id,
                                            G_System_Date,
                                            Update_Break_Pts,
                                            Pricing_Msgs_Rec );
  --
  pgmloc := 65;
  Load_supplier_Rules.Write_File( Log_File
                                , Pricing_Msgs_Rec.Msg_Text
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  V_Index := 1;
  FOR Wrk_Tab_Pattern_Rec IN
                 Get_Wrk_Tab_Patterns( Prd_Running_Parm_Rec.Product_Code
                                     , V_Client_Code )
  LOOP
    --{
    --------------------------------------------------------------
    -- Fetch Work Table Pattern, table name for the given product.
    --------------------------------------------------------------
    pgmloc := 70;
    --
    BEGIN
      --{
      pgmloc := 85;
      V_Price_Date := TO_DATE( SUBSTR( Wrk_Tab_Pattern_Rec.Object_Name
                                     , Wrk_Tab_Pattern_Rec.Plength
                                     )
                             , P_Table_Date_Fmt
                             );
        --
      EXCEPTION
        WHEN OTHERS
        THEN
          GOTO NEXT_TABLE;
      --}
    END;
    --
    pgmloc := 90;
    Price_Date_Table(V_Index).Table_Name := Wrk_Tab_Pattern_Rec.Object_Name;
    Price_Date_Table(V_Index).Prc_Msgs_Value := Wrk_Tab_Pattern_Rec.Prc_Msgs_Value;
    Price_Date_Table(V_Index).Price_Date := V_Price_Date;
    Price_Date_Table(V_Index).Keep_Flag  := 'N';
-- Ajout - JSL - 17 avril 2008
    IF V_Price_Date = V_Run_Date
    THEN
      Price_Date_Table(V_Index).Keep_Flag  := 'Y';
    END IF;
-- Fin d'ajout - JSL - 17 avril 2008
    V_Index := V_Index + 1;
  <<NEXT_TABLE>>
    NULL;
    --}
  END LOOP;
  --
  V_Days := 1;
  -----------------------------------
  -- Keep the number of days required
  -----------------------------------
  WHILE V_Days <= V_Max_Days
  LOOP
    --{
    pgmloc := 105;
    V_Max_Price_Date := Get_Max_Date(V_Run_Date);
    --
    IF V_Max_Price_Date IS NULL THEN EXIT; END IF;
    --
    V_Curr_Index := Price_Date_Table.FIRST;
    WHILE ( V_Curr_Index IS NOT NULL )
    LOOP
      --{
      pgmloc := 110;
      V_Temp_Date := Price_Date_Table(V_Curr_Index).Price_Date;
      IF V_Max_Price_Date = V_Temp_Date
      THEN
        pgmloc := 115;
        Price_Date_Table(V_Curr_Index).Keep_Flag := 'Y';
      END IF;
      --
      pgmloc := 120;
      V_Curr_Index := Price_Date_Table.NEXT(V_Curr_Index);
      --}
    END LOOP;
    --
    V_Run_Date := V_Max_Price_Date;
    V_Days := V_Days + 1;
    --}
  END LOOP;
  -------------------------------------------
  -- Drop the tables that are not to be kept.
  -------------------------------------------
  V_Curr_Index := Price_Date_Table.FIRST;
  --
  WHILE ( V_Curr_Index IS NOT NULL )
  LOOP
    --{
    pgmloc := 125;
    V_Sql_Stmt := NULL;
    --
    IF Price_Date_Table(V_Curr_Index).Keep_Flag = 'N'
    THEN
      --{
      pgmloc := 130;
      V_Sql_Stmt := 'DROP TABLE '||USER||'.'||
                    Price_Date_Table(V_Curr_Index).Table_Name
-- Ajout - JSL - 31 octobre 2015
                 || ' PURGE';
-- FIn d'ajout - JSL - 31 octobre 2015
      pgmloc := 135;
      EXECUTE IMMEDIATE V_Sql_Stmt;
      pgmloc := 140;
      Load_Supplier_Rules.Write_File( Log_File
-- Modification - JSL - 31 octobre 2015
--                                  , '	Dropped Table: '
                                    , '	Dropped and purged Table: '
-- Fin de modification - JSL - 31 octobre 2015
                                    ||Price_Date_Table(V_Curr_Index).Table_Name
                                    , Pricing_Msgs_Rec
                                    , Success
                                    );
      IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
      --
      pgmloc := 145;
      IF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 1 THEN
        Pricing_Msgs_Rec.Value_01:=NVL(Pricing_Msgs_Rec.Value_01,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 2 THEN
        Pricing_Msgs_Rec.Value_02:=NVL(Pricing_Msgs_Rec.Value_02,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 3 THEN
        Pricing_Msgs_Rec.Value_03:=NVL(Pricing_Msgs_Rec.Value_03,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 4 THEN
        Pricing_Msgs_Rec.Value_04:=NVL(Pricing_Msgs_Rec.Value_04,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 5 THEN
        Pricing_Msgs_Rec.Value_05:=NVL(Pricing_Msgs_Rec.Value_05,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 6 THEN
        Pricing_Msgs_Rec.Value_06:=NVL(Pricing_Msgs_Rec.Value_06,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 7 THEN
        Pricing_Msgs_Rec.Value_07:=NVL(Pricing_Msgs_Rec.Value_07,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 8 THEN
        Pricing_Msgs_Rec.Value_08:=NVL(Pricing_Msgs_Rec.Value_08,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 9 THEN
        Pricing_Msgs_Rec.Value_09:=NVL(Pricing_Msgs_Rec.Value_09,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 10 THEN
        Pricing_Msgs_Rec.Value_10:=NVL(Pricing_Msgs_Rec.Value_10,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 11 THEN
        Pricing_Msgs_Rec.Value_11:=NVL(Pricing_Msgs_Rec.Value_11,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 12 THEN
        Pricing_Msgs_Rec.Value_12:=NVL(Pricing_Msgs_Rec.Value_12,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 13 THEN
        Pricing_Msgs_Rec.Value_13:=NVL(Pricing_Msgs_Rec.Value_13,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 14 THEN
        Pricing_Msgs_Rec.Value_14:=NVL(Pricing_Msgs_Rec.Value_14,0)+1;
      ELSIF Price_Date_Table(V_Curr_Index).Prc_Msgs_Value = 15 THEN
        Pricing_Msgs_Rec.Value_15:=NVL(Pricing_Msgs_Rec.Value_15,0)+1;
      END IF;
      --}
    END IF;
    --
    pgmloc := 150;
    V_Curr_Index := Price_Date_Table.NEXT(V_Curr_Index);
    --}
  END LOOP;
  --
  pgmloc := 155;
  load_supplier_Rules.Write_file( Log_File
                                , 'Terminating at '
                                || TO_CHAR( SYSDATE, 'dd-mon-yyyy hh24:mi:ss' )
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 160;
  Load_supplier_Rules.File_Close( Log_File
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  Update_Break_Pts := TRUE;
  Pricing_Msgs_Rec.Msg_Text_2       := 'Tables Successfully Deleted';
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  GOTO UPD_PRC_MSG;
--
<<GET_OUT>>
  Pricing_Msgs_Rec.Msg_Text_2       := 'Error in Deleting. Problem At Pgmloc : ' || TO_CHAR( Pgmloc );
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
  --
  pgmloc := 165;
  Load_supplier_Rules.File_Close( Log_File
                                , Pricing_Msgs_Rec
                                , Success
                                );
  P_Message := Pricing_Msgs_Rec.Err_Text;
  P_Success := FALSE;
--
<<UPD_PRC_MSG>>
  pgmloc := 170;
  Global_File_Updates.Update_Pricing_Msgs ( P_Histo_Program_Id,
                                            G_System_Date,
                                            Update_Break_Pts,
                                            Pricing_Msgs_Rec );
  COMMIT;
  --
--
EXCEPTION
  WHEN OTHERS THEN
    P_Message := SUBSTR( 'Problem At Pgmloc : '
                     || TO_CHAR( Pgmloc )
                     || ' '
                     || SUBSTR( SQLERRM( SQLCODE ), 1, 255 )
                     , 1
                     , 255
                     );
    P_Success := FALSE;
  --}
END;
--
-----------------------------------------------------------------------
END Hist_Del_Tables;
/
show errors
