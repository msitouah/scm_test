CREATE OR REPLACE PACKAGE BODY &&owner..Automate_Div_Restart 
IS
--------------------------------------------------------------------------------
--
--  Created by Phebe -- 27 Apr 2005.
--
--  Modified:
--  JSL - 19Aug-2015 - OASIS-5769
--                   - Add missing () around table definition id drop tables.
--                     Should be limited to the owner.
--  JSL - 31Oct2015  - OASIS-6221
--                   - Add the purge option when dropping a table.
--
--------------------------------------------------------------------------------
--
--
--
G_Product_Code          Products.Code%TYPE;
G_Site_Date             DATE := Constants.Get_Site_Date;
G_Date_Fmt              VARCHAR2(8) := 'YYYYMMDD';
Pricing_Msg_Rec         Pricing_Msgs%ROWTYPE;
--
CURSOR Cur_Get_Pattern  IS
SELECT Code
FROM   Working_Tables_Patterns
WHERE  Product_Code = G_Product_Code;
--
--------------------------------------------------------------------------------
-- The procedure checks if the extract is a full_extract or a partial.
-- It checks if there are tables existing with the date before the 
-- given 'p_rerun_date'. If no tables available, then it is a full_extract.
-- 
PROCEDURE Is_It_Full_Extract
         (P_Client_Code      IN   Clients.Code%TYPE,
          P_Product_Code     IN   Products.Code%TYPE,
          P_User_Name        IN   User_List.User_Name%TYPE,
          P_Rerun_Date       IN   DATE,
          P_Full_Extract     OUT  BOOLEAN,
          P_Success          OUT  BOOLEAN,
          P_Message          OUT  VARCHAR2) 
   --
   IS
   --
   V_Run_Date         DATE;
   V_BlockStr         VARCHAR2(300);
   V_Message          VARCHAR2(250);
   V_Tab_Name         All_Tables.Table_Name%TYPE;
   V_Tab_Date         VARCHAR2(8);
   V_Success          BOOLEAN;
   V_Table_Ct         Integer;     
   V_Total_Tables     Integer;     
   V_Patt_Ct          Integer;
   V_Patt_Code        Working_Tables_Patterns.Code%TYPE;
   --
   TYPE  Ref_Curs_Type   IS  REF CURSOR;
   --
   Cur_Get_Prev_Tab      Ref_Curs_Type;
   --
BEGIN
     --
     P_Success        := FALSE;
     P_Message        := '';
     G_Product_Code   := P_Product_Code;
     V_Patt_Ct        := 0;
     V_Total_Tables   := 0;           
     -----------------------------------------------------------
     -- If the Rerun_Date is not given, the extract will be done
     -- for Site_Date.
     ----------------------------------------------------------- 
     IF P_Rerun_Date IS NULL THEN
         V_Run_Date := G_Site_Date;
     ELSE
         V_Run_Date := P_Rerun_Date;
     END IF;
     -----------------------------------------------------------
     -- Getting Table Patterns for the given product Code (DVS)
     -----------------------------------------------------------
     OPEN Cur_Get_Pattern;
     LOOP
         FETCH Cur_Get_Pattern INTO V_Patt_Code;
         EXIT WHEN Cur_Get_Pattern%NOTFOUND;
         --
         V_Patt_Ct   := V_Patt_Ct + 1;
         V_Table_Ct  := 0;
         -----------------------------------------------------------
         -- Setting Table_Name with Client_Code and the Pattern.
         -----------------------------------------------------------
         V_Tab_Name  := P_Client_Code||'_'||V_Patt_Code;
         -----------------------------------------------------------
         -- Checking if the tables exist for given client_pattern
         -- and the date before the rerun_date.
         ----------------------------------------------------------- 
         V_Blockstr := 'SELECT 1 FROM All_Tables '||
                       'WHERE  Owner = '          ||''''||
                                       P_User_Name||''''||
                       ' AND  (Table_Name LIKE  ' ||''''||
                                       V_Tab_Name ||'_%'||''''||
                       ' AND   TO_DATE(SUBSTR(Table_Name,-8),'||''''||
                                       G_Date_Fmt ||''''||
                                       ') < '     ||''''||
                                       V_Run_Date ||''''||')';  
         --
         OPEN Cur_Get_Prev_Tab FOR V_Blockstr;
         FETCH Cur_Get_Prev_Tab INTO V_Table_Ct;
         CLOSE Cur_Get_Prev_Tab;
         --
         IF V_Table_Ct != 0
         THEN
             V_Total_Tables := V_Total_Tables + V_Table_Ct;
         END IF;
     END LOOP;
     CLOSE Cur_Get_Pattern;
     ------------------------------------------------------------------
     -- If there're no tables existing or if a table doesnot exist 
     -- for a pattern,a Full_Extract should be done.
     ------------------------------------------------------------------
     IF V_Total_Tables = 0  OR 
        V_Total_Tables < V_Patt_Ct
     THEN
         P_Full_Extract := TRUE;
     END IF;
     --
     P_Success := TRUE;
     --
EXCEPTION
    WHEN OTHERS THEN
      IF Cur_Get_Pattern%ISOPEN THEN
          CLOSE Cur_Get_Pattern;
      END IF;
      P_Message := SUBSTR(SQLERRM(SQLCODE),1,150);
 -- 
END Is_It_Full_Extract;        
--------------------------------------------------------------------------------
-- This procedure will drop the dividend extract tables that have date(s) i
-- greater than or equal to the given 'p_rerun_date'. 
-- It outputs the number of tables dropped.
--
PROCEDURE Drop_Tables
             (P_Client_Code      IN   Clients.Code%TYPE,
              P_Product_Code     IN   Products.Code%TYPE,
              P_User_Name        IN   User_List.User_Name%TYPE,
              P_Rerun_Date       IN   DATE,
              P_Tabs_Dropped     OUT  INTEGER,
              P_Success          OUT  BOOLEAN,
              P_Message          OUT  VARCHAR2)
   --
   IS
   --
   V_Run_Date         DATE;
   V_BlockStr         VARCHAR2(300);
   V_BlockStr2        VARCHAR2(300);
   V_Message          VARCHAR2(250);
   V_Tab_Name         All_Tables.Table_Name%TYPE;
   V_Table_To_Drop    All_Tables.Table_Name%TYPE;
   V_Success          BOOLEAN;
   V_Found_Ct         Integer;
   V_Ct               Integer;
   V_Patt_Code        Working_Tables_Patterns.Code%TYPE;
   --
   TYPE  Ref_Curs_Type   IS  REF CURSOR;
   --
   Cur_Get_Tab      Ref_Curs_Type;
   --
BEGIN
     --
     P_Success        := FALSE;
     P_Message        := '';
     G_Product_Code   := P_Product_Code;
     P_Tabs_Dropped   := 0;
     V_Ct             := 0;
     --
     IF P_Rerun_Date IS NULL THEN
         V_Run_Date   := G_Site_Date;
     ELSE
         V_Run_Date   := P_Rerun_Date;
     END IF;
     --
     OPEN Cur_Get_Pattern;
     LOOP
         FETCH Cur_Get_Pattern INTO V_Patt_Code;
         EXIT WHEN Cur_Get_Pattern%NOTFOUND;
         --
         V_Ct        := V_Ct + 1;
         V_Found_Ct  := 0;           
         V_Tab_Name  := P_Client_Code||'_'||V_Patt_Code;
         ---------------------------------------------------------------
         -- Selecting dividend extract tables with dates greater than or
         -- equal to the run_date.
         ---------------------------------------------------------------
         V_Blockstr := 'SELECT Table_Name FROM All_Tables '||
                       'WHERE  Owner = '          ||''''||
                                       P_User_Name||''''||
-- Modification - JSL - 19 aout 2015
--                     ' AND  (Table_Name LIKE  ' ||''''||
                       ' AND ((Table_Name LIKE  ' ||''''||
-- Fin de modification - JSL - 19 aout 2015
                                       V_Tab_Name ||'_%'||''''||
                       ' AND   TO_DATE(SUBSTR(Table_Name,-8),'||''''||
                                       G_Date_Fmt ||''''||
                                       ') >= '    ||''''||
                                       V_Run_Date ||''''||
                      ') OR Table_Name = '        ||''''|| 
-- Modification - JSL - 19 aout 2015
--                                      V_Tab_Name||'''';
                                        V_Tab_Name||''')';
-- Fin de modification - JSL - 19 aout 2015
         --
         OPEN Cur_Get_Tab FOR V_Blockstr;
         LOOP
              FETCH Cur_Get_Tab INTO V_Table_To_Drop;
              EXIT WHEN Cur_Get_Tab%NOTFOUND;
              -------------------------------------------------
              -- Dropping tables...
              -------------------------------------------------
              V_Blockstr2 := 'DROP TABLE '||P_User_Name||'.'||
                              V_Table_To_Drop
-- Ajout - JSL - 31 octobre 2015
                          || ' PURGE';
-- Fin d'ajout - JSL - 31 octobre 2015
              --
              EXECUTE IMMEDIATE V_Blockstr2;
              --
              P_Tabs_Dropped := P_Tabs_Dropped + 1;
              --
         END LOOP;
         CLOSE Cur_Get_Tab;
         --
     END LOOP;
     CLOSE Cur_Get_Pattern;
     --
     P_Success := TRUE;
     --
EXCEPTION
    WHEN OTHERS THEN
      IF Cur_Get_Pattern%ISOPEN THEN
          CLOSE Cur_Get_Pattern;
      END IF;
      --
      P_Message := SUBSTR(SQLERRM(SQLCODE),1,150);
 -- 
END Drop_Tables;       
----------------------------------------------------------------------- 
END Automate_Div_Restart;  
/
show err
