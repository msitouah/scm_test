CREATE OR REPLACE PACKAGE BODY &&owner..Client_Index_Files
AS
--
-- SSI - 30SEP2008 - New field FILE_DATE for all product component code.
--                 - Remove coding on MSCI tables. (To be continued)
--
-- SSI - 01OCT2008 - Fix date problem for index having service @t and @t+1.
--                 - Join with table Successfull_Flags to check for true
--                   failure.
--                 - Rewrite 3 cursors to optimize run time.
--
-- SSI - 07OCT2008 - Add supplier ticker changes.
--                 - Complete rewrite procedure Generate_Ticker_Changes to
--                   optimize run time.
--
-- SSI - 17OCT2008 - Fixes for new index client.
--
-- SSI - 22OCT2008 - Interpret_field - Add new fields ISIN_ID,CUSIP_ID,SEDOL_ID
--                   for supp_index_constituents and Index_Securities records
--                 - Add ticker changes for the additional ID fields
--
-- SSI - 27OCT2008 - Call index custom Holiday function:
--                   Load_Ind_SUpplier_Rules.IS_It_Open
--
-- SSI - 30OCT2008 - Clean up coding on MSCI.
--                 - Addition and defining 2 benchmark files
--                 - New fields for ICB code - it is always assumed that the
--                   ICB code is from column Supp_Index_Constituents.Sector
--                   if given by supplier.
--                 - Add possibility to break by index type on Exchange_rates
--                   characteristics file.
--                 - 3 New fields (special constant fields) added to
--                   index_Series fields interpretations
--                   - C_BNCH_FLAG
--                   - C_WGT_FLAG
--                   - C_RTN_FLAG
--
-- SSI - 01DEC2008 - For constituent level files, create files only on
--                   index types which client wants constituent level info
--                   (ie. those with
--                    Client_Index_Suppliers.constituents_Flag='Y')
--
-- ACB - 28JAN2008 - Add new component for Risk Number Equities (N)
--
-- ACB - 23FEB2009 - Add a new clause for all indices that
--     don't come in index files. In the load, we force an entry in index_series
--     table with type FAKE_INDEX. This indices are not supposed to write in the file
--
-- SSI - 03MAR2009 - Set security_Return_4 and Security_Return_5 to 0 on holiday
--                 - Temporarily hardcode GBI_CASH index type for JPM index to
--                   be excluded from the market cap file.
--                 - Possibility to break Risk Number file by index type for
--                   component 'K'
--
-- SSI - 20MAR2009 - Now calling Global_File_Updates.Update_Pricing_Msgs to
--                   update Pricing_Msgs.
--                 - Update only Rerun_Date and do it after the last update
--                   on Pricing_Msgs beffore exiting.
--
-- SSI - 06APR2009 - Extract done by holiday class as indices for one supplier
--                   can have different holiday classes. Management of
--                   extracting period and holiday is done per holiday class.
--                 - It is assumed that all indices within the same
--                   index type (family) have the same holiday class code.
-- SSI - 16APR2009 - New delimeter type (SDVEU).
--
-- SSI - 29APR2009 - Set rerun date if run other than site date and date not yet
--                   set.
--
-- ACB - 07MAY2009 - Add new function Get_Exchange_Rate_Source
--                 - change the fileds Series Identifier and Series Long name:
--                       XXX/XXX followed by Exchange Rate Source
--                 - Add new field, EXCH_SOURCE (Exchange rate source),
--                     on exchange rate characteristic and index series
--                     characteristic file
--                 - Add new fields STP_CUSIP_ID,STP_ISIN_ID,STP_SEDOL_ID
--                   for supp_index_constituents.
--
-- SSI - 14MAY2009 - Interpret_Fields of SUPP_INDEX_CONSTITUENTS record
--                   - Modify SUPP_COUNTRY,SUPP_CTY_NAME
--                 - New Procedure for a new ticker_change file based on new
--                   and GICS ticker changes.
--
-- SSI - 16JUN2009 - OASIS Ticker changes was not reported correctly.
--
-- SSI - 17JUN2009 - Pricing_Msgs created and updated if run on server.
--
-- SSI - 19JUN2009 - Add condition to reset rerun date only if not running from server
--
-- SSI - 23JUN2009 - Change constituent level file definitions. Add exchange rates characterictics/Values and Ticker changes.
--
--
-- ACB - 04JUL2009 - The file name will be broke by the new column
--                      index_family instead of index_type
--
-- SSI - 29JUN2009 - Wait till all records are fetched. Get Statpro GICS only if no custom GICS from any supplier
--
-- SSI - 03JUL2009 - Load_Custom_Columns - Fix assignment of country code and
--                   country name. Both have to be from the same source.
--                 - Remove useless statement in assignemnt of FIELD_15_Name
--                 - Gen_Risk_File_Equities - Remove join on table Supp_Index_Criteria. It is causing duplicates.
--
-- SSI - 24JUL2009 - Fix Security name overflow while assigning to the user definable field Field_12.
--
-- SSI - 03SEP2009 - Optimize extraction on custom fields.
--
-- SSI - 10SEP2009 - Load_Custom_Columns - Force use of INDEX in cursor as the
--                   optimization is not stable from one date to another.
--
-- ACB - 28SEP2009 - When output exchange rates, we skip when the base currency
--                   is equal to currency.
--
-- SSI - 01OCT2009 - For t+1 service, added securities in characteristics have to be dated previous date.
--
-- ACB - 27OCT2009 - Return_Series_Identifier - For DAX supplier, the index code will be built as M_ for market cap index code
--                                              in market cap file.
--
-- SSI - 24NOV2009 - Gen_Risk_File - For IBOX, exclude records from EOM files.
--
-- SSI - 17DEC2009 - Gen_Risk_File - Remove restriction on Weight<>0 (ie. current constituents only)
--
-- SSI - 06JAN2010 - Assignment to V_Prev_ID moved up - Assign only if a risk record is actually found and written
--
-- SSI - 11FEB2010 - Gen_Risk_File and Gen_Risk_File_Equities - Remove duplicate.
--
-- SSI - 22FEB2010 - Gen_Risk_File_Equities: If it's service @T+1, CFW accordingly if week end or holiday.
--
-- ACB - 21APR2010 - enven if the index requested by the client has the constituent flag=Y, the correspondent security is produced
--
-- SSI - 07MAY2010 - Previous fix slow down enormously the extraction. Add hinti
--                   in sub_query to improve performance.
--
-- SSI - 22JUN2010 - Commit after update of Pricing_Msgs.
--
-- ACB - 14JUL2010 - Change the Risk Number's query for bonds. Depends on the index, the RN file is empty.
--
-- SSI - 14SEP2010 - Special string '||##' to denote merge with next field. Delimiter will be ignored.
--                 - Intrepret field COUPON_2 in Index_Securities.
--
-- ACB - 27OCT2010 - Interpret fields added in Risk Number procedures
--
-- SSI - 08NOV2010 - Gen_Securities_File / Gen_Securities_File2 - Do not write CFW records if holiday and weight=0
--
-- SSI - 07JAN2011 - Load_Custom_Columns - Optimize run time by revising the hints.
--
-- APX - 21JAN2011 - Modified to allow skipping Holiday Files
--
-- SSI - 10FEB2011 - Interpret_Fields - For Index_Securities records: Mapping NFLD_70 to NFLD_99, MKT_CAP_2 to MKT_CAP_10.
--                 - Gen_Risk_File: Rewritting Cursor cur_sel_securities.
--                 - Interpret_Field for Supp_Index_Constituent records: New Field_Code PRC_GBP_ILS,CCY_GBP_ILS
--                   to express prices in GBP/ILS if price is in GPX or ILX, else they are the same price and iso_currency.
--
-- ACB - 15FEB2011 - More interpret fields added in Risk Number procedures
--
-- SSI - 15FEB2011 - New function Return_Clt_Series_Identifier - Return client custom index code, and market cap index code
--                                                               If Client_Index_Code IS NULL then default to supplier's Index code
--                                                               If Client_Mk_Index_Code is null, then default to general rule
--                                                               applied to supplier's index code
--                 - New procedure Get_Clt_Series_Names        - Return client custom index Name / Long Name according to the series type.
--                                                               If not defined, then default to supplier's Index name / Short Name
--                 - Modified 2 overloaded procedures - Get_Record_Format for Index_Series and Supp_Index_Constituents records -
--                                                      Interpreting new field CLT_INDEX_ID,CLT_INDEX_NAME,CLT_INDEX_SNAME
--
-- SSI - 17MAR2011 - Interpret_Fields - For Index_Securities records: EUR_PREEUR
--
-- SSI - 24MAR2011 - Actual fri_client of the extract should be passed in the fundtion Return_Clt_Series_Identifier and procedure Get_Clt_Series_Names,
--                   and not the format layout fri_client.
--                 - Generalize the customization of the index code. Whenever a custom index code or name is used, it will be extracted.
--                   Remove new field CLT_INDEX_ID,CLT_IND_NAME,CLT_IND_LNAM
--
-- SSI - 08APR2011 - Bug in Generate_Ticker_Changes - OASIS_ID flags are not tested correctly.
--
-- ACB - 19APR2011 - More interpret fields added in Risk Number procedures
--
-- SSI - 19APR2011 - New inprepreted fields in series characteristics file. They now reflect whether constituent level
--                   is expected for the index.
--                   - C_BNCH_FLAG2
--                   - C_WGT_FLAG2
--                   - C_RTN_FLAG2
--
-- APX - 20APR2011 - For Montly (freq < 365) extract only for the current day (For MSCI monthly extract of Risk #)
--
-- SSI - 06MAY2011 - Format_Field - Interpret formatted number by a given factor given in output_format with a '*'
--                                  followed by a character code
--                                  '*D' means multiply by ten
--                                  '*H' means multiply by hundred
--                                  '*T' means multiply by thousand
--                                  '*M' means multiply by million
--
-- SSI - 26MAY2011 - New procedure Gen_Securities_Char_File_2 - Dumping of security characteristics by file_type, Del_Time.
--                 - More interpreted fields for Index_Securities type record.
--
-- SSI - 07JUL2011 - Allow client to have its own format layout if exists.
--
-- SSI - 22SEP2011 - Give an error if no files produced for the extraction.
--
-- SSI - 26OCT2011 - Interpret_Fields - New Field MKT_CAP_FTS to interpret FTSE market cap. Market Cap for FTSE UK indices
--                   are calculated in GBX. So MKT_CAP_FTS is the market cap in GBP for UK indices. else it's as is.
--
-- SSI - 03NOV2011 - Fix Wrong message code.
--
-- SSI - 28NOV2011 - Gen_Risk_File_Equities - Fix hint in CURSOR to improve performance
--
-- SSI - 07FEB2012 - Close_File - Change parameter p_file_handle to be IN OUT followed by
--                                change in Load_Supplier_Rules.CLose_File
--
-- SSI - 13FEB2012 - Interpret_Fields - For Index_Securities, field_Code 'SEDOL_ID', fix wrong assignment for Sedol
--
-- SSI - 22MAY2012 -  Gen_Risk_File_Equities - Tune query.
--
-- SSI - 22MAY2012 - Format_Field - Interpret new number format #NZ, which is to nullify 0 value.
--
-- SSI - 21JUN2012 - Format_Field - Interpret Simulated and true benchmark returns on series level
--
-- SSI - 24JUL2012 - Format_Field - Date field is already formated. Remove the reformatting.
--
-- SSI - 07AUG2012 - Gen_Series_Value_File - If holiday, zero out returns.
--
-- SSI - 22NOV2012 - Interpret new fields in Index_Series table, NFLD_32, NFLD_67, NFLD_104
--                 - Interpret new fields in Index_Securities, WEIGHT
--                 - Gen_Securities_File - Customized for MSCI - Calc_Field_07 is a return. Zero it out when it is a holiday.
--                 - Gen_Securities_File2 - Customized for MSCI - Calc_Field_07 is a return. Zero it out when it is a holiday.
--
 
-- Miglena - 01.Mar.2013 -- Interpret new field SUPP_ISIN_ID
--                          procedure 'Interpret_Fields (Index_Securities)
--                          procedure 'Interpret_Fields (SUpp_Index_Constituents)
--
-- VD  - 09APR2013 - Generate - Fix CURSOR Get_Last_Success_Run to get last successfull run date
--
-- SSI - 10APR - Interpret_Fields(for securities) - MAT_DATE is a date to be formatted with given output_format
--
-- SSI - 12APR - Generate - Previous fix on CURSOR Get_Last_Success_Run is returning a null date if no previous success is found, while old CURSOR is returning nothing.
--                          Add test for returning null date.
--
-- Miglena - 10May2013 - Generate_Risk_File_Equities - cursor changed to speed up the query
--
-- SSI - 14JUN2013 - Gen_Risk_File_Equities - Rewrite the big cursor, break it in 2.
--
-- SSI - 16DEC2013 - Generate - Restrict current date only for 'MSCT' client. We are now having supplier with monthly delivery and extraction is from last run date.
--
-- SSI - 18MAR2014 - IDX-1220 - New parameter  MAX_ROLLING_MTHS to define max number of month going back as start date
--
-- SSI - 22APR2014 - IDX-1228 - Generate: Update Break_points upon success
--
-- SSI - 24APR2014 - IDX-1352 - Generate_Ticker_Changes: Select indices with Constituent_Level_Flag='Y' for ticker changes.
--
-- SSI - 25APR2014 - IDX-903  - Generate_Ticker_Changes: Report also ticker changes with Identification code='L' for Security_ID change.
--
-- AP - 25SEP2014 - IDX-1827  - Interpret new field SUPP_ISIN_I2
--                              procedure Interpret_Fields (Index_Securities)
--                              procedure Interpret_Fields (Supp_Index_Constituents)
--
-- SSI - 02JUN2015 - IDX-2947 - Format_Field: Set local variable V_Field_Value the same length as the variable from calling module.
--
-- AP  - 27MAY2015 - IDX-2893 - Interpret new field NFLD_53 (Index_Series)
--
-- SSI - 05JUN2015 - IDX-2924 - Get_Format_Record: Change in all 3 overloaded procedures. New custom concatenate format '||#NONULL#'
--                                                 to concatenate only when it is not null 
--                   IDX-2583 - Interpret_Fields - For Index_Series.Supp_Ccy. Allow populating with user defined null_string in format layout.
--
-- SSI - 16JUN2015 - IDX-2924 - Interpret_Fields - Interpret new field SUPP_SEDOL_2
--
-- SSI - 19JUN2015 - IDX-1493 - Generate - Check specific fri_client when checking for holiday_class/index family.
--
-- AP  - 22Jul2015 - IDX-2583 - Interpret new field ALPHA_CODE
--
-- SSI - 11AUG2015 - IDX-3075 - Interpret_Fields - Interpret new field ISIN_ID2
--
-- SSI - 13OCT2015 - IDX-3387 - Add new interpreted field 'SECTOR_X' for GICS with hierarchy - this one will not try to fill in OASIS GICS.
--
-- SSI - 06NOV2015 - IDX-3544 - Load_Custom_Columns: Add hint to CURSOR Get_Const, to force use of index 02
--
-- SSI - 02DEC2015 - IDX-3460 - FOr T-1 service, current day constituent file CN2 on holiday date, should CFW from the CN1 records of the next business day.
--
-- SSI - 08DEC2015 - IDX-2040 - Interpret_Fields - For INdex_Series, new fields interpreted: NFLD_4, NFLD_6 and YIELD.  
--
-- SSI - 01FEB2016 - IDX-3834 - Interpret_Fields - Field ISIN_ID2 built for client FSTQ required to be changed to remove the '#' in prefix 'ID#' when an ISIN is not found.
-- 
    G_System_Date           DATE;
    G_Site_Date             DATE;
    G_User_ID               VARCHAR2(20) := Constants.Get_User_Id;
    g_flag_is_holiday       BOOLEAN;
    G_Num_Del               NUMBER:=0;
    G_Program_ID            NUMBER;
    g_field_delimiter       VARCHAR2(1);
    g_delimiter_type        ind_client_restrictions.delimiter_type_code%TYPE;
    g_delivery_time         Data_Supply_Schedule.delivery_time%TYPE;
    g_data_supplier_code    Data_Supply_Schedule.data_supplier_code%TYPE;
    g_statpro_gics          supp_index_constituents.gics%TYPE;
-- SSI - 17OCT2008
    G_Fmt_Fri_Client        Clients.Fri_Client%TYPE;
-- ADD ACB 07MAY2009
    g_index_type            supp_index_types.index_type%TYPE;
--
-- SSI - 06APR2009
    G_Holiday_Class         Holiday_Classes.Code%TYPE;
-- SSI - 06APR2009 - End of modif.
-- ACB - 04JUL2009
    G_index_family          supp_index.index_family%TYPE;
-- ACB - 04JUL2009 - End of modif.
    TYPE t_gics_desc IS TABLE OF gics_classes.Text_E%TYPE INDEX BY gics_classes.Code%TYPE;
    g_gics_desc             t_gics_desc;
    G_T_Service             BOOLEAN;
--
    C_Merge_Str_Format      VARCHAR2(5):='||##';
    C_Merge_Str_NotNull_Format      VARCHAR2(10):='||#NONULL#';
--
    pricing_msgs_rec        Pricing_Msgs%ROWTYPE;
--
    LOGICAL_ERROR           EXCEPTION;
--
    TYPE t_rec_client_output_files IS RECORD(
        output_file_name    client_output_files.output_file_name%TYPE,
        component_code      client_output_files.component_code%TYPE
    );
--
    TYPE t_tbl_index_type IS TABLE OF VARCHAR2(1000) INDEX BY BINARY_INTEGER;
    TYPE t_client_output_files IS TABLE OF t_rec_client_output_files
        INDEX BY BINARY_INTEGER;
    TYPE t_file_def_typ IS TABLE OF Client_File_Definitions%ROWTYPE
        INDEX BY Product_Components.Component_Code%TYPE;
    TYPE t_currencies IS TABLE OF currencies%ROWTYPE
        INDEX BY currencies.currency%TYPE;
    v_client_output_file    t_client_output_files;
--
-- SSI - 25MAY2009
    TYPE t_id_sys IS TABLE OF NUMBER INDEX BY Identification_Systems.Code%TYPE;
    g_id_sys                t_id_sys;
-- SSI - 03SEP2009
    TYPE t_Custom_Columns IS TABLE OF Product_Field_Names.Code%TYPE INDEX By BINARY_INTEGER;
    TYPE t_Rec_Columns IS RECORD (Product_Field_Name_Code   Product_Field_Names.Code%TYPE
                                 ,Num                       INTEGER);
    TYPE t_Out_Columns IS TABLE OF t_Rec_Columns INDEX BY BINARY_INTEGER;
-- SSI - 03SEP2009 - En dof modif.
--
-- SSI - 25MAY2009 - end of modif.
--
    G_File_Def              t_file_def_typ;
    G_File_Def_Init         t_file_def_typ;
    g_tbl_currencies_init   t_currencies;
    g_tbl_currencies        t_currencies;
    g_tbl_iso_cur           t_currencies;
    g_ind_type_pos          INTEGER;
    g_tbl_index_type        t_tbl_index_type;
-- SSI - 17JUN2009
    G_On_Server           BOOLEAN     := Constants.On_Server;
--
-- SSI - 15FEB2011
    G_Last_Fri_Client       Clients.Fri_Client%TYPE:=NULL;
    G_Last_Product_Code     Products.Code%TYPE:=NULL;
    G_Last_Supp_Index       Supp_Index.Supp_index_Code%TYPE:=NULL;
    G_Last_Del_Time         VARCHAR2(4):=NULL;
    G_Last_Series_Type      INTEGER:=NULL;
    G_Clt_Index_ID          CLient_Index_Suppliers.Client_Index_Code%TYPE:=NULL;
-- SSI - 15FEB2011 - end of modif.
--
    c_oasis_message         CONSTANT VARCHAR2(30) := 'PROC-CLIENT_INDEX_FILES-';
    c_proc_generate         CONSTANT VARCHAR2(30) := 'Generate';
    c_gen_message           CONSTANT VARCHAR2(50) := 'PROC-GEN-';
    c_field_sep             CONSTANT VARCHAR2(1)  := ',';
    c_name_sep              CONSTANT VARCHAR2(1)  := '/';
    c_date_format           CONSTANT VARCHAR2(8)  := 'yyyymmdd';
    c_type_out_table        CONSTANT VARCHAR2(30) := 'TABLE';
    c_type_out_file         CONSTANT VARCHAR2(30) := 'FILE';
    c_type_out_both         CONSTANT VARCHAR2(30) := 'BOTH';
    c_series_type_series    CONSTANT INTEGER      := 2;
    c_series_type_mkt_cap   CONSTANT INTEGER      := 3;
    c_series_type_exch_rate CONSTANT INTEGER      := 4;
-- SSI - 19APR2011
    c_series_type_const     CONSTANT INTEGER      := 6;
    c_spot_series           CONSTANT VARCHAR2(30) := 'SPOT';
    c_fwrd_series           CONSTANT VARCHAR2(30) := 'FORWARD';
    c_inter_fwd_series      CONSTANT VARCHAR2(30) := 'INTERPOLATED';
    c_null_number           CONSTANT NUMBER       := 0;
    c_null_char             CONSTANT VARCHAR2(1)  := ' ';
    c_del_count             CONSTANT NUMBER       := 2000;
    c_supp_index_const      CONSTANT VARCHAR2(30) := 'SUPP_INDEX_CONST';
    c_supp_index            CONSTANT VARCHAR2(30) := 'SUPP_INDEX';
    C_LF                    CONSTANT CHAR(1)      := chr(10);
    c_msci                  CONSTANT CHAR(4)      := 'MSCT';
    c_iboxx                 CONSTANT CHAR(4)      := 'IBOX';
    c_max_linesize          CONSTANT NUMBER       := 3000;
    c_index_type_pattern    CONSTANT VARCHAR2(30) := '#####';
--  SSI - 18MAY2009
    C_Clt_ID_Prefix         CONSTANT VARCHAR2(10) := 'ID#';
--  SSI - 01FEB2016
    C_Clt_ID_Prefix_2       CONSTANT VARCHAR2(10) := 'ID';
--
--  SSI - 20MAR2009
    C_Upd_Break_Pts         CONSTANT BOOLEAN := TRUE;
    pgmloc                  INTEGER       := 0;
    v_message_code          VARCHAR2(5)   := NULL;
    v_message_val1          VARCHAR2(500) := NULL;
    v_message_val2          VARCHAR2(500) := NULL;
    v_message_val3          VARCHAR2(500) := NULL;
    v_oasis_message         VARCHAR2(1000):= NULL;
--
    v_dir_name                      VARCHAR2(100) := Constants.Get_Dir_Name;
    v_log_file                      Utl_File.File_Type;
    v_log_file_name                 supplier_running_parms.file_name%TYPE;
    v_output_file_series_char       Utl_File.File_Type;
    v_output_file_series_char_name  VARCHAR2(60) := NULL;
    v_output_file_exch_char         Utl_File.File_Type;
    v_output_file_exch_char_name    VARCHAR2(100);
    v_num_files                     INTEGER := 0;
--
    TYPE t_supp_index_constituents IS TABLE OF supp_index_constituents%ROWTYPE;
    v_supp_index_constituents       t_supp_index_constituents;
    v_rec_index_series              index_series%ROWTYPE;
--
    CURSOR cur_format_layout
        (p_fri_client client_format_layouts.fri_client%TYPE,
         p_product_code client_format_layouts.product_code%TYPE,
         p_component Client_format_layouts.component_code%TYPE,
         p_record_type client_format_layouts.record_type_code%TYPE
        )
    IS
    SELECT  A.record_position,
            A.product_field_name_code   field_name,
            A.starting_byte,
            A.field_length,
            A.output_format,
            A.floating_point_flag,
            A.null_character,
            A.null_string,
-- SSI - 01OCT2009
            A.Component_Code,
            B.field_datatype
    FROM client_format_layouts A, product_field_names B
    WHERE A.product_field_name_code = B.code
    AND A.fri_client              = p_fri_client
    AND A.product_code            = p_product_code
    AND A.component_code          = p_component
    AND A.record_type_code        = p_record_type
    ORDER BY 1, 3;
--
-- SSI - 25MAY2009
    CURSOR Get_Clt_ID_SYS (I_Fri_Client    Clients.Fri_Client%TYPE)
    IS
    SELECT *
    FROM   Client_Ident_Systems
    WHERE  Fri_Client = I_Fri_Client;
-- SSI - 25MAY2009 - End of modif.
--
    TYPE t_format_layout IS TABLE OF cur_format_layout%ROWTYPE;
    -- Used For all file format (data, header and trailer)
    g_format_layout  t_format_layout := t_format_layout();
--
    TYPE t_pos_field_not_print IS TABLE OF INTEGER;
--
-- SSI - 15FEB2011
    CURSOR Cur_Get_Clt_Index (i_fri_client         Clients.Fri_Client%TYPE,
                              i_data_supplier_code Data_Suppliers.Code%TYPE,
                              i_delivery_time      Supp_Index.Delivery_Time%TYPE,
                              i_supp_index_code    Supp_index.Supp_Index_Code%TYPE,
                              i_series_type        INTEGER)
    IS
    SELECT C.Fri_Client,
           C.Product_Code,
           C.Supp_Index_Code,
           C.Delivery_Time,
           i_series_type  Series_type,
           NVL(C.Client_Index_Name,I.Text_E) Text_E,
           NVL(C.Client_Index_SName,I.Short_Name) Short_Name,
           NVL(C.Client_Mk_Index_Name,I.Mkt_name) Mkt_Name,
           NVL(C.Client_Mk_Index_SName,I.Mkt_Short_Name) Mkt_Short_Name
    FROM Supp_Index I,
         Client_Index_Suppliers C
    WHERE C.Fri_Client = i_fri_client
    AND   C.data_supplier_code = i_data_supplier_code
    AND   C.delivery_time = i_delivery_time
    AND   C.supp_index_code = i_supp_index_code
    AND   C.Active_Flag = 'Y'
    AND   I.Data_SUpplier_Code = C.Data_Supplier_Code
    AND   I.Supp_Index_Code = C.Supp_Index_Code
    AND   I.Delivery_Time = C.Delivery_Time;
--
    g_rec_Clt_index    Cur_Get_Clt_Index%ROWTYPE;
    g_Series_Long_name Supp_index.Short_Name%TYPE:=NULL;
    g_Series_Desc      Supp_index.Text_E%TYPE:=NULL;
--
--
--
--------------------------------------------------------------------------------
-- INITIALIZE_COUNTER_PRC_MSGS
--------------------------------------------------------------------------------
PROCEDURE Initialize_Counter_Prc_Msgs(
    p_success            OUT boolean
)
AS
BEGIN
    pricing_msgs_rec.value_01  := 0;
    pricing_msgs_rec.value_02  := 0;
    pricing_msgs_rec.value_03  := 0;
    pricing_msgs_rec.value_04  := 0;
    pricing_msgs_rec.value_05  := 0;
    pricing_msgs_rec.value_06  := 0;
    pricing_msgs_rec.value_08  := 0;
    pricing_msgs_rec.value_09  := 0;
    pricing_msgs_rec.value_10  := 0;
    pricing_msgs_rec.value_11  := 0;
    pricing_msgs_rec.value_12  := 0;
    pricing_msgs_rec.value_13  := 0;
    pricing_msgs_rec.value_14  := 0;
    pricing_msgs_rec.value_15  := 0;
END Initialize_Counter_Prc_Msgs;
--
--
--
--------------------------------------------------------------------------------
-- GET_GICS
--------------------------------------------------------------------------------
FUNCTION Get_Gics(
    p_delivery_date     IN  DATE,
    p_supp_index_const  IN  supp_index_constituents%ROWTYPE
)
RETURN VARCHAR2
AS
    v_gics       gic_comp.gsubind%TYPE;
    v_sql_stmt      VARCHAR2(1000);
    v_select        VARCHAR2(1000);
    v_and_clause_1  VARCHAR2(500);
    v_and_clause_2  VARCHAR2(500);
    v_order_by      VARCHAR2(500);
--
    v_id            Tickers.Ticker%TYPE;
--
    TYPE t_cur_gics IS REF CURSOR;
    cur_gics t_cur_gics;
BEGIN
    v_gics := NULL;
    v_select :=   ' SELECT ' ||
--                ' /*+ ALL_ROWS */ ' ||
                  ' gsubind '                        ||
                  ' FROM gic_comp c, gic_secur s '          ||
                  ' WHERE c.gvkey = s.gvkey '               ||
                  ' AND c.delivery_date = s.delivery_date ' ||
--                ' AND s.delivery_date between :1 and :2 ';
                  ' AND s.delivery_date = :1 ';
--                ' AND c.delivery_date = (select max(s1.delivery_date) ' ||
--                                       ' from gic_secur s1 ' ||
--                                       ' where s1.gvkey = s.gvkey ' ||
--                                       ' and   s1.IID = s.IID ' ||
--                                       ' and   s1.delivery_date <= :1 '
--                ;
--  v_order_by := ' order by c.delivery_date desc ';
    v_order_by := null;
    IF p_supp_index_const.cusip IS NOT NULL AND
       LENGTH(p_supp_index_const.cusip) >= 8
    THEN
    --
    -- CUSIP
    --
       v_id := TRIM(p_supp_index_const.cusip);
       IF LENGTH(v_id) < 9
       THEN
          v_id := v_id||Valid_Sec_ID.MOD10GEN(UPPER(v_id));
       END IF;
       v_sql_stmt := NULL;
       v_and_clause_1 := ' AND s.cusip = :2 ';
--     v_and_clause_2 := ' and   s1.cusip = s.cusip) ';
--     v_sql_stmt := v_select || v_and_clause_2 || v_and_clause_1;
       v_sql_stmt := v_select || v_and_clause_1 || v_order_by;
       OPEN cur_gics FOR v_sql_stmt USING p_delivery_date,
                                          v_id;
       FETCH cur_gics INTO v_gics;
       CLOSE cur_gics;
       IF (v_gics IS NOT NULL) THEN
           GOTO return_gics;
       END IF;
    END IF; -- IF CUSIP
    --
    -- SEDOL
    --
    IF p_supp_index_const.sedol IS NOT NULL AND
       LENGTH(p_supp_index_const.sedol) = 7
    THEN
       v_sql_stmt := NULL;
       v_and_clause_1 := ' AND s.sedol = :2 ';
--     v_and_clause_2 := ' and   s1.sedol = s.sedol) ';
--     v_sql_stmt := v_select || v_and_clause_2 || v_and_clause_1;
       v_sql_stmt := v_select || v_and_clause_1 || v_order_by;
       OPEN cur_gics FOR v_sql_stmt USING p_delivery_date,
                                          p_supp_index_const.sedol;
       FETCH cur_gics INTO v_gics;
       CLOSE cur_gics;
       IF (v_gics IS NOT NULL) THEN
           GOTO return_gics;
       END IF;
    END IF;
    --
    -- ISIN
    --
    IF p_supp_index_const.isin IS NOT NULL AND
       LENGTH(p_supp_index_const.isin) between 11 and 12
    THEN
       v_id := TRIM(p_supp_index_const.isin);
       IF LENGTH(v_id) < 12
       THEN
          v_id := v_id||Valid_Sec_Id.MOD10GEN(UPPER(v_id));
       END IF;
       v_sql_stmt := NULL;
       v_and_clause_1 := ' AND s.isin = :2 ';
--     v_and_clause_2 := ' and   s1.isin = s.isin) ';
--     v_sql_stmt := v_select || v_and_clause_2 || v_and_clause_1;
       v_sql_stmt := v_select || v_and_clause_1 || v_order_by;
       OPEN cur_gics FOR v_sql_stmt USING p_delivery_date,
                                          v_id;
       FETCH cur_gics INTO v_gics;
       CLOSE cur_gics;
       IF (v_gics IS NOT NULL) THEN
           GOTO return_gics;
       END IF;
    END IF;
    --
    -- RETURN THE GICS
<<return_gics>>
    RETURN v_gics;
EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
END Get_Gics;
--
--
--
--------------------------------------------------------------------------------
-- SET_COUNTER_PRC_MSGS
--------------------------------------------------------------------------------
PROCEDURE Set_Counter_Prc_Msgs(
    p_success            OUT BOOLEAN
)
AS
BEGIN
    Pricing_Msgs_Rec.value_01 := v_num_files;
    Pricing_Msgs_Rec.value_02 := G_Num_Del;
END Set_Counter_Prc_Msgs;
--
--
--
--------------------------------------------------------------------------------
-- RETURN_SERIES_IDENTIFIER
--------------------------------------------------------------------------------
FUNCTION Return_Series_Identifier(
    p_supp_index_code   IN  index_series.supp_index_code%TYPE,
    p_series_type       IN  INTEGER
)
RETURN VARCHAR2
AS
BEGIN
    IF (p_series_type = c_series_type_mkt_cap AND p_supp_index_code IS NOT NULL)
    THEN
        -- ADD ACB 27OCT2009
        --RETURN 'M' || p_supp_index_code;
        IF (g_data_supplier_code = 'DAX') THEN
            RETURN 'M_' || p_supp_index_code;
        ELSE
            RETURN 'M' || p_supp_index_code;
        END IF;
    ELSE
        RETURN p_supp_index_code;
    END IF;
END Return_Series_Identifier;
--
--
--
-- SSI - 15FEB2011 - New function
--------------------------------------------------------------------------------
-- RETURN_CLT_SERIES_IDENTIFIER
--------------------------------------------------------------------------------
FUNCTION Return_Clt_Series_Identifier(
    p_fri_client        IN  Clients.Fri_Client%TYPE,
    p_product_code      IN  Products.Code%TYPE,
    p_supp_index_code   IN  Supp_Index.supp_index_code%TYPE,
    p_Del_Time          IN  Client_Index_Suppliers.Delivery_Time%TYPE,
    p_series_type       IN  INTEGER
)
RETURN VARCHAR2
AS
V_Clt_Index_Suppliers   Client_Index_Suppliers%ROWTYPE;
--
CURSOR Get_Clt_Index_Code IS
SELECT *
FROM   Client_Index_Suppliers
WHERE  Fri_Client =  p_fri_client
AND    Product_Code = p_product_code
AND    Data_Supplier_Code = g_Data_Supplier_code
AND    Supp_Index_Code = p_supp_index_code
AND    Delivery_Time = p_Del_Time
AND    Active_Flag = 'Y';
--
BEGIN
    IF G_Last_Fri_Client   = p_fri_client AND
       G_Last_Product_Code = p_product_code AND
       G_Last_Supp_Index   = p_supp_index_code AND
       G_Last_Del_Time     = p_Del_time AND
       G_Last_Series_Type  = p_series_type
    THEN
       Return G_Clt_Index_ID;
    ELSE
       OPEN Get_Clt_Index_Code;
       FETCH Get_Clt_Index_Code INTO V_Clt_Index_Suppliers;
       CLOSE Get_Clt_Index_Code;
--
       IF p_series_type = c_series_type_mkt_cap
       THEN
           IF V_Clt_Index_Suppliers.Client_MK_Index_Code IS NULL
           THEN
              G_Clt_Index_ID := Return_Series_Identifier(p_supp_index_code,p_series_type);
           ELSE
-- SSI - 24MAR2011
--            G_Clt_Index_ID := V_Clt_Index_Suppliers.Client_MK_Index_Code;
              G_Clt_Index_ID := NVL(V_Clt_Index_Suppliers.Client_MK_Index_Code,Return_Series_Identifier(p_supp_index_code,p_series_type));
           END IF;
       ELSE
-- SSI - 24MAR2011
--         G_Clt_Index_ID := V_Clt_Index_Suppliers.Client_Index_Code;
           G_Clt_Index_ID := NVL(V_Clt_Index_Suppliers.Client_Index_Code,V_Clt_Index_Suppliers.Supp_Index_Code);
       END IF;
       G_Last_Fri_Client   := p_fri_client;
       G_Last_Product_Code := p_product_code;
       G_Last_Supp_Index   := p_supp_index_code;
       G_Last_Del_Time     := p_Del_time;
       G_Last_Series_Type  := p_series_type;
       Return G_Clt_Index_ID;
    END IF;
END Return_Clt_Series_Identifier;
-- SSI - 15FEB2011
--
--
--
--------------------------------------------------------------------------------
-- ADD ACB 07MAY2009
--------------------------------------------------------------------------------
-- GET_EXCHANGE_RATE_SOURCE
--------------------------------------------------------------------------------
FUNCTION Get_Exchange_Rate_Source(
    p_data_supplier_code   data_suppliers.code%TYPE
)
RETURN VARCHAR2
AS
    v_text_e data_suppliers.text_e%TYPE;
BEGIN
    SELECT text_e
    INTO v_text_e
    FROM data_suppliers
    WHERE code = p_data_supplier_code;
    RETURN trim(v_text_e || ' '|| g_index_type);
END Get_Exchange_Rate_Source;
--
--
--
--------------------------------------------------------------------------------
-- GET_EXPRESSED_CURRENCY
--------------------------------------------------------------------------------
FUNCTION Get_Expressed_Currency(
    p_data_supplier_code    IN  data_supply_schedule.data_supplier_code%TYPE,
    p_index_series          IN index_series%ROWTYPE
)
RETURN VARCHAR2
AS
BEGIN
    IF (p_data_supplier_code = c_msci) THEN
        RETURN 'USD';
--  ELSIF (p_data_supplier_code = c_iboxx) THEN
    ELSE
        RETURN nvl(p_index_series.supp_ccy, '0');
    END IF;
END Get_Expressed_Currency;
--
--
--
--------------------------------------------------------------------------------
-- GET_GICS_DESCRIPTION
--------------------------------------------------------------------------------
FUNCTION Get_GICS_Description(
    p_code IN gics_classes.code%TYPE
)
RETURN VARCHAR2
AS
    v_gics_desc gics_classes.text_e%TYPE;
BEGIN
--
    if g_gics_desc.EXISTS(p_code)
    THEN
       RETURN g_gics_desc(p_Code);
    ELSE
    SELECT replace(text_e, g_field_delimiter, ' ')
    INTO v_gics_desc
    FROM gics_classes
    WHERE code = p_code;
--
       g_gics_desc(p_Code):= v_gics_desc;
    RETURN v_gics_desc;
    END IF;
EXCEPTION
    WHEN OTHERS THEN
        RETURN p_code;
END Get_GICS_Description;
--
--
--
--------------------------------------------------------------------------------
-- GET_SERIES_LONG_NAME
--------------------------------------------------------------------------------
FUNCTION Get_Series_Long_Name(
    p_data_supplier_code  IN  data_supply_schedule.data_supplier_code%TYPE,
    p_supp_index_code     IN  supp_index.supp_index_code%TYPE,
    p_delivery_time       IN  VARCHAR2,
    p_series_type         IN  INTEGER
)
RETURN VARCHAR2
AS
    v_rec_supp_index    supp_index%ROWTYPE;
--
    CURSOR Cur_Get_Index IS
    SELECT *
    FROM supp_index
    WHERE data_supplier_code = p_data_supplier_code
    AND delivery_time = p_delivery_time
    AND supp_index_code = p_supp_index_code;
BEGIN
    pgmloc := 1000;
    OPEN CUR_Get_Index;
    pgmloc := 1010;
    FETCH Cur_Get_Index INTO v_rec_supp_index;
    pgmloc := 1020;
    CLOSE Cur_Get_Index;
--
    pgmloc := 1030;
    IF (p_series_type = c_series_type_mkt_cap) THEN
        pgmloc := 1040;
        RETURN replace(v_rec_supp_index.mkt_short_name, g_field_delimiter, ' ');
    ELSE
        pgmloc := 1050;
        RETURN replace(v_rec_supp_index.short_name, g_field_delimiter, ' ');
    END IF;
END Get_Series_Long_Name;
--
--
--
--------------------------------------------------------------------------------
-- GET_SERIES_DESCRIPTION
--------------------------------------------------------------------------------
FUNCTION Get_Series_Description(
-- SSI - 09SEP2008
--  p_data_supplier_code  IN  data_supply_schedule.data_supplier_code%TYPE,
--  p_supp_index_code     IN  supp_index.supp_index_code%TYPE,
    p_index_series        IN  index_series%ROWTYPE,
    p_series_type         IN  INTEGER
)
RETURN VARCHAR2
AS
    v_rec_supp_index    supp_index%ROWTYPE;
--
    CURSOR Cur_Get_Index IS
    SELECT *
    FROM supp_index
-- SSI- 09SEP2008
--  WHERE data_supplier_code = p_data_supplier_code
--  AND supp_index_code = p_supp_index_code;
    WHERE data_supplier_code = p_index_series.data_supplier_Code
    AND supp_index_code = p_index_series.supp_index_code
    AND   delivery_time = TO_CHAR(p_index_series.delivery_date,'HH24MI');
BEGIN
    pgmloc := 1060;
    OPEN CUR_Get_Index;
    pgmloc := 1070;
    FETCH Cur_Get_Index INTO v_rec_supp_index;
    pgmloc := 1080;
    CLOSE Cur_Get_Index;
--
    pgmloc := 1090;
    IF (p_series_type = c_series_type_mkt_cap) THEN
        RETURN replace(v_rec_supp_index.mkt_name, g_field_delimiter, ' ');
    ELSE
        RETURN replace(v_rec_supp_index.text_e, g_field_delimiter, ' ');
    END IF;
END Get_Series_Description;
--
--
--
--------------------------------------------------------------------------------
-- GET_CLT_SERIES_Names
--------------------------------------------------------------------------------
Procedure Get_Clt_Series_Names(
    p_fri_client          IN  Clients.Fri_Client%TYPE,
    p_product_Code        IN  Products.Code%TYPE,
    p_data_supplier_code  IN  data_supply_schedule.data_supplier_code%TYPE,
    p_supp_index_code     IN  supp_index.supp_index_code%TYPE,
    p_delivery_time       IN  VARCHAR2,
    p_series_type         IN  INTEGER,
    p_series_Long_Name       OUT VARCHAR2,
    p_series_Desc            OUT VARCHAR2
)
-- RETURN VARCHAR2
AS
--
/*
    CURSOR Cur_Get_Clt_Index IS
    SELECT C.Fri_Client,
           C.Product_Code,
           C.Supp_Index_Code,
           C.Delivery_Time,
           p_series_type  Series_type,
           NVL(C.Client_Index_Name,I.Text_E) Text_E,
           NVL(C.Client_Index_SName,I.Short_Name) Short_Name,
           NVL(C.Client_Mk_Index_Name,I.Mkt_name) Mkt_Name,
           NVL(C.Client_Mk_Index_SName,I.Mkt_Short_Name) Mkt_Short_Name
    FROM Supp_Index I
         Client_Index_Suppliers C
    WHERE C.Fri_Client = p_fri_client
    AND   C.data_supplier_code = p_data_supplier_code
    AND   C.delivery_time = p_delivery_time
    AND   C.supp_index_code = p_supp_index_code
    AND   C.Active_Flag = 'Y'
    AND   I.Data_SUpplier_Code = C.Data_Supplier_Code
    AND   I.Supp_Index_Code = C.Supp_Index_Code
    AND   I.Delivery_Time = C.Delivery_Time;
*/
--
    v_rec_Clt_index    Cur_Get_Clt_Index%ROWTYPE;
BEGIN
    IF g_rec_clt_Index.Fri_Client = p_fri_client AND
       g_rec_clt_Index.Product_Code = p_Product_Code AND
       g_rec_clt_Index.Supp_index_Code = p_Supp_index_Code AND
       g_rec_clt_Index.Delivery_Time = p_Delivery_Time AND
       g_rec_clt_Index.Series_Type = p_Series_Type
    THEN
       p_series_long_name := g_series_long_name;
       p_series_desc      := g_series_desc;
    ELSE
       pgmloc := 1100;
       OPEN CUR_Get_Clt_Index(p_fri_client,
                              p_data_supplier_code,
                              p_delivery_time,
                              p_supp_index_code,
                              p_series_type);
       pgmloc := 1110;
       FETCH Cur_Get_Clt_Index INTO v_rec_clt_index;
       pgmloc := 1120;
       CLOSE Cur_Get_Clt_Index;
--
       pgmloc := 1130;
       IF (p_series_type = c_series_type_mkt_cap) THEN
           pgmloc := 1140;
           p_series_long_name := replace(v_rec_clt_index.mkt_short_name, g_field_delimiter, ' ');
           p_series_desc      := replace(v_rec_clt_index.mkt_name, g_field_delimiter, ' ');
           -- RETURN replace(v_rec_supp_index.mkt_short_name, g_field_delimiter, ' ');
       ELSE
           pgmloc := 1150;
           p_series_long_name := replace(v_rec_clt_index.short_name, g_field_delimiter, ' ');
           p_series_desc      := replace(v_rec_clt_index.Text_e, g_field_delimiter, ' ');
       END IF;
       g_series_long_name := p_series_long_name;
       g_series_desc      := p_series_desc;
    END IF;
END Get_Clt_Series_Names;
--
--
--
--------------------------------------------------------------------------------
-- GET_EXCHANGE_SERIES_CODE
--------------------------------------------------------------------------------
FUNCTION Get_Exchange_Series_Code(
    p_iso_currency_symbol IN  Currencies.iso_currency%TYPE,
    p_base_currency       IN  VARCHAR2,
    p_series_type         IN  VARCHAR2
)
RETURN VARCHAR2
AS
BEGIN
    IF (p_series_type = c_spot_series) THEN
        RETURN p_iso_currency_symbol || c_name_sep || p_base_currency;
    ELSIF (p_series_type = c_fwrd_series) THEN
        RETURN p_iso_currency_symbol || c_name_sep || p_base_currency
-- ADD Comment ACB 07MAY2009
--                || ' Fwd 1 month';
                  || ' F1M';
    ELSE -- INTERPOLATED
        RETURN p_iso_currency_symbol || c_name_sep || p_base_currency
-- ADD Comment ACB 07MAY2009
--                || ' Fwd 1 month inter';
                  || ' F1M inter';
    END IF;
END Get_Exchange_Series_Code;
--
--
--
--------------------------------------------------------------------------------
-- GET_NAME_TO_SPLIT_FILE
--
-- For some supplier, we have to generate more than one file. The file name
-- is composed by the index type. So, if the file name configureted in
-- client_file_definitions has the string #####, it means that we have to
-- generate more than one file
--------------------------------------------------------------------------------
PROCEDURE Get_Name_To_Split_File(
    p_fri_client            IN  clients.fri_client%TYPE,
    p_product_code          IN  products.code%TYPE,
    p_sub_product_code      IN  sub_products.sub_product_code%TYPE,
    p_component_code        IN  product_components.component_code%TYPE,
    p_tbl_index_type        OUT t_tbl_index_type,
    p_success               OUT BOOLEAN
)
AS
    v_file_name     VARCHAR2(500);
-- SSI - 01DEC2008
    V_Constituent_Level_Flag    VARCHAR2(1);
--
-- SSI - 17OCT2008
--  CURSOR cur_supp_index
--  IS
--  SELECT DISTINCT index_type
--  FROM supp_index I
--  WHERE data_supplier_code = g_data_supplier_code
--  ORDER BY index_type;
--
-- ACB - 04JUL2009
    CURSOR cur_supp_index
    IS
    SELECT DISTINCT index_family
    FROM supp_index I,
         client_index_suppliers C
    WHERE C.Fri_Client = P_Fri_Client
    AND   C.Product_Code = P_Product_Code
    AND   C.Sub_Product_Code = P_Sub_Product_Code
    AND   C.Data_supplier_code = I.Data_Supplier_Code
    AND   C.Supp_Index_Code = I.Supp_Index_Code
    AND   C.Delivery_Time = I.Delivery_Time
    AND   I.index_family = G_index_family
    AND   C.Active_FLag = 'Y'
    AND   C.constituent_level_flag=NVL(v_constituent_level_flag,
                                       C.constituent_level_flag)
    ORDER BY index_family;
--    CURSOR cur_supp_index
--    IS
--    SELECT DISTINCT index_type
--    FROM supp_index I
--        ,Client_Index_Suppliers C
---- SSI - 06APR2009
--        ,Data_Supply_Schedule D
---- SSI - 06APR2009 - End of modif.
--    WHERE C.Fri_Client = P_Fri_Client
--    AND   C.Product_Code = P_Product_Code
--    AND   C.Sub_Product_Code = P_Sub_Product_Code
--    AND   C.Data_supplier_code = I.Data_Supplier_Code
--    AND   C.Supp_Index_Code = I.Supp_Index_Code
--    AND   C.Delivery_Time = I.Delivery_Time
--    AND   C.Active_FLag = 'Y'
---- SSI - 06APR2009
--    AND   D.Data_Supplier_Code = C.Data_Supplier_Code
--    AND   D.Delivery_Time      = C.Delivery_Time
--    AND   D.Holiday_Class_Code = G_Holiday_Class
---- SSI - 06APR2009 - End of modif.
---- SSI - 01DEC2008
--    AND   C.constituent_level_flag=NVL(V_Constituent_Level_Flag
--                                      ,C.constituent_level_flag)
--    ORDER BY index_type;
-- ACB - 04JUL2009 - End of modif.
BEGIN
--  p_tbl_index_type := t_tbl_index_type();
    -- Get the file name to see if there is the string #####
    Product_Component.Get_Dated_File_Name(p_fri_client,
                                          p_product_code,
                                          p_sub_product_code,
                                          p_component_code,
                                          sysdate,
                                          v_file_name,
                                          p_success,
                                          Pricing_Msgs_Rec.err_text
                                         );
    IF ( NOT p_success) THEN
        v_message_code := '020';
        v_message_val1 := p_product_code || '/' || p_sub_product_code;
        v_message_val2 := p_component_code;
        v_message_val3 := Pricing_Msgs_Rec.err_text;
        RAISE LOGICAL_ERROR;
    END IF;
--
-- SSI - 03MAR2009
--  IF p_component_code IN ('B','C','D')
-- SSI - 23JUN2009
-- Change constituent level file definitions.- add exchange rates characterictics/Values and Ticker changes files
--  IF p_component_code IN ('B','C','D','K','N')
--  IF p_component_code IN ('B','C','D','E','K','N','R','T')
    IF p_component_code IN ('B','C','D','E','K','N','R','T','U')
    THEN
       V_Constituent_Level_Flag := 'Y';
    END IF;
--
    IF REGEXP_LIKE(v_file_name, c_index_type_pattern) THEN
        OPEN cur_supp_index;
        FETCH cur_supp_index BULK COLLECT INTO p_tbl_index_type;
        CLOSE cur_supp_index;
    ELSE
        -- if it is null, it means ot put everything in one single file
--      p_tbl_index_type.EXTEND;
        p_tbl_index_type(1) := NULL;
    END IF;
END Get_Name_To_Split_File;
--
--
--
--------------------------------------------------------------------------------
-- FIELDS_TO_NOT_PRINT
--------------------------------------------------------------------------------
PROCEDURE Fields_To_Not_Print(
    p_rec_format_layout     IN cur_format_layout%ROWTYPE,
    p_num_total_fields      IN INTEGER,
    p_num_group_rec         IN INTEGER,
    p_pos_field_not_print   IN OUT t_pos_field_not_print
)
AS
    v_num_field_per_group   INTEGER;
    v_last_field_group      INTEGER;
    v_first_field_group     INTEGER;
BEGIN
    v_num_field_per_group := p_num_total_fields / p_num_group_rec;
    v_last_field_group  := p_rec_format_layout.record_position * v_num_field_per_group;
    v_first_field_group := v_last_field_group - v_num_field_per_group + 1;
--
    FOR v_pos IN v_first_field_group..v_last_field_group LOOP
        p_pos_field_not_print.EXTEND;
        p_pos_field_not_print(p_pos_field_not_print.LAST) := v_pos;
    END LOOP;
END Fields_To_Not_Print;
--
--
--
--
--------------------------------------------------------------------------------
-- SSI - 16APR2009
-- Define delimiter
--------------------------------------------------------------------------------
PROCEDURE Define_Delimiter(
            p_fri_client       IN      client_format_layouts.fri_client%TYPE,
            p_product_code     IN      client_format_layouts.product_code%TYPE)
IS
--
    CURSOR Get_Delim_Type (I_Fri_Client   Clients.fri_Client%TYPE)
    IS
    SELECT delimiter_type_code
    FROM ind_client_restrictions
    WHERE fri_client = I_fri_client
    AND product_code = p_product_code;
--
BEGIN
    pgmloc := 1160;
--
    g_delimiter_Type := NULL;
--
    OPEN Get_Delim_Type(p_fri_client);
    pgmloc := 1170;
    FETCH Get_Delim_Type INTO g_delimiter_Type;
    IF Get_Delim_Type%NOTFOUND
    THEN
       pgmloc := 1180;
       CLOSE Get_Delim_Type;
       pgmloc := 1190;
       OPEN Get_Delim_Type(G_Fmt_fri_client);
       pgmloc := 1200;
       FETCH Get_Delim_Type INTO g_delimiter_Type;
       IF Get_Delim_Type%NOTFOUND
       THEN
          g_Delimiter_type := 'FIX';
       END IF;
    END IF;
    pgmloc := 1210;
    CLOSE Get_Delim_Type;
--
    pgmloc := 1220;
    IF (g_delimiter_type = 'CSV') THEN
        g_field_delimiter := ',';
    ELSIF (g_delimiter_type = 'PIPE') THEN
        g_field_delimiter := '|';
    ELSIF (g_delimiter_type = 'FIX') THEN
        g_field_delimiter := NULL;
    ELSIF (g_delimiter_type = 'SDV') THEN
        g_field_delimiter := ';';
    ELSIF (g_delimiter_type = 'SDVEU') THEN
        g_field_delimiter := ';';
    END IF;
--
END Define_Delimiter;
--------------------------------------------------------------------------------
-- LOAD_FORMAT_LAYOUT
--------------------------------------------------------------------------------
PROCEDURE Load_Format_Layout(
    p_fri_client       IN      client_format_layouts.fri_client%TYPE,
    p_product_code     IN      client_format_layouts.product_code%TYPE,
    p_component        IN      client_format_layouts.component_code%TYPE,
    p_record_type      IN      client_format_layouts.record_type_code%TYPE
)
AS
BEGIN
    pgmloc := 1230;
    -- Get the format information for the used fields
    OPEN cur_format_layout(p_fri_client, p_product_code,
                           p_component, p_record_type);
    pgmloc := 1240;
    FETCH cur_format_layout BULK COLLECT INTO g_format_layout;
    pgmloc := 1250;
    CLOSE cur_format_layout;
    -- Get the delimiter type
--
--  SSI - 16APR2009
--  Code moved to a procedure Define_Delimiter
--  Call is done in main procedure Generate, we need to define delimiter once
--
--  pgmloc := 1260;
--  SELECT delimiter_type_code
--  INTO g_delimiter_type
--  FROM ind_client_restrictions
--  WHERE fri_client = p_fri_client
--  AND product_code = p_product_code;
--
--  pgmloc := 1270;
--  IF (g_delimiter_type = 'CSV') THEN
--      g_field_delimiter := ',';
--  ELSIF (g_delimiter_type = 'PIPE') THEN
--      g_field_delimiter := '|';
--  ELSIF (g_delimiter_type = 'FIX') THEN
--      g_field_delimiter := NULL;
--  ELSIF (g_delimiter_type = 'SDV') THEN
--      g_field_delimiter := ';';
--  ELSIF (g_delimiter_type = 'SDVEU') THEN
--      g_field_delimiter := ';';
--  END IF;
--  pgmloc := 1280;
EXCEPTION
    WHEN OTHERS THEN
        NULL;
END Load_Format_Layout;
--
--
--
--------------------------------------------------------------------------------
-- INTERPRET_FIELDS (INDEX_SECURITIES)
--------------------------------------------------------------------------------
PROCEDURE Interpret_Fields(
    p_calc_date                 IN  DATE,
    p_rec_format_layout         IN  cur_format_layout%ROWTYPE,
    p_index_securities          IN  index_securities%ROWTYPE,
    p_series_type               IN  INTEGER,
    p_field_value               OUT VARCHAR2,
    p_success                   OUT BOOLEAN
)
AS
BEGIN
    IF (p_rec_format_layout.field_name = 'CONSTANT') THEN
        pgmloc := 1290;
        p_field_value := p_rec_format_layout.output_format;
    ELSIF (p_rec_format_layout.field_name = 'IGNORED') THEN
        pgmloc := 1300;
        p_field_value := NULL;
    ELSIF (p_rec_format_layout.field_name = 'DATA_SUPP') THEN
        pgmloc := 1310;
        p_field_value := g_data_supplier_code;
    ELSIF (p_rec_format_layout.field_name = 'DELIV_DATE') THEN
        pgmloc := 1320;
        p_field_value := to_char(p_index_securities.delivery_date,
                                 p_rec_format_layout.output_format);
-- SSI - 30SEP2008
    ELSIF (p_rec_format_layout.field_name = 'FILE_DATE') THEN
        pgmloc := 1330;
        p_field_value := to_char(p_calc_date,
                                 p_rec_format_layout.output_format);
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID') THEN
        pgmloc := 1340;
        p_field_value := p_index_securities.security_id;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID_2') THEN
        pgmloc := 1350;
        p_field_value := p_index_securities.security_id_2;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID_3') THEN
        pgmloc := 1360;
        p_field_value := p_index_securities.security_id_3;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID_4') THEN
        pgmloc := 1370;
        p_field_value := p_index_securities.security_id_4;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID_5') THEN
        pgmloc := 1380;
        p_field_value := p_index_securities.security_id_5;
    ELSIF (p_rec_format_layout.field_name = 'ISIN') THEN
        pgmloc := 1390;
        p_field_value := p_index_securities.isin;
    ELSIF (p_rec_format_layout.field_name = 'CUSIP') THEN
        pgmloc := 1400;
        p_field_value := p_index_securities.cusip;
    ELSIF (p_rec_format_layout.field_name = 'SEDOL') THEN
        pgmloc := 1410;
        p_field_value := p_index_securities.sedol;
    ELSIF (p_rec_format_layout.field_name = 'TICKER') THEN
        pgmloc := 1420;
        p_field_value := p_index_securities.ticker;
-- SSI - 25APR2009
    ELSIF (p_rec_format_layout.field_name = 'STP_ISIN_ID') THEN
        pgmloc := 1430;
        IF G_ID_Sys.EXISTS('Z') THEN
            p_field_value := nvl(p_index_securities.security_id_6,
                                 c_clt_id_prefix ||p_index_securities.security_id);
        ELSE
            p_field_value := NULL;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_CUSIP_ID') THEN
        pgmloc := 1440;
        IF G_ID_Sys.EXISTS('C') THEN
            p_field_value := nvl(p_index_securities.Security_id_7,
                                 c_clt_id_prefix || p_index_securities.Security_id);
        ELSE
            p_field_value := NULL;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_SEDOL_ID') THEN
        pgmloc := 1450;
        IF G_ID_Sys.EXISTS('S') THEN
            p_field_value := nvl(p_index_securities.Security_id_8,
                                 c_clt_id_prefix || p_index_securities.Security_id);
        ELSE
            p_field_value := NULL;
        END IF;
-- SSI - 22OCT2008
-- New statpro Identifier fields
    ELSIF (p_rec_format_layout.field_name = 'ISIN_ID') THEN
        pgmloc := 1460;
        -- ADD ACB 07MAY2009
        -- p_field_value := NVL(p_index_securities.isin,
        --                      p_index_securities.Security_id);
        IF G_ID_Sys.EXISTS('Z') THEN
            p_field_value := nvl(p_index_securities.security_id_6,
                                 c_clt_id_prefix ||p_index_securities.security_id);
        ELSE
            p_field_value := NVL(p_index_securities.isin,
                                 p_index_securities.Security_id);
        END IF;
--  SSI - 11AUG2015
    ELSIF (p_rec_format_layout.field_name = 'ISIN_ID2') THEN
        pgmloc := 1465;
        IF G_ID_Sys.EXISTS('Z') THEN
            p_field_value := nvl(nvl(p_index_securities.security_id_6,
                                     p_index_securities.isin),
--  SSI - 01FEB2016
--                               c_clt_id_prefix ||p_index_securities.security_id);
                                 c_clt_id_prefix_2 ||p_index_securities.security_id);
        ELSE
            p_field_value := NVL(p_index_securities.isin,
                                 p_index_securities.Security_id);
        END IF;
-- SSI - 11AUG2015 - end of modif
   -- Miglena - 01.03.2013 - add field 'SUPP_ISIN_ID 
    ELSIF (p_rec_format_layout.field_name = 'SUPP_ISIN_ID') THEN
        pgmloc := 1470;
        p_field_value := NVL(p_index_securities.isin,
                             p_index_securities.Security_id);
    -- end Miglena
-- AP - 25Sep2014 - added field 'SUPP_ISIN_I2 
    ELSIF (p_rec_format_layout.field_name = 'SUPP_ISIN_I2') THEN
        pgmloc := 1480;
        p_field_value := NVL(p_index_securities.isin, 
                             c_clt_id_prefix || p_index_securities.security_id);                              
-- end AP
        -- ACB 07MAY2009 - ENd of modif.
    ELSIF (p_rec_format_layout.field_name = 'CUSIP_ID') THEN
        pgmloc := 1490;
        -- ADD ACB 07MAY2009
        -- p_field_value := NVL(p_index_securities.cusip,
        --                      p_index_securities.Security_id);
        --
        IF G_ID_Sys.EXISTS('C') THEN
            p_field_value := nvl(p_index_securities.Security_id_7,
                                 c_clt_id_prefix || p_index_securities.Security_id);
        ELSE
            p_field_value := NVL(p_index_securities.cusip,
                                 p_index_securities.Security_id);
        END IF;
        -- ACB 07MAY2009 - ENd of modif.
    ELSIF (p_rec_format_layout.field_name = 'SEDOL_ID') THEN
        pgmloc := 1500;
        -- ADD ACB 07MAY2009
        -- p_field_value := NVL(p_index_securities.sedol,
        --                      p_index_securities.Security_id);
        IF G_ID_Sys.EXISTS('S') THEN
            p_field_value := nvl(p_index_securities.Security_id_8,
                                 c_clt_id_prefix || p_index_securities.Security_id);
        ELSE
-- SSI - 13FEB2012
--          p_field_value := NVL(p_index_securities.cusip,
            p_field_value := NVL(p_index_securities.sedol,
                                 p_index_securities.Security_id);
        END IF;
        -- ACB 07MAY2009 - ENd of modif.
-- SSI - 22OCT2008 - End of Modif.
--  SSI - 16JUN2015
    ELSIF (p_rec_format_layout.field_name = 'SUPP_SEDOL_2') THEN
        pgmloc := 1510;
        p_field_value := NVL(p_index_securities.sedol,
                             c_clt_id_prefix || p_index_securities.Security_id);
--  SSI - 16JUN2015 - end of modif
    ELSIF (p_rec_format_layout.field_name = 'SEC_NAME') THEN
        pgmloc := 1520;
        p_field_value := p_index_securities.security_name;
    ELSIF (p_rec_format_layout.field_name = 'SEC_NAME_2') THEN
        pgmloc := 1530;
        p_field_value := p_index_securities.security_name_2;
    ELSIF (p_rec_format_layout.field_name = 'YIELD') THEN
        pgmloc := 1540;
        p_field_value := p_index_securities.yield;
    ELSIF (p_rec_format_layout.field_name = 'MOD_DURATION') THEN
        pgmloc := 1550;
        p_field_value := p_index_securities.Modified_Duration;
    ELSIF (p_rec_format_layout.field_name = 'TIME_MATUR') THEN
        pgmloc := 1560;
        p_field_value := p_index_securities.time_to_maturity;
    ELSIF (p_rec_format_layout.field_name = 'DURATION') THEN
        pgmloc := 1570;
        p_field_value := p_index_securities.duration;
    ELSIF (p_rec_format_layout.field_name = 'CONVEXITY') THEN
        pgmloc := 1580;
        p_field_value := p_index_securities.convexity;
    ELSIF (p_rec_format_layout.field_name = 'COUPON') THEN
        pgmloc := 1590;
        p_field_value := p_index_securities.coupon;
-- SSI - 14SEP2010
    ELSIF (p_rec_format_layout.field_name = 'COUPON_2') THEN
        pgmloc := 1600;
        p_field_value := p_index_securities.coupon_2;
    ELSIF (p_rec_format_layout.field_name = 'INFL_COEFFI') THEN
        pgmloc := 1610;
        p_field_value := p_index_securities.inflation_coefficient;
    ELSIF (p_rec_format_layout.field_name = 'ACCRUED_INT') THEN
        pgmloc := 1620;
        p_field_value := p_index_securities.accrued_interest;
    -- ACB 27OCT2010
    ELSIF (p_rec_format_layout.field_name = 'SPREAD') THEN
        pgmloc := 1630;
        p_field_value := p_index_securities.spread;
    ELSIF (p_rec_format_layout.field_name = 'SPREAD_2') THEN
        pgmloc := 1640;
        p_field_value := p_index_securities.spread_2;
    ELSIF (p_rec_format_layout.field_name = 'MOD_DURTN_2') THEN
        pgmloc := 1650;
        p_field_value := p_index_securities.modified_duration_2;
    ELSIF (p_rec_format_layout.field_name = 'YIELD_2') THEN
        pgmloc := 1660;
        p_field_value := p_index_securities.yield_2;
    ELSIF (p_rec_format_layout.field_name = 'DURATION_2') THEN
        pgmloc := 1670;
        p_field_value := p_index_securities.duration_2;
-- ACB 15FEB2011
    ELSIF (p_rec_format_layout.field_name = 'YIELD_3') THEN
        pgmloc := 1680;
        p_field_value := p_index_securities.yield_3;
    ELSIF (p_rec_format_layout.field_name = 'YIELD_4') THEN
        pgmloc := 1690;
        p_field_value := p_index_securities.yield_4;
    ELSIF (p_rec_format_layout.field_name = 'TIME_MATUR_2') THEN
        pgmloc := 1700;
        p_field_value := p_index_securities.time_to_maturity_2;
    ELSIF (p_rec_format_layout.field_name = 'MOD_DURTN_3') THEN
        pgmloc := 1710;
        p_field_value := p_index_securities.modified_duration_3;
    ELSIF (p_rec_format_layout.field_name = 'DURATION_4') THEN
        pgmloc := 1720;
        p_field_value := p_index_securities.duration_4;
    ELSIF (p_rec_format_layout.field_name = 'DURATION_3') THEN
        pgmloc := 1730;
        p_field_value := p_index_securities.duration_3;
    ELSIF (p_rec_format_layout.field_name = 'CONVEXITY_2') THEN
        pgmloc := 1740;
        p_field_value := p_index_securities.convexity_2;
    ELSIF (p_rec_format_layout.field_name = 'CONVEXITY_3') THEN
        pgmloc := 1750;
        p_field_value := p_index_securities.convexity_3;
    ELSIF (p_rec_format_layout.field_name = 'CONVEXITY_4') THEN
        pgmloc := 1760;
        p_field_value := p_index_securities.convexity_4;
    ELSIF (p_rec_format_layout.field_name = 'CONVEXITY_5') THEN
        pgmloc := 1770;
        p_field_value := p_index_securities.convexity_5;
    ELSIF (p_rec_format_layout.field_name = 'CONVEXITY_6') THEN
        pgmloc := 1780;
        p_field_value := p_index_securities.convexity_6;
-- ACB 15FEB2011 - end of modif.
-- ADD ACB 19APR2011
    ELSIF (p_rec_format_layout.field_name = 'MOD_DURTN_4') THEN
        pgmloc := 1790;
        p_field_value := p_index_securities.modified_duration_4;
    ELSIF (p_rec_format_layout.field_name = 'MOD_DURTN_5') THEN
        pgmloc := 1800;
        p_field_value := p_index_securities.modified_duration_5;
-- END ADD ACB 19APR2011
-- SSI - 10FEB2011
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP') THEN
        pgmloc := 1810;
        p_field_value := p_index_securities.market_capitalization;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_2') THEN
        pgmloc := 1820;
        p_field_value := p_index_securities.market_capitalization_2;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_3') THEN
        pgmloc := 1830;
        p_field_value := p_index_securities.market_capitalization_3;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_4') THEN
        pgmloc := 1840;
        p_field_value := p_index_securities.market_capitalization_4;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_5') THEN
        pgmloc := 1850;
        p_field_value := p_index_securities.market_capitalization_5;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_6') THEN
        pgmloc := 1860;
        p_field_value := p_index_securities.market_capitalization_6;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_7') THEN
        pgmloc := 1870;
        p_field_value := p_index_securities.market_capitalization_7;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_8') THEN
        pgmloc := 1880;
        p_field_value := p_index_securities.market_capitalization_8;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_9') THEN
        pgmloc := 1890;
        p_field_value := p_index_securities.market_capitalization_9;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_10') THEN
        pgmloc := 1900;
        p_field_value := p_index_securities.market_capitalization_10;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_70') THEN
        pgmloc := 1910;
        p_field_value := p_index_securities.NFLD_70;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_71') THEN
        pgmloc := 1920;
        p_field_value := p_index_securities.NFLD_71;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_72') THEN
        pgmloc := 1930;
        p_field_value := p_index_securities.NFLD_72;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_73') THEN
        pgmloc := 1940;
        p_field_value := p_index_securities.NFLD_73;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_74') THEN
        pgmloc := 1950;
        p_field_value := p_index_securities.NFLD_74;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_75') THEN
        pgmloc := 1960;
        p_field_value := p_index_securities.NFLD_75;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_76') THEN
        pgmloc := 1970;
        p_field_value := p_index_securities.NFLD_76;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_77') THEN
        pgmloc := 1980;
        p_field_value := p_index_securities.NFLD_77;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_78') THEN
        pgmloc := 1990;
        p_field_value := p_index_securities.NFLD_78;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_79') THEN
        pgmloc := 2000;
        p_field_value := p_index_securities.NFLD_79;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_80') THEN
        pgmloc := 2010;
        p_field_value := p_index_securities.NFLD_80;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_81') THEN
        pgmloc := 2020;
        p_field_value := p_index_securities.NFLD_81;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_82') THEN
        pgmloc := 2030;
        p_field_value := p_index_securities.NFLD_82;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_83') THEN
        pgmloc := 2040;
        p_field_value := p_index_securities.NFLD_83;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_84') THEN
        pgmloc := 2050;
        p_field_value := p_index_securities.NFLD_84;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_85') THEN
        pgmloc := 2060;
        p_field_value := p_index_securities.NFLD_85;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_86') THEN
        pgmloc := 2070;
        p_field_value := p_index_securities.NFLD_86;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_87') THEN
        pgmloc := 2080;
        p_field_value := p_index_securities.NFLD_87;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_88') THEN
        pgmloc := 2090;
        p_field_value := p_index_securities.NFLD_88;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_89') THEN
        pgmloc := 2100;
        p_field_value := p_index_securities.NFLD_89;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_90') THEN
        pgmloc := 2110;
        p_field_value := p_index_securities.NFLD_90;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_91') THEN
        pgmloc := 2120;
        p_field_value := p_index_securities.NFLD_91;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_92') THEN
        pgmloc := 2130;
        p_field_value := p_index_securities.NFLD_92;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_93') THEN
        pgmloc := 2140;
        p_field_value := p_index_securities.NFLD_93;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_94') THEN
        pgmloc := 2150;
        p_field_value := p_index_securities.NFLD_94;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_95') THEN
        pgmloc := 2160;
        p_field_value := p_index_securities.NFLD_95;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_96') THEN
        pgmloc := 2170;
        p_field_value := p_index_securities.NFLD_96;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_97') THEN
        pgmloc := 2180;
        p_field_value := p_index_securities.NFLD_97;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_98') THEN
        pgmloc := 2190;
        p_field_value := p_index_securities.NFLD_98;
    ELSIF (p_rec_format_layout.field_name =  'NFLD_99') THEN
        pgmloc := 2200;
        p_field_value := p_index_securities.NFLD_99;
-- SSI - 10FEB2011 - end of modif.
-- SSI - 22NOV2012
    ELSIF (p_rec_format_layout.field_name =  'WEIGHT') THEN
        pgmloc := 2210;
        p_field_value := p_index_securities.WEIGHT;
-- SSI - 22NOV2012 - end of modif.
-- SSI - 25MAY2011
    ELSIF (p_rec_format_layout.field_name =  'SUPP_COUNTRY') THEN
        pgmloc := 2220;
        p_field_value := p_index_securities.Supp_Country_Code;
    ELSIF (p_rec_format_layout.field_name =  'SUPP_CTY_NAM') THEN
        pgmloc := 2230;
        p_field_value := p_index_securities.Supp_Country_Name;
    ELSIF (p_rec_format_layout.field_name =  'SUPP_CCY') THEN
        pgmloc := 2240;
        p_field_value := p_index_securities.Supp_Ccy;
    ELSIF (p_rec_format_layout.field_name =  'SUPP_CCY_NAM') THEN
        pgmloc := 2250;
        p_field_value := p_index_securities.Supp_Ccy_Name;
    ELSIF (p_rec_format_layout.field_name =  'RATING_CODE') THEN
        pgmloc := 2260;
        p_field_value := p_index_securities.Rating_Code;
    ELSIF (p_rec_format_layout.field_name =  'RATING_NAME') THEN
        pgmloc := 2270;
        p_field_value := p_index_securities.Rating_Description;
    ELSIF (p_rec_format_layout.field_name =  'ASSET_TYPE') THEN
        pgmloc := 2280;
        p_field_value := p_index_securities.Asset_Type;
    ELSIF (p_rec_format_layout.field_name =  'ASSET_TYPENA') THEN
        pgmloc := 2290;
        p_field_value := p_index_securities.Asset_Type_Name;
    ELSIF (p_rec_format_layout.field_name =  'BOND_CATEG') THEN
        pgmloc := 2300;
        p_field_value := p_index_securities.Bond_Category;
    ELSIF (p_rec_format_layout.field_name =  'MAT_DATE') THEN
        pgmloc := 2310;
-- SSI - 10APR2013
--      p_field_value := p_index_securities.Maturity_Date;
        p_field_value := to_char(p_index_securities.Maturity_Date,
                                 p_rec_format_layout.output_format);
    ELSIF (p_rec_format_layout.field_name =  'INCOME_FREQ') THEN
        pgmloc := 2320;
        p_field_value := p_index_securities.Income_Freq;
    ELSIF (p_rec_format_layout.field_name =  'SECTOR_1') THEN
        pgmloc := 2330;
        p_field_value := p_index_securities.Sector_1;
    ELSIF (p_rec_format_layout.field_name =  'ISSUER_CODE') THEN
        pgmloc := 2340;
        p_field_value := p_index_securities.Issuer_Code;
    ELSIF (p_rec_format_layout.field_name =  'CFLD') THEN
        pgmloc := 2350;
        p_field_value := p_index_securities.CFLD;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_2') THEN
        pgmloc := 2360;
        p_field_value := p_index_securities.CFLD_2;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_3') THEN
        pgmloc := 2370;
        p_field_value := p_index_securities.CFLD_3;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_4') THEN
        pgmloc := 2380;
        p_field_value := p_index_securities.CFLD_4;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_5') THEN
        pgmloc := 2390;
        p_field_value := p_index_securities.CFLD_5;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_6') THEN
        pgmloc := 2400;
        p_field_value := p_index_securities.CFLD_6;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_7') THEN
        pgmloc := 2410;
        p_field_value := p_index_securities.CFLD_7;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_8') THEN
        pgmloc := 2420;
        p_field_value := p_index_securities.CFLD_8;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_9') THEN
        pgmloc := 2430;
        p_field_value := p_index_securities.CFLD_9;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_10') THEN
        pgmloc := 2440;
        p_field_value := p_index_securities.CFLD_10;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_11') THEN
        pgmloc := 2450;
        p_field_value := p_index_securities.CFLD_11;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_12') THEN
        pgmloc := 2460;
        p_field_value := p_index_securities.CFLD_12;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_13') THEN
        pgmloc := 2470;
        p_field_value := p_index_securities.CFLD_13;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_14') THEN
        pgmloc := 2480;
        p_field_value := p_index_securities.CFLD_14;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_15') THEN
        pgmloc := 2490;
        p_field_value := p_index_securities.CFLD_15;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_16') THEN
        pgmloc := 2500;
        p_field_value := p_index_securities.CFLD_16;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_17') THEN
        pgmloc := 2510;
        p_field_value := p_index_securities.CFLD_17;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_18') THEN
        pgmloc := 2520;
        p_field_value := p_index_securities.CFLD_18;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_19') THEN
        pgmloc := 2530;
        p_field_value := p_index_securities.CFLD_19;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_20') THEN
        pgmloc := 2540;
        p_field_value := p_index_securities.CFLD_20;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_21') THEN
        pgmloc := 2550;
        p_field_value := p_index_securities.CFLD_21;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_22') THEN
        pgmloc := 2560;
        p_field_value := p_index_securities.CFLD_22;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_23') THEN
        pgmloc := 2570;
        p_field_value := p_index_securities.CFLD_23;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_24') THEN
        pgmloc := 2580;
        p_field_value := p_index_securities.CFLD_24;
    ELSIF (p_rec_format_layout.field_name =  'CFLD_25') THEN
        pgmloc := 2590;
        p_field_value := p_index_securities.CFLD_25;
--        
-- AP - 22Jul2015 - added field 'ALPHA_CODE' for FTSJ   
    ELSIF (p_rec_format_layout.field_name =  'ALPHA_CODE') THEN
        pgmloc := 2600;
        IF p_index_securities.fri_ticker IS NOT NULL THEN
           p_field_value := Security.Get_Sec_Id(p_index_securities.fri_ticker,'J','RAN','E');
        ELSE                                                  
           p_field_value := NULL;
        END IF;                                                            
-- AP - 22Jul2015 - End Modif 
    ELSE
        pgmloc := 2610;
        p_success := FALSE;
        v_message_code := '050';
        v_message_val1 := p_rec_format_layout.field_name;
        v_message_val2 := pgmloc;
    END IF;
END Interpret_Fields; -- for INDEX_SECURITIES
--
--
--
--------------------------------------------------------------------------------
-- INTERPRET_FIELDS (SUPP_INDEX_CONSTITUENTS)
--------------------------------------------------------------------------------
PROCEDURE Interpret_Fields(
    p_calc_date                 IN  DATE,
    p_rec_format_layout         IN  cur_format_layout%ROWTYPE,
    p_supp_index_constituents   IN  supp_index_constituents%ROWTYPE,
    p_series_type               IN  INTEGER,
    p_field_value               OUT VARCHAR2,
    p_success                   OUT BOOLEAN
)
AS
V_PreEur_Flag        VARCHAR2(1);
V_Dummy              INTEGER;
--
CURSOR Is_PreEur(I_CCY   CUrrencies.ISO_Currency%TYPE)
IS
SELECT Old_Euro_Flag
FROM Currencies
WHERE ISO_Currency = I_CCY;
--
CURSOR Is_FTSE_UK_IND
IS
SELECT 1
FROM Supp_index
WHERE Data_Supplier_Code='FTS'
AND   Supp_index_Code = p_supp_index_constituents.Supp_index_Code
AND   Index_Type='UK';
--
BEGIN
    IF (p_rec_format_layout.field_name = 'CONSTANT') THEN
        pgmloc := 2620;
        p_field_value := p_rec_format_layout.output_format;
    ELSIF (p_rec_format_layout.field_name = 'IGNORED') THEN
        pgmloc := 2630;
        p_field_value := NULL;
    ELSIF (p_rec_format_layout.field_name = 'DATA_SUPP') THEN
        pgmloc := 2640;
        p_field_value := g_data_supplier_code;
    ELSIF (p_rec_format_layout.field_name = 'DELIV_DATE') THEN
        pgmloc := 2650;
        p_field_value := to_char(p_supp_index_constituents.delivery_date,
                                 p_rec_format_layout.output_format);
    ELSIF (p_rec_format_layout.field_name = 'PRICE_DATE') THEN
        pgmloc := 2660;
        p_field_value := to_char(p_supp_index_constituents.price_date,
                                 p_rec_format_layout.output_format);
    ELSIF (p_rec_format_layout.field_name = 'FILE_DATE') THEN
        pgmloc := 2670;
-- SSI - 01OCT2009 - For component 'C' - Security characteristics - Price date was set as the effective date
--      p_field_value := to_char(p_calc_date,
--                               p_rec_format_layout.output_format);
        IF p_rec_format_layout.Component_Code = 'C'
        THEN
           p_field_value := to_char(p_supp_index_constituents.price_date,
                                    p_rec_format_layout.output_format);
        ELSE
           p_field_value := to_char(p_calc_date,
                                    p_rec_format_layout.output_format);
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID') THEN
        pgmloc := 2680;
        p_field_value := p_supp_index_constituents.supp_index_code;
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_2') THEN
        pgmloc := 2690;
        p_field_value := p_supp_index_constituents.supp_index_code_2;
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_3') THEN
        pgmloc := 2700;
        p_field_value := p_supp_index_constituents.supp_index_code_3;
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_4') THEN
        pgmloc := 2710;
        p_field_value := p_supp_index_constituents.supp_index_code_4;
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_5') THEN
        pgmloc := 2720;
        p_field_value := p_supp_index_constituents.supp_index_code_5;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID') THEN
        pgmloc := 2730;
        p_field_value := p_supp_index_constituents.supp_sec_id;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID_2') THEN
        pgmloc := 2740;
        p_field_value := p_supp_index_constituents.supp_sec_id_2;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID_3') THEN
        pgmloc := 2750;
        p_field_value := p_supp_index_constituents.supp_sec_id_3;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID_4') THEN
        pgmloc := 2760;
        p_field_value := p_supp_index_constituents.supp_sec_id_4;
    ELSIF (p_rec_format_layout.field_name = 'IND_SEC_ID_5') THEN
        pgmloc := 2770;
        p_field_value := p_supp_index_constituents.supp_sec_id_5;
    -- ADD ACB 07MAY2009
    ELSIF (p_rec_format_layout.field_name = 'STP_CUSIP_ID') THEN
        pgmloc := 2780;
        IF G_ID_Sys.EXISTS('C') THEN
            p_field_value := nvl(p_supp_index_constituents.supp_sec_id_3,
                                 c_clt_id_prefix || p_supp_index_constituents.supp_sec_id);
        ELSE
            p_field_value := NULL;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_SEDOL_ID') THEN
        pgmloc := 2790;
        IF G_ID_Sys.EXISTS('S') THEN
            p_field_value := nvl(p_supp_index_constituents.supp_sec_id_4,
                                 c_clt_id_prefix || p_supp_index_constituents.supp_sec_id);
        ELSE
            p_field_value := NULL;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_ISIN_ID') THEN
        pgmloc := 2800;
        --
        IF G_ID_Sys.EXISTS('Z') THEN
            p_field_value := nvl(p_supp_index_constituents.supp_sec_id_2,
                                 c_clt_id_prefix || p_supp_index_constituents.supp_sec_id);
        ELSE
            p_field_value := NULL;
        END IF;
    -- END ACB 07MAY2009
    ELSIF (p_rec_format_layout.field_name = 'ISIN') THEN
        pgmloc := 2810;
        p_field_value := p_supp_index_constituents.isin;
    ELSIF (p_rec_format_layout.field_name = 'CUSIP') THEN
        pgmloc := 2820;
        p_field_value := p_supp_index_constituents.cusip;
    ELSIF (p_rec_format_layout.field_name = 'SEDOL') THEN
        pgmloc := 2830;
        p_field_value := p_supp_index_constituents.sedol;
-- SSI - 22OCT2008
-- New statpro Identifier fields
    ELSIF (p_rec_format_layout.field_name = 'ISIN_ID') THEN
        pgmloc := 2840;
        -- ADD ACB 07MAY2009
        --p_field_value := NVL(p_supp_index_constituents.isin,
        --                     p_supp_index_constituents.supp_sec_id);
        IF G_ID_Sys.EXISTS('Z') THEN
            p_field_value := nvl(p_supp_index_constituents.supp_sec_id_2,
                                 c_clt_id_prefix || p_supp_index_constituents.supp_sec_id);
        ELSE
            p_field_value := NVL(p_supp_index_constituents.isin,
                                 p_supp_index_constituents.supp_sec_id);
        END IF;
        -- END ACB 07MAY2009
-- SSI - 11AUG2015
-- New field
    ELSIF (p_rec_format_layout.field_name = 'ISIN_ID2') THEN
        pgmloc := 2845;
        IF G_ID_Sys.EXISTS('Z') THEN
            p_field_value := nvl(nvl(p_supp_index_constituents.supp_sec_id_2,
                                     p_supp_index_constituents.isin),
-- SSI - 01FEB2016
--                               c_clt_id_prefix || p_supp_index_constituents.supp_sec_id);
                                 c_clt_id_prefix_2 || p_supp_index_constituents.supp_sec_id);
        ELSE
            p_field_value := NVL(p_supp_index_constituents.isin,
                                 p_supp_index_constituents.supp_sec_id);
        END IF;
-- End of modif - 11AUG2015
   -- Miglena - 01.03.2013 - add field 'SUPP_ISIN_ID 
    ELSIF (p_rec_format_layout.field_name = 'SUPP_ISIN_ID') THEN
        pgmloc := 2850;
        p_field_value := NVL(p_supp_index_constituents.isin,
                             p_supp_index_constituents.supp_sec_id);
    -- end Miglena
-- AP - 25Sep2014 - added field 'SUPP_ISIN_I2' 
    ELSIF (p_rec_format_layout.field_name = 'SUPP_ISIN_I2') THEN
        pgmloc := 2860;
        p_field_value := NVL(p_supp_index_constituents.isin, 
                             c_clt_id_prefix || p_supp_index_constituents.supp_sec_id); 
-- end AP   
    ELSIF (p_rec_format_layout.field_name = 'CUSIP_ID') THEN
        pgmloc := 2870;
        -- ADD ACB 07MAY2009
        --p_field_value := NVL(p_supp_index_constituents.cusip,
        --                     p_supp_index_constituents.supp_sec_id);
        --
        IF G_ID_Sys.EXISTS('C') THEN
            p_field_value := nvl(p_supp_index_constituents.supp_sec_id_3,
                                 c_clt_id_prefix || p_supp_index_constituents.supp_sec_id);
        ELSE
            p_field_value := NVL(p_supp_index_constituents.cusip,
                                 p_supp_index_constituents.supp_sec_id);
        END IF;
        -- END ACB 07MAY2009
    ELSIF (p_rec_format_layout.field_name = 'SEDOL_ID') THEN
        pgmloc := 2880;
        -- ADD ACB 07MAY2009
        --p_field_value := NVL(p_supp_index_constituents.sedol,
        --                     p_supp_index_constituents.supp_sec_id);
        IF G_ID_Sys.EXISTS('S') THEN
            p_field_value := nvl(p_supp_index_constituents.supp_sec_id_4,
                                 c_clt_id_prefix || p_supp_index_constituents.supp_sec_id);
        ELSE
            p_field_value := NVL(p_supp_index_constituents.sedol,
                                 p_supp_index_constituents.supp_sec_id);
        END IF;
        -- END ACB 07MAY2009
-- SSI - 22OCT2008 - End of Modif.
--  SSI - 16JUN2015
    ELSIF (p_rec_format_layout.field_name = 'SUPP_SEDOL_2') THEN
        pgmloc := 2890;
        p_field_value := NVL(p_supp_index_constituents.sedol,
                             c_clt_id_prefix || p_supp_index_constituents.supp_sec_id);
--  SSI - 16JUN2015 - end of modif
    ELSIF (p_rec_format_layout.field_name = 'TICKER') THEN
        pgmloc := 2900;
        p_field_value := p_supp_index_constituents.ticker;
    ELSIF (p_rec_format_layout.field_name = 'FRI_TICK_IND') THEN
        pgmloc := 2910;
        p_field_value := p_supp_index_constituents.fri_ticker_index;
    ELSIF (p_rec_format_layout.field_name = 'FRI_TICK_SEC') THEN
        pgmloc := 2920;
        p_field_value := p_supp_index_constituents.fri_ticker_sec;
    ELSIF (p_rec_format_layout.field_name = 'SEC_NAME') THEN
        pgmloc := 2930;
        p_field_value := p_supp_index_constituents.security_name;
    ELSIF (p_rec_format_layout.field_name = 'SEC_NAME_2') THEN
        pgmloc := 2940;
        p_field_value := p_supp_index_constituents.security_name_2;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP') THEN
        pgmloc := 2950;
        p_field_value := p_supp_index_constituents.market_capitalization;
-- SSI - 26OCT2011
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_FTS') THEN
        pgmloc := 2960;
        IF p_supp_index_constituents.Data_Supplier_Code='FTS'
        THEN
           pgmloc := 2970;
           OPEN Is_FTSE_UK_Ind;
           pgmloc := 2980;
           FETCH Is_FTSE_UK_Ind INTO V_Dummy;
           IF Is_FTSE_UK_Ind%FOUND
           THEN
              pgmloc := 2990;
              p_field_value := p_supp_index_constituents.market_capitalization / 100;
           ELSE
              p_field_value := p_supp_index_constituents.market_capitalization;
           END IF;
        ELSE
           p_field_value := p_supp_index_constituents.market_capitalization;
        END IF;
-- SSI - 26OCT2011 - end of modif.
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_2') THEN
        pgmloc := 3000;
        p_field_value := p_supp_index_constituents.market_capitalization_2;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_3') THEN
        pgmloc := 3010;
        p_field_value := p_supp_index_constituents.market_capitalization_3;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_4') THEN
        pgmloc := 3020;
        p_field_value := p_supp_index_constituents.market_capitalization_4;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_5') THEN
        pgmloc := 3030;
        p_field_value := p_supp_index_constituents.market_capitalization_5;
    ELSIF (p_rec_format_layout.field_name = 'WEIGHT') THEN
        pgmloc := 3040;
        p_field_value := p_supp_index_constituents.weight;
    ELSIF (p_rec_format_layout.field_name = 'WEIGHT_2') THEN
        pgmloc := 3050;
        p_field_value := p_supp_index_constituents.weight_2;
    ELSIF (p_rec_format_layout.field_name = 'WEIGHT_3') THEN
        pgmloc := 3060;
        p_field_value := p_supp_index_constituents.weight_3;
    ELSIF (p_rec_format_layout.field_name = 'WEIGHT_4') THEN
        pgmloc := 3070;
        p_field_value := p_supp_index_constituents.weight_4;
    ELSIF (p_rec_format_layout.field_name = 'WEIGHT_5') THEN
        pgmloc := 3080;
        p_field_value := p_supp_index_constituents.weight_5;
    ELSIF (p_rec_format_layout.field_name = 'SEC_RETURN') THEN
        pgmloc := 3090;
        p_field_value := p_supp_index_constituents.security_return;
ELSIF (p_rec_format_layout.field_name = 'SEC_RETURN_2') THEN
        pgmloc := 3100;
        p_field_value := p_supp_index_constituents.security_return_2;
    ELSIF (p_rec_format_layout.field_name = 'SEC_RETURN_3')  THEN
        pgmloc := 3110;
        p_field_value := p_supp_index_constituents.security_return_3;
    ELSIF (p_rec_format_layout.field_name = 'SEC_RETURN_4') THEN
        pgmloc := 3120;
        p_field_value := p_supp_index_constituents.security_return_4;
    ELSIF (p_rec_format_layout.field_name = 'SEC_RETURN_5') THEN
        pgmloc := 3130;
        p_field_value := p_supp_index_constituents.security_return_5;
    ELSIF (p_rec_format_layout.field_name = 'PRICE') THEN
        pgmloc := 3140;
        p_field_value := p_supp_index_constituents.price;
-- SSI - 10FEB2010
    ELSIF (p_rec_format_layout.field_name = 'PRC_GBP_ILS') THEN
        pgmloc := 3150;
        IF p_supp_index_constituents.price_iso_currency = 'GBX'
        THEN
            p_field_value := p_supp_index_constituents.price /100;
        ELSIF p_supp_index_constituents.price_iso_currency = 'ILX'
        THEN
            p_field_value := p_supp_index_constituents.price /100;
        ELSE
            p_field_value := p_supp_index_constituents.price;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'CCY_GBP_ILS') THEN
        pgmloc := 3160;
        IF p_supp_index_constituents.price_iso_currency = 'GBX'
        THEN
            p_field_value := 'GBP';
        ELSIF p_supp_index_constituents.price_iso_currency = 'ILX'
        THEN
            p_field_value := 'ILS';
        ELSE
            p_field_value := p_supp_index_constituents.price_iso_currency;
        END IF;
-- SSI - 10FEB2010 - end of modif.
    ELSIF (p_rec_format_layout.field_name = 'SUPP_CCY') THEN
        pgmloc := 3170;
        p_field_value := p_supp_index_constituents.price_iso_currency;
    ELSIF (p_rec_format_layout.field_name = 'SUPP_CCY_NAM') THEN
        pgmloc := 3180;
--      p_field_value := nvl(g_tbl_currencies(p_supp_index_constituents.price_iso_currency).text_e,
        IF p_supp_index_constituents.price_iso_currency IS NOT NULL AND
           g_tbl_iso_cur.EXISTS(p_supp_index_constituents.price_iso_currency)
        THEN
           p_field_value := g_tbl_iso_cur(p_supp_index_constituents.price_iso_currency).text_e;
        ELSE
           p_field_value := p_supp_index_constituents.price_iso_currency;
        END IF;
-- SSI - 17MAR2011
-- ISO EUR ccy code for pre eur currency
    ELSIF (p_rec_format_layout.field_name = 'EUR_PREEUR') THEN
        pgmloc := 3190;
        OPEN IS_PREEUR(p_supp_index_constituents.price_iso_currency);
        FETCH IS_PREEUR INTO V_PreEur_Flag;
        IF IS_PREEUR%FOUND and
           V_PreEur_Flag='Y'
        THEN
           p_field_value := 'EUR';
        ELSE
           p_field_value := p_supp_index_constituents.price_iso_currency;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'SHARE_CLASS') THEN
        pgmloc := 3200;
        p_field_value := p_supp_index_constituents.share_class;
    ELSIF (p_rec_format_layout.field_name = 'RATING_CODE') THEN
        pgmloc := 3210;
        p_field_value := p_supp_index_constituents.rating_code;
    ELSIF (p_rec_format_layout.field_name = 'RATING_NAME') THEN
        pgmloc := 3220;
        p_field_value := p_supp_index_constituents.rating_description;
    ELSIF (p_rec_format_layout.field_name = 'ASSET_TYPE') THEN
        pgmloc := 3230;
        p_field_value := p_supp_index_constituents.asset_type_code;
    ELSIF (p_rec_format_layout.field_name = 'ASSET_TYPENA') THEN
        pgmloc := 3240;
        p_field_value := p_supp_index_constituents.asset_type_description;
    ELSIF (p_rec_format_layout.field_name = 'BOND_CATEG') THEN
        pgmloc := 3250;
        p_field_value := p_supp_index_constituents.bond_category;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD') THEN
        pgmloc := 3260;
        p_field_value := p_supp_index_constituents.calc_field_01;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD_2') THEN
        pgmloc := 3270;
        p_field_value := p_supp_index_constituents.calc_field_02;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD_3') THEN
        pgmloc := 3280;
        p_field_value := p_supp_index_constituents.calc_field_03;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD_4') THEN
        pgmloc := 3290;
        p_field_value := p_supp_index_constituents.calc_field_04;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD_5') THEN
        pgmloc := 3300;
        p_field_value := p_supp_index_constituents.calc_field_05;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD_6') THEN
        pgmloc := 3310;
        p_field_value := p_supp_index_constituents.calc_field_06;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD_7') THEN
        pgmloc := 3320;
        p_field_value := p_supp_index_constituents.calc_field_07;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD_8') THEN
        pgmloc := 3330;
        p_field_value := p_supp_index_constituents.calc_field_08;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD_9') THEN
        pgmloc := 3340;
        p_field_value := p_supp_index_constituents.calc_field_09;
    ELSIF (p_rec_format_layout.field_name = 'CALC_FIELD10') THEN
        pgmloc := 3350;
        p_field_value := p_supp_index_constituents.calc_field_10;
    ELSIF (p_rec_format_layout.field_name = 'CFLD') THEN
        pgmloc := 3360;
        p_field_value := p_supp_index_constituents.field_01;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_2') THEN
        pgmloc := 3370;
        p_field_value := p_supp_index_constituents.field_02;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_3') THEN
        pgmloc := 3380;
        p_field_value := p_supp_index_constituents.field_03;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_4') THEN
        pgmloc := 3390;
        p_field_value := p_supp_index_constituents.field_04;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_5') THEN
        pgmloc := 3400;
        p_field_value := p_supp_index_constituents.field_05;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_6') THEN
        pgmloc := 3410;
        p_field_value := p_supp_index_constituents.field_06;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_7') THEN
        pgmloc := 3420;
        p_field_value := p_supp_index_constituents.field_07;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_8') THEN
        pgmloc := 3430;
        p_field_value := p_supp_index_constituents.field_08;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_9') THEN
        pgmloc := 3440;
        p_field_value := p_supp_index_constituents.field_09;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_10') THEN
        pgmloc := 3450;
        p_field_value := p_supp_index_constituents.field_10;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_11') THEN
        pgmloc := 3460;
        p_field_value := p_supp_index_constituents.field_11;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_12') THEN
        pgmloc := 3470;
        p_field_value := p_supp_index_constituents.field_12;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_13') THEN
        pgmloc := 3480;
        p_field_value := p_supp_index_constituents.field_13;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_14') THEN
        pgmloc := 3490;
        p_field_value := p_supp_index_constituents.field_14;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_15') THEN
        pgmloc := 3500;
        p_field_value := p_supp_index_constituents.field_15;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_16') THEN
        pgmloc := 3510;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_16;
        p_field_value := substr(p_supp_index_constituents.field_19, 1, 2);
    ELSIF (p_rec_format_layout.field_name = 'CFLD_17') THEN
        pgmloc := 3520;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_17;
        p_field_value := substr(p_supp_index_constituents.Field_19, 1, 4);
    ELSIF (p_rec_format_layout.field_name = 'CFLD_18') THEN
        pgmloc := 3530;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_18;
        p_field_value := substr(p_supp_index_constituents.Field_19, 1, 6);
    ELSIF (p_rec_format_layout.field_name = 'CFLD_19') THEN
        pgmloc := 3540;
        p_field_value := p_supp_index_constituents.field_19;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_20') THEN
        pgmloc := 3550;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_20;
        p_field_value := Index_Reports.Get_Industry(p_supp_index_constituents.field_23);
    ELSIF (p_rec_format_layout.field_name = 'CFLD_21') THEN
        pgmloc := 3560;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_21;
        p_field_value := Index_Reports.Get_SuperSector(p_supp_index_constituents.field_23);
    ELSIF (p_rec_format_layout.field_name = 'CFLD_22') THEN
        pgmloc := 3570;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_22;
        p_field_value := Index_Reports.Get_Sector(p_supp_index_constituents.Field_23);
    ELSIF (p_rec_format_layout.field_name = 'CFLD_23') THEN
        pgmloc := 3580;
        p_field_value := p_supp_index_constituents.field_23;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_24') THEN
        pgmloc := 3590;
        p_field_value := p_supp_index_constituents.field_24;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_25') THEN
        pgmloc := 3600;
        p_field_value := p_supp_index_constituents.field_25;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_26') THEN
        pgmloc := 3610;
        p_field_value := p_supp_index_constituents.field_26;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_27') THEN
        pgmloc := 3620;
        p_field_value := p_supp_index_constituents.field_27;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_28') THEN
        pgmloc := 3630;
        p_field_value := p_supp_index_constituents.field_28;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_29') THEN
        pgmloc := 3640;
        p_field_value := p_supp_index_constituents.field_29;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_30') THEN
        pgmloc := 3650;
        p_field_value := p_supp_index_constituents.field_30;
-- SSI - 11SEP2008
-- New fields SUPP_COUNTRY and SUPP_CTY_NAM
    ELSIF (p_rec_format_layout.field_name = 'SUPP_COUNTRY') THEN
        pgmloc := 3660;
--      p_field_value := p_supp_index_constituents.iso_nation_code;
-- SSI - 14MAY2009
--      p_field_value := p_supp_index_constituents.Supp_Country_Code;
-- when Fetching priority on supp_country_code, get all iso_nation_code and
-- supp_country_code and supp_country_name
        p_field_value := NVL(p_supp_index_constituents.Supp_Country_Code,
                             p_supp_index_constituents.iso_nation_code);
    ELSIF (p_rec_format_layout.field_name = 'ISO_NAT') THEN
        pgmloc := 3670;
        p_field_value := p_supp_index_constituents.iso_nation_code;
    ELSIF (p_rec_format_layout.field_name = 'SUPP_CTY_NAM') THEN
--
-- 14MAY2009
-- when Fetching priority on supp_country_code, get all iso_nation_code and
-- supp_country_code and supp_country_name
        pgmloc := 3680;
        p_field_value := p_supp_index_constituents.Supp_Country_Name;
        IF p_field_value IS NULL AND
           p_supp_index_constituents.iso_nation_code IS NOT NULL
        THEN
        BEGIN
            SELECT text_e
            INTO p_field_value
            FROM nations
            WHERE iso_nation = p_supp_index_constituents.iso_nation_code;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                p_field_value := NULL;
        END;
        END IF ;
-- 14MAY2009
        IF p_field_value IS NULL
        THEN
           p_field_value := NVL(p_supp_index_constituents.Supp_Country_Code,
                                p_supp_index_constituents.iso_nation_code);
        END IF;
-- 14MAY2009 - End of modif.
    ELSIF (p_rec_format_layout.field_name = 'SECTOR') THEN
        pgmloc := 3690;
        p_field_value := substr(p_supp_index_constituents.gics, 1, 2);
    ELSIF (p_rec_format_layout.field_name = 'SECTOR_NAME') THEN
        pgmloc := 3700;
        p_field_value := Get_GICS_Description(
                            substr(p_supp_index_constituents.gics, 1, 2)
                         );
    ELSIF (p_rec_format_layout.field_name = 'GROUP') THEN
        pgmloc := 3710;
        p_field_value := substr(p_supp_index_constituents.gics, 1, 4);
    ELSIF (p_rec_format_layout.field_name = 'GROUP_NAME') THEN
        pgmloc := 3720;
        p_field_value := Get_GICS_Description(
                            substr(p_supp_index_constituents.gics, 1, 4)
                         );
    ELSIF (p_rec_format_layout.field_name = 'GIND') THEN
        pgmloc := 3730;
        p_field_value := substr(p_supp_index_constituents.gics, 1, 6);
    ELSIF (p_rec_format_layout.field_name = 'GIND_NAME') THEN
        pgmloc := 3740;
        p_field_value := Get_GICS_Description(
                            substr(p_supp_index_constituents.gics, 1, 6)
                         );
    ELSIF (p_rec_format_layout.field_name = 'SUBIND') THEN
        pgmloc := 3750;
        p_field_value := p_supp_index_constituents.gics;
    ELSIF (p_rec_format_layout.field_name = 'SUBIND_NAME') THEN
        pgmloc := 3760;
        p_field_value := Get_GICS_Description(
                            p_supp_index_constituents.gics
                         );
    ELSIF (p_rec_format_layout.field_name = 'IND_LNAME') THEN
        pgmloc := 3770;
        p_field_value := Get_Series_Long_Name(
                            g_data_supplier_code,
                            p_supp_index_constituents.supp_index_code,
                            to_char(p_supp_index_constituents.delivery_date, 'hh24mi'),
                            p_series_type
                         );
-- SSI - 30OCT2008
-- ICB fields
-- SubSector(4),Sector(3),SuperSector(2),Industry(1)
    ELSIF (p_rec_format_layout.field_name = 'ICB_SUB_SEC') THEN
        pgmloc := 3780;
        p_field_value := Index_Reports.Get_SubSector(p_supp_index_constituents.sector);
    ELSIF (p_rec_format_layout.field_name = 'ICB_SECTOR') THEN
        pgmloc := 3790;
        p_field_value := Index_Reports.Get_Sector(p_supp_index_constituents.sector);
    ELSIF (p_rec_format_layout.field_name = 'ICB_SUPER') THEN
        pgmloc := 3800;
        p_field_value := Index_Reports.Get_SuperSector(p_supp_index_constituents.sector);
    ELSIF (p_rec_format_layout.field_name = 'ICB_IND') THEN
        pgmloc := 3810;
        p_field_value := Index_Reports.Get_Industry(p_supp_index_constituents.sector);
    ELSIF (p_rec_format_layout.field_name = 'ICB_SUBSECNA') THEN
        pgmloc := 3820;
        p_field_value := Index_Reports.Get_ICB_Text(p_supp_index_constituents.sector);
    ELSIF (p_rec_format_layout.field_name = 'ICB_SECTORNA') THEN
        pgmloc := 3830;
        p_field_value := Index_Reports.Get_ICB_Text(Index_Reports.Get_Sector(p_supp_index_constituents.sector));
    ELSIF (p_rec_format_layout.field_name = 'ICB_SUPER_NA') THEN
        pgmloc := 3840;
        p_field_value := Index_Reports.Get_ICB_Text(Index_Reports.Get_SuperSector(p_supp_index_constituents.sector));
    ELSIF (p_rec_format_layout.field_name = 'ICB_IND_NA') THEN
        pgmloc := 3850;
        p_field_value := Index_Reports.Get_ICB_Text(Index_Reports.Get_Industry(p_supp_index_constituents.sector));
-- SSI - 30OCT2008
    -- ADD ACB: 14-SEP-2008
        ELSIF (p_rec_format_layout.field_name = 'CFLD_1_NAME') THEN
        pgmloc := 3860;
        p_field_value := p_supp_index_constituents.field_01_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_2_NAME') THEN
        pgmloc := 3870;
        p_field_value := p_supp_index_constituents.field_02_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_3_NAME') THEN
        pgmloc := 3880;
        p_field_value := p_supp_index_constituents.field_03_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_4_NAME') THEN
        pgmloc := 3890;
        p_field_value := p_supp_index_constituents.field_04_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_5_NAME') THEN
        pgmloc := 3900;
        p_field_value := p_supp_index_constituents.field_05_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_6_NAME') THEN
        pgmloc := 3910;
        p_field_value := p_supp_index_constituents.field_06_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_7_NAME') THEN
        pgmloc := 3920;
        p_field_value := p_supp_index_constituents.field_07_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_8_NAME') THEN
        pgmloc := 3930;
        p_field_value := p_supp_index_constituents.field_08_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_9_NAME') THEN
        pgmloc := 3940;
        p_field_value := p_supp_index_constituents.field_09_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_10_NAME') THEN
        pgmloc := 3950;
        p_field_value := p_supp_index_constituents.field_10_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_11_NAME') THEN
        pgmloc := 3960;
        p_field_value := p_supp_index_constituents.field_11_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_12_NAME') THEN
        pgmloc := 3970;
        p_field_value := p_supp_index_constituents.field_12_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_13_NAME') THEN
        pgmloc := 3980;
        p_field_value := p_supp_index_constituents.field_13_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_14_NAME') THEN
        pgmloc := 3990;
        p_field_value := p_supp_index_constituents.field_14_name;
        IF p_field_value IS NULL AND
           p_supp_index_constituents.field_14 IS NOT NULL
        THEN
        BEGIN
            SELECT text_e
            INTO p_field_value
            FROM nations
            WHERE iso_nation = p_supp_index_constituents.field_14;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                p_field_value := NULL;
        END;
        END IF ;
-- If no name still, put the code in
        IF p_field_value IS NULL
        THEN
           p_field_value := p_supp_index_constituents.Field_14;
        END IF;
--  Same treatement as SUPP_CCY_NAME
    ELSIF (p_rec_format_layout.field_name = 'CFLD_15_NAME') THEN
        pgmloc := 4000;
--      SSI - 03JUL2009 - useless statement
--      p_field_value := p_supp_index_constituents.field_15_name;
        IF p_supp_index_constituents.Field_15 IS NOT NULL AND
           g_tbl_iso_cur.EXISTS(p_supp_index_constituents.Field_15)
        THEN
           p_field_value := g_tbl_iso_cur(p_supp_index_constituents.Field_15).text_e;
        ELSE
           p_field_value := p_supp_index_constituents.Field_15;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_16_NAME') THEN
        pgmloc := 4010;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_16_name;
        p_field_value := Get_GICS_Description(
                            substr(p_supp_index_constituents.field_19, 1, 2)
                         );
    ELSIF (p_rec_format_layout.field_name = 'CFLD_17_NAME') THEN
        pgmloc := 4020;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_17_name;
        p_field_value := Get_GICS_Description(
                            substr(p_supp_index_constituents.field_19, 1, 4)
                         );
    ELSIF (p_rec_format_layout.field_name = 'CFLD_18_NAME') THEN
        pgmloc := 4030;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_18_name;
        p_field_value := Get_GICS_Description(
                            substr(p_supp_index_constituents.field_19, 1, 6)
                         );
    ELSIF (p_rec_format_layout.field_name = 'CFLD_19_NAME') THEN
        pgmloc := 4040;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_19_name;
        p_field_value := Get_GICS_Description(
                            p_supp_index_constituents.field_19);
    ELSIF (p_rec_format_layout.field_name = 'CFLD_20_NAME') THEN
        pgmloc := 4050;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_20_name;
        p_field_value := Index_Reports.Get_ICB_Text(Index_Reports.Get_Industry(p_supp_index_constituents.field_23));
    ELSIF (p_rec_format_layout.field_name = 'CFLD_21_NAME') THEN
        pgmloc := 4060;
-- SSI 25MAY2009
--      p_field_value := p_supp_index_constituents.field_21_name;
        p_field_value := Index_Reports.Get_ICB_Text(Index_Reports.Get_SuperSector(p_supp_index_constituents.field_23));
    ELSIF (p_rec_format_layout.field_name = 'CFLD_22_NAME') THEN
        pgmloc := 4070;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_22_name;
        p_field_value := Index_Reports.Get_ICB_Text(Index_Reports.Get_Sector(p_supp_index_constituents.Field_23));
    ELSIF (p_rec_format_layout.field_name = 'CFLD_23_NAME') THEN
        pgmloc := 4080;
-- SSI - 25MAY2009
--      p_field_value := p_supp_index_constituents.field_23_name;
        p_field_value := Index_Reports.Get_ICB_Text(p_supp_index_constituents.Field_23);
    ELSIF (p_rec_format_layout.field_name = 'CFLD_24_NAME') THEN
        pgmloc := 4090;
        p_field_value := p_supp_index_constituents.field_24_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_25_NAME') THEN
        pgmloc := 4100;
        p_field_value := p_supp_index_constituents.field_25_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_26_NAME') THEN
        pgmloc := 4110;
        p_field_value := p_supp_index_constituents.field_26_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_27_NAME') THEN
        pgmloc := 4120;
        p_field_value := p_supp_index_constituents.field_27_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_28_NAME') THEN
        pgmloc := 4130;
        p_field_value := p_supp_index_constituents.field_28_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_29_NAME') THEN
        pgmloc := 4140;
        p_field_value := p_supp_index_constituents.field_29_name;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_30_NAME') THEN
        pgmloc := 4150;
        p_field_value := p_supp_index_constituents.field_30_name;
    ELSIF (p_rec_format_layout.field_name = 'STP_SECTOR') THEN
        pgmloc := 4160;
        IF (g_statpro_gics IS NULL) THEN
            g_statpro_gics := Get_Gics(TRUNC(p_calc_date),
                                       p_supp_index_constituents);
        END IF;
        IF g_statpro_gics IS NOT NULL
        THEN
           p_field_value := substr(g_statpro_gics, 1, 2);
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_SECTO_NA') THEN
        pgmloc := 4170;
        IF (g_statpro_gics IS NULL) THEN
            g_statpro_gics := Get_Gics(TRUNC(p_calc_date),
                                       p_supp_index_constituents);
        END IF;
        IF g_statpro_gics IS NOT NULL
        THEN
           p_field_value := Get_GICS_Description(substr(g_statpro_gics, 1, 2) );
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_GROUP') THEN
        pgmloc := 4180;
        IF (g_statpro_gics IS NULL) THEN
            g_statpro_gics := Get_Gics(TRUNC(p_calc_date),
                                       p_supp_index_constituents);
        END IF;
        IF g_statpro_gics IS NOT NULL
        THEN
            p_field_value := substr(g_statpro_gics, 1, 4);
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_GROUP_NA') THEN
        pgmloc := 4190;
        IF (g_statpro_gics IS NULL) THEN
            g_statpro_gics := Get_Gics(TRUNC(p_calc_date),
                                       p_supp_index_constituents);
        END IF;
        IF g_statpro_gics IS NOT NULL
        THEN
            p_field_value := Get_GICS_Description(substr(g_statpro_gics, 1, 4) );
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_GIND') THEN
        pgmloc := 4200;
        IF (g_statpro_gics IS NULL) THEN
            g_statpro_gics := Get_Gics(TRUNC(p_calc_date),
                                       p_supp_index_constituents);
        END IF;
        IF g_statpro_gics IS NOT NULL
        THEN
           p_field_value := substr(g_statpro_gics, 1, 6);
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_GIND_NA') THEN
        pgmloc := 4210;
        IF (g_statpro_gics IS NULL) THEN
            g_statpro_gics := Get_Gics(TRUNC(p_calc_date),
                                       p_supp_index_constituents);
        END IF;
        IF g_statpro_gics IS NOT NULL
        THEN
           p_field_value := Get_GICS_Description(substr(g_statpro_gics, 1, 6) );
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_GICS') THEN
        pgmloc := 4220;
        IF (g_statpro_gics IS NULL) THEN
            g_statpro_gics := Get_Gics(TRUNC(p_calc_date),
                                       p_supp_index_constituents);
        END IF;
        IF g_statpro_gics IS NOT NULL
        THEN
            p_field_value := g_statpro_gics;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'STP_GICS_NA') THEN
        pgmloc := 4230;
        IF (g_statpro_gics IS NULL) THEN
            g_statpro_gics := Get_Gics(TRUNC(p_calc_date),
                                       p_supp_index_constituents);
        END IF;
        IF g_statpro_gics IS NOT NULL
        THEN
            p_field_value := Get_GICS_Description(g_statpro_gics);
        END IF;
--        
-- AP - 22July2015 - added field 'ALPHA_CODE' for FTSJ   
    ELSIF (p_rec_format_layout.field_name =  'ALPHA_CODE') THEN
        pgmloc := 4240;
        IF p_supp_index_constituents.fri_ticker_sec IS NOT NULL THEN
           p_field_value := Security.Get_Sec_Id(p_supp_index_constituents.fri_ticker_sec,'J','RAN','E');
        ELSE                                                  
           p_field_value := NULL;
        END IF;                                                            
-- AP - 22Jul2015 - End Modif
    ELSE
        pgmloc := 4250;
        p_success := FALSE;
        v_message_code := '050';
        v_message_val1 := p_rec_format_layout.field_name;
        v_message_val2 := pgmloc;
    END IF;
END Interpret_Fields; -- for SUPP_INDEX_CONSTITUENTS
--
--
--
--------------------------------------------------------------------------------
-- INTERPRET_FIELDS (INDEX_SERIES)
--------------------------------------------------------------------------------
PROCEDURE Interpret_Fields(
-- SSI - 30SEP2008
    p_calc_date                 IN  DATE,
    p_rec_format_layout     IN  cur_format_layout%ROWTYPE,
    p_index_series          IN  index_series%ROWTYPE,
    p_series_type           IN  INTEGER,
    p_field_value           OUT VARCHAR2,
    p_success               oUT BOOLEAN
)
AS
BEGIN
    p_field_value := NULL;
    IF (p_rec_format_layout.field_name = 'CONSTANT') THEN
        pgmloc := 4260;
        p_field_value := p_rec_format_layout.output_format;
    ELSIF (p_rec_format_layout.field_name = 'IGNORED') THEN
        pgmloc := 4270;
        p_field_value := NULL;
    ELSIF (p_rec_format_layout.field_name = 'DATA_SUPP') THEN
        p_field_value := g_data_supplier_code;
    ELSIF (p_rec_format_layout.field_name = 'PRICE_DATE') THEN
        pgmloc := 4280;
        p_field_value := to_char(p_index_series.delivery_date,
                                 p_rec_format_layout.output_format);
-- SSI - 30SEP2008
    ELSIF (p_rec_format_layout.field_name = 'FILE_DATE') THEN
        pgmloc := 4290;
        p_field_value := to_char(p_calc_date,
                                 p_rec_format_layout.output_format);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID') THEN
        pgmloc := 4300;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_2') THEN
        pgmloc := 4310;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_2,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_3') THEN
        pgmloc := 4320;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_3,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_4') THEN
        pgmloc := 4330;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_4,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_5') THEN
        pgmloc := 4340;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_5,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_6') THEN
        pgmloc := 4350;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_6,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_7') THEN
        pgmloc := 4360;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_7,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_8') THEN
        pgmloc := 4370;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_8,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_9') THEN
        pgmloc := 4380;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_9,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'INDEX_ID_10') THEN
        pgmloc := 4390;
        p_field_value := Return_Series_Identifier(p_index_series.supp_index_code_10,
                                                  p_series_type);
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL01') THEN
        pgmloc := 4400;
        p_field_value := p_index_series.series_value;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL02') THEN
        pgmloc := 4410;
        p_field_value := p_index_series.series_value_2;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL03') THEN
        pgmloc := 4420;
        p_field_value := p_index_series.series_value_3;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL04') THEN
        pgmloc := 4430;
        p_field_value := p_index_series.series_value_4;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL05') THEN
        pgmloc := 4440;
        p_field_value := p_index_series.series_value_5;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL06') THEN
        pgmloc := 4450;
        p_field_value := p_index_series.series_value_6;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL07') THEN
        pgmloc := 4460;
        p_field_value := p_index_series.series_value_7;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL08') THEN
        pgmloc := 4470;
        p_field_value := p_index_series.series_value_8;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL09') THEN
        pgmloc := 4480;
        p_field_value := p_index_series.series_value_9;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL10') THEN
        pgmloc := 4490;
        p_field_value := p_index_series.series_value_10;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL11') THEN
        pgmloc := 4500;
        p_field_value := p_index_series.series_value_11;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL12') THEN
        pgmloc := 4510;
        p_field_value := p_index_series.series_value_12;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL13') THEN
        pgmloc := 4520;
        p_field_value := p_index_series.series_value_13;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL14') THEN
        pgmloc := 4530;
        p_field_value := p_index_series.series_value_14;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL16') THEN
        pgmloc := 4540;
        p_field_value := p_index_series.series_value_16;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL17') THEN
        pgmloc := 4550;
        p_field_value := p_index_series.series_value_17;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL18') THEN
        pgmloc := 4560;
        p_field_value := p_index_series.series_value_18;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL19') THEN
        pgmloc := 4570;
        p_field_value := p_index_series.series_value_19;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL20') THEN
        pgmloc := 4580;
        p_field_value := p_index_series.series_value_20;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL21') THEN
        pgmloc := 4590;
        p_field_value := p_index_series.series_value_21;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL22') THEN
        pgmloc := 4600;
        p_field_value := p_index_series.series_value_22;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL23') THEN
        pgmloc := 4610;
        p_field_value := p_index_series.series_value_23;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL24') THEN
        pgmloc := 4620;
        p_field_value := p_index_series.series_value_24;
    ELSIF (p_rec_format_layout.field_name = 'SERIES_VAL25') THEN
        pgmloc := 4630;
        p_field_value := p_index_series.series_value_25;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP') THEN
        pgmloc := 4640;
        p_field_value := p_index_series.market_capitalization;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_2') THEN
        pgmloc := 4650;
        p_field_value := p_index_series.market_capitalization_2;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_3') THEN
        pgmloc := 4660;
        p_field_value := p_index_series.market_capitalization_3;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_4') THEN
        pgmloc := 4670;
        p_field_value := p_index_series.market_capitalization_4;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_5') THEN
        pgmloc := 4680;
        p_field_value := p_index_series.market_capitalization_5;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_6') THEN
        pgmloc := 4690;
        p_field_value := p_index_series.market_capitalization_6;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_7') THEN
        pgmloc := 4700;
        p_field_value := p_index_series.market_capitalization_7;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_8') THEN
        pgmloc := 4710;
        p_field_value := p_index_series.market_capitalization_8;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_9') THEN
        pgmloc := 4720;
        p_field_value := p_index_series.market_capitalization_9;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_10') THEN
        pgmloc := 4730;
        p_field_value := p_index_series.market_capitalization_10;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_11') THEN
        pgmloc := 4740;
        p_field_value := p_index_series.market_capitalization_11;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_12') THEN
        pgmloc := 4750;
        p_field_value := p_index_series.market_capitalization_12;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_13') THEN
        pgmloc := 4760;
        p_field_value := p_index_series.market_capitalization_13;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_14') THEN
        pgmloc := 4770;
        p_field_value := p_index_series.market_capitalization_14;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_15') THEN
        pgmloc := 4780;
        p_field_value := p_index_series.market_capitalization_15;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_16') THEN
        pgmloc := 4790;
        p_field_value := p_index_series.market_capitalization_16;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_17') THEN
        pgmloc := 4800;
        p_field_value := p_index_series.market_capitalization_17;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_18') THEN
        pgmloc := 4810;
        p_field_value := p_index_series.market_capitalization_18;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_19') THEN
        pgmloc := 4820;
        p_field_value := p_index_series.market_capitalization_19;
    ELSIF (p_rec_format_layout.field_name = 'MKT_CAP_20') THEN
        pgmloc := 4830;
        p_field_value := p_index_series.market_capitalization_20;
    ELSIF (p_rec_format_layout.field_name = 'SUPP_CCY') THEN
        pgmloc := 4840;
        p_field_value := Get_Expressed_Currency(
                            g_data_supplier_code,
                            p_index_series
                         );
-- SSI - 05JUN2015
--      IF p_filed_value='0' - meaning NULL
        IF p_field_value = '0' AND p_rec_format_layout.null_string IS NOT NULL
        THEN
            p_field_value := NULL;
        END IF;
-- SSI - 05JUN2015
    ELSIF (p_rec_format_layout.field_name = 'IND_LNAME') THEN
        pgmloc := 4850;
        p_field_value := Get_Series_Long_Name(
                            g_data_supplier_code,
                            p_index_series.supp_index_code,
                            to_char(p_index_series.delivery_date, 'hh24mi'),
                            p_series_type
                         );
    ELSIF (p_rec_format_layout.field_name = 'IND_NAME') THEN
        pgmloc := 4860;
         p_field_value := Get_Series_Description(
                      --    g_data_supplier_code,
                      --    p_index_series.supp_index_code,
                            p_index_series,
                            p_series_type
                          );
    ELSIF (p_rec_format_layout.field_name = 'IND_NAME_2') THEN
        pgmloc := 4870;
        p_field_value := p_index_series.supp_index_name_2;
    ELSIF (p_rec_format_layout.field_name = 'IND_TYPE') THEN
        pgmloc := 4880;
-- SSI - 19APR2011
--      p_field_value := p_series_type;
        IF p_series_type = c_series_type_const
        THEN
           p_field_value := c_series_type_series;
        ELSE
           p_field_value := p_series_type;
        END IF;
-- SSI - 05NOV2008
-- Special constant fields
-- These will be nullify for Market Cap Type series - those start with 'M'
    ELSIF (p_rec_format_layout.field_name = 'C_BNCH_FLAG') THEN
        pgmloc := 4890;
        IF (p_series_type = c_series_type_mkt_cap)
        THEN
           p_field_value := NULL;
        ELSE
           p_field_value := p_rec_format_layout.Output_Format;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'C_WGT_FLAG') THEN
        pgmloc := 4900;
        IF (p_series_type = c_series_type_mkt_cap)
        THEN
           p_field_value := NULL;
        ELSE
           p_field_value := p_rec_format_layout.Output_Format;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'C_RTN_FLAG') THEN
        pgmloc := 4910;
        IF (p_series_type = c_series_type_mkt_cap)
        THEN
           p_field_value := NULL;
        ELSE
           p_field_value := p_rec_format_layout.Output_Format;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'C_BNCH_FLAG2') THEN
        pgmloc := 4920;
        IF (p_series_type = c_series_type_mkt_cap)
        THEN
           p_field_value := NULL;
        ELSE
           IF p_series_Type = c_series_type_const
           THEN
              p_field_value := p_rec_format_layout.Output_Format;
           ELSE
              p_field_value := NULL;
           END IF;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'C_WGT_FLAG2') THEN
        pgmloc := 4930;
        IF (p_series_type = c_series_type_mkt_cap)
        THEN
           p_field_value := NULL;
        ELSE
           IF p_series_Type = c_series_type_const
           THEN
              p_field_value := p_rec_format_layout.Output_Format;
           ELSE
              p_field_value := NULL;
           END IF;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'C_RTN_FLAG2') THEN
        pgmloc := 4940;
        IF (p_series_type = c_series_type_mkt_cap)
        THEN
           p_field_value := NULL;
        ELSE
           IF p_series_Type = c_series_type_const
           THEN
              p_field_value := p_rec_format_layout.Output_Format;
           ELSE
              p_field_value := NULL;
           END IF;
        END IF;
    ELSIF (p_rec_format_layout.field_name = 'CFLD') THEN
        pgmloc := 4950;
        p_field_value := p_index_series.cfld;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_2') THEN
        pgmloc := 4960;
        p_field_value := p_index_series.cfld_2;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_3') THEN
        pgmloc := 4970;
        p_field_value := p_index_series.cfld_3;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_4') THEN
        pgmloc := 4980;
        p_field_value := p_index_series.cfld_4;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_5') THEN
        pgmloc := 4990;
        p_field_value := p_index_series.cfld_5;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_6') THEN
        pgmloc := 5000;
        p_field_value := p_index_series.cfld_6;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_7') THEN
        pgmloc := 5010;
        p_field_value := p_index_series.cfld_7;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_8') THEN
        pgmloc := 5020;
        p_field_value := p_index_series.cfld_8;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_9') THEN
        pgmloc := 5030;
        p_field_value := p_index_series.cfld_9;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_10') THEN
        pgmloc := 5040;
        p_field_value := p_index_series.cfld_10;
--  SSI -22NOV2012
    ELSIF (p_rec_format_layout.field_name = 'CFLD_11') THEN
        pgmloc := 5050;
        p_field_value := p_index_series.cfld_11;
    ELSIF (p_rec_format_layout.field_name = 'CFLD_12') THEN
        pgmloc := 5060;
        p_field_value := p_index_series.cfld_12;
--  SSI - 08DEC2015
    ELSIF (p_rec_format_layout.field_name = 'NFLD_4') THEN
        pgmloc := 5070;
        p_field_value := p_index_series.nfld_4;
    ELSIF (p_rec_format_layout.field_name = 'NFLD_6') THEN
        pgmloc := 5070;
        p_field_value := p_index_series.nfld_6;
--  SSI - 08DEC2015 - end of modif
    ELSIF (p_rec_format_layout.field_name = 'NFLD_32') THEN
        pgmloc := 5070;
        p_field_value := p_index_series.nfld_32;
-- AP - 27May2015 - Added NFLD_53
    ELSIF (p_rec_format_layout.field_name = 'NFLD_53') THEN
        pgmloc := 5080;
        p_field_value := p_index_series.nfld_53;
-- AP - 27May2015 - End modif.
    ELSIF (p_rec_format_layout.field_name = 'NFLD_67') THEN
        pgmloc := 5090;
        p_field_value := p_index_series.nfld_67;
    ELSIF (p_rec_format_layout.field_name = 'NFLD_104') THEN
        pgmloc := 5100;
        p_field_value := p_index_series.nfld_104;
-- SSI - 22NOV2012
    -- ADD ACB 07MAY2009
    ELSIF (p_rec_format_layout.field_name = 'EXCH_SOURCE') THEN
        pgmloc := 5110;
        p_field_value := Get_Exchange_Rate_Source(p_index_series.data_supplier_code);
-- SSI - 21JUN2012
    ELSIF (p_rec_format_layout.field_name = 'SIM_RETURN') THEN
        pgmloc := 5120;
        p_field_value := p_index_series.Simulated_Benchmark_Return;
    ELSIF (p_rec_format_layout.field_name = 'TRUE_RETURN') THEN
        pgmloc := 5130;
        p_field_value := p_index_series.True_Benchmark_Return;
-- SSI - 21JUN2012 - end of modif.
--  SSI - 08DEC2015
    ELSIF (p_rec_format_layout.field_name = 'YIELD') THEN
        pgmloc := 5130;
        p_field_value := p_index_series.YIELD;
--  SSI - 08DEC2015 - end of modif
    ELSE
        pgmloc := 5140;
        p_success := FALSE;
        v_message_code := '050';
        v_message_val1 := p_rec_format_layout.field_name;
        v_message_val2 := pgmloc;
    END IF;
END Interpret_Fields; -- for INDEX_SERIES
--
--
--
--------------------------------------------------------------------------------
-- INTERPRET_FIELDS (INDEX_EXCHANGE_RATES)
--------------------------------------------------------------------------------
PROCEDURE Interpret_Fields(
-- SSI - 30SEP2008
    p_calc_date                 IN  DATE,
    p_rec_format_layout     IN  cur_format_layout%ROWTYPE,
    p_index_exchange_rates  IN  index_exchange_rates%ROWTYPE,
    p_field_value           OUT VARCHAR2,
    p_success               OUT BOOLEAN
)
AS
BEGIN
    IF (p_rec_format_layout.field_name = 'CONSTANT') THEN
        pgmloc := 5150;
        p_field_value := p_rec_format_layout.output_format;
    ELSIF (p_rec_format_layout.field_name = 'IGNORED') THEN
        pgmloc := 5160;
        p_field_value := NULL;
    ELSIF (p_rec_format_layout.field_name = 'DATA_SUPP') THEN
        pgmloc := 5170;
        p_field_value := p_index_exchange_rates.data_supplier_code;
    ELSIF (p_rec_format_layout.field_name = 'PRICE_DATE') THEN
        pgmloc := 5180;
        p_field_value := to_char(p_index_exchange_rates.delivery_date,
                                 p_rec_format_layout.output_format);
-- SSI - 30SEP2008
    ELSIF (p_rec_format_layout.field_name = 'FILE_DATE') THEN
        pgmloc := 5190;
        p_field_value := to_char(p_calc_date,
                                 p_rec_format_layout.output_format);
    ELSIF (p_rec_format_layout.field_name = 'BASE_CCY') THEN
        pgmloc := 5200;
        p_field_value := g_tbl_currencies(p_index_exchange_rates.base_currency).iso_currency;
    ELSIF (p_rec_format_layout.field_name = 'CURRENCY') THEN
        pgmloc := 5210;
        p_field_value := g_tbl_currencies(p_index_exchange_rates.currency).iso_currency;
    ELSIF (p_rec_format_layout.field_name = 'CUR_NAME_SP') THEN
        p_field_value := Get_Exchange_Series_Code(
                            g_tbl_currencies(p_index_exchange_rates.currency).iso_currency,
                            g_tbl_currencies(p_index_exchange_rates.base_currency).iso_currency,
                            c_spot_series
--                     -- ACB - 07MAY2009
--                       );
                         ) || ' ' || Get_Exchange_Rate_Source(p_index_exchange_rates.data_supplier_code);
    ELSIF (p_rec_format_layout.field_name = 'CUR_NAME_FW') THEN
        p_field_value := Get_Exchange_Series_Code(
                            g_tbl_currencies(p_index_exchange_rates.currency).iso_currency,
                            g_tbl_currencies(p_index_exchange_rates.base_currency).iso_currency,
                            c_fwrd_series
--                       );
                         -- ADD ACB 07MAY2009
                         ) || ' ' || Get_Exchange_Rate_Source(p_index_exchange_rates.data_supplier_code);
    ELSIF (p_rec_format_layout.field_name = 'CUR_NAME_IP') THEN
        p_field_value := Get_Exchange_Series_Code(
                            g_tbl_currencies(p_index_exchange_rates.currency).iso_currency,
                            g_tbl_currencies(p_index_exchange_rates.base_currency).iso_currency,
                            c_inter_fwd_series
--                   -- ADD ACB 07MAY2009
--                   --  );
                         ) || ' ' || Get_Exchange_Rate_Source(p_index_exchange_rates.data_supplier_code);
    ELSIF (p_rec_format_layout.field_name = 'EXCH_RATE') THEN
        pgmloc := 5220;
        p_field_value := p_index_exchange_rates.spot_rate;
    ELSIF (p_rec_format_layout.field_name = 'FWD_RATE_2') THEN
        pgmloc := 5230;
        p_field_value := p_index_exchange_rates.forward_rate_2;
    ELSIF (p_rec_format_layout.field_name = 'FWD_RATE') THEN
        pgmloc := 5240;
        p_field_value := p_index_exchange_rates.forward_rate;
    ELSIF (p_rec_format_layout.field_name = 'CUR_LNAME_SP') THEN
        pgmloc := 5250;
        p_field_value := Get_Exchange_Series_Code(
                            g_tbl_currencies(p_index_exchange_rates.currency).iso_currency,
                            g_tbl_currencies(p_index_exchange_rates.base_currency).iso_currency,
                            c_spot_series
--                   -- ADD ACB 07MAY2009
--                   --  );
                         ) || ' ' || Get_Exchange_Rate_Source(p_index_exchange_rates.data_supplier_code);
    ELSIF (p_rec_format_layout.field_name = 'CUR_LNAME_FW') THEN
        pgmloc := 5260;
        p_field_value := Get_Exchange_Series_Code(
                            g_tbl_currencies(p_index_exchange_rates.currency).iso_currency,
                            g_tbl_currencies(p_index_exchange_rates.base_currency).iso_currency,
                            c_fwrd_series
--                   -- ADD ACB 07MAY2009
--                   --  );
                         ) || ' ' || Get_Exchange_Rate_Source(p_index_exchange_rates.data_supplier_code);
    ELSIF (p_rec_format_layout.field_name = 'CUR_LNAME_IP') THEN
        pgmloc := 5270;
        p_field_value := Get_Exchange_Series_Code(
                            g_tbl_currencies(p_index_exchange_rates.currency).iso_currency,
                            g_tbl_currencies(p_index_exchange_rates.base_currency).iso_currency,
                            c_inter_fwd_series
--                   -- ADD ACB 07MAY2009
--                   --  );
                         ) || ' ' || Get_Exchange_Rate_Source(p_index_exchange_rates.data_supplier_code);
    ELSIF (p_rec_format_layout.field_name = 'LOCAL_RETURN') THEN
        pgmloc := 5280;
        p_field_value := p_index_exchange_rates.local_return;
    ELSIF (p_rec_format_layout.field_name = 'HEDGED_RET') THEN
        pgmloc := 5290;
        p_field_value := p_index_exchange_rates.hedged_return_base_cur;
    ELSIF (p_rec_format_layout.field_name = 'NFLD') THEN
        pgmloc := 5300;
        p_field_value := p_index_exchange_rates.nfld_1;
    ELSIF (p_rec_format_layout.field_name = 'NFLD_2') THEN
        pgmloc := 5310;
        p_field_value := p_index_exchange_rates.nfld_2;
    ELSIF (p_rec_format_layout.field_name = 'NFLD_3') THEN
        pgmloc := 5320;
        p_field_value := p_index_exchange_rates.nfld_3;
    ELSIF (p_rec_format_layout.field_name = 'NFLD_4') THEN
        pgmloc := 5330;
        p_field_value := p_index_exchange_rates.nfld_4;
    ELSIF (p_rec_format_layout.field_name = 'NFLD_5') THEN
        pgmloc := 5340;
        p_field_value := p_index_exchange_rates.nfld_5;
    -- ADD ACB 07MAY2009
    ELSIF (p_rec_format_layout.field_name = 'EXCH_SOURCE') THEN
        pgmloc := 5350;
        p_field_value := Get_Exchange_Rate_Source(p_index_exchange_rates.data_supplier_code);
    ELSE
        pgmloc := 5360;
        p_success := FALSE;
        v_message_code := '050';
        v_message_val1 := p_rec_format_layout.field_name;
        v_message_val2 := pgmloc;
    END IF;
END Interpret_Fields; -- for EXCHANGE RATE
--
--
--
--------------------------------------------------------------------------------
-- FORMAT_FIELD
--------------------------------------------------------------------------------
PROCEDURE Format_Field(
    p_format_layout         IN cur_format_layout%ROWTYPE,
    p_field_value           IN VARCHAR2,
    p_field_value_formated  OUT VARCHAR2,
    p_success               OUT BOOLEAN
)
IS
-- SSI - 06MAY2011
    V_Factor_Pos       INTEGER;
    V_Factor_Code      VARCHAR2(5);
    V_Factor           NUMBER;
    V_Output_format    Client_format_layouts.Output_format%TYPE;
--  SSI - 02JUN2015
--  V_Field_Value      VARCHAR2(50);
    V_Field_Value      VARCHAR2(4000);
-- SSI - 06MAY2011 - end of modif.
BEGIN
    pgmloc := 5370;
    p_success := TRUE;
--
    IF (p_format_layout.field_datatype = 'NUMBER') THEN
       pgmloc := 5380;
        -- the round is for remove zeros from right side.
-- SSI - 06MAY2011
       V_Factor := 1;
       V_output_format := p_format_layout.output_format;
       V_field_Value   := p_field_Value;
--
       IF p_format_layout.output_format LIKE '#%*%'
       THEN
          V_Factor_Pos := INSTR(p_format_layout.output_format,'*');
          V_Factor_Code := SUBSTR(p_format_layout.output_format,V_Factor_Pos+1);
          IF V_Factor_Code = 'D'    -- ten
          THEN
              V_Factor := 10;
          ELSIF V_Factor_Code = 'H'  -- Hundred
          THEN
              V_Factor := 100;
          ELSIF V_Factor_Code = 'T'  -- Thousand
          THEN
              V_Factor := 1000;
          ELSIF V_Factor_Code = 'M'   -- Million
          THEN
              V_Factor := 1000000;
          END IF;
--
          v_field_value := TO_CHAR(TO_NUMBER(p_field_value) * V_Factor);
          V_output_format := SUBSTR(p_format_layout.output_format,2,V_Factor_Pos-2);
-- SSI - 22MAY2012
       ELSIF p_format_layout.output_format LIKE '#NZ%'
       THEN
          IF v_field_value = 0 THEN
             v_field_value := NULL;
          END IF;
          V_output_format := SUBSTR(p_format_layout.output_format,4);
-- SSI - 22MAY2012 -- end of modif
       END IF;
--      p_field_value_formated := round(to_char(p_field_value,
--                                              p_format_layout.output_format
        p_field_value_formated := round(to_char(v_field_value,
                                                v_output_format
-- SSI - 06MAY2011 - end of modif.
                                        ), 10);
       IF g_delimiter_type = 'SDVEU'
       THEN
          p_field_value_formated := REPLACE(p_field_value_formated,'.',',');
       END IF;
    ELSIF (p_format_layout.field_datatype = 'CHAR') THEN
        pgmloc := 5390;
        IF (p_field_value IS NOT NULL) THEN
            p_field_value_formated := p_field_value;
        ELSE
            p_field_value_formated := p_format_layout.null_string;
        END IF;
        --
        p_field_value_formated := replace(p_field_value_formated,
                                          g_field_delimiter,
                                          ' ');
    ELSIF (p_format_layout.field_datatype = 'DATE') THEN
        pgmloc := 5400;
-- SSI - 24JUL2012
--      p_field_value_formated := to_char(to_date(p_field_value,
--                                                c_date_format
--                                               ),
--                                        p_format_layout.output_format
--                                       );
        p_field_value_formated := TRIM(p_field_value);
    END IF;
    p_field_value_formated := nvl(nvl(p_field_value_formated,
                                      p_format_layout.null_string),
                                  p_format_layout.null_character);
    -- If the format is fixed, complet with blaks
    pgmloc := 5410;
    IF (g_delimiter_type = 'FIX') THEN
        pgmloc := 5420;
        p_field_value_formated := rpad(p_field_value_formated,
                                       p_format_layout.field_length
                                      );
    END IF; -- IF (g_delimiter_type = 'FIX')
    pgmloc := 5430;
EXCEPTION
    WHEN OTHERS THEN
        p_success := FALSE;
        v_message_code := '040';
        v_message_val1 := p_format_layout.field_name;
        v_message_val2 := p_field_value;
        v_message_val3 := p_format_layout.output_format;
END Format_Field;
--
--
--
--------------------------------------------------------------------------------
-- GET_FORMAT_RECORD (INDEX_SECURITIES)
--------------------------------------------------------------------------------
PROCEDURE Get_Format_Record(
    p_calc_date               IN DATE,
    p_index_securities        IN index_securities%ROWTYPE,
    p_fri_client              IN client_format_layouts.fri_client%TYPE,
    p_product_code            IN client_format_layouts.product_code%TYPE,
    p_component               IN client_format_layouts.component_code%TYPE,
    p_record_type             IN client_format_layouts.record_type_code%TYPE,
    p_series_type             IN INTEGER,
    p_record_formated         OUT VARCHAR2,
    p_success                 OUT BOOLEAN
)
IS
    v_field_value           VARCHAR2(4000);
    v_field_value_formated  VARCHAR2(1000);
    v_record_formated       VARCHAR2(4000);
    v_prev_record_position  client_format_layouts.record_position%TYPE;
    v_format_layout         cur_format_layout%ROWTYPE;
--  SSI - 05JUN2015
    V_Skip_Next             BOOLEAN;
BEGIN
    pgmloc := 5440;
    v_prev_record_position := 1;
--  SSI - 05JUN2015
    V_Skip_Next := FALSE;
    FOR v_ind IN 1..g_format_layout.COUNT LOOP
        pgmloc := 5450;
-- SSI - 14SEP2010
--      SSI - 05JUN2015
        IF V_Skip_Next 
        THEN 
           V_Skip_Next := FALSE;
           GOTO Next_Field;
        END IF;
--      SSI - 05JUN 2015 - end of modif
        v_format_layout := g_format_layout(v_ind);
        v_format_layout.Output_Format := REPLACE(v_format_layout.Output_Format,C_Merge_Str_Format);
-- SSI - 14SEP2010 - end of modif.
--      SSI - 05JUN2015
        v_format_layout.Output_Format := REPLACE(v_format_layout.Output_Format,C_Merge_Str_NotNull_Format);
        pgmloc := 5460;
        Interpret_Fields(p_calc_date,
-- SSI -14SEP2010
--                   --  g_format_layout(v_ind),
                         v_format_layout,
                         p_index_securities,
                         p_series_type,
                         v_field_value,
                         p_success
                        );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
--
        pgmloc := 5470;
-- SSI - 14SEP2010
--      Format_Field(g_format_layout(v_ind),
        Format_Field(v_format_layout,
                     v_field_value,
                     v_field_value_formated,
                     p_success
                    );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
        pgmloc := 5480;
        IF (v_prev_record_position = g_format_layout(v_ind).record_position)
        THEN
          IF g_format_layout(v_ind).Output_Format LIKE '%' || C_Merge_Str_Format
          THEN
            p_record_formated := p_record_formated      ||
                                 v_field_value_formated;
--        SSI - 05JUN2015
          ELSIF g_format_layout(v_ind).Output_Format LIKE '%' || C_Merge_Str_NotNull_Format
          THEN
            IF v_field_value_formated IS NOT NULL
            THEN
               p_record_formated := p_record_formated      ||
                                    v_field_value_formated;
            ELSE
               p_record_formated := p_record_formated      ||
                                    v_field_value_formated ||
                                    g_field_delimiter;
               V_Skip_Next := TRUE;
            END IF;
--        SSI - 05JUN2015
          ELSE
            p_record_formated := p_record_formated      ||
                                 v_field_value_formated ||
                                 g_field_delimiter;
          END IF;
        ELSE
            -- remove the last ','
            p_record_formated := substr(p_record_formated, 1,
                                        LENGTH(p_record_formated) - 1
                                        );
            p_record_formated := p_record_formated      ||
                                 C_LF                   ||
                                 v_field_value_formated ||
                                 g_field_delimiter;
        END IF;
<<Next_Field>>
        v_prev_record_position := g_format_layout(v_ind).record_position;
    END LOOP;
    -- remove the last ','
    p_record_formated := substr(p_record_formated, 1,
                                LENGTH(p_record_formated)-1
                               );
    pgmloc := 5490;
END Get_Format_Record;
--
--
--
--------------------------------------------------------------------------------
-- GET_FORMAT_RECORD (SUPP_INDEX_CONSTITUENTS)
--------------------------------------------------------------------------------
PROCEDURE Get_Format_Record(
    p_calc_date               IN DATE,
    p_supp_index_constituents IN supp_index_constituents%ROWTYPE,
-- SSI - 24MAR2011
    p_actual_fri_client       IN clients.fri_client%TYPE,
    p_fri_client              IN client_format_layouts.fri_client%TYPE,
    p_product_code            IN client_format_layouts.product_code%TYPE,
    p_component               IN client_format_layouts.component_code%TYPE,
    p_record_type             IN client_format_layouts.record_type_code%TYPE,
    p_series_type             IN INTEGER,
    p_record_formated         OUT VARCHAR2,
    p_success                 OUT BOOLEAN
)
IS
    v_field_value           VARCHAR2(4000);
    v_field_value_formated  VARCHAR2(1000);
    v_record_formated       VARCHAR2(4000);
    v_prev_record_position  client_format_layouts.record_position%TYPE;
-- SSI - 14SEP2010
    v_format_layout         cur_format_layout%ROWTYPE;
-- SSI - 15FEB2011
    V_Series_Desc           Supp_Index.Text_E%TYPE:=NULL;
    V_Series_Long_Name      Supp_Index.Short_Name%TYPE:=NULL;
-- SSI - 15FEB2011 - end of modif
--  SSI - 05JUN2015
    V_Skip_Next             BOOLEAN;
BEGIN
    pgmloc := 5500;
    v_prev_record_position := 1;
--  SSI - 05JUN2015
    V_Skip_Next := FALSE;
    FOR v_ind IN 1..g_format_layout.COUNT LOOP
        pgmloc := 5510;
--      SSI - 05JUN2015
        IF V_Skip_Next 
        THEN 
           V_Skip_Next := FALSE;
           GOTO Next_Field;
        END IF;
--      SSI - 05JUN 2015 - end of modif
-- SSI - 14SEP2010
        v_format_layout := g_format_layout(v_ind);
        v_format_layout.Output_Format := REPLACE(v_format_layout.Output_Format,C_Merge_Str_Format);
        pgmloc := 5520;
-- SSI - 14SEP2010 - end of modif.
--      SSI - 05JUN2015
        v_format_layout.Output_Format := REPLACE(v_format_layout.Output_Format,C_Merge_Str_NotNull_Format);
        pgmloc := 5530;
-- SSI - 15FEB2011
-- SSI - 24MAR2011
--      IF (v_format_layout.field_name = 'CLT_INDEX_ID') THEN
        IF (v_format_layout.field_name = 'INDEX_ID') THEN
           pgmloc := 5540;
-- SSI - 24MAR2011
--         v_field_value := Return_Clt_Series_Identifier(p_fri_client
           v_field_value := Return_Clt_Series_Identifier(p_actual_fri_client
                                                        ,p_product_code
                                                        ,p_supp_index_constituents.supp_index_code
                                                        ,TO_CHAR(p_supp_index_constituents.delivery_date,'HH24MI')
                                                        ,p_series_type);
-- SSI - 24MAR2011
--      ELSIF v_format_layout.field_name = 'CLT_IND_LNAM'
        ELSIF v_format_layout.field_name = 'IND_LNAME'
        THEN
            IF V_Series_Long_Name IS NULL THEN
               pgmloc := 5550;
               Get_Clt_Series_Names(
-- SSI - 24MAR2011
--                                 p_fri_client
                                   p_actual_fri_client
                                  ,p_product_code
                                  ,p_supp_index_constituents.Data_Supplier_code
                                  ,p_supp_index_constituents.supp_index_code
                                  ,to_char(p_supp_index_constituents.delivery_date, 'hh24mi')
                                  ,p_series_type
                                  ,v_series_Long_Name
                                  ,v_series_Desc
                                );
            END IF;
            v_field_value := V_Series_Long_name;
-- SSI - 24MAR2011
--      ELSIF (v_format_layout.field_name = 'CLT_IND_NAME') THEN
        ELSIF (v_format_layout.field_name = 'IND_NAME') THEN
            pgmloc := 5560;
            IF V_Series_Desc IS NULL THEN
               Get_Clt_Series_Names(
-- SSI - 24MAR2011
--                                 p_fri_client
                                   p_actual_fri_client
                                  ,p_product_code
                                  ,p_supp_index_constituents.Data_Supplier_code
                                  ,p_supp_index_constituents.supp_index_code
                                  ,to_char(p_supp_index_constituents.delivery_date, 'hh24mi')
                                  ,p_series_type
                                  ,v_series_Long_Name
                                  ,v_series_Desc
                               );
            END IF;
            v_field_value := V_Series_Desc;
        ELSE
-- SSI - 15FEB2011 - end of modif
        Interpret_Fields(p_calc_date,
-- SSI - 14SEP2010
--                       g_format_layout(v_ind),
                         v_format_layout,
                         p_supp_index_constituents,
                         p_series_type,
                         v_field_value,
                         p_success
                        );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
-- SSI - 15FEB20111
        END IF;
        pgmloc := 5570;
-- SSI - 14SEP2010
--      Format_Field(g_format_layout(v_ind),
        Format_Field(v_format_layout,
                     v_field_value,
                     v_field_value_formated,
                     p_success
                    );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
        pgmloc := 5580;
        IF (v_prev_record_position = g_format_layout(v_ind).record_position)
        THEN
          IF g_format_layout(v_ind).Output_Format LIKE '%' || C_Merge_Str_Format
          THEN
            p_record_formated := p_record_formated      ||
                                 v_field_value_formated;
--        SSI - 05JUN2015
          ELSIF g_format_layout(v_ind).Output_Format LIKE '%' || C_Merge_Str_NotNull_Format
          THEN
            IF v_field_value_formated IS NOT NULL
            THEN
               p_record_formated := p_record_formated      ||
                                    v_field_value_formated;
            ELSE
               p_record_formated := p_record_formated      ||
                                    v_field_value_formated ||
                                    g_field_delimiter;
              V_Skip_Next := TRUE;
            END IF;
--        SSI - 05JUN2015
          ELSE
            p_record_formated := p_record_formated      ||
                                 v_field_value_formated ||
                                 g_field_delimiter;
          END IF;
        ELSE
            -- remove the last ','
            p_record_formated := substr(p_record_formated, 1,
                                        LENGTH(p_record_formated) - 1
                                        );
            p_record_formated := p_record_formated      ||
                                 C_LF                   ||
                                 v_field_value_formated ||
                                 g_field_delimiter;
        END IF;
<<Next_Field>>
        v_prev_record_position := g_format_layout(v_ind).record_position;
    END LOOP;
    -- remove the last ','
    p_record_formated := substr(p_record_formated, 1,
                                LENGTH(p_record_formated)-1
                               );
    pgmloc := 5590;
END Get_Format_Record;
--
--
--
--------------------------------------------------------------------------------
-- GET_FORMAT_RECORD (INDEX_SERIES)
--------------------------------------------------------------------------------
PROCEDURE Get_Format_Record(
-- SSI - 30SEP2008
    p_calc_date               IN DATE,
    p_index_series            IN index_series%ROWTYPE,
-- SSI - 24MAR2011
    p_actual_fri_client       IN clients.fri_client%TYPE,
    p_fri_client              IN client_format_layouts.fri_client%TYPE,
    p_product_code            IN client_format_layouts.product_code%TYPE,
    p_component               IN client_format_layouts.component_code%TYPE,
    p_record_type             IN client_format_layouts.record_type_code%TYPE,
    p_series_type             IN INTEGER,
    p_record_formated         OUT VARCHAR2,
    p_success                 OUT BOOLEAN
)
IS
    v_field_value           VARCHAR2(4000);
    v_field_value_formated  VARCHAR2(1000);
    v_record_formated       VARCHAR2(4000);
    v_prev_record_position  client_format_layouts.record_position%TYPE;
-- SSI - 14SEP2010
    v_format_layout         cur_format_layout%ROWTYPE;
-- SSI - 15FEB2011
    V_Series_Desc           Supp_Index.Text_E%TYPE:=NULL;
    V_Series_Long_Name      Supp_Index.Short_Name%TYPE:=NULL;
--  SSI - 05JUN2015
    V_Skip_Next             BOOLEAN;
BEGIN
    pgmloc := 5600;
    v_prev_record_position := 1;
-- SSI - 23FEB2011
    p_record_formated := NULL;
--  SSI - 05JUN2015
    V_Skip_Next := FALSE;
    FOR v_ind IN 1..g_format_layout.COUNT LOOP
        pgmloc := 5610;
--      SSI - 05JUN2015
        IF V_Skip_Next 
        THEN 
           V_Skip_Next := FALSE;
           GOTO Next_Field;
        END IF;
--      SSI - 05JUN 2015 - end of modif
-- SSI - 14SEP2010
        v_format_layout := g_format_layout(v_ind);
        v_format_layout.Output_Format := REPLACE(v_format_layout.Output_Format,C_Merge_Str_Format);
-- SSI - 14SEP2010 - end of modif.
--      SSI - 05JUN2015
        v_format_layout.Output_Format := REPLACE(v_format_layout.Output_Format,C_Merge_Str_NotNull_Format);
-- SSI - 15FEB2011
-- SSI - 15FEB2011
-- SSI - 24MAR2011
--      IF (v_format_layout.field_name = 'CLT_INDEX_ID') THEN
        IF (v_format_layout.field_name = 'INDEX_ID') THEN
           pgmloc := 5620;
-- SSI - 24MAR2011
--         v_field_value := Return_Clt_Series_Identifier(p_fri_client
           v_field_value := Return_Clt_Series_Identifier(p_actual_fri_client
                                                        ,p_product_code
                                                        ,p_index_series.supp_index_code
                                                        ,TO_CHAR(p_index_series.delivery_date,'HH24MI')
                                                        ,p_series_type);
-- SSI - 24MAR2011
--      ELSIF v_format_layout.field_name = 'CLT_IND_LNAM'
        ELSIF v_format_layout.field_name = 'IND_LNAME'
        THEN
            IF V_Series_Long_Name IS NULL THEN
               pgmloc := 5630;
               Get_Clt_Series_Names(
-- SSI - 24MAR2011
--                                 p_fri_client
                                   p_actual_fri_client
                                  ,p_product_code
                                  ,p_index_series.Data_Supplier_code
                                  ,p_index_series.supp_index_code
                                  ,to_char(p_index_series.delivery_date, 'hh24mi')
                                  ,p_series_type
                                  ,v_series_Long_Name
                                  ,v_series_Desc
                                );
            END IF;
            v_field_value := V_Series_Long_name;
-- SSI - 24MAR2011
--      ELSIF (v_format_layout.field_name = 'CLT_IND_NAME') THEN
        ELSIF (v_format_layout.field_name = 'IND_NAME') THEN
            pgmloc := 5640;
            IF V_Series_Desc IS NULL THEN
               Get_Clt_Series_Names(
-- SSI - 24MAR2011
--                                 p_fri_client
                                   p_actual_fri_client
                                  ,p_product_code
                                  ,p_index_series.Data_Supplier_code
                                  ,p_index_series.supp_index_code
                                  ,to_char(p_index_series.delivery_date, 'hh24mi')
                                  ,p_series_type
                                  ,v_series_Long_Name
                                  ,v_series_Desc
                               );
            END IF;
            v_field_value := V_Series_Desc;
        ELSE
-- SSI - 15FEB2011 - end of modif
-- SSI - 30SEP2008
--      Interpret_Fields(g_format_layout(v_ind),
        Interpret_Fields(p_Calc_Date,
-- SSI - 14SEP2010
--                       g_format_layout(v_ind),
                         v_format_layout,
                         p_index_series,
                         p_series_type,
                         v_field_value,
                         p_success
                        );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
-- SSI -15FEB2011
        END IF;
        pgmloc := 5650;
-- SSI - 14SEP2010
--      Format_Field(g_format_layout(v_ind),
        Format_Field(v_format_layout,
                     v_field_value,
                     v_field_value_formated,
                     p_success
                    );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
        pgmloc := 5660;
        IF (v_prev_record_position = g_format_layout(v_ind).record_position)
        THEN
          IF g_format_layout(v_ind).Output_Format LIKE '%' || C_Merge_Str_Format
          THEN
        pgmloc := 5670;
            p_record_formated := p_record_formated      ||
                                 v_field_value_formated;
--        SSI - 05JUN2015
          ELSIF g_format_layout(v_ind).Output_Format LIKE '%' || C_Merge_Str_NotNull_Format
          THEN
            IF v_field_value_formated IS NOT NULL
            THEN
               p_record_formated := p_record_formated      ||
                                    v_field_value_formated;
            ELSE
               p_record_formated := p_record_formated      ||
                                    v_field_value_formated ||
                                    g_field_delimiter;
              V_Skip_Next := TRUE;
            END IF;
--        SSI - 05JUN2015
          ELSE
        pgmloc := 5680;
            p_record_formated := p_record_formated      ||
                                 v_field_value_formated ||
                                 g_field_delimiter;
          END IF;
        ELSE
        pgmloc := 5690;
            -- remove the last ','
            p_record_formated := substr(p_record_formated, 1,
                                        LENGTH(p_record_formated) - 1
                                        );
            p_record_formated := p_record_formated      ||
                                 C_LF                   ||
                                 v_field_value_formated ||
                                 g_field_delimiter;
        END IF;
<<Next_Field>>
        pgmloc := 5700;
        v_prev_record_position := g_format_layout(v_ind).record_position;
    END LOOP;
    -- remove the last ','
    p_record_formated := substr(p_record_formated, 1,
                                LENGTH(p_record_formated)-1
                               );
    pgmloc := 5710;
END Get_Format_Record; -- for INDEX_SERIES
--
--
--
--------------------------------------------------------------------------------
-- GET_FORMAT_RECORD (INDEX_EXCHANGE_RATES)
--------------------------------------------------------------------------------
PROCEDURE Get_Format_Record(
-- SSI - 30SEP2008
    p_calc_date               IN DATE,
    p_index_exchange_rates    IN index_exchange_rates%ROWTYPE,
    p_fri_client              IN client_format_layouts.fri_client%TYPE,
    p_product_code            IN client_format_layouts.product_code%TYPE,
    p_component               IN client_format_layouts.component_code%TYPE,
    p_record_type             IN client_format_layouts.record_type_code%TYPE,
    p_record_formated         OUT VARCHAR2,
    p_success                 OUT BOOLEAN
)
IS
    v_field_value           VARCHAR2(4000);
    v_field_value_formated  VARCHAR2(4000);
    v_record_formated       VARCHAR2(4000);
    v_num_group_rec         INTEGER;
    v_prev_record_position  client_format_layouts.record_position%TYPE;
--
    -- Structure to keep all fields not printed
    v_pos_field_not_print   t_pos_field_not_print := t_pos_field_not_print();
-- SSI - 14SEP2010
    v_format_layout         cur_format_layout%ROWTYPE;
--  SSI - 05JUN2015
    V_Skip_Next             BOOLEAN;
BEGIN
    --
    -- Verify if there is record that we should not print. Zero series value
    --    should not be printed.
    --
    pgmloc := 5720;
    v_pos_field_not_print.DELETE;
    v_num_group_rec := g_format_layout(g_format_layout.COUNT).record_position;
    FOR v_ind IN 1..g_format_layout.COUNT LOOP
        pgmloc := 5730;
        IF ( g_format_layout(v_ind).field_name = 'FWD_RATE_2'
             -- OR g_format_layout(v_ind).field_name = 'CUR_NAME_FW')
            AND p_index_exchange_rates.forward_rate_2 IS NULL) THEN
            pgmloc := 5740;
            Fields_To_Not_Print(g_format_layout(v_ind),
                                g_format_layout.COUNT,
                                v_num_group_rec,
                                v_pos_field_not_print);
        ELSIF ( g_format_layout(v_ind).field_name = 'EXCH_RATE'
                -- OR g_format_layout(v_ind).field_name = 'CUR_NAME_SP')
               AND p_index_exchange_rates.spot_rate IS NULL) THEN
            pgmloc := 5750;
            Fields_To_Not_Print(g_format_layout(v_ind),
                                g_format_layout.COUNT,
                                v_num_group_rec,
                                v_pos_field_not_print);
        ELSIF ( g_format_layout(v_ind).field_name = 'FWD_RATE'
                -- OR g_format_layout(v_ind).field_name = 'CUR_NAME_IP')
              AND p_index_exchange_rates.forward_rate IS NULL) THEN
            pgmloc := 5760;
            Fields_To_Not_Print(g_format_layout(v_ind),
                                g_format_layout.COUNT,
                                v_num_group_rec,
                                v_pos_field_not_print);
        END IF;
    END LOOP;
--
    pgmloc := 5770;
    v_prev_record_position := 1;
--  SSI - 05JUN2015
    V_Skip_Next := FALSE;
    FOR v_ind IN 1..g_format_layout.COUNT LOOP
        IF (v_ind NOT MEMBER OF v_pos_field_not_print) THEN
            pgmloc := 5780;
--      SSI - 05JUN2015
        IF V_Skip_Next 
        THEN 
           V_Skip_Next := FALSE;
           GOTO Next_Field;
        END IF;
--      SSI - 05JUN 2015 - end of modif
-- SSI - 14SEP2010
        v_format_layout := g_format_layout(v_ind);
        v_format_layout.Output_Format := REPLACE(v_format_layout.Output_Format,C_Merge_Str_Format);
-- SSI - 14SEP2010 - end of modif.
--      SSI - 05JUN2015
        v_format_layout.Output_Format := REPLACE(v_format_layout.Output_Format,C_Merge_Str_NotNull_Format);
-- SSI - 30SEP2008
--          Interpret_Fields(g_format_layout(v_ind),
            Interpret_Fields(p_calc_date,
-- SSI - 14SEP2010
--                           g_format_layout(v_ind),
                             v_format_layout,
                             p_index_exchange_rates,
                             v_field_value,
                             p_success
                            );
            IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
            pgmloc := 5790;
-- SSI - 14SEP2010
--          Format_Field(g_format_layout(v_ind),
            Format_Field(v_format_layout,
                         v_field_value,
                         v_field_value_formated,
                         p_success
                        );
            IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
            pgmloc := 5800;
            IF (v_prev_record_position = g_format_layout(v_ind).record_position)
            THEN
              IF g_format_layout(v_ind).Output_Format LIKE '%' || C_Merge_Str_Format
              THEN
                p_record_formated := p_record_formated      ||
                                     v_field_value_formated;
--        SSI - 05JUN2015
              ELSIF g_format_layout(v_ind).Output_Format LIKE '%' || C_Merge_Str_NotNull_Format
              THEN
                IF v_field_value_formated IS NOT NULL
                THEN
                   p_record_formated := p_record_formated      ||
                                        v_field_value_formated;
                ELSE
                   p_record_formated := p_record_formated      ||
                                        v_field_value_formated ||
                                        g_field_delimiter;
                  V_Skip_Next := TRUE;
                END IF;
--        SSI - 05JUN2015
              ELSE
                p_record_formated := p_record_formated      ||
                                     v_field_value_formated ||
                                     g_field_delimiter;
              END IF;
            ELSE
                -- remove the last ','
                p_record_formated := substr(p_record_formated, 1,
                                            LENGTH(p_record_formated) - 1
                                            );
                p_record_formated := p_record_formated      ||
                                     C_LF                   ||
                                     v_field_value_formated ||
                                     g_field_delimiter;
            END IF;
<<Next_Field>>
            v_prev_record_position:=g_format_layout(v_ind).record_position;
        END IF;
    END LOOP;
    -- remove the last ','
    p_record_formated := substr(p_record_formated, 1,
                                LENGTH(p_record_formated)-1
                               );
    -- remove the first LF, if exists
    IF (substr(p_record_formated, 1, 1) = C_LF) THEN
        p_record_formated := substr(p_record_formated, 2);
    END IF;
    pgmloc := 5810;
END Get_Format_Record; -- for INDEX_EXCHANGE_RATES
--
--
--
--------------------------------------------------------------------------------
-- CLEAN_CLIENT_OUTPUT
--------------------------------------------------------------------------------
PROCEDURE Clean_Client_Output(
    P_Fri_Client      IN     Clients.Fri_Client%TYPE,
    P_Product         IN     Products.Code%TYPE,
    P_Sub_Product     IN     Sub_Products.Sub_Product_Code%TYPE,
    P_Component       IN     Product_Components.Component_Code%TYPE,
    P_Date            IN     DATE,
    P_From_Date       IN     DATE,
    P_To_Date         IN     DATE,
    P_Success            OUT BOOLEAN,
    P_Message            OUT VARCHAR2
)
AS
BEGIN
   pgmloc := 5820;
   P_Success                   := FALSE;
   P_Message                   := NULL;
    -- Clean up the records for the client/product/date
   IF p_component IS NULL OR
      G_File_Def.EXISTS(P_Component)
   THEN
      pgmloc := 5830;
      Delete Client_Output_Files
      WHERE Fri_Client = P_Fri_Client
      AND   Product_Code = P_Product
      AND   Sub_Product_Code = P_Sub_Product
      AND   Component_Code = NVL(P_Component,Component_Code)
      AND   Date_Of_Prices = P_Date;
      G_Num_Del := G_Num_Del + SQL%ROWCOUNT;
   END IF;
   pgmloc := 5840;
   Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
-- Load_Supplier_Rules.Update_Prc_Msgs(g_program_id,
--                                     G_System_Date,
--                                     Pricing_Msgs_Rec,
--                                     p_success
--                                    );
   Global_File_Updates.Update_Pricing_Msgs( G_Program_ID
                                          , G_System_Date
                                          , C_Upd_Break_Pts
                                          , Pricing_Msgs_rec
                                          );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
   Commit;
   P_Success := TRUE;
EXCEPTION
    WHEN OTHERS THEN
        Manage_Messages.Get_Oasis_Message( C_GEN_Message||'100'
                                    , 'N'
                                    , pgmloc
                                    , SUBSTR( SQLERRM( SQLCODE ),1,512 )
                                    ,'Clean_Client_Output'
                                    , P_Message
                                   );
END Clean_Client_Output;
--
--
--
--------------------------------------------------------------------------------
-- OPEN_FILE
--------------------------------------------------------------------------------
PROCEDURE Open_File(
    p_file_mode         IN  VARCHAR2,
    p_calc_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_component_code    IN  product_components.component_code%TYPE,
    p_file_name         OUT VARCHAR2,
    p_file_handle       OUT UTL_FILE.File_Type,
    p_success           OUT BOOLEAN,
    p_family_name       IN  VARCHAR2 DEFAULT NULL
)
AS
    v_header            varchar2(4000);
BEGIN
    pgmloc := 5850;
    --
    -- If the output type is a file, open it and write the header
    --
    IF ( p_type_output = c_type_out_file OR p_flag_both ) THEN
        -- Get the file name
        Product_Component.Get_Dated_File_Name(p_fri_client,
                                              p_product_code,
                                              p_sub_product_code,
                                              p_component_code,
                                              p_calc_date,
                                              p_file_name,
                                              p_success,
                                              Pricing_Msgs_Rec.err_text
                                             );
        IF ( ( NOT p_success) OR (p_file_name IS NULL) ) THEN
            v_message_code := '020';
            v_message_val1 := p_product_code || '/' || p_sub_product_code;
            v_message_val2 := p_component_code;
            v_message_val3 := Pricing_Msgs_Rec.err_text;
            RAISE LOGICAL_ERROR;
        END IF;
--
        -- Some files, we have to put a name coming from SUPP_INDEX
        IF (p_family_name IS NOT NULL) THEN
            p_file_name := replace(p_file_name,
                                   c_index_type_pattern,
                                   p_family_name);
        END IF;
--
        -- open file
        pgmloc := 5860;
        Load_Supplier_Rules.Open_File(p_file_name,
                                      v_dir_name,
                                      p_file_mode,
                                      c_max_linesize,
                                      Pricing_Msgs_Rec,
                                      p_file_handle,
                                      p_success
                                     );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
--
-- SSI - 19MAY2009
-- Add condition IF to write header only if not in append mode.
        IF p_file_mode <> 'A'
        THEN -- { write header
          -- write the header
          pgmloc := 5870;
-- 17OCT2008
--      Load_Format_Layout(p_fri_client,
          Load_Format_Layout(G_Fmt_Fri_Client,
                             p_product_code,
                             p_component_code,
                             'H' -- record type
                            );
          pgmloc := 5880;
-- SSI - 30SEP2008
--        Get_Format_Record(v_rec_index_series,
          Get_Format_Record(p_calc_date,
                            v_rec_index_series,
-- SSI - 17OCT2008
--                          p_fri_client,
-- SSI - 24MAR2011
                            p_fri_client,
                            G_Fmt_Fri_Client,
                            p_product_code,
                            p_component_code,
                            'H',
                            NULL,
                            v_header,
                            p_success);
          pgmloc := 5890;
          Load_Supplier_Rules.Write_file(p_file_handle,
                                         v_header,
                                         Pricing_Msgs_Rec,
                                         p_success
                                        );
          g_format_layout.DELETE;
-- SSI - 19MAY2009
        END IF; --} Write header
    END IF; -- p_type_output = c_type_out_file
    pgmloc := 5900;
END Open_File;
--
--
--
--------------------------------------------------------------------------------
-- CLOSE_FILE
--------------------------------------------------------------------------------
PROCEDURE Close_File(
    p_file_name             IN VARCHAR2,
-- SSI - 07FEB2012
--  p_file_handle           IN UTL_FILE.File_Type,
    p_file_handle           IN OUT UTL_FILE.File_Type,
    p_component_code        IN product_components.component_code%TYPE,
    p_work_date             IN DATE,
    p_flag_both             IN BOOLEAN,
    p_type_output           IN VARCHAR2,
    p_success               OUT BOOLEAN
)
AS
BEGIN
    IF ( p_type_output = c_type_out_file OR p_flag_both ) THEN
        -- close file
        Load_Supplier_Rules.File_Close(p_file_handle,
                                       Pricing_Msgs_Rec,
                                       p_success
                                      );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
--
        pgmloc := 5910;
        v_num_files := v_num_files + 1;
        v_client_output_file(v_num_files).output_file_name:=p_file_name;
        v_client_output_file(v_num_files).component_code  := p_component_code;
--
        Load_Supplier_Rules.Write_file(v_log_file,
                                       '    ' || p_file_name || ' generated'
                                          || ' (data from '
                                          || to_char(p_work_date, 'dd-MON-yyyy')
                                          || ')',
                                       Pricing_Msgs_Rec,
                                       p_success
                                      );
    END IF; -- IF (p_flag_client_out_file)
--
--  pgmloc := 5920;
--  IF (p_table_name IS NOT NULL)
--      AND (p_type_output = c_type_out_table OR p_flag_both )
--  THEN
--      Load_Supplier_Rules.Write_file(v_log_file,
--                                     '    Inserted into ' || p_table_name
--                                       || ' (data from '
--                                       || to_char(p_work_date, 'dd-MON-yyyy')
--                                       || ')',
--                                     Pricing_Msgs_Rec,
--                                     p_success
--                                    );
--  END IF; -- p_type_output = c_type_out_table
--
--  pgmloc := 5930;
END Close_File;
--
--
--
--------------------------------------------------------------------------------
-- INSERT_CLIENT_OUTPUT_FILES
--------------------------------------------------------------------------------
PROCEDURE Insert_Client_Output_Files(
    p_fri_client         IN clients.fri_client%TYPE,
    p_product_code       IN products.code%TYPE,
    p_sub_product_code   IN sub_products.sub_product_code%TYPE,
    p_date               IN DATE,
    p_client_output_file IN t_client_output_files,
    p_success            OUT BOOLEAN
)
AS
BEGIN
    pgmloc := 5940;
    FOR indice IN 1..p_client_output_file.COUNT LOOP
        dbms_output.put_line('Insertint output file:' ||
                p_client_output_file(indice).component_code ||' '|| p_date || ' ' ||
                p_client_output_file(indice).output_file_name);
        INSERT INTO client_output_files (fri_client, product_code,
                    sub_product_code,
                    component_code, date_of_prices,  output_file_name,
                    send_to_client_flag, last_user, last_stamp)
        VALUES (p_fri_client, p_product_code, p_sub_product_code,
                p_client_output_file(indice).component_code, p_date,
                p_client_output_file(indice).output_file_name, 'Y',
                Constants.Get_User_ID, sysdate);
    END LOOP;
    pgmloc := 5950;
    p_success := TRUE;
EXCEPTION
    WHEN OTHERS THEN
        p_success := FALSE;
        Manage_Messages.Get_Oasis_Message(C_GEN_Message || '100',
                                          'N',
                                          pgmloc,
                                          SUBSTR(SQLERRM, 1, 150),
                                          'Insert_Client_Output_Files',
                                          Pricing_Msgs_Rec.err_text);
END Insert_Client_Output_Files;
--
--
--
--------------------------------------------------------------------------------
-- LOAD_CUSTOM_COLUMNS
--------------------------------------------------------------------------------
PROCEDURE Load_Custom_Columns(
    p_work_date              IN  DATE,
    p_fri_client             IN  clients.fri_client%TYPE,
    p_product_code           IN  products.code%TYPE,
    p_sub_product_code       IN  sub_products.sub_product_code%TYPE,
-- SSI - 03SEP2009
    p_Custom_Columns         IN  t_Out_Columns,
    p_Const                  IN OUT Supp_index_Constituents%ROWTYPE,
    p_success                OUT BOOLEAN
   )
AS
-- SSI - 24AUG2009
   C_Name_Len                INTEGER:=60;
--
   V_Const                   Supp_index_Constituents%ROWTYPE;
   V_First_Time              BOOLEAN;
   V_ICB                     VARCHAR2(4);
   V_GICS                    VARCHAR2(8);
--
-- SSI - 03SEP2009
   TYPE t_Const IS TABLE OF Supp_Index_Constituents%ROWTYPE;
   V_Sel_Const               t_Const;
   V_Open_Const              BOOLEAN;
   Col                       INTEGER;
   PCol                      INTEGER;
   N                         INTEGER;
--
-- SSI - 03SEP2009
-- CURSOR Get_Field_Name IS
-- SELECT DISTINCT Product_Field_Name_Code
-- FROM  Ind_Client_Supp_Fields
-- WHERE  Fri_Client = P_Fri_Client
-- AND    Product_Code = P_Product_Code
-- AND    Sub_Product_Code = P_Sub_Product_Code
-- AND    Active_Flag = 'Y';
--
   CURSOR Get_Const(I_Field          Product_Field_Names.Code%TYPE
                   ,I_Fri_Ticker     Tickers.Fri_Ticker%TYPE)
   IS
-- SSI - 10SEP2009
-- SELECT /*+ ALL_ROWS */
-- SSI - 07JAN2011
-- Revise Hints
-- SELECT /*+ ALL_ROWS
--            INDEX(F Ind_Client_Supp_Fields_X_02)
--            INDEX(S Supp_Index_X_02)
--            INDEX(C Supp_Index_Constituents_X_03) */
-- SSI - 29JAN2013
-- Remove all hints
-- SELECT /*+ INDEX(F Ind_Client_Supp_Fields_X_02)
--            INDEX(S Supp_Index_X_02)
--            INDEX(C Supp_Index_Constituents_X_04) */
-- SSI - 06NOV2015
-- SELECT
   SELECT /*+ INDEX(C SUPP_I_CO_PROD_32K_02) */ 
          C.*
   FROM   Supp_Index_Constituents C
         ,Ind_Client_Supp_Fields  F
         ,Supp_Index              S
   WHERE  F.Fri_Client = P_Fri_Client
   AND    F.Product_Code = P_Product_Code
   AND    F.Sub_Product_Code = P_Sub_Product_Code
   AND    F.Product_Field_Name_Code = I_Field
   AND    F.Active_Flag = 'Y'
   AND    S.Data_Supplier_Code = F.Data_Supplier_Code
   AND    S.Index_Type = F.Index_Type
   AND    C.Data_Supplier_Code = F.Data_Supplier_Code
   AND    C.Delivery_Date BETWEEN p_work_Date and p_work_Date+1
   AND    C.Supp_Index_Code = S.Supp_Index_Code
   AND    C.Fri_Ticker_Sec = I_Fri_Ticker
   ORDER BY F.Priority_Order;
BEGIN
    pgmloc := 5960;
    p_success := FALSE;
    V_First_Time := TRUE;
    IF P_Custom_Columns.COUNT = 0
    THEN
       GOTO End_Custom_Col;
-- SSI - 07JAN2011
-- Useless
--  ELSE
--     Col := P_Custom_Columns.FIRST;
--     LOOP
--        pgmloc := 5970;
--        EXIT WHEN Col = P_Custom_Columns.LAST;
--        Col := P_Custom_Columns.NEXT(Col);
--     END LOOP;
    END IF;
--
--  Note all fields_12 and up should be null unless a priority is defined
--  SSI - 03SEP2009
--  FOR F_Rec IN Get_Field_Name
    Col := P_Custom_Columns.FIRST;
    LOOP
       IF V_First_Time
       THEN
          V_First_time := FALSE;
          pgmloc := 5980;
-- SSI - 24AUG2009
--        P_Const.Field_12      := P_Const.Security_Name;
          pgmloc := 5990;
          P_Const.Field_12      := SUBSTR(P_Const.Security_Name,1,C_Name_Len);
          P_Const.Field_13      := P_Const.Asset_Type_Code;
          pgmloc := 6000;
          P_Const.Field_13_Name := NVL(P_Const.Asset_Type_Description,P_Const.Asset_Type_Code);
          P_Const.Field_14      := NVL(P_Const.Supp_Country_Code,
                                       P_Const.iso_nation_code);
          pgmloc := 6010;
          P_Const.Field_14_Name := P_Const.Supp_Country_Name;
          P_Const.Field_15      := P_Const.Price_ISO_Currency;
          pgmloc := 6020;
          P_Const.Field_15_Name := P_Const.Price_ISO_Currency;
          P_Const.Field_19      := P_Const.GICS;
          P_Const.Field_23      := P_Const.Sector;
       END IF;
--
-- SSI - 03SEP2009
--     Fetch all constituents for the security only if the column selection type has changed from previous
       pgmloc := 6030;
       V_Open_Const := TRUE;
       IF  Col > P_Custom_Columns.FIRST
       THEN
       pgmloc := 6040;
          PCol := P_Custom_Columns.PRIOR(Col);
--        IF  P_Custom_Columns(Col).Num = P_Custom_Columns(Col-1).Num
          IF  P_Custom_Columns(Col).Num = P_Custom_Columns(PCol).Num
          THEN
       pgmloc := 6050;
             V_Open_Const := FALSE;
          END IF;
       END IF;
       pgmloc := 6060;
       IF V_Open_Const
       THEN
          pgmloc := 6070;
--     OPEN Get_Const (F_Rec.Product_Field_Name_Code
          OPEN Get_Const (P_Custom_Columns(Col).Product_Field_Name_Code
                         ,P_Const.Fri_Ticker_Sec);
-- SSI - 03SEP2009
          pgmloc := 6080;
          FETCH Get_Const BULK COLLECT INTO V_Sel_Const;
          pgmloc := 6090;
          CLOSE Get_Const;
       END IF;
-- SSI - 03SEP2009 - End of Modif.
--
       FOR N IN 1 .. V_Sel_Const.COUNT
       LOOP
--        FETCH Get_Const INTO V_Const;
--        EXIT WHEN Get_Const%NOTFOUND;
--        IF same supplier, keep same record to keep consistence
          pgmloc := 6100;
          V_Const := V_Sel_Const(N);
--
          IF V_Const.Data_Supplier_Code = P_Const.Data_Supplier_Code
          THEN
             V_Const := P_Const;
          END IF;
--
-- SSI - 03SEP2009
--        IF F_Rec.Product_Field_Name_Code='SEC_NAME'
          IF P_Custom_Columns(Col).Product_Field_Name_Code='SEC_NAME'
          THEN
-- SSI - 24AUG2009
--           P_Const.Field_12 := NVL(V_Const.Security_Name,P_Const.Security_Name);
          pgmloc := 6110;
             P_Const.Field_12 := SUBSTR(NVL(V_Const.Security_Name,P_Const.Security_Name),1,C_Name_Len);
             EXIT WHEN V_Const.Security_Name IS NOT NULL;
-- SSI - 03SEP2009
--        ELSIF F_Rec.Product_Field_Name_Code='ASSET_TYPE'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='ASSET_TYPE'
          THEN
             P_Const.Field_13 := NVL(V_Const.Asset_Type_Code,
                                     P_Const.Asset_Type_Code);
             P_Const.Field_13_Name := NVL(V_Const.Asset_Type_Description
                                         ,P_Const.Asset_Type_Description);
             EXIT WHEN V_Const.Asset_Type_Code IS NOT NULL;
--        ELSIF F_Rec.Product_Field_Name_Code='SUPP_COUNTRY'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code ='SUPP_COUNTRY'
          THEN
             V_Const.Supp_Country_Code := NVL(V_Const.Supp_Country_Code
                                             ,V_Const.ISO_Nation_Code);
--           SSI - 03JUL2009
--           Assignment of code and name should be from the same source record
--           P_Const.Supp_Country_Code := NVL(P_Const.Supp_Country_Code
--                                           ,P_Const.ISO_Nation_Code);
--           P_Const.Field_14 := NVL(V_Const.Supp_Country_Code,
--                                   P_Const.Supp_Country_Code);
--           P_Const.Field_14_Name := NVL(V_Const.Supp_Country_Name,
--                                        P_Const.Supp_Country_Name);
             IF V_Const.Supp_Country_Code IS NOT NULL
             THEN
                P_Const.Field_14 := V_Const.Supp_Country_Code;
                P_Const.Field_14_Name := V_Const.Supp_Country_Name;
             END IF;
--           SSI - 03JUL2009 - end of modif.
             EXIT WHEN V_Const.Supp_Country_Code IS NOT NULL;
-- SSI - 03SEP2009
--        ELSIF F_Rec.Product_Field_Name_Code='SUPP_CCY'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='SUPP_CCY'
          THEN
             P_Const.Field_15 := NVL(V_Const.Price_ISO_Currency,
                                     P_Const.Price_ISO_Currency);
             EXIT WHEN V_Const.Price_ISO_Currency IS NOT NULL;
-- SSI - 03SEP2009
--        ELSIF F_Rec.Product_Field_Name_Code='SECTOR'
-- SSI - 13OCT2015
--        ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='SECTOR'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='SECTOR' OR
                P_Custom_Columns(Col).Product_Field_Name_Code='SECTOR_X'
          THEN
             P_Const.Field_19 := NVL(V_Const.GICS,P_Const.GICS);
-- SSI - 29JUN2009 - wait till all records are fetched. Get Statpro GICS only if no custom GICS from any supplier
--           g_statpro_gics := NULL;
--           IF (g_statpro_gics IS NULL) THEN
--               g_statpro_gics := Get_Gics(p_Work_Date,
--                                          V_Const);
--           END IF;
--           IF g_statpro_gics IS NOT NULL
--           THEN
--               P_Const.Field_19 := g_statpro_gics;
--           END IF;
             EXIT WHEN V_Const.GICS IS NOT NULL;
-- SSI - 03SEP2009
--        ELSIF F_Rec.Product_Field_Name_Code='ICB_SUB_SEC'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='ICB_SUB_SEC'
          THEN
             P_Const.Field_23 := NVL(V_Const.Sector,P_Const.Sector);
--
             EXIT WHEN V_Const.Sector IS NOT NULL;
-- SSI - 03SEP2009
--        ELSIF F_Rec.Product_Field_Name_Code='CFLD_24'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='CFLD_24'
          THEN
             P_Const.Field_24 := NVL(V_Const.Field_24,P_Const.Field_24);
             P_Const.Field_24_Name := NVL(V_Const.Field_24_Name,
                                          P_Const.Field_24_Name);
             EXIT WHEN V_Const.Field_24 IS NOT NULL;
-- SSI - 03SEP2009
--        ELSIF F_Rec.Product_Field_Name_Code='CFLD_25'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='CFLD_25'
          THEN
             P_Const.Field_25 := NVL(V_Const.Field_25,P_Const.Field_25);
             P_Const.Field_25_Name := NVL(V_Const.Field_25_Name,
                                          P_Const.Field_25_Name);
             EXIT WHEN V_Const.Field_25 IS NOT NULL;
-- SSI - 03SEP2009
--        ELSIF F_Rec.Product_Field_Name_Code='CFLD_26'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='CFLD_26'
          THEN
             P_Const.Field_26 := NVL(V_Const.Field_26,P_Const.Field_26);
             P_Const.Field_26_Name := NVL(V_Const.Field_26_Name,
                                          P_Const.Field_26_Name);
             EXIT WHEN V_Const.Field_26 IS NOT NULL;
-- SSI - 03SEP2009
--        ELSIF F_Rec.Product_Field_Name_Code='CFLD_27'
          ELSIF P_Custom_Columns(Col).Product_Field_Name_Code='CFLD_27'
          THEN
             P_Const.Field_27 := NVL(V_Const.Field_27,P_Const.Field_27);
             P_Const.Field_27_Name := NVL(V_Const.Field_27_Name,
                                          P_Const.Field_27_Name);
             EXIT WHEN V_Const.Field_27 IS NOT NULL;
          END IF;
       END LOOP;
-- SSI - 03SEP2009
--     CLOSE Get_Const;
--
--     IF no GICS found from the supplier list, get from Statpro GICS
-- SSI - 03SEP2009
--     IF F_Rec.Product_Field_Name_Code='SECTOR' AND
       IF P_Custom_Columns(Col).Product_Field_Name_Code='SECTOR' AND
          P_Const.Field_19 IS NULL
       THEN
-- SSI - 29JUN2009
           g_statpro_gics := NULL;
           g_statpro_gics := Get_Gics(p_Work_Date,
                                      P_Const);
           IF g_statpro_gics IS NOT NULL
           THEN
              P_Const.Field_19 := g_statpro_gics;
           END IF;
       END IF;
       EXIT WHEN Col=P_Custom_Columns.LAST;
       Col := P_Custom_Columns.NEXT(Col);
    END LOOP;
<<End_Custom_Col>>
--
-- If we have no priority defined, then nullify predefined fields
-- which are Size code/name and Maturity Band code/name
--
    IF V_First_Time
    THEN
       P_Const.Field_24      := NULL;
       P_Const.Field_24_Name := NULL;
       P_Const.Field_25 := NULL;
       P_Const.Field_25_Name := NULL;
    END IF;
--
    p_Success := TRUE;
END Load_Custom_Columns;
--
--
--
--------------------------------------------------------------------------------
-- GEN_EXCH_RATE_CHAR_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Exch_Rate_Char_File(
    p_calc_date              IN  DATE,
    p_work_date              IN  DATE,
    p_fri_client             IN  clients.fri_client%TYPE,
    p_client_code            IN  clients.code%TYPE,
    p_product_code           IN  products.code%TYPE,
    p_sub_product_code       IN  sub_products.sub_product_code%TYPE,
    p_flag_both              IN  BOOLEAN,
    p_type_output            IN  VARCHAR2,
    p_success                OUT BOOLEAN
)
AS
    v_record               VARCHAR2(1000);
--
    TYPE t_rec_series_values IS RECORD(
        type_series_value  VARCHAR2(30),
        forward_number     NUMBER,
        forward_frequency  NUMBER
    );
    TYPE t_all_series_values IS TABLE OF t_rec_series_values;
    v_all_series_values t_all_series_values := t_all_series_values();
--
-- SSI - 05NOV2008
    v_format_layout             t_format_layout;
    v_tbl_index_type            t_tbl_index_type;
--
-- SSI - 17OCT2008
-- Rewrite cursor
--    CURSOR cur_exch_rate_series
--    IS
--    SELECT iex.*
--    FROM index_exchange_rates iex,
--         client_index_suppliers u
--    WHERE iex.delivery_date between p_work_date and p_work_date + 1
--    and   iex.data_supplier_code = p_sub_product_code
---- SSI - 14SEP2008
----  and iex.data_supplier_code = u.data_supplier_code
----  and  to_char(iex.delivery_date,'HH24MI') = u.delivery_time
--    and iex.data_supplier_code = u.data_supplier_code(+)
--    and  to_char(iex.delivery_date,'HH24MI') = u.delivery_time(+)
---- SSI - 14SEP2008 - end of modif
--    order by iex.currency, iex.base_currency
--     ;
--
-- ACB - 04JUL2009
--    CURSOR cur_exch_rate_series(p_index_type supp_index.index_type%TYPE)
    CURSOR cur_exch_rate_series(p_index_family supp_index.index_family%TYPE)
    IS
    SELECT iex.*
    FROM index_exchange_rates iex
    WHERE iex.delivery_date between p_work_date and p_work_date + 1
    AND   iex.data_supplier_code = g_data_supplier_code
    AND   to_char(iex.delivery_date,'HH24MI')
                IN (SELECT NVL(i.XRate_Delivery_Time,
                           TO_CHAR(iex.delivery_date,'HH24MI'))
                    FROM client_index_suppliers u
                        ,supp_index i
--                  -- ACB - 04JUL2009
--                  -- SSI - 06APR2009
--                        ,Data_Supply_Schedule D
--                  -- ACB - 04JUL2009 - End of modif.
                    WHERE u.fri_client=p_fri_client
                    AND   u.product_code = p_product_code
                    AND   u.sub_product_code=p_sub_product_code
                    AND   u.data_supplier_code =iex.Data_Supplier_code
                    AND   U.Active_Flag='Y'
--               -- ACB - 04JUL2009
                    AND   i.index_family = nvl(p_index_family, i.index_family)
--               -- SSI - 06APR2009
--                    AND   i.index_type = nvl(p_index_type, i.index_type)
--                    AND   D.Data_Supplier_Code = U.Data_Supplier_Code
--                    AND   D.Delivery_Time      = U.Delivery_Time
--                    AND   D.Holiday_Class_Code = G_Holiday_Class
--               -- SSI - 06APR2009 - End of modif.
--               -- ACB - 04JUL2009 - End of modif.
                    AND   i.data_supplier_code = u.data_supplier_code
                    AND   i.supp_index_code = u.supp_index_code
--              -- SSI - 05NOV2008
                    AND   i.delivery_time = u.delivery_time )
    ORDER BY iex.currency, iex.base_currency;
--
--
    TYPE t_index_exchange_rates IS TABLE OF cur_exch_rate_series%ROWTYPE;
    v_index_exchange_rates  t_index_exchange_rates;
    v_prev_index_exch_rates cur_exch_rate_series%ROWTYPE;
BEGIN
    pgmloc := 6120;
    -- Open the Exchange Rate Characteristics file
--  Open_File('w', -- file mode
--            p_calc_date,
--            p_fri_client,
--            p_product_code,
--            p_sub_product_code,
--            p_flag_both,
--            p_type_output,
--            'R', -- component code
--            v_output_file_exch_char_name,
--            v_output_file_exch_char,
--            p_success
--  );
--
    pgmloc := 6130;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'R', -- component code
                       'D' -- record type
                      );
--
    v_format_layout := g_format_layout;
--
    Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'R',
                           v_tbl_index_type,
                           p_success
    );
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
--
    pgmloc := 6140;
-- SSI - 05NOV2008
--  OPEN cur_exch_rate_series;
    OPEN cur_exch_rate_series( v_tbl_index_type(v_ind_type_pos) );
    pgmloc := 6150;
    FETCH cur_exch_rate_series BULK COLLECT INTO v_index_exchange_rates;
    pgmloc := 6160;
    CLOSE cur_exch_rate_series;
--
      IF v_index_exchange_rates.COUNT > 0
      THEN
        pgmloc := 6170;
        Open_File('w', -- file mode
                  p_calc_date,
                  p_fri_client,
                  p_product_code,
                  p_sub_product_code,
                  p_flag_both,
                  p_type_output,
                  'R', -- component code
                  v_output_file_exch_char_name,
                  v_output_file_exch_char,
                  p_success,
                  v_tbl_index_type(v_ind_type_pos)
                 );
-- ADD ACB 07MAY2009
           g_index_type := v_tbl_index_type(v_ind_type_pos);
        ELSE
           GOTO Next_Type;
        END IF;
    FOR v_ind IN 1..v_index_exchange_rates.COUNT
    LOOP
        IF ( v_index_exchange_rates(v_ind).currency =
                v_prev_index_exch_rates.currency AND
             v_index_exchange_rates(v_ind).base_currency =
                v_prev_index_exch_rates.base_currency  )
        THEN
            GOTO next_record;
        END IF;
        pgmloc := 6180;
        v_all_series_values.DELETE;
        v_all_series_values.EXTEND(3);
        v_all_series_values(1).type_series_value  := c_spot_series;
        v_all_series_values(1).forward_number     := NULL;
        v_all_series_values(1).forward_frequency  := NULL;
        v_all_series_values(2).type_series_value  := c_fwrd_series;
        v_all_series_values(2).forward_number     := 1;
        v_all_series_values(2).forward_frequency  := 4;
        v_all_series_values(3).type_series_value  := c_inter_fwd_series;
        v_all_series_values(3).forward_number     := 2;
        v_all_series_values(3).forward_frequency  := 4;
--
        --
        -- Write into the file, if requested
        --
        pgmloc := 6190;
        IF (p_type_output = c_type_out_file OR p_flag_both) THEN
-- SSI - 30SEP2008
--          IF (v_index_exchange_rates(v_ind).currency <> 'US$') THEN
            IF (v_index_exchange_rates(v_ind).Currency <>
                v_index_exchange_rates(v_ind).Base_currency) THEN
                pgmloc := 6200;
-- SSI - 30SEP2008
--              Get_Format_Record(v_index_exchange_rates(v_ind),
                g_format_layout := v_format_layout;
                Get_Format_Record(p_calc_date,
                                  v_index_exchange_rates(v_ind),
--                           -- SSI - 17OCT2008
--                           --   p_fri_client,
                                  G_Fmt_Fri_Client,
                                  p_product_code,
                                  'R',
                                  'D',
                                  v_record,
                                  p_success);
                pgmloc := 6210;
                Load_Supplier_Rules.Write_file(v_output_file_exch_char,
                                               v_record,
                                               Pricing_Msgs_Rec,
                                               p_success
                                              );
                IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
            END IF;
        END IF; -- IF (p_type_output = c_type_out_table OR p_flag_both)
<<next_record>>
        v_prev_index_exch_rates := v_index_exchange_rates(v_ind);
    END LOOP;
--
    -- Close the Exchange Rate Characteristics file
    pgmloc := 6220;
    Close_File(v_output_file_exch_char_name,
               v_output_file_exch_char,
               'R', -- component code
               p_work_date,
               p_flag_both,
               p_type_output,
               p_success
    );
<<Next_Type>>
       NULL;
    END LOOP;
--
    pgmloc := 6230;
    p_success := TRUE;
END Gen_Exch_Rate_Char_File;
--
--
--
--------------------------------------------------------------------------------
-- GEN_EXCHRATE_SERIES_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Exch_Rate_Series_File(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
    v_record                    VARCHAR2(1000);
    v_output_file               utl_file.file_type;
    v_output_file_name          VARCHAR2(60);
--
    v_format_layout_exchrate    t_format_layout;
    v_tbl_index_type            t_tbl_index_type;
--
    TYPE t_rec_series_values IS RECORD(
        type_series_value VARCHAR2(30),
        series_value      NUMBER
    );
    TYPE t_all_series_values IS TABLE OF t_rec_series_values;
    v_all_series_values t_all_series_values := t_all_series_values();
--
-- SSI - 17OCT2008
-- Rewrite cursor
--     CURSOR cur_exch_rate_series(p_index_type supp_index.index_type%TYPE)
--     IS
--     SELECT iex.*
--     FROM index_exchange_rates iex,
-- -- SSI - 14SEP2008
-- --       client_index_suppliers u,
-- --       supp_index s
--          client_index_suppliers u
--     -- Remove from original code
--     --, clients_univ univ
--     WHERE iex.delivery_date between p_work_date and p_work_date + 0.99999
--     and   iex.data_supplier_code = p_sub_product_code
--     and iex.data_supplier_code = u.data_supplier_code(+)
-- -- SSI - 14SEP2008
-- --  and s.data_supplier_code = u.data_supplier_code
-- --  AND s.delivery_time = u.delivery_time
-- --  and  to_char(iex.delivery_date,'HH24MI') = u.delivery_time
-- --  AND s.index_type = nvl(p_index_type, s.index_type)
-- --  AND s.supp_index_code = u.supp_index_code
--     and  to_char(iex.delivery_date,'HH24MI') = u.delivery_time(+)
-- -- SSI - 14SEP2008 - end of modif.
--     -- and univ.fri_client = p_fri_client
--     -- and iex.currency = univ.client_security_id
--     ;
--
-- ACB - 04JUL2009
--    CURSOR cur_exch_rate_series(p_index_type supp_index.index_type%TYPE)
    CURSOR cur_exch_rate_series(p_index_family supp_index.index_family%TYPE)
    IS
    SELECT iex.*
    FROM index_exchange_rates iex
    WHERE iex.delivery_date between p_work_date and p_work_date + 1
    AND   iex.data_supplier_code = g_data_supplier_code
    AND   to_char(iex.delivery_date,'HH24MI')
                IN (SELECT NVL(i.XRate_Delivery_Time,
                               TO_CHAR(iex.delivery_date,'HH24MI'))
                    FROM client_index_suppliers u
                        ,supp_index i
-- ACB - 04JUL2009
-- SSI - 06APR2009
--                      ,Data_Supply_Schedule D
-- ACB - 04JUL2009 - End of modif.
                    WHERE u.fri_client=p_fri_client
                    AND   u.product_code = p_product_code
                    AND   u.sub_product_code=p_sub_product_code
                    AND   u.data_supplier_code =iex.Data_Supplier_code
                    AND   U.Active_Flag='Y'
--               -- ACB - 04JUL2009
--               -- SSI - 06APR2009
--                  AND   D.Data_Supplier_Code = U.Data_Supplier_Code
--                  AND   D.Delivery_Time      = U.Delivery_Time
--                  AND   D.Holiday_Class_Code = G_Holiday_Class
--                    AND   i.index_type = nvl(p_index_type, i.index_type)
                    AND   i.index_family = nvl(p_index_family, i.index_family)
--               -- SSI - 06APR2009 - End of modif.
--               -- ACB - 04JUL2009 - End of modif.
                    AND   i.data_supplier_code = u.data_supplier_code
                    AND   i.supp_index_code = u.supp_index_code
                    AND   i.delivery_time = u.delivery_time );
--
    TYPE t_index_exchange_rates IS TABLE OF cur_exch_rate_series%ROWTYPE;
    v_index_exchange_rates  t_index_exchange_rates;
BEGIN
    pgmloc := 6240;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'E', -- component code
                       'D' -- record type
                      );
    v_format_layout_exchrate := g_format_layout;
--
    Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'E',
                           v_tbl_index_type,
                           p_success
    );
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
    --
        pgmloc := 6250;
        OPEN cur_exch_rate_series( v_tbl_index_type(v_ind_type_pos) );
        pgmloc := 6260;
        FETCH cur_exch_rate_series BULK COLLECT INTO v_index_exchange_rates;
        pgmloc := 6270;
        CLOSE cur_exch_rate_series;
    --
        --
        -- If the output type is a file, open it and write the header
        --
        IF v_index_exchange_rates.COUNT > 0
        THEN
          pgmloc := 6280;
          Open_File('w', -- file mode
                    p_calc_date,
                    p_fri_client,
                    p_product_code,
                    p_sub_product_code,
                    p_flag_both,
                    p_type_output,
                    'E', -- component code
                    v_output_file_name,
                    v_output_file,
                    p_success,
                    v_tbl_index_type(v_ind_type_pos)
                   );
-- ADD ACB 07MAY2009
          g_index_type := v_tbl_index_type(v_ind_type_pos);
        ELSE
           GOTO Next_Type;
        END IF;
--
        FOR v_ind IN 1..v_index_exchange_rates.COUNT
        LOOP
            pgmloc := 6290;
            --
            -- Write into the file, if requested
            --
            pgmloc := 6300;
            IF ( p_type_output = c_type_out_file OR p_flag_both ) THEN
-- ACB 30SEP2009
--                IF (v_index_exchange_rates(v_ind).currency <> 'US$') THEN
                  IF (v_index_exchange_rates(v_ind).Currency <>
                    v_index_exchange_rates(v_ind).Base_currency) THEN
-- ACB 30SEP2009
                    --IF (v_all_series_values(ind).series_value IS NOT NULL) THEN
                        pgmloc := 6310;
                        g_format_layout := v_format_layout_exchrate;
-- SSI - 30SEP2008
--                      Get_Format_Record(v_index_exchange_rates(v_ind),
                        Get_Format_Record(p_calc_date,
                                          v_index_exchange_rates(v_ind),
--                                   -- SSI - 17OCT2008
--                                   --   p_fri_client,
                                          G_Fmt_Fri_Client,
                                          p_product_code,
                                          'E',
                                          'D',
                                          v_record,
                                          p_success);
        --
                        pgmloc := 6320;
                        Load_Supplier_Rules.Write_file(v_output_file,
                                                       v_record,
                                                       Pricing_Msgs_Rec,
                                                       p_success
                                                      );
                        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
                    --END IF; --IF (v_all_series_values(ind).series_value IS NOT NULL)
                END IF;
            END IF; -- p_type_output = c_type_out_file
        END LOOP;
    --
        pgmloc := 6330;
        Close_File(v_output_file_name,
                   v_output_file,
                   'E', -- component code
                   p_work_date,
                   p_flag_both,
                   p_type_output,
                   p_success
        );
<<Next_Type>>
       NULL;
    END LOOP;
--
    pgmloc := 6340;
    COMMIT;
    p_success := TRUE;
END Gen_Exch_Rate_Series_File;
--
--
--
--------------------------------------------------------------------------------
-- GEN_SERIES_CHAR_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Series_Char_File(
    p_rec_index_series      IN  index_series%ROWTYPE,
    p_series_type           IN  INTEGER,
    p_calc_date             IN  DATE,
    p_fri_client            IN  clients.fri_client%TYPE,
    p_client_code           IN  clients.code%TYPE,
    p_product_code          IN  products.code%TYPE,
    p_flag_both             IN  BOOLEAN,
    p_type_output           IN  VARCHAR2,
    p_success               OUT BOOLEAN
)
AS
    v_record               VARCHAR2(1000);
BEGIN
    pgmloc := 6350;
    --v_series_description := Get_Series_Description(
    --                            g_data_supplier_code,
    --                            p_rec_index_series.supp_index_code,
    --                            p_series_type
    --                        );
    --pgmloc := 6360;
    --v_series_long_name := Get_Series_Long_Name(
    --                        g_data_supplier_code,
    --                        p_rec_index_series.supp_index_code,
    --                        p_series_type
    --                      );
    --
--
    --
    -- Write into the file, if requested
    --
    pgmloc := 6370;
    IF ( p_type_output = c_type_out_file OR p_flag_both ) THEN
-- SSI - 30SEP2008
--      Get_Format_Record(p_rec_index_series,
        Get_Format_Record(p_calc_date,
                          p_rec_index_series,
--                   -- SSI - 17OCT2008
--                   --   p_fri_client,
-- SSI - 24MAR2011
                          p_fri_client,
                          G_Fmt_Fri_Client,
                          p_product_code,
                          'H',
                          'D',
                          p_series_type,
                          v_record,
                          p_success);
--
        pgmloc := 6380;
        Pricing_Msgs_Rec.Remarks :=  ' *len* ' || LENGTH(V_Record);
        Load_Supplier_Rules.Write_file(v_output_file_series_char,
                                       v_record,
                                       Pricing_Msgs_Rec,
                                       p_success
                                      );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    END IF; -- p_type_output = c_type_out_file
--
    pgmloc := 6390;
    p_success := TRUE;
END Gen_Series_Char_File;
--
--
--
--------------------------------------------------------------------------------
-- GEN_MARKET_CAP_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Market_Cap_File(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
    v_record                     VARCHAR2(1000);
    v_output_file                utl_file.file_type;
    v_output_file_name           VARCHAR2(60);
    v_series_long_name           VARCHAR2(500);
    v_format_layout_mkt_cap      t_format_layout;
    v_format_layout_series_CHAR  t_format_layout;
--
    v_tbl_index_type             t_tbl_index_type;
--
--  SSI - 01OCT2008
--  Rewrite cursor to optimize selection
--
--  CURSOR cur_series_market_cap(p_index_type supp_index.index_type%TYPE)
--  IS
--  SELECT /*+ ALL_ROWS */
--        i.*
--  FROM index_series i,
--       client_index_suppliers u,
--      ,supp_index s
--  WHERE delivery_date between p_work_date and p_work_date + 1
--  and   to_char(delivery_date,'HH24MI') = u.delivery_time
--  and u.fri_client = p_fri_client
--  and u.supp_index_code = i.supp_index_code
--  AND u.data_supplier_code = g_data_supplier_code
--  AND s.delivery_time = u.delivery_time
--  AND s.data_supplier_code = u.data_supplier_code
--  AND s.supp_index_code = i.supp_index_code
--  AND s.data_supplier_code = i.data_supplier_code
--  AND s.index_type = nvl(p_index_type, S.index_type)
--  and u.active_flag = 'Y'
-- SSI - 09SEP2008
-- Add sort
--  order by i.supp_index_code;
--
--  ACB - 04JUL2009
    --CURSOR cur_series_market_cap(p_index_type supp_index.index_type%TYPE)
    CURSOR cur_series_market_cap(p_index_family supp_index.index_family%TYPE)
    IS
    SELECT /*+ ALL_ROWS */
          i.*
    FROM client_index_suppliers u
        ,supp_index s
        ,index_series i
--  ACB - 04JUL2009
-- SSI - 06APR2009
--        ,Data_Supply_Schedule D
--  ACB - 04JUL2009 - End of modif.
    WHERE u.fri_client = p_fri_client
-- SSI - 17OCT2008
    and   u.product_code = p_product_code
    and   u.sub_product_code = p_sub_product_code
-- SSI 17OCT2008 end modif.
    and u.supp_index_code = s.supp_index_code
-- SSI 17OCT2008
--  AND u.data_supplier_code = g_data_supplier_code
-- SSI 17OCT2008 end modif.
    and u.active_flag = 'Y'
--  ACB - 04JUL2009
    --AND s.index_type = nvl(p_index_type, S.index_type)
    AND s.index_family = nvl(p_index_family, S.index_family)
    --AND   S.index_family = G_index_family
--  SSI - 06APR2009
--  AND   D.Data_Supplier_Code = U.Data_Supplier_Code
--  AND   D.Delivery_Time      = U.Delivery_Time
--  AND   D.Holiday_Class_Code = G_Holiday_Class
--  SSI - 06APR2009 - End of modif.
--  ACB - 04JUL2009 - End of modif.
    AND s.delivery_time = u.delivery_time
    AND s.data_supplier_code = u.data_supplier_code
    AND i.supp_index_code = u.supp_index_code
    AND i.data_supplier_code = u.data_supplier_code
    AND i.delivery_date between p_work_date and p_work_date + 1
    and to_char(i.delivery_date,'HH24MI') = u.delivery_time
    order by i.supp_index_code;
--
    TYPE t_index_series IS TABLE OF cur_series_market_cap%ROWTYPE;
    v_index_series  t_index_series;
BEGIN
    pgmloc := 6400;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'M', -- component code
                       'D' -- record type
                      );
    v_format_layout_mkt_cap := g_format_layout;
    pgmloc := 6410;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'H', -- component code
                       'D' -- record type
                      );
    v_format_layout_series_CHAR := g_format_layout;
--
    IF (g_tbl_index_type(g_ind_type_pos) IS NULL) THEN
        pgmloc := 6420;
        Get_Name_To_Split_File(p_fri_client,
                               p_product_code,
                               p_sub_product_code,
                               'M',
                               v_tbl_index_type,
                               p_success
        );
    ELSE
        pgmloc := 6430;
        v_tbl_index_type(1) := g_tbl_index_type(g_ind_type_pos);
    END IF;
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
        --
        -- If the output type is a file, open it and write the header
        --
--      -- SSI - 03MAR2009
--      -- Temporary hardcode for JPM CASH index
        IF v_tbl_index_type(v_ind_type_pos) = 'GBI_CASH'
        THEN
           GOTO Get_Next_Type;
        END IF;
--
        pgmloc := 6440;
        Open_File('w', -- file mode
                  p_calc_date,
                  p_fri_client,
                  p_product_code,
                  p_sub_product_code,
                  p_flag_both,
                  p_type_output,
                  'M', -- component code
                  v_output_file_name,
                  v_output_file,
                  p_success,
                  v_tbl_index_type(v_ind_type_pos)
        );
    --
        pgmloc := 6450;
        OPEN cur_series_market_cap( v_tbl_index_type(v_ind_type_pos) );
        pgmloc := 6460;
        FETCH cur_series_market_cap BULK COLLECT INTO v_index_series;
        pgmloc := 6470;
        CLOSE cur_series_market_cap;
--
        pgmloc := 6480;
        FOR v_ind IN 1..v_index_series.COUNT
        LOOP
    --
            pgmloc := 6490;
            --
            -- Write into the file, if requested
            --
            pgmloc := 6500;
            IF ( p_type_output = c_type_out_file OR p_flag_both
            -- ADD ACB 23FEB2009
            AND v_index_series(v_ind).file_type <> 'FAKE_INDEX') THEN
                pgmloc := 6510;
                g_format_layout := v_format_layout_mkt_cap;
-- SSI - 30SEP2008
--              Get_Format_Record(v_index_series(v_ind),
                Get_Format_Record(p_calc_date,
                                  v_index_series(v_ind),
--                           -- SSI - 17OCT2008
--                           --   p_fri_client,
-- SSI - 24MAR2011
                                  p_fri_client,
                                  G_Fmt_Fri_Client,
                                  p_product_code,
                                  'M',
                                  'D',
                                  c_series_type_mkt_cap,
                                  v_record,
                                  p_success);
                pgmloc := 6520;
                Load_Supplier_Rules.Write_file(v_output_file,
                                               v_record,
                                               Pricing_Msgs_Rec,
                                               p_success
                                              );
                IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
            END IF;
    --
            -- Call Gen_Series_Char_File. Each line generated here, must be in
            --     the Series Characteristics file
            IF G_File_Def.EXISTS('H') THEN
               pgmloc := 6530;
               g_format_layout := v_format_layout_series_CHAR;
               Gen_Series_Char_File(v_index_series(v_ind),
                                    c_series_type_mkt_cap,
                                    p_calc_date,
--                              --  SSI - 30OCT2008
--                                  'M' || v_index_series(v_ind).supp_index_code,
--                                  v_index_series(v_ind).supp_index_name,
                                    p_fri_client,
                                    p_client_code,
                                    p_product_code,
                                    p_flag_both,
                                    p_type_output,
                                    p_success
                                   );
                pgmloc := 6540;
            END IF;
        END LOOP;
    --
        pgmloc := 6550;
        Close_File(v_output_file_name,
                   v_output_file,
                   'M', -- component code
                   p_work_date,
                   p_flag_both,
                   p_type_output,
                   p_success
        );
        --EXIT WHEN COUNT < indx;
<<Get_Next_Type>>
        NULL;
    END LOOP;
--
    pgmloc := 6560;
    COMMIT;
    p_success := TRUE;
END Gen_Market_Cap_File;
--
--
--
--------------------------------------------------------------------------------
-- GEN_SERIES_VALUE_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Series_Value_File(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
    v_record                     VARCHAR2(1000);
    v_output_file                UTL_FILE.File_Type;
    v_output_file_name           VARCHAR2(60);
    v_format_layout_series       t_format_layout;
    v_format_layout_series_CHAR  t_format_layout;
--
    v_series_Type                INTEGER;
    v_const_flag                 VARCHAR2(1);
--
    v_tbl_index_type             t_tbl_index_type;
--
--
--  SSI - 01OCT2008
--  Rewrite cursor to optimize selection
--
--  Cursor is sorted by Index code
--
--  CURSOR cur_series_value(p_index_type supp_index.index_type%TYPE)
--  IS
--  SELECT /*+ ALL_ROWS */
--        i.*
--  FROM index_series i,
--       client_index_suppliers u,
--       supp_index s
--  WHERE delivery_date between p_work_date and p_work_date + 1
--  and   TO_CHAR(delivery_date,'HH24MI') = u.delivery_time
--  and   u.fri_client = p_fri_client
--  and   u.supp_index_code = i.supp_index_code
--  AND   u.data_supplier_code = g_data_supplier_code
--  AND s.delivery_time = u.delivery_time
--  AND s.data_supplier_code = u.data_supplier_code
--  AND s.supp_index_code = i.supp_index_code
--  AND s.data_supplier_code = i.data_supplier_code
--  AND s.index_type = nvl(p_index_type, S.index_type)
--  and   u.active_flag = 'Y'
--  order by i.supp_index_code;
--
--  ACB - 04JUL2009
    --CURSOR cur_series_value(p_index_type supp_index.index_type%TYPE)
    CURSOR cur_series_value(p_index_family supp_index.index_family%TYPE)
    IS
    SELECT /*+ ALL_ROWS */
          i.*
    FROM client_index_suppliers u
        ,supp_index s
        ,index_series i
--  ACB - 04JUL2009
--        ,Data_Supply_schedule D
    WHERE u.fri_client = p_fri_client
    and u.supp_index_code = s.supp_index_code
-- SSI - 17OCT2008
--  AND u.data_supplier_code = g_data_supplier_code
    AND u.Product_code = p_product_code
    AND u.Sub_product_code = p_sub_product_code
    AND s.delivery_time = u.delivery_time
    AND s.data_supplier_code = u.data_supplier_code
--  ACB - 04JUL2009
    --AND s.index_type = nvl(p_index_type, S.index_type)
    AND s.index_family = nvl(p_index_family, S.index_family)
    --AND   S.index_family = G_index_family
--  SSI - 06APR2009
--  AND   D.Data_Supplier_Code = U.Data_Supplier_Code
--  AND   D.Delivery_Time      = U.Delivery_Time
--  AND   D.Holiday_Class_Code = G_Holiday_Class
--  SSI - 06APR2009 - End of modif.
--  ACB - 04JUL2009 - End of modif.
    AND i.supp_index_code = u.supp_index_code
    AND i.data_supplier_code = u.data_supplier_code
    AND i.delivery_date between p_work_date and p_work_date + 1
    and to_char(i.delivery_date,'HH24MI') = u.delivery_time
    and u.active_flag = 'Y'
    order by i.supp_index_code;
--
-- SSI - 19APR2011
    CURSOR Get_Const_Flag (I_Supp_Index_Code Supp_Index.Supp_index_Code%TYPE
                          ,I_Del_Time        VARCHAR2)
    IS
    SELECT Constituent_Level_Flag
    FROM   Client_Index_Suppliers
    WHERE  Data_Supplier_Code = g_data_supplier_code
    AND    Fri_Client = p_fri_Client
    AND    product_code = p_product_code
    AND    sub_product_code = p_sub_product_code
    AND    Supp_Index_Code = I_Supp_Index_Code
    AND    Delivery_Time = I_Del_Time;
-- SSI - 19APR2011 - end of modif.
--
    TYPE t_index_series IS TABLE OF cur_series_value%ROWTYPE;
    v_index_series  t_index_series;
BEGIN
    pgmloc := 6570;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'V', -- component code
                       'D' -- record type
                      );
    v_format_layout_series := g_format_layout;
    pgmloc := 6580;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'H', -- component code
                       'D' -- record type
                      );
    v_format_layout_series_CHAR := g_format_layout;
--
    pgmloc := 6590;
--  dbms_output.put_line('Processing index type: ' || g_ind_type_pos);
--  dbms_output.put_line('   Index TYPE        : ' || g_tbl_index_type(g_ind_type_pos));
    pgmloc := 6600;
    IF (g_tbl_index_type(g_ind_type_pos) IS NULL) THEN
        pgmloc := 6610;
        Get_Name_To_Split_File(p_fri_client,
                               p_product_code,
                               p_sub_product_code,
                               'V',
                               v_tbl_index_type,
                               p_success
        );
    ELSE
        pgmloc := 6620;
        v_tbl_index_type(1) := g_tbl_index_type(g_ind_type_pos);
    END IF;
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
        --
        -- If the output type is a file, open it and write the header
        --
        pgmloc := 6630;
        Open_File('w', -- file mode
                  p_calc_date,
                  p_fri_client,
                  p_product_code,
                  p_sub_product_code,
                  p_flag_both,
                  p_type_output,
                  'V', -- component code
                  v_output_file_name,
                  v_output_file,
                  p_success,
                  v_tbl_index_type(v_ind_type_pos)
        );
    --
        pgmloc := 6640;
        OPEN cur_series_value( v_tbl_index_type(v_ind_type_pos) );
        pgmloc := 6650;
        FETCH cur_series_value BULK COLLECT INTO v_index_series;
        pgmloc := 6660;
        CLOSE cur_series_value;
        pgmloc := 6670;
        FOR v_ind IN 1..v_index_series.COUNT
        LOOP
    --
            pgmloc := 6680;
            --v_series_long_name := Get_Series_Long_Name(g_data_supplier_code,
            --                                           v_index_series(v_ind).supp_index_code,
            --                                           c_series_type_series
            --                                          );
            --
            -- Write into the file, if requested
            --
            IF (g_flag_is_holiday) THEN
               pgmloc := 6690;
               v_index_series(v_ind).simulated_benchmark_return := 0;
               v_index_series(v_ind).true_benchmark_return      := 0;
            END IF;
            pgmloc := 6700;
            IF ( p_type_output = c_type_out_file OR p_flag_both
            -- ADD ACB 23FEB2009
            AND v_index_series(v_ind).file_type <> 'FAKE_INDEX') THEN
                 pgmloc := 6710;
                 g_format_layout := v_format_layout_series;
-- SSI - 30SEP2008
--               Get_Format_Record(v_index_series(v_ind),
                 Get_Format_Record(p_calc_date,
                                   v_index_series(v_ind),
--                            -- SSI - 17OCT2008
--                            --   p_fri_client,
-- SSI - 24MAR2011
                                   p_fri_client,
                                   G_Fmt_Fri_Client,
                                   p_product_code,
                                   'V',
                                   'D',
                                   c_series_type_series,
                                   v_record,
                                   p_success);
                pgmloc := 6720;
                Load_Supplier_Rules.Write_file(v_output_file,
                                               v_record,
                                               Pricing_Msgs_Rec,
                                               p_success
                                              );
                IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    --
            END IF; -- p_type_output = c_type_out_file
    --
            -- Call Gen_Series_Char_File. Each line generated here, must be in
            --     the Series Characteristics file
            IF G_File_Def.EXISTS('H') THEN
              pgmloc := 6730;
-- SSI - 19APR2011
              OPEN Get_Const_Flag (v_index_series(v_ind).Supp_Index_Code
                                  ,TO_CHAR(v_index_series(v_ind).Delivery_Date,'HH24MI'));
              FETCH Get_Const_Flag INTO v_Const_Flag;
              CLOSE Get_Const_Flag;
   --
              IF v_Const_Flag='Y'
              THEN
                 v_series_type := c_series_type_Const;
              ELSE
                 v_series_type := c_series_type_series;
              END IF;
-- SSI - 19APR2011 - end of modif
              g_format_layout := v_format_layout_series_CHAR;
              Gen_Series_Char_File(v_index_series(v_ind),
--                            -- SSI - 19APR2011
--                            --   c_series_type_series,
                                   v_series_type,
                                   p_calc_date,
-- SSI - 30OCT2008
--                                 v_index_series(v_ind).supp_index_code,
--                                 v_index_series(v_ind).supp_index_name,
                                   p_fri_client,
                                   p_client_code,
                                   p_product_code,
                                   p_flag_both,
                                   p_type_output,
                                   p_success
                                  );
              pgmloc := 6740;
            END IF;
        END LOOP;
    --
        pgmloc := 6750;
        Close_File(v_output_file_name,
                   v_output_file,
                   'V', -- component code
                   p_work_date,
                   p_flag_both,
                   p_type_output,
                   p_success
        );
    END LOOP;
--
    pgmloc := 6760;
    COMMIT;
    p_success := TRUE;
END Gen_Series_Value_File;
--
--
--
--------------------------------------------------------------------------------
-- GEN_SECURITIES_CHAR_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Securities_Char_File(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
-- SSI - 01OCT2009
    p_last_run_Date     IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
    v_supp_index_code            supp_index.supp_index_code%TYPE;
    v_sector_description         category_classes.text_e%TYPE;
    v_industry_group_description category_classes.text_e%TYPE;
    v_industry_description       category_classes.text_e%TYPE;
    v_sub_industry_description   category_classes.text_e%TYPE;
    v_country_desc               nations.text_e%TYPE;
    v_format_layout              t_format_layout;
--
    v_file_handle_secur_char        utl_file.file_type;
    v_output_file_secur_char_name   VARCHAR2(100);
--  v_record                        VARCHAR2(1000);
    v_record                        VARCHAR2(3000);
    v_prev_id                       VARCHAR2(60);
--
    v_tbl_index_type             t_tbl_index_type;
--
-- SSI - 03-SEP2009
    V_ColType                    t_Out_Columns;
    V_Out_Columns                t_Out_Columns;
--
    v_Columns                    t_Custom_Columns;
    nType                        INTEGER;
    nFld                         INTEGER;
    nFld2                        INTEGER;
    Ind                          INTEGER;
    V_Match                      BOOLEAN;
--
-- SSI - 01OCT2009
   TYPE t_IDs IS TABLE OF INTEGER INDEX BY Supp_Index_Constituents.Supp_Sec_ID%TYPE;
   V_Adds                        t_IDs;
-- SSI - 03-SEP2009 - end of modif.
/*
    CURSOR cur_get_index
    IS
    SELECT I.supp_index_code,
           replace(I.text_e, g_field_delimiter, ' ')
    FROM supp_index I,
         client_index_suppliers C
    WHERE C.fri_client = p_fri_client
    AND I.supp_index_code = C.supp_index_code
    AND I.data_supplier_code = g_data_supplier_code
    AND C.constituent_level_flag = 'Y'
    AND C.active_flag = 'Y';
*/
--
-- 15SEP2008
-- Rewrite cursor
--  CURSOR cur_securities_char
--  IS
--  SELECT S.*
--  FROM supp_index_constituents S,
--       supp_index I,
--       client_index_suppliers C
--  WHERE S.delivery_date between p_work_date and p_work_date + 0.99999
--  AND   C.fri_client = p_fri_client
--  AND   I.supp_index_code = C.supp_index_code
--  AND   I.delivery_time   = C.delivery_time
--  AND   C.constituent_level_flag = 'Y'
--  AND   C.active_flag = 'Y'
--  AND   S.supp_index_code = C.supp_index_code
--  AND   S.data_supplier_code = I.data_supplier_code
--  AND   S.data_supplier_code = g_data_supplier_code
--  AND   nvl(S.weight, 0) <> 0
--  ORDER BY supp_sec_id
--  ;
--
--  CURSOR cur_securities_char
--  IS
--  SELECT /*+ ALL_ROWS */
--         S.*
--  FROM supp_index_constituents S,
--       client_index_suppliers C
--  WHERE C.fri_client = p_fri_client
--  AND   C.constituent_level_flag = 'Y'
--  AND   C.active_flag = 'Y'
--  AND   S.data_supplier_code = C.data_supplier_code
--  AND   S.supp_index_code = C.supp_index_code
--  AND   S.delivery_date between p_work_date and p_work_date + 0.99999
--  AND   C.Delivery_Time = TO_CHAR(S.Delivery_date,'HH24MI')
--  AND   nvl(S.weight, 0) <> 0
--  ORDER BY supp_sec_id
--  ;
--
-- SSI - 01OCT2008
-- Rewrite cursor to optimize selection time
--  CURSOR cur_securities_char(p_index_type supp_index.index_type%TYPE)
--  IS
--  SELECT /*+ ALL_ROWS */
--         S.*
--  FROM supp_index_constituents S,
--       client_index_suppliers C
--      ,supp_index i
--  WHERE C.fri_client = p_fri_client
--  AND   C.constituent_level_flag = 'Y'
--  AND   C.active_flag = 'Y'
--  AND   S.data_supplier_code = C.data_supplier_code
--  AND   S.supp_index_code = C.supp_index_code
--  AND   S.delivery_date between p_work_date and p_work_date + 0.99999
--  AND   C.Delivery_Time = TO_CHAR(S.Delivery_date,'HH24MI')
--  AND   nvl(S.weight, 0) <> 0
--  AND   i.delivery_time = C.delivery_time
--  AND   i.data_supplier_code = C.data_supplier_code
--  AND   i.supp_index_code = s.supp_index_code
--  AND   i.data_supplier_code = s.data_supplier_code
--  AND   i.index_type = nvl(p_index_type, i.index_type)
--  ORDER BY supp_sec_id
--  ;
--  ACB - 04JUL2009
--    CURSOR cur_securities_char(p_index_type supp_index.index_type%TYPE)
    CURSOR cur_securities_char(p_index_family supp_index.index_family%TYPE)
    IS
    SELECT /*+ ALL_ROWS */
           S.*
    FROM client_index_suppliers C
        ,supp_index i
        ,supp_index_constituents S
--  ACB - 04JUL2009
-- SSI - 06APR2009
--        ,Data_Supply_Schedule D
-- SSI - 06APR2009 - End of modif.
--  ACB - 04JUL2009 - End of modif.
    WHERE C.fri_client = p_fri_client
    AND   C.Product_Code = p_product_Code
    AND   C.Sub_Product_Code = p_sub_product_Code
    AND   C.constituent_level_flag = 'Y'
    AND   C.active_flag = 'Y'
    AND   i.data_supplier_code = C.data_supplier_code
    AND   i.supp_index_code = C.supp_index_code
    AND   i.delivery_time = C.delivery_time
--  ACB - 04JUL2009
    AND   i.index_family = nvl(p_index_family, i.index_family)
--  SSI - 06APR2009
--  AND   i.index_type = nvl(p_index_type, i.index_type)
--  AND   D.Data_Supplier_Code = U.Data_Supplier_Code
--  AND   D.Delivery_Time      = U.Delivery_Time
--  AND   D.Holiday_Class_Code = G_Holiday_Class
--  SSI - 06APR2009 - End of modif.
--  ACB - 04JUL2009 - End of modif.
    AND   S.data_supplier_code = C.data_supplier_code
    AND   S.supp_index_code = C.supp_index_code
    AND   S.delivery_date between p_work_date and p_work_date + 0.99999
    AND   C.Delivery_Time = TO_CHAR(S.Delivery_date,'HH24MI')
    AND   nvl(S.weight, 0) <> 0
    ORDER BY supp_sec_id
    ;
--
-- SSI - 03SEP2009
    CURSOR Get_Field_Name IS
    SELECT DISTINCT Product_Field_Name_Code
    FROM  Ind_Client_Supp_Fields
    WHERE  Fri_Client = P_Fri_Client
    AND    Product_Code = P_Product_Code
    AND    Sub_Product_Code = P_Sub_Product_Code
    AND    Active_Flag = 'Y';
--
-- SSI - 01OCT2009
-- New cursor to find out the newly added securities
    CURSOR cur_securities_Add(p_index_family supp_index.index_family%TYPE
                             ,p_last_run_date DATE)
    IS
    SELECT /*+ INDEX(S SUPP_INDEX_CONSTITUENTS_X_01) */
           supp_Sec_ID
    FROM client_index_suppliers C
        ,supp_index i
        ,supp_index_constituents S
    WHERE C.fri_client = p_fri_client
    AND   C.Product_Code = p_product_Code
    AND   C.Sub_Product_Code = p_sub_product_Code
    AND   C.constituent_level_flag = 'Y'
    AND   C.active_flag = 'Y'
    AND   i.data_supplier_code = C.data_supplier_code
    AND   i.supp_index_code = C.supp_index_code
    AND   i.delivery_time = C.delivery_time
    AND   i.index_family = nvl(p_index_family, i.index_family)
    AND   S.data_supplier_code = C.data_supplier_code
    AND   S.delivery_date between p_work_date and p_work_date + 1
    AND   S.supp_index_code = C.supp_index_code
    AND   nvl(S.weight, 0) <> 0
    AND   C.Delivery_Time = TO_CHAR(S.Delivery_date,'HH24MI')
    MINUS
    SELECT /*+ INDEX(S SUPP_INDEX_CONSTITUENTS_X_01) */
           NVL(T.New_Ticker,S.Supp_Sec_ID)
    FROM client_index_suppliers C
        ,supp_index i
        ,supp_index_constituents S
        ,Supp_Ticker_Changes T
    WHERE C.fri_client = p_fri_client
    AND   C.Product_Code = p_product_Code
    AND   C.Sub_Product_Code = p_sub_product_Code
    AND   C.constituent_level_flag = 'Y'
    AND   C.active_flag = 'Y'
    AND   i.data_supplier_code = C.data_supplier_code
    AND   i.supp_index_code = C.supp_index_code
    AND   i.delivery_time = C.delivery_time
    AND   i.index_family = nvl(p_index_family, i.index_family)
    AND   S.data_supplier_code = C.data_supplier_code
    AND   S.delivery_date between p_Last_Run_Date and p_Last_Run_Date+1
    AND   S.supp_index_code = C.supp_index_code
    AND   nvl(S.weight, 0) <> 0
    AND   C.Delivery_Time = TO_CHAR(S.Delivery_date,'HH24MI')
--  For possible ticker changes
    AND   T.Data_Supplier_Code(+) = S.Data_Supplier_Code
    AND   T.New_Ticker(+) = S.Supp_Sec_ID
    AND   T.Effective_Date(+) = TRUNC(S.Delivery_Date)
    AND   T.App_Flag(+) = 'Y';
--
    CURSOR Get_Diff_Supp (I_Field   Product_Field_Names.Code%TYPE
                         ,I_Field_2 Product_Field_Names.Code%TYPE)
    IS
    SELECT Data_Supplier_Code,Index_Type,Rownum
    FROM
    (SELECT Data_Supplier_Code,Index_Type,Priority_Order
     FROM  Ind_Client_Supp_Fields
     WHERE  Fri_Client = P_Fri_Client
     AND    Product_Code = P_Product_Code
     AND    Sub_Product_Code = P_Sub_Product_Code
     AND    Product_Field_Name_Code = I_Field
     AND    Active_Flag = 'Y'
     order by priority_order)
    MINUS
    SELECT Data_Supplier_Code,Index_Type,Rownum
    FROM
    (SELECT Data_Supplier_Code,Index_Type,Priority_Order
     FROM  Ind_Client_Supp_Fields
     WHERE  Fri_Client = P_Fri_Client
     AND    Product_Code = P_Product_Code
     AND    Sub_Product_Code = P_Sub_Product_Code
     AND    Product_Field_Name_Code = I_Field_2
     AND    Active_Flag = 'Y'
     order by priority_order)
;
    V_Diff_Supp                  Get_Diff_Supp%ROWTYPE;
-- SSI - 03SEP2009
--
    v_prev_reg         cur_securities_char%ROWTYPE;
BEGIN
    pgmloc := 6770;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'C', -- component code
                       'D' -- record type
                      );
    v_format_layout := g_format_layout;
-- SSI - 14SEP2008
-- - ORA-04030: out of process memory
--
--  pgmloc := 6780;
--  OPEN cur_securities_char;
--  pgmloc := 6790;
--  FETCH cur_securities_char BULK COLLECT INTO v_supp_index_constituents;
--  pgmloc := 6800;
--  CLOSE cur_securities_char;
--
--  FOR v_ind IN 1..v_supp_index_constituents.COUNT
     Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'C',
                           v_tbl_index_type,
                           p_success
                           );
--
--  SSI - 03SEP2009
--  Define custom columns to be loaded
--
    pgmloc := 6810;
    nType := 0;
    OPEN Get_Field_Name;
    pgmloc := 6820;
    FETCH Get_Field_Name BULK COLLECT INTO V_Columns;
    pgmloc := 6830;
    CLOSE Get_Field_Name;
--
--  For each column field, compare the supplier priority list with the previous column fields
--  Then group them by type number in V_Out_Columns
    FOR nFld IN 1 .. V_Columns.COUNT
    LOOP
       pgmloc := 6840;
       V_MATCH := FALSE;
       IF nFld > 1
       THEN
          FOR nFld2 IN 1 .. V_ColType.COUNT
          LOOP
             pgmloc := 6850;
             OPEN Get_Diff_Supp(V_Columns(nFld),
                                V_Columns(nFld2));
             pgmloc := 6860;
             FETCH Get_Diff_Supp INTO V_Diff_Supp;
--           Exit whenever we found a match - No diff found
             IF Get_Diff_Supp%NOTFOUND
             THEN
                pgmloc := 6870;
                V_Match := TRUE;
                nType := nFld2;    -- This the the m
                CLOSE Get_Diff_Supp;
                EXIT;
             END IF;
             CLOSE Get_Diff_Supp;
             pgmloc := 6880;
          END LOOP;
       END IF;
       pgmloc := 6890;
       IF V_MATCH
       THEN
--     -- Inc count for number of column found within the same group type
          V_ColType(nType).Num := V_ColType(nType).Num + 1;
       ELSE
--     1st Field or No match found, create a new type
          nType := V_ColType.COUNT + 1;
          V_ColType(nType).Product_Field_Name_Code := V_Columns(nFld);
          V_ColType(nType).Num := 1;
       END IF;
       pgmloc := 6900;
--     Assign index # to order column
       Ind := (nType-1) * V_Columns.COUNT + V_ColType(nType).Num + 1;
       V_Out_Columns(ind).Product_Field_Name_Code := V_Columns(nFld);
       V_Out_Columns(ind).Num                := nType;
    END LOOP;
-- SSI - 03SEP2009 - End of modif.
--
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
        pgmloc := 6910;
        -- Open the Securities Characteristics file
        Open_File('w', -- file mode
                  p_calc_date,
                  p_fri_client,
                  p_product_code,
                  p_sub_product_code,
                  p_flag_both,
                  p_type_output,
                  'C', -- component code
                  v_output_file_secur_char_name,
                  v_file_handle_secur_char,
                  p_success,
                  v_tbl_index_type(v_ind_type_pos)
        );
-- SSI - 01OCT2009
--
--      For t+1 services, Load newly added securities
--
        IF NOT G_T_Service
        THEN
           FOR Add_Sec IN cur_securities_Add(v_tbl_index_type(v_ind_type_pos)
                                            ,p_last_run_date)
           LOOP
              V_Adds(Add_Sec.Supp_Sec_ID) := 1;
           END LOOP;
        END IF;
-- SSI - 01OCT2009 - End of modif.
--
        FOR v_supp_index_constituents IN cur_securities_char(v_tbl_index_type(v_ind_type_pos))
        LOOP
    -- SSI - 14SEP2008
    --      IF (v_supp_index_constituents(v_ind).supp_sec_id = v_prev_reg.supp_sec_id)
            IF (v_supp_index_constituents.supp_sec_id = v_prev_reg.supp_sec_id)
            THEN
                GOTO next_sec;
            END IF;
--
--      SSI - 25MAY2009
--      Get client selected columns if defined
-- SSI - 01OCT2009
            Load_Custom_Columns(p_work_date
                               ,p_fri_client
                               ,p_product_code
                               ,p_sub_product_code
--                        -- SSI - 03SEP2009 - New paramater
                               ,v_Out_Columns
                               ,v_supp_index_constituents
                               ,p_success);
--
            -- Get country description
            pgmloc := 6920;
--          dbms_output.put_line('Sec record loop:' || v_supp_index_constituents.supp_sec_id);
-- SSI - 25MAY2009
-- Useless code
--          BEGIN
--              SELECT text_e
--              INTO v_country_desc
--              FROM nations
--  -- SSI - 14SEP2008
--  --          WHERE iso_nation = v_supp_index_constituents(v_ind).iso_nation_code;
--              WHERE iso_nation = v_supp_index_constituents.iso_nation_code;
--          EXCEPTION
--              WHEN NO_DATA_FOUND THEN
--  -- SSI - 14SEP2008
--  --              v_country_desc := v_supp_index_constituents(v_ind).iso_nation_code;
--                  v_country_desc := v_supp_index_constituents.iso_nation_code;
--          END;
            --
            -- Write into the file, if requested
            --
            pgmloc := 6930;
            g_statpro_gics := NULL;
            IF (p_type_output = c_type_out_file OR p_flag_both) THEN
                pgmloc := 6940;
--
-- SSI - 01OCT2009
--
-- Alter Price date for Characteristics output as effective date
                IF NOT G_T_Service AND
                   NOT V_Adds.EXISTS(v_supp_index_constituents.supp_sec_id)
                THEN
                   v_supp_index_constituents.Price_Date := p_Calc_Date;
                END IF;
-- SSI - 01OCT2009 - End of modif.
--
                g_format_layout := v_format_layout;
                Get_Format_Record(p_calc_date,
    -- SSI - 14SEP2008
    --                        --  v_supp_index_constituents(v_ind),
                                  v_supp_index_constituents,
--                           -- SSI - 17OCT2008
--                           --   p_fri_client,
-- SSI - 24MAR2011
                                  p_fri_client,
                                  G_Fmt_Fri_Client,
                                  p_product_code,
                                  'C', -- component code
                                  'D', -- record type,
                                  NULL, -- series type
                                  v_record,
                                  p_success);
               pgmloc := 6950;
    --
--          dbms_output.put_line('Get fmt and write:' || v_supp_index_constituents.supp_sec_id);
--          dbms_output.put_line(SUBSTR(v_record,1,20));
               pgmloc := 6960;
               Load_Supplier_Rules.Write_file(v_file_handle_secur_char,
                                              v_record,
                                              Pricing_Msgs_Rec,
                                              p_success
                                             );
               IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
            END IF; -- IF (p_type_output = c_type_out_table OR p_flag_both)
    <<next_sec>>
    -- SSI - 14SEP2008
    --      v_prev_reg := v_supp_index_constituents(v_ind);
            v_prev_reg := v_supp_index_constituents;
        END LOOP;
        /*END LOOP; -- OPEN cur_get_index*/
    --
        /*pgmloc := 6970;
        CLOSE cur_get_index;*/
    --
        pgmloc := 6980;
        Close_File(v_output_file_secur_char_name,
                  v_file_handle_secur_char,
                  'C', -- component code
                  p_work_date,
                  p_flag_both,
                  p_type_output,
                  p_success
       );
    END LOOP;
--
    pgmloc := 6990;
    p_success := TRUE;
END Gen_Securities_Char_File;
--
--
--
--------------------------------------------------------------------------------
-- GEN_Securities_Char_File_2
--------------------------------------------------------------------------------
PROCEDURE Gen_Securities_Char_File_2(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_file_type         IN  VARCHAR2,
    p_del_Time          IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
    v_file_handle_secur_char        utl_file.file_type;
    v_output_file_secur_char_name   VARCHAR2(100);
    v_record                        VARCHAR2(1000);
    v_prev_id                       VARCHAR2(60);
    v_format_layout_sec             t_format_layout;
    v_tbl_index_type                t_tbl_index_type;
--
    CURSOR cur_Sel_Securities
    IS
    SELECT I.*
    FROM index_securities I
    WHERE I.data_supplier_code = g_data_supplier_code
    AND   I.delivery_date between p_work_date and p_work_date + 0.99999
    AND   I.file_type = NVL(p_file_type,I.File_Type)
    AND   TO_CHAR(I.Delivery_Date,'HH24MI')=NVL(p_del_time,TO_CHAR(I.Delivery_Date,'HH24MI'))
    ORDER BY Security_ID;
--
    v_index_securities cur_Sel_securities%ROWTYPE;
--
    V_num1    INTEGER:=0;
BEGIN
    pgmloc := 7000;
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'U', -- component code
                       'D' -- record type
                      );
    v_format_layout_sec := g_format_layout;
--
    pgmloc := 7010;
    Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'U',   -- component code
                           v_tbl_index_type,
                           p_success
    );
--
--  Add loop to loop through each index type required
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
--
       Open_File('w', -- file mode
                 p_calc_date,
                 p_fri_client,
                 p_product_code,
                 p_sub_product_code,
                 p_flag_both,
                 p_type_output,
                 'U', -- component code
                 v_output_file_secur_char_name,
                 v_file_handle_secur_char,
                 p_success
                ,v_tbl_index_type(v_ind_type_pos)
       );
--
       pgmloc := 7020;
       v_Prev_Id := NULL;
       FOR v_index_securities IN cur_sel_securities
       LOOP  --{ Index_Type
         IF V_Prev_ID IS NULL OR
            V_Prev_ID <> v_index_securities.Security_id
         THEN
           V_Num1 := V_Num1 + 1;
--
           pgmloc := 7030;
--         IF (p_type_output = c_type_out_file OR p_flag_both) THEN
           IF (p_type_output = c_type_out_file OR p_flag_both) THEN
               pgmloc := 7040;
               g_format_layout := v_format_layout_sec;
               Get_Format_Record(p_calc_date,
                                 v_index_securities,
                                 G_Fmt_Fri_Client,
                                 p_product_code,
                                 'U', -- component code
                                 'D', -- record type,
                                 NULL, -- series type
                                 v_record,
                                 p_success);
              pgmloc := 7050;
              Load_Supplier_Rules.Write_file(v_file_handle_secur_char,
                                             v_record,
                                             Pricing_Msgs_Rec,
                                             p_success
                                            );
              IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
              V_Prev_Id := v_index_securities.security_id;
<<skip_rec>>
              NULL;
           END IF; -- IF (p_type_output = c_type_out_table OR p_flag_both)
           pgmloc := 7060;
         END IF; --} if new ID
       END LOOP;
--
       pgmloc := 7070;
       Close_File(v_output_file_secur_char_name,
                 v_file_handle_secur_char,
                 'U', -- component code
                 p_work_date,
                 p_flag_both,
                 p_type_output,
                 p_success
      );
      dbms_output.put_line ('Type ''U'' - Sec # ' || V_Num1);
      V_Num1 := 0;
    END LOOP; -- }Index_Type
--
    pgmloc := 7080;
    p_success := TRUE;
END Gen_Securities_Char_File_2;
--
--
--
--------------------------------------------------------------------------------
-- GEN_SECURITIES_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Securities_File(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
-- SSI - 01OCT2008
    p_holiday           IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
    v_file_handle          UTL_FILE.file_type;
    v_output_file_name     VARCHAR2(60);
    v_record               VARCHAR2(1000);
--
    v_supp_index_code       supp_index.supp_index_code%TYPE;
    v_text_e                supp_index.text_e%TYPE;
    v_holiday               BOOLEAN;
    v_format_layout_sec     t_format_layout;
-- SSI - 15SEP2008
    v_del_date              DATE;
-- SSI - 01OCT2008
    v_calc_date             DATE;
--
    v_tbl_index_type             t_tbl_index_type;
--
--  ACB - 04JUL2009
--    CURSOR cur_get_index(p_index_type supp_index.index_type%TYPE)
    CURSOR cur_get_index(p_index_family supp_index.index_family%TYPE)
    IS
--  SELECT I.supp_index_code
    SELECT C.*
    FROM supp_index I,
         client_index_suppliers C
--  ACB - 04JUL2009
-- SSI - 06APR2009
--      ,Data_Supply_Schedule D
-- SSI - 06APR2009 - End of modif.
--  ACB - 04JUL2009 - End of modif.
    WHERE C.fri_client = p_fri_client
--  SSI - 17OCT2008
    AND   C.Product_Code = p_product_Code
    AND   C.Sub_Product_Code = p_sub_product_Code
    AND   C.Data_Supplier_Code = I.Data_Supplier_Code
    AND I.supp_index_code = C.supp_index_code
    AND I.delivery_time = C.delivery_time
--  ACB - 04JUL2009
    AND i.index_family = nvl(p_index_family, i.index_family)
--  SSI - 06APR2009
--  AND i.index_type = nvl(p_index_type, i.index_type)
--  AND   D.Data_Supplier_Code = U.Data_Supplier_Code
--  AND   D.Delivery_Time      = U.Delivery_Time
--  AND   D.Holiday_Class_Code = G_Holiday_Class
--  SSI - 06APR2009 - End of modif.
--  ACB - 04JUL2009 - End of modif.
-- SSI - 17OCT2008
--  AND I.data_supplier_code = g_data_supplier_code
    AND C.constituent_level_flag = 'Y'
    AND C.active_flag = 'Y';
--
    CURSOR cur_securities(p_supp_index_code    supp_index.supp_index_code%TYPE,
                          p_data_supplier_code supp_index.data_supplier_code%TYPE,
-- SSI - 15SEP2008
--                        p_del_time           VARCHAR2)
                          p_del_date           DATE)
    IS
    SELECT *
    FROM supp_index_constituents
-- SSI - 15SEP2008
--  WHERE delivery_date between p_work_date and p_work_date + 0.99999
--  AND   TO_CHAR(delivery_date,'HH24MI') = p_del_time
    WHERE delivery_date = p_del_date
    AND supp_index_code = p_supp_index_code
-- SSI - 30OCT2008
    AND price_date <> TRUNC(p_del_date)
    AND data_supplier_code = p_data_supplier_code;
--
--  TYPE t_rec_index IS RECORD(
--      supp_index_code    supp_index.supp_index_code%TYPE
--  );
    TYPE t_tbl_index IS TABLE OF client_index_suppliers%ROWTYPE;
    v_tbl_index t_tbl_index;
BEGIN
    pgmloc := 7090;
--  v_holiday := FALSE;
--  IF ( to_char(p_calc_date, 'D') IN (1, 7)
--      OR Holiday.Is_It_Open(p_calc_date,
--                            'SUPPLIER',
--                            g_data_supplier_code,
--                            g_delivery_time) IS NOT NULL
--     )
--  THEN
--     v_holiday := TRUE;
--  END IF;
--
    pgmloc := 7100;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'B', -- component code
                       'D' -- record type
                      );
    v_format_layout_sec := g_format_layout;
--
    Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'B',
                           v_tbl_index_type,
                           p_success
    );
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
        --
        -- If the output type is a file, open it and write the header
        --
        pgmloc := 7110;
        Open_File('w', -- file mode
                 p_calc_date,
                 p_fri_client,
                 p_product_code,
                 p_sub_product_code,
                 p_flag_both,
                 p_type_output,
                 'B', -- component code
                 v_output_file_name,
                 v_file_handle,
                 p_success,
                 v_tbl_index_type(v_ind_type_pos)
        );
    --
        IF G_T_Service THEN
           V_Calc_Date := P_Calc_Date;
        ELSE
           V_Calc_Date := P_Calc_Date-1;
        END IF;
--
        pgmloc := 7120;
        OPEN cur_get_index( v_tbl_index_type(v_ind_type_pos) );
        FETCH cur_get_index BULK COLLECT INTO v_tbl_index;
        CLOSE cur_get_index;
        FOR v_ind_index IN 1..v_tbl_index.COUNT -- {for each index
        LOOP
            pgmloc := 7130;
            --FETCH cur_get_index INTO v_supp_index_code,
            --                         v_text_e;
            --EXIT WHEN cur_get_index%NOTFOUND;
    --
            V_Del_Date := TO_DATE( TO_CHAR(p_work_Date,'DDMMYYYY') ||
                                     v_tbl_index(v_ind_index).delivery_time
                                  ,'DDMMYYYYHH24MI');
--          pgmloc := 7140;
--          OPEN cur_securities(v_tbl_index(v_ind_index).supp_index_code,
--                              g_data_supplier_code,
-- SSI - 15SEP2008
--                         --   v_tbl_index(v_ind_index).delivery_time);
--                              v_del_date);
--          pgmloc := 7150;
--          FETCH cur_securities BULK COLLECT INTO v_supp_index_constituents;
--          pgmloc := 7160;
--          CLOSE cur_securities;
    --
--          FOR v_ind IN 1..v_supp_index_constituents.COUNT
            FOR v_supp_index_constituents IN cur_securities
                                   (v_tbl_index(v_ind_index).supp_index_code
--                              -- SSI - 17OCT2008
--                              -- ,g_data_supplier_code
                                   ,v_tbl_index(v_ind_index).Data_Supplier_Code
                                   ,v_del_date)
            LOOP
-- SSI - 01OCT2008
--              IF (v_holiday) THEN
--
-- SSI - 05NOV2010
              IF (p_holiday AND NVL(v_supp_index_constituents.weight,0)=0)
              THEN  --{
                 NULL;   -- Skip if holiday and no weight, ie we do not CFW deleted securities
              ELSE  --}{
-- SSI - 05NOV2010 - end of modif.
                IF (p_holiday) THEN
                    pgmloc := 7170;
--                  v_supp_index_constituents(v_ind).security_return   := 0;
--                  v_supp_index_constituents(v_ind).security_return_2 := 0;
--                  v_supp_index_constituents(v_ind).security_return_3 := 0;
                    v_supp_index_constituents.security_return   := 0;
                    v_supp_index_constituents.security_return_2 := 0;
                    v_supp_index_constituents.security_return_3 := 0;
--              --  SSI - 03MAR2009
                    v_supp_index_constituents.security_return_4 := 0;
                    v_supp_index_constituents.security_return_5 := 0;
--              --  SSI - 22NOV2012
--              --  MSCI hedged index has 6 returns, where calc_field_07 is net return in CAD
                    IF v_tbl_index(v_ind_index).Data_Supplier_Code='MSCT'
                    THEN
                        v_supp_index_constituents.calc_field_07 := 0;
                    END IF;
--              -- SSI - 22NOV2012 - end of modif
                END IF;
                --pgmloc := 7180;
                --v_series_long_name := Get_Series_Long_Name(
                --                        g_data_supplier_code,
                --                        v_supp_index_constituents(v_ind).supp_index_code,
                --                        null
                --                       );
                --
                -- Write into the file, if requested
                --
                pgmloc := 7190;
                IF ( p_type_output = c_type_out_file OR p_flag_both ) THEN
                    pgmloc := 7200;
                    g_format_layout := v_format_layout_sec;
--              --  SSI - 01OCT2008
--                  Get_Format_Record(p_calc_date,
                    Get_Format_Record(v_calc_date,
                                -- 15SEP2008
                                --    v_supp_index_constituents(v_ind),
                                      v_supp_index_constituents,
--                               -- SSI - 17OCT2008
--                               --   p_fri_client,
-- SSI - 24MAR2011
                                      p_fri_client,
                                      G_Fmt_Fri_Client,
                                      p_product_code,
                                      'B',
                                      'D',
                                      NULL, -- series type
                                      v_record,
                                      p_success);
--
                    pgmloc := 7210;
                    Load_Supplier_Rules.Write_file(v_file_handle,
                                                   v_record,
                                                   Pricing_Msgs_Rec,
                                                   p_success
                                                  );
                   IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
               END IF;
              END IF; -- }
           END LOOP;
        END LOOP;  --} For each index
    --
        pgmloc := 7220;
        --CLOSE cur_get_index;
    --
        pgmloc := 7230;
        Close_File(v_output_file_name,
                   v_file_handle,
                   'B', -- component code
                   p_work_date,
                   p_flag_both,
                   p_type_output,
                   p_success
        );
    END LOOP;
--
    pgmloc := 7240;
    p_success := TRUE;
END Gen_Securities_File;
--
--
--
--------------------------------------------------------------------------------
-- GEN_SECURITIES_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Securities_File2(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_holiday           IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
    v_file_handle          UTL_FILE.file_type;
    v_output_file_name     VARCHAR2(60);
    v_record               VARCHAR2(1000);
--
    v_supp_index_code       supp_index.supp_index_code%TYPE;
    v_text_e                supp_index.text_e%TYPE;
    v_holiday               BOOLEAN;
    v_format_layout_sec     t_format_layout;
    v_del_date              DATE;
    v_work_date             DATE;
--
    v_tbl_index_type             t_tbl_index_type;
--
--  ACB - 04JUL2009
    --CURSOR cur_get_index(p_index_type supp_index.index_type%TYPE)
    CURSOR cur_get_index(p_index_family supp_index.index_family%TYPE)
    IS
    SELECT C.*
    FROM supp_index I,
         client_index_suppliers C
--  ACB - 04JUL2009
-- SSI - 06APR2009
--      ,Data_Supply_Schedule D
-- SSI - 06APR2009 - End of modif.
--  ACB - 04JUL2009 - End of modif.
    WHERE C.fri_client = p_fri_client
    AND   C.Product_Code = p_product_Code
    AND   C.Sub_Product_Code = p_sub_product_Code
    AND   C.Data_Supplier_Code = I.Data_Supplier_Code
--  ACB - 04JUL2009
    AND i.index_family = nvl(p_index_family, i.index_family)
--  SSI - 06APR2009
--  AND i.index_type = nvl(p_index_type, i.index_type)
--  AND   D.Data_Supplier_Code = U.Data_Supplier_Code
--  AND   D.Delivery_Time      = U.Delivery_Time
--  AND   D.Holiday_Class_Code = G_Holiday_Class
--  SSI - 06APR2009 - End of modif.
--  ACB - 04JUL2009 - End of modif.
    AND I.supp_index_code = C.supp_index_code
    AND I.delivery_time = C.delivery_time
    AND C.constituent_level_flag = 'Y'
    AND C.active_flag = 'Y';
--
    CURSOR cur_securities(i_supp_index_code    supp_index.supp_index_code%TYPE,
                          i_data_supplier_code supp_index.data_supplier_code%TYPE,
                          i_del_date           DATE)
    IS
    SELECT *
    FROM supp_index_constituents
    WHERE delivery_date = i_del_date
    AND supp_index_code = i_supp_index_code
-- SSI - 10FEB2011
--  AND price_date =  TRUNC(i_del_date)
    AND price_date =  p_work_date
    AND data_supplier_code = i_data_supplier_code;
--
    TYPE t_tbl_index IS TABLE OF client_index_suppliers%ROWTYPE;
    v_tbl_index t_tbl_index;
BEGIN
    pgmloc := 7250;
--
    pgmloc := 7260;
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'D', -- component code
                       'D' -- record type
                      );
    v_format_layout_sec := g_format_layout;
--
    Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'D',   -- component code
                           v_tbl_index_type,
                           p_success
    );
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
        --
        -- If the output type is a file, open it and write the header
        --
        pgmloc := 7270;
        Open_File('w', -- file mode
                 p_calc_date,
                 p_fri_client,
                 p_product_code,
                 p_sub_product_code,
                 p_flag_both,
                 p_type_output,
                 'D', -- component code
                 v_output_file_name,
                 v_file_handle,
                 p_success,
                 v_tbl_index_type(v_ind_type_pos)
        );
--
        pgmloc := 7280;
        OPEN cur_get_index( v_tbl_index_type(v_ind_type_pos) );
        FETCH cur_get_index BULK COLLECT INTO v_tbl_index;
        CLOSE cur_get_index;
        FOR v_ind_index IN 1..v_tbl_index.COUNT -- {for each index
        LOOP
            pgmloc := 7290;
--      SSI - 02DEC2015
            v_work_date := p_work_date;
            IF NOT G_T_Service AND
               g_flag_is_holiday
            THEN
               v_work_date := Load_Ind_Supplier_Rules.Get_Next_Open_Date (p_work_date,
                                                                          g_data_supplier_code,
                                                                          v_tbl_index(v_ind_index).delivery_time);
            END IF;
--      SSI - 02DEC2015
--          V_Del_Date := TO_DATE( TO_CHAR(p_work_Date,'DDMMYYYY') ||
            V_Del_Date := TO_DATE( TO_CHAR(v_work_Date,'DDMMYYYY') ||
--      SSI - 02DEC2015 - end of modif
                                     v_tbl_index(v_ind_index).delivery_time
                                  ,'DDMMYYYYHH24MI');
--
            FOR v_supp_index_constituents IN cur_securities
                                   (v_tbl_index(v_ind_index).supp_index_code
                                   ,v_tbl_index(v_ind_index).Data_Supplier_Code
                                   ,v_del_date)
            LOOP
-- SSI - 05NOV2010
              IF (p_holiday AND NVL(v_supp_index_constituents.weight,0)=0)
              THEN  --{
                 NULL;   -- Skip if holiday and no weight, ie we do not CFW deleted securities
              ELSE  --}{
-- SSI - 05NOV2010 - end of modif.
                IF (p_holiday) THEN
                    pgmloc := 7300;
                    v_supp_index_constituents.security_return   := 0;
                    v_supp_index_constituents.security_return_2 := 0;
                    v_supp_index_constituents.security_return_3 := 0;
                    v_supp_index_constituents.security_return_4 := 0;
                    v_supp_index_constituents.security_return_5 := 0;
--              --  SSI - 22NOV2012
--              --  MSCI hedged index has 6 returns, where calc_field_07 is net return in CAD
                    IF v_tbl_index(v_ind_index).Data_Supplier_Code='MSCT'
                    THEN
                        v_supp_index_constituents.calc_field_07 := 0;
                    END IF;
--              -- SSI - 22NOV2012 - end of modif
                END IF;
                --
                -- Write into the file, if requested
                --
                pgmloc := 7310;
                IF ( p_type_output = c_type_out_file OR p_flag_both ) THEN
                    pgmloc := 7320;
                    g_format_layout := v_format_layout_sec;
                    Get_Format_Record(p_calc_date,
                                      v_supp_index_constituents,
-- SSI - 24MAR2011
                                      p_fri_client,
                                      G_Fmt_Fri_Client,
                                      p_product_code,
                                      'D',   -- component
                                      'D',
                                      NULL, -- series type
                                      v_record,
                                      p_success);
--
                    pgmloc := 7330;
                    Load_Supplier_Rules.Write_file(v_file_handle,
                                                   v_record,
                                                   Pricing_Msgs_Rec,
                                                   p_success
                                                  );
                   IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
               END IF;
              END IF;  --}
           END LOOP;
        END LOOP;  --} For each index
    --
        pgmloc := 7340;
        --CLOSE cur_get_index;
    --
        pgmloc := 7350;
        Close_File(v_output_file_name,
                   v_file_handle,
                   'D', -- component code
                   p_work_date,
                   p_flag_both,
                   p_type_output,
                   p_success
        );
    END LOOP;
--
    pgmloc := 7360;
    p_success := TRUE;
END Gen_Securities_File2;
--
--
--
--------------------------------------------------------------------------------
-- GEN_RISK_FILE
--------------------------------------------------------------------------------
PROCEDURE Gen_Risk_File(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
    v_file_handle_secur_char        utl_file.file_type;
    v_output_file_secur_char_name   VARCHAR2(100);
    v_record                        VARCHAR2(1000);
    v_prev_id                       VARCHAR2(60);
-- SSI - 03MAR2009
    v_format_layout_sec             t_format_layout;
    v_tbl_index_type                t_tbl_index_type;
--
    CURSOR cur_risk_numbers(p_work_date   DATE,
                            p_security_id index_securities.security_id%TYPE,
                            p_file_type   index_securities.file_type%TYPE)
    IS
    SELECT I.*
    FROM index_securities I
    WHERE I.security_id = p_security_id
    AND I.delivery_date between p_work_date and p_work_date + 0.99999
    AND I.data_supplier_code = g_data_supplier_code
    AND I.file_type = p_file_type;
--
-- ADD COMMENT ACB 14JUL2010 (entire cursor cur_sel_securities)
---- SSI - 03MAR2009
----  CURSOR cur_sel_securities
---- ACB - 04JUL2009
----    CURSOR cur_sel_securities(W_Index_Type    Supp_Index_Types.Index_Type%TYPE)
--    CURSOR cur_sel_securities (w_index_family    Supp_Index_families.index_family%TYPE)
--    IS
----  SELECT DISTINCT S.supp_sec_id, SC.file_type
--    SELECT DISTINCT S.supp_sec_id
--                  , S.supp_sec_id_2
--                  , S.supp_sec_id_3
--                  , S.supp_sec_id_4
--                  , SC.file_type
---- SSI - 18DEC2009
--                  , SC.Sort_Order
--    FROM supp_index_constituents S,
--         supp_index_criteria SC,
--         client_index_suppliers C,
--    -- ADD ACB 29JAN2009
--         supp_index I
----  ACB - 04JUL2009
---- SSI - 06APR2009
----      ,Data_Supply_Schedule D
---- SSI - 06APR2009 - End of modif.
----  ACB - 04JUL2009 - End of modif.
--    WHERE I.supp_index_code = SC.supp_index_code
--    AND I.supp_index_code = S.supp_index_code
--    AND I.data_supplier_code = S.data_supplier_code
--    AND I.data_supplier_code = SC.data_supplier_code
--    -- END ADD ACB
--    AND S.delivery_date between p_work_date and p_work_date + 0.99999
--    AND C.fri_client = p_fri_client
---- SSI - 17OCT2008
--    AND C.product_Code = p_product_code
--    AND C.sub_product_Code = p_sub_product_code
--    AND C.constituent_level_flag = 'Y'
--    AND C.active_flag = 'Y'
--    AND S.supp_index_code = C.supp_index_code
---- SSI - 17OCT2008
----  AND S.data_supplier_code = g_data_supplier_code
--    AND S.data_supplier_code = SC.data_supplier_code
---- SSI - 01OCT2008
--    AND C.Data_Supplier_Code = S.Data_Supplier_Code
----
--    AND C.Delivery_Time = SC.Delivery_Time
----  ACB - 04JUL2009
----  SSI - 06APR2009
----  AND   D.Data_Supplier_Code = U.Data_Supplier_Code
----  AND   D.Delivery_Time      = U.Delivery_Time
----  AND   D.Holiday_Class_Code = G_Holiday_Class
----  SSI - 06APR2009 - End of modif.
----  ACB - 04JUL2009 - End of modif.
--    AND SC.supp_index_code = C.supp_index_code
--    AND p_work_date BETWEEN SC.starting_date AND nvl(SC.ending_date,p_work_date)
--    AND SC.active_flag = 'Y'
----  SSI - 17DEC2009
----  AND nvl(S.weight, 0) <> 0
----  ACB - 04JUL2009
----  SSI - 03MAR2009
--    --AND I.index_type = nvl(W_index_type, i.index_type)
--    AND I.index_family = nvl(w_index_family, i.index_family)
---- ACB - 04JUL2009 - End of modif.
---- SSI - 18DEC2009
----  AND I.risk_number_flag = 'Y';
--    AND I.risk_number_flag = 'Y'
--order by S.Supp_Sec_ID,SC.Sort_Order;
-- END COMMENT ACB 14JUL2010
--
-- ADD ACB 14JUL2010 (new cursor cur_sel_securities)
-- SSI - 10FEB2011
-- Rewrite cursor
--  CURSOR cur_sel_securities (w_index_family  Supp_Index_families.index_family%TYPE)
--  IS
--  SELECT /*+ USE_HASH(S,I) */
--         DISTINCT S.supp_sec_id,
--                  S.supp_sec_id_2,
--                  S.supp_sec_id_3,
--                  S.supp_sec_id_4,
--                  SC.file_type,
--                  SC.Sort_Order
--  FROM supp_index I,
--       supp_index_constituents S,
--       supp_index_criteria SC
--  WHERE I.supp_index_code = SC.supp_index_code
--  AND I.data_supplier_code = SC.data_supplier_code
--  AND S.data_supplier_code = SC.data_supplier_code
--  AND SC.active_flag = 'Y'
--  AND p_work_date BETWEEN SC.starting_date AND nvl(SC.ending_date,p_work_date)
--  AND I.data_supplier_code = G_Data_Supplier_Code
--  AND I.Risk_Number_Flag = 'Y'
--  AND I.Index_Family = nvl(w_index_family, i.index_family)
--  AND S.Data_Supplier_Code = G_Data_Supplier_Code
--  AND S.Supp_Index_Code = I.Supp_Index_Code
--  AND S.Delivery_Date BETWEEN p_work_date and p_work_date + 1
--  AND I.Delivery_Time = TO_CHAR (S.Delivery_Date, 'hh24mi')
--                     1
--              FROM Supp_Index_Constituents S1,
--                   Supp_Index I1,
--                   Client_Index_Suppliers C
--              WHERE  C.Fri_Client = p_fri_client
--              AND C.Product_Code = p_product_code
--              AND C.Sub_Product_Code = p_sub_product_code
--              AND C.Constituent_Level_Flag = 'Y'
--              AND C.Active_Flag = 'Y'
--              AND C.Data_Supplier_Code = G_Data_Supplier_Code
--              AND C.Delivery_Time = TO_CHAR (S1.Delivery_Date, 'hh24mi')
--              AND I1.Data_Supplier_Code = G_Data_Supplier_Code
--              AND C.Supp_Index_Code = I1.Supp_Index_Code
--              AND I1.Index_Family = nvl(w_index_family, I1.index_family)
--              AND I1.Delivery_Time = TO_CHAR (S1.Delivery_Date, 'hh24mi')
--              AND I1.Constituent_Level_Flag = 'Y'
--              AND C.Data_Supplier_Code = S1.Data_Supplier_Code
--              AND C.Supp_Index_Code = S1.Supp_Index_Code
--              AND S1.Delivery_Date BETWEEN p_work_date and p_work_date + 1
--              AND S1.Supp_Sec_Id = S.Supp_Sec_Id
--              AND S1.Data_Supplier_Code = S.Data_Supplier_Code
--              AND NVL (S1.Weight, 0) <> 0)
--  ORDER BY S.Supp_Sec_Id, SC.Sort_Order;
-- ACB 14JUL2010 - END new cursor
--
    CURSOR cur_sel_securities (w_index_family  Supp_Index_families.index_family%TYPE)
    IS
-- SSI - 29JAN2013
--  SELECT /*+ USE_HASH(S,I,S1,I1) */
    SELECT /*+ USE_HASH(C,I1,S1,SC) ORDERED */
           DISTINCT S.supp_sec_id,
                    S.supp_sec_id_2,
                    S.supp_sec_id_3,
                    S.supp_sec_id_4,
                    SC.file_type,
                    SC.Sort_Order
    FROM supp_index I,
         supp_index_constituents S,
         supp_index_criteria SC,
         Supp_Index_Constituents S1,
         Supp_Index I1,
         Client_Index_Suppliers C
    WHERE I.data_supplier_code = G_Data_Supplier_Code
    AND I.Risk_Number_Flag = 'Y'
    AND I.Index_Family = nvl(w_index_family, i.index_family)
    AND SC.supp_index_code = I.supp_index_code
    AND SC.data_supplier_code = I.data_supplier_code
    AND SC.active_flag = 'Y'
    AND p_work_date BETWEEN SC.starting_date AND nvl(SC.ending_date,p_work_date)
-- SSI - 29JAN2013
--  AND S.Data_Supplier_Code = G_Data_Supplier_Code
    AND S.Data_Supplier_Code = I.Data_Supplier_Code
    AND S.Supp_Index_Code = I.Supp_Index_Code
    AND S.Delivery_Date BETWEEN p_work_date and p_work_date + 1
    AND S.Price_Date = p_work_date
    AND I.Delivery_Time = TO_CHAR (S.Delivery_Date, 'hh24mi')
    AND C.Fri_Client = p_fri_client
    AND C.Product_Code = p_product_code
    AND C.Sub_Product_Code = p_sub_product_code
    AND C.Constituent_Level_Flag = 'Y'
    AND C.Active_Flag = 'Y'
-- SSI - 29JAN2013
--  AND C.Data_Supplier_Code = G_Data_Supplier_Code
    AND C.Data_Supplier_Code = I.Data_Supplier_Code
    AND C.Delivery_Time = TO_CHAR (S1.Delivery_Date, 'hh24mi')
-- SSI - 29JAN2013
--  AND I1.Data_Supplier_Code = G_Data_Supplier_Code
    AND I1.Data_Supplier_Code = I.Data_Supplier_Code
    AND C.Supp_Index_Code = I1.Supp_Index_Code
    AND I1.Index_Family = nvl(w_index_family, I.index_family)
    AND I1.Delivery_Time = TO_CHAR (S1.Delivery_Date, 'hh24mi')
    AND I1.Constituent_Level_Flag = 'Y'
    AND S1.Data_Supplier_Code = C.Data_Supplier_Code
    AND S1.Supp_Index_Code = C.Supp_Index_Code
    AND S1.Delivery_Date BETWEEN p_work_date and p_work_date + 1
    AND S1.Price_Date = p_work_date
    AND S1.Supp_Sec_Id = S.Supp_Sec_Id
    AND S1.Data_Supplier_Code = S.Data_Supplier_Code
    AND NVL (S1.Weight, 0) <> 0
    ORDER BY S.Supp_Sec_Id, SC.Sort_Order;
-- SSI - 10FEB2011 - Cursor rewrite end.
--
    v_index_securities cur_risk_numbers%ROWTYPE;
--
    V_num1    INTEGER:=0;
    V_num2    INTEGER:=0;
BEGIN
    pgmloc := 7370;
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'K', -- component code
                       'D' -- record type
                      );
    v_format_layout_sec := g_format_layout;
--
    pgmloc := 7380;
    Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'K',   -- component code
                           v_tbl_index_type,
                           p_success
    );
--
--  Add loop to loop through each index type required
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP
--
    -- Open the Risk Number file
       Open_File('w', -- file mode
                 p_calc_date,
                 p_fri_client,
                 p_product_code,
                 p_sub_product_code,
                 p_flag_both,
                 p_type_output,
                 'K', -- component code
                 v_output_file_secur_char_name,
                 v_file_handle_secur_char,
                 p_success
-- SSI - 03MAR2009
                ,v_tbl_index_type(v_ind_type_pos)
       );
--
       pgmloc := 7390;
--  SSI - 17OCT2008
--  Load_Format_Layout(p_fri_client,
-- SSI - 03MAR2009
-- Coding moved up
--  Load_Format_Layout(G_Fmt_Fri_Client,
--                     p_product_code,
--                     'K', -- component code
--                     'D' -- record type
--                    );
--
-- SSI - 03MAR2009
--     FOR reg IN cur_sel_securities
-- SSI - 18DEC2009
       v_Prev_Id := NULL;
       FOR reg IN cur_sel_securities(v_tbl_index_type(v_ind_type_pos))
       LOOP  --{ Index_Type
-- SSI - 18DEC2009
-- Add IF block to skip duplicates
         IF V_Prev_ID IS NULL OR
            V_Prev_ID <> reg.supp_sec_id
         THEN
           V_Num1 := V_Num1 + 1;
           pgmloc := 7400;
           OPEN cur_risk_numbers(p_work_date, reg.supp_sec_id, reg.file_type);
           pgmloc := 7410;
           FETCH cur_risk_numbers INTO v_index_securities;
-- SSI - 01OCT2008
--         pgmloc := 7420;
--         CLOSE cur_risk_numbers;
--
           pgmloc := 7430;
--         IF (p_type_output = c_type_out_file OR p_flag_both) THEN
           IF cur_risk_numbers%FOUND AND
              (p_type_output = c_type_out_file OR p_flag_both) THEN
-- SSI - 24NOV2009
--         Exeptions
               IF G_Data_Supplier_Code=C_IBOXX AND
                  reg.File_type like 'EOM%'
               THEN
                  GOTO Skip_Rec;
               END IF;
-- SSI 24NOV2009 - end of modif.
           V_Num2 := V_Num2 + 1;
-- SSI - 25MAY2009
-- Assign OASIS IDs for output purposes only
               v_index_securities.security_id_6 := Reg.Supp_sec_id_2;
               v_index_securities.security_id_7 := Reg.Supp_sec_id_3;
               v_index_securities.security_id_8 := Reg.Supp_sec_id_4;
-- SSI - 25MAY2009 - End of modif.
               pgmloc := 7440;
               g_format_layout := v_format_layout_sec;
               Get_Format_Record(p_calc_date,
                                 v_index_securities,
--                          -- SSI - 17OCT2008
--                          --   p_fri_client,
                                 G_Fmt_Fri_Client,
                                 p_product_code,
                                 'K', -- component code
                                 'D', -- record type,
                                 NULL, -- series type
                                 v_record,
                                 p_success);
              pgmloc := 7450;
              Load_Supplier_Rules.Write_file(v_file_handle_secur_char,
                                             v_record,
                                             Pricing_Msgs_Rec,
                                             p_success
                                            );
              IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
-- SSI - 06JAN2010
-- Assignment moved up - Assign only if a risk record is actually found and written
              V_Prev_Id := Reg.Supp_sec_id;
-- SSI 24NOV2009
<<skip_rec>>
              NULL;
-- SSI 24NOV2009 - end of modif.
           END IF; -- IF (p_type_output = c_type_out_table OR p_flag_both)
-- SSI - 01OCT2008
           pgmloc := 7460;
           CLOSE cur_risk_numbers;
-- SSI - 06JAN2010
-- Move assignment up
--         V_Prev_Id := Reg.Supp_sec_id;
-- SSI - 18DEC2009
         END IF; --} if new ID
       END LOOP;
--
       pgmloc := 7470;
       Close_File(v_output_file_secur_char_name,
                 v_file_handle_secur_char,
                 'K', -- component code
-- SSI - 08NOV2008
--               NULL,
                 p_work_date,
                 p_flag_both,
                 p_type_output,
                 p_success
      );
      dbms_output.put_line ('Risk # ' || V_Num1 || ' ' || V_Num2);
      V_Num1 := 0;
      V_Num2 := 0;
    END LOOP; -- }Index_Type
--
    pgmloc := 7480;
    p_success := TRUE;
END Gen_Risk_File;
--
--
--
--------------------------------------------------------------------------------
-- GEN_RISK_FILE_EQUITIES
--------------------------------------------------------------------------------
PROCEDURE Gen_Risk_File_Equities(
    p_calc_date         IN  DATE,
    p_work_date         IN  DATE,
    p_fri_client        IN  clients.fri_client%TYPE,
    p_client_code       IN  clients.code%TYPE,
    p_product_code      IN  products.code%TYPE,
    p_sub_product_code  IN  sub_products.sub_product_code%TYPE,
    p_flag_both         IN  BOOLEAN,
    p_type_output       IN  VARCHAR2,
    p_success           OUT BOOLEAN
)
AS
--  SSI 16JUN2013
    TYPE t_tbl_ids IS TABLE OF INTEGER INDEX BY Supp_Index_Constituents.Supp_Sec_ID%TYPE;
--
    v_tbl_ids                       t_tbl_ids;
    V_ID                            Supp_Index_Constituents.Supp_Sec_ID%TYPE;
--  SSI - 14JUN2013 - end of modif
    v_file_handle_secur_char        utl_file.file_type;
    v_output_file_secur_char_name   VARCHAR2(100);
    v_record                        VARCHAR2(1000);
-- SSI - 06APR2009
    v_format_layout_sec             t_format_layout;
    v_tbl_index_type                t_tbl_index_type;
-- 
-- SSI - 14JUN2013
-- Comment out cursor to be rewritten in 2 seoarate cursors
--
-- ACB 04JUL2009 PUT ALL CURSOR IN COMMENT
---- SSI - 06APR2009
----  CURSOR cur_sel_securities
--    CURSOR cur_sel_securities(W_Index_Type    Supp_Index_Types.Index_Type%TYPE)
--    IS
----  SSI - 06-APR-2009
----  No need for the distincy - the whole record is selected
----  SELECT DISTINCT S.*
--    SELECT S.*
--    FROM supp_index_constituents S,
---- SSI - 03JUL2009
---- Remove join on Supp_Index_Criteria
----       supp_index_criteria SC,
--         client_index_suppliers C,
--         supp_index I
---- SSI - 06APR2009
--        ,Data_Supply_Schedule D
---- SSI - 06APR2009 - End of modif.
---- SSI - 03JUL2009
----  WHERE I.supp_index_code = SC.supp_index_code
----  AND I.supp_index_code = S.supp_index_code
--    WHERE I.supp_index_code = S.supp_index_code
--    AND I.data_supplier_code = S.data_supplier_code
---- SSI - 03JUL2009
----  AND I.data_supplier_code = SC.data_supplier_code
--    AND I.delivery_time = C.delivery_time
--    AND S.delivery_date between p_work_date and p_work_date + 0.99999
--    AND C.fri_client = p_fri_client
--    AND C.product_Code = p_product_code
--    AND C.sub_product_Code = p_sub_product_code
--    AND C.constituent_level_flag = 'Y'
--    AND C.active_flag = 'Y'
---- SSI - 06APR2009
--    AND D.Data_Supplier_Code = C.Data_Supplier_Code
--    AND D.Delivery_Time      = C.Delivery_Time
--    AND D.Holiday_Class_Code = G_Holiday_Class
----  Add restriction on index type if given
--    AND I.index_type = nvl(W_index_type, i.index_type)
---- SSI - 06APR2009 - End of modif.
--    AND S.supp_index_code = C.supp_index_code
---- SSI - 03JUL2009
----  AND S.data_supplier_code = SC.data_supplier_code
--    AND C.Data_Supplier_Code = S.Data_Supplier_Code
---- SSI - 03JUL2009
----  AND C.Delivery_Time = SC.Delivery_Time
----  AND SC.supp_index_code = C.supp_index_code
----  AND p_work_date BETWEEN SC.starting_date AND nvl(SC.ending_date,p_work_date)
----  AND SC.active_flag = 'Y'
--    AND nvl(S.weight, 0) <> 0
--    AND I.risk_number_flag = 'Y';
-- ACB 04JUL2009 END - PUT ALL CURSOR IN COMMENT
--
-- ACB - 21APR2010 Cursor cur_sel_securities in comment
-- ACB - 04JUL2009 Cursor rewritten
--    CURSOR cur_sel_securities (w_index_family supp_index_families.Index_family%TYPE)
--    IS
--    SELECT S.*
--    FROM supp_index_constituents S,
--         client_index_suppliers C,
--         supp_index I
--    WHERE I.supp_index_code = I.supp_index_code
--    AND I.supp_index_code = S.supp_index_code
--    AND I.data_supplier_code = S.data_supplier_code
--    AND I.delivery_time = C.delivery_time
--    AND S.delivery_date between p_work_date and p_work_date + 0.99999
--    AND C.fri_client = p_fri_client
--    AND C.product_Code = p_product_code
--    AND C.sub_product_Code = p_sub_product_code
--    AND C.constituent_level_flag = 'Y'
--    AND C.active_flag = 'Y'
--    AND I.index_family = nvl(w_index_family, i.index_family)
--    AND S.supp_index_code = C.supp_index_code
--    AND C.Data_Supplier_Code = S.Data_Supplier_Code
--    AND C.Delivery_Time = I.Delivery_Time
--    AND nvl(S.weight, 0) <> 0
---- SSI - 11FEB2010
----  AND I.risk_number_flag = 'Y';
--    AND I.risk_number_flag = 'Y'
---- SSI - 22FEB2010
----  order by S.Supp_Sec_ID,S.Price_date ASC;
--    order by S.Supp_Sec_ID,S.Price_date ASC;
--
--  SSI - 14JUN2013
--  CURSOR cur_sel_securities (w_index_family supp_index_families.Index_family%TYPE)
--  IS
-- SSI - 28NOV2011
--  SELECT /*+ USE_HASH(S,I) */
--  SELECT /*+ LEADING(I) */
--         S.*
--  FROM supp_index I,
--       supp_index_constituents S
--  WHERE I.data_supplier_code = G_Data_Supplier_Code
--  AND I.Risk_Number_Flag = 'Y'
--  AND I.Index_Family = nvl(w_index_family, i.index_family)
--  SSI - 22MAY2012
--  AND S.Data_Supplier_Code = G_Data_Supplier_Code
--  AND S.Data_Supplier_Code = I.Data_Supplier_Code
--  AND S.Supp_Index_Code = I.Supp_Index_Code
    -- Miglena - 10.May.2013 - cursor changed to speed it up
    --AND S.Delivery_Date BETWEEN p_work_date and p_work_date + 0.99999
--  AND S.Delivery_Date >= p_work_date
--  AND S.Delivery_Date <  p_work_date + 1
    -- end Miglena
--  AND NVL (S.Weight, 0) <> 0
--  AND I.Delivery_Time = TO_CHAR (S.Delivery_Date, 'hh24mi')
-- SSI - 28NOV2011
--  AND EXISTS (SELECT /*+ index (S1 supp_index_constituents_X_01) index(I1 Supp_Index_X_02) */
-- SSI - 22MAY2012
--  AND EXISTS (SELECT /*+ USE_HASH(C,I1) */
--  AND EXISTS (SELECT/*+ LEADING(C) */
--                     1
--              FROM Supp_Index_Constituents S1,
--                   Supp_Index I1,
--                   Client_Index_Suppliers C
--              WHERE  C.Fri_Client = p_fri_client
--              AND C.Product_Code = p_product_code
--              AND C.Sub_Product_Code = p_sub_product_code
--              AND C.Constituent_Level_Flag = 'Y'
--              AND C.Active_Flag = 'Y'
--              AND C.Data_Supplier_Code = G_Data_Supplier_Code
--              AND C.Delivery_Time = TO_CHAR (S1.Delivery_Date, 'hh24mi')
--              AND I1.Data_Supplier_Code = G_Data_Supplier_Code
--              AND C.Supp_Index_Code = I1.Supp_Index_Code
--              AND I1.Index_Family = nvl(w_index_family, I1.index_family)
-- SSI - 22MAY2012
--              AND I1.Delivery_Time = TO_CHAR (S1.Delivery_Date, 'hh24mi')
--              AND I1.Delivery_Time = C.Delivery_Time
--              AND I1.Constituent_Level_Flag = 'Y'
--              AND C.Data_Supplier_Code = S1.Data_Supplier_Code
--              AND C.Supp_Index_Code = S1.Supp_Index_Code
                -- Miglena - 10.May.2013 - cursor changed to speed it up
                --AND S1.Delivery_Date BETWEEN p_work_date and p_work_date + 0.99999
--              AND S1.Delivery_Date >= p_work_date
--              AND S1.Delivery_Date <  p_work_date + 1
                -- end Miglena
--              AND S1.Supp_Sec_Id = S.Supp_Sec_Id
--              AND S1.Data_Supplier_Code = S.Data_Supplier_Code
--              AND NVL (S1.Weight, 0) <> 0)
--  ORDER BY S.Supp_Sec_Id,S.Price_Date ASC;
-- END ACB 21APR2010
--
    CURSOR cur_clt_securities (w_index_family supp_index_families.Index_family%TYPE)
    IS
    SELECT S1.Supp_Sec_ID
    FROM Supp_Index_Constituents S1,
         Supp_Index I1,
         Client_Index_Suppliers C
    WHERE  C.Fri_Client = p_fri_client
    AND C.Product_Code = p_product_code
    AND C.Sub_Product_Code = p_sub_product_code
    AND C.Constituent_Level_Flag = 'Y'
    AND C.Active_Flag = 'Y'
    AND C.Data_Supplier_Code = G_Data_Supplier_Code
    AND C.Delivery_Time = TO_CHAR (S1.Delivery_Date, 'hh24mi')
    AND I1.Data_Supplier_Code = G_Data_Supplier_Code
    AND C.Supp_Index_Code = I1.Supp_Index_Code
    AND I1.Index_Family = nvl(w_index_family, I1.index_family)
    AND I1.Delivery_Time = C.Delivery_Time
    AND I1.Constituent_Level_Flag = 'Y'
    AND C.Data_Supplier_Code = S1.Data_Supplier_Code
    AND C.Supp_Index_Code = S1.Supp_Index_Code
    AND S1.Delivery_Date > p_work_date
    AND S1.Delivery_Date <  p_work_date + 1
    AND NVL (S1.Weight, 0) <> 0;
--
    CURSOR cur_sel_securities (w_index_family supp_index_families.Index_family%TYPE)
    IS
    SELECT S.*
    FROM supp_index I,
         supp_index_constituents S
    WHERE I.data_supplier_code = G_Data_Supplier_Code
    AND I.Risk_Number_Flag = 'Y'
    AND I.Index_Family = nvl(w_index_family, i.index_family)
    AND S.Data_Supplier_Code = I.Data_Supplier_Code
    AND S.Supp_Index_Code = I.Supp_Index_Code
    AND S.Delivery_Date > p_work_date
    AND S.Delivery_Date <  p_work_date + 1
    AND NVL (S.Weight, 0) <> 0
    AND I.Delivery_Time = TO_CHAR (S.Delivery_Date, 'hh24mi')
    ORDER BY S.Supp_Sec_Id,S.Price_Date ASC;
--
    Prev_Rec   cur_sel_securities%ROWTYPE;
BEGIN
-- SSI - 06APR2009
-- Possibility to break by Index_Type
    pgmloc := 7490;
    Load_Format_Layout(G_Fmt_Fri_Client,
                       p_product_code,
                       'N', -- component code
                       'D' -- record type
                      );
    v_format_layout_sec := g_format_layout;
--
    pgmloc := 7500;
    Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'N',   -- component code
                           v_tbl_index_type,
                           p_success
    );
--
--  Add loop to loop through each index type required
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP -- {Index_Type
--
    pgmloc := 7510;
    -- Open the Risk Number file
    Open_File('w', -- file mode
              p_calc_date,
              p_fri_client,
              p_product_code,
              p_sub_product_code,
              p_flag_both,
              p_type_output,
              'N', -- component code
              v_output_file_secur_char_name,
              v_file_handle_secur_char,
              p_success
-- SSI - 06APR2009
             ,v_tbl_index_type(v_ind_type_pos)
    );
--
    pgmloc := 7520;
-- SSI - 06APR2009
-- Call move up ealier in coding
--  Load_Format_Layout(G_Fmt_Fri_Client,
--                     p_product_code,
--                     'N', -- component code
--                     'D' -- record type
--                    );
--
-- SSI - 06APR2009
--  FOR reg_supp_index_constituents IN cur_sel_securities
-- SSI - 06APR2009
    g_format_layout := v_format_layout_sec;
--  
-- SSI 14JUN2013
-- Save all ids in memory table
--
    OPEN cur_clt_securities(v_tbl_index_type(v_ind_type_pos));
    LOOP
        pgmloc := 7530;
        FETCH cur_clt_securities INTO v_id;
        EXIT WHEN cur_clt_securities%NOTFOUND;
        pgmloc := 7540;
        v_tbl_ids(v_id) := 1;
    END LOOP;
    CLOSE cur_clt_securities;
--
    pgmloc := 7550;
    FOR reg_supp_index_constituents IN cur_sel_securities(v_tbl_index_type(v_ind_type_pos))
    LOOP
--  -- SSI - 14JUN2013
-- If client has it
     IF v_tbl_ids.EXISTS(reg_supp_index_constituents.Supp_sec_id)
     THEN
-- SSI - 11FEB2010
      IF reg_supp_index_constituents.Supp_Sec_ID <> NVL(Prev_Rec.Supp_Sec_ID,'######') OR
          reg_supp_index_constituents.Price_Date  <> NVL(Prev_Rec.Price_Date,'01-JAN-1960')
      THEN
        pgmloc := 7560;
-- SSI -  22FEB2010
        IF reg_supp_index_constituents.Price_Date = p_Calc_Date
        THEN
           NULL;
-- IF price date is > evaluation date, we CFW the prev record
        ELSIF reg_supp_index_constituents.Price_Date > p_Calc_Date
        THEN
           reg_supp_index_constituents := Prev_Rec;
           reg_supp_index_constituents.Price_Date := reg_supp_index_constituents.Price_Date + 1;
-- IF price_date < evaluation date,adjust price date accordingly
        ELSE
           reg_supp_index_constituents.Price_Date := reg_supp_index_constituents.Price_Date +
                                                     (p_calc_date - reg_supp_index_constituents.Price_Date)-1;
        END IF;
-- SSI -  22FEB2010 - end of modif.
        IF (p_type_output = c_type_out_file OR p_flag_both) THEN
            pgmloc := 7570;
            Get_Format_Record(p_calc_date,
                              reg_supp_index_constituents,
-- SSI - 24MAR2011
                              p_fri_client,
                              G_Fmt_Fri_Client,
                              p_product_code,
                              'N', -- component code
                              'D', -- record type,
                              NULL, -- series type
                              v_record,
                              p_success);
           pgmloc := 7580;
           Load_Supplier_Rules.Write_file(v_file_handle_secur_char,
                                          v_record,
                                          Pricing_Msgs_Rec,
                                          p_success
                                         );
           IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
        END IF; -- IF (p_type_output = c_type_out_table OR p_flag_both)
        Prev_Rec := reg_supp_index_constituents;
      END IF;
--     -- SSI - 14JUN2013
     END IF;
    END LOOP;
--
    pgmloc := 7590;
    Close_File(v_output_file_secur_char_name,
              v_file_handle_secur_char,
              'N', -- component code
              p_work_date,
              p_flag_both,
              p_type_output,
              p_success
   );
-- SSI - 06APR2009
    END LOOP; -- }Index_Type
--
    pgmloc := 7600;
    p_success := TRUE;
END Gen_Risk_File_Equities;
--
--
--
--------------------------------------------------------------------------------
-- GENERATE_TICKER_CHANGES
--------------------------------------------------------------------------------
PROCEDURE Generate_Ticker_Changes(
    p_calc_date          IN  DATE,
    p_work_date          IN  DATE,
    p_last_open_date     IN  DATE,
    p_fri_client         IN  clients.fri_client%TYPE,
    p_product_code       IN  products.code%TYPE,
    p_sub_product_code   IN  sub_products.sub_product_code%TYPE,
    p_success            OUT BOOLEAN
)
AS
    v_fields_name VARCHAR2(1000) := 'change date' || c_field_sep ||
                                    'symbol'      || c_field_sep ||
                                    'old code'    || c_field_sep ||
                                    'new code';
    --
    v_header      VARCHAR2(1000) := 'statpro'     || c_field_sep ||
                                    'statpro'     || c_field_sep ||
                                    'statpro'     || c_field_sep ||
                                    'statpro';
--
--  SSI - 25APR2014
--  TYPE t_Tick_Chg IS TABLE OF TICKERS.TICKER%TYPE INDEX BY TICKERS.TICKER%TYPE;
    TYPE t_Tick_Chg IS TABLE OF Supp_Ticker_Changes.New_Ticker%TYPE INDEX BY Supp_Ticker_Changes.New_Ticker%TYPE;
    V_Chg_C             t_Tick_Chg;
    V_Chg_S             t_Tick_Chg;
    V_Chg_Z             t_Tick_Chg;
--  SSI - 25APR2014
    V_Chg_L             t_Tick_Chg;
    V_Chg_ID_C          t_Tick_Chg;
    V_Chg_ID_S          t_Tick_Chg;
    V_Chg_ID_I          t_Tick_Chg;
--  SSI - 25APR2014
    V_Chg_ID_L          t_Tick_Chg;
    V_Chg               t_Tick_Chg;
--
    n                   INTEGER;
--
    v_delivery_date     DATE;
    v_prev_delivery_date     DATE;
    v_file_handle       Utl_File.File_Type;
    v_file_name         VARCHAR2(100);
    v_record            VARCHAR2(1000);
    V_Symbol            VARCHAR2(20);
--  SSI - 25APR2014
--  V_Old_Ticker        VARCHAR2(20);
--  V_New_Ticker        VARCHAR2(20);
    V_Old_Ticker        Supp_Ticker_Changes.Old_Ticker%TYPE;
    V_New_Ticker        Supp_Ticker_Changes.New_Ticker%TYPE;
--  SSI - 25APR2014 - end of modif
--
-- SSI - 06APR2009
    v_tbl_index_type                t_tbl_index_type;
--
-- SSI - 19MAY2009
--  SSI -  16JUN2009
--  TYPE t_id_sys IS TABLE OF NUMBER INDEX BY Identification_Systems.Code%TYPE;
    v_id_sys      t_id_sys;
    v_id_sys_code Identification_systems.Code%TYPE;
    V_Oasis_ID    YES_NO.Code%TYPE;
    V_Oasis_ISIN  YES_NO.Code%TYPE;
    V_Oasis_CUSIP YES_NO.Code%TYPE;
    V_Oasis_SEDOL YES_NO.Code%TYPE;
    nloop         INTEGER;
--
    CURSOR Get_Clt_ID_SYS
    IS
    SELECT *
    FROM   Client_Ident_Systems
    WHERE  Fri_Client = P_Fri_Client;
-- SSI - 19MAY2009 - End of modif.
--
    CURSOR cur_sel_ticker_change
                (I_Data_Supplier_Code   Data_Suppliers.Code%TYPE
                ,I_Supp_index_Code      Supp_Index.Supp_Index_Code%TYPE
                )
    IS
    SELECT DISTINCT
           C.supp_sec_id
          ,C.SEDOL
          ,C.CUSIP
          ,C.ISIN
-- SSI - 22OCT2008
-- SSI - 19MAY2009
--        ,NVL(C.SEDOL,C.Supp_sec_Id) ID_S
--        ,NVL(C.CUSIP,C.Supp_sec_Id) ID_C
--        ,NVL(C.ISIN,C.Supp_sec_Id) ID_I
          ,DECODE(V_OASIS_SEDOL,'N',NVL(C.SEDOL,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_4) ID_S
                     NVL(C.Supp_Sec_ID_4,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_S
          ,DECODE(V_OASIS_CUSIP,'N',NVL(C.CUSIP,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_3) ID_C
                     NVL(C.Supp_Sec_ID_3,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_C
          ,DECODE(V_OASIS_ISIN,'N',NVL(C.ISIN,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_2) ID_I
                     NVL(C.Supp_Sec_ID_2,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_I
-- SSI - 19MAY2009 - End of modif
    FROM  supp_index_constituents C
    WHERE C.Data_Supplier_Code = I_Data_Supplier_Code
    AND   C.supp_Index_Code = I_supp_index_code
    AND   C.delivery_Date = v_Delivery_Date
    AND   EXISTS (Select 1
                  FROM Supp_Index_Constituents C1
                  WHERE C1.Data_Supplier_Code = C.Data_Supplier_Code
                  AND   C1.supp_Index_Code = C.supp_index_code
                  AND   C1.delivery_Date = v_prev_Delivery_Date)
MINUS
    SELECT DISTINCT
           C.supp_sec_id
          ,C.SEDOL
          ,C.CUSIP
          ,C.ISIN
-- SSI - 22OCT2008
-- SSI - 19MAY2009
--        ,NVL(C.SEDOL,C.Supp_sec_Id) ID_S
--        ,NVL(C.CUSIP,C.Supp_sec_Id) ID_C
--        ,NVL(C.ISIN,C.Supp_sec_Id) ID_I
          ,DECODE(V_OASIS_SEDOL,'N',NVL(C.SEDOL,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_4) ID_S
                     NVL(C.Supp_Sec_ID_4,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_S
          ,DECODE(V_OASIS_CUSIP,'N',NVL(C.CUSIP,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_3) ID_C
                     NVL(C.Supp_Sec_ID_3,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_C
          ,DECODE(V_OASIS_ISIN,'N',NVL(C.ISIN,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_2) ID_I
                     NVL(C.Supp_Sec_ID_2,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_I
-- SSI - 19MAY2009 - End of modif
    FROM  supp_index_constituents C
    WHERE C.Data_Supplier_Code = I_Data_Supplier_Code
    AND   C.supp_Index_Code = I_supp_index_code
    AND   C.delivery_Date = v_prev_delivery_Date;
--
    CURSOR cur_sel_old_ticker
                 (I_Data_Supplier_Code   Data_Suppliers.Code%TYPE,
                  I_Supp_Index_Code      Supp_Index.Supp_Index_Code%TYPE,
                  I_sec_id               Index_Securities.Security_ID%TYPE
                 )
    IS
    SELECT C.supp_sec_id
          ,C.SEDOL
          ,C.CUSIP
          ,C.ISIN
-- SSI - 22OCT2008
-- SSI - 19MAY2009
--        ,NVL(C.SEDOL,C.Supp_sec_Id) ID_S
--        ,NVL(C.CUSIP,C.Supp_sec_Id) ID_C
--        ,NVL(C.ISIN,C.Supp_sec_Id) ID_I
          ,DECODE(V_OASIS_SEDOL,'N',NVL(C.SEDOL,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_4) ID_S
                     NVL(C.Supp_Sec_ID_4,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_S
          ,DECODE(V_OASIS_CUSIP,'N',NVL(C.CUSIP,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_3) ID_C
                     NVL(C.Supp_Sec_ID_3,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_C
          ,DECODE(V_OASIS_ISIN,'N',NVL(C.ISIN,C.Supp_sec_Id),
-- SSI - 15JUN2009
--                   C.Supp_Sec_ID_2) ID_I
                     NVL(C.Supp_Sec_ID_2,C_Clt_ID_Prefix || C.Supp_Sec_ID)) ID_I
-- SSI - 19MAY2009 - End of modif
    FROM  supp_index_constituents C
    WHERE C.Data_Supplier_Code =  I_Data_Supplier_Code
    AND   C.Supp_Index_Code = I_Supp_Index_Code
    AND   C.Supp_Sec_ID = I_Sec_ID
    AND   C.delivery_Date = V_Prev_Delivery_Date;
--
    CURSOR cur_supp_ticker_change
                 (I_Data_Supplier_Code   Data_Suppliers.Code%TYPE,
                  I_Supp_Index_Code      Supp_Index.Supp_Index_Code%TYPE)
    IS
    SELECT Identification_System_Code
          ,Old_Ticker
          ,New_Ticker
-- SSI - 22OCT2008
          ,Applied_Flag
    FROM  Supp_Ticker_Changes C
    WHERE Data_Supplier_Code = g_data_supplier_code
    AND   Effective_Date = p_work_date
    AND   Effective_Date <  p_work_date+1
    AND   App_Flag = 'Y'
    AND   EXISTS (Select 1
                  FROM Supp_Index_Constituents I
                  WHERE I.Data_Supplier_Code = I_Data_Supplier_Code
                  AND   I.Delivery_Date = v_Delivery_date
                  AND   I.Supp_Index_Code = I_Supp_Index_Code
                  AND   ((C.Identification_System_Code IN ('C','D') AND
                          I.Cusip=C.New_Ticker)
                      OR (C.Identification_System_Code='S' AND
                          I.Sedol=C.New_Ticker)
                      OR (C.Identification_System_Code IN ('Z','O') AND
-- SSI - 25APR2014
-- Add Supp_Sec_ID changes (Local ID)
--                        I.ISIN=C.New_Ticker))
                          I.ISIN=C.New_Ticker)
                      OR (C.Identification_System_Code IN ('L') AND
                          I.Supp_Sec_ID=C.New_Ticker))
                        )
                       ;
--
-- SSI - 06APR2009
--  CURSOR cur_get_index
    --CURSOR cur_get_index (W_Index_Type    Supp_Index_Types.Index_Type%TYPE)
    CURSOR cur_get_index (w_index_family    Supp_Index_families.Index_family%TYPE)
    IS
    SELECT C.*
    FROM client_index_suppliers C
--  SSI - 06APR2009
--  ACB - 04JUL2009
--        ,Data_Supply_Schedule   D
        ,Supp_Index             I
--  SSI - 06APR2009
    WHERE C.fri_client = p_fri_client
    AND   C.Product_Code = p_product_Code
    AND   C.Sub_product_Code = p_sub_product_code
-- SSI - 24APR2014
    AND   C.Constituent_Level_Flag='Y'
-- SSI - 06APR2009
--  ACB - 04JUL2009
--  AND D.Data_Supplier_Code = C.Data_Supplier_Code
--  AND D.Delivery_Time      = C.Delivery_Time
--  AND D.Holiday_Class_Code = G_Holiday_Class
--  ACB - 04JUL2009 - End of modif.
    AND I.Data_Supplier_Code = C.Data_Supplier_Code
    AND I.Supp_Index_Code = C.Supp_Index_Code
    AND I.Delivery_Time = C.Delivery_Time
--  Add restriction on index type if given
    --AND I.index_type = nvl(W_index_type, I.index_type)
    AND I.index_family = nvl(w_index_family, I.index_family)
-- SSI - 06APR2009 - End of modif
    AND   C.active_flag = 'Y';
--
BEGIN
    pgmloc := 7610;
    p_success := FALSE;
--
-- SSI - 06APR2009
-- Possibility to break by Index_Type
--
    V_Oasis_ID    := 'N';
    V_Oasis_ISIN  := 'N';
    V_Oasis_CUSIP := 'N';
    V_Oasis_SEDOL := 'N';
--
-- SSI - 16JUN2009
    V_ID_Sys := G_Id_Sys;
--
    IF V_ID_Sys.EXISTS('Z')
    THEN
       V_Oasis_ISIN := 'Y';
-- SSI - 08APR2011
    END IF;
--  ELSIF V_ID_Sys.EXISTS('C')
    IF V_ID_Sys.EXISTS('C')
    THEN
       V_Oasis_CUSIP := 'Y';
-- SSI - 08APR2011
    END IF;
--  ELSIF V_ID_Sys.EXISTS('S')
    IF V_ID_Sys.EXISTS('S')
    THEN
       V_Oasis_SEDOL := 'Y';
    END IF;
--
    pgmloc := 7620;
    Get_Name_To_Split_File(p_fri_client,
                           p_product_code,
                           p_sub_product_code,
                           'T',   -- component code
                           v_tbl_index_type,
                           p_success
    );
--
--  Add loop to loop through each index type required
    FOR v_ind_type_pos IN 1..v_tbl_index_type.COUNT
    LOOP -- {Index_Type
--
    -- open the file and write the header
       pgmloc := 7630;
       Open_File('w', -- file mode
                 p_calc_date,
                 p_fri_client,
                 p_product_code,
                 p_sub_product_code,
                 FALSE,
                 'FILE',
                 'T', -- component code
                 v_file_name,
                 v_file_handle,
-- SSI - 06APR2009
--            p_success);
                 p_success,
                 v_tbl_index_type(v_ind_type_pos));
-- SSI - 09OCT2008
-- New loop to loop by index
--
-- SSI - 06APR2009
--     FOR Clt_Ind IN Cur_Get_Index
       FOR Clt_Ind IN Cur_Get_Index(v_tbl_index_type(v_ind_type_pos))
       LOOP  -- { Per Index
          pgmloc := 7640;
          v_delivery_date := TO_DATE(TO_CHAR(p_work_date,'DDMONYYYY') ||
                                     Clt_Ind.Delivery_Time, 'DDMONYYYYHH24MI');
-- SSI - 27OCT2008
--        v_prev_delivery_date := Holiday.Get_Actual_Last_Open_Date(p_work_date,
--                                'SUPPLIER',
--                                Clt_Ind.Data_Supplier_Code,
--                                Clt_Ind.Delivery_Time);
          v_prev_delivery_date := Load_Ind_Supplier_Rules.Get_Last_Open_Date
                                    (p_work_date,
                                     Clt_Ind.Data_Supplier_Code,
                                     Clt_Ind.Delivery_Time);
          v_prev_delivery_date := TO_DATE(TO_CHAR(v_prev_delivery_date,'DDMONYYYY') ||
                                          Clt_Ind.Delivery_Time, 'DDMONYYYYHH24MI');
--
    -- loop to get all tickers that changed and write in the file
          pgmloc := 7650;
          FOR reg IN cur_sel_ticker_change(Clt_Ind.Data_Supplier_Code
                                          ,Clt_Ind.Supp_Index_Code
                                          )
          LOOP  -- { New Ticker
             FOR Old_Reg IN Cur_sel_Old_Ticker(Clt_Ind.Data_Supplier_Code
                                              ,Clt_Ind.Supp_Index_Code
                                              ,reg.Supp_Sec_Id)
             LOOP --{ old ticker
                 pgmloc := 7660;
                 V_Symbol := NULL;
                 IF reg.CUSIP <> Old_reg.Cusip
                 THEN
                    pgmloc := 7670;
                    V_Symbol := 'CUSIP';
                    V_Chg_C(Old_Reg.CUSIP) := reg.CUSIP;
                 END IF;
                 IF reg.SEDOL <> Old_reg.SEDOL
                 THEN
                    pgmloc := 7680;
                    V_Symbol := 'SEDOL';
                    V_Chg_S(Old_Reg.SEDOL) := reg.SEDOL;
                 END IF;
                 IF reg.ISIN <> Old_reg.ISIN
                 THEN
                    pgmloc := 7690;
                    V_Symbol := 'ISIN';
                    V_Chg_Z(Old_Reg.ISIN) := reg.ISIN;
                 END IF;
--        -- SSI - 22OCT2008
                 IF reg.ID_S <> Old_reg.ID_S
                 THEN
                    pgmloc := 7700;
                    V_Symbol := 'Additional ID3';
                    V_Chg_ID_S(Old_Reg.ID_S) := reg.ID_S;
                 END IF;
                 IF reg.ID_C <> Old_reg.ID_C
                 THEN
                    pgmloc := 7710;
                    V_Symbol := 'Additional ID2';
                    V_Chg_ID_C(Old_Reg.ID_C) := reg.ID_C;
                 END IF;
                 IF reg.ID_I <> Old_reg.ID_I
                 THEN
                    pgmloc := 7720;
                    V_Symbol := 'Additional ID1';
                    V_Chg_ID_I(Old_Reg.ID_I) := reg.ID_I;
                 END IF;
             END LOOP; -- } Old_Ticker
          END LOOP; -- } New_Ticker
--
--     Supplier ticker changes
--     This is from supplier, not index specified
--
-- SSI - 19MAY2009
-- Add condition to check for supplier ticker changes only if there's no
-- ID system specified. ie. not using OASIS ID
--  SSI - 16JUN2009
--       nloop := 3;
--        IF NVL(G_Id_Sys.COUNT,0) = 0
--        THEN
-- SSI - 16JUN2009 - end of modif.
-- SSI - 25APR2014
--        nloop := 6;
          nloop := 7;
-- SSI - 21MAY2009
          FOR reg IN cur_supp_ticker_change(Clt_Ind.Data_Supplier_Code
                                           ,Clt_Ind.Supp_Index_Code)
          LOOP --{
-- SSI - 21MAY2009
-- Skip if it's part of client required OASIS id system
           IF NOT G_Id_Sys.EXISTS(reg.Identification_System_Code)
           THEN --{
-- SSI - 21MAY2009
             pgmloc := 7730;
             IF reg.Identification_System_Code IN ('C','D')
             THEN
                V_Chg_C(Reg.Old_Ticker) := reg.New_Ticker;
-- SSI - 22OCT2008
-- Report those applied to Identifier
                IF reg.Applied_Flag = 'Y'
                THEN
                   V_Chg_ID_C(Reg.Old_Ticker) := reg.New_Ticker;
                END IF;
             ELSIF reg.Identification_System_Code ='S'
             THEN
                V_Chg_S(Reg.Old_Ticker) := reg.New_Ticker;
-- SSI - 22OCT2008
-- Report those applied to Identifier
                IF reg.Applied_Flag = 'Y'
                THEN
                   V_Chg_ID_S(Reg.Old_Ticker) := reg.New_Ticker;
                END IF;
             ELSIF reg.Identification_System_Code IN ('Z','O')
             THEN
                V_Chg_Z(Reg.Old_Ticker) := reg.New_Ticker;
-- SSI - 22OCT2008
-- Report those applied to Identifier
                IF reg.Applied_Flag = 'Y'
                THEN
                   V_Chg_ID_I(Reg.Old_Ticker) := reg.New_Ticker;
                END IF;
             END IF;
           END IF;  --} if not part of client id sys
--
--         SSI - 25APR2014
           pgmloc := 7740;
           IF reg.Identification_System_Code IN ('L')
           THEN
--            V_Chg_L(Reg.Old_Ticker) := reg.New_Ticker;
              IF reg.Applied_Flag = 'Y'
              THEN
                 V_Chg_ID_L(Reg.Old_Ticker) := reg.New_Ticker;
              END IF;
           END IF;
--         SSI - 25APR2014 - end of modif.
          END LOOP;  --}
-- SSI 16JUN2009
--        END IF;
       END LOOP; -- } For each index
--
--  FOR n IN 1..3
--     FOR n IN 1..6
       FOR n IN 1..nloop
       LOOP  --{
           pgmloc := 7750;
           IF n=1
           THEN
              V_Chg := V_Chg_C;
              V_Symbol := 'CUSIP';
           END IF;
           IF n=2
           THEN
              V_Chg := V_Chg_S;
              V_Symbol := 'SEDOL';
           END IF;
           IF n=3
           THEN
              V_Chg := V_Chg_Z;
              V_Symbol := 'ISIN';
           END IF;
-- SSI - 22OCT2008
           IF n=4
           THEN
              V_Chg := V_Chg_ID_I;
-- SSI - 21MAY2009
--            V_Symbol := 'Additional ID1';
              V_Symbol := 'Statpro ISIN';
           END IF;
           IF n=5
           THEN
              V_Chg := V_Chg_ID_C;
-- SSI - 21MAY2009
--            V_Symbol := 'Additional ID2';
              V_Symbol := 'Statpro Cusip';
           END IF;
           IF n=6
           THEN
              V_Chg := V_Chg_ID_S;
-- SSI - 21MAY2009
--            V_Symbol := 'Additional ID3';
              V_Symbol := 'Statpro Sedol';
           END IF;
-- SSI - 25APR2014
           IF n=7
           THEN
              V_Chg := V_Chg_ID_L;
              V_Symbol := 'SEC_ID';
           END IF;
-- SSI - 25APR2014 -- end of modif
           pgmloc := 7760;
--
           V_Old_Ticker := V_Chg.FIRST;
           FOR i IN 1 .. V_Chg.COUNT
           LOOP
              pgmloc := 7770;
              v_record := to_char(p_work_date, c_date_format) || c_field_sep ||
                         V_Symbol                            || c_field_sep ||
                         V_Old_Ticker                        || c_field_sep ||
                         V_Chg(V_Old_Ticker);
              pgmloc := 7780;
              Load_Supplier_Rules.Write_file(v_file_handle,
                                             v_record,
                                             Pricing_Msgs_Rec,
                                             p_success
                                            );
              IF i <= V_Chg.COUNT
              THEN
              V_Old_Ticker := V_Chg.Next(V_Old_Ticker);
              END IF;
           END LOOP;
       END LOOP;  -- } do it three time for each ID system
--
    -- close file
       pgmloc := 7790;
       Close_File(v_file_name,
                  v_file_handle,
                  'T', -- component code
-- SSI - 08NOV2008
--                NULL,
                  p_work_date,
                  FALSE,
                  'FILE',
                  p_success);
-- SSI - 06APR2009
    END LOOP; --}Index_Type
--
    p_success := TRUE;
    pgmloc := 7800;
END Generate_Ticker_Changes;
--
--
--
--------------------------------------------------------------------------------
-- GENERATE
--------------------------------------------------------------------------------
PROCEDURE Generate(
    p_program_id        IN  Ops_Scripts.Program_Id%TYPE,
    p_Commit_Count      IN  NUMBER,
    p_success           OUT BOOLEAN,
    p_message           OUT VARCHAR2
)
AS
    AFT_ERROR                EXCEPTION;
--
    v_date_per_file         DATE;
    v_work_date             DATE;
    v_work_date_2           DATE;
-- SSI - 19FEB2010
    v_work_date_3           DATE;
    v_last_open_date        DATE;
    v_type_output           VARCHAR2(30);
    v_formatted_date        VARCHAR2(10);
--
    v_file_def              client_file_definitions%ROWTYPE;
    v_rec_currencies        currencies%ROWTYPE;
    v_client_code           clients.code%TYPE;
    v_fri_client            clients.fri_client%TYPE;
    v_product               products.code%TYPE;
    v_sub_product           sub_products.sub_product_code%TYPE;
    v_rerun_date            DATE;
    v_AFT_Flag              VARCHAR2(10);
    v_flag_both             BOOLEAN;
    v_child_pgmid           NUMBER;
--
    V_Last_Prc_Date         DATE;
    V_Start_Date            DATE;
    V_End_Date              DATE;
--
--  SSI - 01OCT2008
    V_Date_Per_File_2       DATE;
    V_Holiday_2             BOOLEAN;
--
-- SSSI - 06APR2009
    V_Delivery_Time         VARCHAR2(4);
    V_Explicit_Period       BOOLEAN;
--
-- APX 21JAN2011 New generate holiday files flag
    v_holiday_file_flag     ind_client_restrictions.gen_holiday_files_flag%TYPE;
    v_gen_file              BOOLEAN;
    v_gen_file_2            BOOLEAN;
--
-- APX 20APR2011 Frequency value validation
    v_frequency             Frequencies.Value%TYPE;
--
-- SSI - 25MAY2011
    v_custom_file_type      VARCHAR2(30);
    v_custom_del_time       VARCHAR2(4);
--
-- SSI - 18MAR2014
    v_max_mths              INTEGER;
    v_min_start_date        DATE;
    -- Get the latest successfull run prior to a given date
    CURSOR Get_Last_Success_Run (I_Date    DATE)
    IS
--VD  09APR2013
--  SELECT Date_Of_Prices
    SELECT MAX(Date_Of_Prices)
    FROM   Pricing_Msgs M
-- SSI - 01OCT2008
-- Join with successfull flag to determine if code is failure
          ,Successfull_Flags F
    WHERE  program_id = p_program_id
-- SSI - 01OCT2008
--  AND    Successfull_Flag = 'Y'
    AND    Successfull_Flag = F.Code
    AND    F.Failure_Flag='N'
--VD  09APR2013  last successfull run date
--    AND    Date_Of_Prices = (SELECT MAX(Date_Of_Prices)
--                             FROM   Pricing_Msgs M1
--                             WHERE M1.Program_Id = M.Program_Id
--                             AND   M1.Date_Of_Prices < I_Date)
    AND M.Date_OF_Prices < I_Date 
--VD  End 09APR2013 
    AND   M.Date_OF_Msg    = (SELECT MAX(M1.Date_Of_Msg)
                             FROM   Pricing_Msgs M1
                             WHERE M1.Program_Id = M.Program_Id
                             AND   M1.Date_Of_Prices = M.Date_Of_Prices);
--
    CURSOR Get_File_Def
    IS
    SELECT *
    FROM client_file_definitions
    WHERE fri_client = v_fri_client
    AND   product_code = v_product
    AND   sub_product_code = v_sub_product
    AND   active_flag = 'Y';
--
-- SSI - 17OCT2008
--  New cursor to get data supplier_code
--
    CURSOR Get_Data_Supplier
    IS
    Select Distinct Data_Supplier_Code
    FROM   Client_Index_Suppliers
    WHERE  Fri_Client = V_Fri_Client
    AND   product_code = v_product
    AND   sub_product_code = v_sub_product
    AND   active_flag = 'Y';
--
--  The sub product code is the client code of which the format layout
--  is to be used
--  SSI - 07JUL2011 - Rewrite cursor
--  CURSOR Get_Fmt_Fri_Client
--  IS
--  SELECT Fri_Client
--  FROM   Clients
--  WHERE  Code = V_Sub_Product;
--  Get custom client format is applicable, else the format as of the sub product
    CURSOR Get_Fmt_Fri_Client (I_fri_client  Clients.fri_client%TYPE
                              ,I_Product     Products.Code%TYPE
                              ,I_Sub_Product Sub_Products.Sub_Product_Code%TYPE)
    IS
    SELECT Fri_Client
    FROM   Clients C
    WHERE  Code = I_Sub_Product
    AND    NOT EXISTS (SELECT 1
                       FROM Client_Format_Layouts L
                       WHERE L.Fri_Client = I_Fri_Client
                       AND   L.Product_Code = I_Product)
    UNION
    SELECT I_fri_client
    FROM   Dual
    WHERE  EXISTS (SELECT 1
                   FROM Client_Format_Layouts L
                   WHERE L.Fri_Client = I_Fri_Client
                   AND   L.Product_Code = I_Product);
--
-- SSI - 17OCT2008 -- End of Modif.
--
    CURSOR cur_currencies
    IS
    SELECT *
    FROM currencies;
--
--  SSI - 06APR2009
    CURSOR Invalid_Classification
    IS
-- ACB - 04JUL2009
    --SELECT I.index_type,COUNT(distinct R.holiday_class_Code)
    SELECT I.index_family, COUNT(distinct R.holiday_class_Code)
    FROM supp_index I
        ,Client_Index_Suppliers C
        ,Data_Supply_schedule R
    WHERE C.Data_Supplier_Code = G_Data_Supplier_Code
    AND   C.Product_Code = V_Product
    AND   C.Sub_Product_Code = V_Sub_Product
    AND   C.Supp_Index_Code = I.Supp_Index_Code
    AND   C.Active_Flag = 'Y'
--  SSI - 19JUN2015
    AND   C.fri_client = v_fri_Client
    AND   I.Data_Supplier_Code =  C.Data_Supplier_Code
    AND   I.Delivery_Time = C.Delivery_Time
    AND   R.Data_Supplier_Code = I.Data_SUpplier_Code
    AND   I.Delivery_Time = R.Delivery_Time
-- ACB - 04JUL2009
    --GROUP BY I.Index_Type
    GROUP BY I.index_family
    HAVING COUNT(distinct R.holiday_class_Code) > 1;
--
    CURSOR Get_Holiday_Class
    IS
-- ACB - 04JUL2009
    --SELECT distinct R.holiday_class_Code
    SELECT distinct R.holiday_class_Code, I.index_family
    FROM supp_index I
        ,Client_Index_Suppliers C
        ,Data_Supply_schedule R
    WHERE C.Data_Supplier_Code = G_Data_Supplier_Code
    AND   C.Product_Code = V_Product
    AND   C.Sub_Product_Code = V_Sub_Product
    AND   C.Supp_Index_Code = I.Supp_Index_Code
--  SSI - 19JUN2015
    AND   C.fri_client = v_fri_Client
    AND   C.Active_Flag = 'Y'
    AND   R.Data_Supplier_Code = C.Data_Supplier_Code
    AND   R.Data_Supplier_Code = I.Data_SUpplier_Code
    AND   I.Delivery_Time = R.Delivery_Time;
--
    CURSOR Get_Delivery
    IS
    SELECT Delivery_Time
    FROM   Data_Supply_Schedule
    WHERE  Data_Supplier_Code = G_Data_Supplier_Code
    AND    Holiday_Class_Code = G_Holiday_Class;
-- SSI - 17JUN2009
    CURSOR Get_Prc_Msgs (prog_id       NUMBER,
                         i_price_date  DATE)
    IS
    SELECT *
    FROM pricing_msgs P
    WHERE program_id = prog_id
    AND date_of_prices = i_price_date
    AND successfull_flag = 'N'
    AND terminal_id = Constants.Get_Terminal_Id
    AND date_of_msg = (SELECT max(P1.date_of_msg)
                       FROM pricing_msgs P1
                       WHERE P.program_id     = P1.program_id
                       AND P.date_of_prices = P1.date_of_prices
                       AND P.terminal_id    = P1.terminal_id
                      );
-- SSI - 17JUN2009 - end of modif
--
    Invalid_Classification_Err      EXCEPTION;
--  SSI - 06APR2009 - End of modif.
BEGIN
    -- Initialize some variables
    pgmloc                := 7810;
    G_System_Date         := sysdate;
    v_message_code        := NULL;
    v_message_val1        := NULL;
    v_message_val2        := NULL;
    v_message_val3        := NULL;
    v_oasis_message       := NULL;
    v_num_files           := 0;
    g_site_date           := Constants.Get_Site_date;
    g_program_id          := p_program_id;
--  SSI - 16APR2009
    g_delimiter_type      := NULL;
    v_client_output_file.delete;
-- SSI - 17JUN2009
    G_On_Server           := Constants.On_Server;
-- SSI - 17JUN2009 - end of modif
    -----------------------------------------------------
    -- Get PRODUCT_RUNNING_PARMS data for this program_id
    -----------------------------------------------------
    pgmloc := 7820;
    Extract_Info.Get_Running_Info(p_program_id,
                                  v_client_code,
                                  v_fri_client,
                                  v_product,
                                  v_sub_product,
                                  v_rerun_date,
                                  v_aft_flag,
                                  Pricing_Msgs_Rec,
                                  p_success
                                );
    IF (NOT p_success) THEN
        v_message_code := NULL;
        RAISE LOGICAL_ERROR;
    END IF;
--
--  SSI - 29APR2009
--  IF running other than site_date, Set rerun date is not set
    IF V_Rerun_Date <> G_Site_Date
    THEN
       UPDATE Product_Running_Parms
       SET Rerun_Date = V_Rerun_Date
       WHERE Program_Id = P_Program_ID
       AND   Rerun_Date IS NULL;
       --
       Commit;
    END IF;
--  SSI - 29APR2009 - end of modif.
--  g_data_supplier_code := v_client_code;
-- SSI - 17OCT2008
--  g_data_supplier_code := v_sub_product;
--
-- SSI - 17JUN2009
    IF G_On_Server THEN  --{
        pgmloc := 7830;
        OPEN Get_Prc_Msgs (P_Program_Id,
                           V_Rerun_Date);
        FETCH Get_Prc_Msgs INTO Pricing_Msgs_Rec;
        IF (Get_Prc_Msgs%Found) THEN  -- {
            pgmloc := 7840;
            G_System_Date := Pricing_Msgs_Rec.Date_Of_Msg;
            CLOSE Get_Prc_Msgs;
            GOTO Skip_Init_Msgs;
        END IF;   --}
        CLOSE Get_Prc_Msgs;
    END IF; --}
-- SSI - 17JUN2009 - end of modif
--
    pgmloc := 7850;
    Load_Supplier_Rules.Initialize_Prc_Msgs(p_program_id,
                                            g_system_date,
                                            v_rerun_date,
                                            pricing_msgs_rec,
                                            p_success
                                           );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
<<Skip_Init_Msgs>>
    pgmloc := 7860;
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
    Pricing_Msgs_Rec.Msg_Text         := 'Generate Client Index files';
    Pricing_Msgs_Rec.Msg_Text_2       := 'Program in progress.....';
    Initialize_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--  Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                      G_System_Date,
--                                      Pricing_Msgs_Rec,
--                                      p_success
--                                     );
--  IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                           , G_System_Date
                                           , C_Upd_Break_Pts
                                           , Pricing_Msgs_rec
                                           );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
   Commit;
--
-- SSI - 17OCT2008
    pgmloc := 7870;
    OPEN Get_Data_Supplier;
    FETCH Get_Data_Supplier INTO G_Data_Supplier_Code;
    IF Get_Data_Supplier%NOTFOUND THEN
        v_message_code := '060';
        RAISE LOGICAL_ERROR;
    END IF;
    CLOSE Get_Data_Supplier;
--
    pgmloc := 7880;
--  SSI - 07JUL2011
--  OPEN Get_Fmt_Fri_Client;
    OPEN Get_Fmt_Fri_Client(v_fri_client
                           ,v_product
                           ,v_sub_product);
    FETCH Get_Fmt_Fri_Client INTO G_Fmt_Fri_Client;
    IF Get_Fmt_Fri_Client%NOTFOUND THEN
        v_message_code := '070';
        RAISE LOGICAL_ERROR;
    END IF;
    CLOSE Get_Fmt_Fri_Client;
-- SSI - 17OCT2008 - End of modif.
--
    ---------------------------------
    -- Get and open the log file name
    ---------------------------------
    pgmloc := 7890;
    Product_Component.Get_Dated_File_Name(v_fri_client,
                                          v_product,
                                          v_sub_product,
                                          'L',
                                          G_System_Date,
                                          v_log_file_name,
                                          p_success,
                                          Pricing_Msgs_Rec.err_text
                                         );
    IF (NOT p_success) THEN
        v_message_code := NULL;
        RAISE LOGICAL_ERROR;
    END IF;
    pgmloc := 7900;
    Load_Supplier_Rules.Open_File(v_log_file_name,
                                  v_dir_name,
                                  'W',
                                  pricing_msgs_rec,
                                  v_log_file,
                                  p_success
                                 );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    Load_Supplier_Rules.Write_file(v_log_file,
                                   c_proc_generate || ' started at '
                                   || to_char(G_System_Date,
                                                    'dd-MON-yyyy hh24:mi:ss'),
                                   Pricing_Msgs_Rec,
                                   p_success
                                  );
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Program_id ' || to_char(p_program_id),
                                   Pricing_Msgs_Rec,
                                   p_success
                                  );
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Client ' || v_fri_client || '(' ||
                                        v_client_code || ')',
                                   Pricing_Msgs_Rec,
                                   p_success
                                  );
--
    pgmloc := 7910;
    g_delivery_time := Scripts.Get_Arg_Value(p_program_id, 'DELIVERY_TIME');
    v_type_output := Scripts.Get_Arg_Value(p_program_id, 'TYPE_OUTPUT');
    v_custom_file_type := Scripts.Get_Arg_Value(p_program_id,'CUS_FILE_TYPE');
    v_custom_Del_Time  := Scripts.Get_Arg_Value(p_program_id,'CUS_DEL_TIME');
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Type Output: ' || v_type_output,
                                   Pricing_Msgs_Rec,
                                   p_success
                                  );
--
    pgmloc := 7920;
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Site date ' ||
                                       to_char(G_Site_Date,'dd-MON-yyyy'),
                                   Pricing_Msgs_Rec,
                                   p_success
                                  );
--
    -------------------------------------------------
    -- Verify if the file type generated is supported
    -------------------------------------------------
    pgmloc := 7930;
    IF ( v_type_output not in (c_type_out_table,
                               c_type_out_file,
                               c_type_out_both)
       )
    THEN
        v_message_code := '030';
        v_message_val1 := v_type_output;
        RAISE LOGICAL_ERROR;
    END IF;
--
    v_flag_both := FALSE;
    IF (v_type_output = c_type_out_both) THEN
        v_flag_both := TRUE;
    END IF;
--
    ----------------------
    -- Get file definition
    ----------------------
    G_File_Def := G_File_Def_Init;
    pgmloc := 7940;
    OPEN Get_File_Def;
    LOOP
       pgmloc := 7950;
       FETCH Get_File_Def INTO v_file_def;
       pgmloc := 7960;
       EXIT WHEN Get_File_Def%NOTFOUND;
       -- record (based on table client_file_definitions) indexed by component_code
       G_File_Def(v_file_def.component_code) := v_file_def;
    END LOOP;
    CLOSE Get_File_Def;
--
--  SSI - 16APR2009
--  Get delimiter type
--
    Define_Delimiter(v_fri_Client
                    ,v_product);
--  SSI - 16APR2009 -- end of modif.
--
    ----------------------
    -- Get currencies
    ----------------------
    g_tbl_currencies := g_tbl_currencies_init;
    pgmloc := 7970;
    OPEN cur_currencies;
    LOOP
       pgmloc := 7980;
       FETCH cur_currencies INTO v_rec_currencies;
       pgmloc := 7990;
       EXIT WHEN cur_currencies%NOTFOUND;
       g_tbl_currencies(v_rec_currencies.currency)  := v_rec_currencies;
       g_tbl_iso_cur(v_rec_currencies.iso_currency) := v_rec_currencies;
    END LOOP;
    CLOSE cur_currencies;
--
--  Load client ID System Code
    FOR Rec IN Get_Clt_ID_Sys(V_Fri_Client)
    LOOP
--     v_id_sys_code := Rec.Identification_System_Code;
       G_ID_Sys(Rec.Identification_System_Code) := Rec.Priority_Order;
    END LOOP;
    ------------------------------------
    --  Define the period of extraction
    ------------------------------------
    --  Find the last run date of given run date
    Pgmloc  := 8000;
    OPEN Get_Last_Success_Run(v_rerun_date);
    Pgmloc  := 8010;
    FETCH Get_Last_Success_Run INTO v_last_prc_date;
    Pgmloc  := 8020;
--  SSI - The cursor may return a null date
--  IF Get_Last_Success_Run%NOTFOUND THEN
    IF Get_Last_Success_Run%NOTFOUND OR 
       v_last_prc_date IS NULL
    THEN
       v_last_prc_date := v_rerun_date - 1;
    END IF;
--
--  APX 20Apr2011 For Montly (freq < 365) extract only for the current day
    pgmloc  := 8030;
--    v_start_date  := nvl(Scripts.Get_Break_Arg_Value(p_program_id, 'DATE1'),
--                         v_last_prc_date + 1);
--
    v_end_date    := nvl(Scripts.Get_Break_Arg_Value(p_program_id, 'DATE2'),
                         v_rerun_date);
--
--  APX 20Apr2011 For Montly (freq < 365) extract only for the current day
    v_frequency := NULL;
    SELECT P.Frequency_Value
    INTO v_frequency
    FROM   PRODUCT_RUNNING_PARMS P
    WHERE  P.Program_Id = p_program_id;
--
-- SSI - 16DEC2013
--  IF v_frequency < 365 THEN
    IF v_frequency < 365 and
       g_data_supplier_code='MSCT' THEN
-- SSI - 16DEC2013
        v_start_date  := v_end_date;
    ELSE
        v_start_date  := nvl(Scripts.Get_Break_Arg_Value(p_program_id, 'DATE1'),
                             v_last_prc_date + 1);
    END IF;
--  APX 20Apr2011 - End of modification
--
--  SSI - 18MAR2014
    v_max_mths := Scripts.Get_Arg_Value(p_program_id, 'MAX_ROLLING_MTHS');
    IF v_max_mths IS NOT NULL
    THEN
       v_min_start_date := ADD_MONTHS(TRUNC(Constants.Get_Site_Date), -1*v_max_mths);
       IF v_min_start_date > v_start_date
       THEN
          v_message_code := '100';
          v_message_val1 := TO_CHAR(v_min_start_date,'DDMONYYYY');
          RAISE LOGICAL_ERROR;
       END IF;
    END IF;
--  SSI - 18MAR2014 - end of modif.
--
--
--  SSI - Explicit period given?
    V_Explicit_Period := FALSE;
    IF Scripts.Get_Break_Arg_Value(p_program_id, 'DATE1') IS NOT NULL OR
       Scripts.Get_Break_Arg_Value(p_program_id, 'DATE2') IS NOT NULL
    THEN
       V_Explicit_Period := TRUE;
    END IF;
--
--
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'Requested index between '
                                        || to_char(v_start_date,'dd-MON-yyyy')
                                        || ' and '
                                        || to_char(v_end_date,'dd-MON-yyyy'),
                                   Pricing_Msgs_Rec,
                                   p_success
                                  );
--
    pgmloc := 8040;
    Pricing_Msgs_Rec.Msg_Text := 'Generate Client Index files (type ' ||
        v_type_output ||
        ') for ' || to_char(v_start_date, 'ddMONyyyy') ||
        ' - ' || to_char(v_end_date, 'ddMONyyyy');
-- SSI - 20MAR2009
--  Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                      G_System_Date,
--                                      Pricing_Msgs_Rec,
--                                      p_success
--                                     );
--  IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
    Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                           , G_System_Date
                                           , C_Upd_Break_Pts
                                           , Pricing_Msgs_rec
                                           );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
   Commit;
    --
    --  Clean the output file table
    --
    Pgmloc  := 8050;
    Clean_Client_Output (v_fri_client
                        ,v_product
                        ,v_sub_product
                        ,NULL
                        ,v_rerun_date
                        ,v_start_date
                        ,v_end_date
                        ,p_Success
                        ,p_Message);
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
--
    -------------------------------------------
    -- Generating all files, one file per day
    -------------------------------------------
    pgmloc := 8060;
-- SSI - 01OCT2008
--  IF G_File_Def('B').Date_Keyword_Code = 'TD'
    IF G_File_Def.EXISTS('B')
    THEN
       IF G_File_Def('B').Date_Keyword_Code = 'TD'
       THEN
          G_T_Service :=  TRUE;
       ELSE
          G_T_Service := FALSE;
       END IF;
    END IF;
-- SSI - 01OCT2008
--
-- SSI - 06APR2009
-- Check if indices are classified correctly per holiday class
    pgmloc := 8070;
    FOR V_Invalid IN Invalid_Classification
    LOOP
      V_Message_Code := '080';
      RAISE Logical_Error;
    END LOOP;
--
--  Extract by index family (main loop)
--
    pgmloc := 8080;
    OPEN Get_Holiday_Class;
    LOOP
    pgmloc := 8090;
-- ACB - 04JUL2009
   --FETCH Get_Holiday_Class INTO G_Holiday_Class;
   FETCH Get_Holiday_Class INTO G_Holiday_Class, G_index_family;
-- ACB - 04JUL2009 - End of modif.
    EXIT WHEN Get_Holiday_Class%NOTFOUND;
    pgmloc := 8100;
    OPEN Get_delivery;
    pgmloc := 8110;
    FETCH Get_Delivery INTO V_Delivery_Time;
    pgmloc := 8120;
    CLOSE Get_Delivery;
    g_delivery_Time := V_Delivery_Time;
    pgmloc := 8130;
--
--  SSI - 06APR2009
--  For non explicit period, when rerun date is a holiday since the last price
--  date run, then skip the extraction of this holiday class, As this should be
--  extracted only on the following business day ....
    IF NOT V_Explicit_Period AND
       Load_Ind_Supplier_Rules.IS_It_Open (V_End_date,
                                           g_data_supplier_code,
                                           g_delivery_time)='N' AND
       Load_Ind_Supplier_Rules.Get_Next_Open_Date (v_last_prc_date,
                                           g_data_supplier_code,
                                           g_delivery_time)=V_End_Date
    THEN
       GOTO Next_Hol_Class;
    END IF;
--
    v_date_per_file := v_start_date;
-- SSI - 27OCT2008
--  v_work_date := Holiday.Get_Actual_Last_Open_Date(v_date_per_file,
--                                                   'SUPPLIER',
--                                                   g_data_supplier_code,
--                                                   g_delivery_time);
    v_work_date := Load_Ind_Supplier_Rules.Get_Last_Open_Date
                                                    (v_date_per_file,
                                                     g_data_supplier_code,
                                                     g_delivery_time);
    v_last_open_date := v_work_date;
--  SSI - 06APr2009
--  Readjust 1st file date to be last open date + 1
    v_date_per_file := v_last_open_date + 1;
--
    v_date_per_file_2 := v_date_per_file - 1;
--  v_work_date_2 := Holiday.Get_Actual_Next_Open_Date(v_work_date,
-- SSI - 27OCT2008
--  v_work_date_2 := Holiday.Get_Actual_Next_Open_Date(v_date_per_file_2,
--                                                     'SUPPLIER',
--                                                     g_data_supplier_code,
--                                                     g_delivery_time);
    v_work_date_2 := Load_Ind_Supplier_Rules.Get_Next_Open_Date
                                                      (v_date_per_file_2,
                                                       g_data_supplier_code,
                                                       g_delivery_time);
-- SSI - 06APR2009
--  IF v_work_date_2 > v_end_date
--  THEN
--     GOTO Next_Hol_Class;
--  END IF;
--
-- APX - 21JAN2011 - Read Gen_Holiday_Files flag
    BEGIN
        SELECT gen_holiday_files_flag
          INTO v_holiday_file_flag
        FROM ind_client_restrictions
        WHERE fri_client = v_fri_client;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_holiday_file_flag := 'Y';
    END;
--
    pgmloc := 8140;
--  SSI - 06APR2009
--  WHILE (v_date_per_file <= v_end_date AND v_date_per_file <= G_Site_Date)
    WHILE v_date_per_file <= v_end_date  AND
--        v_date_per_file <= G_Site_Date AND
          v_work_date_2   <= v_end_date
    LOOP
-- SSI - 27OCT2008
--      IF ( to_char(v_date_per_file, 'D') IN (1, 7)
--          OR Holiday.Is_It_Open(v_date_per_file,
--                                'SUPPLIER',
--                                g_data_supplier_code,
--                                g_delivery_time) IS NOT NULL
--         )
        IF Load_Ind_Supplier_Rules.Is_It_Open(v_date_per_file,
                                              g_data_supplier_code,
                                              g_delivery_time)='N'
        THEN
           g_flag_is_holiday := TRUE;
        ELSE
           v_last_open_date := v_work_date;
           v_work_date := v_date_per_file;
--         v_work_date_2 := v_date_per_file;
           g_flag_is_holiday := FALSE;
        END IF;
        --
--
        IF G_T_Service
        THEN
--     Servie @t
           V_Work_Date_2 := V_Work_Date;
           V_holiday_2   := g_flag_is_holiday;
           v_date_per_file_2 := v_date_per_file;
        ELSE
--     Servie @t+1
           v_date_per_file_2 := v_date_per_file-1;
-- SSI - 27OCT2008
--         IF ( to_char(v_date_per_file_2, 'D') IN (1, 7)
--             OR Holiday.Is_It_Open(v_date_per_file_2,
--                                   'SUPPLIER',
--                                   g_data_supplier_code,
--                                   g_delivery_time) IS NOT NULL
--            )
           IF Load_Ind_Supplier_Rules.Is_It_Open(v_date_per_file_2,
                                                 g_data_supplier_code,
                                                 g_delivery_time)='N'
           THEN
              v_holiday_2 := TRUE;
           ELSE
              v_holiday_2 := FALSE;
-- SSI - 27OCT2008
--            v_work_date_2 := Holiday.Get_Actual_Next_Open_Date(v_date_per_File_2,
--                                                        'SUPPLIER',
--                                                        g_data_supplier_code,
--                                                        g_delivery_time);
              v_work_date_2 := Load_Ind_Supplier_Rules.Get_Next_Open_Date
                                                         (v_date_per_File_2,
                                                          g_data_supplier_code,
                                                          g_delivery_time);
           END IF;
        END IF;
-- APX/SSI 21JAN2011 If it is holiday evaluate generate holidays files flag
        v_gen_file  := TRUE;
        IF g_flag_is_holiday THEN
           v_gen_file:= (v_holiday_file_flag = 'Y');
        dbms_output.put_line(' --- *****1 ----' || v_date_per_file || ' is a holiday, file will produce: ' || v_holiday_file_flag);
        END IF;
--
        v_gen_file_2:= TRUE;
        IF v_holiday_2 THEN
           v_gen_file_2:= (v_holiday_file_flag = 'Y');
        dbms_output.put_line(' --- *****2 ----' || v_date_per_file_2 || ' is a holiday, file will produce: ' || v_holiday_file_flag);
        END IF;
-- APX/SSI 21JAN2011 - end of modif
--
        dbms_output.put_line('**** date       ' || v_date_per_file);
        dbms_output.put_line('**** file date  ' || v_date_per_file_2);
        dbms_output.put_line('**** work date  ' || v_work_date);
        dbms_output.put_line('**** work date2 ' || v_work_date_2);
--
        v_output_file_series_char := NULL;
        v_output_file_exch_char   := NULL;
--
        Pricing_Msgs_rec.remarks := 'Generating data from ' ||
            to_char(v_date_per_file,'dd-MON-yyyy');
        --
        Load_Supplier_Rules.Write_file(v_log_file,
                              '  => ' || to_char(v_date_per_file,'dd-MON-yyyy'),
                              Pricing_Msgs_Rec,
                              p_success
        );
        -- Open the Series Characteristics file
-- APX 21JAN2011 if it's holiday, the files are generated only if the client require them
--      IF G_File_Def.EXISTS('H') THEN
        IF G_File_Def.EXISTS('H') AND v_gen_file THEN
           pgmloc := 8150;
           g_tbl_index_type.DELETE;
           Get_Name_To_Split_File(v_fri_client,
                                   v_product,
                                   v_sub_product,
                                   'H',
                                   g_tbl_index_type,
                                   p_success
            );
--         dbms_output.put_line(g_tbl_index_type.COUNT || ' index types to process');
           FOR v_ind_type_pos IN 1..g_tbl_index_type.COUNT
           LOOP
               g_ind_type_pos := v_ind_type_pos;
               g_format_layout.DELETE;
               Open_File('w', -- file mode
                         v_date_per_file,
                         v_fri_client,
                         v_product,
                         v_sub_product,
                         v_flag_both,
                         v_type_output,
                         'H', -- component code
                         v_output_file_series_char_name,
                         v_output_file_series_char,
                         p_success,
                         g_tbl_index_type(g_ind_type_pos)
               );
-- ADD ACB 07MAY2009
                g_index_type := g_tbl_index_type(v_ind_type_pos);
        --END IF;
--              dbms_output.put_line(v_output_file_series_char_name);
               IF G_File_Def.EXISTS('V') THEN
                   pgmloc := 8160;
                   g_format_layout.DELETE;
                   Gen_Series_Value_File(v_date_per_file, -- file date name
                                         v_work_date, -- date used in the select
                                         v_fri_client,
                                         v_client_code,
                                         v_product,
                                         v_sub_product,
                                         v_flag_both,
                                         v_type_output,
                                         p_success
                                        );
                   Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--                 Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                                     G_System_Date,
--                                                     Pricing_Msgs_Rec,
--                                                     p_success
--                                                    );
                   Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                          , G_System_Date
                                                          , C_Upd_Break_Pts
                                                          , Pricing_Msgs_rec
                                                          );
-- SSI - 20MAR2009 - End of modif.
--             --SSI- 22JUN2010
                   Commit;
                END IF;
                IF G_File_Def.EXISTS('M') THEN
                   pgmloc := 8170;
                   g_format_layout.DELETE;
                   Gen_Market_Cap_File(v_date_per_file, -- file date name
                                       v_work_date, -- date used in the select
                                       v_fri_client,
                                       v_client_code,
                                       v_product,
                                       v_sub_product,
                                       v_flag_both,
                                       v_type_output,
                                       p_success
                                      );
                   Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--                 Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                                     G_System_Date,
--                                                     Pricing_Msgs_Rec,
--                                                     p_success
--                                                    );
                   Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                          , G_System_Date
                                                          , C_Upd_Break_Pts
                                                          , Pricing_Msgs_rec
                                                          );
-- SSI - 20MAR2009 - End of modif.
--             -- SSI- 22JUN2010
                  Commit;
                END IF;
                -- Close the Series Characteristics file
                IF G_File_Def.EXISTS('H') THEN
                   pgmloc := 8180;
                   g_format_layout.DELETE;
                   Close_File(v_output_file_series_char_name,
                              v_output_file_series_char,
                              'H', -- component code
                              v_work_date,
                              v_flag_both,
                              v_type_output,
                              p_success
                   );
                   Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--                 Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                                     G_System_Date,
--                                                     Pricing_Msgs_Rec,
--                                                     p_success
--                                                   );
                   Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                          , G_System_Date
                                                          , C_Upd_Break_Pts
                                                          , Pricing_Msgs_rec
                                                          );
-- SSI - 20MAR2009 - End of modif.
--             -- SSI- 22JUN2010
                  Commit;
                END IF;
            END LOOP;
        END IF;
        -- Exchange rate file
-- APX 21JAN2011 if it's holiday, the files are generated only if the client require them  AND v_gen_file
--      IF G_File_Def.EXISTS('E') THEN
        IF G_File_Def.EXISTS('E') AND v_gen_file THEN
           pgmloc := 8190;
           g_format_layout.DELETE;
           Gen_Exch_Rate_Series_File(v_date_per_file, -- file date name
                                     v_work_date, -- date used in the select
                                     v_fri_client,
                                     v_client_code,
                                     v_product,
                                     v_sub_product,
                                     v_flag_both,
                                     v_type_output,
                                     p_success
                                    );
           Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--         Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                             G_System_Date,
--                                             Pricing_Msgs_Rec,
--                                             p_success
--                                            );
           Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                  , G_System_Date
                                                  , C_Upd_Break_Pts
                                                  , Pricing_Msgs_rec
                                                  );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
           Commit;
        END IF;
-- APX 21JAN2011 if it's holiday, the files are generated only if the client require them
--        IF G_File_Def.EXISTS('R') THEN
        IF G_File_Def.EXISTS('R') AND v_gen_file THEN
           pgmloc := 8200;
           g_format_layout.DELETE;
           Gen_Exch_Rate_Char_File(v_date_per_file, -- file date name
                                   v_work_date, -- date used in the select
                                   v_fri_client,
                                   v_client_code,
                                   v_product,
                                   v_sub_product,
                                   v_flag_both,
                                   v_type_output,
                                   p_success
                                    );
           Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--         Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                             G_System_Date,
--                                             Pricing_Msgs_Rec,
--                                             p_success
--                                            );
           Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                  , G_System_Date
                                                  , C_Upd_Break_Pts
                                                  , Pricing_Msgs_rec
                                                  );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
           Commit;
        END IF;
        -- Security file @T
-- SSI - 06APR2009
--      IF G_File_Def.EXISTS('B') THEN
-- APX 21JAN2011 if it's holiday, the files are generated only if the client require them
--        IF G_File_Def.EXISTS('B') AND
--           v_work_date_2 <=v_end_date
        IF G_File_Def.EXISTS('B') AND v_gen_file_2 AND
           v_work_date_2 <= v_end_date
        THEN
           pgmloc := 8210;
           g_format_layout.DELETE;
--         Gen_Securities_File(v_date_per_file - 1, -- file date name
           Gen_Securities_File(v_date_per_file,         -- file date name
                               v_work_date_2, -- date used in the select
                               v_fri_client,
                               v_client_code,
                               v_product,
                               v_sub_product,
                               v_flag_both,
--                          -- SSI - 01OCT2008
                               v_holiday_2,
                               v_type_output,
                               p_success
                              );
           Set_Counter_Prc_Msgs(p_success);
-- SSI - 20MAR2009
--         Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                             G_System_Date,
--                                             Pricing_Msgs_Rec,
--                                             p_success
--                                            );
           Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                  , G_System_Date
                                                  , C_Upd_Break_Pts
                                                  , Pricing_Msgs_rec
                                                  );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
           Commit;
        END IF;
-- APX 21JAN2011 if it's holiday, the files are generated only if the client require them
--      IF G_File_Def.EXISTS('D') THEN
        IF G_File_Def.EXISTS('D') AND v_gen_file THEN
--      SSI - 02DEC2015
--      Skip if it is a t-1 service, and it is a holiday date and next open date passes the end date required
           IF NOT G_T_Service AND
              g_flag_is_holiday AND
              v_work_date_2 > v_end_date
           THEN
              GOTO skip_d;
           END IF;
-- SSI - 02DEC2015 - end of modif
           pgmloc := 8220;
           g_format_layout.DELETE;
           Gen_Securities_File2(v_date_per_file,         -- file date name
                               v_work_date, -- date used in the select
                               v_fri_client,
                               v_client_code,
                               v_product,
                               v_sub_product,
                               v_flag_both,
                               g_flag_is_holiday ,
                               v_type_output,
                               p_success
                              );
           Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--         Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                             G_System_Date,
--                                             Pricing_Msgs_Rec,
--                                             p_success
--                                            );
           Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                  , G_System_Date
                                                  , C_Upd_Break_Pts
                                                  , Pricing_Msgs_rec
                                                  );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
-- SSI 02DEC2015
<<skip_d>>
           Commit;
        END IF;
        IF G_File_Def.EXISTS('C') THEN
           pgmloc := 8230;
           g_format_layout.DELETE;
           -- If it is holiday, the file will not be generated
           IF NOT (g_flag_is_holiday) THEN
               Gen_Securities_Char_File(v_date_per_file, -- file date name
                                        v_work_date, -- date used in the select
--                                   -- SSI - 01OCT2009
                                        v_last_open_Date,
                                        v_fri_client,
                                        v_client_code,
                                        v_product,
                                        v_sub_product,
                                        v_flag_both,
                                        v_type_output,
                                        p_success
                                       );
           END IF;
        END IF;
-- SSI - 26MAY2011
        IF G_File_Def.EXISTS('U') THEN
           pgmloc := 8240;
           g_format_layout.DELETE;
           -- If it is holiday, the file will not be generated
           IF NOT (g_flag_is_holiday) THEN
               Gen_Securities_Char_File_2(v_date_per_file, -- file date name
                                          v_work_date, -- date used in the select
                                          v_fri_client,
                                          v_client_code,
                                          v_product,
                                          v_sub_product,
                                          v_flag_both,
                                          v_type_output,
                                          v_Custom_File_type,
                                          v_Custom_Del_Time,
                                          p_success
                                         );
           END IF;
        END IF;
-- SSI - 26MAY2011 - end of modif.
        IF G_File_Def.EXISTS('T') THEN
           pgmloc := 8250;
           -- If it is weekend or holiday, the file will not be generated
           IF NOT (g_flag_is_holiday) THEN
               Generate_Ticker_Changes(v_date_per_file,
                                       v_work_date,
                                       v_last_open_date,
                                       v_fri_client,
                                       v_product,
                                       v_sub_product,
                                       p_success
                                      );
           END IF;
        END IF;
        -- Risk file
-- APX 21JAN2011 if it's holiday, the files are generated only if the client require them
--      IF G_File_Def.EXISTS('K') THEN
        IF G_File_Def.EXISTS('K') AND v_gen_file THEN
           pgmloc := 8260;
           g_format_layout.DELETE;
           Gen_Risk_File(v_date_per_file, -- file date name
                         v_work_date, -- date used in the select
                         v_fri_client,
                         v_client_code,
                         v_product,
                         v_sub_product,
                         v_flag_both,
                         v_type_output,
                         p_success);
           Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--         Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                             G_System_Date,
--                                             Pricing_Msgs_Rec,
--                                             p_success
--                                            );
           Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                  , G_System_Date
                                                  , C_Upd_Break_Pts
                                                  , Pricing_Msgs_rec
                                                  );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
           Commit;
        END IF;
        -- Risk file Equities
-- APX 21JAN2011 if it's holiday, the files are generated only if the client require them
--      IF G_File_Def.EXISTS('N') THEN
        IF G_File_Def.EXISTS('N') AND v_gen_file THEN
           pgmloc := 8270;
           g_format_layout.DELETE;
-- SSI - 22FEB2010
           V_Work_Date_3 := v_work_date;
           IF NOT G_T_Service
           THEN
              V_Work_Date_3 := v_work_date_2;
           END IF;
-- SSI - 22FEB2010 - end of modif.
           Gen_Risk_File_Equities(v_date_per_file, -- file date name
--                          -- SSI - 22FEB2010
--                                v_work_date, -- date used in the select
                                  v_Work_Date_3, -- date used in the select
                                  v_fri_client,
                                  v_client_code,
                                  v_product,
                                  v_sub_product,
                                  v_flag_both,
                                  v_type_output,
                                  p_success);
           Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--         Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                             G_System_Date,
--                                             Pricing_Msgs_Rec,
--                                             p_success
--                                            );
           Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                                  , G_System_Date
                                                  , C_Upd_Break_Pts
                                                  , Pricing_Msgs_rec
                                                  );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
           Commit;
        END IF;
--
        v_date_per_file := v_date_per_file + 1;
-- SSI - 01-OCT-2008
--      v_work_date_2 := v_work_date;
        Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--      Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                          G_System_Date,
--                                          Pricing_Msgs_Rec,
--                                          p_success
--                                         );
        Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                               , G_System_Date
                                               , C_Upd_Break_Pts
                                               , Pricing_Msgs_rec
                                               );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
        Commit;
    END LOOP; -- loop per data
<<Next_Hol_CLass>>
    NULL;
-- SSI - 06APR2009
    END LOOP; -- per holiday_Class
    CLOSE Get_Holiday_Class;
--
    -----------------------------------------------------------------
    -- Insert all files generated into the table CLIENT_OUTPUT_FILES
    -----------------------------------------------------------------
    pgmloc := 8280;
    IF (v_num_files <> 0) THEN
        Load_Supplier_Rules.Write_file(v_log_file,
                          'Insert the ' || v_num_files ||
                          ' file(s) generated in the table CLIENT_OUTPUT_FILES',
                          Pricing_Msgs_Rec,
                          p_success
                         );
        Insert_Client_Output_Files(v_fri_client,
                                   v_product,
                                   v_sub_product,
                                   v_rerun_date,
                                   v_client_output_file,
                                   p_success
                                  );
        IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
-- SSI - 22SEP2011
    ELSE
-- SSI - 03NOV2011
--      V_message_Code := 60;
        V_message_Code := '090';
        RAISE Logical_Error;
-- SSI - 22SEP2011 - end of modif
    END IF;
    --------------------------
    -- Close the log file
    --------------------------
    pgmloc := 8290;
    Load_Supplier_Rules.Write_file(v_log_file,
                                   'End at '
                                  || to_char(sysdate, 'dd-MON-yyyy hh24:mi:ss'),
                                   Pricing_Msgs_Rec,
                                   p_success
                                  );
    Load_Supplier_Rules.File_Close(v_log_file,
                                   Pricing_Msgs_Rec,
                                   p_success
                                  );
    IF (NOT p_success) THEN RAISE LOGICAL_ERROR; END IF;
--
    --------------------------
    -- Compressed and AFT
    --------------------------
-- Remove check if AFT_FLAG='Y'. Check if AFT is done in the procedure
--  IF v_aft_flag = 'Y' THEN
       v_child_pgmid := p_program_id * 100 + 01;
       Index_Reports.Send_Compressed_Files(v_child_pgmid
                                          ,v_rerun_date
                                          ,Pricing_Msgs_Rec
                                          ,p_success
                                          ,p_message);
       IF (NOT p_success) THEN RAISE AFT_ERROR; END IF;
--  END IF;
--
    -------------------------
    -- End of the program
    -------------------------
--  SSI - 20MAR2009
--  UPDATE product_running_parms
--  SET last_run_date  = sysdate,
--      rerun_date = NULL
--  WHERE program_id = p_program_id;
-- SSI - 20MAR2009 - End of modif.
--
    Pricing_Msgs_Rec.Msg_Text := 'Generate Client Index files - ' ||
        'output type ' || v_type_output;
    Pricing_Msgs_Rec.Msg_Text_2 := 'Successfully generated';
    Pricing_Msgs_rec.remarks    := 'Data between '
                                        || to_char(v_start_date,'dd-MON-yyyy')
                                        || ' and '
                                        || to_char(v_end_date,'dd-MON-yyyy')
                                        || ' generated.';
    Set_Counter_Prc_Msgs(p_success);
-- SSI - 17JUN2009
--  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
    IF NOT G_On_Server THEN  --{
       Pricing_Msgs_Rec.Successfull_Flag := 'Y';
    END IF;
-- SSI - 17JUN2009 - end of modif
    -- Final update in Pricing_Msgs record
    pgmloc := 8300;
--  SSI - 20MAR2009
--  Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                      G_System_Date,
--                                      Pricing_Msgs_Rec,
--                                      p_success
--                                     );
-- SSI test
--  Pricing_Msgs_Rec.Remarks := 'SSI version';
    Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                           , G_System_Date
                                           , C_Upd_Break_Pts
                                           , Pricing_Msgs_rec
                                           );
-- SSI - 20MAR2009 - End of modif.
--  SSI - 06APR2009 - commit
    commit;
    p_success  := TRUE;
    p_message := Pricing_Msgs_Rec.Msg_Text_2;
    utl_file.fclose_all;
--
--  SSI - 20MAR2009
--  Reset only rerun date and after update of Pricing_Msgs
--  SSI - 19JUN2009
--  Add condition to reset rerun date only if not running from server
    IF NOT G_On_Server THEN
       UPDATE product_running_parms
       SET rerun_date = NULL
       WHERE program_id = p_program_id;
   END IF;
-- SSI - 21MAR2009 - End of modif.
-- SSI - 06APR2009 - commit
    commit;
--  SSI - 22APR2014
--
    UPDATE Break_points
    SET   Status='DONE'
         ,information = TO_CHAR(SYSDATE,'DD-MON-YYYYY HH24:MI') ||  ' # #DATE1=#DATE2='
    WHERE Program_ID = p_program_id;
--
--  SSI - 22APR-2014 -- end of modif
    pgmloc := 8310;
EXCEPTION
    WHEN AFT_ERROR THEN
        Manage_Messages.Get_Oasis_Message(c_Oasis_Message || '040',
                                         'N',
                                          V_Child_Pgmid,
                                          pricing_msgs_rec.err_text);
        Pricing_Msgs_Rec.Msg_Text := 'Generate Output index Files - ' ||
            'output type ' || v_type_output;
        Pricing_Msgs_Rec.Msg_Text_2 := 'Successfully generated';
        Pricing_Msgs_rec.remarks    := 'Data between '
                                        || to_char(v_start_date,'dd-MON-yyyy')
                                        || ' and '
                                        || to_char(v_end_date,'dd-MON-yyyy')
                                        || ' generated.';
        Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--      Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                          G_System_Date,
--                                          Pricing_Msgs_Rec,
--                                          p_success);
        Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                               , G_System_Date
                                               , C_Upd_Break_Pts
                                               , Pricing_Msgs_rec
                                               );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
        Commit;
    WHEN LOGICAL_ERROR THEN
        IF (v_message_code IS NOT NULL) THEN
            Manage_Messages.Get_Oasis_Message(c_Oasis_Message || v_message_code,
                                             'N',
                                              v_message_val1,
                                              v_message_val2,
                                              v_message_val3,
                                              pricing_msgs_rec.err_text);
        END IF;
        pricing_msgs_rec.Msg_Text         := c_proc_generate || ' aborted!' ||
            ' Output type - ' || v_type_output;
        pricing_msgs_rec.Msg_Text_2       := 'Error in generating the file.';
        Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--      Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                          G_System_Date,
--                                          Pricing_Msgs_Rec,
--                                          p_success);
        Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                               , G_System_Date
                                               , C_Upd_Break_Pts
                                               , Pricing_Msgs_rec
                                               );
-- SSI - 20MAR2009 - End of modif.
-- SSI- 22JUN2010
        Commit;
        IF (utl_file.is_open(v_log_file) ) THEN
            load_supplier_Rules.Write_file(v_log_file,
                                           pricing_msgs_rec.err_text,
                                           Pricing_Msgs_Rec,
                                           p_success
                                          );
            load_supplier_Rules.Write_file(v_log_file,
                                           'pgmloc => ' || pgmloc,
                                           Pricing_Msgs_Rec,
                                           p_success
                                          );
        END IF;
        p_message := pricing_msgs_rec.err_text;
        p_success := FALSE;
        utl_file.fclose_all;
    WHEN OTHERS THEN
        Manage_Messages.Get_Oasis_Message(C_GEN_Message || '100',
                                          'N',
                                          pgmloc,
                                          SUBSTR(SQLERRM, 1, 150),
                                          c_proc_generate,
                                          v_oasis_message);
        pricing_msgs_rec.Msg_Text         := c_proc_generate || ' aborted!' ||
            ' Output type - ' || v_type_output;
        pricing_msgs_rec.Msg_Text_2       := 'Error in generating the file.';
        pricing_msgs_rec.Successfull_Flag := 'N';
        pricing_msgs_rec.err_text         := v_oasis_message;
        Set_Counter_Prc_Msgs(p_success);
--  SSI - 20MAR2009
--      Load_Supplier_Rules.Update_Prc_Msgs(p_program_id,
--                                          G_System_Date,
--                                          Pricing_Msgs_Rec,
--                                          p_success);
        Global_File_Updates.Update_Pricing_Msgs( P_Program_ID
                                               , G_System_Date
                                               , C_Upd_Break_Pts
                                               , Pricing_Msgs_rec
                                               );
-- SSI - 20MAR2009 - End of modif.
--
-- SSI - 06APR2009
        Commit;
--
        IF (utl_file.is_open(v_log_file) ) THEN
            load_supplier_Rules.Write_file(v_log_file,
                                           v_oasis_message,
                                           Pricing_Msgs_Rec,
                                           p_success
                                          );
            load_supplier_Rules.Write_file(v_log_file,
                                           'pgmloc => ' || pgmloc,
                                           Pricing_Msgs_Rec,
                                           p_success
                                          );
        END IF;
        p_message := v_oasis_message;
        p_success := FALSE;
        utl_file.fclose_all;
END Generate;
--
END Client_Index_Files;
/
