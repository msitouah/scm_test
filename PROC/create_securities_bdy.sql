CREATE OR REPLACE PACKAGE BODY &&owner..Create_Securities IS
--
--
-- New Package Body to Create Foreign Securities
-- Requested by Alfred
-- Richard (28 Sep 2006)
--
-- Conversion from foreign_securities to create_securities
-- It is now creating foreign bonds and North American Municipal Bonds.
-- JSL - December 2006 and January 2007.
--
-- JSL - 08jan2007 - Start adding the treatment of North American mutual funds
-- JSL - 10jan2007 - Added tracking of progress.
--                 - Added further header constants
--                 - Corrected the tracking of AFT error.
-- JSL - 15jan2007 - Continue adding the treatment of North American mutual
--                   funds
--                 - Tickers list may be on many records.  Keep the previous
--                   identifier (CUSIP) and the creation of the previous
--                   security
-- JSL - 26jan2007 - Adding the creation of foreign stocks
--                 - Verify if the tickers are to be changed soon or were
--                   changed lately
--                 - Can create the issuer if identified as 'JUNK' or 'TEMP'
--                   or NULL.
-- JSL - 15feb2007 - Correction : Issue should be inserted into Securities.
--                 - Dynamically set the default based on security type.
-- JSL - 27feb2007 - Have an absolute field maximum length C_Max_Field_Length
--                 - Default par currency is done after many validation only.
-- JSL - 09mar2007 - Corrections to Mutual Funds.  The previous entry could
--                   have failed, when that happens, ignore the previous.
--                 - Verify if the given ticker already exists.  Compare with
--                   the current fri_ticker.
-- JSL - 13mar2007 - Corrections for foreign stocks.
-- JSL - 15mar2007 - Corrections for foreign stocks.  The latest version was
--                   not the same on Linux as on the Database...
-- JSL - 21mar2007 - Accept that the Ticker Currency be set to "NULL", eg with
--                   the identification FSERV.
-- JSL - 27mar2007 - Added trim of some weird characters send over by the user
--                   when she copy from ??? to the input file.
-- JSL - 30avr2007 - Do not even try to modify the CUSIP.
-- JSL - 07mai2007 - Added the treatment to add the indices.
--                 - Does have two sources for one exchange ticker.
--                 - Modularisation of the various validations.
-- JSL - 16jul2007 - Added trading statuses for the two exchanges of indices.
-- JSL - 17jul2007 - Correction to Canadian Municipal bonds and redemption.
-- JSL - 13nov2007 - Make sure that par and redemption currency are the same.
-- JSL - 30nov2007 - When verifying that the security have a purchase fund
--                   schedule, we must verify in that table, not another one...
-- JSL - 14dec2007 - Added Issuer French name, Security French Long Name,
--                   Issue Nation(?) and Isid.
-- JSL - 10avr2008 - Considers CUSIP/CINS and ISIN/OISIN as synonyms when
--                   verifying the existence of the security
-- JSL - 26mai2008 - Even if considered as synonym, do not insert the OISIN.
--                 - Insert the CINS if the cusip start with a letter
-- JSL - 10sep2008 - The Issuer name should always be in upper case.
-- JSL - 26sep2008 - Added EDI Identification.
-- JSL - 14oct2008 - Issuer nationality should only be a warning, not an error.
-- JSL - 23oct2008 - Do not add cusip in security if it is a CINS.  Let the
--                   trigger take care of it.
-- JSL - 24nov2008 - If this is a CINS, set the cusip to a TEMP
-- JSL - 26nov2008 - If this is a CINS, set the cusip to a TEMP (Again!)
-- JSL - 10dec2008 - FS Generic issuers are all used up.  Change to FT.
-- JSL - 01ave2009 - Change call from AFT to TMS to transfer a file.
-- JSL - 14sep2009 - The OISIN does not exists anymore.  It is an ISIN.  Do the
--                   check on the ISIN, not the OISIN.
--                 - Remove some useless coding.
-- JSL - 15sep2009 - Add the field iso_nation_code to tickers as this is now a
--                   requirement for sedol.
--                 - Changed lines are not all marked to facilitate the reading
--                   of the code.
-- JSL - 12oct2009 - Change in perpetual validation : A maturity schedule is
--                   mandatory, but perpetual is optional, the there is no
--                   maturity!
-- JSL - 22oct2009 - Allow securities to have the same description as an
--                   existing one.  This is only for some user and when a
--                   parameter is set in ops_scripts.  Used by DBA only!
-- JSL - 16avr2010 - Added extra long security and issuer name
--                 - Added treatment of issuer_xref identifier
--                 - Removed comments prior to 2009
-- JSL - 30aug2011 - Issuer MF has reach its capacity, change it to MG.
-- mlz - 19dec2011 - do not force issuer_name, nor any security name to uppercase
-- JSL - 07mar2012 - Relax restriction on security with same description.
--                   Any user can do it.
-- JSL - 01mai2012 - Accepts to create saving bonds by setting the constant
--                   price flag to Y when the security type is SB.
-- mlz - 06aug2012 - enlarged fields E_Value_0x to avoid truncation
-- mlz - 13sep2012 - Added parameter: DNBO_ISSUER_NAME (do not block on ...)
--                 -   to allow mismatch on issuer_name and issuer code
-- JSL - 14nov2012 - OASIS-377
--                 - When we already have the error message, do not replace it!
-- JSL - 01aug2013 - OASIS-2400
--                 - Add issuer code to the list of securities having been created
--                 - double space that list.
--                 - Add day count method and Inisys day count method
--                 - Many other changes : ticker list, schedules, trading status, etc.
-- JSL - 24sep2013 - OASIS-2400
--                 - Move set_default to the last field, not individual fields, once is enough
--                 - Add robustness to set_default
--                 - Add call schedule to the schedules
--                 - Issuer code is 12 characters, not 6!
-- JSL - 29jan2014 - OASIS-2398
--                 - Add remarks field.
-- JSL - 05feb2014 - OASIS-2923
--                 - Add US Municipal bonds treatment.
--                 - OASIS-2398
--                 - Correction to mandatory fields, if the field is null, do not go
--                   through the field loop.
-- JSL - 18feb2014 - OASIS-2923
--                 - Continue add US Municipal bonds treatment after promotion of 2398.
-- JSL - 23apr2014 - OASIS-3711
--                 - Success was not set to true while exiting gracefully from add_schedule.
-- JSL - 10may2014 - OASIS-2398
--                 - Add accruals start date processing in procedure add schedule.
--                 - Add default values for call_calc_parms in add call schedule.
--                 - Accept ISIN, CUSIP, CINS, SEDOL or FRI_ID as unique identifier
--                   when creating the schedules
-- JSL - 18jun2014 - OASIS-3835
--                 - Accept to create matured bonds, with confirmation that it is matured.
-- JSL - 25Sep2015 - OASIS-5311
--                 - Remove old comments that are adding nothing to the comprehension.
--                 - Add the updating of securities in clients universe.
-- JSL - 15Dec2015 - OASIS-6076
--                 - On auto format, accept parameters to render fields mandatory.
--
----------------------------------------------------------------------
--
--
G_Dir_Separator    VARCHAR2(1)                  := Constants.Get_Dir_Seperator;
G_Program_ID       Ops_Scripts.Program_ID%TYPE  ;
G_Site_Date        DATE                         := Constants.Get_Site_Date;
G_System_Date      DATE ;
G_User             User_List.User_ID%TYPE       := Constants.Get_User_ID;
--
G_Tracing          BOOLEAN                      := FALSE;
G_Auto             BOOLEAN                      := FALSE;
C_Auto             VARCHAR2(32)                 := 'AUTO';
--
G_Message_Code     Oasis_Messages.Code%TYPE:='PROC-CREATE_SECURITIES-';
G_FMessage_Code    Oasis_Messages.Code%TYPE;
G_Del_Flag         Oasis_Messages.Delete_Flag%TYPE := 'N';
--
-- Constants
--
C_Footer           VARCHAR2(8)                  := 'FOOTER';
C_Header           VARCHAR2(8)                  := 'CUSIP';
G_Footer           VARCHAR2(8)                  := 'FOOTER';
G_Header           VARCHAR2(8)                  := 'CUSIP';
-- The field for issuer is "issuer code"
C_Issuer_Tab       VARCHAR2(16)                 := 'ISSUER CODE'||CHR(9);
C_Indent           VARCHAR2(8)                  := '   '   ;
C_Tab              VARCHAR2(1)                  := CHR(9);
C_#Tab             VARCHAR2(2)                  := '#'||CHR(9);
C_Date_Format      VARCHAR2(16)                 := 'DD-MON-YYYY';
C_Date_Length      INTEGER                      := 11;
C_Max_Field_Length INTEGER                      := 128;
--
C_ISID_IDS         Issuer_IDS.Code%TYPE := 'ISID';
--
C_Trim_Value       VARCHAR2(01)                 := CHR(160); -- 0xA0
--
-- Ajout - JSL - 15 decembre 2015
G_All_Parms                  scripts.parms_table_type;
-- Fin d'ajout - JSL - 15 decembre 2015
--
--
TYPE OneField IS RECORD (
     F_Name           VARCHAR2(32)
   , F_Max_Length     INTEGER
   , F_Numeric        BOOLEAN
   , F_Format         VARCHAR2(20)
   , F_Mandatory      BOOLEAN
   );
G_Number_of_Fields   INTEGER := 100;
TYPE FieldList IS VARRAY(100) OF OneField;
GT_Fields            FieldList := FieldList();
--
Type One_Identifier IS RECORD (
     Valid          BOOLEAN
   , Found          BOOLEAN
   , Pending        BOOLEAN
   , T_Rec          Tickers%ROWTYPE
   );
TYPE Many_Identifiers
    IS TABLE OF One_Identifier
       INDEX BY PLS_INTEGER;
IDS   PLS_INTEGER;
--
TYPE Sched IS RECORD
   ( Fri_Ticker                     Securities.Fri_Ticker%TYPE
   , Unique_Id                      VARCHAR2(12)
   , Fri_Id                         VARCHAR2(6)
   , Schedule                       VARCHAR2(30)
   , Accruals_Start_Date            DATE
   , Accruals_Calc_Formula_Code     Accruals_Calc_Formulas.Code%TYPE
   , Annual_Income_Rate             NUMBER
   , Base_Bond                      Pricing_Notes.Base_Bond%TYPE
   , Base_Ticker                    Pricing_Notes.Base_Ticker%TYPE
   , Call_Type_Code                 Call_Types.Code%TYPE
   , Class_Code                     Category_Classes.Class_Code%TYPE
   , Class_Type                     Category_Classes.Category_Code%TYPE
   , Currency_Code                  Currencies.Currency%TYPE
   , Date_Of_Status                 DATE
   , Day_Count                      Day_Count_Methods.Mnemonic%TYPE
   , Denominations                  INTEGER
   , Ending_Date                    DATE
   , Final_Reset_Date               DATE
   , First_Payment_Date             DATE
   , Fixed_Premium                  NUMBER
   , Frequency_Value                Frequencies.Value%TYPE
   , Identification_System_Code     Identification_Systems.Code%TYPE
   , Identification_System_Mnemonic Identification_Systems.Mnemonic%TYPE
   , Income_Currency                Currencies.Currency%TYPE
   , INISYS_Day_Count               Day_Count_Methods.Mnemonic%TYPE
   , ISO_Nation                     Nations.Iso_Nation%TYPE
   , Issue_Date                     DATE
   , Market_Measurement_Days        INTEGER
   , Market_Measurement_Factor      NUMBER
   , Market_Measurement_Method_Code Market_Measurement_Methods.Code%TYPE
   , Market_Measurement_Type_Code   Market_Measurement_Types.Code%TYPE
   , Maturity_Date                  DATE
   , Maturity_Type_Code             Maturity_Types.Code%TYPE
   , Measured_Unique_Id             VARCHAR2(12)
   , Measured_Fri_Id                VARCHAR2(6)
   , Measured_Fri_Ticker            Securities.Fri_Ticker%TYPE
   , Offering_Date                  DATE
   , Option_Date                    Pricing_Notes.Option_Date%TYPE
   , Option_Price                   Pricing_Notes.Option_Price%TYPE
   , Option_Type                    Pricing_Notes.Option_Type%TYPE
   , Optional_Income_Currency       Currencies.Currency%TYPE
   , Price                          NUMBER
   , Pricing_Currency               Currencies.Currency%TYPE
   , Pricing_Note                   Pricing_Notes.Pricing_Note%TYPE
   , Pricing_Source_Code            Sources.Code%TYPE
   , Private_Placement_Flag         Yes_No.Code%TYPE
   , Quantity                       INTEGER
   , Rater                          Rating_Codes.Rater%TYPE
   , Rating_Code                    Rating_Codes.Code%TYPE
   , Rating_Date                    DATE
   , Redemption_Amount              NUMBER
   , Redemption_Currency            Currencies.Currency%TYPE
   , Remarks                        VARCHAR2(256)
   , Scheduled_Event_Flag           Yes_No.Code%TYPE
   , Sinking_Fund_Flag              Yes_No.Code%TYPE
   , Source_Code                    Sources.Code%TYPE
   , Spread_Note                    Pricing_Notes.Spread%TYPE
   , Spread                         NUMBER
   , Starting_Date                  DATE
   , Trading_Status_Mnemonic        Trading_Status.Mnemonic%TYPE
   , Trading_Status_Code            Trading_Status.Code%TYPE
   , Ticker                         Tickers.Ticker%TYPE
   , UL_Fri_Ticker                  Securities.Fri_ticker%TYPE
   , UL_Unique_Id                   VARCHAR2(12)
   , UL_Fri_Id                      VARCHAR2(6)
   );
--
-- Ajout - JSL - 25 septembre 2015
TYPE Univ IS RECORD
   ( Fri_Ticker                     Securities.Fri_Ticker%TYPE
   , Fri_Id                         VARCHAR2(6)
   , Client_Code                    Clients.Code%TYPE
   , Fri_Client                     Clients.Fri_Client%TYPE
   , Client_Security_Id             Clients_Univ.Client_Security_Id%TYPE
   , RIC                            Tickers.Ticker%TYPE
   , Source_Code                    Sources.Code%TYPE
   , Price_Currency                 Currencies.Currency%TYPE
   );
-- Fin d'ajout - JSL - 25 septembre 2015
--
C_ID_CUSIP           VARCHAR2(1)     := 'C';
C_ID_CINS            VARCHAR2(1)     := 'D';
C_ID_EXCH            VARCHAR2(1)     := 'E';
C_ID_ISIN            VARCHAR2(1)     := 'Z';
C_ID_OISIN           VARCHAR2(1)     := 'O';
C_ID_SEDOL           VARCHAR2(1)     := 'S';
C_ID_RIC             VARCHAR2(1)     := 'R';
C_FRI_ID             VARCHAR2(1)     := 'X';
C_ID_EDI             Identification_Systems.Code%TYPE := 'P';
C_ID_Pool            Identification_Systems.Code%TYPE := '#';
--
C_NO_SOURCE          VARCHAR2(4)     := 'NA';
--
C_Ignore             VARCHAR2(32)    := 'Ignore';
C_Issuer_Code        VARCHAR2(32)    := 'Issuer Code';
C_Issuer_Name        VARCHAR2(32)    := 'Issuer Name';
C_Issuer_Name_F      VARCHAR2(32)    := 'Issuer Name French';
C_Issuer_Name_2      VARCHAR2(32)    := 'Issuer Long Name 2';
C_Issuer_Name_2F     VARCHAR2(32)    := 'Issuer Long Name 2 French';
C_ISID               VARCHAR2(32)    := 'ISID Code';
C_Issue_Nation       VARCHAR2(32)    := 'Issue Nation';
C_Issuer_Nation      VARCHAR2(32)    := 'Issuer Nation';
C_Issuer_Type        VARCHAR2(32)    := 'Issuer Type';
C_Issuer_Sub_Type    VARCHAR2(32)    := 'Issuer Sub Type';
C_Issuer_Xref_Type   VARCHAR2(32)    := 'Issuer XREF Type';
C_Issuer_Xref_Code   VARCHAR2(32)    := 'Issuer XREF Code';
C_Coupon             VARCHAR2(32)    := 'Coupon';
C_Maturity_Date      VARCHAR2(32)    := 'Maturity Date';
C_Maturity_Type      VARCHAR2(32)    := 'Maturity Type Code';
C_Market_Code        VARCHAR2(32)    := 'Market Code';
C_Sec_Long_Name      VARCHAR2(32)    := 'Security Long Name';
C_Sec_Long_Name_F    VARCHAR2(32)    := 'Security Long Name French';
C_Sec_Long_Name_2    VARCHAR2(32)    := 'Security Long Name 2';
C_Sec_Long_Name_2F   VARCHAR2(32)    := 'Security Long Name 2 French';
C_ISO_Ccy_Code       VARCHAR2(32)    := 'ISO Currency Code';
C_Income_Type        VARCHAR2(32)    := 'Income Type';
C_Income_Freq        VARCHAR2(32)    := 'Income Frequency';
C_Pricing_Note       VARCHAR2(32)    := 'Pricing Note';
C_Security_Type      VARCHAR2(32)    := 'Security Type';
C_Security_Sub_Type  VARCHAR2(32)    := 'Security Sub Type';
C_Call_Flag          VARCHAR2(32)    := 'Call Flag';
C_Floating_Flag      VARCHAR2(32)    := 'Floating Flag';
C_Convertible_Flag   VARCHAR2(32)    := 'Convertible Flag';
C_Perpetual_Flag     VARCHAR2(32)    := 'Perpetual Flag';
C_Intangibility_Flag VARCHAR2(32)    := 'Intangibility Flag';
C_E_R_Flag           VARCHAR2(32)    := 'Extendible/Retractable Flag';
C_CUSIP              VARCHAR2(32)    := 'Cusip';
C_ISIN               VARCHAR2(32)    := 'ISIN';
C_ISIN_Currency      VARCHAR2(32)    := 'ISIN Currency Code';
C_OISIN              VARCHAR2(32)    := 'Official ISIN';
C_OISIN_Currency     VARCHAR2(32)    := 'Official ISIN Currency Code';
C_SEDOL              VARCHAR2(32)    := 'Sedol Symbol';
C_SEDOL_Currency     VARCHAR2(32)    := 'Sedol Currency Code';
C_SEDOL_ISO_Nation   VARCHAR2(32)    := 'Sedol ISO Nation Code';
C_SEDOL_Nation       VARCHAR2(32)    := 'Sedol Nation Code';
C_CINS_Symbol        VARCHAR2(32)    := 'CINS Symbol';
C_CINS_Currency      VARCHAR2(32)    := 'CINS Currency Code';
C_RIC_Symbol         VARCHAR2(32)    := 'Ric Symbol';
C_RIC_Source         VARCHAR2(32)    := 'Ric Source Code';
C_RIC_Currency       VARCHAR2(32)    := 'Ric Currency Code';
C_Exch_Symbol        VARCHAR2(32)    := 'Exch Symbol';
C_Exch_Source_1      VARCHAR2(32)    := 'Exch Primary Source Code';
C_Exch_Source_2      VARCHAR2(32)    := 'Exch Secondary Source Code';
C_First_Pay_Date     VARCHAR2(32)    := 'First Payment Date';
C_Creation_Date      VARCHAR2(32)    := 'Creation Date';
C_Issue_Offer_Date   VARCHAR2(32)    := 'Issue Offer Date';
C_Accrual_Start_Date VARCHAR2(32)    := 'Accruals Start Date';
C_Trading_Currency   VARCHAR2(32)    := 'Trading Currency';
C_Data_Source_Code   VARCHAR2(32)    := 'Pricing Source Code';
C_Data_Supp_Code     VARCHAR2(32)    := 'Data Supplier Code';
C_Data_Deliv_Time    VARCHAR2(32)    := 'Data Delivery time';
C_Data_Currency      VARCHAR2(32)    := 'Pricing Currency Code';
C_Priv_Plac_Flag     VARCHAR2(32)    := 'Private Placement Flag';
C_Sched_Event_Flag   VARCHAR2(32)    := 'Scheduled Event Flag';
C_Serie              VARCHAR2(32)    := 'Security Serie';
C_Class              VARCHAR2(32)    := 'Security Class';
C_Redemption         VARCHAR2(32)    := 'Redemption Value';
C_Redemption_Currency VARCHAR2(32)   := 'Redemption Currency';
C_Trading_Source     VARCHAR2(32)    := 'Trading Source Code';
C_Ticker_Mnemonic    VARCHAR2(32)    := 'Ticker Mnemonic';
C_Ticker_Source      VARCHAR2(32)    := 'Ticker Source';
C_Ticker_Currency    VARCHAR2(32)    := 'Ticker currency';
C_Ticker_Symbol      VARCHAR2(32)    := 'Ticker Symbol';
C_Par_Currency       VARCHAR2(32)    := 'Par Currency';
C_Par_Value          VARCHAR2(32)    := 'Par Value';
C_Income_Currency    VARCHAR2(32)    := 'Income Currency';
C_EDI_Symbol         VARCHAR2(32)    := 'EDI Security Id';
C_Pool_Number        VARCHAR2(32)    := 'Pool Number';
C_Pool_No            VARCHAR2(32)    := 'Pool#';
--
C_Day_Count                   VARCHAR2(32)    := 'Day Count';
C_Acc_Form_Code               VARCHAR2(32)    := 'Accrual Formula Code';
C_INISYS_Day_Count            VARCHAR2(32)    := 'INISYS Day Count';
C_Inflation_Linked            VARCHAR2(32)    := 'Inflation Linked Flag';
C_FRI_Symbol                  VARCHAR2(32)    := 'Statpro ID';
C_Schedule                    VARCHAR2(32)    := 'Schedule Code';
C_Annual_Income_Rate          VARCHAR2(32)    := 'Annual Income Rate';
C_Base_Bond                   VARCHAR2(32)    := 'Base Bond';
C_Base_Ticker                 VARCHAR2(32)    := 'Base Ticker';
C_Call_Type_Code              VARCHAR2(32)    := 'Call Type Code';
C_Class_Code                  VARCHAR2(32)    := 'Class Code';
C_Class_Type                  VARCHAR2(32)    := 'Class Type';
C_Currency_Code               VARCHAR2(32)    := 'Currency';
C_Date_Of_Status              VARCHAR2(32)    := 'Date Of Status';
C_Denominations               VARCHAR2(32)    := 'Denominations';
C_Ending_Date                 VARCHAR2(32)    := 'Ending Date';
C_Final_Reset_Date            VARCHAR2(32)    := 'Final Reset Date';
C_First_Payment_Date          VARCHAR2(32)    := 'First Payment Date';
C_Fixed_Premium               VARCHAR2(32)    := 'Fixed Premium';
C_Frequency_Value             VARCHAR2(32)    := 'Frequency Value';
C_Issue_Date                  VARCHAR2(32)    := 'Issue Date';
C_Market_Measurement_Days     VARCHAR2(32)    := 'Market Measurement Days';
C_Market_Measurement_Factor   VARCHAR2(32)    := 'Market Measurement Factor';
C_Market_Measurement_Method   VARCHAR2(32)    := 'Market Measurement Method';
C_Market_Measurement_Type     VARCHAR2(32)    := 'Market Measurement Type';
C_Measured_Statpro_Id         VARCHAR2(32)    := 'Measured Statpro Id';
C_Offering_Date               VARCHAR2(32)    := 'Offering Date';
C_Option_Date                 VARCHAR2(32)    := 'Option Date';
C_Option_Price                VARCHAR2(32)    := 'Option Price';
C_Option_Type                 VARCHAR2(32)    := 'Option Type';
C_Optional_Income_Currency    VARCHAR2(32)    := 'Optional Income Currency';
C_Price                       VARCHAR2(32)    := 'Price';
C_Pricing_Currency            VARCHAR2(32)    := 'Pricing Currency';
C_Pricing_Source_Code         VARCHAR2(32)    := 'Pricing Source Code';
C_Private_Placement_Flag      VARCHAR2(32)    := 'Private Placement Flag';
C_Quantity                    VARCHAR2(32)    := 'Quantity';
C_Rater                       VARCHAR2(32)    := 'Rater';
C_Rating_Code                 VARCHAR2(32)    := 'Rating';
C_Rating_Date                 VARCHAR2(32)    := 'Rating Date';
C_Remarks                     VARCHAR2(32)    := 'Remarks';
C_Scheduled_Event_Flag        VARCHAR2(32)    := 'Scheduled Event Flag';
C_Sinking_Fund_Flag           VARCHAR2(32)    := 'Sinking Fund Flag';
C_Source_Code                 VARCHAR2(32)    := 'Source Code';
C_Spread_Note                 VARCHAR2(32)    := 'Spread Note';
C_Spread                      VARCHAR2(32)    := 'Spread';
C_Starting_Date               VARCHAR2(32)    := 'Starting Date';
C_Trading_Status              VARCHAR2(32)    := 'Trading Status Code';
C_Symbol                      VARCHAR2(32)    := 'Symbol';
C_UL_Fri_Symbol               VARCHAR2(32)    := 'UL Statpro Id';
--
C_US_Issuer_Type              VARCHAR2(32)    := 'US Issuer Type';
C_Type_I                      VARCHAR2(32)    := 'Type_I';
C_Type_II                     VARCHAR2(32)    := 'Type_II';
C_Type_III                    VARCHAR2(32)    := 'Type_III';
C_Taxation                    VARCHAR2(32)    := 'Taxation';
C_Insurance                   VARCHAR2(32)    := 'Insurance';
C_Refinancing                 VARCHAR2(32)    := 'Refinancing';
C_State                       VARCHAR2(32)    := 'State';
C_Matured_Bond       CONSTANT VARCHAR2(32)    := 'Matured';  -- Header
C_Matured            CONSTANT VARCHAR2(32)    := 'MATURED';  -- Value expected
--
C_Index_Category            VARCHAR2(32) := 'Index Category';
C_Index_Base_Date           VARCHAR2(32) := 'Index Base Date';
C_Index_Base_Value          VARCHAR2(32) := 'Index Base Value';
C_Index_Rebalance_Frequency VARCHAR2(32) := 'Index Rebalance Frequency';
C_Index_Description         VARCHAR2(32) := 'Index Description';
--
-- Ajout - JSL - 25 septembre 2015
C_Client_Code        CONSTANT VARCHAR2(32)    := 'Client Code';
C_Client_Security_ID CONSTANT VARCHAR2(32)    := 'Client security ID';
-- Fin d'ajout - JSL - 25 septembre 2015
--
C_Error              VARCHAR2(16)    := 'ERROR';
C_Inserted           VARCHAR2(16)    := 'Inserted';
C_Modified           VARCHAR2(16)    := 'Modified';
C_NULL               VARCHAR2(16)    := NULL;
--
-- Fields defaulted
--
G_Acc_Calc_Formula   Accruals_Calc_Formulas.Code%TYPE;
G_Acc_Freq_Value     Frequencies.Value%TYPE;
G_Book_Based_Flag    Yes_NO.Code%TYPE;
G_Const_Prc_Flag     Yes_NO.Code%TYPE;
G_Day_Count          Day_Count_Methods.Code%TYPE;
G_Gen_Pay_Flag       Yes_NO.Code%TYPE;
G_Income_Type        Securities.Income_Type_Code%TYPE;
G_Intangibility_Flag Yes_NO.Code%TYPE;
G_Mat_Date_Method    Mat_Date_Sel_Methods.Code%TYPE;
G_Maturity_Type      Maturity_Types.Code%TYPE;
G_Call_Flag          Yes_NO.Code%TYPE;
G_Conv_Flag          Yes_NO.Code%TYPE;
G_E_R_Flag           Yes_NO.Code%TYPE;
G_Perp_Flag          Yes_NO.Code%TYPE;
G_MTN_Flag           Yes_NO.Code%TYPE;
G_Par_Value          Securities.Par_Value%TYPE;
G_Par_Fri_Cy         Currencies.Currency%TYPE;
G_Pur_Fund_Flag      Yes_No.Code%TYPE;
G_Ratings_Flag       Yes_No.Code%TYPE;
G_Redemption_Amount  Maturity_Schedule.Redemption_Amount%TYPE;
G_Redemption_Currency Maturity_Schedule.Redemption_Currency%TYPE;
G_SAC_Changed_Flag   Yes_No.Code%TYPE;
G_Splits_Flag        Yes_No.Code%TYPE;
G_Sink_Fund_Flag     Yes_No.Code%TYPE;
G_Trd_Status         Trading_Status.Code%TYPE;
G_User_ID            Internal_Jobbers.Code%TYPE := Constants.Get_User_ID;
G_Freq_Value         Frequencies.Value%TYPE;
G_Sec_Type           Security_Types.Code%TYPE;
G_Sec_SubType        Security_Sub_Types.Security_sub_type_Code%TYPE;
G_Series             Securities.Series%TYPE;
G_Class              Securities.Class%TYPE;
G_More_Header        VARCHAR2(32)  := C_TAB||'%';
G_Track_Progress     BOOLEAN       := TRUE;
G_Many_Tickers       BOOLEAN       := FALSE;
G_Previous_Ticker    Tickers.Ticker%TYPE;
G_Issuer_Generic     VARCHAR2(04)  := 'ZZ';
G_MAY_EXISTS         BOOLEAN       := FALSE;
G_Reg_Expr           VARCHAR2(32)  := '[0-9][0-9][0-9][0-9]$';
G_Reg_Format         VARCHAR2(32)  := '0999';
--
G_Index_Details      BOOLEAN                          := FALSE;
G_Id_Syst            Identification_Systems.Code%TYPE := NULL;
--
BR_IC_Parms          BOOLEAN;
BR_Index_Details     BOOLEAN;
BR_II_Rates          BOOLEAN;
BR_Issuers           BOOLEAN;
BR_Issues            BOOLEAN;
BR_Mat_Sched         BOOLEAN;
BR_PC_Sched          BOOLEAN;
BR_Prc_Notes         BOOLEAN;
BR_Securities        BOOLEAN;
BR_ST_Status         BOOLEAN;
BR1_ST_Status        BOOLEAN;
BR2_ST_Status        BOOLEAN;
--
Pgmloc               INTEGER;
V_Record             VARCHAR2(1024);
W_Record_Status      VARCHAR2(16);
V_Err_Tracked        BOOLEAN;
V_Row_Tracked        BOOLEAN;
V_Row_Errors         BOOLEAN;
--
V_Error              BOOLEAN;
V_Warning            BOOLEAN;
V_Message            VARCHAR2(255);
V_Rows_Read          INTEGER := 0;
V_Rows_Skip          INTEGER := 0;
V_Rows_Err           INTEGER := 0;
V_Tick_Ins           INTEGER := 0;
V_Trd_Ins            INTEGER := 0;
V_Mat_Ins            INTEGER := 0;
V_PrcN_Ins           INTEGER := 0;
V_US_Munis           INTEGER := 0;
V_Success            BOOLEAN;
V_Track_Progress     BOOLEAN;
C_Error_Len          INTEGER := 80;
E_Value_01           VARCHAR2(80);
E_Value_02           VARCHAR2(80);
--
Field_Number         INTEGER;
Field_Name           VARCHAR2(32);
Field_Numeric        BOOLEAN;
Field_Max_Length     INTEGER;
Field_Format         VARCHAR2(20);
Field_Mandatory      BOOLEAN;
--
W_Stack              VARCHAR2(4000);
W_Backtrace          VARCHAR2(4000);
W_sqlcode            INTEGER;
--
Data_File            Utl_File.File_Type;
Copy_File            Utl_File.File_Type;
Log_File             Utl_File.File_Type;
Err_File             Utl_File.File_Type;
Pricing_Msgs_Rec     Pricing_Msgs%ROWTYPE;
--
----------------------------------------------------------------------
--  Clear tickers
----------------------------------------------------------------------
--
PROCEDURE Clear_Ticker
        ( P_Identifiers   IN OUT NOCOPY Many_Identifiers
        , Prc_Msgs_Rec    IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , Success            OUT NOCOPY BOOLEAN
        ) IS
        --{
BEGIN
    --{
    SUCCESS := FALSE;
    P_Identifiers.DELETE;
    SUCCESS := TRUE;
    --}}
END Clear_Ticker;
--
----------------------------------------------------------------------
--  Set tickers
----------------------------------------------------------------------
--
PROCEDURE Set_Ticker
        ( P_Ticker        IN            Tickers.Ticker%Type
        , P_Id_Syst_Code  IN            Identification_Systems.Code%TYPE
        , P_Source_Code   IN            Sources.Code%TYPE
        , P_Currency      IN            Currencies.Currency%TYPE
        , P_ISO_Nation    IN            Nations.ISO_Nation%TYPE
        , P_Identifiers   IN OUT NOCOPY Many_Identifiers
        , Prc_Msgs_Rec    IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , Success            OUT NOCOPY BOOLEAN
        ) IS
        --{
V_Identifier  One_Identifier;
V_Source_Code Sources.Code%TYPE;
V_ISO_Nation  Nations.ISO_Nation%TYPE;
V_IDS         PLS_INTEGER;
BEGIN
    --{
    SUCCESS := FALSE;
    Pgmloc := 1010;
    IF P_id_Syst_Code IS NULL
    THEN --{
        NULL;
    ELSE --}{
        Pgmloc := 1020;
        V_Source_Code := NVL( P_Source_Code, 'NA' );
        IF P_Id_Syst_Code = C_ID_SEDOL
        THEN --{
            V_ISO_Nation := P_ISO_Nation;
        ELSE --}{
            V_ISO_Nation := NULL;
        END IF; --}
        Pgmloc := 1090;
        -- Create entry
        V_Identifier.Valid                            := FALSE;
        V_Identifier.Found                            := FALSE;
        V_Identifier.Pending                          := FALSE;
        V_Identifier.T_Rec.Fri_Ticker                 := NULL;
        V_Identifier.T_Rec.Identification_System_Code := P_Id_Syst_Code;
        V_Identifier.T_Rec.Source_Code                := V_Source_Code;
        V_Identifier.T_Rec.Currency_Code              := P_Currency;
        V_Identifier.T_Rec.Ticker                     := P_Ticker;
        V_Identifier.T_Rec.ISO_Nation_Code            := V_ISO_Nation;
        Pgmloc := 1100;
        V_IDS := NVL( P_Identifiers.LAST, 0 ) + 1;
        P_Identifiers( V_IDS ) := V_Identifier;
    END IF; --} IF P_id_Syst_Code IS NULL
    Pgmloc := 1110;
    SUCCESS := TRUE;
    --}}
END Set_Ticker;
--
----------------------------------------------------------------------
--  Get FRI from tickers
----------------------------------------------------------------------
--
PROCEDURE Get_Fri_From_Ticker
        ( P_In_Ticker             IN            Tickers%ROWTYPE
        , P_Out_Ticker               OUT        Tickers%ROWTYPE
        , P_Found                    OUT        BOOLEAN
        , P_Pending_Changes          OUT        BOOLEAN
        , P_Message               IN OUT NOCOPY VARCHAR2
        , Success                    OUT NOCOPY BOOLEAN
        ) IS
        --{
--
V_Message    VARCHAR2(255);
Field_01     VARCHAR2(255);
Field_02     VARCHAR2(255);
Field_03     VARCHAR2(255);
--
T_Rec        Tickers%ROWTYPE;
TC_Rec       Ticker_Changes%ROWTYPE;
--
CURSOR Get_Fri_Ticker
     ( Cur_ID_Sys        Identification_Systems.Code%TYPE
     , Cur_Ticker        Tickers.Ticker%TYPE
     , Cur_Source_Code   Tickers.Source_Code%TYPE
     ) IS
SELECT T.*
  FROM Tickers T
 WHERE T.Identification_System_Code = Cur_ID_Sys
   AND T.Ticker                     = Cur_Ticker
   AND T.Source_Code                = Cur_Source_Code;
--
CURSOR Get_New_Fri_Ticker
     ( Cur_ID_Sys      Identification_Systems.Code%TYPE
     , Cur_Ticker      Tickers.Ticker%TYPE
     , Cur_Source_Code Tickers.Source_Code%TYPE
     ) IS
SELECT T.*
  FROM Ticker_Changes T
 WHERE T.Identification_System_Code = Cur_ID_Sys
   AND T.NEW_Ticker                 = Cur_Ticker
   AND T.Source_Code                = Cur_Source_Code
   AND T.Effective_Date             > TRUNC( Sysdate - 10 );
--
CURSOR Get_Old_Fri_Ticker
     ( Cur_ID_Sys      Identification_Systems.Code%TYPE
     , Cur_Ticker      Tickers.Ticker%TYPE
     , Cur_Source_Code Tickers.Source_Code%TYPE
     ) IS
SELECT T.*
  FROM Ticker_Changes T
 WHERE T.Identification_System_Code = Cur_ID_Sys
   AND T.OLD_Ticker                 = Cur_Ticker
   AND T.Source_Code                = Cur_Source_Code
   AND T.Effective_Date             > TRUNC( Sysdate - 10 );
--
BEGIN
    --{
    SUCCESS := FALSE;
    P_Found := FALSE;
    P_Pending_Changes := FALSE;
    -- Initialization of output ticker row
    P_Out_Ticker.Fri_Ticker                 := P_In_Ticker.Fri_Ticker;
    P_Out_Ticker.Identification_System_Code := P_in_Ticker.Identification_System_Code;
    P_Out_Ticker.Source_Code                := P_In_Ticker.Source_Code;
    P_Out_Ticker.Currency_Code              := P_In_Ticker.Currency_Code;
    P_Out_Ticker.Ticker                     := P_In_Ticker.Ticker;
    P_Out_Ticker.Used_By_RT_Flag            := P_In_Ticker.Used_by_RT_Flag;
    P_Out_Ticker.ISO_Nation_Code            := P_In_Ticker.ISO_Nation_Code;
    --
    pgmloc := 1120;
    OPEN Get_Fri_Ticker ( P_Out_Ticker.Identification_System_Code
                        , P_Out_Ticker.Ticker
                        , P_Out_Ticker.Source_Code
                        );
    pgmloc := 1130;
    FETCH Get_Fri_Ticker INTO T_Rec;
    IF Get_Fri_Ticker%FOUND
    THEN
        P_Out_Ticker.Fri_Ticker      := T_Rec.Fri_Ticker;
        P_Out_Ticker.Currency_Code   := T_Rec.Currency_Code;
        P_Out_Ticker.Used_By_Rt_Flag := T_Rec.Used_By_RT_Flag;
        P_Out_Ticker.ISO_Nation_Code := T_Rec.ISO_Nation_Code;
        P_Found                      := TRUE;
    END IF;
    pgmloc := 1140;
    CLOSE Get_Fri_Ticker;
    --
    IF NOT P_Found
    THEN --{
        -- Look in recent changes
        pgmloc := 1150;
        OPEN Get_New_Fri_Ticker ( P_Out_Ticker.Identification_System_Code
                                , P_Out_Ticker.Ticker
                                , P_Out_Ticker.Source_Code
                                );
        pgmloc := 1160;
        FETCH Get_New_Fri_Ticker INTO TC_Rec;
        IF Get_New_Fri_Ticker%FOUND
        THEN
            P_Pending_Changes       := TRUE;
            P_Out_Ticker.Fri_Ticker := TC_Rec.Fri_Ticker;
            P_Found                 := TRUE;
        END IF;
        pgmloc := 1170;
        CLOSE Get_New_Fri_Ticker;
    END IF; --}
    --
    IF NOT P_Found
    THEN --{
        -- Look in recent changes
        pgmloc := 1180;
        OPEN Get_Old_Fri_Ticker ( P_Out_Ticker.Identification_System_Code
                                , P_Out_Ticker.Ticker
                                , P_Out_Ticker.Source_Code
                                );
        pgmloc := 1190;
        FETCH Get_Old_Fri_Ticker INTO TC_Rec;
        IF Get_Old_Fri_Ticker%FOUND
        THEN
            P_Pending_Changes       := TRUE;
            P_Out_Ticker.Fri_Ticker := TC_Rec.Fri_Ticker;
            P_Found                 := TRUE;
        END IF;
        pgmloc := 1200;
        CLOSE Get_Old_Fri_Ticker;
    END IF; --}
    --
    SUCCESS := TRUE;
    GOTO Get_Out;
    --
<<Log_Error>>
    Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                      , G_Del_Flag
                                      , Field_01
                                      , Field_02
                                      , Field_03
                                      , V_Message
                                      );
    P_Message := V_Message;
    Success := FALSE;
    --
<<Get_Out>>
    NULL;
    --}
EXCEPTION
WHEN OTHERS THEN
    P_Message := SUBSTR( Pgmloc || ' ' || SQLERRM( SQLCODE ), 1, 255 );
    Success := FALSE;
    --}
END Get_Fri_From_Ticker;
--
----------------------------------------------------------------------
--  Get tickers
----------------------------------------------------------------------
--
PROCEDURE Get_Tickers
        ( P_Identifiers           IN OUT NOCOPY Many_Identifiers
        , P_May_Exists            IN            BOOLEAN
        , P_Row_Tracked           IN OUT NOCOPY BOOLEAN
        , P_Err_Tracked           IN OUT NOCOPY BOOLEAN
        , P_Record_Read           IN            VARCHAR2
        , P_Rows_Read             IN            INTEGER
        , P_Log_File              IN            Utl_File.File_Type
        , P_Err_File              IN            Utl_File.File_Type
        , P_Valid                    OUT        BOOLEAN
        , P_Found                    OUT        BOOLEAN
        , P_Same                     OUT        BOOLEAN
        , P_Pending_Changes          OUT        BOOLEAN
        , P_Fri_Ticker               OUT        Securities.Fri_Ticker%TYPE
        , P_Prc_Msgs_Rec          IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Success                  OUT NOCOPY BOOLEAN
        ) IS
        --{
W_T_Rec           Tickers%ROWTYPE;
W_Ticker          Tickers.Ticker%TYPE;
W_Fri_Ticker      Securities.Fri_Ticker%TYPE;
W_Found           BOOLEAN;
W_Pending_Changes BOOLEAN;
W_Id_Syst         Identification_Systems.Code%TYPE;
--
V_Message         VARCHAR2(255);
V_Chk_Digit       INTEGER;
V_Status          INTEGER;
--
Field_01          VARCHAR2(255);
Field_02          VARCHAR2(255);
Field_03          VARCHAR2(255);
--
Delayed_Message   Varchar2(255);
B_Delayed_Message BOOLEAN := FALSE;
--
CURSOR Get_Identification_System
     ( Cur_Id_Sys     Identification_Systems.Code%TYPE
     ) IS
SELECT *
  FROM Identification_Systems
 WHERE Identification_Systems.Code = Cur_Id_Sys;
IDS_Rec Get_Identification_System%ROWTYPE;
--
CURSOR Get_Ticker
     ( Cur_ID_Sys     Identification_Systems.Code%TYPE
     , Cur_Fri_Ticker Tickers.Fri_Ticker%TYPE
     , Cur_Currency   Tickers.Currency_Code%TYPE
     ) IS
SELECT T.Ticker
  FROM Tickers T
 WHERE T.Identification_System_Code = Cur_ID_Sys
   AND T.Fri_Ticker                 = Cur_Fri_Ticker
   AND ( T.Currency_Code            = Cur_Currency
      OR ( T.Currency_Code IS NULL
       AND Cur_Currency    IS NULL
         )
       );
--
BEGIN
    --{
    P_SUCCESS         := FALSE;
    P_Found           := FALSE;
    P_Same            := TRUE;
    P_VALID           := TRUE;
    P_Pending_Changes := FALSE;
    P_Fri_Ticker      := NULL;
    W_Fri_Ticker      := NULL;
    --
    pgmloc := 1210;
    IDS := P_Identifiers.FIRST;
    pgmloc := 1220;
    WHILE IDS IS NOT NULL
    LOOP  --{
        W_Fri_Ticker := NULL;
        pgmloc := 1230;
        OPEN Get_Identification_System ( P_Identifiers(IDS).T_Rec.Identification_System_Code );
        pgmloc := 1240;
        FETCH Get_Identification_System
         INTO IDS_Rec;
        pgmloc := 1250;
        CLOSE Get_Identification_System;
        --
        pgmloc := 1260;
        IF IDS_Rec.Source_Dependence_Flag = 'Y'
        THEN --{
            pgmloc := 1270;
            IF P_Identifiers(IDS).T_Rec.Source_Code = 'NA'
            OR P_Identifiers(IDS).T_Rec.Source_Code IS NULL
            THEN
                pgmloc := 1280;
                G_FMessage_Code := G_Message_Code || '505';
                Field_01 := IDS_Rec.Mnemonic;
                Field_02 := P_Identifiers(IDS).T_Rec.Source_Code;
                Field_03 := P_Identifiers(IDS).T_Rec.Ticker;
                GOTO Log_Error;
            END IF;
        ELSE --}{
            pgmloc := 1290;
            IF P_Identifiers(IDS).T_Rec.Source_Code IS NULL
            THEN
                pgmloc := 1300;
                P_Identifiers(IDS).T_Rec.Source_Code := 'NA';
            ELSIF P_Identifiers(IDS).T_Rec.Source_Code <> 'NA'
            THEN
                pgmloc := 1310;
                G_FMessage_Code := G_Message_Code || '510';
                Field_01 := IDS_Rec.Mnemonic;
                Field_02 := P_Identifiers(IDS).T_Rec.Source_Code;
                Field_03 := P_Identifiers(IDS).T_Rec.Ticker;
                GOTO Log_Error;
           END IF;
        END IF; --}
        --
        pgmloc := 1320;
        IF IDS_Rec.Nation_Allowed_Flag = 'Y'
        THEN --{
            pgmloc := 1330;
            IF P_Identifiers(IDS).T_Rec.ISO_Nation_Code IS NULL
            THEN
                pgmloc := 1340;
                G_FMessage_Code := G_Message_Code || '515';
                Field_01 := IDS_Rec.Mnemonic;
                Field_02 := P_Identifiers(IDS).T_Rec.ISO_Nation_Code;
                Field_03 := P_Identifiers(IDS).T_Rec.Ticker;
                GOTO Log_Error;
            END IF;
        ELSE --}{
            pgmloc := 1350;
            IF P_Identifiers(IDS).T_Rec.ISO_Nation_Code IS NULL
            THEN
                NULL;
            ELSE
                pgmloc := 1360;
                G_FMessage_Code := G_Message_Code || '520';
                Field_01 := IDS_Rec.Mnemonic;
                Field_02 := P_Identifiers(IDS).T_Rec.ISO_Nation_Code;
                Field_03 := P_Identifiers(IDS).T_Rec.Ticker;
                GOTO Log_Error;
           END IF;
        END IF; --}
        -- Is it a Valid Cusip, ISIN, OISIN or Sedol?
        pgmloc := 1370;
        Valid_Sec_Id.Ticker ( W_Fri_Ticker
                            , P_Identifiers(IDS).T_Rec.Ticker
                            , P_Identifiers(IDS).T_Rec.Identification_System_Code
                            , V_Chk_Digit
                            , V_Status
                            , V_Message
                            ) ;
        --
        pgmloc := 1380;
        V_Message := TRIM(V_Message);
        --
        IF V_Status = 0
        THEN --{
            pgmloc := 1390;
            P_Identifiers(IDS).Valid := TRUE;
            W_Found := FALSE;
            pgmloc := 1400;
            Get_Fri_From_Ticker( P_Identifiers(IDS).T_Rec
                               , W_T_Rec
                               , W_Found
                               , W_Pending_Changes
                               , P_Prc_Msgs_Rec.Err_Text
                               , P_Success
                               );
            pgmloc := 1410;
            IF NOT P_SUCCESS
            THEN
                pgmloc := 1420;
                V_Message := P_Prc_Msgs_Rec.Err_Text;
                GOTO Log_Error_Message;
            END IF;
            --
            --
            pgmloc := 1430;
            IF NOT W_Found
            THEN --{
                pgmloc := 1440;
                W_Id_Syst := NULL;
                IF    P_Identifiers(IDS).T_Rec.Identification_System_Code = 'C'
                THEN W_Id_Syst := 'D';
                ELSIF P_Identifiers(IDS).T_Rec.Identification_System_Code = 'D'
                THEN W_Id_Syst := 'C';
                END IF;
                pgmloc := 1450;
                IF W_Id_Syst IS NOT NULL
                THEN
                    pgmloc := 1460;
                    Get_Fri_From_Ticker( P_Identifiers(IDS).T_Rec
                                       , W_T_Rec
                                       , W_Found
                                       , W_Pending_Changes
                                       , P_Prc_Msgs_Rec.Err_Text
                                       , P_Success
                                       );
                    pgmloc := 1470;
                    IF NOT P_SUCCESS
                    THEN
                        pgmloc := 1480;
                        V_Message := P_Prc_Msgs_Rec.Err_Text;
                        GOTO Log_Error_Message;
                    END IF;
                END IF;
            END IF; --} IF NOT W_Found
        ELSIF V_Status = 1
        THEN --}{
            pgmloc := 1490;
            G_FMessage_Code := G_Message_Code || '090';
            Field_01        := IDS_Rec.Mnemonic;
            Field_02        := P_Identifiers(IDS).T_Rec.Ticker;
            Field_03        := NULL;
            GOTO Log_Error;
        ELSIF V_Status   > 0
        AND   V_Message IS NULL
        THEN --}{
            pgmloc := 1500;
            G_FMessage_Code := G_Message_Code || '100';
            Field_01        := IDS_Rec.Mnemonic;
            Field_02        := P_Identifiers(IDS).T_Rec.Ticker;
            Field_03        := NULL;
            GOTO Log_Error;
        ELSIF V_Status   > 0
        AND   V_Message IS NOT NULL
        THEN --}{
            pgmloc := 1510;
            B_Delayed_Message := TRUE;
            Delayed_Message   := V_Message;
            G_FMessage_Code := G_Message_Code || '110';
            Field_01        := IDS_Rec.Mnemonic;
            Field_02        := P_Identifiers(IDS).T_Rec.Ticker;
            Field_03        := NULL;
            GOTO Log_Error;
        END IF; --}
        --
        -- Return W_T_Rec into record...  Currency, used_by_rt, ...
        pgmloc := 1520;
        P_Identifiers(IDS).Found                            := W_Found;
        P_Identifiers(IDS).Pending                          := W_Pending_Changes;
        P_Identifiers(IDS).T_Rec.Fri_Ticker                 := W_T_Rec.Fri_Ticker;
        P_Identifiers(IDS).T_Rec.Identification_System_Code := W_T_Rec.Identification_System_Code;
        P_Identifiers(IDS).T_Rec.Source_Code                := W_T_Rec.Source_Code;
        P_Identifiers(IDS).T_Rec.Currency_Code              := W_T_Rec.Currency_Code;
        P_Identifiers(IDS).T_Rec.Used_By_Rt_Flag            := W_T_Rec.Used_By_RT_Flag;
        P_Identifiers(IDS).T_Rec.ISO_Nation_Code            := W_T_Rec.ISO_Nation_Code;
        --
        W_Fri_Ticker := NULL;
        IF W_Pending_Changes
        THEN
            P_Pending_Changes := TRUE;
        END IF;
        --
        IF W_Found
        THEN --{
            P_Found := TRUE;
            IF P_Fri_Ticker IS NULL
            THEN
                P_Fri_Ticker := W_T_Rec.Fri_Ticker;
            ELSIF P_Fri_Ticker <> W_T_Rec.Fri_Ticker
            THEN
                P_Same := FALSE;
            END IF;
        END IF; --}
        --
        IF W_Found
        AND NOT P_May_Exists
        THEN
            pgmloc := 1530;
            G_FMessage_Code := G_Message_Code || '130';
            Field_01        := IDS_Rec.Mnemonic
                            || ':='''
                            || P_Identifiers(IDS).T_Rec.Ticker
                            || '''';
            Field_02        := NULL;
            Field_03        := NULL;
            GOTO Log_Error;
        END IF;
        --
        pgmloc := 1540;
        GOTO Next_Iteration;
        --
<< Log_Error >>
        Pgmloc := 1550;
        Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                          , G_Del_Flag
                                          , Field_01
                                          , Field_02
                                          , Field_03
                                          , V_Message
                                          );
<< Log_Error_Message>>
        Pgmloc := 1560;
        IF NOT P_Row_Tracked
        THEN --{
            P_Row_Tracked := TRUE;
            pgmloc             := 1570;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , NULL
                                           , P_Prc_Msgs_Rec
                                           , P_Success
                                           );
            IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc             := 1580;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , 'Processing Row # '
                                          || TO_CHAR(P_Rows_read)
                                           , P_Prc_Msgs_Rec
                                           , P_Success
                                           );
            IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF; --}
        Pgmloc := 1590;
        IF NOT P_Err_Tracked
        THEN --{
            P_Err_Tracked := TRUE;
            Pgmloc             := 1600;
            Load_Supplier_Rules.Write_File ( P_Err_File
                                           , 'Processing Row # '
                                          || TO_CHAR(P_Rows_read)
                                           , P_Prc_Msgs_Rec
                                           , P_Success
                                           );
            IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
            Pgmloc             := 1610;
            Load_Supplier_Rules.Write_File ( P_Err_File
                                           , P_Record_Read
                                           , P_Prc_Msgs_Rec
                                           , P_Success
                                           );
            IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF; --}
        --
        P_Valid := FALSE;
        --
        Pgmloc := 1620;
        V_Message := C_Indent || V_Message;
        Load_Supplier_Rules.Write_File ( P_Log_File
                                       , V_Message
                                       , P_Prc_Msgs_Rec
                                       , P_Success
                                       );
        IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
        Pgmloc := 1630;
        Load_Supplier_Rules.Write_File ( P_Err_File
                                       , V_Message
                                       , P_Prc_Msgs_Rec
                                       , P_Success
                                       );
        IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
        --
        IF B_Delayed_Message
        THEN B_Delayed_Message := FALSE;
          Pgmloc := 1640;
          V_Message := C_Indent || Delayed_Message;
          Load_Supplier_Rules.Write_File ( P_Log_File
                                         , V_Message
                                         , P_Prc_Msgs_Rec
                                         , P_Success
                                         );
          IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
          Pgmloc := 1650;
          Load_Supplier_Rules.Write_File ( P_Err_File
                                         , V_Message
                                         , P_Prc_Msgs_Rec
                                         , P_Success
                                         );
          IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF;
        --
<< Next_Iteration >>
        Pgmloc := 1660;
        IDS := P_Identifiers.NEXT(IDS);
        --
    END LOOP; --}
    --
    IF  G_May_Exists
    AND P_Found
    THEN --{
        Pgmloc := 1670;
        -- Obtenir les symboles uniques et verifier si c'est le meme que l'on
        -- obtient en utilisant le fri_ticker.
        IDS := P_Identifiers.FIRST;
        WHILE IDS IS NOT NULL
        LOOP  --{
            -- Skip the one that were rejected before...
            IF NOT P_Identifiers(IDS).Valid
            THEN
                GOTO Next_Iteration_1;
            END IF;
            --
            pgmloc := 1680;
            OPEN Get_Identification_System ( P_Identifiers(IDS).T_Rec.Identification_System_Code );
            pgmloc := 1690;
            FETCH Get_Identification_System
             INTO IDS_Rec;
            pgmloc := 1700;
            CLOSE Get_Identification_System;
            --
            -- Verify only the one that we can only have one identifiers.
            IF IDS_Rec.Duplicate_Ticker_Flag = 'Y'
            THEN
                GOTO Next_Iteration_1;
            END IF;
            --
            W_Ticker := NULL;
            pgmloc := 1710;
            OPEN Get_Ticker( P_Identifiers(IDS).T_Rec.Identification_System_Code
                           , P_Fri_Ticker
                           , P_Identifiers(IDS).T_Rec.Currency_Code
                           );
            pgmloc := 1720;
            FETCH Get_Ticker INTO W_Ticker;
            IF G_Tracing
            THEN
                pgmloc             := 1730;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , IDS_Rec.Mnemonic
                                              || ' '''
                                              || W_Ticker
                                              || ''''
                                               , P_Prc_Msgs_Rec
                                               , P_Success
                                               );
                IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            pgmloc := 1740;
            CLOSE Get_Ticker;
            IF NVL( W_Ticker, P_Identifiers(IDS).T_Rec.Ticker )
            <> P_Identifiers(IDS).T_Rec.Ticker
            THEN
                G_FMessage_Code := G_Message_Code || '130';
                Field_01 := IDS_Rec.Mnemonic||':='''||W_Ticker||'''';
                Field_02 := NULL;
                Field_03 := NULL;
                GOTO Log_Error_1;
            END IF;
            --
            GOTO Next_Iteration_1;
            --
<< Log_Error_1 >>
            IF NOT P_Row_Tracked
            THEN --{
                P_Row_Tracked := TRUE;
                pgmloc             := 1750;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , NULL
                                               , P_Prc_Msgs_Rec
                                               , P_Success
                                               );
                IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
                pgmloc             := 1760;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Processing Row # '
                                              || TO_CHAR(P_Rows_read)
                                               , P_Prc_Msgs_Rec
                                               , P_Success
                                               );
                IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF; --}
            IF NOT P_Err_Tracked
            THEN --{
                P_Err_Tracked := TRUE;
                pgmloc             := 1770;
                Load_Supplier_Rules.Write_File ( P_Err_File
                                               , 'Processing Row # '
                                              || TO_CHAR(P_Rows_read)
                                               , P_Prc_Msgs_Rec
                                               , P_Success
                                               );
                IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
                pgmloc             := 1780;
                Load_Supplier_Rules.Write_File ( P_Err_File
                                               , P_Record_Read
                                               , P_Prc_Msgs_Rec
                                               , P_Success
                                               );
                IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF; --}
            --
            P_Valid := FALSE;
            Pgmloc := 1790;
            Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                              , G_Del_Flag
                                              , Field_01
                                              , Field_02
                                              , Field_03
                                              , V_Message
                                              );
            Pgmloc := 1800;
            V_Message := C_Indent || V_Message;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , V_Message
                                           , P_Prc_Msgs_Rec
                                           , P_Success
                                           );
            IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
            Pgmloc := 1810;
            Load_Supplier_Rules.Write_File ( P_Err_File
                                           , V_Message
                                           , P_Prc_Msgs_Rec
                                           , P_Success
                                           );
            IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
            --
<<next_iteration_1>>
            IDS := P_Identifiers.NEXT(IDS);
        END LOOP; --}
    END IF; --} G_May_Exists
    --
    Pgmloc := 1820;
    P_SUCCESS := TRUE;
    --
<< Get_Out >>
    --
    Pgmloc := 1830;
    NULL;
    --
EXCEPTION
WHEN OTHERS THEN
    P_Prc_Msgs_Rec.Err_Text := SUBSTR( Pgmloc || ' ' || SQLERRM( SQLCODE )
                                     , 1, 255 );
    P_Success := FALSE;
END Get_Tickers; --}}
--
----------------------------------------------------------------------
-- Initialize Issuers_xref
----------------------------------------------------------------------
--
PROCEDURE Initialize_Issuers_Xref_Row (
          R_Issuers_Xref IN OUT NOCOPY Issuers_Xref%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Issuers_Xref.FRI_Issuer                := NULL;
    R_Issuers_Xref.Issuer_Ids_Code           := NULL;
    R_Issuers_Xref.Issuer_Identifier         := NULL;
    R_Issuers_Xref.Last_User                 := G_User;
    R_Issuers_Xref.Last_Stamp                := NULL;
    --}}
END Initialize_Issuers_Xref_Row;
--
----------------------------------------------------------------------
-- Initialize Index_Details Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Index_Details_Row (
          R_Index_Details IN OUT NOCOPY Index_Details%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Index_Details.FRI_Ticker                := NULL;
    R_Index_Details.I_Description             := NULL;
    R_Index_Details.I_Category                := NULL;
    R_Index_Details.Base_Date                 := NULL;
    R_Index_Details.Base_Value                := NULL;
    R_Index_Details.Rebal_Frequency_Value     := NULL;
    R_Index_Details.Last_User                 := G_User;
    R_Index_Details.Last_Stamp                := NULL;
    --}}
END Initialize_Index_Details_Row;
--
----------------------------------------------------------------------
-- Initialize Interest_Calc_parms Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Int_Calc_Parms_Row (
          R_IC_Parms   IN OUT NOCOPY Interest_Calc_parms%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_IC_Parms.FRI_Ticker                       := NULL;
    R_IC_Parms.Day_Count_Method_Code            := G_Day_Count;
    R_IC_Parms.Accruals_Calc_Frequency_Value    := G_Acc_Freq_Value;
    R_IC_Parms.Interest_Schedule_Flag           := NULL;
    R_IC_Parms.Floating_Rate_Flag               := NULL;
    R_IC_Parms.Accruals_Calc_Formula_Code       := G_Acc_Calc_Formula;
    R_IC_Parms.Accruals_Day_Count_Method_Code   := G_Day_Count;
    R_IC_Parms.Compounding_Frequency_Value      := NULL;
    R_IC_Parms.Capital_Amortization_Date        := NULL;
    R_IC_Parms.Participation_Note               := NULL;
    R_IC_Parms.Generate_Int_Sch_Flag            := NULL;
    R_IC_Parms.End_of_Month_Flag                := NULL;
    R_IC_Parms.Next_Adjustment_Date             := NULL;
    R_IC_Parms.Next_Adjustment_Text             := NULL;
    R_IC_Parms.Rounding_Factor_Code             := NULL;
    R_IC_Parms.Use_First_Pay_Date_Flag          := NULL;
    R_IC_Parms.Yield_Curve_Code                 := NULL;
    R_IC_Parms.Spread_on_FRI_Ticker             := NULL;
    R_IC_Parms.Spread_on_Source_Code            := NULL;
    R_IC_Parms.Spread                           := NULL;
    R_IC_Parms.Sort_Order                       := NULL;
    R_IC_Parms.Effective_Date                   := NULL;
    R_IC_Parms.UL_FRI_Ticker                    := NULL;
    R_IC_Parms.Last_User                        := G_User;
    R_IC_Parms.Last_Stamp                       := NULL;
    --}}
END Initialize_Int_Calc_Parms_Row;
--
----------------------------------------------------------------------
-- Initialize Interest_Income_Rates Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Int_Inc_Rates_Row (
          R_II_Rates   IN OUT NOCOPY Interest_Income_Rates%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_II_Rates.FRI_Ticker                := NULL;
    R_II_Rates.Accruals_Start_Date       := NULL;
    R_II_Rates.First_Payment_Date        := NULL;
    R_II_Rates.Annual_Income_Rate        := NULL;
    R_II_Rates.Income_Currency           := NULL;
    R_II_Rates.Opt_Income_Currency       := NULL;
    R_II_Rates.Scheduled_Event_Flag      := NULL;
    R_II_Rates.Last_User                 := G_User;
    R_II_Rates.Last_Stamp                := NULL;
    --}}
END Initialize_Int_Inc_Rates_Row;
--
----------------------------------------------------------------------
-- Initialize Issuers Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Issuers_Row (
          R_Issuers    IN OUT NOCOPY Issuers%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Issuers.Code                       := NULL;
    R_Issuers.FRI_Issuer                 := NULL;
    R_Issuers.Issuer_Type_Code           := NULL;
    R_Issuers.Issuer_Sub_Type_Code       := NULL;
    R_Issuers.Publishing_Name_E          := NULL;
    R_Issuers.Publishing_Name_F          := NULL;
    R_Issuers.Text_E                     := NULL;
    R_Issuers.Text_F                     := NULL;
    R_Issuers.Long_Name_E                := NULL;
    R_Issuers.Long_Name_F                := NULL;
    R_Issuers.Long_Name_2_E              := NULL;
    R_Issuers.Long_Name_2_F              := NULL;
    R_Issuers.Nation_Code                := NULL;
    R_Issuers.Active_Flag                := NULL;
    R_Issuers.Date_of_Status             := NULL;
    R_Issuers.Jobber_Code_For_Status     := NULL;
    R_Issuers.FRI_Address                := NULL;
    R_Issuers.Fiscal_Year_End            := NULL;
    R_Issuers.Last_Annual_Report_on_File := NULL;
    R_Issuers.Remarks                    := NULL;
    R_Issuers.Last_User                  := G_User;
    R_Issuers.Last_Stamp                 := NULL;
    R_Issuers.New_Issuer_Code            := NULL;
    --}}
END Initialize_Issuers_Row;
--
----------------------------------------------------------------------
-- Initialize Issues Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Issues_Row (
          R_Issues    IN OUT NOCOPY Issues%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Issues.FRI_Ticker             := NULL;
    R_Issues.Issue_Date             := NULL;
    R_Issues.Quantity               := NULL;
    R_Issues.Denominations          := NULL;
    R_Issues.Price                  := NULL;
    R_Issues.Price_Currency         := NULL;
    R_Issues.Price_High             := NULL;
    R_Issues.Price_Low              := NULL;
    R_Issues.Offering_Date          := NULL;
    R_Issues.Spread                 := NULL;
    R_Issues.Private_Placement_Flag := NULL;
    R_Issues.Last_User              := G_User;
    R_Issues.Last_Stamp             := NULL;
    --}}
END Initialize_Issues_Row;
--
----------------------------------------------------------------------
-- Initialize Maturity Schedule Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Mat_Schedule_Row (
          R_Mat_Sched    IN OUT NOCOPY Maturity_Schedule%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Mat_Sched.FRI_Ticker                 := NULL;
    R_Mat_Sched.Maturity_Date              := NULL;
    R_Mat_Sched.Maturity_Type_Code         := G_Maturity_Type;
    R_Mat_Sched.Redemption_Amount          := G_Redemption_Amount;
    R_Mat_Sched.Redemption_Currency        := G_Redemption_Currency;
    R_Mat_Sched.Starting_Notification_Date := NULL;
    R_Mat_Sched.Ending_Notification_Date   := NULL;
    R_Mat_Sched.Last_User                  := G_User;
    R_Mat_Sched.Last_Stamp                 := NULL;
    --}}
END Initialize_Mat_Schedule_Row;
--
----------------------------------------------------------------------
-- Initialize Pricing_Calc_Schedule Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_PC_Schedule_Row (
          R_PC_Sched   IN OUT NOCOPY Pricing_Calc_Schedule%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_PC_Sched.FRI_Ticker                := NULL;
    R_PC_Sched.Source_Code               := NULL;
    R_PC_Sched.Data_Supplier_Code        := NULL;
    R_PC_Sched.Delivery_Time             := NULL;
    R_PC_Sched.Last_User                 := G_User;
    R_PC_Sched.Last_Stamp                := NULL;
    --}}
END Initialize_PC_Schedule_Row;
--
----------------------------------------------------------------------
-- Initialize Pricing_Notes Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Prc_Notes_Row (
          R_Prc_Notes IN OUT NOCOPY Pricing_Notes%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Prc_Notes.FRI_Ticker                := NULL;
    R_Prc_Notes.Pricing_Note              := NULL;
    R_Prc_Notes.Base_Ticker               := NULL;
    R_Prc_Notes.Base_Bond                 := NULL;
    R_Prc_Notes.Spread                    := NULL;
    R_Prc_Notes.Option_Type               := NULL;
    R_Prc_Notes.Option_Date               := NULL;
    R_Prc_Notes.Option_Price              := NULL;
    R_Prc_Notes.Sinking_Fund_Flag         := G_Sink_Fund_Flag;
    R_Prc_Notes.Last_User                 := G_User;
    R_Prc_Notes.Last_Stamp                := NULL;
    --}}
END Initialize_Prc_Notes_Row;
--
----------------------------------------------------------------------
-- Initialize Securities Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Securities_Row (
          R_Securities    IN OUT NOCOPY Securities%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Securities.FRI_Ticker                := NULL;
    R_Securities.Cusip                     := NULL;
    R_Securities.I_Cusip                   := NULL;
    R_Securities.Security_Type_Code        := G_Sec_Type;
    R_Securities.Security_Sub_Type_Code    := G_Sec_SubType;
    R_Securities.FRI_Issuer                := NULL;
    R_Securities.Trading_Status_Code       := G_Trd_Status;
    R_Securities.SAC_Changed_by_User       := G_SAC_Changed_Flag;
    R_Securities.Date_of_Status            := Constants.Get_Site_Date;
    R_Securities.Jobber_Code_for_Status    := G_User;
    R_Securities.Book_Based_Eligible_Flag  := G_Book_Based_Flag;
    R_Securities.Constant_Prices_Flag      := G_Const_Prc_Flag;
    R_Securities.Long_Name_E               := NULL;
    R_Securities.Long_Name_F               := NULL;
    R_Securities.Short_Name_E              := NULL;
    R_Securities.Short_Name_F              := NULL;
    R_Securities.MUSIC_Text                := NULL;
    R_Securities.Series                    := G_Series;
    R_Securities.Class                     := G_Class;
    R_Securities.Holder_Votes              := NULL;
    R_Securities.Income_Type_Code          := G_Income_Type;
    R_Securities.Income_Frequency_Value    := NULL;
    R_Securities.Income_Currency           := NULL;
    R_Securities.Optional_Income_Currency  := NULL;
    R_Securities.Indicated_Annual_Rate     := NULL;
    R_Securities.Par_Value                 := NULL;
    R_Securities.Par_Currency              := NULL;
    R_Securities.Optional_Par_Currency     := NULL;
    R_Securities.Call_Schedule_Flag        := G_Call_Flag;
    R_Securities.Conversion_Schedule_Flag  := G_Conv_Flag;
    R_Securities.Ext_Ret_Flag              := G_E_R_Flag;
    R_Securities.Intangibility_Flag        := G_Intangibility_Flag;
    R_Securities.Maturity_Schedule_Flag    := NULL;
    R_Securities.Mat_Date_Sel_Method_Code  := G_Mat_Date_Method;
    R_Securities.Perpetual_Flag            := G_Perp_Flag;
    R_Securities.Purchase_Fund_Flag        := G_Pur_Fund_Flag;
    R_Securities.Ratings_Flag              := G_Ratings_Flag;
    R_Securities.Sinking_Fund_Flag         := G_Sink_Fund_Flag;
    R_Securities.Split_Flag                := G_Splits_Flag;
    R_Securities.Mid_Term_Note_Flag        := G_MTN_Flag;
    R_Securities.DBT_Flag                  := NULL;
    R_Securities.Last_User                 := G_User;
    R_Securities.Last_Stamp                := NULL;
    R_Securities.CDS_Flag                  := NULL;
    R_Securities.Corp_Actions_Flag         := NULL;
    R_Securities.Corp_Actions_Date         := NULL;
    R_Securities.StepUp_Bond_Flag          := NULL;
    R_Securities.Fixed_to_Float_Flag       := NULL;
    R_Securities.Par_Effective_Date        := NULL;
    R_Securities.Par_Source_Code           := NULL;
    R_Securities.Par_Data_Supplier_Code    := NULL;
    R_Securities.Primary_Currency          := NULL;
    R_Securities.Primary_Source_Code       := NULL;
    R_Securities.Primary_Effective_Date    := NULL;
    R_Securities.Inflation_Linked_Bond_Flag:= NULL;
    R_Securities.Long_Name_2_E             := NULL;
    R_Securities.Long_Name_2_F             := NULL;
    --}}
END Initialize_Securities_Row;
--
----------------------------------------------------------------------
-- Initialize Security Trading Status Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_ST_Status_Row (
          R_ST_Status    IN OUT NOCOPY Security_Trading_Status%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_ST_Status.FRI_Ticker                := NULL;
    R_ST_Status.Source_Code               := NULL;
    R_ST_Status.Price_Currency            := NULL;
    R_ST_Status.Trading_Status_Code       := G_Trd_Status;
    R_ST_Status.Date_of_Status            := Constants.Get_Site_Date;
    R_ST_Status.Jobber_Code_for_Status    := G_User;
    R_ST_Status.Date_of_Status_BC         := NULL;
    R_ST_Status.Remarks                   := NULL;
    R_ST_Status.Last_User                 := G_User;
    R_ST_Status.Last_Stamp                := NULL;
    --}}
END Initialize_ST_Status_Row;
--
PROCEDURE Log_Error
IS --{
BEGIN --{
    -- Switching the position of conditions
    -- to accomodate OASIS_Messages calls with Parameters
    -- Adding label Log_Error_Got_Message for them
    IF V_Message IS NULL
    THEN
        Pgmloc := 1840;
        Manage_Messages.Get_Oasis_Message( G_FMessage_Code
                                         , G_Del_Flag
                                         , Field_Name
                                         , E_Value_01
                                         , E_Value_02
                                         , V_Message
                                         );
    END IF;
            --
    IF NOT V_Row_Tracked
    THEN --{
        V_Row_Tracked := TRUE;
        pgmloc             := 1850;
        Load_Supplier_Rules.Write_File( Log_File
                                      , NULL
                                      , Pricing_Msgs_Rec
                                      , V_Success
                                      );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        pgmloc             := 1860;
        Load_Supplier_Rules.Write_File( Log_File
                                      , 'Processing Row # '
                                     || TO_CHAR(V_Rows_read)
                                      , Pricing_Msgs_Rec
                                      , V_Success
                                      );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        Pgmloc             := 1870;
        Load_Supplier_Rules.Write_File( Err_File
                                      , V_Record
                                      , Pricing_Msgs_Rec
                                      , V_Success
                                      );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    END IF; --}
    IF NOT V_Err_Tracked
    THEN --{
        V_Err_Tracked := TRUE;
        Pgmloc             := 1880;
        Load_Supplier_Rules.Write_File( Err_File
                                      , 'Processing Row # '
                                     || TO_CHAR(V_Rows_read)
                                      , Pricing_Msgs_Rec
                                      , V_Success
                                      );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        Pgmloc             := 1890;
        Load_Supplier_Rules.Write_File( Err_File
                                      , V_Record
                                      , Pricing_Msgs_Rec
                                      , V_Success
                                      );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    END IF; --}
    --
    IF NOT V_Warning
    THEN
        IF NOT V_Row_Errors
        THEN
            V_Rows_Err   := V_Rows_Err + 1;
            V_Row_Errors := TRUE;
        END IF;
        W_Record_Status := C_Error;
    ELSE
        v_message:= 'WARN: '||v_message;
    END IF;
    V_Warning := FALSE;
            --
    V_Message := C_Indent || V_Message;
    Pgmloc := 1900;
    Load_Supplier_Rules.Write_File( Log_File
                                  , V_Message
                                  , Pricing_Msgs_Rec
                                  , V_Success
                                  );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    Pgmloc := 1910;
    Load_Supplier_Rules.Write_File( Err_File
                                  , V_Message
                                  , Pricing_Msgs_Rec
                                  , V_Success
                                  );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    V_Message := NULL;
<< Get_Out >>
    NULL;
--
END Log_Error; --}}
--
--
----------------------------------------------------------------------
-- Initialize Call Calc Parms Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Call_Calc_Row (
          R_Call         IN OUT NOCOPY Call_Calc_Parms%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Call.FRI_Ticker                := NULL;
    R_Call.Financial_Advantages_Flag := NULL;
    R_Call.Callable_For_Fin_Adv_Date := NULL;
    R_Call.Sinking_Fund_Only_Flag    := NULL;
    R_Call.Partial_Call_Flag         := NULL;
    R_Call.Rounding_Factor_Code      := NULL;
    R_Call.Max_O_S_For_Call          := NULL;
    R_Call.Call_Condition_Flag       := NULL;
    R_Call.Call_Schedule_Flag        := NULL;
    R_Call.Remarks                   := NULL;
    R_Call.Last_User                 := G_User;
    R_Call.Last_Stamp                := NULL;
    --}}
END Initialize_Call_Calc_Row;
--
----------------------------------------------------------------------
-- Initialize Call Calc Parms Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Call_Cond_Row (
          R_Call         IN OUT NOCOPY Call_Conditions%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Call.FRI_Ticker                   := NULL;
    R_Call.Starting_Date                := NULL;
    R_Call.Ending_Date                  := NULL;
    R_Call.UL_Fri_TIcker                := NULL;
    R_Call.UL_Source_Code               := NULL;
    R_Call.Market_Measurement_Type_Code := NULL;
    R_Call.Threshold_Type_Code          := NULL;
    R_Call.Value                        := NULL;
    R_Call.Price_Currency               := NULL;
    R_Call.Number_Of_Trading_Days       := NULL;
    R_Call.Number_Of_Cons_Trading_Days  := NULL;
    R_Call.Issuer_Must_Call_Flag        := NULL;
    R_Call.Last_User                    := G_User;
    R_Call.Last_Stamp                   := NULL;
    --}}
END Initialize_Call_Cond_Row;
--
----------------------------------------------------------------------
-- Initialize Call Schedule Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Call_Schd_Row (
          R_Call         IN OUT NOCOPY Call_Schedule%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Call.FRI_Ticker            := NULL;
    R_Call.Call_Type_Code        := NULL;
    R_Call.Starting_Date         := NULL;
    R_Call.Ending_Date           := NULL;
    R_Call.Price                 := NULL;
    R_Call.Price_Currency        := NULL;
    R_Call.Spread                := NULL;
    R_Call.Data_Supplier_Code    := NULL;
    R_Call.Last_User             := G_User;
    R_Call.Last_Stamp            := NULL;
    --}}
END Initialize_Call_Schd_Row;
--
----------------------------------------------------------------------
-- Initialize Conversion Schedule Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Conv_Schd_Row (
          R_CNV          IN OUT NOCOPY Conversion_Schedule%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_CNV.FRI_Ticker            := NULL;
    R_CNV.Decision_Type_Code    := NULL;
    R_CNV.Starting_Date         := NULL;
    R_CNV.Ending_Date           := NULL;
    R_CNV.Fuseau_Horaire_Code   := NULL;
    R_CNV.Event_Type_Code       := NULL;
    R_CNV.Conversion_Date       := NULL;
    R_CNV.Notice_Days           := NULL;
    R_CNV.Quantity_To_Convert   := NULL;
    R_CNV.Quantity_Received     := NULL;
    R_CNV.Rounding_Factor_Code  := NULL;
    R_CNV.UL_Fri_TIcker         := NULL;
    R_CNV.UL_Source_Code        := NULL;
    R_CNV.Strike_Price          := NULL;
    R_CNV.Strike_Price_Currency := NULL;
    R_CNV.Notes                 := NULL;
    R_CNV.Last_User             := G_User;
    R_CNV.Last_Stamp            := NULL;
    --}}
END Initialize_Conv_Schd_Row;
--
----------------------------------------------------------------------
-- Initialize Group Ticker COmposition Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Group_Ticker_Row (
          R_GTC          IN OUT NOCOPY Group_Tickers_Composition%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_GTC.FRI_Ticker            := NULL;
    R_GTC.Fri_Ticker_Component  := NULL;
    R_GTC.Source_Code           := NULL;
    R_GTC.Quantity              := NULL;
    R_GTC.Percent_of_Group_Cost := NULL;
    R_GTC.Last_User             := G_User;
    R_GTC.Last_Stamp            := NULL;
    --}}
END Initialize_Group_Ticker_Row;
--
----------------------------------------------------------------------
-- Initialize Interest Floating Rates Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Int_Float_Rates_Row (
          R_IFR          IN OUT NOCOPY Interest_Floating_Rates%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_IFR.FRI_Ticker                     := NULL;
    R_IFR.Accruals_Start_Date            := NULL;
    R_IFR.Final_Reset_Date               := NULL;
    R_IFR.Measured_Fri_Ticker            := NULL;
    R_IFR.Pricing_Source_Code            := NULL;
    R_IFR.Price_Currency                 := NULL;
    R_IFR.Market_Measurement_Factor      := 1;
    R_IFR.Market_Measurement_Type_Code   := 'CLS';
    R_IFR.Market_Measurement_Days        := 1;
    R_IFR.Time_Measurement_Method_Code   := NULL;
    R_IFR.Market_Measurement_Method_Code := 'AM';
    R_IFR.Market_Measurement_Fixed       := NULL;
    R_IFR.Frequency_Value                := NULL;
    R_IFR.Fixed_Premium                  := NULL;
    R_IFR.Minimum_Guaranteed_Rate        := NULL;
    R_IFR.Maximum_Guaranteed_Rate        := NULL;
    R_IFR.Last_User                      := G_User;
    R_IFR.Last_Stamp                     := NULL;
    --}}
END Initialize_Int_Float_Rates_Row;
--
----------------------------------------------------------------------
-- Initialize Purchase Fund Calc Parms Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Purch_Fnd_Calc_Row (
          R_PFS          IN OUT NOCOPY Purchase_Fund_Calc_Parms%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_PFS.FRI_Ticker                     := NULL;
    R_PFS.Cumulative_Fund_Flag           := NULL;
    R_PFS.Fund_Payment_Type_Code         := NULL;
    R_PFS.Payment_Date                   := NULL;
    R_PFS.Purchase_Fund_Schedule_Flag    := NULL;
    R_PFS.Remarks                        := NULL;
    R_PFS.Last_User                      := G_User;
    R_PFS.Last_Stamp                     := NULL;
    --}}
END Initialize_Purch_Fnd_Calc_Row;
--
----------------------------------------------------------------------
-- Initialize Purchase Fund Schedule Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Purch_Fnd_Schd_Row (
          R_PFS          IN OUT NOCOPY Purchase_Fund_Schedule%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_PFS.FRI_Ticker                     := NULL;
    R_PFS.Starting_Date                  := NULL;
    R_PFS.Ending_Date                    := NULL;
    R_PFS.Percentage                     := NULL;
    R_PFS.Optional_Percentage            := NULL;
    R_PFS.Number_of_Periods              := NULL;
    R_PFS.Maximum_Purchase_Price         := NULL;
    R_PFS.Price_Limits_Currency          := NULL;
    R_PFS.Last_User                      := G_User;
    R_PFS.Last_Stamp                     := NULL;
    --}}
END Initialize_Purch_Fnd_Schd_Row;
--
----------------------------------------------------------------------
-- Initialize Ratings Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Ratings_Row (
          R_Rat          IN OUT NOCOPY Ratings%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_Rat.FRI_Ticker                     := NULL;
    R_Rat.Rating_Date                    := NULL;
    R_Rat.Rater                          := NULL;
    R_Rat.Rating_Code                    := NULL;
    R_Rat.Rating_Date_BC                 := NULL;
    R_Rat.Last_User                      := G_User;
    R_Rat.Last_Stamp                     := NULL;
    --}}
END Initialize_Ratings_Row;
--
----------------------------------------------------------------------
-- Initialize Sinking Fund Calc Parms Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Sink_Fnd_Calc_Row (
          R_SFS          IN OUT NOCOPY Sinking_Fund_Calc_Parms%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_SFS.FRI_Ticker                     := NULL;
    R_SFS.Repurchasing_Condition_Code    := NULL;
    R_SFS.Rounding_Factor_Code           := NULL;
    R_SFS.Fund_Payment_Type_Code         := NULL;
    R_SFS.Payment_Date                   := NULL;
    R_SFS.Defeased_Flag                  := NULL;
    R_SFS.Sinking_Fund_Schedule_Flag     := NULL;
    R_SFS.Remarks                        := NULL;
    R_SFS.Last_User                      := G_User;
    R_SFS.Last_Stamp                     := NULL;
    --}}
END Initialize_Sink_Fnd_Calc_Row;
--
----------------------------------------------------------------------
-- Initialize Sinking Fund Schedule Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Sink_Fnd_Schd_Row (
          R_SFS          IN OUT NOCOPY Sinking_Fund_Schedule%ROWTYPE
        ) IS
--{
BEGIN
    --{
    R_SFS.FRI_Ticker                     := NULL;
    R_SFS.Starting_Date                  := NULL;
    R_SFS.Ending_Date                    := NULL;
    R_SFS.Percentage                     := NULL;
    R_SFS.Optional_Percentage            := NULL;
    R_SFS.Price                          := NULL;
    R_SFS.Price_Currency                 := NULL;
    R_SFS.Amount                         := NULL;
    R_SFS.Cashflow_Amount                := NULL;
    R_SFS.Par_OS_Amount                  := NULL;
    R_SFS.RPB_Amount                     := NULL;
    R_SFS.Last_User                      := G_User;
    R_SFS.Last_Stamp                     := NULL;
    --}}
END Initialize_Sink_Fnd_Schd_Row;
--
--
----------------------------------------------------------------------
-- Initialize Schedule Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Schd_Row (
          Rec          IN OUT NOCOPY Sched
        ) IS
--{
BEGIN
    --{
    Rec.Fri_Ticker                     := NULL;
    Rec.Unique_Id                      := NULL;
    Rec.Fri_Id                         := NULL;
    Rec.Schedule                       := NULL;
    Rec.Accruals_Start_Date            := NULL;
    Rec.Accruals_Calc_Formula_Code     := NULL;
    Rec.Annual_Income_Rate             := NULL;
    Rec.Base_Bond                      := NULL;
    Rec.Base_Ticker                    := NULL;
    Rec.Call_Type_Code                 := NULL;
    Rec.Class_Code                     := NULL;
    Rec.Class_Type                     := NULL;
    Rec.Currency_Code                  := NULL;
    Rec.Date_Of_Status                 := NULL;
    Rec.Day_Count                      := NULL;
    Rec.Denominations                  := NULL;
    Rec.Ending_Date                    := NULL;
    Rec.Final_Reset_Date               := NULL;
    Rec.First_Payment_Date             := NULL;
    Rec.Fixed_Premium                  := NULL;
    Rec.Frequency_Value                := NULL;
    Rec.Identification_System_Code     := NULL;
    Rec.Identification_System_Mnemonic := NULL;
    Rec.Income_Currency                := NULL;
    Rec.INISYS_Day_Count               := NULL;
    Rec.ISO_Nation                     := NULL;
    Rec.Issue_Date                     := NULL;
    Rec.Market_Measurement_Days        := NULL;
    Rec.Market_Measurement_Factor      := NULL;
    Rec.Market_Measurement_Method_Code := NULL;
    Rec.Market_Measurement_Type_Code   := NULL;
    Rec.Maturity_Date                  := NULL;
    Rec.Maturity_Type_Code             := NULL;
    Rec.Measured_Fri_Id                := NULL;
    Rec.Measured_Unique_Id             := NULL;
    Rec.Measured_Fri_Ticker            := NULL;
    Rec.Offering_Date                  := NULL;
    Rec.Option_Date                    := NULL;
    Rec.Option_Price                   := NULL;
    Rec.Option_Type                    := NULL;
    Rec.Optional_Income_Currency       := NULL;
    Rec.Price                          := NULL;
    Rec.Pricing_Currency               := NULL;
    Rec.Pricing_Note                   := NULL;
    Rec.Pricing_Source_Code            := NULL;
    Rec.Private_Placement_Flag         := NULL;
    Rec.Quantity                       := NULL;
    Rec.Rater                          := NULL;
    Rec.Rating_Code                    := NULL;
    Rec.Rating_Date                    := NULL;
    Rec.Redemption_Amount              := NULL;
    Rec.Redemption_Currency            := NULL;
    Rec.Remarks                        := NULL;
    Rec.Scheduled_Event_Flag           := NULL;
    Rec.Sinking_Fund_Flag              := NULL;
    Rec.Source_Code                    := NULL;
    Rec.Spread_Note                    := NULL;
    Rec.Spread                         := NULL;
    Rec.Starting_Date                  := NULL;
    Rec.Trading_Status_Mnemonic        := NULL;
    Rec.Trading_Status_Code            := NULL;
    Rec.Ticker                         := NULL;
    Rec.UL_Fri_Ticker                  := NULL;
    Rec.UL_Unique_Id                   := NULL;
    Rec.UL_Fri_Id                      := NULL;
END Initialize_Schd_Row; --}}
--
--
----------------------------------------------------------------------
-- Initialize Tickers Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_Tickers_Row (
          Rec          IN OUT NOCOPY Tickers%ROWTYPE
        ) IS
--{
BEGIN
    --{
    Rec.Fri_Ticker                     := NULL;
    Rec.Identification_System_Code     := NULL;
    Rec.Source_Code                    := NULL;
    Rec.Ticker                         := NULL;
    Rec.Currency_Code                  := NULL;
    Rec.Used_By_RT_Flag                := NULL;
    Rec.DBT_Flag                       := NULL;
    Rec.ISO_Nation_Code                := NULL;
    Rec.Official_Flag                  := NULL;
    Rec.Source_NA                      := NULL;
    Rec.Ticker_NA                      := NULL;
    Rec.Currency_NA                    := NULL;
    Rec.ISO_Nation_NA                  := NULL;
    Rec.Last_User                      := G_User;
    Rec.Last_Stamp                     := NULL;
END Initialize_Tickers_Row; --}}
--
--
----------------------------------------------------------------------
-- Initialize US MUNIS Attributes Row
----------------------------------------------------------------------
--
PROCEDURE Initialize_US_Munis_Attr_Row (
          Rec          IN OUT NOCOPY US_MUNIS_Attributes%ROWTYPE
        ) IS
--{
BEGIN
    --{
    Rec.Fri_Ticker                     := NULL;
    Rec.Issuer_Type                    := NULL;
    Rec.Type_I                         := NULL;
    Rec.Type_II                        := NULL;
    Rec.Type_III                       := NULL;
    Rec.Taxation_Flag                  := NULL;
    Rec.Insurance_Flag                 := NULL;
    Rec.Refinancing_Flag               := NULL;
    Rec.State                          := NULL;
    Rec.Creation_User                  := G_User;
    Rec.Creation_Stamp                 := NULL;
    Rec.Last_User                      := G_User;
    Rec.Last_Stamp                     := NULL;
END Initialize_US_Munis_Attr_Row; --}}
--
----------------------------------------------------------------------
-- Get Fri Ticker from a Fri Id
----------------------------------------------------------------------
--
PROCEDURE Get_Ticker_From_Id (
          P_Id           IN     Tickers.Ticker%TYPE
        , P_Ticker          OUT Tickers.Fri_Ticker%TYPE
        , P_Log_File     IN     Utl_File.File_Type
        ) IS
--{
CURSOR Get_Fri_Ticker
     ( I_Id Tickers.Ticker%TYPE
     ) IS
SELECT Fri_Ticker
  FROM Tickers
 WHERE Ticker = I_ID
   AND Identification_System_Code = 'X'
   AND LENGTH(I_ID) = 6
UNION
SELECT Fri_Ticker
  FROM Tickers
 WHERE Ticker = I_ID
   AND Identification_System_Code IN ( 'C', 'D' )
   AND LENGTH(I_ID) = 9
UNION
SELECT Fri_Ticker
  FROM Tickers
 WHERE Ticker = I_ID
   AND Identification_System_Code = 'Z'
   AND LENGTH(I_ID) = 12
UNION
SELECT Fri_Ticker
  FROM Tickers
 WHERE Ticker = I_ID
   AND Identification_System_Code = 'S'
   AND LENGTH(I_ID) = 7
       ;

BEGIN
    --{
    P_Ticker := NULL;
    OPEN Get_Fri_TIcker( UPPER( P_Id ) );
    FETCH Get_Fri_Ticker INTO P_Ticker;
    CLOSE Get_Fri_Ticker;
    --}}
END Get_Ticker_From_Id;
--
--
PROCEDURE Default_Call_Calc_Parms
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
CCP Call_Calc_Parms%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    CCP.Fri_Ticker                := P_Rec.Fri_Ticker;
    CCP.Financial_Advantages_Flag := 'N';
    CCP.Callable_For_Fin_Adv_Date := NULL;
    CCP.Sinking_Fund_Only_Flag    := 'N';
    CCP.Partial_Call_Flag         := 'Y';
    CCP.Rounding_Factor_Code      := NULL;
    CCP.Max_O_S_For_Call          := NULL;
    CCP.Notice_Days_Min           := 30;
    CCP.Notice_Days_Max           := 60;
    CCP.Call_Condition_Flag       := 'N';
    CCP.Call_Schedule_Flag        := 'Y';
    CCP.Remarks                   := NULL;
    CCP.Last_User                 := Constants.Get_User_Id;
    CCP.Last_Stamp                := SYSDATE;

    INSERT INTO Call_Calc_Parms
    VALUES CCP;

    V_Warning :=  TRUE;
    Field_Name := 'CALL_CALC_PARMS';
    G_Fmessage_Code := G_Message_Code||'650';
    Log_Error;

    -- Do not commit or rollback, this will be done by the calling routine.

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN DUP_VAL_ON_INDEX
THEN
    -- No need to insert defaulted values, real values are there.
    P_Success := TRUE;
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Default_Call_Calc_Parms;  --}}
--
----------------------------------------------------------------------
-- Add Call Schedule
----------------------------------------------------------------------
--
PROCEDURE Add_Call_Schedule
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
CS Call_Schedule%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Call_Type_Code IS NULL
    THEN
        Field_Name := C_Call_Type_Code;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Starting_Date IS NULL
    THEN
        Field_Name := C_Starting_Date;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Ending_Date IS NULL
    THEN
        Field_Name := C_Ending_Date;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    CS.Fri_Ticker     := P_Rec.Fri_Ticker;
    CS.Call_Type_Code := P_Rec.Call_Type_Code;
    CS.Starting_Date  := P_Rec.Starting_Date;
    CS.Ending_Date    := P_Rec.Ending_Date;
    CS.Price          := P_Rec.Price;
    CS.Price_Currency := P_Rec.Pricing_Currency;
    CS.Spread         := P_Rec.Spread;
    CS.Last_User      := Constants.Get_User_Id;
    CS.Last_Stamp     := SYSDATE;

    Default_Call_Calc_Parms( P_rec
                           , P_Success
                           );

    INSERT INTO Call_Schedule
    VALUES CS;
 
    IF P_Create
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Call_Schedule;  --}}
--
----------------------------------------------------------------------
-- Add Classification
----------------------------------------------------------------------
--
PROCEDURE Add_Classification
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
WCC Workbench_Category_Classes%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Class_Type IS NULL
    THEN
        Field_Name := C_Class_Type;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Class_Code IS NULL
    THEN
        Field_Name := C_CLass_Code;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    WCC.Workbench_Code := 'ISS';
    WCC.Fri_Workbench := P_Rec.Fri_Ticker;
    WCC.Category_Code := P_Rec.Class_Type;
    WCC.Class_Code    := P_Rec.Class_Code;
    WCC.Last_User     := Constants.Get_User_Id;
    WCC.Last_Stamp    := SYSDATE;

    INSERT INTO Workbench_Category_Classes
    VALUES WCC;
 
    IF P_Create
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Classification;  --}}
--
--
----------------------------------------------------------------------
-- Add Group Tickers Composition
----------------------------------------------------------------------
--
PROCEDURE Add_Group_Tickers_Composition
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
GTC      Group_Tickers_Composition%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.UL_Unique_Id IS NULL
    THEN
        Field_Name := C_UL_Fri_Symbol;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Quantity IS NULL
    THEN
        Field_Name := C_Quantity;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Source_Code IS NULL
    THEN
        Field_Name := C_Source_Code;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Get_Ticker_From_Id( P_Rec.UL_Unique_Id, P_Rec.UL_Fri_Ticker, Log_File );
    IF P_Rec.UL_Fri_Ticker IS NULL
    THEN
        G_FMessage_Code := G_Message_Code || '610';
        E_Value_01      := P_Rec.UL_Fri_Id;
        E_Value_01      := P_Rec.UL_Unique_Id;
        Log_Error;
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Initialize_Group_Ticker_Row( GTC );

    GTC.FRI_Ticker            := P_Rec.Fri_Ticker;
    GTC.Fri_Ticker_Component  := P_Rec.UL_Fri_Ticker;
    GTC.Source_Code           := P_Rec.Source_Code;
    GTC.Quantity              := P_Rec.Quantity;
    GTC.Percent_of_Group_Cost := NULL;
    GTC.Last_User             := G_User;
    GTC.Last_Stamp            := SYSDATE;

    INSERT INTO Group_Tickers_Composition
    VALUES GTC;
 
    IF P_Create
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Group_Tickers_Composition;  --}}
--
--
----------------------------------------------------------------------
-- Add Interest Floating Rates
----------------------------------------------------------------------
--
PROCEDURE Add_Interest_Floating_Rates
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
V_Dummy    Dual.Dummy%TYPE;
IFR        Interest_Floating_Rates%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Accruals_Start_Date IS NULL
    THEN
        Field_Name := C_Accrual_Start_Date;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Measured_Unique_Id IS NULL
    THEN
        Field_Name := C_Measured_Statpro_Id;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Pricing_Source_Code IS NULL
    THEN
        Field_Name := C_Pricing_Source_Code;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Pricing_Currency IS NULL
    THEN
        Field_Name := C_Pricing_Currency;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Frequency_Value IS NULL
    THEN
        Field_Name := C_Frequency_Value;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Fixed_Premium IS NULL
    THEN
        Field_Name := C_Fixed_Premium;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Get_Ticker_From_Id( P_Rec.Measured_Unique_Id, P_Rec.Measured_Fri_Ticker, Log_File );
    IF P_Rec.Measured_Fri_Ticker IS NULL
    THEN
        G_FMessage_Code := G_Message_Code || '610';
        E_Value_01      := P_Rec.Measured_Unique_Id;
        Log_Error;
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    BEGIN
        V_Dummy := NULL;
        SELECT Dummy
          INTO V_Dummy
          FROM DUAL
         WHERE NOT EXISTS (
               SELECT 1
                 FROM Securities S
                    , Interest_Calc_Parms ICP
                WHERE S.Fri_Ticker = P_Rec.Fri_Ticker
                  AND S.Income_Type_Code = 'I'
                  AND ICP.Fri_Ticker = S.Fri_Ticker
                  AND ICP.Floating_Rate_Flag = 'Y'
               );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    IF V_Dummy IS NOT NULL
    THEN
        -- Security is not defined as a floating rate interest bearing security.
        G_FMessage_Code := G_Message_Code || '630';
        E_Value_01      := P_Rec.Fri_Id;
        Log_Error;
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Initialize_Int_Float_Rates_Row( IFR );

    IFR.FRI_Ticker                     := P_Rec.Fri_Ticker;
    IFR.Accruals_Start_Date            := P_Rec.Accruals_Start_Date;
    IFR.Final_Reset_Date               := NVL( P_Rec.Final_Reset_Date, P_Rec.Accruals_Start_Date-1);
    IFR.Measured_Fri_Ticker            := P_Rec.Measured_Fri_Ticker;
    IFR.Pricing_Source_Code            := P_Rec.Pricing_Source_Code;
    IFR.Price_Currency                 := P_Rec.Pricing_Currency;
    IFR.Market_Measurement_Factor      := NVL( P_Rec.Market_Measurement_Factor, 1);
    IFR.Market_Measurement_Type_Code   := NVL( P_Rec.Market_Measurement_Type_Code, 'CLS');
    IFR.Market_Measurement_Days        := NVL( P_Rec.Market_Measurement_Days, 1);
    IFR.Market_Measurement_Method_Code := NVL( P_Rec.Market_Measurement_Method_Code, 'AM');
    IFR.Frequency_Value                := P_Rec.Frequency_Value;
    IFR.Fixed_Premium                  := P_Rec.Fixed_Premium;
    IFR.Last_User                      := G_User;
    IFR.Last_Stamp                     := SYSDATE;

    INSERT INTO Interest_Floating_Rates
    VALUES IFR;
 
    IF P_Create
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Interest_Floating_Rates;  --}}
--
--
----------------------------------------------------------------------
-- Add Interest Income Rates
----------------------------------------------------------------------
--
PROCEDURE Add_Interest_Income_Rates
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
V_Dummy    Dual.Dummy%TYPE;
IIR        Interest_Income_Rates%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Accruals_Start_Date IS NULL
    THEN
        Field_Name := C_Accrual_Start_Date;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.First_Payment_Date IS NULL
    THEN
        Field_Name := C_First_Payment_Date;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Scheduled_Event_Flag IS NULL
    THEN
        Field_Name := C_Scheduled_Event_Flag;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;


    BEGIN
        V_Dummy := NULL;
        SELECT Dummy
          INTO V_Dummy
          FROM DUAL
         WHERE NOT EXISTS (
               SELECT 1
                 FROM Securities S
                WHERE S.Fri_Ticker = P_Rec.Fri_Ticker
                  AND S.Income_Type_Code = 'I'
               );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    IF V_Dummy IS NOT NULL
    THEN
        -- Security is not defined as an interest bearing security.
        G_FMessage_Code := G_Message_Code || '640';
        E_Value_01      := P_Rec.Fri_Id;
        Log_Error;
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Initialize_Int_Inc_Rates_Row ( IIR );

    IIR.FRI_Ticker                := P_Rec.Fri_Ticker;
    IIR.Accruals_Start_Date       := P_Rec.Accruals_Start_Date;
    IIR.First_Payment_Date        := P_Rec.First_Payment_Date;
    IIR.Annual_Income_Rate        := P_Rec.Annual_Income_Rate;
    IIR.Income_Currency           := P_Rec.Income_Currency;
    IIR.Opt_Income_Currency       := P_Rec.Optional_Income_Currency;
    IIR.Scheduled_Event_Flag      := P_Rec.Scheduled_Event_Flag;
    IIR.Last_User                 := G_User;
    IIR.Last_Stamp                := SYSDATE;

    INSERT INTO Interest_Income_Rates
    VALUES IIR;
 
    IF P_Create
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Interest_Income_Rates;  --}}
--
--
----------------------------------------------------------------------
-- Add Issues
----------------------------------------------------------------------
--
PROCEDURE Add_Issues
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
ISS        Issues%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Issue_Date IS NULL
    THEN
        Field_Name := C_Issue_Date;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Initialize_Issues_Row ( ISS );

    Iss.FRI_Ticker             := P_Rec.Fri_Ticker;
    Iss.Issue_Date             := P_Rec.Issue_Date;
    Iss.Quantity               := P_Rec.Quantity;
    Iss.Denominations          := P_Rec.Denominations;
    Iss.Price                  := P_Rec.Price;
    Iss.Price_Currency         := P_Rec.Pricing_Currency;
    Iss.Offering_Date          := P_Rec.Offering_Date;
    Iss.Spread                 := P_Rec.Spread;
    Iss.Private_Placement_Flag := P_Rec.Private_Placement_Flag;
    Iss.Last_User              := G_User;
    Iss.Last_Stamp             := SYSDATE;

    INSERT INTO Issues
    VALUES ISS;
 
    IF P_Create
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Issues;  --}}
--
--
----------------------------------------------------------------------
-- Add Maturity Schedule
----------------------------------------------------------------------
--
PROCEDURE Add_Maturity_Schedule
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
V_Dummy    Dual.Dummy%TYPE;
MS         Maturity_Schedule%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Maturity_Date IS NULL
    THEN
        Field_Name := C_Maturity_Date;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Maturity_Type_Code IS NULL
    THEN
        Field_Name := C_Maturity_Type;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Redemption_Amount IS NULL
    THEN
        Field_Name := C_Redemption;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Redemption_Currency IS NULL
    THEN
        Field_Name := C_Redemption_Currency;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;


    BEGIN
        V_Dummy := NULL;
        SELECT Dummy
          INTO V_Dummy
          FROM DUAL
         WHERE NOT EXISTS (
               SELECT 1
                 FROM Securities S
                WHERE S.Fri_Ticker = P_Rec.Fri_Ticker
                  AND S.Maturity_Schedule_Flag = 'Y'
                  AND S.Perpetual_Flag = 'N'
               );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        NULL;
    END;
    IF V_Dummy IS NOT NULL
    THEN
        -- Security is not defined as an interest bearing security.
        G_FMessage_Code := G_Message_Code || '640';
        E_Value_01      := P_Rec.Fri_Id;
        Log_Error;
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Initialize_Mat_Schedule_Row ( MS );
    
    MS.FRI_Ticker                 := P_Rec.Fri_Ticker;
    MS.Maturity_Date              := P_Rec.Maturity_Date;
    MS.Maturity_Type_Code         := P_Rec.Maturity_Type_Code;
    MS.Redemption_Amount          := P_Rec.Redemption_Amount;
    MS.Redemption_Currency        := P_Rec.Redemption_Currency;
    MS.Last_User                  := G_User;
    MS.Last_Stamp                 := SYSDATE;


    INSERT INTO Maturity_Schedule
    VALUES MS;
 
    IF P_Create
    THEN
        COMMIT;
        V_Mat_Ins := V_Mat_Ins + 1;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Maturity_Schedule;  --}}
--
--
----------------------------------------------------------------------
-- Add Pricing Notes
----------------------------------------------------------------------
--
PROCEDURE Add_Pricing_Notes
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
PN         Pricing_Notes%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Initialize_Prc_Notes_Row ( PN );
    
    PN.FRI_Ticker                := P_Rec.Fri_Ticker;
    PN.Pricing_Note              := P_Rec.Pricing_Note;
    PN.Base_Ticker               := P_Rec.Base_Ticker;
    PN.Base_Bond                 := P_Rec.Base_Bond;
    PN.Spread                    := P_Rec.Spread_Note;
    PN.Option_Type               := P_Rec.Option_Type;
    PN.Option_Date               := P_Rec.Option_Date;
    PN.Option_Price              := P_Rec.Option_Price;
    PN.Sinking_Fund_Flag         := P_Rec.Sinking_Fund_Flag;
    PN.Last_User                 := G_User;
    PN.Last_Stamp                := SYSDATE;

    INSERT INTO Pricing_Notes
    VALUES PN;
 
    IF P_Create
    THEN
        COMMIT;
        V_PrcN_Ins := V_PrcN_Ins + 1;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Pricing_Notes;  --}}
--
--
----------------------------------------------------------------------
-- Add Ratings
----------------------------------------------------------------------
--
PROCEDURE Add_Ratings
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
Rat        Ratings%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Rating_Date IS NULL
    THEN
        Field_Name := C_Rating_Date;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Rater IS NULL
    THEN
        Field_Name := C_Rater;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Rating_Code IS NULL
    THEN
        Field_Name := C_Rating_Code;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Initialize_Ratings_Row ( Rat );
    
    Rat.FRI_Ticker                     := P_Rec.Fri_Ticker;
    Rat.Rating_Date                    := P_Rec.Rating_Date;
    Rat.Rater                          := P_Rec.Rater;
    Rat.Rating_Code                    := P_Rec.Rating_Code;
    Rat.Last_User                      := G_User;
    Rat.Last_Stamp                     := Sysdate;
    
    INSERT INTO Ratings
    VALUES Rat;
 
    IF P_Create
    THEN
        COMMIT;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Ratings;  --}}
--
--
----------------------------------------------------------------------
-- Add Tickers
----------------------------------------------------------------------
--
PROCEDURE Add_Tickers
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
TCK        Tickers%ROWTYPE;
ids        Identification_Systems%ROWTYPE;
Src        Sources%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Identification_System_Mnemonic IS NULL
    THEN
        Field_Name := C_Ticker_Mnemonic;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Ticker IS NULL
    THEN
        Field_Name := C_Symbol;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;


    BEGIN
        SELECT *    
          INTO IDS
          FROM Identification_Systems
         WHERE Mnemonic = P_Rec.Identification_System_Mnemonic
               ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        G_FMessage_Code := G_Message_Code || '050';
        Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                          , G_Del_Flag
                                          , C_Ticker_Mnemonic
                                          , P_Rec.Identification_System_Mnemonic
                                          , V_Message
                                          );
        Log_Error;
        P_Success       := TRUE;
        GOTO Get_Out;
    END;

    IF Ids.Source_Dependence_Flag = 'Y'
    OR Ids.Nation_Allowed_Flag    = 'Y'
    THEN
        -- We must have a source
        IF P_Rec.Source_Code IS NULL
        THEN
            Field_Name := C_Source_Code;
            G_FMessage_Code := G_Message_Code||'020';
            Log_Error;
        END IF;
        BEGIN
            SELECT *
              INTO Src
              FROM Sources
             WHERE Code = P_Rec.Source_Code
                   ;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
            G_FMessage_Code := G_Message_Code || '050';
            Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                              , G_Del_Flag
                                              , C_Source_Code
                                              , P_Rec.Source_Code
                                              , V_Message
                                              );
            Log_Error;
            P_Success       := TRUE;
            GOTO Get_Out;
        END;
        IF Ids.Nation_Allowed_Flag = 'Y'
        THEN
            SELECT ISO_Nation
              INTO P_Rec.ISO_Nation
              FROM Nations
             WHERE Code = Src.Nation_Code;
             P_Rec.Source_Code := 'NA';
        END IF;
    ELSE
        P_Rec.Source_Code := 'NA';
    END IF;

    IF Ids.Currency_Flag = 'M'
    AND P_Rec.Currency_Code IS NULL
    THEN
        Field_Name := C_Currency_Code;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;

    Initialize_Tickers_Row ( TCK );
    
    TCK.FRI_Ticker                     := P_Rec.Fri_Ticker;
    TCK.Identification_System_Code     := IDS.Code;
    TCK.Source_Code                    := P_Rec.Source_Code;
    TCK.Ticker                         := P_Rec.Ticker;
    TCK.Currency_Code                  := P_Rec.Currency_Code;
    TCK.ISO_Nation_Code                := P_Rec.ISO_Nation;
    TCK.Last_User                      := G_User;
    TCK.Last_Stamp                     := SYSDATE;

    IF TCK.Identification_System_Code = C_ID_CUSIP
    -- starts by a letter
    AND LOWER(SUBSTR(TCK.Ticker,1,1)) <> UPPER(SUBSTR(TCK.Ticker,1,1))
    AND TCK.Ticker NOT LIKE 'TMP%'
    THEN --}{
        -- Substitute CINS instead of CUSIP
        TCK.Identification_System_Code := C_ID_CINS;
    ELSIF TCK.Identification_System_Code = C_ID_CINS
          -- Do not starts by a letter
    AND LOWER(SUBSTR(TCK.Ticker,1,1)) = UPPER(SUBSTR(TCK.Ticker,1,1))
    THEN --}{
        -- Do not insert a CINS if the ticker do not starts
        -- by a letter
        TCK.Identification_System_Code := C_ID_CUSIP;
    END IF;

    -- Do a ticker change on the cusip only if the old one is a temporary
    IF TCK.Identification_System_Code = C_ID_CUSIP
    THEN
        IF Security.Get_Cusip( TCK.Fri_Ticker ) LIKE 'TMP%'
        THEN
            INSERT INTO Ticker_Changes
                 ( Fri_Ticker
                 , Effective_Date
                 , User_Id
                 , Entry_Date
                 , Identification_System_Code
                 , Source_Code
                 , Old_Ticker
                 , New_Ticker
                 , Updated_Flag
                 , Changes_Flag
                 , Remarks
                 , Effective_Date_Bc
                 , Last_User
                 , Last_Stamp
                 )
            VALUES
                 ( Tck.Fri_Ticker
                 , TRUNC( SYSDATE )
                 , G_User
                 , TRUNC( SYSDATE )
                 , Tck.Identification_System_Code
                 , 'NA' -- Source not applicable
                 , Security.Get_Cusip( Tck.Fri_Ticker )
                 , TCK.Ticker
                 , 'N'  -- Not updated yet
                 , 'Y'  -- It is a change, not a desactivation
                 , TRIM( SUBSTR( P_Rec.Remarks, 1, 60 ) )
                 , NULL -- Date bc will be set by trigger
                 , G_User
                 , SYSDATE
                 );
        ELSE
            Field_Name := C_Cusip;
            E_Value_01 := Tck.Ticker;
            E_Value_02 := Security.get_cusip( TCK.Fri_Ticker );
            G_Fmessage_Code := G_Message_Code||'020';
            Log_Error;
        END IF;
    ELSE
        INSERT INTO Tickers
        VALUES TCK;
    END IF;
 
    IF P_Create
    THEN
        COMMIT;
        V_Tick_Ins := V_Tick_Ins + 1;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Tickers;  --}}
--
--
----------------------------------------------------------------------
-- Add Trading_Status
----------------------------------------------------------------------
--
PROCEDURE Add_Trading_Status
        ( P_Rec           IN OUT NOCOPY Sched
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
STS        Security_Trading_Status%ROWTYPE;
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Source_Code IS NULL
    THEN
        Field_Name := C_Source_Code;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Pricing_Currency IS NULL
    THEN
        Field_Name := C_Pricing_Currency;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Trading_Status_Mnemonic IS NULL
    THEN
        Field_Name := C_Trading_status;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF P_Rec.Date_Of_Status IS NULL
    THEN
        Field_Name := C_Date_Of_Status;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;


    BEGIN
        SELECT Code 
          INTO P_Rec.Trading_Status_Code
          FROM Trading_Status
         WHERE Mnemonic = P_Rec.Trading_status_Mnemonic
            OR Code     = P_Rec.Trading_Status_Mnemonic
               ;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        G_FMessage_Code := G_Message_Code || '050';
        Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                          , G_Del_Flag
                                          , C_Trading_Status
                                          , P_Rec.Trading_Status_Mnemonic
                                          , V_Message
                                          );
        Log_Error;
        P_Success       := TRUE;
        GOTO Get_Out;
    END;

    Initialize_ST_Status_Row ( STS );
    
    STS.FRI_Ticker                := P_Rec.Fri_Ticker;
    STS.Source_Code               := P_Rec.Source_Code;
    STS.Price_Currency            := P_Rec.Pricing_Currency;
    STS.Trading_Status_Code       := P_Rec.Trading_Status_Code;
    STS.Date_of_Status            := P_Rec.Date_Of_Status;
    STS.Jobber_Code_for_Status    := G_User;
    STS.Remarks                   := SUBSTR( P_Rec.Remarks, 1, 60 );
    STS.Last_User                 := G_User;
    STS.Last_Stamp                := SYSDATE;


    INSERT INTO Security_Trading_Status
    VALUES STS;
 
    IF P_Create
    THEN
        COMMIT;
        V_Trd_Ins := V_Trd_Ins + 1;
    ELSE
        ROLLBACK;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    ROLLBACK;
    
END Add_Trading_Status;  --}}
--
--
----------------------------------------------------------------------
-- Add US_MUNIS_Attributes
----------------------------------------------------------------------
--
PROCEDURE Add_US_Munis_Attributes
        ( P_Rec           IN OUT NOCOPY US_Munis_Attributes%ROWTYPE
        , P_Create        IN            BOOLEAN
        , P_Success          OUT NOCOPY BOOLEAN
        ) IS
--{
BEGIN ---{
    P_Success   := FALSE;
    W_Stack     := NULL;
    W_Backtrace := NULL;
    W_SQLCode   := NULL;

    IF P_Rec.Fri_Ticker IS NULL
    THEN
        Field_Name := C_Source_Code;
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF  P_Rec.Issuer_Type      IS NULL
    AND P_Rec.Type_I           IS NULL
    AND P_Rec.Type_II          IS NULL
    AND P_Rec.Type_III         IS NULL
    AND P_Rec.Taxation_Flag    IS NULL
    AND P_Rec.Insurance_Flag   IS NULL
    AND P_Rec.Refinancing_Flag IS NULL
    AND P_Rec.State            IS NULL
    THEN
        Field_Name := 'All values';
        G_Fmessage_Code := G_Message_Code||'020';
        Log_Error;
    END IF;
    IF V_Row_Errors
    THEN
        P_Success := TRUE;
        GOTO Get_Out;
    END IF;
    
    BEGIN
      INSERT INTO US_Munis_Attributes
      VALUES P_Rec;
    EXCEPTION
    WHEN DUP_VAL_ON_INDEX
    THEN
      UPDATE US_Munis_Attributes
         SET Issuer_Type      = NVL( P_Rec.Issuer_Type     , Issuer_Type      )
           , Type_I           = NVL( P_Rec.Type_I          , Type_I           )
           , Type_II          = NVL( P_Rec.Type_II         , Type_II          )
           , Type_III         = NVL( P_Rec.Type_III        , Type_III         )
           , Taxation_Flag    = NVL( P_Rec.Taxation_Flag   , Taxation_Flag    )
           , Insurance_Flag   = NVL( P_Rec.Insurance_Flag  , Insurance_Flag   )
           , Refinancing_Flag = NVL( P_Rec.Refinancing_Flag, Refinancing_Flag )
           , State            = NVL( P_Rec.State,            State )
       WHERE Fri_Ticker = P_Rec.Fri_Ticker;
    END ;
 
    IF P_Create
    THEN
        V_US_Munis := V_US_Munis + 1;
    END IF;

    P_Success := TRUE;
<<Get_Out>>
    NULL;

EXCEPTION
WHEN OTHERS
THEN
    W_SQLCode   := SQLCODE;
    W_Stack     := DBMS_UTILITY.Format_Error_Stack;
    W_Backtrace := DBMS_UTILITY.Format_Error_Backtrace;
    V_Message := W_Stack;
    Log_Error;
    
END Add_US_Munis_Attributes; --}}
--
----------------------------------------------------------------------
-- Verify Frequency_Validation
----------------------------------------------------------------------
--
PROCEDURE Verify_Frequency
        ( R_Securities    IN OUT NOCOPY Securities%ROWTYPE
        , R_FV            IN OUT NOCOPY Frequency_Validation%ROWTYPE
        , Field_Name      IN OUT NOCOPY VARCHAR2
        , R_Field         IN OUT NOCOPY VARCHAR2
        ) IS
--{
CURSOR Get_Frequency_Validation
     ( Cur_Security_Type_Code  Security_Types.Code%TYPE
     , Cur_Frequency_Value     Frequencies.Value%TYPE
     ) IS
SELECT *
  FROM Frequency_Validation FV
 WHERE FV.Security_Type_Code       =  Cur_Security_Type_Code
   AND FV.Invalid_Frequency_Value  =  Cur_Frequency_Value ;
--
BEGIN
    --{
    pgmloc := 1920;
    OPEN  Get_Frequency_Validation ( R_Securities.Security_Type_Code
                                   , R_Securities.Income_Frequency_Value
                                   );
    pgmloc := 1930;
    FETCH Get_Frequency_Validation
     INTO R_FV;
    IF Get_Frequency_Validation%FOUND
    THEN
        pgmloc := 1940;
        CLOSE Get_Frequency_Validation;
        Field_Name := C_Income_Freq||'&'||C_Security_Type;
        R_Field := R_Securities.Income_Frequency_Value
                || '&'
                || R_Securities.Security_Type_Code;
        G_FMessage_Code := G_Message_Code || '050';
        GOTO Get_Out;
    END IF;
    pgmloc := 1950;
    CLOSE Get_Frequency_Validation;
    --
<<Get_Out>>
    NULL;
--
END Verify_Frequency; --}}
--
----------------------------------------------------------------------
-- Verify Maturity_Controls
----------------------------------------------------------------------
--
PROCEDURE Verify_Maturity_Controls
        ( R_Security_Pattern_Code IN    Security_Patterns.Code%TYPE
        , R_Securities    IN OUT NOCOPY Securities%ROWTYPE
        , R_Mat_Controls  IN OUT NOCOPY Maturity_Controls%ROWTYPE
        , R_Mat_Sched     IN OUT NOCOPY Maturity_Schedule%ROWTYPE
        , Field_Name      IN OUT NOCOPY VARCHAR2
        , R_Field         IN OUT NOCOPY VARCHAR2
        ) IS
--{
CURSOR Get_Maturity_Controls
     ( Cur_Security_Pattern_Code  Security_Patterns.Code%TYPE
     ) IS
SELECT *
  FROM Maturity_Controls MC
 WHERE MC.Security_Pattern_Code =  Cur_Security_Pattern_Code ;
--
--
BEGIN
    --{
    pgmloc := 1960;
    OPEN Get_Maturity_Controls ( R_Security_Pattern_Code );
    pgmloc := 1970;
    FETCH Get_Maturity_Controls
     INTO R_Mat_Controls;
    IF Get_Maturity_Controls%NOTFOUND
    THEN
        pgmloc := 1980;
        CLOSE Get_Maturity_Controls;
        Field_Name := C_Income_Freq||'&'||C_Security_Type;
        R_Field := R_Securities.Income_Frequency_Value
                || '&'
                || R_Securities.Security_Type_Code;
        G_FMessage_Code := G_Message_Code || '050';
        GOTO Get_Out;
    END IF;
    pgmloc := 1990;
    CLOSE Get_Maturity_Controls;
    --
<<Get_Out>>
    NULL;
--
END Verify_Maturity_Controls; --}}
--
----------------------------------------------------------------------
-- Verify Security_Controls
----------------------------------------------------------------------
--
PROCEDURE Verify_Security_Controls
        ( R_Securities    IN OUT NOCOPY Securities%ROWTYPE
        , R_Sec_Controls  IN OUT NOCOPY Security_Controls%ROWTYPE
        , B_Freq_Value    IN            BOOLEAN
        , B_Intangibility_Flag IN       BOOLEAN
        , Field_Name      IN OUT NOCOPY VARCHAR2
        , R_Field         IN OUT NOCOPY VARCHAR2
        ) IS
--{
CURSOR Get_Security_Controls
     ( Cur_Security_Sec_Type  Security_Types.Code%TYPE
     , Cur_Income_Type_Code   Income_Types.Code%TYPE
     ) IS
SELECT *
  FROM Security_Controls s
 WHERE S.Security_Type_Code = Cur_Security_Sec_Type
   AND S.Income_Type_Code   = Cur_Income_Type_Code;
--
BEGIN
    --{
    pgmloc := 2000;
    OPEN Get_Security_Controls( R_Securities.Security_Type_Code
                              , R_Securities.Income_Type_Code
                              );
    pgmloc := 2010;
    FETCH Get_Security_Controls
     INTO R_Sec_Controls;
    IF Get_Security_Controls%NOTFOUND
    THEN
        pgmloc := 2020;
        CLOSE Get_Security_Controls;
        Field_Name := C_Security_Type||' & '||C_Income_Type;
        R_Field := R_Securities.Security_Type_Code||' '||r_securities.income_type_code;
        G_FMessage_Code := G_Message_Code || '050';
        GOTO Get_Out;
    END IF;
    pgmloc := 2030;
    CLOSE Get_Security_Controls;
    IF NOT B_Freq_Value
    AND    R_Securities.Income_Frequency_Value IS NULL
    THEN
        R_Securities.Income_Frequency_Value
            := R_Sec_Controls.Default_Frequency_Value;
    END IF;
    IF NOT B_Intangibility_Flag
    AND    R_Securities.Intangibility_Flag IS NULL
    THEN
        R_Securities.Intangibility_Flag := R_Sec_Controls.Intangibility_Flag;
    END IF;
    IF R_Securities.Income_Type_Code <> 'I'
    THEN
        BR_IC_Parms := FALSE;
        BR_II_Rates := FALSE;
    END IF;
    --
<<Get_Out>>
    NULL;
--
END Verify_Security_Controls; --}}
--
----------------------------------------------------------------------
-- Verify Security_Pattern_Controls
----------------------------------------------------------------------
--
PROCEDURE Verify_Security_Ptrn_Controls
        ( R_Security_Pattern_Code IN    Security_Patterns.Code%TYPE
        , R_Securities    IN OUT NOCOPY Securities%ROWTYPE
        , R_SPC           IN OUT NOCOPY Security_Pattern_Controls%ROWTYPE
        , R_ST_Status     IN OUT NOCOPY Security_Trading_Status%ROWTYPE
        , R_Mat_Sched     IN OUT NOCOPY Maturity_Schedule%ROWTYPE
        , B_Par_Value     IN            BOOLEAN
        , B_Call_Flag     IN            BOOLEAN
        , B_Conv_Flag     IN            BOOLEAN
        , B_E_R_Flag      IN            BOOLEAN
        , B_Perp_Flag     IN            BOOLEAN
        , R_Fri_Cy        IN OUT NOCOPY VARCHAR2
        , Field_Name      IN OUT NOCOPY VARCHAR2
        , R_Field         IN OUT NOCOPY VARCHAR2
        ) IS
--{
CURSOR Get_Security_Pattern_Controls
     ( Cur_Security_Pattern_Code  Security_Patterns.Code%TYPE
     ) IS
SELECT *
  FROM Security_Pattern_Controls SPC
 WHERE SPC.Security_Pattern_Code =  Cur_Security_Pattern_Code ;
--
BEGIN
    --{
    pgmloc := 2040;
    OPEN Get_Security_Pattern_Controls ( R_Security_Pattern_Code );
    pgmloc := 2050;
    FETCH Get_Security_Pattern_Controls
     INTO R_SPC;
    IF Get_Security_Pattern_Controls%NOTFOUND
    THEN
        pgmloc := 2060;
        CLOSE Get_Security_Pattern_Controls;
        Field_Name := C_Income_Freq||'&'||C_Security_Type;
        R_Field := R_Securities.Income_Frequency_Value
                || '&'
                || R_Securities.Security_Type_Code;
        G_FMessage_Code := G_Message_Code || '050';
        GOTO Get_Out;
    END IF;
    pgmloc := 2070;
    CLOSE Get_Security_Pattern_Controls;
    --
    IF R_SPC.Maturity_Schedule_Flag = 'N'
    THEN
        BR_Mat_Sched := FALSE;
    END IF;
    IF R_SPC.Issue_Flag = 'N'
    THEN
        BR_Issues := FALSE;
    END IF;
    IF R_SPC.Pricing_Flag = 'N'
    THEN
        BR_IC_Parms := FALSE;
    END IF;
    IF R_SPC.Prc_Calc_Sch_Flag = 'N'
    THEN
        BR_PC_Sched := FALSE;
    END IF;
    IF R_SPC.Trading_Status_Flag = 'N'
    THEN
        BR_ST_Status := FALSE;
        BR1_ST_Status := FALSE;
        BR2_ST_Status := FALSE;
    END IF;
    IF R_SPC.Par_Value_Flag = 'N'
    THEN --{
        R_Securities.Par_Value  := NULL;
        R_Securities.Par_Currency := NULL;
    ELSE --}{
        -- Set the value not read from the file
        IF NOT B_Par_Value
        THEN --{
            R_Securities.Par_Value := R_SPC.Default_Par_Value;
        END IF; --}
        IF R_Securities.Par_Value IS NOT NULL
        THEN --{
            IF R_Securities.Par_Currency IS NULL
            THEN --{
                R_Securities.Par_Currency
                    := NVL( R_Fri_Cy
                          , NVL( G_Par_Fri_Cy
                               , R_ST_Status.Price_Currency
                               )
                          );
            END IF; --}
        END IF; --}
        IF R_Securities.Par_Currency IS NULL
        THEN --{
            IF R_SPC.Par_Value_Flag = 'O'
            THEN --{
                R_Securities.Par_Value := NULL;
            ELSE --}{
                -- The trapping is done further down.
                NULL;
            END IF; --}
        END IF; --}
    END IF; --}
    IF R_SPC.Call_Schedule_Flag = 'N'
    THEN --{
        IF B_Call_Flag
        THEN --{
            IF NVL( R_Securities.Call_Schedule_Flag, 'N' ) != 'N'
            THEN --{
                Field_Name := C_Call_Flag ||'&'||C_Security_Type;
                R_Field    := R_Securities.Call_Schedule_Flag;
                G_FMessage_Code := G_Message_Code || '050';
                GOTO Get_Out;
            ELSE --}{
                R_Securities.Call_Schedule_Flag := 'N';
            END IF; --}
        ELSE --}{
            R_Securities.Call_Schedule_Flag := 'N';
        END IF; --}
    ELSIF R_SPC.Call_Schedule_Flag = 'M'
    THEN --}{
        IF B_Call_Flag
        THEN --{
            IF NVL( R_Securities.Call_Schedule_Flag, 'N' ) != 'Y'
            THEN --{
                Field_Name := C_Call_Flag ||'&'||C_Security_Type;
                R_Field    := R_Securities.Call_Schedule_Flag;
                G_FMessage_Code := G_Message_Code || '050';
                GOTO Get_Out;
            ELSE --}{
                R_Securities.Call_Schedule_Flag := 'Y';
            END IF; --}
        ELSE --}{
            R_Securities.Call_Schedule_Flag := 'Y';
        END IF; --}
    ELSIF NOT B_Call_Flag
    THEN --}{
        R_Securities.Call_Schedule_Flag := G_Call_Flag;
    END IF; --}
    IF R_SPC.Conversion_Schedule_Flag = 'N'
    THEN --{
        IF NVL( R_Securities.Conversion_Schedule_Flag, 'N' ) != 'N'
        AND B_Conv_Flag
        THEN --{
            Field_Name := C_Convertible_Flag ||'&'||C_Security_Type;
            R_Field    := R_Securities.Conversion_Schedule_Flag;
            G_FMessage_Code := G_Message_Code || '050';
            GOTO Get_Out;
        ELSE --}{
            R_Securities.Conversion_Schedule_Flag := 'N';
        END IF; --}
    ELSIF R_SPC.Conversion_Schedule_Flag = 'M'
    THEN --}{
        IF B_Conv_Flag
        THEN --{
            IF NVL( R_Securities.Conversion_Schedule_Flag, 'N' ) != 'Y'
            THEN --{
                Field_Name := C_Convertible_Flag ||'&'||C_Security_Type;
                R_Field    := R_Securities.Conversion_Schedule_Flag;
                G_FMessage_Code := G_Message_Code || '050';
                GOTO Get_Out;
            END IF; --}
        ELSE --}{
            R_Securities.Conversion_Schedule_Flag := 'Y';
        END IF; --}
    END IF; --}
    IF R_SPC.Ext_Ret_Flag = 'N'
    THEN --{
        IF NVL( R_Securities.Ext_Ret_Flag, 'N' ) != 'N'
        AND B_E_R_Flag
        THEN --{
            Field_Name := C_E_R_Flag ||'&'||C_Security_Type;
            R_Field    := R_Securities.Ext_Ret_Flag;
            G_FMessage_Code := G_Message_Code || '050';
            GOTO Get_Out;
        ELSE --}{
            R_Securities.Ext_Ret_Flag := 'N';
        END IF; --}
    ELSIF R_SPC.Ext_Ret_Flag = 'M'
    THEN --}{
        IF B_E_R_Flag
        THEN --{
            IF NVL( R_Securities.Ext_Ret_Flag, 'N' ) != 'Y'
            THEN --{
                Field_Name := C_E_R_Flag ||'&'||C_Security_Type;
                R_Field    := R_Securities.Ext_Ret_Flag;
                G_FMessage_Code := G_Message_Code || '050';
                GOTO Get_Out;
            END IF; --}
        ELSE --}{
            R_Securities.Ext_Ret_Flag := 'Y';
        END IF; --}
    END IF; --}
    IF R_SPC.Maturity_Schedule_Flag = 'N'
    THEN --{
        IF R_Mat_Sched.Maturity_Date IS NOT NULL
        THEN --{
            Field_Name := C_Maturity_Date ||'&'||C_Security_Type;
            R_Field    := R_Mat_Sched.Maturity_Date;
            G_FMessage_Code := G_Message_Code || '050';
            GOTO Get_Out;
        ELSE --}{
            R_Securities.Mat_Date_Sel_Method_Code := NULL;
        END IF; --}
    ELSIF R_SPC.Maturity_Schedule_Flag = 'M'
      AND R_Mat_Sched.Maturity_Date IS NULL
    THEN --}{
      IF R_SPC.Perpetual_Flag <> 'N'
      AND R_Securities.Perpetual_Flag = 'Y'
      THEN
        NULL;
      ELSE
        Field_Name := C_Maturity_Date ||'&'||C_Security_Type;
        R_Field    := R_Mat_Sched.Maturity_Date;
        G_FMessage_Code := G_Message_Code || '050';
        GOTO Get_Out;
      END IF; --}
    END IF; --}
    IF R_SPC.Purchase_Fund_Flag = 'N'
    THEN
        R_Securities.Purchase_Fund_Flag := 'N';
    ELSIF R_SPC.Purchase_Fund_Flag = 'M'
    THEN
        R_Securities.Purchase_Fund_Flag := 'Y';
    END IF;
    IF R_SPC.Sinking_Fund_Flag = 'N'
    THEN
        R_Securities.Sinking_Fund_Flag := 'N';
    ELSIF R_SPC.Sinking_Fund_Flag = 'M'
    THEN
        R_Securities.Sinking_Fund_Flag := 'Y';
    END IF;
    IF R_SPC.Split_Flag = 'N'
    THEN
        R_Securities.Split_Flag := 'N';
    ELSIF R_SPC.Split_Flag = 'M'
    THEN
        R_Securities.Split_Flag := 'Y';
    END IF;
    IF R_SPC.Perpetual_Flag = 'N'
    THEN
        R_Securities.Perpetual_Flag := 'N';
    ELSIF R_SPC.Perpetual_Flag = 'M'
    THEN
        R_Securities.Perpetual_Flag := 'Y';
    END IF;
    --
<<Get_Out>>
    NULL;
--
END Verify_Security_Ptrn_Controls; --}}
--
----------------------------------------------------------------------
-- Initialize field definitions
----------------------------------------------------------------------
--
PROCEDURE Initialize_Index    (
          Prc_Msgs_Rec    IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Create_Issuer    OUT NOCOPY BOOLEAN
        , Success            OUT NOCOPY BOOLEAN
        ) IS
--{
BEGIN
    --{
    Success            := FALSE;
    G_Number_of_Fields := 15;
    --
    GT_Fields := FieldList( NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          );
    --
    GT_Fields(01).F_Name       := C_Exch_Symbol;
    GT_Fields(01).F_Max_Length := 15;
    GT_Fields(01).F_Numeric    := FALSE;
    GT_Fields(01).F_Format     := NULL;
    GT_Fields(01).F_Mandatory  := TRUE;
    GT_Fields(02).F_Name       := C_Exch_Source_1;
    GT_Fields(02).F_Max_Length := 4;
    GT_Fields(02).F_Numeric    := FALSE;
    GT_Fields(02).F_Format     := NULL;
    GT_Fields(02).F_Mandatory  := TRUE;
    GT_Fields(03).F_Name       := C_Exch_Source_2; -- source (total return opt!)
    GT_Fields(03).F_Max_Length := 4;
    GT_Fields(03).F_Numeric    := FALSE;
    GT_Fields(03).F_Format     := NULL;
    GT_Fields(03).F_Mandatory  := FALSE;
    GT_Fields(04).F_Name       := C_Sec_Long_Name;
    GT_Fields(04).F_Max_Length := 50;
    GT_Fields(04).F_Numeric    := FALSE;
    GT_Fields(04).F_Format     := NULL;
    GT_Fields(04).F_Mandatory  := TRUE;
    GT_Fields(05).F_Name       := C_Issuer_Code;
    GT_Fields(05).F_Max_Length := 12;
    GT_Fields(05).F_Numeric    := FALSE;
    GT_Fields(05).F_Format     := NULL;
    GT_Fields(05).F_Mandatory  := TRUE;
    GT_Fields(06).F_Name       := C_Security_Type;
    GT_Fields(06).F_Max_Length := 3;
    GT_Fields(06).F_Numeric    := FALSE;
    GT_Fields(06).F_Format     := NULL;
    GT_Fields(06).F_Mandatory  := TRUE;
    GT_Fields(07).F_Name       := C_Security_Sub_Type;
    GT_Fields(07).F_Max_Length := 6;
    GT_Fields(07).F_Numeric    := FALSE;
    GT_Fields(07).F_Format     := NULL;
    GT_Fields(07).F_Mandatory  := TRUE;
    GT_Fields(08).F_Name       := C_ISO_Ccy_Code;
    GT_Fields(08).F_Max_Length := 3;
    GT_Fields(08).F_Numeric    := FALSE;
    GT_Fields(08).F_Format     := NULL;
    GT_Fields(08).F_Mandatory  := TRUE;
    GT_Fields(09).F_Name       := C_Income_Type;
    GT_Fields(09).F_Max_Length := 1;
    GT_Fields(09).F_Numeric    := FALSE;
    GT_Fields(09).F_Format     := NULL;
    GT_Fields(09).F_Mandatory  := TRUE;
    GT_Fields(10).F_Name       := C_Income_Freq;
    GT_Fields(10).F_Max_Length := 3;
    GT_Fields(10).F_Numeric    := FALSE;
    GT_Fields(10).F_Format     := NULL;
    GT_Fields(10).F_Mandatory  := TRUE;
    GT_Fields(11).F_Name       := C_Index_Category;  -- Category
    GT_Fields(11).F_Max_Length := 64;
    GT_Fields(11).F_Numeric    := FALSE;
    GT_Fields(11).F_Format     := NULL;
    GT_Fields(11).F_Mandatory  := FALSE;
    GT_Fields(12).F_Name       := C_Index_Base_Date;
    GT_Fields(12).F_Max_Length := 10;
    GT_Fields(12).F_Numeric    := FALSE;
    GT_Fields(12).F_Format     := 'YYYY-MM-DD';
    GT_Fields(12).F_Mandatory  := FALSE;
    GT_Fields(13).F_Name       := C_Index_Base_Value;
    GT_Fields(13).F_Max_Length := 10;
    GT_Fields(13).F_Numeric    := TRUE;
    GT_Fields(13).F_Format     := NULL;
    GT_Fields(13).F_Mandatory  := FALSE;
    GT_Fields(14).F_Name       := C_Index_Rebalance_Frequency;
    GT_Fields(14).F_Max_Length := 3;
    GT_Fields(14).F_Numeric    := FALSE;
    GT_Fields(14).F_Format     := NULL;
    GT_Fields(14).F_Mandatory  := FALSE;
    GT_Fields(15).F_Name       := C_Index_Description;
    GT_Fields(15).F_Max_Length := 400;
    GT_Fields(15).F_Numeric    := FALSE;
    GT_Fields(15).F_Format     := NULL;
    GT_Fields(15).F_Mandatory  := FALSE;
    --
    -- Fields by default
    --
    G_Acc_Calc_Formula       := 'SIU';
    G_Acc_Freq_Value         := 365;
    G_Book_Based_Flag        := 'N';
    G_Const_Prc_Flag         := 'N';
    G_Day_Count              := '0';
    G_Gen_Pay_Flag           := 'N';
    G_Income_Type            := 'I';
    G_Intangibility_Flag     := 'N';
    G_Mat_Date_Method        := 'R';
    G_Maturity_Type          := 'STD';
    G_MTN_Flag               := 'N';
    G_Perp_Flag              := 'N';
    G_E_R_Flag               := 'N';
    G_Call_Flag              := 'N';
    G_Conv_Flag              := 'N';
    G_Par_Value              := 100;
    G_Par_Fri_Cy             := 'US$';
    G_Pur_Fund_Flag          := 'N';
    G_Ratings_Flag           := 'N';
    G_Redemption_Amount      := 100;
    G_Redemption_Currency    := 'US$';
    G_SAC_Changed_Flag       := 'N';
    G_Site_Date              := Constants.Get_Site_Date;
    G_Splits_Flag            := 'N';
    G_Sink_Fund_Flag         := 'N';
    G_Trd_Status             := 'A';
    G_User_ID                := Constants.Get_User_ID;
    G_Freq_Value             := NULL;
    G_Sec_Type               := NULL;
    G_Sec_SubType            := NULL;
    G_Id_Syst                := NULL;
    --
    G_More_Header            := 'Index Code/Ticker/EXCHG'||C_TAB||'%';
    G_Track_Progress         := TRUE;
    G_Many_Tickers           := FALSE;
    --
    P_Create_Issuer          := FALSE;
    G_Issuer_Generic         := 'IX';
    G_MAY_EXISTS             := FALSE;
    --
    G_Index_Details          := TRUE;
    --
    Success := TRUE;
    --
EXCEPTION
    WHEN OTHERS
    THEN
        Prc_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
        Success := FALSE;
    --}}
END Initialize_Index;
--
----------------------------------------------------------------------
-- Initialize field definitions
----------------------------------------------------------------------
--
PROCEDURE Initialize_Fgn_Bonds(
          Prc_Msgs_Rec    IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Create_Issuer    OUT NOCOPY BOOLEAN
        , Success            OUT NOCOPY BOOLEAN
        ) IS
--{
BEGIN
    --{
    Success            := FALSE;
    G_Number_of_Fields := 26;
    --
    GT_Fields := FieldList( NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          );
    --
    GT_Fields(01).F_Name       := C_Ignore;
    GT_Fields(01).F_Max_Length := 50;
    GT_Fields(01).F_Numeric    := FALSE;
    GT_Fields(01).F_Format     := NULL;
    GT_Fields(01).F_Mandatory  := FALSE;
    GT_Fields(02).F_Name       := C_Ignore;
    GT_Fields(02).F_Max_Length := 50;
    GT_Fields(02).F_Numeric    := FALSE;
    GT_Fields(02).F_Format     := NULL;
    GT_Fields(02).F_Mandatory  := FALSE;
    GT_Fields(03).F_Name       := C_Ignore;
    GT_Fields(03).F_Max_Length := 50;
    GT_Fields(03).F_Numeric    := FALSE;
    GT_Fields(03).F_Format     := NULL;
    GT_Fields(03).F_Mandatory  := FALSE;
    GT_Fields(04).F_Name       := C_Ignore;
    GT_Fields(04).F_Max_Length := 50;
    GT_Fields(04).F_Numeric    := FALSE;
    GT_Fields(04).F_Format     := NULL;
    GT_Fields(04).F_Mandatory  := FALSE;
    GT_Fields(05).F_Name       := C_Issuer_Code;
    GT_Fields(05).F_Max_Length := 12;
    GT_Fields(05).F_Numeric    := FALSE;
    GT_Fields(05).F_Format     := NULL;
    GT_Fields(05).F_Mandatory  := FALSE;
    GT_Fields(06).F_Name       := C_Ignore;
    GT_Fields(06).F_Max_Length := 50;
    GT_Fields(06).F_Numeric    := FALSE;
    GT_Fields(06).F_Format     := NULL;
    GT_Fields(06).F_Mandatory  := FALSE;
    GT_Fields(07).F_Name       := C_Coupon;
    GT_Fields(07).F_Max_Length := 50;
    GT_Fields(07).F_Numeric    := TRUE;
    GT_Fields(07).F_Format     := NULL;
    GT_Fields(07).F_Mandatory  := FALSE;
    GT_Fields(08).F_Name       := C_Maturity_Date;
    GT_Fields(08).F_Max_Length := 50;
    GT_Fields(08).F_Numeric    := FALSE;
    GT_Fields(08).F_Format     := 'DD-Mon-YY';
    GT_Fields(08).F_Mandatory  := FALSE;
    GT_Fields(09).F_Name       := C_Sec_Long_Name;
    GT_Fields(09).F_Max_Length := 50;
    GT_Fields(09).F_Numeric    := FALSE;
    GT_Fields(09).F_Format     := NULL;
    GT_Fields(09).F_Mandatory  := FALSE;
    GT_Fields(10).F_Name       := C_Market_Code;
    GT_Fields(10).F_Max_Length := 4;
    GT_Fields(10).F_Numeric    := FALSE;
    GT_Fields(10).F_Format     := NULL;
    GT_Fields(10).F_Mandatory  := FALSE;
    GT_Fields(11).F_Name       := C_ISO_Ccy_Code;
    GT_Fields(11).F_Max_Length := 3;
    GT_Fields(11).F_Numeric    := FALSE;
    GT_Fields(11).F_Format     := NULL;
    GT_Fields(11).F_Mandatory  := FALSE;
    GT_Fields(12).F_Name       := C_Ignore;
    GT_Fields(12).F_Max_Length := 50;
    GT_Fields(12).F_Numeric    := FALSE;
    GT_Fields(12).F_Format     := NULL;
    GT_Fields(12).F_Mandatory  := FALSE;
    GT_Fields(13).F_Name       := C_Income_Freq;
    GT_Fields(13).F_Max_Length := 3;
    GT_Fields(13).F_Numeric    := FALSE;
    GT_Fields(13).F_Format     := NULL;
    GT_Fields(13).F_Mandatory  := FALSE;
    GT_Fields(14).F_Name       := C_Pricing_Note;
    GT_Fields(14).F_Max_Length := 240;
    GT_Fields(14).F_Numeric    := FALSE;
    GT_Fields(14).F_Format     := NULL;
    GT_Fields(14).F_Mandatory  := FALSE;
    GT_Fields(15).F_Name       := C_Security_Type;
    GT_Fields(15).F_Max_Length := 3;
    GT_Fields(15).F_Numeric    := FALSE;
    GT_Fields(15).F_Format     := NULL;
    GT_Fields(15).F_Mandatory  := FALSE;
    GT_Fields(16).F_Name       := C_Security_Sub_Type;
    GT_Fields(16).F_Max_Length := 6;
    GT_Fields(16).F_Numeric    := FALSE;
    GT_Fields(16).F_Format     := NULL;
    GT_Fields(16).F_Mandatory  := FALSE;
    GT_Fields(17).F_Name       := C_Call_Flag;
    GT_Fields(17).F_Max_Length := 1;
    GT_Fields(17).F_Numeric    := FALSE;
    GT_Fields(17).F_Format     := NULL;
    GT_Fields(17).F_Mandatory  := FALSE;
    GT_Fields(18).F_Name       := C_Floating_Flag;
    GT_Fields(18).F_Max_Length := 1;
    GT_Fields(18).F_Numeric    := FALSE;
    GT_Fields(18).F_Format     := NULL;
    GT_Fields(18).F_Mandatory  := FALSE;
    GT_Fields(19).F_Name       := C_Convertible_Flag;
    GT_Fields(19).F_Max_Length := 1;
    GT_Fields(19).F_Numeric    := FALSE;
    GT_Fields(19).F_Format     := NULL;
    GT_Fields(19).F_Mandatory  := FALSE;
    GT_Fields(20).F_Name       := C_E_R_Flag;
    GT_Fields(20).F_Max_Length := 1;
    GT_Fields(20).F_Numeric    := FALSE;
    GT_Fields(20).F_Format     := NULL;
    GT_Fields(20).F_Mandatory  := FALSE;
    GT_Fields(21).F_Name       := C_Perpetual_Flag;
    GT_Fields(21).F_Max_Length := 1;
    GT_Fields(21).F_Numeric    := FALSE;
    GT_Fields(21).F_Format     := NULL;
    GT_Fields(21).F_Mandatory  := FALSE;
    GT_Fields(22).F_Name       := C_CUSIP;
    GT_Fields(22).F_Max_Length := 9;
    GT_Fields(22).F_Numeric    := FALSE;
    GT_Fields(22).F_Format     := NULL;
    GT_Fields(22).F_Mandatory  := FALSE;
    GT_Fields(23).F_Name       := C_ISIN;
    GT_Fields(23).F_Max_Length := 12;
    GT_Fields(23).F_Numeric    := FALSE;
    GT_Fields(23).F_Format     := NULL;
    GT_Fields(23).F_Mandatory  := FALSE;
    GT_Fields(24).F_Name       := C_OISIN;
    GT_Fields(24).F_Max_Length := 12;
    GT_Fields(24).F_Numeric    := FALSE;
    GT_Fields(24).F_Format     := NULL;
    GT_Fields(24).F_Mandatory  := FALSE;
    GT_Fields(25).F_Name       := C_SEDOL;
    GT_Fields(25).F_Max_Length := 7;
    GT_Fields(25).F_Numeric    := FALSE;
    GT_Fields(25).F_Format     := NULL;
    GT_Fields(25).F_Mandatory  := FALSE;
    GT_Fields(26).F_Name       := C_SEDOL_ISO_Nation;
    GT_Fields(26).F_Max_Length := 2;
    GT_Fields(26).F_Numeric    := FALSE;
    GT_Fields(26).F_Format     := NULL;
    GT_Fields(26).F_Mandatory  := FALSE;
    --
    -- Fields by default
    --
    G_Acc_Calc_Formula       := 'SIU';
    G_Acc_Freq_Value         := 365;
    G_Book_Based_Flag        := 'N';
    G_Const_Prc_Flag         := 'N';
    G_Day_Count              := '0';
    G_Gen_Pay_Flag           := 'N';
    G_Income_Type            := 'I';
    G_Intangibility_Flag     := 'N';
    G_Mat_Date_Method        := 'R';
    G_Maturity_Type          := 'STD';
    G_MTN_Flag               := 'N';
    G_Perp_Flag              := 'N';
    G_E_R_Flag               := 'N';
    G_Call_Flag              := 'N';
    G_Conv_Flag              := 'N';
    G_Par_Value              := 100;
    G_Par_Fri_Cy             := 'US$';
    G_Pur_Fund_Flag          := 'N';
    G_Ratings_Flag           := 'N';
    G_Redemption_Amount      := 100;
    G_Redemption_Currency    := 'US$';
    G_SAC_Changed_Flag       := 'N';
    G_Site_Date              := Constants.Get_Site_Date;
    G_Splits_Flag            := 'N';
    G_Sink_Fund_Flag         := 'N';
    G_Trd_Status             := 'A';
    G_User_ID                := Constants.Get_User_ID;
    G_Freq_Value             := NULL;
    G_Sec_Type               := NULL;
    G_Sec_SubType            := NULL;
    --
    G_More_Header            := '1'||C_Tab||'2'||C_Tab||'3'||C_TAB||'%';
    G_Track_Progress         := TRUE;
    G_Many_Tickers           := FALSE;
    --
    P_Create_Issuer          := FALSE;
    G_Issuer_Generic         := 'FB';
    G_MAY_EXISTS             := FALSE;
    --
    Success := TRUE;
    --
EXCEPTION
    WHEN OTHERS
    THEN
        Prc_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
        Success := FALSE;
    --}}
END Initialize_Fgn_Bonds;
--
--
----------------------------------------------------------------------
-- Initialize field definitions
----------------------------------------------------------------------
--
PROCEDURE Initialize_Fgn_Stocks(
          Prc_Msgs_Rec    IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Create_Issuer    OUT NOCOPY BOOLEAN
        , Success            OUT NOCOPY BOOLEAN
        ) IS
--{
BEGIN
    --{
    Success            := FALSE;
    G_Number_of_Fields := 28;
    --
    GT_Fields := FieldList( NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          );
    --
    GT_Fields(01).F_Name       := C_Issuer_Name;
    GT_Fields(01).F_Max_Length := 60;
    GT_Fields(01).F_Numeric    := FALSE;
    GT_Fields(01).F_Format     := NULL;
    GT_Fields(01).F_Mandatory  := TRUE;
    GT_Fields(02).F_Name       := C_Issuer_Nation;
    GT_Fields(02).F_Max_Length := 3;
    GT_Fields(02).F_Numeric    := FALSE;
    GT_Fields(02).F_Format     := NULL;
    GT_Fields(02).F_Mandatory  := TRUE;
    GT_Fields(03).F_Name       := C_Issuer_Type;
    GT_Fields(03).F_Max_Length := 3;
    GT_Fields(03).F_Numeric    := FALSE;
    GT_Fields(03).F_Format     := NULL;
    GT_Fields(03).F_Mandatory  := TRUE;
    GT_Fields(04).F_Name       := C_Issuer_Sub_type;
    GT_Fields(04).F_Max_Length := 3;
    GT_Fields(04).F_Numeric    := FALSE;
    GT_Fields(04).F_Format     := NULL;
    GT_Fields(04).F_Mandatory  := TRUE;
    GT_Fields(05).F_Name       := C_CUSIP;
    GT_Fields(05).F_Max_Length := 9;
    GT_Fields(05).F_Numeric    := FALSE;
    GT_Fields(05).F_Format     := NULL;
    GT_Fields(05).F_Mandatory  := FALSE;
    GT_Fields(06).F_Name       := C_Security_Type;
    GT_Fields(06).F_Max_Length := 3;
    GT_Fields(06).F_Numeric    := FALSE;
    GT_Fields(06).F_Format     := NULL;
    GT_Fields(06).F_Mandatory  := TRUE;
    GT_Fields(07).F_Name       := C_Security_Sub_Type;
    GT_Fields(07).F_Max_Length := 6;
    GT_Fields(07).F_Numeric    := FALSE;
    GT_Fields(07).F_Format     := NULL;
    GT_Fields(07).F_Mandatory  := TRUE;
    GT_Fields(08).F_Name       := C_Issuer_Code;
    GT_Fields(08).F_Max_Length := 12;
    GT_Fields(08).F_Numeric    := FALSE;
    GT_Fields(08).F_Format     := NULL;
    GT_Fields(08).F_Mandatory  := FALSE;
    GT_Fields(09).F_Name       := C_Sec_Long_Name;
    GT_Fields(09).F_Max_Length := 50;
    GT_Fields(09).F_Numeric    := FALSE;
    GT_Fields(09).F_Format     := NULL;
    GT_Fields(09).F_Mandatory  := FALSE;
    GT_Fields(10).F_Name       := C_Income_Type;
    GT_Fields(10).F_Max_Length := 1;
    GT_Fields(10).F_Numeric    := FALSE;
    GT_Fields(10).F_Format     := NULL;
    GT_Fields(10).F_Mandatory  := FALSE;
    GT_Fields(11).F_Name       := C_Income_Freq;
    GT_Fields(11).F_Max_Length := 3;
    GT_Fields(11).F_Numeric    := FALSE;
    GT_Fields(11).F_Format     := NULL;
    GT_Fields(11).F_Mandatory  := FALSE;
    GT_Fields(12).F_Name       := C_Serie;
    GT_Fields(12).F_Max_Length := 10;
    GT_Fields(12).F_Numeric    := FALSE;
    GT_Fields(12).F_Format     := NULL;
    GT_Fields(12).F_Mandatory  := FALSE;
    GT_Fields(13).F_Name       := C_Class;
    GT_Fields(13).F_Max_Length := 1;
    GT_Fields(13).F_Numeric    := FALSE;
    GT_Fields(13).F_Format     := NULL;
    GT_Fields(13).F_Mandatory  := FALSE;
    GT_Fields(14).F_Name       := C_Trading_Source;
    GT_Fields(14).F_Max_Length := 4;
    GT_Fields(14).F_Numeric    := FALSE;
    GT_Fields(14).F_Format     := NULL;
    GT_Fields(14).F_Mandatory  := TRUE;
    GT_Fields(15).F_Name       := C_Trading_Currency;
    GT_Fields(15).F_Max_Length := 3;
    GT_Fields(15).F_Numeric    := FALSE;
    GT_Fields(15).F_Format     := NULL;
    GT_Fields(15).F_Mandatory  := TRUE;
    GT_Fields(16).F_Name       := C_RIC_Symbol;
-- Modification - JSL - 25 septembre 2015
--  GT_Fields(16).F_Max_Length := 15;
    GT_Fields(16).F_Max_Length := 30;
-- Fin de modification - JSL - 25 septembre 2015
    GT_Fields(16).F_Numeric    := FALSE;
    GT_Fields(16).F_Format     := NULL;
    GT_Fields(16).F_Mandatory  := FALSE;
    GT_Fields(17).F_Name       := C_RIC_Currency;
    GT_Fields(17).F_Max_Length := 3;
    GT_Fields(17).F_Numeric    := FALSE;
    GT_Fields(17).F_Format     := NULL;
    GT_Fields(17).F_Mandatory  := FALSE;
    GT_Fields(18).F_Name       := C_RIC_Source;
    GT_Fields(18).F_Max_Length := 4;
    GT_Fields(18).F_Numeric    := FALSE;
    GT_Fields(18).F_Format     := NULL;
    GT_Fields(18).F_Mandatory  := FALSE;
    GT_Fields(19).F_Name       := C_SEDOL;
    GT_Fields(19).F_Max_Length := 7;
    GT_Fields(19).F_Numeric    := FALSE;
    GT_Fields(19).F_Format     := NULL;
    GT_Fields(19).F_Mandatory  := FALSE;
    GT_Fields(20).F_Name       := C_SEDOL_Currency;
    GT_Fields(20).F_Max_Length := 3;
    GT_Fields(20).F_Numeric    := FALSE;
    GT_Fields(20).F_Format     := NULL;
    GT_Fields(20).F_Mandatory  := FALSE;
    GT_Fields(21).F_Name       := C_CINS_Symbol;
    GT_Fields(21).F_Max_Length := 9;
    GT_Fields(21).F_Numeric    := FALSE;
    GT_Fields(21).F_Format     := NULL;
    GT_Fields(21).F_Mandatory  := FALSE;
    GT_Fields(22).F_Name       := C_CINS_Currency;
    GT_Fields(22).F_Max_Length := 3;
    GT_Fields(22).F_Numeric    := FALSE;
    GT_Fields(22).F_Format     := NULL;
    GT_Fields(22).F_Mandatory  := FALSE;
    GT_Fields(23).F_Name       := C_OISIN;
    GT_Fields(23).F_Max_Length := 12;
    GT_Fields(23).F_Numeric    := FALSE;
    GT_Fields(23).F_Format     := NULL;
    GT_Fields(23).F_Mandatory  := FALSE;
    GT_Fields(24).F_Name       := C_OISIN_Currency;
    GT_Fields(24).F_Max_Length := 3;
    GT_Fields(24).F_Numeric    := FALSE;
    GT_Fields(24).F_Format     := NULL;
    GT_Fields(24).F_Mandatory  := FALSE;
    GT_Fields(25).F_Name       := C_Maturity_Type;
    GT_Fields(25).F_Max_Length := 3;
    GT_Fields(25).F_Numeric    := FALSE;
    GT_Fields(25).F_Format     := NULL;
    GT_Fields(25).F_Mandatory  := FALSE;
    GT_Fields(26).F_Name       := C_Maturity_Date;
    GT_Fields(26).F_Max_Length := 11;
    GT_Fields(26).F_Numeric    := FALSE;
    GT_Fields(26).F_Format     := 'DD-MON-YYYY';
    GT_Fields(26).F_Mandatory  := FALSE;
    GT_Fields(27).F_Name       := C_Redemption;
    GT_Fields(27).F_Max_Length := 50;
    GT_Fields(27).F_Numeric    := TRUE;
    GT_Fields(27).F_Format     := NULL;
    GT_Fields(27).F_Mandatory  := FALSE;
    GT_Fields(28).F_Name       := C_Redemption_Currency;
    GT_Fields(28).F_Max_Length := 3;
    GT_Fields(28).F_Numeric    := FALSE;
    GT_Fields(28).F_Format     := NULL;
    GT_Fields(28).F_Mandatory  := FALSE;
    --
    -- Fields by default
    --
    G_Acc_Calc_Formula       := 'SIU';
    G_Acc_Freq_Value         := 365;
    G_Book_Based_Flag        := 'N';
    G_Const_Prc_Flag         := 'N';
    G_Day_Count              := '0';
    G_Gen_Pay_Flag           := 'N';
    G_Income_Type            := 'D';
    G_Intangibility_Flag     := 'N';
    G_Mat_Date_Method        := 'R';
    G_Maturity_Type          := 'STD';
    G_MTN_Flag               := 'N';
    G_Perp_Flag              := 'N';
    G_E_R_Flag               := 'N';
    G_Call_Flag              := 'N';
    G_Conv_Flag              := 'N';
    G_Par_Value              := NULL;
    G_Par_Fri_Cy             := NULL;
    G_Pur_Fund_Flag          := 'N';
    G_Ratings_Flag           := 'N';
    G_Redemption_Amount      := NULL;
    G_Redemption_Currency    := NULL;
    G_SAC_Changed_Flag       := 'N';
    G_Site_Date              := Constants.Get_Site_Date;
    G_Splits_Flag            := 'Y';
    G_Sink_Fund_Flag         := 'N';
    G_Trd_Status             := 'A';
    G_User_ID                := Constants.Get_User_ID;
    G_Freq_Value             := NULL;
    G_Sec_Type               := NULL;
    G_Sec_SubType            := NULL;
    --
    G_More_Header            := 'NAME'||C_Tab||'%';
    G_Track_Progress         := TRUE;
    G_Many_Tickers           := FALSE;
    --
    P_Create_Issuer          := TRUE;
    G_Issuer_Generic         := 'FS';
-- FS issuer codes all used, now using FT - JSL - 10 decembre 2008
    G_Issuer_Generic         := 'FT';
    G_MAY_EXISTS             := TRUE;
    --
    Success := TRUE;
    --
EXCEPTION
    WHEN OTHERS
    THEN
        Prc_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
        Success := FALSE;
    --}}
END Initialize_Fgn_Stocks;
--
--
----------------------------------------------------------------------
-- Initialize field definitions
----------------------------------------------------------------------
--
PROCEDURE Initialize_Muni_Bonds(
          Prc_Msgs_Rec    IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Create_Issuer    OUT NOCOPY BOOLEAN
        , Success            OUT NOCOPY BOOLEAN
        ) IS
--{
BEGIN
    --{
    Success            := FALSE;
    G_Number_of_Fields := 27;
    --
    GT_Fields := FieldList( NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          );
    --
    GT_Fields(01).F_Name       := C_Ignore;
    GT_Fields(01).F_Max_Length := 50;
    GT_Fields(01).F_Numeric    := FALSE;
    GT_Fields(01).F_Format     := NULL;
    GT_Fields(01).F_Mandatory  := FALSE;
    GT_Fields(02).F_Name       := C_Ignore;
    GT_Fields(02).F_Max_Length := 50;
    GT_Fields(02).F_Numeric    := FALSE;
    GT_Fields(02).F_Format     := NULL;
    GT_Fields(02).F_Mandatory  := FALSE;
    GT_Fields(03).F_Name       := C_Ignore;
    GT_Fields(03).F_Max_Length := 50;
    GT_Fields(03).F_Numeric    := FALSE;
    GT_Fields(03).F_Format     := NULL;
    GT_Fields(03).F_Mandatory  := FALSE;
    GT_Fields(04).F_Name       := C_Issuer_Code;
    GT_Fields(04).F_Max_Length := 12;
    GT_Fields(04).F_Numeric    := FALSE;
    GT_Fields(04).F_Format     := NULL;
    GT_Fields(04).F_Mandatory  := TRUE;
    GT_Fields(05).F_Name       := C_ISO_Ccy_Code;
    GT_Fields(05).F_Max_Length := 3;
    GT_Fields(05).F_Numeric    := FALSE;
    GT_Fields(05).F_Format     := NULL;
    GT_Fields(05).F_Mandatory  := TRUE;
    GT_Fields(06).F_Name       := C_Coupon;
    GT_Fields(06).F_Max_Length := 50;
    GT_Fields(06).F_Numeric    := TRUE;
    GT_Fields(06).F_Format     := NULL;
    GT_Fields(06).F_Mandatory  := FALSE;
    GT_Fields(07).F_Name       := C_Maturity_Date;
    GT_Fields(07).F_Max_Length := 9;
    GT_Fields(07).F_Numeric    := FALSE;
    GT_Fields(07).F_Format     := 'DD-Mon-YY';
    GT_Fields(07).F_Mandatory  := FALSE;
    GT_Fields(08).F_Name       := C_Sec_Long_Name;
    GT_Fields(08).F_Max_Length := 50;
    GT_Fields(08).F_Numeric    := FALSE;
    GT_Fields(08).F_Format     := NULL;
    GT_Fields(08).F_Mandatory  := FALSE;
    GT_Fields(09).F_Name       := C_Market_Code;
    GT_Fields(09).F_Max_Length := 4;
    GT_Fields(09).F_Numeric    := FALSE;
    GT_Fields(09).F_Format     := NULL;
    GT_Fields(09).F_Mandatory  := FALSE;
    GT_Fields(10).F_Name       := C_Trading_Currency;
    GT_Fields(10).F_Max_Length := 3;
    GT_Fields(10).F_Numeric    := FALSE;
    GT_Fields(10).F_Format     := NULL;
    GT_Fields(10).F_Mandatory  := FALSE;
    GT_Fields(11).F_Name       := C_Income_Freq;
    GT_Fields(11).F_Max_Length := 3;
    GT_Fields(11).F_Numeric    := FALSE;
    GT_Fields(11).F_Format     := NULL;
    GT_Fields(11).F_Mandatory  := FALSE;
    GT_Fields(12).F_Name       := C_Security_Type;
    GT_Fields(12).F_Max_Length := 3;
    GT_Fields(12).F_Numeric    := FALSE;
    GT_Fields(12).F_Format     := NULL;
    GT_Fields(12).F_Mandatory  := TRUE;
    GT_Fields(13).F_Name       := C_Security_Sub_Type;
    GT_Fields(13).F_Max_Length := 6;
    GT_Fields(13).F_Numeric    := FALSE;
    GT_Fields(13).F_Format     := NULL;
    GT_Fields(13).F_Mandatory  := TRUE;
    GT_Fields(14).F_Name       := C_Call_Flag;
    GT_Fields(14).F_Max_Length := 1;
    GT_Fields(14).F_Numeric    := FALSE;
    GT_Fields(14).F_Format     := NULL;
    GT_Fields(14).F_Mandatory  := TRUE;
    GT_Fields(15).F_Name       := C_Floating_Flag;
    GT_Fields(15).F_Max_Length := 1;
    GT_Fields(15).F_Numeric    := FALSE;
    GT_Fields(15).F_Format     := NULL;
    GT_Fields(15).F_Mandatory  := TRUE;
    GT_Fields(16).F_Name       := C_Convertible_Flag;
    GT_Fields(16).F_Max_Length := 1;
    GT_Fields(16).F_Numeric    := FALSE;
    GT_Fields(16).F_Format     := NULL;
    GT_Fields(16).F_Mandatory  := TRUE;
    GT_Fields(17).F_Name       := C_E_R_Flag;
    GT_Fields(17).F_Max_Length := 1;
    GT_Fields(17).F_Numeric    := FALSE;
    GT_Fields(17).F_Format     := NULL;
    GT_Fields(17).F_Mandatory  := TRUE;
    GT_Fields(18).F_Name       := C_Perpetual_Flag;
    GT_Fields(18).F_Max_Length := 1;
    GT_Fields(18).F_Numeric    := FALSE;
    GT_Fields(18).F_Format     := NULL;
    GT_Fields(18).F_Mandatory  := TRUE;
    GT_Fields(19).F_Name       := C_Data_Source_Code;
    GT_Fields(19).F_Max_Length := 4;
    GT_Fields(19).F_Numeric    := FALSE;
    GT_Fields(19).F_Format     := NULL;
    GT_Fields(19).F_Mandatory  := TRUE;
    GT_Fields(20).F_Name       := C_Data_Supp_Code;
    GT_Fields(20).F_Max_Length := 4;
    GT_Fields(20).F_Numeric    := FALSE;
    GT_Fields(20).F_Format     := NULL;
    GT_Fields(20).F_Mandatory  := TRUE;
    GT_Fields(21).F_Name       := C_Data_Deliv_Time;
    GT_Fields(21).F_Max_Length := 4;
    GT_Fields(21).F_Numeric    := TRUE;
    GT_Fields(21).F_Format     := NULL;
    GT_Fields(21).F_Mandatory  := TRUE;
    GT_Fields(22).F_Name       := C_Issue_Offer_Date;
    GT_Fields(22).F_Max_Length := 9;
    GT_Fields(22).F_Numeric    := FALSE;
    GT_Fields(22).F_Format     := 'DD-mon-YY';
    GT_Fields(22).F_Mandatory  := TRUE;
    GT_Fields(23).F_Name       := C_Priv_Plac_Flag;
    GT_Fields(23).F_Max_Length := 1;
    GT_Fields(23).F_Numeric    := FALSE;
    GT_Fields(23).F_Format     := NULL;
    GT_Fields(23).F_Mandatory  := TRUE;
    GT_Fields(24).F_Name       := C_Accrual_Start_Date;
    GT_Fields(24).F_Max_Length := 9;
    GT_Fields(24).F_Numeric    := FALSE;
    GT_Fields(24).F_Format     := 'DD-mon-YY';
    GT_Fields(24).F_Mandatory  := TRUE;
    GT_Fields(25).F_Name       := C_First_Pay_Date;
    GT_Fields(25).F_Max_Length := 9;
    GT_Fields(25).F_Numeric    := FALSE;
    GT_Fields(25).F_Format     := 'DD-mon-YY';
    GT_Fields(25).F_Mandatory  := TRUE;
    GT_Fields(26).F_Name       := C_Sched_Event_Flag;
    GT_Fields(26).F_Max_Length := 1;
    GT_Fields(26).F_Numeric    := FALSE;
    GT_Fields(26).F_Format     := NULL;
    GT_Fields(26).F_Mandatory  := TRUE;
    GT_Fields(27).F_Name       := C_CUSIP;
    GT_Fields(27).F_Max_Length := 9;
    GT_Fields(27).F_Numeric    := FALSE;
    GT_Fields(27).F_Format     := NULL;
    GT_Fields(27).F_Mandatory  := FALSE;
    --
    -- Fields by default
    --
    G_Acc_Calc_Formula       := 'SIC';
--
    G_Acc_Freq_Value         := 365;
    G_Book_Based_Flag        := 'N';
    G_Const_Prc_Flag         := 'N';
    G_Day_Count              := '0';
    G_Gen_Pay_Flag           := 'N';
    G_Income_Type            := 'I';
    G_Intangibility_Flag     := 'N';
    G_Mat_Date_Method        := 'R';
    G_Maturity_Type          := 'STD';
    G_MTN_Flag               := 'N';
    G_Perp_Flag              := 'N';
    G_E_R_Flag               := 'N';
    G_Call_Flag              := 'N';
    G_Conv_Flag              := 'N';
    G_Par_Value              := 100;
    G_Par_Fri_Cy             := 'US$';
--
    G_Pur_Fund_Flag          := 'N';
    G_Ratings_Flag           := 'N';
    G_Redemption_Amount      := 100;
    G_Redemption_Currency    := 'US$';
--
    G_SAC_Changed_Flag       := 'N';
    G_Site_Date              := Constants.Get_Site_Date;
    G_Splits_Flag            := 'N';
    G_Sink_Fund_Flag         := 'N';
    G_Trd_Status             := 'A';
    G_User_ID                := Constants.Get_User_ID;
    G_Freq_Value             := NULL;
    G_Sec_Type               := NULL;
    G_Sec_SubType            := NULL;
    G_Many_Tickers           := FALSE;
    --
    G_More_Header            := 'CREATION DATE'||C_TAB||'%';
    G_Track_Progress         := FALSE;
    --
    P_Create_Issuer          := FALSE;
    G_Issuer_Generic         := 'MB';
    G_MAY_EXISTS             := FALSE;
    --
    Success := TRUE;
    --
EXCEPTION
    WHEN OTHERS
    THEN
        Prc_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
        Success := FALSE;
    --}}
END Initialize_Muni_Bonds;
--
----------------------------------------------------------------------
-- Initialize field definitions
----------------------------------------------------------------------
--
PROCEDURE Initialize_Mutual_Funds(
          Prc_Msgs_Rec    IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Create_Issuer    OUT NOCOPY BOOLEAN
        , Success            OUT NOCOPY BOOLEAN
        ) IS
--{
BEGIN
    --{
    Success            := FALSE;
    G_Number_of_Fields := 12;
    --
    GT_Fields := FieldList( NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          , NULL, NULL, NULL, NULL, NULL
                          );
    --
    GT_Fields(01).F_Name       := C_CUSIP;
    GT_Fields(01).F_Max_Length := 9;
    GT_Fields(01).F_Numeric    := FALSE;
    GT_Fields(01).F_Format     := NULL;
    GT_Fields(01).F_Mandatory  := FALSE;
    GT_Fields(02).F_Name       := C_Security_Sub_Type;
    GT_Fields(02).F_Max_Length := 6;
    GT_Fields(02).F_Numeric    := FALSE;
    GT_Fields(02).F_Format     := NULL;
    GT_Fields(02).F_Mandatory  := TRUE;
    GT_Fields(03).F_Name       := C_Issuer_Code;
    GT_Fields(03).F_Max_Length := 12;
    GT_Fields(03).F_Numeric    := FALSE;
    GT_Fields(03).F_Format     := NULL;
    GT_Fields(03).F_Mandatory  := TRUE;
    GT_Fields(04).F_Name       := C_Sec_Long_Name;
    GT_Fields(04).F_Max_Length := 50;
    GT_Fields(04).F_Numeric    := FALSE;
    GT_Fields(04).F_Format     := NULL;
    GT_Fields(04).F_Mandatory  := TRUE;
    GT_Fields(05).F_Name       := C_Serie;
    GT_Fields(05).F_Max_Length := 10;
    GT_Fields(05).F_Numeric    := FALSE;
    GT_Fields(05).F_Format     := NULL;
    GT_Fields(05).F_Mandatory  := FALSE;
    GT_Fields(06).F_Name       := C_Class;
    GT_Fields(06).F_Max_Length := 1;
    GT_Fields(06).F_Numeric    := FALSE;
    GT_Fields(06).F_Format     := NULL;
    GT_Fields(06).F_Mandatory  := FALSE;
    GT_Fields(07).F_Name       := C_Trading_Source;
    GT_Fields(07).F_Max_Length := 4;
    GT_Fields(07).F_Numeric    := FALSE;
    GT_Fields(07).F_Format     := NULL;
    GT_Fields(07).F_Mandatory  := TRUE;
    GT_Fields(08).F_Name       := C_Trading_Currency;
    GT_Fields(08).F_Max_Length := 3;
    GT_Fields(08).F_Numeric    := FALSE;
    GT_Fields(08).F_Format     := NULL;
    GT_Fields(08).F_Mandatory  := TRUE;
    GT_Fields(09).F_Name       := C_Ticker_Mnemonic;
    GT_Fields(09).F_Max_Length := 5;
    GT_Fields(09).F_Numeric    := FALSE;
    GT_Fields(09).F_Format     := NULL;
    GT_Fields(09).F_Mandatory  := TRUE;
    GT_Fields(10).F_Name       := C_Ticker_Symbol;
    GT_Fields(10).F_Max_Length := 15;
    GT_Fields(10).F_Numeric    := FALSE;
    GT_Fields(10).F_Format     := NULL;
    GT_Fields(10).F_Mandatory  := TRUE;
    GT_Fields(11).F_Name       := C_Ticker_Currency;
    GT_Fields(11).F_Max_Length := 3;
    GT_Fields(11).F_Numeric    := FALSE;
    GT_Fields(11).F_Format     := NULL;
    GT_Fields(11).F_Mandatory  := TRUE;
    GT_Fields(12).F_Name       := C_Ticker_Source;
    GT_Fields(12).F_Max_Length := 4;
    GT_Fields(12).F_Numeric    := FALSE;
    GT_Fields(12).F_Format     := NULL;
    GT_Fields(12).F_Mandatory  := TRUE;
    --
    -- Fields by default
    --
    G_Acc_Calc_Formula       := NULL;
    G_Acc_Freq_Value         := NULL;
    G_Book_Based_Flag        := 'N';
    G_Const_Prc_Flag         := 'N';
    G_Day_Count              := '0';
    G_Gen_Pay_Flag           := 'N';
    G_Income_Type            := 'D';
    G_Intangibility_Flag     := 'N';
    G_Mat_Date_Method        := NULL;
    G_Maturity_Type          := NULL;
    G_MTN_Flag               := 'N';
    G_Perp_Flag              := 'N';
    G_E_R_Flag               := 'N';
    G_Call_Flag              := 'N';
    G_Conv_Flag              := 'N';
    G_Par_Value              := NULL;
    G_Par_Fri_Cy             := NULL;
    G_Pur_Fund_Flag          := 'N';
    G_Ratings_Flag           := 'N';
    G_Redemption_Amount      := NULL;
    G_Redemption_Currency    := NULL;
    G_SAC_Changed_Flag       := 'N';
    G_Site_Date              := Constants.Get_Site_Date;
    G_Splits_Flag            := 'N';
    G_Sink_Fund_Flag         := 'N';
    G_Trd_Status             := 'A';
    G_User_ID                := Constants.Get_User_ID;
    G_Freq_Value             := 0 ; -- 'NF'
    G_Sec_Type               := 'MF';
    G_Sec_SubType            := 'NONE';
    --
    G_More_Header            := 'CREATION DATE'||C_TAB||'%';
    G_Track_Progress         := FALSE;
    G_Many_Tickers           := TRUE;
    --
    P_Create_Issuer          := FALSE;
    G_Issuer_Generic         := 'MG';
    G_MAY_EXISTS             := TRUE;
    --
    Success := TRUE;
    --
EXCEPTION
    WHEN OTHERS
    THEN
        Prc_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
        Success := FALSE;
    --}}
END Initialize_Mutual_Funds;
--
----------------------------------------------------------------------
-- Automatic initialisation of field definitions
----------------------------------------------------------------------
--
PROCEDURE Initialize_Auto_Fields(
          Record_Read     IN            VARCHAR2
        , P_Log_File      IN            Utl_File.File_Type
        , Prc_Msgs_Rec    IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , Success            OUT NOCOPY BOOLEAN
        ) IS
--{
  Start_Position INTEGER;
  End_Position   INTEGER;
  Temp_Length    INTEGER;
  Temp_Name      VARCHAR2(32);
  V_Message      VARCHAR2(256);
  --
  B_MSMF         BOOLEAN := FALSE;

-- Ajout - JSL - 15 decembre 2015
  V_Parm           Ops_Scripts_Parms%ROWTYPE;
-- Fin d'ajout - JSL - 15 decembre 2015
  --
BEGIN
    --{
    IF G_Tracing
    THEN
        pgmloc             := 2080;
        Load_Supplier_Rules.Write_File ( P_Log_File
                                       , 'Initialize_Auto_Fields'
                                       , Prc_Msgs_Rec
                                       , Success
                                       );
        IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
    END IF;
    --
    B_MSMF             := FALSE;
    Success            := FALSE;
    G_Number_of_Fields := 0;
    --
    GT_Fields := FieldList();
    --
    --
    -- Parse the line
    --
    Start_Position := 1;
    End_Position   := 0;
    G_Number_of_Fields := 0;
    WHILE Start_Position < LENGTH( Record_Read )
    AND   G_Number_Of_Fields < GT_Fields.LIMIT
    LOOP --{
        pgmloc := 2090;
        End_Position   := INSTR( Record_Read
                               , C_Tab
                               , Start_Position
                               , 1
                               );
        pgmloc := 2100;
        IF End_Position <> 0
        THEN
            Temp_Length := End_Position - Start_Position;
        ELSE
            Temp_Length := LENGTH( Record_Read ) - Start_Position + 1;
            End_Position := LENGTH( Record_Read );
        END IF;
        --
        pgmloc := 2110;
        G_Number_of_Fields := G_Number_Of_Fields + 1;
        GT_Fields.Extend;
        GT_Fields(G_Number_Of_Fields).F_Numeric   := FALSE;
        GT_Fields(G_Number_Of_Fields).F_Format    := NULL;
        GT_Fields(G_Number_Of_Fields).F_Mandatory := FALSE;
        --
        pgmloc := 2120;
        Temp_name := UPPER( SUBSTR( Record_Read, Start_position, Temp_Length ) );
        IF G_Tracing
        THEN
            pgmloc             := 2130;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , 'Field ' || G_Number_Of_Fields
                                          || ' : ''' || Temp_Name || ''''
                                           , Prc_Msgs_Rec
                                           , Success
                                           );
            IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF;
    --
        pgmloc := 2140;
        IF Temp_Name IS NULL
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Ignore;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 500;
        ELSIF Temp_Name = UPPER( C_Coupon )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Coupon;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Issuer_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 12;
            GT_Fields(G_Number_Of_Fields).F_Mandatory  := TRUE;
        ELSIF Temp_Name = UPPER( C_Issuer_Name )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Name;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 60;
        ELSIF Temp_Name = UPPER( C_Issuer_Name_F )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Name_F;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 60;
        ELSIF Temp_Name = UPPER( C_Issuer_Name_2 )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Name_2;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 2000;
        ELSIF Temp_Name = UPPER( C_Issuer_Name_2F )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Name_2F;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 2000;
        ELSIF Temp_Name = UPPER( C_Issuer_Xref_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Xref_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 12;
            B_MSMF := TRUE;
        ELSIF Temp_Name = UPPER( C_Issuer_Xref_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Xref_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 12;
            B_MSMF := TRUE;
        ELSIF Temp_Name = UPPER( C_Isid )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Isid;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 8;
            GT_Fields(G_Number_Of_Fields).F_Mandatory  := TRUE;
        ELSIF Temp_Name = UPPER( C_Issue_Nation )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issue_Nation;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Issuer_Nation )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Nation;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Issuer_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Issuer_Sub_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issuer_Sub_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Market_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Market_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Maturity_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Maturity_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Maturity_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Maturity_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Sec_Long_Name )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Sec_Long_Name;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Mandatory  := TRUE;
        ELSIF Temp_Name = UPPER( C_Sec_Long_Name_F )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Sec_Long_Name_F;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
        ELSIF Temp_Name = UPPER( C_Sec_Long_Name_2 )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Sec_Long_Name_2;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 2000;
        ELSIF Temp_Name = UPPER( C_Sec_Long_Name_2F )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Sec_Long_Name_2F;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 2000;
        ELSIF Temp_Name = UPPER( C_ISO_Ccy_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_ISO_Ccy_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Income_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Income_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Income_Freq )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Income_Freq;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Pricing_Note )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Pricing_Note;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 240;
        ELSIF Temp_Name = UPPER( C_Security_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Security_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
            GT_Fields(G_Number_Of_Fields).F_Mandatory  := TRUE;
        ELSIF Temp_Name = UPPER( C_Security_Sub_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Security_Sub_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 6;
            GT_Fields(G_Number_Of_Fields).F_Mandatory  := TRUE;
        ELSIF Temp_Name = UPPER( C_Call_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Call_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Floating_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Floating_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Convertible_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Convertible_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Perpetual_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Perpetual_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Intangibility_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Intangibility_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_E_R_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_E_R_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_CUSIP )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_CUSIP;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 9;
            GT_Fields(G_Number_Of_Fields).F_Mandatory  := TRUE;
        ELSIF Temp_Name = UPPER( C_ISIN )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_ISIN;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 12;
        ELSIF Temp_Name = UPPER( C_ISIN_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_ISIN_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_OISIN )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_OISIN;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 12;
        ELSIF Temp_Name = UPPER( C_OISIN_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_OISIN_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_SEDOL )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_SEDOL;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 7;
        ELSIF Temp_Name = UPPER( C_SEDOL_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_SEDOL_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_SEDOL_Nation )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_SEDOL_Nation;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_SEDOL_ISO_Nation )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_SEDOL_ISO_Nation;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 2;
        ELSIF Temp_Name = UPPER( C_CINS_Symbol )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_CINS_Symbol;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 9;
        ELSIF Temp_Name = UPPER( C_CINS_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_CINS_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_RIC_Symbol )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_RIC_Symbol;
-- Modification - JSL - 25 septembre 2015
--          GT_Fields(G_Number_Of_Fields).F_Max_Length := 15;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
-- Fin de modification - JSL - 25 septembre 2015
        ELSIF Temp_Name = UPPER( C_RIC_Source )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_RIC_Source;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_RIC_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_RIC_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Exch_Source_1 )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Exch_Source_1;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Exch_Source_2 )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Exch_Source_2;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_First_Pay_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_First_Pay_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Creation_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Creation_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Issue_Offer_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issue_Offer_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Accrual_Start_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Accrual_Start_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Trading_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Trading_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Data_Source_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Data_Source_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Data_Supp_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Data_Supp_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Data_Deliv_Time )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Data_Deliv_Time;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Data_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Data_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Priv_Plac_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Priv_Plac_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Sched_Event_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Sched_Event_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Serie )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Serie;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 10;
        ELSIF Temp_Name = UPPER( C_Class )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Class;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Redemption )
           OR Temp_Name = UPPER( 'Redemption Amount' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Redemption;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Redemption_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Redemption_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Trading_Source )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Trading_Source;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Par_Value )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Par_Value;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Par_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Par_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Income_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Income_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Ticker_Mnemonic )
           OR Temp_Name = UPPER( 'Identification System' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Ticker_Mnemonic;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 5;
        ELSIF Temp_Name = UPPER( C_Ticker_Symbol )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Ticker_Symbol;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 15;
        ELSIF Temp_Name = UPPER( C_Ticker_Source )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Ticker_Source;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Ticker_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Ticker_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Index_Category )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Index_Category;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 64;
        ELSIF Temp_Name = UPPER( C_Index_Base_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Index_Base_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Index_Base_Value )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Index_Base_Value;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Index_Rebalance_Frequency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Index_Rebalance_Frequency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Index_Description )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Index_Description;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 400;
        ELSIF Temp_Name = UPPER( C_Exch_Symbol )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Exch_Symbol;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 15;
        ELSIF Temp_Name = UPPER( C_EDI_Symbol )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_EDI_Symbol;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 9;
        ELSIF Temp_Name = UPPER( C_Pool_Number )
           OR Temp_Name = UPPER( C_Pool_No )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Pool_Number;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 9;
        ELSIF Temp_Name = UPPER( C_Day_Count )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Day_Count;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 7;
        ELSIF Temp_Name = UPPER( C_INISYS_Day_Count )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_INISYS_Day_Count;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 7;
        ELSIF Temp_Name = UPPER( C_Inflation_Linked )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Inflation_Linked;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Acc_Form_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Acc_Form_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Fri_Symbol )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Fri_Symbol;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 12;
        ELSIF Temp_Name = UPPER( C_Schedule )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Schedule;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
        ELSIF Temp_Name = UPPER( C_Annual_Income_Rate )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Annual_Income_Rate;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Base_Bond )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Base_Bond;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
        ELSIF Temp_Name = UPPER( C_Base_Ticker )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Base_Ticker;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
        ELSIF Temp_Name = UPPER( C_Call_Type_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Call_Type_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Class_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Class_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
        ELSIF Temp_Name = UPPER( C_Class_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Class_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
        ELSIF Temp_Name = UPPER( C_Currency_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Currency_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Date_Of_Status )
           OR Temp_Name = UPPER( 'As of Date' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Date_Of_Status;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Denominations )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Denominations;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Ending_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Ending_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Final_Reset_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Final_Reset_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_First_Payment_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_First_Pay_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Fixed_Premium )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Fixed_Premium;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Frequency_Value )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Frequency_Value;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Issue_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Issue_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Market_Measurement_Days )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Market_Measurement_Days;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Market_Measurement_Factor )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Market_Measurement_Factor;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Market_Measurement_Method )
           OR Temp_Name = UPPER( 'Market Measurement Method Code' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Market_Measurement_Method;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
        ELSIF Temp_Name = UPPER( C_Market_Measurement_Type )
           OR Temp_Name = UPPER( 'Market Measurement Type Code' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Market_Measurement_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
        ELSIF Temp_Name = UPPER( C_Measured_Statpro_Id )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Measured_Statpro_Id;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 12;
        ELSIF Temp_Name = UPPER( C_Offering_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Offering_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Option_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Option_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Option_Price )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Option_Price;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Option_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Option_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Optional_Income_Currency )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Optional_Income_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Price )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Price;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Pricing_Currency )
           OR Temp_Name = UPPER( 'Price Currency' )
           OR Temp_Name = UPPER( 'Currency Code' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Pricing_Currency;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Pricing_Source_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Pricing_Source_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Private_Placement_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Private_Placement_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Quantity )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Quantity;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Rater )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Rater;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Rating_Code )
           OR Temp_Name = UPPER( 'Rating' )
           OR Temp_Name = UPPER( 'Rating Code' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Rating_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 10;
        ELSIF Temp_Name = UPPER( C_Rating_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Rating_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Remarks )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Remarks;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 250;
        ELSIF Temp_Name = UPPER( C_Scheduled_Event_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Scheduled_Event_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Sinking_Fund_Flag )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Sinking_Fund_Flag;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 1;
        ELSIF Temp_Name = UPPER( C_Source_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Source_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Spread_Note )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Spread_Note;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
        ELSIF Temp_Name = UPPER( C_Spread )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Spread;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 50;
            GT_Fields(G_Number_Of_Fields).F_Numeric    := TRUE;
        ELSIF Temp_Name = UPPER( C_Starting_Date )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Starting_Date;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := C_Date_Length;
            GT_Fields(G_Number_Of_Fields).F_Format     := C_Date_Format;
        ELSIF Temp_Name = UPPER( C_Trading_Status )
           OR Temp_Name = UPPER( 'Status' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Trading_Status;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 3;
        ELSIF Temp_Name = UPPER( C_Symbol )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Symbol;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 15;
        ELSIF Temp_Name = UPPER( C_UL_Fri_Symbol )
           OR Temp_Name = UPPER( 'UNDERLYING STATPRO ID' )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_UL_Fri_Symbol;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 12;
        ELSIF Temp_Name = UPPER( C_US_Issuer_Type )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_US_Issuer_Type;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 32;
        ELSIF Temp_Name = UPPER( C_Type_I )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Type_I;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 32;
        ELSIF Temp_Name = UPPER( C_Type_II )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Type_II;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 32;
        ELSIF Temp_Name = UPPER( C_Type_III )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Type_III;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 32;
        ELSIF Temp_Name = UPPER( C_Taxation )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Taxation;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 10;
        ELSIF Temp_Name = UPPER( C_Insurance )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Insurance;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 10;
        ELSIF Temp_Name = UPPER( C_Refinancing )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Refinancing;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 10;
        ELSIF Temp_Name = UPPER( C_State )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_State;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 2;
        ELSIF Temp_Name = UPPER( C_Matured_Bond )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Matured_Bond;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 7;
-- Ajout - JSL - 25 septembre 2015
        ELSIF Temp_Name = UPPER( C_Client_Code )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Client_Code;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 4;
        ELSIF Temp_Name = UPPER( C_Client_Security_Id )
        THEN
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Client_Security_Id;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 30;
-- Fin d'ajout - JSL - 25 septembre 2015
        ELSE
           Manage_Messages.Get_Oasis_Message ( G_Message_Code||'100'
                                             , G_Del_Flag
                                             , Temp_Name
                                             , V_Message
                                             );
            pgmloc             := 2150;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , V_Message
                                           , Prc_Msgs_Rec
                                           , Success
                                           );
            IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
            GT_Fields(G_Number_Of_Fields).F_Name       := C_Ignore;
            GT_Fields(G_Number_Of_Fields).F_Max_Length := 500;
        END IF;
        Start_Position := End_Position + 1;
    END LOOP; --}
    --
--  Ignorer les champs inutiles a la fin des enregistrements
    pgmloc := 2160;
    WHILE GT_Fields(G_Number_Of_Fields).F_Name = C_Ignore
    LOOP
        G_Number_Of_Fields := G_Number_Of_Fields - 1;
        EXIT WHEN G_Number_Of_Fields = 0;
    END LOOP;

-- Ajout - JSL - 15 decembre 2015
    Pgmloc := 2161;
    V_Parm.Parm_Name := G_All_Parms.FIRST;
    LOOP
        IF V_Parm.Parm_Name LIKE 'MANDATORY!_%' ESCAPE '!'
        THEN
            V_Parm := G_All_Parms( V_Parm.Parm_Name );
            FOR I IN 1 .. G_Number_Of_Fields
            LOOP
                IF UPPER(V_Parm.Parm_Value) = UPPER(GT_Fields( I ).F_Name)
                THEN
                    GT_Fields( I ).F_Mandatory := TRUE;
                    EXIT;
                END IF;
            END LOOP;
        END IF;
        EXIT WHEN V_Parm.Parm_Name = G_All_Parms.LAST;
        V_Parm.Parm_Name := G_All_Parms.NEXT( V_Parm.Parm_Name );
    END LOOP;
-- Fin d'ajout - JSL - 15 decembre 2015

    --
    -- Fields by default
    --
    pgmloc := 2170;
    G_Acc_Calc_Formula       := NULL;
    G_Acc_Freq_Value         := NULL;
    G_Book_Based_Flag        := 'N';
    G_Const_Prc_Flag         := 'N';
    G_Day_Count              := '0';
    G_Gen_Pay_Flag           := 'N';
    G_Income_Type            := 'N';
    G_Intangibility_Flag     := 'N';
    G_Mat_Date_Method        := NULL;
    G_Maturity_Type          := 'STD';
    G_MTN_Flag               := 'N';
    G_Perp_Flag              := 'N';
    G_E_R_Flag               := 'N';
    G_Call_Flag              := 'N';
    G_Conv_Flag              := 'N';
    G_Par_Value              := NULL;
    G_Par_Fri_Cy             := NULL;
    G_Pur_Fund_Flag          := 'N';
    G_Ratings_Flag           := 'N';
    G_Redemption_Amount      := NULL;
    G_Redemption_Currency    := NULL;
    G_SAC_Changed_Flag       := 'N';
    G_Site_Date              := Constants.Get_Site_Date;
    G_Splits_Flag            := 'N';
    G_Sink_Fund_Flag         := 'N';
    G_Trd_Status             := 'A';
    G_User_ID                := Constants.Get_User_ID;
    G_Freq_Value             := 0; -- 'NF'
    G_Sec_Type               := 'JNK';
    G_Sec_SubType            := 'NONE';
    --
    G_More_Header            := 'AUTO'||C_TAB||'%';
    G_Track_Progress         := TRUE;
    G_Many_Tickers           := FALSE;
    --
    G_Issuer_Generic         := 'MG';
    G_MAY_EXISTS             := TRUE;
    pgmloc := 2180;
    IF B_MSMF
    THEN
      pgmloc := 2190;
      G_Issuer_Generic := 'MSMF';
      G_Reg_Expr       := '[0-9][0-9][0-9][0-9][0-9][0-9]$';
      G_Reg_Format     := '099999';
    END IF;
    --
    Success := TRUE;
<<Get_Out>>
    NULL;
    --
EXCEPTION
    WHEN OTHERS
    THEN
        Prc_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
        Success := FALSE;
    --}}
END Initialize_Auto_Fields;
--
--
------------------------------------------------------------------------
-- Validate Security Type
------------------------------------------------------------------------
--
PROCEDURE Validate_Security_Type
        ( P_Fri_Ticker    IN     Securities.Fri_Ticker%TYPE
        , P_Sec_Controls  IN     Security_Controls%ROWTYPE
        , P_SPC           IN     Security_Pattern_Controls%ROWTYPE
        , P_Message       IN OUT VARCHAR2
        , Success         IN OUT BOOLEAN
        ) IS
--{
    W_DUMMY       Dual.Dummy%TYPE;
    W_Securities  Securities%ROWTYPE;
--
    V_Message            VARCHAR2(255);
    Field_01             VARCHAR2(255);
    Field_02             VARCHAR2(255);
    Field_03             VARCHAR2(255);
--
CURSOR Get_Securities
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT *
  FROM SECURITIES
 WHERE Fri_Ticker = I_Fri_Ticker;
--
CURSOR Get_If_Call_Schedule
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Call_Schedule
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
CURSOR Get_If_Conversion_Schedule
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Conversion_Schedule
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
CURSOR Get_If_Maturity_Schedule
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Maturity_Schedule
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
CURSOR Get_If_Purchase_Fund_Schedule
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Purchase_Fund_Schedule
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
CURSOR Get_If_Sinking_Fund_Schedule
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Sinking_Fund_Schedule
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
CURSOR Get_If_Splits
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Splits
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
CURSOR Get_If_Ratings
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Ratings
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
CURSOR Get_If_Issues
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Issues
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
CURSOR Get_If_Pricing_Calc_Schedule
     ( I_Fri_Ticker       Securities.Fri_Ticker%TYPE
     ) IS
SELECT Dummy
  FROM DUAL
 WHERE EXISTS (
       SELECT 1
         FROM Pricing_Calc_Schedule
        WHERE Fri_Ticker = I_Fri_Ticker
       );
--
BEGIN --{
    Success   := FALSE;
    Field_01  := NULL;
    Field_02  := NULL;
    Field_03  := NULL;
    V_Message := NULL;
    -- Does the security type change?
    pgmloc := 2200;
    OPEN Get_Securities( P_Fri_Ticker );
    pgmloc := 2210;
    FETCH Get_Securities INTO W_Securities;
    IF Get_Securities%NOTFOUND
    THEN
        G_FMessage_Code := G_Message_Code || '132';
        Field_01 := P_Fri_Ticker;
        Field_02 := NULL;
        Field_03 := NULL;
        GOTO Log_Error;
    END IF;
    pgmloc := 2220;
    CLOSE Get_Securities;
    --
    IF P_Sec_Controls.Security_Type_Code = W_Securities.Security_Type_Code
    THEN
        Success := TRUE;
        GOTO Get_Out;
    ELSIF W_Securities.Security_Type_Code <> 'JNK'
    THEN
        G_FMessage_Code := G_Message_Code || '190';
        Field_01 := Security.Get_Fri_Id( P_Fri_Ticker );
        Field_02 := W_Securities.Security_Type_Code;
        Field_03 := P_Sec_Controls.Security_Type_Code;
        GOTO Log_Error;
    END IF;
    --
    W_Dummy := NULL;
    G_FMessage_Code := G_Message_Code || '200';
    --
    IF P_SPC.Call_Schedule_Flag = 'N'
    THEN
        Pgmloc := 2230;
        OPEN GET_IF_Call_Schedule( P_Fri_Ticker );
        Pgmloc := 2240;
        FETCH Get_IF_Call_Schedule
         INTO W_Dummy;
        Pgmloc := 2250;
        CLOSE Get_IF_Call_Schedule;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Call_Schedule';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    IF P_SPC.Conversion_Schedule_Flag = 'N'
    THEN
        Pgmloc := 2260;
        OPEN GET_IF_Conversion_Schedule( P_Fri_Ticker );
        Pgmloc := 2270;
        FETCH Get_IF_Conversion_Schedule
         INTO W_Dummy;
        Pgmloc := 2280;
        CLOSE Get_IF_Conversion_Schedule;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Conversion_Schedule';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    IF P_SPC.Maturity_Schedule_Flag = 'N'
    THEN
        Pgmloc := 2290;
        OPEN GET_IF_Maturity_Schedule( P_Fri_Ticker );
        Pgmloc := 2300;
        FETCH Get_IF_Maturity_Schedule
         INTO W_Dummy;
        Pgmloc := 2310;
        CLOSE Get_IF_Maturity_Schedule;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Maturity_Schedule';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    IF P_SPC.Purchase_Fund_Flag = 'N'
    THEN
        Pgmloc := 2320;
        OPEN GET_IF_Purchase_Fund_Schedule( P_Fri_Ticker );
        Pgmloc := 2330;
        FETCH Get_IF_Purchase_Fund_Schedule
         INTO W_Dummy;
        Pgmloc := 2340;
        CLOSE Get_IF_Purchase_Fund_Schedule;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Purchase_Fund_Schedule';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    IF P_SPC.Sinking_Fund_Flag = 'N'
    THEN
        Pgmloc := 2350;
        OPEN GET_IF_Sinking_Fund_Schedule( P_Fri_Ticker );
        Pgmloc := 2360;
        FETCH Get_IF_Sinking_Fund_Schedule
         INTO W_Dummy;
        Pgmloc := 2370;
        CLOSE Get_IF_Sinking_Fund_Schedule;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Sinking_Fund_Schedule';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    IF P_SPC.Split_Flag = 'N'
    THEN
        Pgmloc := 2380;
        OPEN GET_IF_Splits( P_Fri_Ticker );
        Pgmloc := 2390;
        FETCH Get_IF_Splits
         INTO W_Dummy;
        Pgmloc := 2400;
        CLOSE Get_IF_Splits;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Splits';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    IF P_SPC.Issue_Flag = 'N'
    THEN
        Pgmloc := 2410;
        OPEN GET_IF_Issues( P_Fri_Ticker );
        Pgmloc := 2420;
        FETCH Get_IF_Issues
         INTO W_Dummy;
        Pgmloc := 2430;
        CLOSE Get_IF_Issues;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Issues';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    IF P_SPC.Prc_Calc_Sch_Flag = 'N'
    THEN
        Pgmloc := 2440;
        OPEN GET_IF_Pricing_Calc_Schedule( P_Fri_Ticker );
        Pgmloc := 2450;
        FETCH Get_IF_Pricing_Calc_Schedule
         INTO W_Dummy;
        Pgmloc := 2460;
        CLOSE Get_IF_Pricing_Calc_Schedule;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Pricing_Calc_Schedule';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    IF P_SPC.Prc_Calc_Sch_Flag = 'N'
    THEN
        Pgmloc := 2470;
        OPEN GET_IF_Pricing_Calc_Schedule( P_Fri_Ticker );
        Pgmloc := 2480;
        FETCH Get_IF_Pricing_Calc_Schedule
         INTO W_Dummy;
        Pgmloc := 2490;
        CLOSE Get_IF_Pricing_Calc_Schedule;
        IF W_Dummy IS NOT NULL
        THEN
            Field_01 := 'Pricing_Calc_Schedule';
            GOTO Log_Error;
        END IF;
    END IF;
    --
    --
    Success := TRUE;
    GOTO Get_Out;
<<Log_Error>>
    Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                      , G_Del_Flag
                                      , Field_01
                                      , Field_02
                                      , Field_03
                                      , V_Message
                                      );
    P_Message := V_Message;
    Success := FALSE;
<<Get_Out>>
    NULL;
    --
EXCEPTION
WHEN OTHERS THEN
    P_Message := SUBSTR( Pgmloc || ' ' || SQLERRM( SQLCODE ), 1, 255 );
    Success := FALSE;
    --
END Validate_Security_Type;  --}}
--
------------------------------------------------------------------------
--
PROCEDURE Add_Schedules
        ( P_Create        IN     BOOLEAN
        , P_Success       OUT    NOCOPY BOOLEAN
        ) IS
--{
    --
    End_Of_File          BOOLEAN;
    --
    V_CY                 Currencies.Currency%TYPE;
    V_Fri_Ticker         Securities.Fri_Ticker%TYPE;
    V_Tick_Ins           INTEGER := 0;
    R_Field_Name         VARCHAR2(128);
    R_Field              VARCHAR2(2000);
    --
    R_Pos_Start          INTEGER;
    R_Pos_End            INTEGER;
    R_Pos_Len            INTEGER;
    V_Num_Flds           INTEGER := 1;
    --
    V_Number             Number;
    V_Date               Date;
    --
    V_Error_Code         VARCHAR2(32);
    V_Error_Message      VARCHAR2(1024);
    V_Error_Column       All_Tab_Columns.Column_Name%TYPE;
 
    Rec Sched;


    --
BEGIN --{
    --
    pgmloc  := 2500;
    P_Success            := FALSE;
    --
    V_Track_Progress   := G_Track_Progress;
    IF V_Track_Progress IS NULL
    THEN
        V_Track_Progress := TRUE;
    END IF;
    --
    End_of_File        := FALSE;
    --
    -- Process all Lines find in File of Foreign Bonds
    --
    pgmloc := 2510;
    WHILE NOT End_Of_File
    LOOP
        --{
        W_Record_Status    := NULL;
        V_Row_Errors       := FALSE;
        V_Warning          := FALSE;
        V_Row_Tracked      := FALSE;
        V_Err_Tracked      := FALSE;
        --
        pgmloc := 2520;
        Load_Supplier_Rules.Get_Line( Data_File
                                    , Pricing_Msgs_Rec
                                    , V_Record
                                    , V_Success
                                    , End_Of_File
                                    );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        EXIT WHEN End_Of_File;
        --
        pgmloc := 2530;
        V_Rows_Read := V_Rows_Read + 1;
        IF G_Tracing
        THEN
            pgmloc             := 2540;
            Load_Supplier_Rules.Write_File( Log_File
                                          , 'Read row # '
                                         || TO_CHAR(V_Rows_read)
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc             := 2550;
            Load_Supplier_Rules.Write_File( Log_File
                                          , V_Record
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF;
        --
        IF UPPER( V_Record ) LIKE UPPER(C_Fri_Symbol||C_Tab||C_Schedule||C_Tab) || '%'
        THEN
          -- Write the header in the error file so we can reload it later on
          Pgmloc := 2560;
          Load_Supplier_Rules.Write_File ( Err_File
                                         , V_Record
                                         , Pricing_Msgs_Rec
                                         , V_Success
                                         );
          IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
          IF G_Auto
          THEN
            Initialize_Auto_Fields( V_Record
                                  , Log_File
                                  , Pricing_Msgs_Rec
                                  , V_Success
                                  );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            V_Num_Flds := G_Number_Of_Fields;
            IF G_Tracing
            THEN
              pgmloc             := 2570;
              Load_Supplier_Rules.Write_File( Log_File
                                            , 'Number of fields '
                                           || G_Number_Of_Fields
                                            , Pricing_Msgs_Rec
                                            , V_Success
                                            );
              IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
          END IF;
        END IF;
        --
        -- Prepare the Record
        --
        IF V_Record        LIKE C_Tab    || '%'
        OR UPPER(V_Record) LIKE C_#tab   || '%'
        OR UPPER( V_Record ) LIKE UPPER(C_Fri_Symbol||C_Tab||C_Schedule||C_Tab) || '%'
        OR V_Record        IS NULL
        THEN
            IF G_Tracing
            THEN
                pgmloc             := 2580;
                Load_Supplier_Rules.Write_File( Log_File
                                              , 'Skipped row # '
                                             || TO_CHAR(V_Rows_read)
                                              , Pricing_Msgs_Rec
                                              , V_Success
                                              );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            V_Rows_Skip := V_Rows_Skip + 1;
            GOTO Next_Row;
        END IF;
        --
        -- Looping Here to Process/Validate All Fields
        --
        IF V_Track_Progress
        THEN --{
            V_Row_Tracked := TRUE;
            pgmloc             := 2590;
            Load_Supplier_Rules.Write_File( Log_File
                                          , NULL
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc             := 2600;
            Load_Supplier_Rules.Write_File( Log_File
                                          , 'Processing Row # '
                                         || TO_CHAR(V_Rows_read)
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            Pgmloc := 2610;
            Load_Supplier_Rules.Write_File( Log_File
                                          , V_Record
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF; --}
        Pgmloc := 2620;
        -- Initialise the record
        Initialize_Schd_Row( Rec );
        Pgmloc := 2630;
        --
        -- parse the record
        FOR X IN 1..V_Num_Flds
        LOOP --{
            IF G_Tracing
            THEN
              pgmloc             := 2640;
              Load_Supplier_Rules.Write_File( Log_File
                                            , 'Field number '
                                           || X
                                            , Pricing_Msgs_Rec
                                            , V_Success
                                            );
              IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            pgmloc           := 2650;
            E_Value_01       := NULL;
            E_Value_02       := NULL;
            Field_Number     := X;
            pgmloc           := 2660;
            Field_Numeric    := GT_Fields(Field_Number).F_Numeric;
            pgmloc           := 2670;
            Field_Max_Length := GT_Fields(Field_Number).F_Max_Length;
            pgmloc           := 2680;
            Field_Name       := GT_Fields(Field_Number).F_Name;
            pgmloc           := 2690;
            Field_Format     := GT_Fields(Field_Number).F_Format;
            pgmloc           := 2700;
            Field_Mandatory  := GT_Fields(Field_Number).F_Mandatory;
            IF Field_Name = C_Ignore
            THEN
                GOTO Next_Fld;
            END IF;
            --
            pgmloc             := 2710;
            R_Pos_Start := 1;
            IF X > 1
            THEN
                R_Pos_Start := INSTR(V_Record, C_Tab, 1, X-1);
                IF R_Pos_Start < 1
                THEN
                    -- Field is missing - Checked later if mandatory
                    R_Pos_Len   := 0;
                    R_Field     := NULL;
                    R_Pos_Start := 0;
                ELSE
                    R_Pos_Start := R_Pos_Start + 1;
                END IF;
            END IF;
            R_Pos_End := INSTR(V_Record, C_Tab, 1, X) - 1;
            -- Ending tabs missing?
            IF R_Pos_End <= 0
            THEN
                R_Pos_End := LENGTH( V_Record );
            END IF;
            IF R_Pos_Start > 0
            THEN
                Pgmloc := 2720;
                R_Pos_Len := R_Pos_End - R_Pos_Start + 1;
                IF R_Pos_Len > C_Max_Field_Length
                THEN
                    R_Pos_Len := C_Max_Field_Length;
                END IF;
                Pgmloc  := 2730;
                R_Field := TRIM(SUBSTR(V_Record, R_Pos_Start, R_Pos_Len));
                IF INSTR( R_Field, ' [' ) > 0
                THEN
                  R_Field := TRIM( SUBSTR( R_Field, 1, INSTR( R_Field, ' [' ) - 1 ) );
                END IF;
                R_Field := TRIM( '"' FROM R_Field );
                R_Field := TRIM( C_Trim_Value FROM R_FIELD );
            END IF;
            IF G_Tracing
            THEN
                pgmloc             := 2740;
                Load_Supplier_Rules.Write_File( Log_File
                                              , Field_Name
                                             || ' Read as '''
                                             || R_Field
                                             || ''''
                                              , Pricing_Msgs_Rec
                                              , V_Success
                                              );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            pgmloc := 2750;
            V_Message   := '';
--          -- Special clause for perpetual maturity
-- NOT IN SCHEDULE!
            IF LENGTH( R_Field ) > Field_Max_Length
            THEN --{
                IF NOT V_Row_Tracked
                THEN --{
                    V_Row_Tracked := TRUE;
                    pgmloc             := 2760;
                    Load_Supplier_Rules.Write_File( Log_File
                                                  , NULL
                                                  , Pricing_Msgs_Rec
                                                  , V_Success
                                                  );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 2770;
                    Load_Supplier_Rules.Write_File( Log_File
                                                  , 'Processing Row # '
                                                 || TO_CHAR(V_Rows_read)
                                                  , Pricing_Msgs_Rec
                                                  , V_Success
                                                  );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    Pgmloc := 2780;
                    Load_Supplier_Rules.Write_File ( Log_File
                                                   , V_Record
                                                   , Pricing_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                pgmloc := 2790;
                G_FMessage_Code := G_Message_Code || '010';
                Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                                  , G_Del_Flag
                                                  , Field_Name
                                                  , Field_Max_Length
                                                  , V_Message
                                                  );
                pgmloc := 2800;
                Load_Supplier_Rules.Write_File ( Log_File
                                               , C_Indent || V_Message
                                               , Pricing_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                R_Field := SUBSTR( R_Field, 1, Field_Max_Length );
            ELSIF R_Field IS NULL
              AND Field_Mandatory
            THEN --}{
                G_FMessage_Code := G_Message_Code || '020';
                Log_Error;
                Goto Next_Fld;
            END IF; --}
            pgmloc := 2810;
            IF Field_Numeric
            THEN --{
                BEGIN
                    V_Success := FALSE;
                    V_Number  := TO_NUMBER( R_Field );
                    V_Success := TRUE;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;
                IF NOT V_Success
                THEN
                    -- Success is set to TRUE when the field is numeric.
                    --{
                    G_FMessage_Code := G_Message_Code || '030';
                    E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                    Log_Error;
                    Goto Next_Fld;
                    --}
                END IF;
            END IF; --}
            pgmloc := 2820;
            IF Field_Format IS NOT NULL
            THEN --{
                BEGIN
                    V_Success := FALSE;
                    V_Date := TO_DATE( R_Field , Field_Format );
                    V_Success := TRUE;
                EXCEPTION
                WHEN OTHERS
                THEN
                    BEGIN
                        V_Date := TO_DATE( R_Field, 'YYYY-MM-DD' );
                        V_Success := TRUE;
                    EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                    END;
                END;
                IF NOT V_Success
                THEN
                    -- Success is set to TRUE when the field is a correct date.
                    --{
                    G_FMessage_Code := G_Message_Code || '040';
                    E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                    E_Value_02      := Field_Format;
                    Log_Error;
                    Goto Next_Fld;
                    --}
                END IF;
            END IF; --}
            --
            -- Individual fields
            --
            pgmloc := 2830;
            IF R_Field IS NULL
            THEN
                null;
            ELSIF Field_Name = C_Day_Count
            THEN
                pgmloc := 2840;
                Rec.Day_Count := NVL( R_Field, Rec.Day_Count );
            ELSIF Field_Name = C_INISYS_Day_Count
            THEN
                pgmloc := 2850;
                Rec.Inisys_Day_Count := NVL( R_Field, Rec.Inisys_Day_Count );
            ELSIF Field_Name = C_Acc_Form_Code
            THEN
                pgmloc := 2860;
                Rec.Accruals_Calc_Formula_Code := NVL( R_Field, Rec.Accruals_Calc_Formula_Code );
            ELSIF Field_Name = C_Fri_Symbol
            THEN
                pgmloc := 2870;
                Rec.Unique_Id := NVL( UPPER( R_Field ), Rec.Unique_Id );
            ELSIF Field_Name = C_Schedule
            THEN
                pgmloc := 2880;
                Rec.Schedule := NVL( UPPER( R_Field ), Rec.Schedule );
            ELSIF Field_Name = C_Annual_Income_Rate
            THEN
                pgmloc := 2890;
                Rec.Annual_Income_Rate := NVL( V_Number, Rec.Annual_Income_Rate );
            ELSIF Field_Name = C_Base_Bond
            THEN
                pgmloc := 2900;
                Rec.Base_Bond := NVL( R_Field, Rec.Base_Bond );
            ELSIF Field_Name = C_Base_Ticker
            THEN
                pgmloc := 2910;
                Rec.Base_Ticker := NVL( R_Field, Rec.Base_Ticker );
            ELSIF Field_Name = C_Call_Type_Code
            THEN
                pgmloc := 2920;
                Rec.Call_Type_Code := NVL( R_Field, Rec.Call_Type_Code );
            ELSIF Field_Name = C_Class_Code
            THEN
                pgmloc := 2920;
                Rec.Class_Code := NVL( R_Field, Rec.Class_Code );
            ELSIF Field_Name = C_Class_Type
            THEN
                pgmloc := 2930;
                Rec.Class_Type := NVL( R_Field, Rec.Class_Type );
            ELSIF Field_Name = C_Currency_Code
            THEN
                pgmloc := 2940;
                Rec.Currency_Code := NVL( UPPER( R_Field ), Rec.Currency_Code );
            ELSIF Field_Name = C_Date_Of_Status
            THEN
                pgmloc := 2950;
                Rec.Date_Of_Status := NVL( V_Date, Rec.Date_Of_Status );
            ELSIF Field_Name = C_Denominations
            THEN
                pgmloc := 2960;
                Rec.Denominations := NVL( V_Number, Rec.Denominations );
            ELSIF Field_Name = C_Ending_Date
            THEN
                pgmloc := 2950;
                Rec.Ending_Date := NVL( V_Date, Rec.Ending_Date );
            ELSIF Field_Name = C_Final_Reset_Date
            THEN
                pgmloc := 2970;
                Rec.Final_Reset_Date := NVL( V_Date, Rec.Final_Reset_Date );
            ELSIF Field_Name = C_First_Payment_Date
            THEN
                pgmloc := 2980;
                Rec.First_Payment_Date := NVL( V_Date, Rec.First_Payment_Date );
            ELSIF Field_Name = C_Fixed_Premium
            THEN
                pgmloc := 2990;
                Rec.Fixed_Premium := NVL( V_Number, Rec.Fixed_Premium );
            ELSIF Field_Name = C_Frequency_Value
            THEN
                pgmloc := 3000;
                Rec.Frequency_Value := NVL( V_Number, Rec.Frequency_Value );
            ELSIF Field_Name = C_Income_Currency
            THEN
                pgmloc := 3010;
                Rec.Income_Currency := NVL( UPPER( R_Field ), Rec.Income_Currency );
            ELSIF Field_Name = C_Ticker_Mnemonic
            THEN
                pgmloc := 3020;
                Rec.Identification_System_Mnemonic := NVL( UPPER( R_Field ), Rec.Identification_System_Mnemonic );
            ELSIF Field_Name = C_Issue_Date
            THEN
                pgmloc := 3030;
                Rec.Issue_Date := NVL( V_Date, Rec.Issue_Date );
            ELSIF Field_Name = C_Market_Measurement_Days
            THEN
                pgmloc := 3040;
                Rec.Market_Measurement_Days := NVL( V_Number, Rec.Market_Measurement_Days );
            ELSIF Field_Name = C_Market_Measurement_Factor
            THEN
                pgmloc := 3050;
                Rec.Market_Measurement_Factor := NVL( V_Number, Rec.Market_Measurement_Factor );
            ELSIF Field_Name = C_Market_Measurement_Method
            THEN
                pgmloc := 3060;
                Rec.Market_Measurement_Method_Code := NVL( R_Field, Rec.Market_Measurement_Method_Code );
            ELSIF Field_Name = C_Market_Measurement_Type
            THEN
                pgmloc := 3070;
                Rec.Market_Measurement_Type_Code := NVL( R_Field, Rec.Market_Measurement_Type_Code );
            ELSIF Field_Name = C_Maturity_Date
            THEN
                pgmloc := 3080;
                Rec.Maturity_Date := NVL( V_Date, Rec.Maturity_Date );
            ELSIF Field_Name = C_Maturity_Type
            THEN
                pgmloc := 3090;
                Rec.Maturity_Type_Code := NVL( UPPER( R_Field ), Rec.Maturity_Type_Code );
            ELSIF Field_Name = C_Measured_Statpro_Id
            THEN
                pgmloc := 3100;
                Rec.Measured_Unique_Id := NVL( UPPER( R_Field ), Rec.Measured_Unique_ID );
            ELSIF Field_Name = C_Offering_Date
            THEN
                pgmloc := 3110;
                Rec.Offering_Date := NVL( V_Date, Rec.Option_Date );
            ELSIF Field_Name = C_Option_Date
            THEN
                pgmloc := 3120;
                Rec.Option_Date := NVL( V_Date, Rec.Option_Date );
            ELSIF Field_Name = C_Option_Price
            THEN
                pgmloc := 3130;
                Rec.Option_Price := NVL( V_Number, Rec.Option_Price );
            ELSIF Field_Name = C_Option_Type
            THEN
                pgmloc := 3140;
                Rec.Option_Type := NVL( R_Field, Rec.Option_Type );
            ELSIF Field_Name = C_Optional_Income_Currency
            THEN
                pgmloc := 3150;
                Rec.Optional_Income_Currency := NVL( UPPER( R_Field ), Rec.Optional_Income_Currency );
            ELSIF Field_Name = C_Price
            THEN
                pgmloc := 3160;
                Rec.Price := NVL( V_Number, Rec.Price );
            ELSIF Field_Name = C_Pricing_Currency
            THEN
                pgmloc := 3170;
                Rec.Pricing_Currency := NVL( UPPER( R_Field ), Rec.Pricing_Currency );
            ELSIF Field_Name = C_Pricing_Note
            THEN
                pgmloc := 3180;
                Rec.Pricing_Note := NVL( R_Field, Rec.Pricing_Note );
            ELSIF Field_Name = C_Pricing_Source_Code
            THEN
                pgmloc := 3190;
                Rec.Pricing_Source_Code := NVL( UPPER( R_Field ), Rec.Pricing_Source_Code );
            ELSIF Field_Name = C_Private_Placement_Flag
            THEN
                pgmloc := 3200;
                Rec.Private_Placement_Flag := NVL( UPPER( R_Field ), Rec.Private_Placement_Flag );
            ELSIF Field_Name = C_Quantity
            THEN
                pgmloc := 3210;
                Rec.Quantity := NVL( V_Number, Rec.Quantity );
            ELSIF Field_Name = C_Rater
            THEN
                pgmloc := 3220;
                Rec.Rater := NVL( UPPER( R_Field ), Rec.Rater );
            ELSIF Field_Name = C_Rating_Code
            THEN
                pgmloc := 3230;
                Rec.Rating_Code := NVL( R_Field, Rec.Rating_Code );
            ELSIF Field_Name = C_Rating_Date
            THEN
                pgmloc := 3240;
                Rec.Rating_Date := NVL( V_Date, Rec.Rating_Date );
            ELSIF Field_Name = C_Redemption
            THEN
                pgmloc := 3250;
                Rec.Redemption_Amount := NVL( V_Number, Rec.Redemption_Amount );
            ELSIF Field_Name = C_Redemption_Currency
            THEN
                pgmloc := 3260;
                Rec.Redemption_Currency := NVL( UPPER( R_Field ), Rec.Redemption_Currency );
            ELSIF Field_Name = C_Remarks
            THEN
                pgmloc := 3260;
                Rec.Remarks := NVL( UPPER( R_Field ), Rec.Remarks );
            ELSIF Field_Name = C_Scheduled_Event_Flag
            THEN
                pgmloc := 3270;
                Rec.Scheduled_Event_Flag := NVL( UPPER( R_Field ), Rec.Scheduled_Event_Flag );
            ELSIF Field_Name = C_Sinking_Fund_Flag
            THEN
                pgmloc := 3280;
                Rec.Sinking_Fund_Flag := NVL( UPPER( R_Field ), Rec.Sinking_Fund_Flag );
            ELSIF Field_Name = C_Source_Code
            THEN
                pgmloc := 3290;
                Rec.Source_Code := NVL( UPPER( R_Field ), Rec.Source_Code );
            ELSIF Field_Name = C_Spread_Note
            THEN
                pgmloc := 3300;
                Rec.Spread_Note := NVL( R_Field, Rec.Spread_Note );
            ELSIF Field_Name = C_Spread
            THEN
                pgmloc := 3310;
                Rec.Spread := NVL( V_Number, Rec.Spread );
            ELSIF Field_Name = C_Starting_Date
            THEN
                pgmloc := 2950;
                Rec.Starting_Date := NVL( V_Date, Rec.Starting_Date );
            ELSIF Field_Name = C_Trading_Status
            THEN
                pgmloc := 3320;
                Rec.Trading_Status_Mnemonic := NVL( UPPER( R_Field ), Rec.Trading_Status_Mnemonic );
            ELSIF Field_Name = C_Symbol
            THEN
                pgmloc := 3330;
                Rec.Ticker := NVL( R_Field, Rec.Ticker );
            ELSIF Field_Name = C_UL_Fri_Symbol
            THEN
                pgmloc := 3340;
                Rec.UL_Unique_Id := NVL( UPPER( R_Field ), Rec.UL_Unique_Id );
            ELSIF Field_Name = C_Accrual_Start_Date
            THEN
                pgmloc := 3240;
                Rec.Accruals_Start_Date := NVL( V_Date, Rec.Accruals_Start_Date );
            END IF;
<< Next_Fld >>
            pgmloc := 3350;
            NULL;
        END LOOP; --}
        --
        pgmloc := 3360;
        IF Rec.Unique_Id   IS NULL
        OR Rec.Schedule IS NULL
        THEN
            -- This is an error
            G_FMessage_Code := G_Message_Code || '600';
            Log_Error;
            GOTO Next_Row;
        END IF;
        --
        -- Getting the ticker from id
        pgmloc := 3370;
        Get_Ticker_from_Id( Rec.Unique_Id, Rec.Fri_Ticker, Log_File );
        --
        pgmloc := 3380;
        IF Rec.Fri_Ticker IS NULL
        THEN
            -- This is an error
            G_FMessage_Code := G_Message_Code || '610';
            E_Value_01      := Rec.Fri_Id;
            Log_Error;
            GOTO Next_Row;
        ELSE
            Rec.Fri_Id := Security.Get_Fri_Id( Rec.Fri_Ticker );
        END IF;
        -- 
        -- Dispatching
        pgmloc := 3390;
        IF Rec.Schedule = UPPER( 'Call Schedule' )
        THEN
            pgmloc := 3400;
            Add_Call_Schedule( Rec
                             , P_Create
                             , V_Success
                             );
        ELSIF Rec.Schedule = UPPER( 'Classification' )
        THEN
            pgmloc := 3400;
            -- Insert into workbench_category_classes
            -- with workbench_code set to 'ISS'
            Add_Classification( Rec
                              , P_Create
                              , V_Success
                              );
        ELSIF Rec.Schedule = UPPER( 'Group Ticker Composition' )
        THEN
            pgmloc := 3410;
            Add_Group_Tickers_Composition( Rec
                                         , P_Create
                                         , V_Success
                                         );
        ELSIF Rec.Schedule = UPPER( 'Interest Floating Rates' )
        THEN
            pgmloc := 3420;
            Add_Interest_Floating_Rates( Rec
                                       , P_Create
                                       , V_Success
                                       );
        ELSIF Rec.Schedule = UPPER( 'Interest Income Rates' )
        THEN
            pgmloc := 3430;
            Add_Interest_Income_Rates( Rec
                                     , P_Create
                                     , V_Success
                                     );
        ELSIF Rec.Schedule = UPPER( 'Issues' )
        THEN
            pgmloc := 3440;
            Add_Issues( Rec
                      , P_Create
                      , V_Success
                      );
        ELSIF Rec.Schedule = UPPER( 'Maturity Schedule' )
        THEN
            pgmloc := 3450;
            Add_Maturity_Schedule( Rec
                                 , P_Create
                                 , V_Success
                                 );
        ELSIF Rec.Schedule = UPPER( 'Pricing Notes' )
        THEN
            pgmloc := 3460;
            Add_Pricing_Notes( Rec
                             , P_Create
                             , V_Success
                             );
        ELSIF Rec.Schedule = UPPER( 'Ratings' )
        THEN
            pgmloc := 3470;
            Add_Ratings( Rec
                       , P_Create
                       , V_Success
                       );
        ELSIF Rec.Schedule = UPPER( 'Tickers' )
        THEN
            pgmloc := 3480;
            Add_Tickers( Rec
                       , P_Create
                       , V_Success
                       );
        ELSIF Rec.Schedule = UPPER( 'Trading Status' )
        THEN
            pgmloc := 3490;
            Add_Trading_Status( Rec
                              , P_Create
                              , V_Success
                              );
        ELSE
            -- This is an error
            pgmloc := 3500;
            G_FMessage_Code := G_Message_Code || '620';
            E_Value_01      := Rec.Schedule;
            Log_Error;
            GOTO Next_Row;
        END IF;
        --
        pgmloc := 3510;
        IF NOT V_Success
        OR W_SQLCode IS NOT NULL
        THEN
            pgmloc := 3520;
            IF W_SQLCode in ( -1, -2290, -2291, -2292 )
            -- -1    ==> Duplicate key on index
            -- -2290 ==> Check constraint violated
            -- -2291 ==> Parent key not found
            -- -2292 ==> Child record found (Apply on delete/update)
            THEN
                pgmloc := 3530;
                Manage_Messages.Process_Error( W_SQLCode
                                             , W_Stack
                                             , V_Error_Column
                                             , V_Message
                                             );
                IF V_Message IS NULL
                THEN
                    pgmloc := 3540;
                    V_Message := W_Stack;
                END IF;
                IF ( INSTR( V_Message, 'ORA-06512', 1, 1 ) <> 0 )
                -- -6512 ==> Backtrace
                THEN
                    pgmloc := 3550;
                    V_Message := SUBSTR( V_Message
                                       , 1
                                       , INSTR( V_Message, 'ORA-06512', 1, 1 ) - 2
                                       );
                END IF;
                W_Backtrace := NULL;
            ELSIF W_SQLCode BETWEEN -20999 AND -20000
            -- -20999 ==> Raise_Application_Error, user defined errors
            THEN
                -- Remove the ... ORA-20001: ...
                pgmloc := 3560;
                V_Message := SUBSTR( W_Stack, 12 );
                -- -6512 ==> Backtrace
                IF ( INSTR( V_Message, 'ORA-06512', 1, 1 ) <> 0 )
                THEN
                    pgmloc := 3570;
                    V_Message := SUBSTR( V_Message
                                       , 1
                                       , INSTR( V_Message, 'ORA-06512', 1, 1 ) - 2
                                       );
                END IF;
                W_Backtrace := NULL;
            ELSE
                pgmloc := 3580;
                V_Message := W_Stack;
            END IF;
            pgmloc := 3590;
            Log_Error;
            IF W_Backtrace IS NOT NULL
            THEN
                pgmloc := 3600;
                V_Message := W_Backtrace;
                Log_Error;
            END IF;
            pgmloc := 3610;
        END IF;
        pgmloc := 3620;
        -- While parsing if found blank [ then truncate at [ and trim.
        -- depending on the schedule
        -- -- dispatch to appropriate routine
        -- -- si validate seulement :
        -- -- -- essayer l'insert avec un rollback;
        -- -- -- rapporter l'erreur qui aurait eu lieu au create
        -- commit frequently to show progress
<< Next_Row >>
        --
        pgmloc := 3630;
        Pricing_Msgs_Rec.Value_01 := V_Rows_Read;
        Pricing_Msgs_Rec.Value_02 := V_Rows_Skip;
        Pricing_Msgs_Rec.Value_03 := V_Rows_Err ;
--      Pricing_Msgs_Rec.Value_04 := V_Secs_Ins ;
--      Pricing_Msgs_Rec.Value_05 := V_Secs_Upd ;
        Pricing_Msgs_Rec.Value_06 := V_Tick_Ins ;
        Pricing_Msgs_Rec.Value_07 := V_Trd_Ins  ;
        Pricing_Msgs_Rec.Value_08 := V_Mat_Ins  ;
        Pricing_Msgs_Rec.Value_09 := V_PrcN_Ins ;
        --
        pgmloc := 3640;
        Load_Supplier_Rules.Update_Prc_Msgs ( G_Program_ID
                                            , G_System_Date
                                            , Pricing_Msgs_Rec
                                            , V_Success
                                            );
        pgmloc := 3650;
        COMMIT;
        NULL;
    END LOOP; --}
    pgmloc := 3660;
    P_Success            := TRUE;
    -- verifier que le ticker peut etre ajouter
    -- ajouter le symbole
    --
    -- rapporter les symboles ajoutes
<< Get_Out >>
    NULL;
    --
END Add_Schedules; --}}
--
------------------------------------------------------------------------
--
-- Ajout - JSL - 25 septembre 2015
FUNCTION Get_Field_Number
       ( V_Field_Name IN VARCHAR2
       )
RETURN INTEGER IS --{
BEGIN --{
    FOR X in 1 .. G_Number_Of_Fields
    LOOP
        IF GT_Fields(X).F_Name = V_Field_Name
        THEN
            RETURN X;
        END IF;
    END LOOP;
    RETURN TO_NUMBER( NULL );
END Get_Field_Number; --}}
--
----------------------------------------------------------------------
-- Get Fri Ticker from a Fri Id
----------------------------------------------------------------------
--
PROCEDURE Get_Ticker_From_Ric
        ( P_Ric          IN     Tickers.Ticker%TYPE
        , P_Ticker          OUT Tickers.Fri_Ticker%TYPE
        , P_Source          OUT Tickers.Source_Code%TYPE
        , P_Currency        OUT Tickers.Currency_Code%TYPE
        ) IS
--{
CURSOR Get_Fri_Ticker
     ( I_Id Tickers.Ticker%TYPE
     ) IS
SELECT Fri_Ticker
     , Source_Code
     , Currency_Code
  FROM Tickers
 WHERE Ticker = I_Id
   AND Identification_System_Code = C_ID_Ric
       ;

BEGIN
    --{
    P_Ticker := NULL;
    P_Source := NULL;
    P_Currency := NULL;
    OPEN Get_Fri_Ticker( P_Ric );
    FETCH Get_Fri_Ticker INTO P_Ticker, P_Source, P_Currency;
    CLOSE Get_Fri_Ticker;
    --}}
END Get_Ticker_From_Ric;
--
------------------------------------------------------------------------
--
PROCEDURE Add_Universes
        ( P_Create        IN     BOOLEAN
        , P_Success       OUT    NOCOPY BOOLEAN
        ) IS
--{
    --
    End_Of_File          BOOLEAN;
    --
    V_CY                 Currencies.Currency%TYPE;
    V_Fri_Ticker         Securities.Fri_Ticker%TYPE;
    V_Tick_Ins           INTEGER := 0;
    R_Field_Name         VARCHAR2(128);
    R_Field              VARCHAR2(2000);
    --
    R_Pos_Start          INTEGER;
    R_Pos_End            INTEGER;
    R_Pos_Len            INTEGER;
    V_Num_Flds           INTEGER := 1;
    --
    V_Number             Number;
    V_Date               Date;
    --
    V_Error_Code         VARCHAR2(32);
    V_Error_Message      VARCHAR2(1024);
    V_Error_Column       All_Tab_Columns.Column_Name%TYPE;
 
    V_More_Header        VARCHAR2(32);
    Rec                  Univ;

    CURSOR Get_Client_From_Code
         ( I_Code  Clients.Code%TYPE
         ) IS
    SELECT *
      FROM Clients
     WHERE Code = I_Code
           ;
    clt_rec              Clients%rowtype;

    CURSOR Get_Client_Univ
         ( I_Fri_Client  Clients.Fri_Client%type
         , I_Clt_Sec_ID  Clients_Univ.Client_Security_Id%TYPE
         ) IS
    SELECT *
      FROM Clients_Univ
     WHERE Fri_Client         = I_Fri_Client
       AND Client_Security_Id = I_Clt_Sec_Id
           ;
    cu_rec               Clients_Univ%ROWTYPE;


    --
BEGIN --{
    --
    pgmloc  := 2500;
    P_Success            := FALSE;
    --
    V_More_Header      := UPPER( G_More_Header );
    V_Track_Progress   := G_Track_Progress;
    IF V_More_Header IS NULL
    THEN
        V_More_Header  := C_TAB||'%';
    END IF;
    V_Track_Progress   := G_Track_Progress;
    IF V_Track_Progress IS NULL
    THEN
        V_Track_Progress := TRUE;
    END IF;
    --
    End_of_File        := FALSE;
    --
    -- Process all lines found in file
    --
    pgmloc := 2510;
    WHILE NOT End_Of_File
    LOOP
        --{
        W_Record_Status    := NULL;
        V_Row_Errors       := FALSE;
        V_Warning          := FALSE;
        V_Row_Tracked      := FALSE;
        V_Err_Tracked      := FALSE;
        --
        pgmloc := 2520;
        Load_Supplier_Rules.Get_Line( Data_File
                                    , Pricing_Msgs_Rec
                                    , V_Record
                                    , V_Success
                                    , End_Of_File
                                    );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        EXIT WHEN End_Of_File;
        --
        pgmloc := 2530;
        V_Rows_Read := V_Rows_Read + 1;
        IF G_Tracing
        THEN
            pgmloc             := 2540;
            Load_Supplier_Rules.Write_File( Log_File
                                          , 'Read row # '
                                         || TO_CHAR(V_Rows_read)
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc             := 2550;
            Load_Supplier_Rules.Write_File( Log_File
                                          , V_Record
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF;
        --
        -- Keep same check as in procedure validate_or_create
        IF UPPER( V_Record ) LIKE C_Header || '%'
        OR UPPER(V_Record)   LIKE C_Issuer_Tab ||'%'
        THEN
          -- Write the header in the error file so we can reload it later on
          Pgmloc := 2560;
          Load_Supplier_Rules.Write_File ( Err_File
                                         , V_Record
                                         , Pricing_Msgs_Rec
                                         , V_Success
                                         );
          IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
          IF G_Auto
          THEN
            Initialize_Auto_Fields( V_Record
                                  , Log_File
                                  , Pricing_Msgs_Rec
                                  , V_Success
                                  );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            V_Num_Flds := G_Number_Of_Fields;
            IF G_Tracing
            THEN
              pgmloc             := 2570;
              Load_Supplier_Rules.Write_File( Log_File
                                            , 'Number of fields '
                                           || G_Number_Of_Fields
                                            , Pricing_Msgs_Rec
                                            , V_Success
                                            );
              IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            -- Initialise Client code and id as mandatory
            IF Get_Field_Number( C_Client_Code ) IS NULL
            THEN
              Manage_Messages.Get_Oasis_Message ( G_Message_Code||'022'
                                                , G_Del_Flag
                                                , C_Client_Code
                                                , Pricing_Msgs_Rec.Err_Text
                                                );
              V_Success := FALSE;
              GOTO GET_OUT;
            ELSE
              GT_Fields( Get_Field_Number( C_Client_Code ) ).F_Mandatory := TRUE;
            END IF;
            IF Get_Field_Number( C_Client_Security_ID ) IS NULL
            THEN
              Manage_Messages.Get_Oasis_Message ( G_Message_Code||'022'
                                                , G_Del_Flag
                                                , C_Client_Security_ID
                                                , Pricing_Msgs_Rec.Err_Text
                                                );
              V_Success := FALSE;
              GOTO GET_OUT;
            ELSE
              GT_Fields( Get_Field_Number( C_Client_Security_ID ) ).F_Mandatory := TRUE;
            END IF;
            IF Get_Field_Number( C_Ric_Symbol ) IS NULL
            THEN
              Manage_Messages.Get_Oasis_Message ( G_Message_Code||'022'
                                                , G_Del_Flag
                                                , C_RIC_Symbol
                                                , Pricing_Msgs_Rec.Err_Text
                                                );
              V_Success := FALSE;
              GOTO GET_OUT;
            END IF;
          END IF;
        END IF;
        --
        -- Prepare the Record
        -- Keep same check as in procedure validate_or_create
        IF V_Record        LIKE C_Tab    || '%'
        OR UPPER(V_Record) LIKE C_Header || '%'
        OR UPPER(V_Record) LIKE C_Footer || '%'
        OR UPPER(V_Record) LIKE C_#tab   || '%'
        OR UPPER(V_Record) LIKE C_Issuer_Tab ||'%'
        OR UPPER(V_Record) LIKE V_More_Header
        OR V_Record        IS NULL
        THEN
            IF G_Tracing
            THEN
                pgmloc             := 2580;
                Load_Supplier_Rules.Write_File( Log_File
                                              , 'Skipped row # '
                                             || TO_CHAR(V_Rows_read)
                                              , Pricing_Msgs_Rec
                                              , V_Success
                                              );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            V_Rows_Skip := V_Rows_Skip + 1;
            GOTO Next_Row;
        END IF;
        --
        -- Looping Here to Process/Validate All Fields
        --
        IF V_Track_Progress
        THEN --{
            V_Row_Tracked := TRUE;
            pgmloc             := 2590;
            Load_Supplier_Rules.Write_File( Log_File
                                          , NULL
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc             := 2600;
            Load_Supplier_Rules.Write_File( Log_File
                                          , 'Processing Row # '
                                         || TO_CHAR(V_Rows_read)
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            Pgmloc := 2610;
            Load_Supplier_Rules.Write_File( Log_File
                                          , V_Record
                                          , Pricing_Msgs_Rec
                                          , V_Success
                                          );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF; --}
        Pgmloc := 2620;
        -- Initialise the record
--      Initialize_Univ_Row( Rec );
        Rec.Fri_Ticker                     := NULL;
        Rec.Fri_Id                         := NULL;
        Rec.Client_Code                    := NULL;
        Rec.Fri_Client                     := NULL;
        Rec.Client_Security_Id             := NULL;
        Rec.RIC                            := NULL;
        Rec.Source_Code                    := NULL;
        Rec.Price_Currency                 := NULL;
        Pgmloc := 2630;
        --
        -- parse the record
        FOR X IN 1..V_Num_Flds
        LOOP --{
            IF G_Tracing
            THEN
              pgmloc             := 2640;
              Load_Supplier_Rules.Write_File( Log_File
                                            , 'Field number '
                                           || X
                                            , Pricing_Msgs_Rec
                                            , V_Success
                                            );
              IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            pgmloc           := 2650;
            E_Value_01       := NULL;
            E_Value_02       := NULL;
            Field_Number     := X;
            Field_Numeric    := GT_Fields(Field_Number).F_Numeric;
            Field_Max_Length := GT_Fields(Field_Number).F_Max_Length;
            Field_Name       := GT_Fields(Field_Number).F_Name;
            Field_Format     := GT_Fields(Field_Number).F_Format;
            Field_Mandatory  := GT_Fields(Field_Number).F_Mandatory;
            IF Field_Name in ( C_Ric_Symbol
                             , C_Client_Code
                             , C_Client_Security_ID
                             )
            THEN
                NULL;
            ELSE
                GOTO Next_Fld;
            END IF;
            --
            pgmloc             := 2710;
            R_Pos_Start := 1;
            IF X > 1
            THEN
                R_Pos_Start := INSTR(V_Record, C_Tab, 1, X-1);
                IF R_Pos_Start < 1
                THEN
                    -- Field is missing - Checked later if mandatory
                    R_Pos_Len   := 0;
                    R_Field     := NULL;
                    R_Pos_Start := 0;
                ELSE
                    R_Pos_Start := R_Pos_Start + 1;
                END IF;
            END IF;
            R_Pos_End := INSTR(V_Record, C_Tab, 1, X) - 1;
            -- Ending tabs missing?
            IF R_Pos_End <= 0
            THEN
                R_Pos_End := LENGTH( V_Record );
            END IF;
            IF R_Pos_Start > 0
            THEN
                Pgmloc := 2720;
                R_Pos_Len := R_Pos_End - R_Pos_Start + 1;
                IF R_Pos_Len > C_Max_Field_Length
                THEN
                    R_Pos_Len := C_Max_Field_Length;
                END IF;
                Pgmloc  := 2730;
                R_Field := TRIM(SUBSTR(V_Record, R_Pos_Start, R_Pos_Len));
                IF INSTR( R_Field, ' [' ) > 0
                THEN
                  R_Field := TRIM( SUBSTR( R_Field, 1, INSTR( R_Field, ' [' ) - 1 ) );
                END IF;
                R_Field := TRIM( '"' FROM R_Field );
                R_Field := TRIM( C_Trim_Value FROM R_FIELD );
            END IF;
            IF G_Tracing
            THEN
                pgmloc             := 2740;
                Load_Supplier_Rules.Write_File( Log_File
                                              , Field_Name
                                             || ' Read as '''
                                             || R_Field
                                             || ''''
                                              , Pricing_Msgs_Rec
                                              , V_Success
                                              );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            pgmloc := 2750;
            V_Message   := '';
            IF LENGTH( R_Field ) > Field_Max_Length
            THEN --{
                IF NOT V_Row_Tracked
                THEN --{
                    V_Row_Tracked := TRUE;
                    pgmloc             := 2760;
                    Load_Supplier_Rules.Write_File( Log_File
                                                  , NULL
                                                  , Pricing_Msgs_Rec
                                                  , V_Success
                                                  );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 2770;
                    Load_Supplier_Rules.Write_File( Log_File
                                                  , 'Processing Row # '
                                                 || TO_CHAR(V_Rows_read)
                                                  , Pricing_Msgs_Rec
                                                  , V_Success
                                                  );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    Pgmloc := 2780;
                    Load_Supplier_Rules.Write_File ( Log_File
                                                   , V_Record
                                                   , Pricing_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                pgmloc := 2790;
                G_FMessage_Code := G_Message_Code || '010';
                Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                                  , G_Del_Flag
                                                  , Field_Name
                                                  , Field_Max_Length
                                                  , V_Message
                                                  );
                pgmloc := 2800;
                Load_Supplier_Rules.Write_File ( Log_File
                                               , C_Indent || V_Message
                                               , Pricing_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                R_Field := SUBSTR( R_Field, 1, Field_Max_Length );
            ELSIF R_Field IS NULL
              AND Field_Mandatory
            THEN --}{
                G_FMessage_Code := G_Message_Code || '020';
                Log_Error;
                Goto Next_Fld;
            END IF; --}
            pgmloc := 2810;
            IF Field_Numeric
            THEN --{
                BEGIN
                    V_Success := FALSE;
                    V_Number  := TO_NUMBER( R_Field );
                    V_Success := TRUE;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;
                IF NOT V_Success
                THEN
                    -- Success is set to TRUE when the field is numeric.
                    --{
                    G_FMessage_Code := G_Message_Code || '030';
                    E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                    Log_Error;
                    Goto Next_Fld;
                    --}
                END IF;
            END IF; --}
            pgmloc := 2820;
            IF Field_Format IS NOT NULL
            THEN --{
                BEGIN
                    V_Success := FALSE;
                    V_Date := TO_DATE( R_Field , Field_Format );
                    V_Success := TRUE;
                EXCEPTION
                WHEN OTHERS
                THEN
                    BEGIN
                        V_Date := TO_DATE( R_Field, 'YYYY-MM-DD' );
                        V_Success := TRUE;
                    EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                    END;
                END;
                IF NOT V_Success
                THEN
                    -- Success is set to TRUE when the field is a correct date.
                    --{
                    G_FMessage_Code := G_Message_Code || '040';
                    E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                    E_Value_02      := Field_Format;
                    Log_Error;
                    Goto Next_Fld;
                    --}
                END IF;
            END IF; --}
            --
            -- Individual fields
            --
            pgmloc := 2830;
            IF R_Field IS NULL
            THEN
                null;
            ELSIF Field_Name = C_Ric_Symbol
            THEN
                pgmloc := 2840;
                Rec.Ric := NVL( R_Field, Rec.Ric );
            ELSIF Field_Name = C_Client_Code
            THEN
                pgmloc := 2850;
                Rec.Client_Code := NVL( R_Field, Rec.Client_Code );
            ELSIF Field_Name = C_Client_Security_ID
            THEN
                pgmloc := 2860;
                Rec.Client_Security_ID := NVL( R_Field, Rec.Client_Security_ID );
            END IF;
<< Next_Fld >>
            pgmloc := 3350;
            NULL;
        END LOOP; --}
        --
        --
        -- Getting the ticker from id
        pgmloc := 3370;
        Get_Ticker_from_Ric( Rec.Ric, Rec.Fri_Ticker, Rec.Source_Code, Rec.Price_Currency );
        --
        pgmloc := 3380;
        IF Rec.Fri_Ticker IS NULL
        THEN
            -- This is an error
            G_FMessage_Code := G_Message_Code || '710';
            Field_Name      := Rec.Ric;
            Log_Error;
            GOTO Next_Row;
        ELSE
            Rec.Fri_Id := Security.Get_Fri_Id( Rec.Fri_Ticker );
        END IF;
        --
        -- Get the client from code (specific error)
        pgmloc := 0;
        OPEN Get_Client_from_Code( UPPER(Rec.Client_Code) );
        Clt_Rec.Fri_Client := NULL;
        pgmloc := 0;
        FETCH Get_Client_from_Code INTO Clt_Rec;
        pgmloc := 0;
        CLOSE Get_Client_from_Code;
        Pgmloc := 0;
        IF Clt_Rec.Fri_Client IS NULL
        THEN
            -- This is an error
            G_FMessage_Code := G_Message_Code || '700';
            Field_Name      := Rec.Client_Code;
            Log_Error;
            GOTO Next_Row;
        END IF;
        Rec.Fri_Client := Clt_Rec.Fri_CLient;
        --
        -- Get the client security id
        pgmloc := 0;
        OPEN Get_Client_Univ( Rec.Fri_Client, Rec.Client_Security_Id );
        CU_Rec.Client_Security_Id := NULL;
        CU_Rec.Fri_Client         := NULL;
        CU_Rec.Fri_Ticker         := NULL;
        pgmloc := 0;
        FETCH Get_Client_Univ INTO CU_Rec;
        pgmloc := 0;
        CLOSE Get_Client_Univ;
        Pgmloc := 0;
        IF cu_rec.Client_Security_Id is NULL
        THEN
            -- Client does not have the security id given
            -- This is an error
            G_FMessage_Code := G_Message_Code || '720';
            Field_Name      := Rec.Client_Security_Id;
            Log_Error;
        -- Is it already linked? to the one we have?
        ELSIF cu_rec.fri_ticker = rec.fri_ticker
        THEN
            -- Nothing to do
            G_FMessage_Code := G_Message_Code || '730';
            Field_Name      := Rec.Client_Security_Id;
            Log_Error;
        ELSIF cu_rec.fri_ticker IS NULL
        THEN
            -- We have to link
            Pgmloc := 0;
            UPDATE Clients_Univ
               SET FRI_TICKER = Rec.Fri_Ticker
             WHERE Fri_Client = Rec.Fri_CLient
               AND Client_Security_Id = REC.Client_Security_Id
                   ;
            G_FMessage_Code := G_Message_Code || '740';
            Field_Name      := Rec.Client_Security_Id;
            E_Value_01      := Rec.Fri_Id;
            Log_Error;
        ELSE
            -- is already linked to something else
            -- Nothing to do
            G_FMessage_Code := G_Message_Code || '750';
            Field_Name      := Rec.Client_Security_Id;
            E_Value_01      := Security.Get_Fri_Id( CU_Rec.Fri_Ticker );
            Log_Error;
        END IF;
        --
        pgmloc := 3620;
    
<< Next_Row >>
        --
        pgmloc := 3630;
        Pricing_Msgs_Rec.Value_01 := V_Rows_Read;
        Pricing_Msgs_Rec.Value_02 := V_Rows_Skip;
        Pricing_Msgs_Rec.Value_03 := V_Rows_Err ;
--      Pricing_Msgs_Rec.Value_04 := V_Secs_Ins ;
--      Pricing_Msgs_Rec.Value_05 := V_Secs_Upd ;
        Pricing_Msgs_Rec.Value_06 := V_Tick_Ins ;
        Pricing_Msgs_Rec.Value_07 := V_Trd_Ins  ;
        Pricing_Msgs_Rec.Value_08 := V_Mat_Ins  ;
        Pricing_Msgs_Rec.Value_09 := V_PrcN_Ins ;
        --
        pgmloc := 3640;
        Load_Supplier_Rules.Update_Prc_Msgs ( G_Program_ID
                                            , G_System_Date
                                            , Pricing_Msgs_Rec
                                            , V_Success
                                            );
        pgmloc := 3650;
        COMMIT;
        NULL;
    END LOOP; --}
    pgmloc := 3660;
    P_Success            := TRUE;
    -- verifier que le ticker peut etre ajouter
    -- ajouter le symbole
    --
    -- rapporter les symboles ajoutes
<< Get_Out >>
    NULL;
    --
END Add_Universes; --}}
-- Fin d'ajout - JSL - 25 septembre 2015
--
------------------------------------------------------------------------
--
PROCEDURE Validate_Or_Create
        ( P_Data_File     IN     Utl_File.File_Type
        , P_Log_File      IN     Utl_File.File_Type
        , P_Err_File      IN     Utl_File.File_Type
        , P_Create_Sec    IN     BOOLEAN
        , P_Create_Issuer IN     BOOLEAN
        , P_Prc_Msgs_Rec  IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Success       OUT    NOCOPY BOOLEAN
        ) IS
--{
    --
    End_Of_File          BOOLEAN;
    --
    V_CY                 Currencies.Currency%TYPE;
    V_Error              BOOLEAN;
    V_Warning            BOOLEAN;
    V_Fri_Ticker         Securities.Fri_Ticker%TYPE;
    V_Fri_Ticker_Start   Securities.Fri_Ticker%TYPE;
    V_Fri_Ticker_End     Securities.Fri_Ticker%TYPE;
    V_Fri_Ticker_Valid   BOOLEAN;
    V_ID_Errors          BOOLEAN;
    V_ID_Name            VARCHAR2(8);
    V_ID_Sys             Identification_Systems.Code%TYPE;
    V_Long_Name_Len      INTEGER;
    V_Message            VARCHAR2(255);
    V_New_Sec            BOOLEAN;
    V_New_Sec_Text       VARCHAR2(24);
    V_Num_Flds           INTEGER;
    V_Record             VARCHAR2(1024);
    V_Row_Errors         BOOLEAN;
    V_Script_Parm        VARCHAR2(20);
    V_Create_Sec         BOOLEAN;
    V_Create_Issuer      BOOLEAN;
    V_Valid              BOOLEAN;
--
    Delayed_Message   Varchar2(255);
    B_Delayed_Message BOOLEAN := FALSE;
    --
    V_Rows_Read          INTEGER := 0;
    V_Rows_Skip          INTEGER := 0;
    V_Rows_Err           INTEGER := 0;
    V_Secs_Ins           INTEGER := 0;
    V_Secs_Upd           INTEGER := 0;
    --
    V_Chk_Digit          INTEGER;
    V_Status             INTEGER;
    V_Success            BOOLEAN;
    V_System_Date        DATE;
    --
    -- Variable to fetch from Check_Unique_Security CURSOR
    V_Sec_Exists         Tickers.ticker%TYPE;
    -- Record Fields or Fields Fetch from DB
    --
    R_Field_Name         VARCHAR2(128);
    R_Field              VARCHAR2(2000);
    --
    R_IC_Parms           Interest_Calc_Parms%ROWTYPE;
    R_Index_Details      Index_Details%ROWTYPE;
    R_II_Rates           Interest_Income_Rates%ROWTYPE;
    R_Issuers            Issuers%ROWTYPE;
    R_Issues             Issues%ROWTYPE;
    R_Mat_Sched          Maturity_Schedule%ROWTYPE;
    R_PC_Sched           Pricing_Calc_Schedule%ROWTYPE;
    R_Prc_Notes          Pricing_Notes%ROWTYPE;
    R_Securities         Securities%ROWTYPE;
    R_ST_Status          Security_Trading_Status%ROWTYPE;
    R_Issuers_Xref       Issuers_Xref%ROWTYPE;
    --
    W_Identifiers        Many_Identifiers;
    B_Ticker_Found       BOOLEAN;
    B_Ticker_Same        BOOLEAN;
    B_Pending_Changes    BOOLEAN;
    --
    R_Fri_ID             Tickers.Ticker%TYPE;
    R_Fri_Issuer         Issuers.Fri_Issuer%TYPE;
    R_Fri_Ticker         Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_C       Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_D       Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_I       Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_O       Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_R       Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_S       Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_E_1     Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_E_2     Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_T       Securities.Fri_Ticker%TYPE;
    R_Fri_Ticker_P       Securities.Fri_Ticker%TYPE;
    R_ISO_Nation         Nations.ISO_Nation%TYPE;
    R_Issuer             Issuers.Code%TYPE;
    R_Issuer_Name        Issuers.Long_Name_E%TYPE;
    R_Issuer_Type        Issuers.Issuer_Type_Code%TYPE;
    R_Issuer_Sub_Type    Issuers.Issuer_Sub_Type_Code%TYPE;
    R_Long_Name_E        Securities.Long_Name_E%TYPE;
    R_Source             Sources.Code%TYPE;
    R_FRI_Cy             Currencies.Currency%TYPE;
    R_ISO_Cy             Currencies.ISO_Currency%TYPE;
    R_Cusip              Securities.Cusip%TYPE;
    R_ISIN               Tickers.Ticker%TYPE;
    R_ISIN_Cy            Currencies.Currency%TYPE;
    R_OISIN              Tickers.Ticker%TYPE;
    R_OISIN_Cy           Currencies.Currency%TYPE;
    R_SEDOL              Tickers.Ticker%TYPE;
    R_SEDOL_Cy           Currencies.Currency%TYPE;
    R_SEDOL_Nation       Nations.Code%TYPE;
    R_SEDOL_ISO_Nation   Nations.ISO_Nation%TYPE;
    R_RIC_Symbol         Tickers.Ticker%TYPE;
    R_RIC_Source         Sources.Code%TYPE;
    R_RIC_Cy             Currencies.Currency%TYPE;
    R_CINS_Symbol        Tickers.Ticker%TYPE;
    R_CINS_Cy            Currencies.Currency%TYPE;
    R_Exch_Symbol        Tickers.Ticker%TYPE;
    R_Exch_Source_1      Sources.Code%TYPE;
    R_Exch_Source_2      Sources.Code%TYPE;
    R_EDI_Symbol         Tickers.Ticker%TYPE;
    R_Pool_Number        Tickers.Ticker%TYPE;
    R_Creation_Date      DATE;
    R_Data_Supp_Code     Data_Suppliers.Code%TYPE;
    R_Data_Deliv_Time    Supplier_Running_Parms.Delivery_Time%TYPE;
    --
    W_Fri_Issuer         Issuers.Fri_Issuer%TYPE;
    W_Issuer_Name        Issuers.Long_Name_E%TYPE;
    W_Nation_Code        Nations.Code%TYPE;
    W_ISO_Nation_Code    Nations.ISO_Nation%TYPE;
    W_Source             Sources.Code%TYPE;
    W_FRI_Cy             Currencies.Currency%TYPE;
    W_ISO_Cy             Currencies.ISO_Currency%TYPE;
    W_Cusip              Securities.Cusip%TYPE;
    W_Issuer             Issuers.Code%TYPE;
    W_Long_Name_E        Securities.Long_Name_E%TYPE;
    W_Series             Securities.Series%TYPE;
    W_Class              Securities.Class%TYPE;
    W_Created            BOOLEAN;
    W_Id_Syst            Identification_Systems.Code%TYPE;
    W_Freq_Code          Frequencies.Code%TYPE;
    W_Freq_Value         Frequencies.Value%TYPE;
    W_Isid               Issuers_Xref.Issuer_Identifier%TYPE;
    W_Ids_Code           Issuers_Xref.Issuer_Ids_Code%TYPE;
    R_Ticker_Source      Sources.Code%TYPE;
    R_Data_FRI_Cy        Currencies.Currency%TYPE;
    R_Data_ISO_Cy        Currencies.ISO_Currency%TYPE;
    R_Ticker_FRI_Cy      Currencies.Currency%TYPE;
    R_Ticker_ISO_Cy      Currencies.ISO_Currency%TYPE;
    R_Ticker_Symbol      Tickers.Ticker%TYPE;
    R_Ticker_Mnemonic    Identification_Systems.Mnemonic%TYPE;
    Previous_Fri_Ticker  Securities.Fri_Ticker%TYPE;
    Previous_Fri_ID      Tickers.Ticker%TYPE;
    Previous_Cusip       Securities.Cusip%TYPE;
    Previous_Issuer      Issuers.Code%TYPE;
    Previous_Long_Name_E Securities.Long_Name_E%TYPE;
    Previous_Class       Securities.Class%TYPE;
    Previous_Series      Securities.Series%TYPE;
    Previous_Created     BOOLEAN;
    --
    -- Fields to be defaulted
    --
    R_Acc_Calc_Formula   Accruals_Calc_Formulas.Code%TYPE;
    R_Acc_Freq_Value     Frequencies.Value%TYPE;
    R_Book_Based_Flag    Yes_NO.Code%TYPE;
    R_Const_Prc_Flag     Yes_NO.Code%TYPE;
    R_Day_Count          Day_Count_Methods.Code%TYPE;
    R_Gen_Pay_Flag       Yes_NO.Code%TYPE;
    R_Income_Type        Securities.Income_Type_Code%TYPE;
    R_Last_User          Internal_Jobbers.Code%TYPE;
    R_MTN_Flag           Yes_NO.Code%TYPE;
    R_Ratings_Flag       Yes_No.Code%TYPE;
    R_SAC_Changed_Flag   Yes_No.Code%TYPE;
    R_Site_Date          DATE;
    R_Trd_Status         Trading_Status.Code%TYPE;
    R_User_ID            Internal_Jobbers.Code%TYPE;
    R_Series             Securities.Series%TYPE;
    R_Class              Securities.Class%TYPE;
    --
-- Est-ce que le par value est obtenu du fichier?
    B_Par_Value          BOOLEAN;
    B_Freq_Value         BOOLEAN;
    B_Income_Type        BOOLEAN;
    B_Call_Flag          BOOLEAN;
    B_Conv_Flag          BOOLEAN;
    B_E_R_Flag           BOOLEAN;
    B_Perp_Flag          BOOLEAN;
    B_Priv_Plac_Flag     BOOLEAN;
    B_Intangibility_Flag BOOLEAN;
    B_Redemption_Amount  BOOLEAN;
    B_Redemption_Currency BOOLEAN;
    B_Par_Currency       BOOLEAN;
    B_Income_Currency    BOOLEAN;
    B_Issuers_Xref       BOOLEAN;
    B_Isid               BOOLEAN;
-- Argument to create security even is a security with the same description
-- already exists.
    B_Create_Even        BOOLEAN;
    V_Create_Even        VARCHAR2(20);
    --
    B_US_Munis           BOOLEAN;
    R_US_Munis           US_MUNIS_Attributes%ROWTYPE;
    --
    R_Pos_Start          INTEGER;
    R_Pos_End            INTEGER;
    R_Pos_Len            INTEGER;
    --
    Field_Number         INTEGER;
    Field_Name           VARCHAR2(32);
    Field_Numeric        BOOLEAN;
    Field_Max_Length     INTEGER;
    Field_Format         VARCHAR2(20);
    Field_Mandatory      BOOLEAN;
    --
    C_Error_Len          INTEGER := 80;
    E_Value_01           VARCHAR2(80);
    E_Value_02           VARCHAR2(80);
    --
    V_Number             Number;
    V_Date               Date;
    --
    V_Error_Code         VARCHAR2(32);
    V_Error_Message      VARCHAR2(1024);
    V_Error_Column       All_Tab_Columns.Column_Name%TYPE;
    --
    V_More_Header        VARCHAR2(32);
    V_Track_Progress     BOOLEAN;
    V_Row_Tracked        BOOLEAN;
    V_Err_Tracked        BOOLEAN;
    --
    W_Id_Systems_Row     Identification_Systems%ROWTYPE;
    W_Ticker_Changes_Row Ticker_Changes%ROWTYPE;
    W_MAX_Issuer_Count   VARCHAR2(12);
    --
    W_Day_Count          Day_Count_Methods.Code%TYPE;
    W_Inflation_Linked   Yes_No.Code%TYPE;
    W_Acc_Calc_Formula   Accruals_Calc_Formulas.Code%TYPE;
    --
    W_Fri_Id             Tickers.Ticker%TYPE;
    V_Ticker             Tickers.Ticker%TYPE;
    W_Ticker             Tickers.Ticker%TYPE;
    --
    -- Value to be discarded as only for validation
    D_Issuer_Type        Issuers.Issuer_Type_Code%TYPE;
    W_Record_Status      VARCHAR2(16);
    --
    R_Index_Description      Index_Details.I_Description%TYPE;
    R_Index_Category         Index_Details.I_Category%TYPE;
    R_Index_Base_Date        Index_Details.Base_Date%TYPE;
    R_Index_Base_Value       Index_Details.Base_Value%TYPE;
    R_Index_Rebal_Freq_Value Frequencies.Value%TYPE;
    R_Index_Rebal_Freq_Code  Frequencies.Code%TYPE;
    --
    R_Security_Pattern_Code  Security_Patterns.Code%TYPE;
    --
    R_Matured_Bond           VARCHAR2(32);
--
    V_DNBO_issuer_name boolean;
    --
CURSOR Get_Income_Type
     ( Cur_Code Income_Types.Code%TYPE
     ) IS
SELECT IT.Code
  FROM Income_Types IT
 WHERE IT.Code = Cur_Code;
--
CURSOR Get_Issuer
     ( Cur_Issuer Issuers.Code%TYPE
     ) IS
SELECT I.Fri_Issuer
     , N.ISO_Nation
     , I.Long_Name_E
  FROM Issuers I,
       Nations N
 WHERE I.Code        = Cur_Issuer
   AND I.Nation_Code = N.Code;
--
CURSOR Get_Issuer_By_Name
     ( Cur_Long_Name_E Issuers.Long_Name_E%TYPE
     ) IS
SELECT I.Code
     , I.Fri_Issuer
     , I.Nation_Code
  FROM Issuers I
 WHERE UPPER( I.Long_Name_E ) = UPPER( Cur_Long_Name_E )
    OR UPPER( I.TEXT_E )      = UPPER( Cur_Long_Name_E );
--
CURSOR Get_Issuer_By_Ticker
     ( Cur_Fri_Ticker Securities.Fri_Ticker%TYPE
     ) IS
SELECT I.Code
     , I.Fri_Issuer
     , I.Nation_Code
  FROM Issuers I
     , Securities S
 WHERE I.Fri_Issuer = S.Fri_Issuer
   AND S.Fri_Ticker = Cur_Fri_Ticker;
--
CURSOR Get_MAX_Issuer_Code
     ( Cur_Generic_Code VARCHAR2
     ) IS
SELECT MAX( SUBSTR( Code, length( Cur_Generic_Code ) + 1 ) )
  FROM Issuers I
 WHERE REGEXP_SUBSTR( Code, Cur_Generic_Code || G_Reg_Expr )
       IS NOT NULL;
--
CURSOR Get_Issuer_Type
     ( Cur_Code Issuer_Types.Code%TYPE
     ) IS
SELECT IT.Code
  FROM Issuer_Types IT
 WHERE IT.Code = Cur_Code;
--
CURSOR Get_Issuer_Sub_Type
     ( Cur_Type     Issuer_Sub_Types.Issuer_Type_Code%TYPE
     , Cur_Sub_Type Issuer_Sub_Types.Issuer_Sub_Type_Code%TYPE
     ) IS
SELECT MIN(IST.Issuer_Type_Code)
     , MIN(IST.issuer_Sub_Type_Code)
  FROM Issuer_Sub_Types IST
 WHERE IST.Issuer_Type_Code     = NVL(Cur_Type, IST.Issuer_Type_Code)
   AND IST.Issuer_Sub_Type_Code = Cur_Sub_Type;
--
CURSOR Get_Source
     ( Cur_Source Sources.Code%TYPE
     ) IS
SELECT S.Code
  FROM Sources S
 WHERE S.Code = Cur_Source;
--
CURSOR Get_Trd_CY
     ( Cur_ISO_CY Currencies.ISO_Currency%TYPE
     ) IS
SELECT C.Currency
     , C.ISO_Currency
  FROM Currencies C
 WHERE C.ISO_Currency = Cur_ISO_CY;
--
CURSOR Get_Trd_CY_2
     ( Cur_FRI_CY Currencies.Currency%TYPE
     ) IS
SELECT C.Currency
     , C.ISO_Currency
  FROM Currencies C
 WHERE C.Currency = Cur_FRI_CY;
--
CURSOR Get_Freq
     ( Cur_Freq Frequencies.Code%TYPE
     ) IS
SELECT *
  FROM Frequencies F
 WHERE ( F.Code                    = Cur_Freq
      OR TRIM(TO_CHAR(F.Value))    = Cur_Freq
       )
   AND F.Compounding_Validity_Flag = 'Y';
R_Frequencies  Frequencies%ROWTYPE;
--
CURSOR Get_Sec_Type
     ( Cur_Sec_Type Security_Types.Code%TYPE
     ) IS
SELECT S.Code
  FROM Security_Types S
 WHERE S.Code = Cur_Sec_Type;
--
CURSOR Get_Sec_SubType
     ( Cur_Sec_Type    Security_Types.Code%TYPE
     , Cur_Sec_SubType Security_Sub_Types.Security_sub_Type_Code%TYPE
     ) IS
SELECT S.Security_Sub_Type_Code
  FROM Security_Sub_Types S
 WHERE S.Security_Type_Code     = Cur_Sec_Type
   AND S.Security_Sub_Type_Code = Cur_Sec_SubType;
--
CURSOR Get_Fri_Ticker
     ( Cur_ID_Sys Identification_Systems.Code%TYPE
     , Cur_Ticker Tickers.Ticker%TYPE
     ) IS
SELECT T.Fri_Ticker
  FROM Tickers T
 WHERE T.Identification_System_Code = Cur_ID_Sys
   AND T.Ticker                     = Cur_Ticker;
--
CURSOR Get_New_Tickers
     ( Cur_FRI_Ticker_Start Securities.Fri_Ticker%TYPE
     , Cur_FRI_Ticker_End   Securities.Fri_Ticker%TYPE
     ) IS
SELECT Security.Get_Fri_ID(S.Fri_Ticker) FRI_ID,
       S.Long_Name_E Issue_Name,
       I.Long_Name_E Issuer_Name,
       S.Income_Currency
     , I.Code Issuer_Code
  FROM Securities S,
       Issuers    I
 WHERE S.Fri_Ticker BETWEEN Cur_Fri_Ticker_Start AND Cur_Fri_Ticker_End
   AND S.Fri_Issuer = I.Fri_Issuer
   AND S.Last_User = G_User_Id
ORDER BY S.Fri_Ticker;
--
CURSOR Get_Supplier_Code
     ( Cur_Code Data_Suppliers.Code%TYPE
     ) IS
SELECT Code
  FROM Data_Suppliers
 WHERE Code = Cur_Code
   AND Fri_Data_Flag = 'Y';
--
CURSOR Get_Identification_System
     ( Cur_Mnemonic Identification_Systems.Mnemonic%TYPE
     ) IS
SELECT *
  FROM Identification_Systems
 WHERE Mnemonic = UPPER(Cur_Mnemonic);
--
CURSOR Get_Supplier_Delivery
     ( Cur_Code          Data_Suppliers.Code%TYPE
     , Cur_Delivery_Time Data_Supply_Schedule.Delivery_Time%TYPE
     ) IS
SELECT Delivery_Time
  FROM Supplier_Running_Parms
 WHERE Data_Supplier_Code = Cur_Code
   AND Delivery_Time      = Cur_Delivery_Time
   AND Product_Code       = 'LOD'
   AND Active_Flag        = 'Y';
--
CURSOR Get_Nation_Code
     ( Cur_Code Nations.Code%TYPE
     ) IS
SELECT N.Code
     , N.Iso_Nation
  FROM Nations N
 WHERE N.Code = Cur_Code;
--
CURSOR Get_Nation_Code_From_ISO
     ( Cur_ISO_Code Nations.ISO_Nation%TYPE
     ) IS
SELECT N.Code
     , N.Iso_Nation
  FROM Nations N
 WHERE N.ISO_Nation = Cur_ISO_Code;
--
CURSOR Get_ISO_Nation_From_Source
     ( Cur_Source_Code Sources.Code%TYPE
     ) IS
SELECT N.Iso_Nation
  FROM Nations N
     , Sources S
 WHERE N.Code = S.Nation_Code
   AND S.Code = Cur_Source_Code;
--
--
CURSOR Get_Maturity_Type
     ( Cur_Code Maturity_Types.Code%TYPE
     ) IS
SELECT MT.Code
  FROM Maturity_Types MT
 WHERE MT.Code = Cur_Code;
--
CURSOR Get_Day_Count
     ( Cur_Code       Day_Count_Methods.Mnemonic%TYPE
     ) IS
SELECT Code
  FROM Day_Count_Methods
 WHERE Code = Cur_Code
    OR Mnemonic = Cur_Code
     ;
--
CURSOR Get_Inisys_Day_Count
     ( Cur_Code       Day_Count_Methods.Mnemonic%TYPE
     ) IS
SELECT Code
  FROM Day_Count_Methods
 WHERE Inisys_Day_Count_Code = Cur_Code
    OR Mnemonic              = Cur_Code
     ;
--
CURSOR Get_Acc_Calc_Form
     ( Cur_Code       Accruals_Calc_Formulas.Code%TYPE
     ) IS
SELECT Code
  FROM Accruals_Calc_Formulas
 WHERE Code = Cur_Code
     ;
--
-- Obtenir les defauts pour les titres.
--
R_Sec_Controls Security_Controls%ROWTYPE;
--
--
R_FV Frequency_Validation%ROWTYPE;
--
R_SPC Security_Pattern_Controls%ROWTYPE;
--
-- Cursor to check if the security already exists
CURSOR Check_Unique_Security
     ( Cur_Fri_Issuer         Issuers.Fri_Issuer%TYPE
     , Cur_Sec_Type           Security_Types.Code%TYPE
     , Cur_Sec_Sub_Type       Security_Sub_Types.Security_sub_type_Code%TYPE
     , Cur_Long_Name_E        Securities.Long_Name_E%TYPE
     , Cur_Coupon             Securities.Indicated_Annual_Rate%TYPE
     , Cur_Freq_Value         Frequencies.Value%TYPE
     , Cur_Fri_Cy             Currencies.Currency%TYPE
     , Cur_Pricing_Note       Pricing_Notes.Pricing_Note%TYPE
     , Cur_Maturity           DATE
     , Cur_Maturity_Type      Maturity_Types.Code%TYPE
     , Cur_Perp_Flag          Yes_No.Code%TYPE
     ) IS
SELECT security.get_fri_id( S.fri_ticker ) fri_id
  FROM Securities         S
     , Maturity_Schedule  M
 WHERE S.FRI_Ticker             = M.FRI_Ticker --Join
   AND S.FRI_Issuer             = Cur_FRI_Issuer
   AND S.Security_Type_Code     = Cur_Sec_Type
   AND S.Security_Sub_Type_Code = Cur_Sec_Sub_Type
   AND upper(S.Long_Name_E)     = upper(Cur_Long_Name_E)
   AND NVL(S.Indicated_annual_Rate,-1)  = NVL(Cur_Coupon,-1)
   AND NVL(S.Income_Frequency_Value,-5) = NVL(Cur_Freq_Value,-5)
   AND S.Income_Currency        = Cur_FRI_Cy
   AND M.Maturity_Date          = Cur_Maturity
   AND M.Maturity_Type_Code     = Cur_Maturity_Type
   AND S.PERPETUAL_FLAG         = Cur_Perp_Flag
   AND Cur_PERP_FLAG            = 'N'
   AND ( Cur_Pricing_Note IS NULL
      OR EXISTS (
         SELECT 1
           FROM Pricing_Notes   P
          WHERE S.Fri_Ticker    = P.FRI_Ticker
            AND P.Pricing_Note  = Cur_Pricing_Note
         )
       )
UNION
SELECT security.get_fri_id( S.fri_ticker ) fri_id
  FROM Securities         S
 WHERE S.FRI_Issuer             = Cur_FRI_Issuer
   AND S.Security_Type_Code     = Cur_Sec_Type
   AND S.Security_Sub_Type_Code = Cur_Sec_Sub_Type
   AND upper(S.Long_Name_E)     = upper(Cur_Long_Name_E)
   AND NVL(S.Indicated_annual_Rate,-1)  = NVL(Cur_Coupon,-1)
   AND NVL(S.Income_Frequency_Value,-5) = NVL(Cur_Freq_Value,-5)
   AND S.Income_Currency        = Cur_FRI_Cy
   AND S.PERPETUAL_FLAG         = Cur_Perp_Flag
   AND ( Cur_PERP_FLAG          = 'Y'
      OR ( Cur_Perp_Flag        = 'N'
       AND Cur_Maturity            IS NULL
       AND Cur_Maturity_Type       IS NULL
         )
       )
   AND ( Cur_Pricing_Note IS NULL
      OR EXISTS (
         SELECT 1
           FROM Pricing_Notes   P
          WHERE S.Fri_Ticker    = P.FRI_Ticker
            AND P.Pricing_Note  = Cur_Pricing_Note
         )
       )
     ;
--
CURSOR Get_Issuers_Xref_Isid
     ( Cur_Isid        Issuers_Xref.Issuer_Identifier%TYPE
     , Cur_Ids_Code    Issuers_Xref.Issuer_Ids_Code%TYPE
     ) IS
SELECT *
  FROM Issuers_Xref
 WHERE Issuer_Ids_Code   = Cur_Ids_Code
   AND Issuer_Identifier = Cur_Isid;
--
-- Must stay internal to the procedure 
FUNCTION IS_Field_Present
       ( V_Field_Name IN VARCHAR2
       )
RETURN BOOLEAN IS --{
BEGIN --{
    FOR X in 1 .. G_Number_Of_Fields
    LOOP
-- Modification - JSL - 25 septembre 2015
--      IF GT_Fields(Field_Number).F_Name = V_Field_Name
        IF GT_Fields(X).F_Name = V_Field_Name
-- Fin de modification - JSL - 25 septembre 2015
        THEN
            RETURN TRUE;
        END IF;
    END LOOP;
    RETURN FALSE;
END Is_Field_Present; --}}

PROCEDURE Set_Defaults
IS --{
--
CURSOR Get_Security_Controls
     ( Cur_Security_Sec_Type  Security_Types.Code%TYPE
     , Cur_Income_Type_Code   Income_Types.Code%TYPE
     ) IS
SELECT *
  FROM Security_Controls s
 WHERE S.Security_Type_Code = Cur_Security_Sec_Type
   AND S.Income_Type_Code   = Cur_Income_Type_Code;
SC Security_Controls%ROWTYPE;
--
R_Security_Pattern_Code Security_patterns.Code%TYPE;
--
CURSOR Get_Security_Pattern_Controls
     ( Cur_Security_Pattern_Code  Security_Patterns.Code%TYPE
     ) IS
SELECT *
  FROM Security_Pattern_Controls SPC
 WHERE SPC.Security_Pattern_Code =  Cur_Security_Pattern_Code ;
SPC Security_Pattern_Controls%ROWTYPE;
--
BEGIN --{
    -- Get the security pattern
    Pgmloc := 3670;
    IF R_Securities.Income_Type_Code IS NULL
    THEN
        -- Do we have the field to get the income type?
        -- If Yes, then too bad
        IF Is_Field_Present( C_Income_Type )
        THEN
            GOTO Get_Out;
        ELSE
            Pgmloc := 3680;
            OPEN Get_Security_Controls( R_Securities.Security_Type_Code
                                      , R_Securities.Security_Sub_Type_Code
                                      );
            Pgmloc := 3690;
            FETCH Get_Security_Controls INTO SC;
            Pgmloc := 3700;
            CLOSE Get_Security_Controls;
            Pgmloc := 3710;
            R_Income_Type := SC.Income_Type_Code;
            R_Securities.Income_Type_Code := SC.Income_Type_Code;
            R_Securities.Income_Frequency_Value := NVL( R_Securities.Income_Frequency_Value
                                                      , SC.Default_Frequency_Value );
            R_Securities.Intangibility_Flag := NVL( R_Securities.Intangibility_Flag
                                                  , SC.Intangibility_Flag
                                                  );
        END IF;
    END IF;
    Pgmloc := 3720;
    -- Get the security_pattern
    BEGIN
    R_Security_Pattern_Code := Security.Get_Sec_Type_Pattern( R_Securities.Security_Type_Code
                                                            , R_Securities.Income_Frequency_Value
                                                            );
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        R_Security_Pattern_Code := NULL;
        pgmloc := 3730;
        Load_Supplier_Rules.Write_File ( Log_File
                                       , 'Cannot get the security pattern  : TYPE <'
                                      || R_Securities.Security_Type_Code
                                      || '>  Frequency <'
                                      || R_Securities.Income_Frequency_Value
                                      || '>'
                                       , Pricing_Msgs_Rec
                                       , V_Success
                                       );
    WHEN OTHERS        THEN PGMLOC := 3740; RAISE;
    END;
    -- Get the security_pattern controls
    IF R_Securities.Income_Type_Code = 'I'
    THEN
        IF NOT Is_Field_Present( C_Acc_Form_Code )
        THEN
            R_Acc_Calc_Formula := NVL( R_Acc_Calc_Formula, 'SIU');
            R_Acc_Freq_Value   := NVL( R_Acc_Freq_Value, 365 );
        END IF;
        IF NOT Is_Field_Present( C_Par_Currency )
        THEN
           R_Securities.Par_Currency := NVL( R_Securities.Par_Currency
                                           , NVL( R_Securities.Income_Currency, 'US$' )
                                           );
        END IF;
    END IF;
    Pgmloc := 3750;
<< Get_Out >>
    Pgmloc := 3760;
    NULL;
END Set_Defaults; --}}
    --
BEGIN
    --{
    pgmloc  := 3770;
    --
    -- Initialize Variables
    --
    P_Success            := FALSE;
    --
    pgmloc  := 3780;
    V_DNBO_issuer_name :=
      case SCRIPTS.Get_Arg_Value ( G_Program_id, 'DNBO_ISSUER_NAME')
      when 'Y' then true
      else false
      end;
    Pgmloc        := 3790;
    B_Create_Even := FALSE;
    IF P_Create_SEC
    THEN --{
      Pgmloc        := 3800;
      V_Create_Even := 'Y';
      IF V_Create_Even = 'Y'
      THEN --{
        -- Are we set to Create even if ...
        Pgmloc := 3810;
        V_Create_Even
          := UPPER( NVL( Scripts.Get_Arg_Value( P_Prc_Msgs_Rec.Program_Id
                                              , 'EVEN' )
                       , 'N' ) );
        IF V_Create_Even = 'Y'
        THEN --{
          B_Create_Even := TRUE;
        END IF; --}
      END IF; --}
    END IF; --}
    --
    V_Fri_Ticker_Start := '';
    V_Long_name_Len    := 50;
    V_Num_Flds         := G_Number_Of_Fields;
    V_More_Header      := UPPER( G_More_Header );
    V_Track_Progress   := G_Track_Progress;
    IF V_More_Header IS NULL
    THEN
        V_More_Header  := C_TAB||'%';
    END IF;
    IF V_Track_Progress IS NULL
    THEN
        V_Track_Progress := TRUE;
    END IF;
    Previous_Fri_Ticker  := NULL;
    Previous_Fri_ID      := NULL;
    Previous_Cusip       := NULL;
    Previous_Issuer      := NULL;
    Previous_Long_Name_E := NULL;
    Previous_Class       := NULL;
    Previous_Series      := NULL;
    Previous_Created     := FALSE;
    --
    End_of_File        := FALSE;
    --
    -- Process all Lines find in File of Foreign Bonds
    --
    pgmloc := 3820;
    WHILE NOT End_Of_File
    LOOP
        --{
        W_Record_Status    := NULL;
        V_Row_Errors       := FALSE;
        V_Warning          := FALSE;
        V_Row_Tracked      := FALSE;
        V_Err_Tracked      := FALSE;
        --
        pgmloc := 3830;
        Load_Supplier_Rules.Get_Line( P_Data_File
                                    , P_Prc_Msgs_Rec
                                    , V_Record
                                    , V_Success
                                    , End_Of_File
                                    );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        EXIT WHEN End_Of_File;
        --
        pgmloc := 3840;
        V_Rows_Read := V_Rows_Read + 1;
        V_Create_Sec    := FALSE;
        V_Create_Issuer := FALSE;
        IF G_Tracing
        THEN
            pgmloc             := 3850;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , 'Read row # '
                                          || TO_CHAR(V_Rows_read)
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc             := 3860;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , V_Record
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF;
        --
        IF UPPER( V_Record ) LIKE C_Header || '%'
        OR UPPER(V_Record) LIKE C_Issuer_Tab ||'%'
        THEN
          -- Write the header in the error file so we can reload it later on
          Pgmloc := 3870;
          Load_Supplier_Rules.Write_File ( P_Err_File
                                         , V_Record
                                         , P_Prc_Msgs_Rec
                                         , V_Success
                                         );
          IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
          IF G_Auto
          THEN
            Initialize_Auto_Fields( V_Record
                                  , P_Log_File
                                  , P_Prc_Msgs_Rec
                                  , V_Success
                                  );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            V_Num_Flds := G_Number_Of_Fields;
            IF G_Tracing
            THEN
              pgmloc             := 3880;
              Load_Supplier_Rules.Write_File ( P_Log_File
                                             , 'Number of fields '
                                            || G_Number_Of_Fields
                                             , P_Prc_Msgs_Rec
                                             , V_Success
                                             );
              IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
          END IF;
        END IF;
        --
        -- Prepare the Record
        --
        IF V_Record        LIKE C_Tab    || '%'
        OR UPPER(V_Record) LIKE C_Header || '%'
        OR UPPER(V_Record) LIKE C_Footer || '%'
        OR UPPER(V_Record) LIKE C_#tab   || '%'
        OR UPPER(V_Record) LIKE C_Issuer_Tab ||'%'
        OR UPPER(V_Record) LIKE V_More_Header
        OR V_Record        IS NULL
        THEN
            IF G_Tracing
            THEN
                pgmloc             := 3890;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Skipped row # '
                                              || TO_CHAR(V_Rows_read)
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            V_Rows_Skip := V_Rows_Skip + 1;
            GOTO Next_Row;
        END IF;
        --
        -- Looping Here to Process/Validate All Fields
        --
        IF V_Track_Progress
        THEN --{
            V_Row_Tracked := TRUE;
            pgmloc             := 3900;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , NULL
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc             := 3910;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , 'Processing Row # '
                                          || TO_CHAR(V_Rows_read)
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF; --}
        --
        pgmloc             := 3920;
        --
        R_Fri_Ticker_C     := '';
        R_Fri_Ticker_D     := '';
        R_Fri_Ticker_I     := '';
        R_Fri_Ticker_O     := '';
        R_Fri_Ticker_R     := '';
        R_Fri_Ticker_S     := '';
        R_Fri_Ticker_T     := '';
        R_Fri_Ticker_E_1   := NULL;
        R_Fri_Ticker_E_2   := NULL;
        R_Fri_Ticker_P     := NULL;
        V_ID_Errors        := FALSE;
        R_Fri_Ticker := '';
        V_Fri_Ticker_Valid := FALSE;
        --
        pgmloc := 3930;
        Initialize_Index_Details_Row( R_Index_Details );
        BR_Index_Details := FALSE;
        pgmloc := 3940;
        Initialize_Int_Calc_Parms_Row( R_IC_Parms );
        BR_IC_Parms := FALSE;
        pgmloc := 3950;
        Initialize_Int_Inc_Rates_Row( R_II_Rates );
        BR_II_Rates := FALSE;
        pgmloc := 3960;
        Initialize_Issuers_Row( R_Issuers );
        BR_Issuers := FALSE;
        pgmloc := 3970;
        Initialize_Issues_Row( R_Issues );
        BR_Issues := FALSE;
        pgmloc := 3980;
        Initialize_Mat_Schedule_Row( R_Mat_Sched );
        BR_Mat_Sched := FALSE;
        pgmloc := 3990;
        Initialize_PC_Schedule_Row( R_PC_Sched );
        BR_PC_Sched := FALSE;
        pgmloc := 4000;
        Initialize_Prc_Notes_Row( R_Prc_Notes );
        BR_Prc_Notes := FALSE;
        pgmloc := 4010;
        Initialize_Securities_Row( R_Securities );
        BR_Securities := FALSE;
        pgmloc := 4020;
        Initialize_ST_Status_Row( R_ST_Status );
        BR_ST_Status := FALSE;
        BR1_ST_Status := FALSE;
        BR2_ST_Status := FALSE;
        --
        pgmloc := 4030;
        Initialize_Issuers_Xref_Row( R_Issuers_Xref );
        B_Issuers_Xref := FALSE;
        --
        pgmloc := 4032;
        Initialize_US_Munis_Attr_Row ( R_Us_Munis );
        B_Us_Munis := FALSE;
        --
        R_Fri_ID             := NULL;
        R_Fri_Issuer         := NULL;
        R_Fri_Ticker         := NULL;
        R_ISO_Nation         := NULL;
        R_Issuer             := NULL;
        R_Issuer_Name        := NULL;
        R_Issuer_Type        := NULL;
        R_Issuer_Sub_Type    := NULL;
        R_Long_Name_E        := NULL;
        R_Source             := NULL;
        R_FRI_Cy             := NULL;
        R_ISO_Cy             := NULL;
        R_Cusip              := NULL;
        R_ISIN               := NULL;
        R_OISIN              := NULL;
        R_SEDOL              := NULL;
        R_Creation_Date      := NULL;
        R_Data_Supp_Code     := NULL;
        R_Data_Deliv_Time    := NULL;
        R_Ticker_Source      := NULL;
        R_Data_FRI_Cy        := NULL;
        R_Data_ISO_Cy        := NULL;
        R_Ticker_FRI_Cy      := NULL;
        R_Ticker_ISO_Cy      := NULL;
        R_Ticker_Symbol      := NULL;
        R_ISIN_Cy            := NULL;
        R_OISIN_Cy           := NULL;
        R_SEDOL_Cy           := NULL;
        R_RIC_Symbol         := NULL;
        R_RIC_Source         := NULL;
        R_RIC_Cy             := NULL;
        R_EDI_Symbol         := NULL;
        R_Pool_Number        := NULL;
        R_Exch_Symbol        := NULL;
        R_Exch_Source_1      := NULL;
        R_Exch_Source_2      := NULL;
        R_CINS_Symbol        := NULL;
        R_CINS_Cy            := NULL;
        R_SEDOL_Nation       := NULL;
        R_SEDOL_ISO_Nation   := NULL;
        R_Matured_Bond       := NULL;
        W_Issuer_Name        := NULL;
        W_Source             := NULL;
        W_FRI_Cy             := NULL;
        W_ISO_Cy             := NULL;
        --
        W_Id_Syst            := G_Id_Syst;
        IF G_Index_Details
        THEN
            R_Index_Description      := NULL;
            R_Index_Category         := NULL;
            R_Index_Base_Date        := NULL;
            R_Index_Base_Value       := NULL;
            R_Index_Rebal_Freq_Value := NULL;
            R_Index_Rebal_Freq_Code  := NULL;
        END IF;
        --
        R_Acc_Calc_Formula   := G_Acc_Calc_Formula;
        R_Acc_Freq_Value     := G_Acc_Freq_Value;
        R_Book_Based_Flag    := G_Book_Based_Flag;
        R_Const_Prc_Flag     := G_Const_Prc_Flag;
        R_Day_Count          := G_Day_Count;
        R_Gen_Pay_Flag       := G_Gen_Pay_Flag;
        R_Income_Type        := G_Income_Type;
        R_Last_User          := G_User_Id;
        R_Mat_Sched.Maturity_Type_Code      := G_Maturity_Type;
        R_MTN_Flag           := G_MTN_Flag;
        R_Securities.Conversion_Schedule_Flag          := G_Conv_Flag;
        R_Securities.Par_Value          := G_Par_Value;
        R_Ratings_Flag       := G_Ratings_Flag;
-- -- Le redemption amount et currency sont prises apres plusieurs validations
-- --   R_Redemption_Amount  := G_Redemption_Amount;
-- --   R_Redemption_Currency := G_Redemption_Currency;
        R_SAC_Changed_Flag   := G_SAC_Changed_Flag;
        R_Site_Date          := G_Site_Date;
        R_Trd_Status         := G_Trd_Status;
        R_User_ID            := G_User_ID;
        R_Series             := G_Series;
        R_Class              := G_Class;
-- Est-ce que le par value est obtenu du fichier?
        B_Par_Value           := FALSE;
        B_Freq_Value          := FALSE;
        B_Income_Type         := FALSE;
        B_Call_Flag           := FALSE;
        B_Conv_Flag           := FALSE;
        B_E_R_Flag            := FALSE;
        B_Perp_Flag           := FALSE;
        B_Priv_Plac_Flag      := FALSE;
        B_Intangibility_Flag  := FALSE;
        B_Redemption_Amount   := FALSE;
        B_Redemption_Currency := FALSE;
        B_Par_Currency        := FALSE;
        B_Income_Currency     := FALSE;
        W_Created             := FALSE;
        --
        FOR X IN 1..V_Num_Flds
        LOOP --{
            IF G_Tracing
            THEN
              pgmloc             := 4040;
              Load_Supplier_Rules.Write_File ( P_Log_File
                                             , 'Field number '
                                            || X
                                             , P_Prc_Msgs_Rec
                                             , V_Success
                                             );
              IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            pgmloc           := 4050;
            E_Value_01       := NULL;
            E_Value_02       := NULL;
            Field_Number     := X;
            pgmloc           := 4060;
            Field_Numeric    := GT_Fields(Field_Number).F_Numeric;
            pgmloc           := 4070;
            Field_Max_Length := GT_Fields(Field_Number).F_Max_Length;
            pgmloc           := 4080;
            Field_Name       := GT_Fields(Field_Number).F_Name;
            pgmloc           := 4090;
            Field_Format     := GT_Fields(Field_Number).F_Format;
            pgmloc           := 4100;
            Field_Mandatory  := GT_Fields(Field_Number).F_Mandatory;
            IF Field_Name = C_Ignore
            THEN
                GOTO Next_Fld;
            END IF;
            --
            pgmloc             := 4110;
            R_Pos_Start := 1;
            IF X > 1
            THEN
                R_Pos_Start := INSTR(V_Record, C_Tab, 1, X-1);
                IF R_Pos_Start < 1
                THEN
                    -- Field is missing - Checked later if mandatory
                    R_Pos_Len   := 0;
                    R_Field     := NULL;
                    R_Pos_Start := 0;
                ELSE
                    R_Pos_Start := R_Pos_Start + 1;
                END IF;
            END IF;
            pgmloc             := 4120;
            R_Pos_End := INSTR(V_Record, C_Tab, 1, X) - 1;
            -- Ending tabs missing?
            IF R_Pos_End <= 0
            THEN
                R_Pos_End := LENGTH( V_Record );
            END IF;
            IF R_Pos_Start > 0
            THEN
                Pgmloc := 4130;
                R_Pos_Len := R_Pos_End - R_Pos_Start + 1;
                IF R_Pos_Len > C_Max_Field_Length
                THEN
                    R_Pos_Len := C_Max_Field_Length;
                END IF;
                Pgmloc  := 4140;
                R_Field := TRIM(SUBSTR(V_Record, R_Pos_Start, R_Pos_Len));
                IF INSTR( R_Field, ' [' ) > 0
                THEN
                  R_Field := TRIM( SUBSTR( R_Field, 1, INSTR( R_Field, ' [' ) - 1 ) );
                END IF;
                R_Field := TRIM( '"' FROM R_Field );
                R_Field := TRIM( C_Trim_Value FROM R_FIELD );
            END IF;
            IF G_Tracing
            THEN
                pgmloc             := 4150;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                                 , Field_Name
                                                 || ' Read as '''
                                                 || R_Field
                                                 || ''''
                                                  , P_Prc_Msgs_Rec
                                                 , V_Success
                                                  );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            pgmloc := 4160;
            V_Message   := '';
            -- Special clause for perpetual maturity
            IF  TRIM(R_Field)    = 'PERP'
            AND Field_Name = C_Maturity_Date
            THEN
                R_Field := '';
            END IF;
            IF LENGTH( R_Field ) > Field_Max_Length
            THEN --{
                IF NOT V_Row_Tracked
                THEN --{
                    V_Row_Tracked := TRUE;
                    pgmloc             := 4170;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , NULL
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 4180;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Processing Row # '
                                                  || TO_CHAR(V_Rows_read)
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                pgmloc := 4190;
                G_FMessage_Code := G_Message_Code || '010';
                Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                                  , G_Del_Flag
                                                  , Field_Name
                                                  , Field_Max_Length
                                                  , V_Message
                                                  );
                pgmloc := 4200;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , C_Indent || V_Message
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                R_Field := SUBSTR( R_Field, 1, Field_Max_Length );
            ELSIF R_Field IS NULL
              AND Field_Mandatory
            THEN --}{
                G_FMessage_Code := G_Message_Code || '020';
                GOTO Log_Error;
            END IF; --}
            IF Field_Numeric
            THEN --{
                pgmloc             := 4210;
                BEGIN
                    V_Success := FALSE;
                    V_Number  := TO_NUMBER( R_Field );
                    V_Success := TRUE;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;
                pgmloc             := 4220;
                IF NOT V_Success
                THEN
                    -- Success is set to TRUE when the field is numeric.
                    --{
                    G_FMessage_Code := G_Message_Code || '030';
                    E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                    --}
                END IF;
            END IF; --}
            IF Field_Format IS NOT NULL
            THEN --{
                BEGIN
                    pgmloc             := 4230;
                    V_Success := FALSE;
                    V_Date := TO_DATE( R_Field , Field_Format );
                    V_Success := TRUE;
                EXCEPTION
                WHEN OTHERS
                THEN
                    BEGIN
                        pgmloc             := 4240;
                        V_Date := TO_DATE( R_Field, 'YYYY-MM-DD' );
                        V_Success := TRUE;
                    EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                    END;
                END;
                IF NOT V_Success
                THEN
                    -- Success is set to TRUE when the field is a correct date.
                    --{
                    G_FMessage_Code := G_Message_Code || '040';
                    E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                    E_Value_02      := Field_Format;
                    GOTO Log_Error;
                    --}
                END IF;
            END IF; --}
            --
            -- Individual fields
            --
            pgmloc             := 4250;
            IF R_Field IS NULL
            THEN
                null;
            ELSIF Field_Name = C_Income_Type
            THEN --{
                R_Income_Type := UPPER( R_Field );
                B_Income_Type := TRUE;
                pgmloc := 4260;
                OPEN Get_Income_Type(R_Income_Type);
                pgmloc := 4270;
                FETCH Get_Income_Type
                 INTO R_Securities.Income_Type_Code;
                IF Get_Income_Type % NOTFOUND
                THEN
                    pgmloc := 4280;
                    CLOSE Get_Income_Type;
                    G_FMessage_Code := G_Message_Code || '050';
                    E_Value_01 := SUBSTR(R_Field, 1, C_Error_Len);
                    GOTO Log_Error;
                END IF;
                pgmloc := 4290;
                CLOSE Get_Income_Type;
                BR_Securities := TRUE;
            ELSIF Field_Name = C_Issuer_Code
            THEN --}{
                pgmloc := 4300;
                -- Get the Fri Issuer
                R_Issuer := UPPER( R_Field );
                W_Issuer := R_Field;
                IF R_Issuer = 'JUNK'
                OR R_Issuer = 'TEMP'
                OR R_Issuer IS NULL
                THEN --{
                    IF P_Create_Issuer
                    THEN --{
                        -- Will be validated later with the name
                        NULL;
                    ELSE --}{
                        G_FMessage_Code := G_Message_Code || '050';
                        E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                        GOTO Log_Error;
                    END IF; --} If Create_Issuer
                ELSE --}{
                    pgmloc := 4310;
                    OPEN  Get_Issuer (R_Issuer);
                    pgmloc := 4320;
                    FETCH Get_Issuer
                     INTO R_Fri_Issuer
                        , R_ISO_Nation
                        , W_Issuer_Name;
                    IF Get_Issuer%NotFound
                    THEN
                        pgmloc := 4330;
                        CLOSE Get_Issuer;
                        G_FMessage_Code := G_Message_Code || '050';
                        E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                        GOTO Log_Error;
                    END IF;
                    pgmloc := 4340;
                    CLOSE Get_Issuer;
                    R_Issuers.Fri_Issuer    := R_Fri_Issuer;
                    R_Securities.Fri_Issuer := R_Fri_Issuer;
                    BR_Issuers    := TRUE;
                    BR_Securities := TRUE;
                END IF; --} If JUNK or TEMP
            ELSIF Field_Name = C_Issuer_Name
            THEN --}{
                R_Issuer_Name               := R_Field;
                R_Issuers.Long_Name_E       := R_Field;
                R_Issuers.Long_Name_F       := R_Field;
                R_Issuers.Text_E            := SUBSTR( R_Field, 1, 30 );
                R_Issuers.Text_F            := SUBSTR( R_Field, 1, 30 );
                R_Issuers.Publishing_Name_E := SUBSTR( R_Field, 1, 12 );
                R_Issuers.Publishing_Name_F := SUBSTR( R_Field, 1, 12 );
                BR_Issuers    := TRUE;
            ELSIF Field_Name = C_Issuer_Name_F
            THEN --}{
                R_Issuers.Long_Name_F       := R_Field;
                R_Issuers.Text_F            := SUBSTR( R_Field, 1, 30 );
                R_Issuers.Publishing_Name_F := SUBSTR( R_Field, 1, 12 );
                BR_Issuers    := TRUE;
            ELSIF Field_Name = C_Issuer_Name_2
            THEN --}{
                R_Issuers.Long_Name_2_E     := R_Field;
                R_Issuers.Long_Name_2_F     := R_Field;
                BR_Issuers    := TRUE;
            ELSIF Field_Name = C_Issuer_Name_2F
            THEN --}{
                R_Issuers.Long_Name_2_F     := R_Field;
                BR_Issuers    := TRUE;
            ELSIF Field_Name IN ( C_Issuer_Nation
                                , C_Issue_Nation
                                , C_Sedol_Nation
                                )
            THEN --}{
                W_Nation_Code := UPPER(R_Field);
                pgmloc := 4350;
                OPEN Get_Nation_Code(W_Nation_Code);
                pgmloc := 4360;
                FETCH Get_Nation_Code
                 INTO W_Nation_Code
                    , W_ISO_Nation_Code;
                IF Get_Nation_Code % NOTFOUND
                THEN
                    pgmloc := 4370;
                    CLOSE Get_Nation_Code;
                    G_FMessage_Code := G_Message_Code || '050';
                    E_Value_01 := SUBSTR(R_Field, 1, C_Error_Len);
                    GOTO Log_Error;
                END IF;
                pgmloc := 4380;
                CLOSE Get_Nation_Code;
                IF Field_Name = C_Issuer_Nation
                THEN
                    R_Issuers.Nation_Code := W_Nation_Code;
                    BR_Issuers    := TRUE;
                ELSIF Field_Name = C_Issue_Nation
                THEN
                    NULL; -- ICITTE -- N'est pas utilise!
                ELSIF Field_Name = C_Sedol_Nation
                THEN
                    R_Sedol_Nation     := W_Nation_Code;
                    R_Sedol_ISO_Nation := W_ISO_Nation_Code;
                END IF;
            ELSIF Field_Name IN ( C_Sedol_Iso_Nation
                                )
            THEN --}{
                IF R_Field IS NOT NULL
                THEN --{
                    W_Nation_Code := UPPER(R_Field);
                    pgmloc := 4390;
                    OPEN Get_Nation_Code_From_ISO(W_Nation_Code);
                    pgmloc := 4400;
                    FETCH Get_Nation_Code_From_ISO
                     INTO W_Nation_Code
                        , W_ISO_Nation_Code;
                    IF Get_Nation_Code_From_ISO % NOTFOUND
                    THEN
                        pgmloc := 4410;
                        CLOSE Get_Nation_Code_From_ISO;
                        G_FMessage_Code := G_Message_Code || '050';
                        E_Value_01 := SUBSTR(R_Field, 1, C_Error_Len);
                        GOTO Log_Error;
                    END IF;
                    pgmloc := 4420;
                    CLOSE Get_Nation_Code_From_ISO;
                    R_Sedol_Nation     := W_Nation_Code;
                    R_Sedol_ISO_Nation := W_ISO_Nation_Code;
                END IF; --}
            ELSIF Field_Name IN ( C_Isid
                                )
            THEN --}{
                W_Isid := UPPER(R_Field);
                W_Ids_Code := C_ISID_IDS;
                B_Isid := TRUE;
            ELSIF Field_Name IN ( C_Issuer_Xref_Type
                                )
            THEN --}{
                W_Ids_Code := UPPER(R_Field);
                B_Isid     := TRUE;
            ELSIF Field_Name IN ( C_Issuer_Xref_Code
                                )
            THEN --}{
                W_Isid := UPPER(R_Field);
                B_Isid := TRUE;
            ELSIF Field_Name = C_Issuer_Type
            THEN --}{
                pgmloc := 4430;
                OPEN get_issuer_type( UPPER( R_Field ) );
                pgmloc := 4440;
                FETCH get_issuer_type
                 INTO R_Issuers.Issuer_Type_Code;
                IF get_issuer_type % NOTFOUND
                THEN
                    pgmloc := 4450;
                    CLOSE get_issuer_type;
                    g_fmessage_code := g_message_code || '050';
                    e_value_01 := SUBSTR(r_field, 1, c_error_len);
                    GOTO log_error;
                END IF;
                pgmloc := 4460;
                CLOSE get_issuer_type;
                BR_Issuers    := TRUE;
            ELSIF field_name = c_issuer_sub_type
            THEN --}{
                pgmloc := 4470;
                OPEN get_issuer_sub_type( NULL, UPPER( R_Field ) );
                pgmloc := 4480;
                FETCH get_issuer_sub_type
                 INTO d_issuer_type
                    , R_Issuers.Issuer_Sub_Type_Code;
                IF get_issuer_sub_type % NOTFOUND
                OR D_Issuer_Type IS NULL
                THEN
                    pgmloc := 4490;
                    CLOSE get_issuer_sub_type;
                    g_fmessage_code := g_message_code || '050';
                    e_value_01 := SUBSTR(r_field, 1, c_error_len);
                    GOTO log_error;
                END IF;
                pgmloc := 4500;
                CLOSE get_issuer_sub_type;
                BR_Issuers    := TRUE;
            ELSIF Field_Name = C_Coupon
            THEN --}{
                -- Numeric is already validated.
                pgmloc := 4510;
                R_Securities.Indicated_Annual_Rate := TO_NUMBER(R_Field);
                BR_Securities := TRUE;
            ELSIF Field_Name = C_Redemption
            THEN --}{
                -- Numeric is already validated.
                pgmloc := 4520;
                R_Mat_Sched.Redemption_Amount := TO_NUMBER( R_Field );
                B_Redemption_Amount           := TRUE;
                BR_Mat_Sched := TRUE;
            ELSIF Field_Name = C_Par_Value
            THEN --}{
                -- Numeric is already validated.
                pgmloc := 4530;
                R_Securities.Par_Value := TO_NUMBER(R_Field);
                -- Par value provient du fichier
                B_Par_Value := TRUE;
                BR_Securities := TRUE;
            ELSIF Field_Name = C_Maturity_Date
            THEN --}{
                pgmloc := 4540;
                R_Mat_Sched.Maturity_Date := V_Date;
                BR_Mat_Sched := TRUE;
            ELSIF Field_Name = C_Maturity_Type
            THEN --}{
                IF  R_Field IS NULL
                AND NOT Field_Mandatory
                THEN --{
                    NULL;
                ELSE --}{
                    pgmloc := 4560;
                    OPEN get_maturity_type( UPPER( R_Field ) );
                    pgmloc := 4570;
                    FETCH get_maturity_type
                     INTO R_Mat_Sched.Maturity_Type_Code;
                    IF get_maturity_type % NOTFOUND
                    THEN
                        pgmloc := 4580;
                        CLOSE get_maturity_type;
                        g_fmessage_code := g_message_code || '050';
                        e_value_01 := SUBSTR(r_field, 1, c_error_len);
                        GOTO log_error;
                    END IF;
                    pgmloc := 4590;
                    CLOSE get_maturity_type;
                    BR_Mat_Sched := TRUE;
                END IF; --}
            ELSIF Field_Name = C_First_Pay_Date
            THEN --}{
                pgmloc := 4600;
                R_II_Rates.First_Payment_Date := V_Date;
                BR_II_Rates := TRUE;
-- -- -- -- Creation date is not used
-- -- -- -- ELSIF Field_Name = C_Creation_Date
-- -- -- -- THEN --}{
-- -- -- --     pgmloc := 4610;
-- -- -- --     R_Creation_Date := TO_DATE( R_Field, Field_Format );
            ELSIF Field_Name = C_Issue_Offer_Date
            THEN --}{
                pgmloc := 4620;
                R_Issues.Offering_Date := V_Date;
                BR_Issues := TRUE;
            ELSIF Field_Name = C_Accrual_Start_Date
            THEN --}{
                pgmloc := 4630;
                R_II_Rates.Accruals_Start_Date := V_Date;
                BR_II_Rates := TRUE;
            ELSIF Field_Name = C_Data_Supp_Code
            THEN --}{
                -- The combination data supplier and delivery time is
                -- validated at the end, when all fields are in
                pgmloc := 4640;
                R_Data_Supp_Code := UPPER( R_Field );
                pgmloc := 4650;
                OPEN  Get_Supplier_Code ( R_Data_Supp_Code );
                pgmloc := 4660;
                FETCH Get_Supplier_Code
                 INTO R_PC_Sched.Data_Supplier_Code;
                IF Get_Supplier_Code%NotFound
                THEN
                    pgmloc := 4670;
                    CLOSE Get_Supplier_Code;
                    G_FMessage_Code := G_Message_Code || '070';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                pgmloc := 4680;
                CLOSE Get_Supplier_Code;
                BR_PC_Sched := TRUE;
            ELSIF Field_Name = C_Data_Deliv_Time
            THEN --}{
                -- The combination data supplier and delivery time is
                -- validated at the end, when all fields are in
                pgmloc := 4690;
                R_Data_Deliv_Time := R_Field;
                R_PC_Sched.Delivery_Time := R_Field;
                BR_PC_Sched := TRUE;
            ELSIF Field_Name = C_Ticker_Symbol
            THEN --}{
                -- The combination is validated at the end, when all fields
                -- are in
                pgmloc := 4700;
                R_Ticker_Symbol := R_Field;
            ELSIF Field_Name = C_Exch_Symbol
            THEN --}{
                -- The combination is validated at the end, when all fields
                -- are in
                pgmloc := 4710;
                R_Exch_Symbol := R_Field;
            ELSIF Field_Name = C_EDI_Symbol
            THEN --}{
                -- The combination is validated at the end, when all fields
                -- are in
                pgmloc := 4720;
                R_EDI_Symbol := LPAD( TRIM( R_Field ), 9, '0');
            ELSIF Field_Name = C_Pool_Number
            THEN --}{
                -- The combination is validated at the end, when all fields
                -- are in
                pgmloc := 4720;
                R_Pool_Number := R_Field;
            ELSIF Field_Name = C_Ticker_Mnemonic
            THEN --}{
                -- The combination is validated at the end, when all fields
                -- are in
                pgmloc := 4730;
                W_Id_Syst         := NULL;
                R_Ticker_Mnemonic := R_Field;
                pgmloc := 4740;
                OPEN  Get_Identification_System ( R_Ticker_Mnemonic );
                pgmloc := 4750;
                FETCH Get_Identification_System
                 INTO W_Id_Systems_Row;
                IF Get_Identification_System%NotFound
                THEN
                    pgmloc := 4760;
                    CLOSE Get_Identification_System;
                    G_FMessage_Code := G_Message_Code || '070';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                pgmloc := 4770;
                CLOSE Get_Identification_System;
                W_Id_Syst := W_Id_Systems_Row.Code;
            ELSIF Field_Name = C_Sec_Long_Name
            THEN --}{
                pgmloc := 4780;
                -- Validate the Long_Name
                R_Long_Name_E := 'NO NAME';
                IF R_Field IS NOT NULL
                THEN --{
                    pgmloc := 4790;
                    R_Long_Name_E := R_Field;
                    R_Securities.Long_Name_E := R_Long_name_E;
                    BR_Securities := TRUE;
                END IF; --} IF R_Field IS NOT NULL
                W_Long_Name_E := R_Field;
            ELSIF Field_Name = C_Sec_Long_Name_F
            THEN --}{
                pgmloc := 4800;
                IF R_Field IS NOT NULL
                THEN --{
                    pgmloc := 4810;
                    R_Securities.Long_Name_F := R_Field;
                    BR_Securities := TRUE;
                END IF; --} IF R_Field IS NOT NULL
            ELSIF Field_Name = C_Sec_Long_Name_2
            THEN --}{
                pgmloc := 4820;
                IF R_Field IS NOT NULL
                THEN --{
                    pgmloc := 4830;
                    R_Securities.Long_Name_2_E := R_Field;
                    BR_Securities := TRUE;
                END IF; --} IF R_Field IS NOT NULL
            ELSIF Field_Name = C_Sec_Long_Name_2F
            THEN --}{
                pgmloc := 4840;
                IF R_Field IS NOT NULL
                THEN --{
                    pgmloc := 4850;
                    R_Securities.Long_Name_2_F := R_Field;
                    BR_Securities := TRUE;
                END IF; --} IF R_Field IS NOT NULL
            ELSIF Field_Name = C_Serie
            THEN --}{
                pgmloc := 4860;
                R_Series := UPPER( R_Field );
                W_Series := R_Field;
                R_Securities.Series := R_Series;
                BR_Securities := TRUE;
            ELSIF Field_Name = C_Class
            THEN --}{
                pgmloc := 4870;
                R_Class := UPPER( R_Field );
                W_Class := R_Field;
                R_Securities.Class := R_Class;
                BR_Securities := TRUE;
            ELSIF Field_Name IN ( C_Market_Code
                                , C_Data_Source_Code
                                , C_Trading_Source
                                , C_Ticker_Source
                                , C_Ric_Source
                                , C_Exch_Source_1
                                , C_Exch_Source_2
                                )
            THEN --}{
                -- Is it a Valid Source Code
                IF  Field_Name = C_Exch_Source_2
                AND R_Field IS NULL
                THEN
                    -- This field may be null, so don't care if it is.
                    NULL;
                ELSIF Field_Name = C_Ric_Source
                  AND R_Field      IS NULL
                  AND R_RIC_CY     IS NULL
                  AND R_RIC_SYMBOL IS NULL
                THEN
                    NULL;
                ELSE
                    pgmloc := 4880;
                    Open  Get_Source (R_Field);
                    pgmloc := 4890;
                    FETCH Get_Source
                     INTO W_Source;
                    IF Get_Source%NotFound
                    THEN
                        pgmloc := 4900;
                        Close Get_Source;
                        G_FMessage_Code := G_Message_Code || '050';
                        E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                        GOTO Log_Error;
                    END IF;
                    pgmloc := 4910;
                    Close Get_Source;
                    IF Field_Name = C_Market_Code
                    THEN
                        R_Source := W_Source;
                        R_PC_Sched.Source_Code := W_Source;
                        BR_PC_Sched := TRUE;
-- -- -- -- -- -- -- -- As the market code is used for the pricing source,
-- -- -- -- -- -- -- -- it should also be in trading status.
                        R_ST_Status.Source_Code := W_Source;
                        BR_ST_Status := TRUE;
                    ELSIF Field_Name = C_Data_Source_Code
                    THEN
                        R_ST_Status.Source_Code := W_Source;
                        BR_ST_Status := TRUE;
                    ELSIF Field_Name = C_Trading_Source
                    THEN
                        R_ST_Status.Source_Code := W_Source;
                        BR_ST_Status := TRUE;
                    ELSIF Field_Name = C_Ticker_Source
                    THEN
                        R_Ticker_Source := W_Source;
                    ELSIF field_name = c_ric_source
                    THEN
                        r_ric_source := w_source;
                    ELSIF Field_name = C_Exch_Source_1
                    THEN
                        R_Exch_Source_1 := W_Source;
                        BR1_ST_Status := TRUE;
                    ELSIF Field_name = C_Exch_Source_2
                    THEN
                        R_Exch_Source_2 := W_Source;
                        BR2_ST_Status := TRUE;
                    END IF;
                END IF;
            ELSIF Field_Name IN ( C_Trading_Currency
                                , C_Iso_Ccy_Code
                                , C_Par_Currency
                                , C_Data_Currency
                                , C_Income_Currency
                                , C_Ticker_Currency
                                , c_ric_currency
                                , c_sedol_currency
                                , c_cins_currency
                                , C_ISIN_Currency
                                , C_OISIN_Currency
                                , C_Redemption_Currency
                                )
            THEN --}{
                IF  Field_Name       = C_Ticker_Currency
                AND UPPER( R_Field ) = 'NUL'
                THEN
                    R_Field := NULL;
                END IF;
                IF R_Field IS NOT NULL
                THEN --{
                    -- Is it a Valid ISO Currency
                    pgmloc := 4920;
                    Open  Get_Trd_CY (R_Field);
                    pgmloc := 4930;
                    Fetch Get_Trd_CY
                     into W_FRI_CY
                        , W_ISO_CY;
                    IF Get_Trd_CY%NotFound
                    THEN --{
                        pgmloc := 4940;
                        Close Get_Trd_CY;
                        -- I will try using the FRI Currency
                        pgmloc := 4950;
                        Open  Get_Trd_CY_2 (R_Field);
                        pgmloc := 4960;
                        Fetch Get_Trd_CY_2
                         into W_FRI_CY
                            , W_ISO_CY;
                        pgmloc := 4970;
                        IF Get_Trd_CY_2%NotFound
                        THEN
                            pgmloc := 4980;
                            Close Get_Trd_CY_2;
                            G_FMessage_Code := G_Message_Code || '050';
                            E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                            GOTO Log_Error;
                        END IF;
                        pgmloc := 4990;
                        Close Get_Trd_CY_2;
                    END IF; --} IF Get_Trd_CY%NotFound
                    pgmloc := 5000;
                    IF Get_Trd_CY%ISOPEN
                    THEN
                        pgmloc := 5010;
                        Close Get_Trd_CY;
                    END IF;
                    IF Field_Name = C_Trading_Currency
                    THEN
                        R_ST_Status.Price_Currency := W_FRI_Cy;
                        IF W_Fri_Cy IS NOT NULL
                        THEN
                            BR_ST_Status := TRUE;
                        END IF;
                    ELSIF Field_Name = C_Iso_Ccy_Code
                    THEN
                        R_FRI_Cy := W_Fri_Cy;
                        R_ISO_Cy := W_Iso_Cy;
                    ELSIF Field_Name = C_Par_Currency
                    THEN
                        R_Securities.Par_Currency := W_FRI_Cy;
                        BR_Securities := TRUE;
                        B_Par_Currency := TRUE;
                    ELSIF Field_Name = C_Data_Currency
                    THEN
                        R_Data_FRI_Cy := W_Fri_Cy;
                        R_Data_ISO_Cy := W_Iso_Cy;
                    ELSIF Field_Name = C_Income_Currency
                    THEN
                        R_Securities.Income_Currency := W_FRI_Cy;
                        BR_Securities := TRUE;
                        B_Income_Currency := TRUE;
                    ELSIF Field_Name = C_Ticker_Currency
                    THEN
                        R_Ticker_FRI_Cy := W_Fri_Cy;
                        R_Ticker_ISO_Cy := W_Iso_Cy;
                    ELSIF field_name = c_ric_currency
                    THEN
                        r_ric_cy := w_fri_cy;
                    ELSIF field_name = c_sedol_currency
                    THEN
                        r_sedol_cy := w_fri_cy;
                    ELSIF field_name = c_ISIN_currency
                    THEN
                        r_ISIN_cy := w_fri_cy;
                    ELSIF field_name = c_OISIN_currency
                    THEN
                        r_OISIN_cy := w_fri_cy;
                    ELSIF field_name = c_cins_currency
                    THEN
                        r_cins_cy := w_fri_cy;
                    ELSIF field_name = C_Redemption_Currency
                    THEN
                        B_Redemption_Currency := TRUE;
                        R_Mat_Sched.Redemption_Currency := W_FRI_Cy;
                        BR_Mat_Sched := TRUE;
                    END IF;
                END IF; --}
            ELSIF Field_Name in ( C_Income_Freq
                                , C_Index_Rebalance_Frequency
                                )
            THEN --}{
                -- Is it a Valid Frequency?
                w_freq_code := r_field;
                IF Field_Name = C_Income_Freq
                THEN
                    B_Freq_Value := TRUE;
                END IF;
                IF w_freq_code IS NULL
                THEN
                    w_freq_code := 'NF';
                END IF;
                pgmloc := 5020;
                Open  Get_Freq (W_Freq_Code);
                pgmloc := 5030;
                Fetch Get_Freq
                 into R_Frequencies;
                IF Get_Freq%NotFound
                THEN
                    pgmloc := 5040;
                    Close Get_Freq;
                    G_FMessage_Code := G_Message_Code || '050';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                pgmloc := 5050;
                Close Get_Freq;
                IF Field_Name = C_Income_Freq
                THEN
                    R_Securities.Income_Frequency_Value := R_Frequencies.Value;
                    BR_Securities := TRUE;
                ELSIF Field_Name = C_Index_Rebalance_Frequency
                AND   R_Field IS NOT NULL
                THEN
                    IF R_Frequencies.Annual_Validity_Flag = 'Y'
                    THEN
                        NULL;
                    ELSE
                        G_FMessage_Code := G_Message_Code || '520';
                        E_Value_01 := SUBSTR( W_Freq_Code, 1, C_Error_Len );
                        GOTO Log_Error;
                    END IF;
                    R_Index_Rebal_Freq_Code  := R_Frequencies.Code;
                    R_Index_Rebal_Freq_Value := R_Frequencies.Value;
                    R_Index_Details.Rebal_Frequency_Value := R_Frequencies.Value;
                    BR_Index_Details := TRUE;
                END IF;
            ELSIF Field_Name = C_Pricing_Note
            THEN --}{
                -- Load the Pricing Note
                pgmloc := 5060;
                R_Prc_Notes.Pricing_Note := R_Field;
                BR_Prc_Notes := TRUE;
            ELSIF Field_Name = C_Security_Type
            THEN --}{
                pgmloc := 5070;
                Open  Get_Sec_Type (R_Field);
                pgmloc := 5080;
                Fetch Get_Sec_Type
                 into R_Securities.Security_Type_Code;
                IF Get_Sec_Type%NotFound
                THEN
                    pgmloc := 5090;
                    Close Get_Sec_Type;
                    G_FMessage_Code := G_Message_Code || '050';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                pgmloc := 5100;
                Close Get_Sec_Type;
                BR_Securities := TRUE;
            ELSIF Field_Name = C_Security_Sub_Type
            THEN --}{
                -- Is it a Valid Security Sub Type?
                pgmloc := 5110;
                Open  Get_Sec_SubType (R_Securities.Security_Type_Code, R_Field);
                pgmloc := 5120;
                Fetch Get_Sec_SubType
                 into R_Securities.Security_Sub_Type_Code;
                IF Get_Sec_SubType%NotFound
                THEN
                    pgmloc := 5130;
                    Close Get_Sec_SubType;
                    G_FMessage_Code := G_Message_Code || '050';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                pgmloc := 5140;
                Close Get_Sec_SubType;
                BR_Securities := TRUE;
            ELSIF Field_Name IN ( C_Call_Flag
                                , C_Floating_Flag
                                , C_Convertible_Flag
                                , C_Perpetual_Flag
                                , C_E_R_Flag
                                , C_Sched_Event_Flag
                                )
            THEN --}{
                IF R_Field IS NULL
                THEN
                    R_Field := 'N';
                END IF;
                IF R_Field NOT IN ( 'Y', 'N' )
                THEN
                    G_FMessage_Code := G_Message_Code || '080';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                IF Field_Name = C_Call_Flag
                THEN
                    B_Call_Flag := TRUE;
                    R_Securities.Call_Schedule_Flag := R_Field;
                    BR_Securities := TRUE;
                ELSIF Field_Name = C_Floating_Flag
                THEN
                    R_IC_Parms.Floating_Rate_Flag := R_Field;
                    BR_IC_Parms := TRUE;
                ELSIF Field_Name = C_Convertible_Flag
                THEN
                    B_Conv_Flag  := TRUE;
                    R_Securities.Conversion_Schedule_Flag := R_Field;
                    BR_Securities := TRUE;
                ELSIF Field_Name = C_Perpetual_Flag
                THEN
                    B_Perp_Flag  := TRUE;
                    R_Securities.Perpetual_Flag := R_Field;
                    BR_Securities := TRUE;
                ELSIF Field_Name = C_E_R_Flag
                THEN
                    B_E_R_Flag   := TRUE;
                    R_Securities.Ext_Ret_Flag := R_Field;
                    BR_Securities := TRUE;
                ELSIF Field_Name = C_Priv_Plac_Flag
                THEN
                    R_Issues.Private_Placement_Flag := R_Field;
                    BR_Issues := TRUE;
                ELSIF Field_Name = C_Sched_Event_Flag
                THEN
                    R_II_Rates.Scheduled_Event_Flag := R_Field;
                    BR_II_Rates := TRUE;
                END IF;
            ELSIF Field_Name IN ( C_Priv_Plac_Flag
                                , C_Inflation_Linked
                                )
            THEN --}{
                IF NVL( R_Field, 'N' ) NOT IN ( 'Y', 'N' )
                THEN
                    G_FMessage_Code := G_Message_Code || '080';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                IF Field_Name = C_Priv_Plac_Flag
                THEN
                    R_Issues.Private_Placement_Flag := R_Field;
                    BR_Issues := TRUE;
                ELSIF Field_Name = C_Inflation_Linked
                THEN
                    R_Securities.Inflation_Linked_Bond_Flag := R_Field;
                END IF;
            ELSIF Field_Name IN ( C_Cusip
                                , C_CINS_Symbol
                                , C_Isin
                                , C_Oisin
                                , C_RIC_Symbol
                                , C_Sedol
                                )
            THEN --}{
                pgmloc := 5150;
                R_Fri_Ticker := '';
                V_Fri_Ticker := '';
                --
                IF Field_Name = C_Cusip
                THEN
                    V_ID_Sys      := C_ID_CUSIP;
                    V_ID_Name     := 'CUSIP';
                    R_Cusip       := '';
                    W_Cusip       := R_Field;
                ELSIF Field_Name = C_CINS_Symbol
                THEN
                    V_Id_Sys      := C_ID_CINS;
                    V_Id_Name     := 'CINS';
                    R_CINS_Symbol := NULL;
                ELSIF Field_Name = C_Isin
                THEN
                    V_ID_Sys      := C_ID_ISIN;
                    V_ID_Name     := 'ISIN';
                    R_ISIN        := '';
                ELSIF Field_Name = C_Oisin
                THEN
                    V_ID_Sys      := C_ID_ISIN;
                    V_ID_Name     := 'OISIN';
                    R_OISIN       := '';
                ELSIF Field_Name = C_RIC_Symbol
                THEN
                    V_ID_Sys      := C_ID_RIC;
                    V_ID_Name     := 'RIC';
                    R_Sedol       := NULL;
                ELSIF Field_Name = C_Sedol
                THEN
                    V_ID_Sys      := C_ID_SEDOL;
                    V_ID_Name     := 'SEDOL';
                    R_Sedol       := '';
                END IF;
                --
                IF R_Field = 'TEMP'
                OR R_FIELD = 'TMPCUSIP'
                THEN
                    R_Field := NULL;
                ELSIF R_Field LIKE 'TMP%'
                  AND Field_Name = C_Cusip
                THEN
                    R_Cusip := R_Field;
                    R_Field := NULL;
                END IF;
                --
                pgmloc := 5160;
                IF R_Field IS NOT NULL
                THEN --{
                    R_Fri_Ticker := NULL;
                    -- Is it a Valid Cusip, ISIN, OISIN or Sedol?
                    pgmloc := 5170;
                    Valid_Sec_id.Ticker ( V_Fri_Ticker
                                        , R_Field
                                        , V_ID_Sys
                                        , V_Chk_Digit
                                        , V_Status
                                        , V_Message
                                        ) ;
                    --
                    pgmloc := 5180;
                    V_Message := TRIM(V_Message);
                    --
                    IF V_Status   = 0
                    THEN --{
                        pgmloc := 5190;
                        IF Field_Name = C_Cusip
                        THEN --{
                            R_Cusip            := R_Field;
                        ELSIF Field_Name = C_CINS_Symbol
                        THEN --}{
                            R_CINS_Symbol := R_Field;
                        ELSIF Field_Name = C_Isin
                        THEN --}{
                            R_ISIN  := R_Field;
                        ELSIF Field_Name = C_Oisin
                        THEN --}{
                            R_OISIN := R_Field;
                        ELSIF Field_Name = C_RIC_Symbol
                        THEN --}{
                            R_Ric_Symbol := R_Field;
                        ELSIF Field_Name = C_Sedol
                        THEN --}{
                            R_Sedol := R_Field;
                        END IF; --}
                    ELSIF V_Status = 1
                    THEN --}{
                        pgmloc := 5200;
                        V_ID_Errors := TRUE;
                        G_FMessage_Code := G_Message_Code || '090';
                        Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                                          , G_Del_Flag
                                                          , V_ID_Name
                                                          , R_Field
                                                          , V_Message
                                                          );
                        GOTO Log_Error_Got_Message;
                    ELSIF V_Status   > 0
                    AND   V_Message IS NULL
                    THEN --}{
                        pgmloc := 5210;
                        V_ID_Errors := TRUE;
                        G_FMessage_Code := G_Message_Code || '100';
                        Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                                          , G_Del_Flag
                                                          , V_ID_Name
                                                          , R_Field
                                                          , V_Message
                                                          );
                        GOTO Log_Error_Got_Message;
                    ELSIF V_Status   > 0
                    AND   V_Message IS NOT NULL
                    THEN --}{
                        pgmloc := 5220;
                        V_ID_Errors := TRUE;
                        Delayed_Message   := V_Message;
                        B_Delayed_Message := TRUE;
                        G_FMessage_Code := G_Message_Code || '110';
                        Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                                          , G_Del_Flag
                                                          , V_ID_Name
                                                          , R_Field
                                                          , V_Message
                                                          );
                        GOTO Log_Error_Got_Message;
                    END IF; --}
                END IF; --} IF R_Field IS NOT NULL
            ELSIF Field_Name = C_Index_Category
            AND   R_Field IS NOT NULL
            THEN --}{
                pgmloc := 5230;
                R_Index_Details.I_Category := R_Field;
                BR_Index_Details := TRUE;
            ELSIF Field_Name = C_Index_Description
            AND   R_Field IS NOT NULL
            THEN --}{
                pgmloc := 5240;
                R_Index_Details.I_Description := R_Field;
                BR_Index_Details := TRUE;
            ELSIF Field_Name = C_Index_Base_Value
            AND   R_Field IS NOT NULL
            THEN --}{
                pgmloc := 5250;
                R_Index_Details.Base_Value := TO_NUMBER( R_Field );
                BR_Index_Details := TRUE;
            ELSIF Field_Name = C_Index_Base_Date
            AND   R_Field IS NOT NULL
            THEN --}{
                pgmloc := 5260;
                R_Index_Details.Base_Date := V_Date;
                BR_Index_Details := TRUE;
            ELSIF Field_Name = C_Day_Count
            THEN --}{
                pgmloc := 5270;
                Open  Get_Day_Count( UPPER(R_Field ));
                pgmloc := 5280;
                Fetch Get_Day_Count
                 into W_Day_Count;
                IF Get_Day_Count%Found
                THEN
                    R_Day_Count := W_Day_Count;
                ELSE
                    pgmloc := 5290;
                    Close Get_Day_Count;
                    G_FMessage_Code := G_Message_Code || '050';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                pgmloc := 5300;
                Close Get_Day_Count;
            ELSIF Field_Name = C_Inisys_Day_Count
            THEN --}{
                pgmloc := 5310;
                Open  Get_Inisys_Day_Count( UPPER(R_Field ));
                pgmloc := 5320;
                Fetch Get_Inisys_Day_Count
                 into W_Day_Count;
                IF Get_Inisys_Day_Count%Found
                THEN
                    R_Day_Count := W_Day_Count;
                ELSE
                    pgmloc := 5330;
                    Close Get_Inisys_Day_Count;
                    G_FMessage_Code := G_Message_Code || '050';
                    E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                    GOTO Log_Error;
                END IF;
                pgmloc := 5340;
                Close Get_Inisys_Day_Count;
            ELSIF Field_Name = C_Acc_Form_Code
            THEN --}{
                IF R_Field IS NULL
                THEN
                    NULL;
                ELSE
                    pgmloc := 5350;
                    Open  Get_Acc_Calc_Form( UPPER(R_Field ));
                    pgmloc := 5360;
                    Fetch Get_Acc_Calc_Form
                     into W_Acc_Calc_Formula;
                    IF Get_Acc_Calc_Form%Found
                    THEN
                        R_Acc_Calc_Formula := W_Acc_Calc_Formula;
                    ELSE
                        pgmloc := 5370;
                        Close Get_Acc_Calc_Form;
                        G_FMessage_Code := G_Message_Code || '050';
                        E_Value_01 := SUBSTR( R_Field, 1, C_Error_Len );
                        GOTO Log_Error;
                    END IF;
                    pgmloc := 5380;
                    Close Get_Acc_Calc_Form;
                END IF;
            ELSIF Field_Name = C_US_Issuer_Type
            THEN --}{
                pgmloc := 5381;
                B_Us_Munis := TRUE;
                R_Us_Munis.Issuer_Type := R_Field;
            ELSIF Field_Name = C_Type_I
            THEN --}{
                pgmloc := 5382;
                B_Us_Munis := TRUE;
                R_Us_Munis.Type_I := R_Field;
            ELSIF Field_Name = C_Type_II
            THEN --}{
                pgmloc := 5383;
                B_Us_Munis := TRUE;
                R_Us_Munis.Type_II := R_Field;
            ELSIF Field_Name = C_Type_III
            THEN --}{
                pgmloc := 5384;
                B_Us_Munis := TRUE;
                R_Us_Munis.Type_III := R_Field;
            ELSIF Field_Name = C_Taxation
            THEN --}{
                pgmloc := 5385;
                B_Us_Munis := TRUE;
                R_Us_Munis.Taxation_Flag := R_Field;
            ELSIF Field_Name = C_Insurance
            THEN --}{
                pgmloc := 5386;
                B_Us_Munis := TRUE;
                R_Us_Munis.Insurance_Flag := R_Field;
            ELSIF Field_Name = C_Refinancing
            THEN --}{
                pgmloc := 5387;
                B_Us_Munis := TRUE;
                R_Us_Munis.Refinancing_Flag := R_Field;
            ELSIF Field_Name = C_State
            THEN --}{
                pgmloc := 5388;
                B_Us_Munis := TRUE;
                R_Us_Munis.State := UPPER( R_Field );
            ELSIF Field_Name = C_Matured_Bond
            THEN --}{
                pgmloc := 5388;
                R_Matured_Bond := UPPER( R_Field );
            END IF; --}
            --
            -- Setting defaults that are not in the format
            -- on auto mode only
            --
            --
            -- More validations required
            --
            Pgmloc  := 5390;
            R_Field := '';
            --
            IF X = V_Num_Flds
            THEN --{
                IF BR_Mat_Sched
                THEN
                  -- Maturity Date must be in future
                  pgmloc := 4550;
                  IF R_Mat_Sched.Maturity_Date <= TRUNC( SYSDATE )
                  THEN
                    IF R_Matured_Bond = C_Matured
                    THEN
                      NULL;
                    ELSE
                      G_FMessage_Code := G_Message_Code || '060';
                      E_Value_01      := SUBSTR( R_Field, 1, C_Error_Len );
                      E_Value_02      := TO_CHAR( SYSDATE, Field_Format );
                      GOTO Log_Error;
                    END IF;
                  END IF;
                END IF;
                --
                R_Field_Name    := NULL;
                R_Field         := NULL;
                R_Fri_Ticker_T  := NULL;
                --
                -- Default the trading currency and source
                -- to the one from the ticker.
                -- Must stay at top as these are used with multiple tickers...
                R_FRI_Cy := NVL( R_FRI_Cy, R_Ticker_Fri_Cy );
                R_Source := NVL( R_Source, R_Ticker_Source );
                R_ST_Status.Price_Currency
                    := NVL( R_ST_Status.Price_Currency, R_FRI_Cy );
                R_ST_Status.Source_Code
                    := NVL( R_ST_Status.Source_Code, R_Source );
                R_Securities.Income_Type_Code
                    := NVL( R_Securities.Income_Type_Code, R_Income_Type );
                R_Securities.Security_Type_Code
                    := NVL( R_Securities.Security_Type_Code, G_Sec_Type );
                R_Securities.Security_Sub_Type_Code
                    := NVL( R_Securities.Security_Sub_Type_Code, G_Sec_SubType);
                R_Securities.Income_Frequency_Value
                    := NVL( R_Securities.Income_Frequency_Value, G_Freq_Value );
                R_Securities.Series
                    := NVL( R_Securities.Series, R_Series );
                R_Securities.Class
                    := NVL( R_Securities.Class, R_Class );
                R_Securities.Call_Schedule_Flag
                    := NVL( R_Securities.Call_Schedule_Flag, G_Call_Flag );
                R_Securities.Perpetual_Flag
                    := NVL( R_Securities.Perpetual_Flag , 'N' );
                R_Issuers.Issuer_Type_Code
                    := NVL( R_Issuers.Issuer_Type_Code, R_Issuer_Type );
                R_Issuers.Issuer_Sub_Type_Code
                    := NVL( R_Issuers.Issuer_Sub_Type_Code, R_Issuer_Sub_Type );
                IF B_Redemption_Amount
                THEN
                    R_Mat_Sched.Redemption_Currency := NVL( R_Mat_Sched.Redemption_Currency, R_Fri_Cy );
                END IF;
                IF B_Par_Value
                THEN
                    R_Securities.Par_Currency := NVL( R_Securities.Par_Currency, R_Fri_Cy );
                END IF;
                R_Securities.Income_Currency := NVL( R_Securities.Income_Currency, R_Fri_Cy );
                Pgmloc := 5400;
                IF NOT V_Row_Errors
                THEN
                    Set_Defaults;
                END IF;
                --
                pgmloc := 5410;
                Clear_Ticker ( W_Identifiers
                             , P_Prc_Msgs_Rec
                             , V_Success
                             );
                IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                --
                -- Insert the tickers
                --
                IF R_CUSIP IS NOT NULL
                THEN
                    Pgmloc := 5420;
                    Set_Ticker ( R_CUSIP
                               , C_ID_CUSIP
                               , C_NO_SOURCE
                               , NULL
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    Pgmloc := 5430;
                    Set_Ticker ( R_CUSIP
                               , C_ID_CINS
                               , C_NO_SOURCE
                               , NULL
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                END IF;
                IF R_CINS_Symbol IS NOT NULL
                THEN
                    Pgmloc := 5440;
                    Set_Ticker ( R_CINS_Symbol
                               , C_ID_CINS
                               , C_NO_SOURCE
                               , NVL( R_CINS_CY, R_ST_Status.Price_Currency )
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    Pgmloc := 5450;
                    Set_Ticker ( R_CINS_Symbol
                               , C_ID_CUSIP
                               , C_NO_SOURCE
                               , NULL
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                END IF;
                IF R_SEDOL IS NOT NULL
                THEN
                    IF R_Sedol_ISO_Nation IS NULL
                    THEN
                        --First try with the trading source/nation
                        --Then the issuer natioanlity
                        IF R_Source IS NOT NULL
                        THEN -- Get nation from source
                            Pgmloc := 5460;
                            OPEN Get_ISO_Nation_From_Source( R_Source );
                            Pgmloc := 5470;
                            FETCH Get_Iso_Nation_From_Source
                             INTO R_Sedol_ISO_Nation;
                            Pgmloc := 5480;
                            CLOSE Get_Iso_Nation_From_Source;
                            NULL;
                        ELSIF R_ISO_Nation IS NOT NULL
                        THEN
                            R_Sedol_Iso_Nation := R_Iso_Nation;
                        ELSIF R_Issuers.Nation_Code IS NOT NULL
                        THEN
                            Pgmloc := 5490;
                            OPEN Get_Nation_Code( R_Issuers.Nation_Code );
                            Pgmloc := 5500;
                            FETCH Get_Nation_Code
                             INTO W_Nation_Code
                                , R_Sedol_Iso_Nation;
                            Pgmloc := 5510;
                            CLOSE Get_Nation_Code;
                        ELSIF R_Exch_Source_1 IS NOT NULL
                        THEN -- Get nation from source
                            Pgmloc := 5460;
                            OPEN Get_ISO_Nation_From_Source( R_exch_Source_1 );
                            Pgmloc := 5470;
                            FETCH Get_Iso_Nation_From_Source
                             INTO R_Sedol_ISO_Nation;
                            Pgmloc := 5480;
                            CLOSE Get_Iso_Nation_From_Source;
                        ELSIF R_ST_Status.Source_Code IS NOT NULL
                        THEN -- Get nation from source
                            Pgmloc := 5460;
                            OPEN Get_ISO_Nation_From_Source( R_ST_Status.Source_Code );
                            Pgmloc := 5470;
                            FETCH Get_Iso_Nation_From_Source
                             INTO R_Sedol_ISO_Nation;
                            Pgmloc := 5480;
                            CLOSE Get_Iso_Nation_From_Source;
                        END IF;
                    END IF;
                    Pgmloc := 5520;
                    Set_Ticker ( R_SEDOL
                               , C_ID_SEDOL
                               , C_NO_SOURCE
                               , NVL( R_SEDOL_Cy, R_ST_Status.Price_Currency )
                               , R_Sedol_Iso_Nation
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                END IF;
                IF R_ISIN IS NOT NULL
                THEN
                    Pgmloc := 5530;
                    Set_Ticker ( R_ISIN
                               , C_ID_ISIN
                               , C_NO_SOURCE
                               , NVL( R_ISIN_Cy, R_ST_Status.Price_Currency )
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                END IF;
                IF R_OISIN IS NOT NULL
                THEN
                    Pgmloc := 5560;
                    Set_Ticker ( R_OISIN
                               , C_ID_ISIN
                               , C_NO_SOURCE
                               , NVL( R_OISIN_CY, R_ST_Status.Price_Currency )
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                END IF;
                IF R_RIC_Symbol IS NOT NULL
                THEN
                    Pgmloc := 5570;
                    Set_Ticker ( R_RIC_Symbol
                               , C_ID_RIC
                               , NVL( R_RIC_Source, C_NO_SOURCE )
                               , NVL( R_RIC_Cy, R_ST_Status.Price_Currency )
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                END IF;
                IF W_Id_Syst IS NOT NULL
                AND R_Ticker_Symbol IS NOT NULL
                THEN
                    Pgmloc := 5580;
                    Set_Ticker ( R_Ticker_Symbol
                               , W_Id_Syst
                               , R_Ticker_Source
                               , R_Ticker_FRI_Cy
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    IF W_Id_Syst = C_ID_CUSIP
                    THEN
                      Pgmloc := 5590;
                      Set_Ticker ( R_Ticker_Symbol
                                 , C_ID_CINS
                                 , C_NO_SOURCE
                                 , NULL
                                 , NULL
                                 , W_Identifiers
                                 , P_Prc_Msgs_Rec
                                 , V_Success
                                 );
                      IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    ELSIF W_Id_Syst = C_ID_CINS
                    THEN
                      Pgmloc := 5600;
                      Set_Ticker ( R_Ticker_Symbol
                                 , C_ID_CUSIP
                                 , C_NO_SOURCE
                                 , NULL
                                 , NULL
                                 , W_Identifiers
                                 , P_Prc_Msgs_Rec
                                 , V_Success
                                 );
                      IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    ELSIF W_Id_Syst = C_ID_ISIN
                    THEN
                      Pgmloc := 5610;
                      Set_Ticker ( R_Ticker_Symbol
                                 , C_ID_ISIN
                                 , C_NO_SOURCE
                                 , NVL( R_Ticker_FRI_Cy
                                      , R_ST_Status.Price_Currency )
                                 , NULL
                                 , W_Identifiers
                                 , P_Prc_Msgs_Rec
                                 , V_Success
                                 );
                      IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    ELSIF W_Id_Syst = C_ID_OISIN
                    THEN
                      Pgmloc := 5620;
                      Set_Ticker ( R_Ticker_Symbol
                                 , C_ID_ISIN
                                 , C_NO_SOURCE
                                 , NVL( R_Ticker_FRI_Cy
                                      , R_ST_Status.Price_Currency )
                                 , NULL
                                 , W_Identifiers
                                 , P_Prc_Msgs_Rec
                                 , V_Success
                                 );
                      IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    END IF;
                END IF;
                IF R_Exch_Source_1 IS NOT NULL
                AND R_Exch_Symbol IS NOT NULL
                THEN
                    Pgmloc := 5630;
                    Set_Ticker ( R_Exch_Symbol
                               , C_ID_EXCH
                               , R_Exch_Source_1
                               , R_FRI_Cy
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    Pgmloc := 5640;
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    Pgmloc := 5650;
                END IF;
                IF R_Exch_Source_2 IS NOT NULL
                AND R_Exch_Symbol IS NOT NULL
                THEN
                    Pgmloc := 5660;
                    Set_Ticker ( R_Exch_Symbol
                               , C_ID_EXCH
                               , R_Exch_Source_2
                               , R_FRI_Cy
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    Pgmloc := 5670;
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                    Pgmloc := 5680;
                END IF;
                IF R_EDI_Symbol IS NOT NULL
                THEN
                    Pgmloc := 5690;
                    Set_Ticker ( R_EDI_Symbol
                               , C_ID_EDI
                               , C_NO_SOURCE
                               , NULL
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                END IF;
                IF R_Pool_Number IS NOT NULL
                THEN
                    Pgmloc := 5690;
                    Set_Ticker ( R_Pool_Number
                               , C_ID_Pool
                               , C_NO_SOURCE
                               , NULL
                               , NULL
                               , W_Identifiers
                               , P_Prc_Msgs_Rec
                               , V_Success
                               );
                    IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                END IF;
                pgmloc := 5700;
                Get_Tickers ( W_Identifiers
                            , G_MAY_EXISTS
                            , V_Row_Tracked
                            , V_Err_Tracked
                            , V_Record
                            , V_Rows_Read
                            , P_Log_File
                            , P_Err_File
                            , V_Fri_Ticker_Valid
                            , B_Ticker_Found
                            , B_Ticker_Same
                            , B_Pending_Changes
                            , R_Fri_Ticker
                            , P_Prc_Msgs_Rec
                            , V_Success
                            );
                pgmloc := 5710;
                IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
                pgmloc := 5720;
                IF NOT V_Fri_Ticker_Valid
                THEN
                    IF NOT V_Row_Errors
                    THEN
                        V_Rows_Err   := V_Rows_Err + 1;
                        V_Row_Errors := TRUE;
                    END IF;
                    W_Record_Status := C_Error;
                END IF;
                pgmloc := 5730;
                --
                -- If the same security than before and allowing for
                -- multiple entries, go directly to the creation of
                -- the identifiers.
                IF  G_Many_Tickers
                AND W_Cusip       = Previous_Cusip
                AND W_Long_Name_E||'>'||W_Class||'>'||W_Series
                    = Previous_Long_Name_E||'>'||Previous_Class||'>'||Previous_Series
                AND W_Issuer      = Previous_Issuer
                THEN
                    IF G_Tracing
                    THEN
                        pgmloc             := 5740;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Same values as previous'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 5750;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , '###Previous Fri_Ticker '''||Previous_fri_Ticker||''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF;
                    W_Created          := Previous_Created;
                    R_Fri_Ticker_C     := Previous_Fri_Ticker;
                    R_Fri_Ticker       := Previous_Fri_Ticker;
                    R_Fri_Id           := Previous_Fri_ID;
                    GOTO Next_Fld;
                END IF;
--
                IF G_Tracing
                THEN
                    pgmloc             := 5760;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                    , 'So far so good'
                                                    , P_Prc_Msgs_Rec
                                                    , V_Success
                                                    );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF;
--
--
-- Obtenir le security_pattern_controls
-- Mettre les defaut approprie
-- Certains champs peuvent venir de la lecture
--
                IF G_Tracing
                THEN
                    pgmloc             := 5770;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Getting Security_controls'
                                                  || ' with '''
                                                  || R_Securities.Security_Type_Code
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF;
                pgmloc := 5780;
                G_FMessage_Code := NULL;
                -- Global, will be modified by routine if error
                Verify_Security_Controls ( R_Securities
                                         , R_Sec_Controls
                                         , B_Freq_Value
                                         , B_Intangibility_Flag
                                         , Field_Name
                                         , R_Field
                                         );
                IF (G_Fmessage_Code IS NOT NULL )
                THEN
                    GOTO Log_Error;
                END IF;
                IF G_Tracing
                THEN --{
                    pgmloc             := 5790;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Got income_type '''
                                                  || R_Sec_Controls.Income_Type_Code
                                                  || '''  Default frequency '''
                                                  || R_Sec_Controls.Default_Frequency_Value
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5800;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'income_frequency_flag '''
                                                  || R_Sec_Controls.Income_Frequency_Flag
                                                  || ''' intangibility_Flag '''
                                                  || R_Sec_Controls.Intangibility_Flag
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                IF G_Tracing
                THEN --{
                    pgmloc             := 5810;
                    Load_Supplier_Rules.Write_File
                                       ( P_Log_File
                                       , 'Getting Security_Pattern_controls'
                                      || ' with '''
                                      || R_Securities.Security_Type_Code
                                      || ''' and '''
                                      || R_Securities.Income_Frequency_Value
                                      || ''''
                                       , P_Prc_Msgs_Rec
                                       , V_Success
                                       );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                pgmloc := 5820;
                G_FMessage_Code := NULL;
                -- Global, will be modified by routine if error
                Verify_Frequency ( R_Securities
                                 , R_FV
                                 , Field_Name
                                 , R_Field
                                 );
                IF (G_Fmessage_Code IS NOT NULL )
                THEN
                    GOTO Log_Error;
                END IF;
                BEGIN
                    pgmloc := 5830;
                    R_Security_Pattern_Code
                        := Security.Get_Sec_Type_Pattern
                                   ( R_Securities.Security_Type_Code
                                   , R_Securities.Income_Frequency_Value
                                   );
                EXCEPTION
                WHEN OTHERS THEN
                    Field_Name := C_Income_Freq||'&'||C_Security_Type;
                    R_Field := R_Securities.Income_Frequency_Value
                            || '&'
                            || R_Securities.Security_Type_Code;
                    G_FMessage_Code := G_Message_Code || '050';
                END;
                IF (G_Fmessage_Code IS NOT NULL )
                THEN
                    GOTO Log_Error;
                END IF;
                pgmloc := 5840;
                G_FMessage_Code := NULL;
                -- Global, will be modified by routine if error
                Verify_Security_Ptrn_Controls ( R_Security_Pattern_Code
                                              , R_Securities
                                              , R_SPC
                                              , R_ST_Status
                                              , R_Mat_Sched
                                              , B_Par_Value
                                              , B_Call_Flag
                                              , B_Conv_Flag
                                              , B_E_R_Flag
                                              , B_Perp_Flag
                                              , R_Fri_Cy
                                              , Field_Name
                                              , R_Field
                                              );
                IF (G_Fmessage_Code IS NOT NULL )
                THEN
                    GOTO Log_Error;
                END IF;
                IF G_Tracing
                THEN --{
                    pgmloc             := 5850;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Got Par_Value_Flag '''
                                                  || R_SPC.Par_Value_Flag
                                                  || '''   Default_Par_Value '''
                                                  || R_SPC.Default_Par_Value
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5860;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' Par_Value_Test_Amount '''
                                                  || R_SPC.Par_Value_Test_Amount
                                                  || '''   Par_Value_Operator '''
                                                  || R_SPC.Par_Value_Operator
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5870;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' CAN_PAR_VALUE_BE_CHANGED '''
                                                  || R_SPC.CAN_PAR_VALUE_BE_CHANGED
                                                  || '''   ABS_ADJUST_PRICES_FLAG '''
                                                  || R_SPC.ABS_ADJUST_PRICES_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5880;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' BLOCK_TRADES_FLAG '''
                                                  || R_SPC.BLOCK_TRADES_FLAG
                                                  || '''   CALL_SCHEDULE_FLAG '''
                                                  || R_SPC.CALL_SCHEDULE_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5890;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' CONVERSION_SCHEDULE_FLAG '''
                                                  || R_SPC.CONVERSION_SCHEDULE_FLAG
                                                  || '''   CURRENCY_FLAG '''
                                                  || R_SPC.CURRENCY_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5900;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' EXT_RET_FLAG '''
                                                  || R_SPC.EXT_RET_FLAG
                                                  || '''   GROUPING_FLAG '''
                                                  || R_SPC.GROUPING_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5910;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' HOLDER_VOTES_FLAG '''
                                                  || R_SPC.HOLDER_VOTES_FLAG
                                                  || '''   MATURITY_SCHEDULE_FLAG '''
                                                  || R_SPC.MATURITY_SCHEDULE_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5920;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' MAT_DATE_SEL_METHOD_FLAG '''
                                                  || R_SPC.MAT_DATE_SEL_METHOD_FLAG
                                                  || '''   PERPETUAL_FLAG '''
                                                  || R_SPC.PERPETUAL_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5930;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' PURCHASE_FUND_FLAG '''
                                                  || R_SPC.PURCHASE_FUND_FLAG
                                                  || '''   SINKING_FUND_FLAG '''
                                                  || R_SPC.SINKING_FUND_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5940;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' SPLIT_FLAG '''
                                                  || R_SPC.SPLIT_FLAG
                                                  || '''   MULTIPLE_ISSUE_FLAG '''
                                                  || R_SPC.MULTIPLE_ISSUE_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5950;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' PRICING_FLAG '''
                                                  || R_SPC.PRICING_FLAG
                                                  || '''   PRC_CALC_SCH_FLAG '''
                                                  || R_SPC.PRC_CALC_SCH_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 5960;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , ' TRADING_STATUS_FLAG '''
                                                  || R_SPC.TRADING_STATUS_FLAG
                                                  || '''   QTY_OUTS_CY_FLAG '''
                                                  || R_SPC.QTY_OUTS_CY_FLAG
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                IF G_May_Exists
                AND R_Fri_Ticker IS NOT NULL
                THEN
                    Validate_Security_Type( R_Fri_Ticker
                                          , R_Sec_Controls
                                          , R_SPC
                                          , V_Message
                                          , V_Success
                                          );
                -- If not success - write messages to log etc.
                    IF NOT V_Success THEN GOTO Log_Error_got_Message; END IF;
                END IF;
                --
                -- Validate the Maturity Date
                --
                IF R_Securities.Income_Type_Code = 'I'
                THEN --{
                    Pgmloc := 5970;
                    IF  R_Securities.Perpetual_Flag     = 'Y'
                    AND R_Mat_Sched.Maturity_Date IS NOT NULL
                    THEN
                        G_FMessage_Code := G_Message_Code || '160';
                       GOTO Log_Error;
                    ELSIF R_Securities.Perpetual_Flag = 'N'
                    AND   R_Mat_Sched.Maturity_Date IS NULL
                    THEN
                        G_FMessage_Code := G_Message_Code || '170';
                        GOTO Log_Error;
                    END IF;
                END IF; --}
                --
                pgmloc := 5980;
                IF  R_Issues.Offering_Date IS NULL
                AND R_Issues.Private_Placement_Flag IS NOT NULL
                THEN
                    G_FMessage_Code := G_Message_Code || '140';
                    Field_Name := C_Issue_Offer_Date;
                    E_Value_01 := C_Priv_Plac_Flag;
                    GOTO Log_Error;
                ELSIF R_Issues.Offering_Date IS NOT NULL
                  AND R_Issues.Private_Placement_Flag IS NULL
                THEN
                    G_FMessage_Code := G_Message_Code || '140';
                    Field_Name := C_Issue_Offer_Date;
                    E_Value_01 := C_Priv_Plac_Flag;
                    GOTO Log_Error;
                END IF;
                --
                pgmloc := 5990;
                IF  R_PC_Sched.Data_Supplier_Code IS NULL
                AND R_Data_Deliv_Time IS NOT NULL
                THEN
                    G_FMessage_Code := G_Message_Code || '140';
                    Field_Name := C_Data_Supp_Code;
                    E_Value_01 := C_Data_Deliv_Time;
                    GOTO Log_Error;
                ELSIF R_PC_Sched.Data_Supplier_Code IS NOT NULL
                  AND R_Data_Deliv_Time IS NULL
                THEN
                    G_FMessage_Code := G_Message_Code || '140';
                    Field_Name := C_Data_Supp_Code;
                    E_Value_01 := C_Data_Deliv_Time;
                    GOTO Log_Error;
                END IF;
                --
                pgmloc := 6000;
                IF  R_II_Rates.First_Payment_Date IS NULL
                AND R_II_Rates.Accruals_Start_Date IS NOT NULL
                THEN
                    G_FMessage_Code := G_Message_Code || '140';
                    Field_Name := C_First_Pay_Date;
                    E_Value_01 := C_Accrual_Start_Date;
                    GOTO Log_Error;
                ELSIF R_II_Rates.First_Payment_Date IS NOT NULL
                  AND R_II_Rates.Accruals_Start_Date IS NULL
                THEN
                    G_FMessage_Code := G_Message_Code || '140';
                    Field_Name := C_First_Pay_Date;
                    E_Value_01 := C_Accrual_Start_Date;
                    GOTO Log_Error;
                END IF;
                --
                IF  R_PC_Sched.Data_Supplier_Code  IS NOT NULL
                AND R_Data_Deliv_Time IS NOT NULL
                THEN --{
                    pgmloc := 6010;
                    OPEN  Get_Supplier_Delivery ( R_PC_Sched.Data_Supplier_Code
                                                , R_Data_Deliv_Time
                                                );
                    pgmloc := 6020;
                    FETCH Get_Supplier_Delivery
                     INTO R_PC_Sched.Delivery_Time;
                    IF Get_Supplier_Delivery%NotFound
                    THEN
                        pgmloc := 6030;
                        CLOSE Get_Supplier_Delivery;
                        G_FMessage_Code := G_Message_Code || '150';
                        Field_Name := R_PC_Sched.Data_Supplier_Code;
                        E_Value_01 := R_Data_Deliv_Time;
                        GOTO Log_Error;
                    END IF;
                    pgmloc := 6040;
                    CLOSE Get_Supplier_Delivery;
                END IF; --}
                --
                IF R_Securities.Income_Type_Code = 'I'
                AND R_Securities.Perpetual_Flag != 'Y'
                THEN --{
                    IF R_Mat_Sched.Redemption_Currency
                       = R_Securities.Par_Currency
                    THEN --{
                        NULL;
                    ELSE --}{
                        IF B_Par_Currency
                        THEN
                            IF B_Redemption_Currency
                            THEN
                                G_FMessage_Code := G_Message_Code || '140';
                                Field_Name := C_Par_Currency
                                           || ' & '
                                           || C_Redemption_Currency;
                                E_Value_01 := R_Securities.Par_Currency;
                                E_Value_02 := R_Mat_Sched.Redemption_Currency;
                                GOTO Log_Error;
                            ELSE
                                R_Mat_Sched.Redemption_Currency
                                := R_Securities.Par_Currency;
                            END IF;
                        ELSE
                            IF B_Redemption_Currency
                            THEN
                                R_Securities.Par_Currency
                                := R_Mat_Sched.Redemption_Currency;
                            END IF;
                        END IF;
                    END IF; --}
                END IF; --}
                --
                IF R_Securities.Par_Value IS NOT NULL
                THEN --{
                    IF NOT B_Par_Currency
                    THEN
                        IF R_Securities.Par_Currency IS NULL
                        THEN
                            R_Securities.Par_Currency := NVL( R_Fri_Cy
                                               , NVL( G_Par_Fri_Cy
                                                    , R_ST_Status.Price_Currency
                                                    )
                                               );
                        END IF;
                    END IF;
                    IF R_Securities.Par_Currency IS NULL
                    THEN
                        G_FMessage_Code := G_Message_Code || '140';
                        Field_Name := C_Par_Currency||' <'||R_Securities.Par_Currency||'>';
                        E_Value_01 := C_Par_Value||' <'||R_Securities.Par_Value||'>';
                        GOTO Log_Error;
                    END IF;
                ELSE
                    IF R_Securities.Par_Currency IS NOT NULL
                    THEN
                        G_FMessage_Code := G_Message_Code || '140';
                        Field_Name := C_Par_Value;
                        E_Value_01 := C_Par_Currency;
                        GOTO Log_Error;
                    END IF;
                END IF; --}
                --
                IF R_RIC_Symbol IS NOT NULL
                THEN --{
                    IF R_RIC_Cy IS NULL
                    THEN
                        R_RIC_Cy := R_ST_Status.Price_Currency;
                    END IF;
                    IF R_Ric_Cy IS NULL
                    THEN
                        G_FMessage_Code := G_Message_Code || '140';
                        Field_Name := C_RIC_Symbol;
                        E_Value_01 := C_RIC_Currency;
                        GOTO Log_Error;
                    END IF;
                    IF R_RIC_Source IS NULL
                    THEN
                        R_RIC_Source := R_ST_Status.Source_Code;
                    END IF;
                    IF R_Ric_Source IS NULL
                    THEN
                        G_FMessage_Code := G_Message_Code || '140';
                        Field_Name := C_RIC_Symbol;
                        E_Value_01 := C_RIC_Source;
                        GOTO Log_Error;
                    END IF;
                END IF; --}
                --
                -- Validate the Issuer and its name
                --
                V_Create_Issuer := FALSE;
                IF P_Create_Issuer
                AND ( R_Issuer = 'JUNK'
                   OR R_Issuer = 'TEMP'
                   OR R_Issuer IS NULL
                    )
                THEN --{
                    W_Issuer      := NULL;
                    W_Fri_Issuer  := NULL;
                    W_Nation_Code := NULL;
                    IF R_Fri_Ticker IS NOT NULL
                    THEN
                        pgmloc := 6050;
                        OPEN Get_Issuer_by_Ticker ( R_Fri_Ticker );
                        pgmloc := 6060;
                        FETCH Get_Issuer_by_Ticker
                         INTO W_Issuer
                            , W_Fri_Issuer
                            , W_Nation_Code;
                        pgmloc := 6070;
                        CLOSE Get_Issuer_By_Ticker;
                    ELSE
                        IF R_Issuer_Name IS NULL
                        THEN --{
                            G_FMessage_Code := G_Message_Code || '180';
                            Field_Name      := C_Issuer_Name;
                            E_Value_01      := C_Issuer_Code;
                            GOTO Log_Error;
                        ELSE --}{
                            Pgmloc := 6080;
                            OPEN Get_Issuer_By_Name ( R_Issuer_Name );
                            pgmloc := 6090;
                            FETCH Get_Issuer_By_Name
                             INTO W_Issuer
                                , W_Fri_Issuer
                                , W_Nation_Code;
                            pgmloc := 6100;
                            CLOSE Get_Issuer_By_Name;
                        END IF;
                    END IF;
                    IF W_FRI_Issuer IS NOT NULL
                    THEN
                        R_Issuer     := W_Issuer;
                        R_Fri_Issuer := W_Fri_Issuer;
                        R_Issuers.Fri_Issuer    := W_Fri_Issuer;
                        R_Issuers.Code          := W_Issuer;
                        R_Securities.Fri_Issuer := W_Fri_Issuer;
                        IF W_Nation_Code != R_Issuers.Nation_Code
                        THEN
                            Pgmloc := 6110;
                            G_FMessage_Code := G_Message_Code || '052';
                            Field_Name      := C_Issuer_Nation;
                            E_Value_01      := SUBSTR( R_Issuer
                                                    || ' => '
                                                    || W_Nation_Code
                                                    || ' != '
                                                    || R_Issuers.Nation_Code
                                                     , 1
                                                     , C_Error_Len
                                                     );
                            V_Warning := TRUE;
                            GOTO Log_Error;
                        END IF;
                    ELSE
                        V_Create_Issuer := TRUE;
                    END IF; --}
                END IF; --}
                --
                IF B_Isid
                THEN --{
                    pgmloc := 6120;
                    OPEN  Get_Issuers_Xref_ISID ( W_Isid, W_Ids_Code );
                    pgmloc := 6130;
                    FETCH Get_Issuers_Xref_ISID
                     INTO R_Issuers_Xref;
                    pgmloc := 6140;
                    CLOSE Get_Issuers_Xref_ISID;
                    IF R_Issuers_Xref.Fri_issuer IS NOT NULL
                    THEN --{
                        IF R_Fri_Issuer is NULL
                        THEN --{
                            R_Fri_Issuer    := R_Issuers_Xref.Fri_Issuer;
                            V_Create_Issuer := FALSE;
                        ELSIF R_Fri_Issuer <> R_Issuers_Xref.Fri_Issuer
                        THEN --}{
                            Pgmloc := 6150;
                            G_FMessage_Code := G_Message_Code || '052';
                            Field_Name      := C_Isid;
                            E_Value_01      := SUBSTR( W_Ids_Code
                                                    || ' '
                                                    || W_Isid
                                                    || ' => '
                                                    || R_Issuers_Xref.Fri_Issuer
                                                    || ' != '
                                                    || R_Fri_Issuer
                                                     , 1
                                                     , C_Error_Len
                                                     );
                            if V_DNBO_issuer_name then
                              R_Fri_Issuer    := R_Issuers_Xref.Fri_Issuer;
                              V_Create_Issuer := FALSE;
                              V_Warning := true;
                            end if;
                            GOTO Log_Error;
                        ELSE --}{
                            V_Create_Issuer := FALSE;
                        END IF; --}
                    END IF; --}
                END IF; --} B_Isid
                --
                IF NOT V_ID_Errors
                AND NOT G_MAY_EXISTS
                THEN --{
                    IF G_Tracing
                    THEN
                        pgmloc             := 6160;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Checking unique security'
                                                       || R_Fri_Ticker_T
                                                       || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6170;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Fri_Issuer : '''
                                                       || R_Fri_Issuer
                                                       || '''   Sec_type : '''
                                                       || R_Securities.Security_Type_Code
                                                       || '''   Sec_Sub_Type : '''
                                                       || R_Securities.Security_Sub_Type_Code
                                                       || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6180;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Long_Name : '''
                                                       || R_Securities.Long_Name_E
                                                       || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6190;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Coupon : '''
                                                       || R_Securities.Indicated_Annual_Rate
                                                       || '''   Freq value : '''
                                                       || R_Securities.Income_Frequency_Value
                                                       || '''   Fri_Cy : '''
                                                       || R_Fri_Cy
                                                       || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6200;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Pricing Note : '''
                                                       || R_Prc_Notes.Pricing_Note
                                                       || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6210;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Maturity Date : '''
                                                       || R_Mat_Sched.Maturity_Date
                                                       || '''   maturity Type : '''
                                                       || R_Mat_Sched.Maturity_Type_Code
                                                       || '''   Perp : '''
                                                       || R_Securities.Perpetual_Flag
                                                       || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF;
                    --Checking for Already existing Security
                    pgmloc := 6220;
                    OPEN  Check_Unique_Security
                        ( R_Fri_Issuer
                        , R_Securities.Security_Type_Code
                        , R_Securities.Security_Sub_Type_Code
                        , R_Securities.Long_Name_E
                        , R_Securities.Indicated_Annual_Rate
                        , R_Securities.Income_Frequency_Value
                        , R_Fri_Cy
                        , R_Prc_Notes.Pricing_Note
                        , R_Mat_Sched.Maturity_Date
                        , R_Mat_Sched.Maturity_Type_Code
                        , R_Securities.Perpetual_Flag
                        );
                    pgmloc := 6230;
                    FETCH Check_Unique_Security
                     INTO V_Sec_Exists;
                    --
                    IF Check_Unique_Security%Found
                    AND NOT B_Create_Even
                    THEN
                        G_FMessage_Code := G_Message_Code || '130';
                        Field_Name := 'FRI_ID : '||V_Sec_Exists;
                        pgmloc := 6240;
                        CLOSE Check_Unique_Security;
                        GOTO Log_Error;
                    END IF;
                    pgmloc := 6250;
                    CLOSE Check_Unique_Security;
                END IF; --} IF NOT V_ID_Errors
            END IF; --} IF X = V_Num_Flds
            --
            Pgmloc := 6260;
            --
            GOTO Next_Fld;
            --
<< Log_Error >>
            -- Switching the position of conditions
            -- to accomodate OASIS_Messages calls with Parameters
            -- Adding label Log_Error_Got_Message for them
            Pgmloc := 6270;
            Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                              , G_Del_Flag
                                              , Field_Name
                                              , E_Value_01
                                              , E_Value_02
                                              , V_Message
                                              );
            --
<< Log_Error_Got_Message>>
            IF NOT V_Row_Tracked
            THEN --{
                V_Row_Tracked := TRUE;
                pgmloc             := 6280;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , NULL
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                pgmloc             := 6290;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Processing Row # '
                                              || TO_CHAR(V_Rows_read)
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF; --}
            IF NOT V_Err_Tracked
            THEN --{
                V_Err_Tracked := TRUE;
                Pgmloc             := 6300;
                Load_Supplier_Rules.Write_File ( P_Err_File
                                               , 'Processing Row # '
                                              || TO_CHAR(V_Rows_read)
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                Pgmloc             := 6310;
                Load_Supplier_Rules.Write_File ( P_Err_File
                                               , V_Record
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF; --}
            --
            IF NOT V_Warning
            THEN
                IF NOT V_Row_Errors
                THEN
                    V_Rows_Err   := V_Rows_Err + 1;
                    V_Row_Errors := TRUE;
                END IF;
                W_Record_Status := C_Error;
            else
              v_message:= 'WARN: '||v_message;
            END IF;
            V_Warning := FALSE;
            --
            V_Message := C_Indent || V_Message;
            Pgmloc := 6320;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , V_Message
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            Pgmloc := 6330;
            Load_Supplier_Rules.Write_File ( P_Err_File
                                           , V_Message
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            --
            IF B_Delayed_Message
            THEN
              B_Delayed_Message := FALSE;
              V_Message := C_Indent || Delayed_Message;
              Pgmloc := 6340;
              Load_Supplier_Rules.Write_File ( P_Log_File
                                             , V_Message
                                             , P_Prc_Msgs_Rec
                                             , V_Success
                                             );
              IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
              Pgmloc := 6350;
              Load_Supplier_Rules.Write_File ( P_Err_File
                                             , V_Message
                                             , P_Prc_Msgs_Rec
                                             , V_Success
                                             );
              IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            V_Message := '';
            --
<< Next_Fld >>
            Null;
        END LOOP; --} FOR X IN 1..V_Num_Flds
        --
        -- Is it a New Security?
        --
        pgmloc := 6360;
        V_New_Sec      := FALSE;
        V_New_Sec_Text := '';
        --
        IF  R_Fri_Ticker IS NULL
        THEN
            IF G_Tracing
            THEN
                pgmloc             := 6370;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Got NO fri ticker : A new security'
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            V_New_Sec := TRUE;
        END IF;
        --
        IF V_Track_Progress
        THEN --{
            IF NOT V_Row_Tracked
            THEN --{
                V_Row_Tracked := TRUE;
                pgmloc             := 6380;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , NULL
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                pgmloc             := 6390;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Processing Row # '
                                              || TO_CHAR(V_Rows_read)
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF; --}
            IF R_Cusip IS NOT NULL
            OR R_CINS_Symbol IS NOT NULL
            OR R_ISIN  IS NOT NULL
            OR R_OISIN IS NOT NULL
            OR R_RIC_Symbol IS NOT NULL
            OR R_Sedol IS NOT NULL
            THEN --{
                Pgmloc := 6400;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , C_Indent
                                              || 'Cusip: '
                                              || NVL(R_Cusip,'NO CUSIP')
                                              || ' CINS: '
                                              || NVL(R_CINS_SYMBOL,'NO CINS')
                                              || ' ISIN: '
                                              || NVL(R_Isin, 'NO ISIN')
                                              || ' OISIN: '
                                              || NVL(R_OIsin,'NO ISIN')
                                              || ' RIC: '
                                              || NVL(R_RIC_Symbol,'NO RIC')
                                              || ' Sedol: '
                                              || NVL(R_Sedol,'NO SEDOL')
                                              || V_New_Sec_Text
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF; --}
            Pgmloc := 6410;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , C_Indent
                                          || 'Issuer: '
                                          || R_Issuer
                                          || ' Coupon: '
                                          || TO_CHAR(R_Securities.Indicated_Annual_Rate,'999.999')
                                          || ' Maturity: '
                                          || TO_CHAR(R_Mat_Sched.Maturity_Date,'DDMonYYYY')
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc := 6420;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , C_Indent
                                          || 'Market: '
                                          || R_Source
                                          || ' ISO CY: '
                                          || R_ISO_CY
                                          || ' Long Name: '
                                          || R_Securities.Long_Name_E
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF; --}
        --
        IF V_Row_Errors
        THEN --{
            IF G_Tracing
            THEN
                pgmloc             := 6430;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Got error : Next row'
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            GOTO Next_Row;
        END IF; --} IF V_Row_Errors
<< Process_the_Row >>
        -- In order to Create the Foreign Securities,
        -- The Input Process Parm must be equal to CreateFgn
        IF NOT P_Create_SEC
        THEN
            GOTO Next_Row;
        END IF;
        --
        -- Create the Fields by Default
        --
        pgmloc := 6440;
        R_Securities.Maturity_Schedule_Flag := 'N';
        IF  B_Redemption_Amount
        AND R_Mat_Sched.Redemption_Amount IS NULL
        THEN
            R_Mat_Sched.Redemption_Amount := NVL( R_Securities.Par_Value
                                                , G_Redemption_Amount );
        ELSIF NOT B_Redemption_Amount
        THEN
            R_Mat_Sched.Redemption_Amount := NVL( R_Securities.Par_Value
                                                , G_Redemption_Amount );
        END IF;
        IF  B_Redemption_Currency
        AND R_Mat_Sched.Redemption_Currency IS NULL
        THEN
            R_Mat_Sched.Redemption_Currency := NVL( R_Securities.Par_Currency
                                                  , G_Redemption_Currency );
        ELSIF NOT B_Redemption_Currency
        THEN
            R_Mat_Sched.Redemption_Currency := NVL( R_Securities.Par_Currency
                                                  , G_Redemption_Currency );
        END IF;
        IF BR_Mat_Sched
        THEN
            IF R_Mat_Sched.Maturity_Date IS NOT NULL
            OR R_Securities.Ext_Ret_Flag      = 'Y'
            OR R_Mat_Sched.Redemption_Amount IS NOT NULL
            THEN
                R_Securities.Maturity_Schedule_Flag := 'Y';
            END IF;
        END IF;
        --
        -- Validate the Issuer and its name
        --
        IF  P_Create_Issuer
        AND V_Create_Issuer
        THEN --{
            IF G_Tracing
            THEN
                pgmloc             := 6450;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Creating issuer'
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            -- If we need to create the issuer, we also need to create
            -- the security.
            V_New_Sec := TRUE;
            -- Need to create!
            -- Get the code!
            pgmloc := 6460;
            OPEN  Get_Max_Issuer_Code ( G_Issuer_Generic );
            pgmloc := 6470;
            FETCH Get_Max_Issuer_Code
             INTO W_Max_Issuer_Count;
            pgmloc := 6480;
            CLOSE Get_Max_Issuer_Code;
            pgmloc := 6490;
            R_Issuer := G_Issuer_Generic
                     || TRIM( TO_CHAR( NVL( TO_NUMBER( W_Max_Issuer_Count )
                                          , 0
                                          ) + 1
                                     , G_Reg_Format
                                     ) );
            pgmloc := 6500;
            SELECT Fri_Issuer_Long_Seq.NextVal
              INTO R_Fri_Issuer
              FROM DUAL;
            R_Issuers.Fri_Issuer    := R_Fri_Issuer;
            R_Issuers.Code          := R_Issuer;
            R_Securities.Fri_Issuer := R_Fri_Issuer;
            IF G_Tracing
            THEN
                pgmloc             := 6510;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Issuer : '''
                                               || R_Issuers.Code
                                               || '''   Fri_Issuer : '''
                                               || R_Issuers.Fri_Issuer
                                               || '''   Type : '''
                                               || R_Issuers.Issuer_Type_Code
                                               || '''   Sub type : '''
                                               || R_Issuers.Issuer_Sub_Type_Code
                                               || ''''
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                pgmloc             := 6520;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Issuer Name : '''
                                               || R_Issuers.Long_name_E
                                               || '''   nation : '''
                                               || R_Issuers.Nation_Code
                                               || ''''
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
            BEGIN --{
                pgmloc := 6530;
                INSERT
                  INTO ISSUERS
                     ( CODE             , FRI_ISSUER
                     , ISSUER_TYPE_CODE , ISSUER_SUB_TYPE_CODE
                     , PUBLISHING_NAME_E
                     , PUBLISHING_NAME_F
                     , TEXT_E
                     , TEXT_F
                     , LONG_NAME_E
                     , LONG_NAME_F
                     , LONG_NAME_2_E
                     , LONG_NAME_2_F
                     , NATION_CODE      , ACTIVE_FLAG
                     , DATE_OF_STATUS   , JOBBER_CODE_FOR_STATUS
                     , FRI_ADDRESS      , FISCAL_YEAR_END
                     , LAST_ANNUAL_REPORT_ON_FILE
                     , REMARKS
                     , LAST_USER        , LAST_STAMP
                     , NEW_ISSUER_CODE
                     )
                VALUES
                     ( R_Issuer
                     , R_Fri_Issuer
                     , R_Issuers.Issuer_Type_Code
                     , R_Issuers.Issuer_Sub_Type_Code
                     , R_Issuers.Publishing_Name_E
                     , R_Issuers.Publishing_Name_F
                     , R_Issuers.Text_E
                     , R_Issuers.Text_F
                     , R_Issuers.Long_Name_E
                     , R_Issuers.Long_Name_F
                     , R_Issuers.Long_Name_2_E
                     , R_Issuers.Long_Name_2_F
                     , R_Issuers.Nation_Code          , 'Y'
                     , TRUNC( SYSDATE )               , G_User_Id
                     , NULL                           , NULL
                     , NULL
                     , 'AUTOMATIC CREATION'
                     , G_User_Id                      , SYSDATE
                     , NULL
                     );
            EXCEPTION
                WHEN OTHERS
                THEN --{
                    V_Error_Code := TRIM( TO_CHAR( SQLCODE ) );
                    V_Message := SUBSTR( SQLERRM( SQLCODE ) ,1 ,255 );
                    IF ( SQLCODE BETWEEN -20999 AND -20000 )
                    THEN
                        V_Error_Message := SUBSTR( V_Message, 12 );
                    ELSE
                        Manage_Messages.Process_Error( V_Error_Code
                                                     , V_Message
                                                     , V_Error_Column
                                                     , V_Error_Message
                                                     );
                    END IF;
                    IF V_Error_Message IS NULL
                    THEN
                        V_Error_Message := SUBSTR( SQLERRM( SQLCODE ) ,1 ,255 );
                    END IF;
                    IF ( INSTR( V_Error_Message, 'ORA-06512', 1, 1 ) <> 0 )
                    THEN
                        V_Error_Message := SUBSTR( V_Error_Message
                                                 , 1
                                                 , INSTR( V_Error_Message
                                                        , 'ORA-06512', 1, 1 )
                                                   - 2
                                                 );
                    END IF;
                    --
                    ROLLBACK;
                    V_Rows_Err := V_Rows_Err + 1;
                    W_Record_Status := C_Error;
                    V_Secs_Ins := P_Prc_Msgs_Rec.Value_04 ;
                    V_Secs_Upd := P_Prc_Msgs_Rec.Value_05 ;
                    V_Tick_Ins := P_Prc_Msgs_Rec.Value_06 ;
                    V_Trd_Ins  := P_Prc_Msgs_Rec.Value_07 ;
                    V_Mat_Ins  := P_Prc_Msgs_Rec.Value_08 ;
                    V_PrcN_Ins := P_Prc_Msgs_Rec.Value_09 ;
                    IF NOT V_Row_Tracked
                    THEN --{
                        V_Row_Tracked := TRUE;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , NULL
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Processing Row # '
                                                      || TO_CHAR(V_Rows_read)
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF; --}
                    IF NOT V_Err_Tracked
                    THEN --{
                        V_Err_Tracked := TRUE;
                        Load_Supplier_Rules.Write_File ( P_Err_File
                                                       , 'Processing Row # '
                                                      || TO_CHAR(V_Rows_read)
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        Load_Supplier_Rules.Write_File ( P_Err_File
                                                       , V_Record
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF; --}
                    --
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , Pgmloc||C_Indent || V_Error_Message
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    --
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , R_Fri_Issuer||C_indent||R_Issuer
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    Load_Supplier_Rules.Write_File ( P_Err_File
                                                   , Pgmloc||C_Indent || V_Error_Message
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    --
                    Load_Supplier_Rules.Write_File ( P_Err_File
                                                   , R_Fri_Issuer||C_indent||R_Issuer
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    V_Row_Errors := TRUE;
                 --} WHEN OTHERS THEN
            END; --}
        END IF; --}
        IF V_Row_Errors
        THEN
            GOTO Next_Row;
        END IF;
        --
        IF  B_Isid
        AND R_Issuers_Xref.Fri_Issuer IS NULL
        THEN --{
            R_Issuers_Xref.Fri_Issuer    := R_Fri_Issuer;
            BEGIN --{
                pgmloc := 6590;
                INSERT
                  INTO ISSUERS_XREF
                     ( FRI_ISSUER
                     , ISSUER_IDS_CODE
                     , ISSUER_IDENTIFIER
                     , LAST_USER
                     , LAST_STAMP
                     )
                VALUES
                     ( R_Fri_Issuer
                     , W_Ids_Code
                     , W_Isid
                     , G_User_Id
                     , SYSDATE
                     );
            EXCEPTION
                WHEN DUP_VAL_ON_INDEX
                THEN
                    NULL;
                WHEN OTHERS
                THEN --{
                    V_Error_Code := TRIM( TO_CHAR( SQLCODE ) );
                    V_Message := SUBSTR( SQLERRM( SQLCODE ) ,1 ,255 );
                    IF ( SQLCODE BETWEEN -20999 AND -20000 )
                    THEN
                        V_Error_Message := SUBSTR( V_Message, 12 );
                    ELSE
                        Manage_Messages.Process_Error( V_Error_Code
                                                     , V_Message
                                                     , V_Error_Column
                                                     , V_Error_Message
                                                     );
                    END IF;
                    IF V_Error_Message IS NULL
                    THEN
                        V_Error_Message := SUBSTR( SQLERRM( SQLCODE ) ,1 ,255 );
                    END IF;
                    IF ( INSTR( V_Error_Message, 'ORA-06512', 1, 1 ) <> 0 )
                    THEN
                        V_Error_Message := SUBSTR( V_Error_Message
                                                 , 1
                                                 , INSTR( V_Error_Message
                                                        , 'ORA-06512', 1, 1 )
                                                   - 2
                                                 );
                    END IF;
                    --
                    ROLLBACK;
                    V_Rows_Err := V_Rows_Err + 1;
                    W_Record_Status := C_Error;
                    V_Secs_Ins := P_Prc_Msgs_Rec.Value_04 ;
                    V_Secs_Upd := P_Prc_Msgs_Rec.Value_05 ;
                    V_Tick_Ins := P_Prc_Msgs_Rec.Value_06 ;
                    V_Trd_Ins  := P_Prc_Msgs_Rec.Value_07 ;
                    V_Mat_Ins  := P_Prc_Msgs_Rec.Value_08 ;
                    V_PrcN_Ins := P_Prc_Msgs_Rec.Value_09 ;
                    IF NOT V_Row_Tracked
                    THEN --{
                        V_Row_Tracked := TRUE;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , NULL
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Processing Row # '
                                                      || TO_CHAR(V_Rows_read)
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF; --}
                    IF NOT V_Err_Tracked
                    THEN --{
                        V_Err_Tracked := TRUE;
                        Load_Supplier_Rules.Write_File ( P_Err_File
                                                       , 'Processing Row # '
                                                      || TO_CHAR(V_Rows_read)
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        Load_Supplier_Rules.Write_File ( P_Err_File
                                                       , V_Record
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF; --}
                    --
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , Pgmloc||C_Indent || V_Error_Message
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    --
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , R_Fri_Issuer
                                                  || C_indent
                                                  || R_Issuer
                                                  || C_Indent
                                                  || W_Ids_Code
                                                  || C_Indent
                                                  || W_Isid
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    --
                    Load_Supplier_Rules.Write_File ( P_Err_File
                                                   , Pgmloc||C_Indent || V_Error_Message
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    --
                    Load_Supplier_Rules.Write_File ( P_Err_File
                                                   , R_Fri_Issuer
                                                  || C_indent
                                                  || R_Issuer
                                                  || C_Indent
                                                  || W_Ids_Code
                                                  || C_Indent
                                                  || W_Isid
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    V_Row_Errors := TRUE;
                 --} WHEN OTHERS THEN
            END; --}
        END IF; --}
        IF V_Row_Errors
        THEN
            GOTO Next_Row;
        END IF;
        --
        -- Set the Fri_Ticker
        --
        IF V_New_Sec
        THEN --{
            IF G_Tracing
            THEN
                pgmloc             := 6650;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Getting new fri ticker'
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
            END IF;
            pgmloc := 6660;
            SELECT Fri_Ticker_Fgn_Seq.NextVal
              INTO R_Fri_Ticker
              FROM Dual;
            R_Fri_ID  := Security.set_fri_id (R_Fri_Ticker);
            Pgmloc := 6670;
            Set_Ticker ( R_Fri_Id
                       , C_FRI_ID
                       , C_NO_SOURCE
                       , NULL
                       , NULL
                       , W_Identifiers
                       , P_Prc_Msgs_Rec
                       , V_Success
                       );
            IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
            IF R_Cusip IS NULL
-- Ne pas mettre le cusip si c'est un cins...
            OR (  LOWER(SUBSTR(R_CUSIP,1,1)) <> UPPER(SUBSTR(R_CUSIP,1,1))
                 -- starts by a letter
             AND R_CUSIP NOT LIKE 'TMP%'
               )
            THEN
                R_Cusip := 'TMP' || R_Fri_Id;
                R_Securities.Cusip := R_Cusip;
                Pgmloc := 6680;
                Set_Ticker ( R_CUSIP
                           , C_ID_CUSIP
                           , C_NO_SOURCE
                           , NULL
                           , NULL
                           , W_Identifiers
                           , P_Prc_Msgs_Rec
                           , V_Success
                           );
                IF ( NOT V_Success ) THEN GOTO GET_OUT; END IF;
            END IF;
            R_Securities.Fri_Ticker := R_Fri_Ticker;
        ELSE --}{
            R_Fri_ID     := Security.Get_FRi_ID(R_Fri_Ticker);
            IF R_Cusip   IS NULL
            THEN
-- Get cusip returns the CINS if the cusip is inhouse.
                R_Cusip    := Security.Get_Sec_Id( R_Fri_Ticker
                                                 , NULL
                                                 , NULL
                                                 , C_ID_CUSIP
                                                 );
                R_Securities.Cusip := R_Cusip;
            END IF;
        END IF; --} IF V_New_Sec
        IF V_Track_Progress
        OR G_Tracing
        THEN --{
            pgmloc := 6690;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , C_Indent
                                          || 'Fri_Ticker:'
                                          || TO_CHAR(R_Fri_Ticker)
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc := 6700;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , C_Indent
                                          || ' FRI ID: '
                                          || R_FRI_ID
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc := 6710;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , C_Indent
                                          || ' Cusip: '
                                          || R_Cusip
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF; --}
        --
        IF V_Fri_Ticker_Start IS NULL
        AND V_New_Sec
        THEN
            V_Fri_Ticker_Start := R_Fri_Ticker;
        END IF;
        --
        pgmloc := 6720;
        V_Message := '';
        --
        BEGIN --{
            -- If the same security than before and allowing for
            -- multiple entries, go directly to the creation of
            -- the identifiers.
            IF  G_Many_Tickers
            AND W_Cusip       = Previous_Cusip
            AND W_Long_Name_E||'>'||W_Class||'>'||W_Series
                = Previous_Long_Name_E||'>'||Previous_Class||'>'||Previous_Series
            AND W_Issuer      = Previous_Issuer
            THEN --{
                R_Fri_Ticker     := Previous_Fri_Ticker;
                BR_IC_Parms      := FALSE;
                BR_Index_Details := FALSE;
                BR_II_Rates      := FALSE;
                BR_Issuers       := FALSE;
                BR_Issues        := FALSE;
                BR_Mat_Sched     := FALSE;
                BR_PC_Sched      := FALSE;
                BR_Prc_Notes     := FALSE;
                BR_Securities    := FALSE;
            ELSE --}{
                IF V_New_Sec
                THEN --{
                    IF R_Securities.Security_Type_Code = 'SB'
                    THEN
                      R_Const_Prc_Flag := 'Y';
                    END IF;
                    IF G_Tracing
                    THEN
                        pgmloc             := 6730;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Creating a new security'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6740;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Fri_Ticker : '''
                                                      || R_Fri_Ticker
                                                      || '''   Cusip : '''
                                                      || R_Cusip
                                                      || '''   Type : '''
                                                      || R_Securities.Security_Type_Code
                                                      || '''   Sub type : '''
                                                      || R_Securities.Security_Sub_Type_Code
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6750;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , ' Fri Issuer : '''
                                                      || R_Fri_Issuer
                                                      || '''   Trading Status : '''
                                                      || R_trd_status
                                                      || '''   SAC_Changed_Flag : '''
                                                      || R_SAC_Changed_Flag
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6760;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Site_Date : '''
                                                      || R_Site_Date
                                                      || '''   User_ID : '''
                                                      || R_User_ID
                                                      || '''   Book_Based_Flag : '''
                                                      || R_Book_Based_Flag
                                                      || '''   Const_Prc_Flag : '''
                                                      || R_Const_Prc_Flag
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6770;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Long_Name_E : '''
                                                      || R_Securities.Long_Name_E
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6780;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Series : '''
                                                      || R_Securities.Series
                                                      || '''   Class : '''
                                                      || R_Securities.Class
                                                      || '''   Income_Type : '''
                                                      || R_Securities.Income_Type_Code
                                                      || '''   Freq_Value : '''
                                                      || R_Securities.Income_Frequency_Value
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6790;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Fri_Cy : '''
                                                      || R_Fri_Cy
                                                      || '''   Coupon : '''
                                                      || R_Securities.Indicated_Annual_Rate
                                                      || '''   Par_Value : '''
                                                      || R_Securities.Par_Value
                                                      || '''   Par_Fri_Cy : '''
                                                      || R_Securities.Par_Currency
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6800;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Call_Flag : '''
                                                      || R_Securities.Call_Schedule_Flag
                                                      || '''   Conv_Flag : '''
                                                      || R_Securities.Conversion_Schedule_Flag
                                                      || '''   E_R_Flag : '''
                                                      || R_Securities.Ext_Ret_Flag
                                                      || '''   Intangibility_Flag : '''
                                                      || R_Securities.Intangibility_Flag
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6810;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Maturity_Flag : '''
                                                      || R_Securities.Maturity_Schedule_Flag
                                                      || '''   Mat_Date_Method : '''
                                                      ||  R_Securities.Mat_Date_Sel_Method_Code
                                                      || '''   Perp_Flag : '''
                                                      || R_Securities.Perpetual_Flag
                                                      || '''   Pur_Fund_Flag : '''
                                                      || R_Securities.Purchase_Fund_Flag
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                        pgmloc             := 6820;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Ratings_Flag : '''
                                                      || R_Ratings_Flag
                                                      || '''   Sink_Fund_Flag : '''
                                                      || R_Securities.Sinking_Fund_Flag
                                                      || '''   Splits_Flag : '''
                                                      || R_Securities.Split_Flag
                                                      || '''   MTN_Flag : '''
                                                      || R_MTN_Flag
                                                      || ''''
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF;
-- Ne pas mettre le cusip si c'est un cins...
                    IF LOWER(SUBSTR(R_CUSIP,1,1)) <> UPPER(SUBSTR(R_CUSIP,1,1))
                        -- starts by a letter
                    AND R_CUSIP NOT LIKE 'TMP%'
                    THEN
                      R_CUSIP := 'TMP'
                              || Security.set_fri_id (R_Fri_Ticker);
                    END IF;
                    pgmloc := 6830;
                    INSERT
                      INTO SECURITIES
                         ( FRI_TICKER,               CUSIP
                         , SECURITY_TYPE_CODE,       SECURITY_SUB_TYPE_CODE
                         , FRI_ISSUER,               TRADING_STATUS_CODE
                         , SAC_CHANGED_BY_USER
                         , DATE_OF_STATUS,           JOBBER_CODE_FOR_STATUS
                         , BOOK_BASED_ELIGIBLE_FLAG, CONSTANT_PRICES_FLAG
                         , LONG_NAME_E
                         , LONG_NAME_F
                         , LONG_NAME_2_E
                         , LONG_NAME_2_F
                         , SERIES,                   CLASS
                         , INCOME_TYPE_CODE,         INCOME_FREQUENCY_VALUE
                         , INCOME_CURRENCY,          INDICATED_ANNUAL_RATE
                         , PAR_VALUE,                PAR_CURRENCY
                         , CALL_SCHEDULE_FLAG,       CONVERSION_SCHEDULE_FLAG
                         , EXT_RET_FLAG,             INTANGIBILITY_FLAG
                         , MATURITY_SCHEDULE_FLAG,   MAT_DATE_SEL_METHOD_CODE
                         , PERPETUAL_FLAG,           PURCHASE_FUND_FLAG
                         , RATINGS_FLAG,             SINKING_FUND_FLAG
                         , SPLIT_FLAG,               MID_TERM_NOTE_FLAG
                         , LAST_USER,                LAST_STAMP)
                    SELECT R_Fri_Ticker
                         , R_Cusip
                         , R_Securities.Security_Type_Code
                         , R_Securities.Security_Sub_Type_Code
                         , R_fri_issuer,             R_Trd_Status
                         , R_SAC_Changed_Flag
                         , R_Site_Date,              R_User_ID
                         , R_Book_Based_Flag,        R_Const_Prc_Flag
                         , NVL(R_Securities.Long_Name_E, 'NO NAME')
                         , R_Securities.Long_Name_F
                         , R_Securities.Long_Name_2_E
                         , R_Securities.Long_Name_2_F
                         , R_Securities.Series
                         , R_Securities.Class
                         , R_Securities.Income_Type_Code
                         , R_Securities.Income_Frequency_Value
                         , R_Fri_Cy
                         , R_Securities.Indicated_Annual_Rate
                         , R_Securities.Par_Value
                         , R_Securities.Par_Currency
                         , NVL(R_Securities.Call_Schedule_Flag,'N')
                         , NVL(R_Securities.Conversion_Schedule_Flag,'N')
                         , NVL(R_Securities.Ext_Ret_Flag,'N')
                         , NVL(R_Securities.Intangibility_Flag,'N')
                         , R_Securities.Maturity_Schedule_Flag
                         , R_Securities.Mat_Date_Sel_Method_Code
                         , NVL(R_Securities.Perpetual_Flag,'N')
                         , R_Securities.Purchase_Fund_Flag
                         , R_Ratings_Flag
                         , R_Securities.Sinking_Fund_Flag
                         , R_Securities.Split_Flag
                         , R_MTN_Flag
                         , R_Last_User,              sysdate
                      FROM DUAL
                     WHERE NOT EXISTS (
                           SELECT 'X'
                             FROM Securities S
                            WHERE S.Fri_Ticker = R_Fri_Ticker
                           );
                    --
                    V_Secs_Ins := V_Secs_Ins + SQL%RowCount;
                    W_Record_Status := C_Inserted;
                    Previous_Fri_Ticker := R_Fri_Ticker;
                    W_Created       := TRUE;
                ELSE --}{
                    -- An update is required
                    pgmloc := 6840;
                    --
                    -- No Update.
                    --
                    UPDATE Securities
-- -- -- Do not modify CUSIP
--                     SET CUSIP = NVL( R_CUSIP, CUSIP )
                       SET SECURITY_TYPE_CODE = NVL( R_Securities.Security_Type_Code, Security_Type_Code )
                         , SECURITY_SUB_TYPE_CODE = NVL( R_Securities.Security_Sub_Type_Code, Security_Sub_Type_Code )
                         , FRI_ISSUER = NVL( R_Fri_Issuer, FRI_ISSUER )
                         , BOOK_BASED_ELIGIBLE_FLAG = NVL( R_Book_Based_Flag, BOOK_BASED_ELIGIBLE_FLAG )
                         , CONSTANT_PRICES_FLAG = NVL( R_Const_Prc_Flag, CONSTANT_PRICES_FLAG )
                         , LONG_NAME_E = NVL( R_Securities.Long_Name_E, LONG_NAME_E )
                         , SERIES = NVL( R_Securities.Series, SERIES )
                         , CLASS = NVL( R_Securities.Class, CLASS )
                         , INCOME_TYPE_CODE = NVL( R_Securities.Income_Type_Code, INCOME_TYPE_CODE )
                         , INCOME_FREQUENCY_VALUE = NVL( R_Securities.Income_Frequency_Value, INCOME_FREQUENCY_VALUE )
                         , INCOME_CURRENCY = NVL( R_Fri_Cy, INCOME_CURRENCY )
                         , INDICATED_ANNUAL_RATE = NVL( R_Securities.Indicated_Annual_Rate, INDICATED_ANNUAL_RATE )
                         , Par_Value = NVL( R_Securities.Par_Value, Par_Value )
                         , Par_Currency = NVL( R_Securities.Par_Currency, Par_Currency )
                         , CALL_SCHEDULE_FLAG = NVL( R_Securities.Call_Schedule_Flag, CALL_SCHEDULE_FLAG )
                         , CONVERSION_SCHEDULE_FLAG = NVL( R_Securities.Conversion_Schedule_Flag, CONVERSION_SCHEDULE_FLAG )
                         , EXT_RET_FLAG = NVL( R_Securities.Ext_Ret_Flag, EXT_RET_FLAG )
                         , INTANGIBILITY_FLAG = NVL( R_Securities.Intangibility_Flag, INTANGIBILITY_FLAG )
                         , MATURITY_SCHEDULE_FLAG = NVL( R_Securities.Maturity_Schedule_Flag, MATURITY_SCHEDULE_FLAG )
                         , MAT_DATE_SEL_METHOD_CODE = NVL( R_Securities.mat_Date_Sel_Method_Code, MAT_DATE_SEL_METHOD_CODE )
                         , PERPETUAL_FLAG = NVL( R_Securities.Perpetual_Flag, PERPETUAL_FLAG )
                         , PURCHASE_FUND_FLAG = NVL( R_Securities.Purchase_Fund_Flag, PURCHASE_FUND_FLAG )
                         , RATINGS_FLAG = NVL( R_Ratings_Flag, RATINGS_FLAG )
                         , SINKING_FUND_FLAG = NVL( R_Securities.Sinking_Fund_Flag, SINKING_FUND_FLAG )
                         , SPLIT_FLAG = NVL( R_Securities.Split_Flag, SPLIT_FLAG )
                         , MID_TERM_NOTE_FLAG = NVL( R_MTN_Flag, MID_TERM_NOTE_FLAG )
                         , LAST_USER = R_Last_User
                         , LAST_STAMP = Sysdate
                     WHERE fri_ticker = R_Fri_Ticker;
                    W_Record_Status := C_Modified;
                END IF; --} IF V_New_Sec
                -- Do I need to create more Tickers
                --   (FRI ID, Cusip, ISIN, OISIN, SEDOL)
                --
                IF G_Tracing
                THEN --{
                    pgmloc             := 6850;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Inserting Tickers'
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 6860;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Fri_Ticker : '''
                                                  || R_Fri_Ticker
                                                  || '''   Fri_Id : '''
                                                  || R_Fri_Id
                                                  || '''   Cusip : '''
                                                  || R_Cusip
                                                  || '''   Cins : '''
                                                  || R_Cins_Symbol
                                                  || '''   Cy : '''
                                                  || R_Cins_Cy
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 6870;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'ISIN : '''
                                                  || R_ISIN
                                                  || '''   Cy : '''
                                                  || R_ISIN_Cy
                                                  || '''   OISIN : '''
                                                  || R_OISIN
                                                  || '''   Cy : '''
                                                  || R_OISIN_Cy
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 6880;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Ric : '''
                                                  || R_RIC_Symbol
                                                  || '''   Source : '''
                                                  || R_RIC_Source
                                                  || '''   Cy : '''
                                                  || R_RIC_Cy
                                                  || '''   Sedol : '''
                                                  || R_Sedol
                                                  || '''   Cy : '''
                                                  || R_Sedol_Cy
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                --
                IF R_Fri_Ticker IS NOT NULL
                THEN --{
                    IDS := W_Identifiers.FIRST;
                    WHILE IDS IS NOT NULL
                    LOOP  --{
                      IF G_Tracing
                      THEN --{
                        pgmloc             := 6890;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Identification system code <'
                                                      || W_Identifiers(IDS).T_Rec.Identification_System_Code
                                                      || '>  Source <'
                                                      || W_Identifiers(IDS).T_Rec.Source_Code
                                                      || '>  Ticker <'
                                                      || W_Identifiers(IDS).T_Rec.Ticker
                                                      || '>  Currency <'
                                                      || W_Identifiers(IDS).T_Rec.Currency_Code
                                                      || '>  Iso nation <'
                                                      || W_Identifiers(IDS).T_Rec.Iso_Nation_Code
                                                      || '>'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                      END IF; --}
                      IF W_Identifiers(IDS).T_Rec.Identification_System_Code
                         = C_ID_OISIN
                      THEN --{
                        -- Do not insert OISIN anymore.
                        NULL;
                      ELSIF W_Identifiers(IDS).T_Rec.Identification_System_Code
                            = C_ID_CUSIP
                        -- starts by a letter
                        AND LOWER(SUBSTR(W_Identifiers(IDS).T_Rec.Ticker,1,1))
                         <> UPPER(SUBSTR(W_Identifiers(IDS).T_Rec.Ticker,1,1))
                        AND W_Identifiers(IDS).T_Rec.Ticker NOT LIKE 'TMP%'
                      THEN --}{
                        -- Do not insert a cusip if the ticker starts by
                        -- a letter
                        NULL;
                      ELSIF W_Identifiers(IDS).T_Rec.Identification_System_Code
                            = C_ID_CINS
                        -- Do not starts by a letter
                        AND ( LOWER(SUBSTR(W_Identifiers(IDS).T_Rec.Ticker,1,1))
                            = UPPER(SUBSTR(W_Identifiers(IDS).T_Rec.Ticker,1,1))
                           OR W_Identifiers(IDS).T_Rec.Ticker LIKE 'TMP%'
                            )
                      THEN --}{
                        -- Do not insert a CINS if the ticker do not starts
                        -- by a letter
                        NULL;
                      ELSE --}{
                        pgmloc := 6900;
                        INSERT
                          INTO TICKERS
                              (FRI_TICKER,    IDENTIFICATION_SYSTEM_CODE,
                               SOURCE_CODE,   TICKER,
                               CURRENCY_CODE,
                               ISO_NATION_CODE,
                               LAST_USER,     LAST_STAMP)
                        SELECT R_Fri_Ticker
                             , W_Identifiers(IDS).T_Rec.Identification_System_Code
                             , W_Identifiers(IDS).T_Rec.Source_Code
                             , W_Identifiers(IDS).T_Rec.Ticker
                             , W_Identifiers(IDS).T_Rec.Currency_Code
                             , W_Identifiers(IDS).T_Rec.Iso_Nation_Code
                             , R_Last_User,  SYSDATE
                          FROM DUAL
                         WHERE R_Fri_ID IS NOT NULL
                           AND NOT EXISTS (
                               SELECT 'X'
                                 FROM Tickers T
                                WHERE T.Fri_Ticker = R_Fri_Ticker
                                  AND T.identification_system_code
                                    = W_Identifiers(IDS).T_Rec.Identification_System_Code
                                  AND T.Source_Code
                                    = W_Identifiers(IDS).T_Rec.Source_Code
                                  AND ( T.Currency_Code
                                    = W_Identifiers(IDS).T_Rec.Currency_Code
                                     OR ( T.Currency_Code IS NULL
                                      AND W_Identifiers(IDS).T_Rec.Currency_Code IS NULL
                                        )
                                      )
                                  AND T.Iso_Nation_Code
                                    = W_Identifiers(IDS).T_Rec.Iso_Nation_Code
                                  AND ( T.Iso_Nation_Code
                                    = W_Identifiers(IDS).T_Rec.Iso_Nation_Code
                                     OR ( T.Iso_Nation_Code IS NULL
                                      AND W_Identifiers(IDS).T_Rec.Iso_Nation_Code IS NULL
                                        )
                                      )
                               );
                        --
                        V_Tick_Ins := V_Tick_Ins + SQL%RowCount;
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                      END IF; --}
                        IDS := W_Identifiers.NEXT(IDS);
                    END LOOP; --}
                END IF; --}
                --
                IF BR_IC_Parms
                THEN --{
                    -- Inserting the Interest Calc Parms
                    IF  R_Securities.Income_Type_Code = 'I'
                    AND R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 6910;
                        --
                        INSERT
                          INTO Interest_Calc_Parms
                              (Fri_Ticker,
                               Day_Count_Method_Code,
                               Accruals_Calc_Frequency_Value,
                               Interest_Schedule_Flag,
                               Floating_Rate_Flag,
                               Accruals_Calc_Formula_Code,
                               Compounding_frequency_Value,
                               Generate_Int_Sch_Flag,
                               Use_First_Pay_Date_Flag,
                               Last_User,
                               Last_Stamp)
                        SELECT R_Fri_Ticker,
                               R_Day_Count,
                               R_Acc_Freq_Value,
                               'N',
                               R_IC_Parms.Floating_Rate_Flag,
                               R_Acc_Calc_Formula,
                               R_Securities.Income_Frequency_Value,
                               R_Gen_Pay_Flag,
                               'N',
                               R_Last_User,
                               SYSDATE
                          FROM Dual
                         WHERE NOT EXISTS (
                               SELECT 'X'
                                 FROM Interest_Calc_Parms I
                                WHERE I.Fri_Ticker = R_Fri_Ticker
                               );
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --}
                END IF; --}
                --
                IF BR_ST_Status
                THEN --{
                    IF  R_SPC.Trading_Status_Flag != 'N'
                    AND R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 6920;
                        INSERT
                          INTO Security_Trading_Status
                              (FRI_TICKER,          SOURCE_CODE,
                               PRICE_CURRENCY,      TRADING_STATUS_CODE,
                               DATE_OF_STATUS,      JOBBER_CODE_FOR_STATUS,
                               LAST_USER,           LAST_STAMP)
                        SELECT R_Fri_Ticker,        R_ST_Status.Source_Code,
                               R_ST_Status.Price_Currency,    R_Trd_Status,
                               R_Site_Date,         R_User_ID,
                               R_Last_User,         SYSDATE
                          FROM Dual
                         WHERE NOT EXISTS (
                               SELECT 'X'
                                 FROM Security_Trading_Status T
                                WHERE T.fri_ticker = R_Fri_Ticker
                                  AND T.Source_Code = R_ST_Status.Source_Code
                               );
                        --
                        V_Trd_Ins := V_Trd_Ins + SQL%RowCount;
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --}
                END IF; --}
                IF G_Tracing
                THEN
                    pgmloc             := 6930;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Should we insert trading status?'
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    IF BR1_ST_Status
                    THEN
                        pgmloc             := 6940;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'BR1_ST_Status := TRUE'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                    ELSE
                        pgmloc             := 6950;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'BR1_ST_Status := FALSE'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                    END IF;
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 6960;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'trading_Status_Flag : '''
                                                  || R_SPC.Trading_Status_Flag
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 6970;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'R_Fri_Ticker : '''
                                                  || NVL(to_char(R_Fri_Ticker), 'NULL')
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF;
                --
                -- Trading status for exchange source 1
                IF  BR1_ST_Status
                AND R_SPC.Trading_Status_Flag != 'N'
                AND R_Fri_Ticker IS NOT NULL
                THEN --{
                    IF G_Tracing
                    THEN
                        pgmloc             := 6980;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Trying to trading status'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF;
                    pgmloc := 6990;
                    INSERT
                      INTO Security_Trading_Status
                          (FRI_TICKER,          SOURCE_CODE,
                           PRICE_CURRENCY,      TRADING_STATUS_CODE,
                           DATE_OF_STATUS,      JOBBER_CODE_FOR_STATUS,
                           LAST_USER,           LAST_STAMP)
                    SELECT R_Fri_Ticker,        R_Exch_Source_1,
                           R_Fri_Cy,            NVL(R_Trd_Status,G_Trd_Status),
                           R_Site_Date,         R_User_ID,
                           R_Last_User,         SYSDATE
                      FROM Dual
                     WHERE NOT EXISTS (
                           SELECT 'X'
                             FROM Security_Trading_Status T
                            WHERE T.fri_ticker = R_Fri_Ticker
                              AND T.Source_Code = R_Exch_Source_1
                           );
                    --
                    V_Trd_Ins := V_Trd_Ins + SQL%RowCount;
                    IF G_Tracing
                    THEN
                        pgmloc             := 7000;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Added '||SQL%Rowcount
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF;
                    IF  SQL%RowCount > 0
                    AND W_Record_Status IS NULL
                    THEN
                        W_Record_Status := C_Modified;
                    END IF;
                END IF; --}
                --
                -- Trading status for exchange source 2
                IF G_Tracing
                THEN
                    pgmloc             := 7010;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Should we insert trading status?'
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    IF BR2_ST_Status
                    THEN
                        pgmloc             := 7020;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'BR2_ST_Status := TRUE'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                    ELSE
                        pgmloc             := 7030;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'BR2_ST_Status := FALSE'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                    END IF;
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 7040;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'trading_Status_Flag : '''
                                                  || R_SPC.Trading_Status_Flag
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    pgmloc             := 7050;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'R_Fri_Ticker : '''
                                                  || NVL(to_char(R_Fri_Ticker), 'NULL')
                                                  || ''''
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF;
                IF  BR2_ST_Status
                AND R_SPC.Trading_Status_Flag != 'N'
                AND R_Fri_Ticker IS NOT NULL
                THEN --{
                    IF G_Tracing
                    THEN
                        pgmloc             := 7060;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Trying to trading status'
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF;
                    pgmloc := 7070;
                    INSERT
                      INTO Security_Trading_Status
                          (FRI_TICKER,          SOURCE_CODE,
                           PRICE_CURRENCY,      TRADING_STATUS_CODE,
                           DATE_OF_STATUS,      JOBBER_CODE_FOR_STATUS,
                           LAST_USER,           LAST_STAMP)
                    SELECT R_Fri_Ticker,        R_Exch_Source_2,
                           R_Fri_Cy,            NVL(R_Trd_Status,G_Trd_Status),
                           R_Site_Date,         R_User_ID,
                           R_Last_User,         SYSDATE
                      FROM Dual
                     WHERE NOT EXISTS (
                           SELECT 'X'
                             FROM Security_Trading_Status T
                            WHERE T.fri_ticker = R_Fri_Ticker
                              AND T.Source_Code = R_Exch_Source_2
                           );
                    --
                    V_Trd_Ins := V_Trd_Ins + SQL%RowCount;
                    IF G_Tracing
                    THEN
                        pgmloc             := 7080;
                        Load_Supplier_Rules.Write_File ( P_Log_File
                                                       , 'Added '||SQL%Rowcount
                                                       , P_Prc_Msgs_Rec
                                                       , V_Success
                                                       );
                        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    END IF;
                    IF  SQL%RowCount > 0
                    AND W_Record_Status IS NULL
                    THEN
                        W_Record_Status := C_Modified;
                    END IF;
                END IF; --}
                --
                IF BR_Mat_Sched
                THEN --{
                    IF  R_Mat_Sched.Maturity_Date IS NOT NULL
                    AND R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 7090;
                        INSERT
                          INTO Maturity_Schedule
                              (FRI_TICKER,
                               MATURITY_DATE,        MATURITY_TYPE_CODE,
                               REDEMPTION_AMOUNT,    REDEMPTION_CURRENCY,
                               LAST_USER,            LAST_STAMP)
                        SELECT R_Fri_ticker
                             , R_Mat_Sched.Maturity_Date
                             , R_Mat_Sched.Maturity_Type_Code
                             , R_Mat_Sched.Redemption_Amount
                             , R_Mat_Sched.Redemption_Currency
                             , R_Last_User,          SYSDATE
                          FROM Dual
                         WHERE NOT EXISTS (
                               SELECT 'X'
                                 FROM maturity_schedule M
                                WHERE M.Fri_Ticker = R_Fri_Ticker
                               );
                        --
                        V_Mat_Ins := V_Mat_Ins + SQL%RowCount;
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --} IF R_Maturity IS NOT NULL
                END IF; --} IF R_Maturity IS NOT NULL
                --
                IF BR_Prc_Notes
                THEN --{
                    IF  R_Prc_Notes.Pricing_Note IS NOT NULL
                    AND R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 7100;
                        INSERT
                          INTO Pricing_Notes
                              (FRI_TICKER,        PRICING_NOTE,
                               LAST_USER,         LAST_STAMP)
                        SELECT R_Fri_ticker,      R_Prc_Notes.Pricing_Note,
                               R_Last_User,       SYSDATE
                          FROM Dual
                         WHERE NOT EXISTS (
                               SELECT 'X'
                                 FROM Pricing_Notes P
                                WHERE P.Fri_Ticker = R_Fri_Ticker
                               );
                        --
                        V_PrcN_Ins := V_PrcN_Ins + SQL%RowCount;
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --} IF R_Prc_Notes.Pricing_Note IS NOT NULL
                END IF; --}
                --
                IF BR_Issues
                THEN --{
                    IF  R_Securities.Income_Type_Code = 'I'
                    AND R_Issues.Offering_Date IS NOT NULL
                    AND R_SPC.Issue_Flag != 'N'
                    AND R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 7110;
                        INSERT
                          INTO Issues
                             ( FRI_TICKER           , ISSUE_DATE
                             , PRIVATE_PLACEMENT_FLAG
                             , LAST_USER            , LAST_STAMP
                             )
                        SELECT R_Fri_ticker,      R_Issues.Offering_Date
                             , NVL( R_Issues.Private_Placement_Flag, 'N' )
                             , R_Last_User,       SYSDATE
                          FROM Dual
                         WHERE NOT EXISTS (
                               SELECT 'X'
                                 FROM Issues P
                                WHERE P.Fri_Ticker         = R_Fri_Ticker
                               );
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --}
                END IF; --}
                --
                IF BR_II_Rates
                THEN --{
                    IF  R_Securities.Income_Type_Code = 'I'
                    AND R_II_Rates.First_Payment_Date     IS NOT NULL
                    AND R_II_Rates.Accruals_Start_Date IS NOT NULL
                    AND R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 7120;
                        INSERT
                          INTO Interest_Income_Rates
                             ( FRI_TICKER           , ACCRUALS_START_DATE
                             , FIRST_PAYMENT_DATE   , ANNUAL_INCOME_RATE
                             , INCOME_CURRENCY      , OPT_INCOME_CURRENCY
                             , SCHEDULED_EVENT_FLAG
                             , LAST_USER            , LAST_STAMP
                             )
                        SELECT R_Fri_ticker,      R_II_Rates.Accruals_Start_Date
                             , R_II_Rates.First_Payment_Date,  R_Securities.Indicated_Annual_Rate
                             , R_Fri_Cy        ,  NULL
                             , NVL( R_II_Rates.Scheduled_Event_Flag, 'N' )
                             , R_Last_User,       SYSDATE
                          FROM Dual
                         WHERE NOT EXISTS (
                               SELECT 'X'
                                 FROM Interest_Income_Rates P
                                WHERE P.Fri_Ticker         = R_Fri_Ticker
                               );
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --}
                END IF; --}
                --
                IF BR_PC_Sched
                THEN --{
                    IF  R_PC_Sched.Data_Supplier_Code  IS NOT NULL
                    AND R_PC_Sched.Delivery_Time IS NOT NULL
                    AND R_PC_Sched.Source_Code          IS NOT NULL
                    AND R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 7130;
                        INSERT
                          INTO Pricing_Calc_Schedule
                             ( FRI_TICKER         , SOURCE_CODE
                             , DATA_SUPPLIER_CODE , DELIVERY_TIME
                             , LAST_USER          , LAST_STAMP
                             )
                        SELECT R_Fri_ticker,      R_PC_Sched.Source_Code
                             , R_PC_Sched.Data_Supplier_Code,  R_PC_Sched.Delivery_Time
                             , R_Last_User,       SYSDATE
                          FROM Dual
                         WHERE NOT EXISTS (
                               SELECT 'X'
                                 FROM Pricing_Calc_Schedule P
                                WHERE P.Fri_Ticker         = R_Fri_Ticker
                                  AND P.Source_Code        = R_PC_Sched.Source_Code
                                  AND P.Data_Supplier_Code = R_PC_Sched.Data_Supplier_Code
                                  AND P.Delivery_Time      = R_PC_Sched.Delivery_Time
                               );
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --}
                END IF; --}
                --
                IF BR_Index_Details
                THEN --{
                    -- Inserting the Interest Calc Parms
                    IF   R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 7140;
                        --
                        INSERT
                          INTO Index_Details
                              (Fri_Ticker,
                               I_Description,
                               I_Category,
                               Base_Date,
                               Base_Value,
                               Rebal_Frequency_Value,
                               Last_User,
                               Last_Stamp)
                        SELECT R_Fri_Ticker,
                               R_Index_Details.I_Description,
                               R_Index_Details.I_Category,
                               R_Index_Details.Base_Date,
                               R_Index_Details.Base_Value,
                               R_Index_Details.Rebal_Frequency_Value,
                               R_Last_User,
                               SYSDATE
                          FROM Dual
                         WHERE NOT EXISTS (
                               SELECT 'X'
                                 FROM Index_Details I
                                WHERE I.Fri_Ticker = R_Fri_Ticker
                               );
                        IF  SQL%RowCount > 0
                        AND W_Record_Status IS NULL
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --}
                END IF; --}
                --
                IF B_US_Munis
                THEN --{
                    -- Inserting the Interest Calc Parms
                    IF   R_Fri_Ticker IS NOT NULL
                    THEN --{
                        pgmloc := 7142;
                        R_Us_Munis.Fri_Ticker := R_Fri_Ticker;
                        Pgmloc := 7144;
                        Add_US_Munis_Attributes( R_Us_Munis, P_Create_Sec, V_Success );
                        IF V_Success
                        THEN
                            W_Record_Status := C_Modified;
                        END IF;
                    END IF; --}
                END IF; --}
            END IF; --} G_Many_Tickers AND ...
            --
            IF V_New_Sec
            THEN
                V_Fri_ticker_End := R_Fri_Ticker;
            END IF;
            --
        EXCEPTION
            WHEN OTHERS
            THEN --{
                V_Error_Code := TRIM( TO_CHAR( SQLCODE ) );
                V_Message := SUBSTR( SQLERRM( SQLCODE ) ,1 ,255 );
                IF ( SQLCODE BETWEEN -20999 AND -20000 )
                THEN
                    V_Error_Message := SUBSTR( V_Message, 12 );
                ELSE
                    Manage_Messages.Process_Error( V_Error_Code
                                                 , V_Message
                                                 , V_Error_Column
                                                 , V_Error_Message
                                                 );
                END IF;
                IF V_Error_Message IS NULL
                THEN
                    V_Error_Message := SUBSTR( SQLERRM( SQLCODE ) ,1 ,255 );
                END IF;
                IF ( INSTR( V_Error_Message, 'ORA-06512', 1, 1 ) <> 0 )
                THEN
                    V_Error_Message := SUBSTR( V_Error_Message
                                             , 1
                                             , INSTR( V_Error_Message
                                                    , 'ORA-06512', 1, 1 )
                                               - 2
                                             );
                END IF;
                --
                ROLLBACK;
                V_Rows_Err := V_Rows_Err + 1;
                W_Record_Status := C_Error;
                V_Secs_Ins := P_Prc_Msgs_Rec.Value_04 ;
                V_Secs_Upd := P_Prc_Msgs_Rec.Value_05 ;
                V_Tick_Ins := P_Prc_Msgs_Rec.Value_06 ;
                V_Trd_Ins  := P_Prc_Msgs_Rec.Value_07 ;
                V_Mat_Ins  := P_Prc_Msgs_Rec.Value_08 ;
                V_PrcN_Ins := P_Prc_Msgs_Rec.Value_09 ;
                IF NOT V_Row_Tracked
                THEN --{
                    V_Row_Tracked := TRUE;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , NULL
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    Load_Supplier_Rules.Write_File ( P_Log_File
                                                   , 'Processing Row # '
                                                  || TO_CHAR(V_Rows_read)
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                IF NOT V_Err_Tracked
                THEN --{
                    V_Err_Tracked := TRUE;
                    Load_Supplier_Rules.Write_File ( P_Err_File
                                                   , 'Processing Row # '
                                                  || TO_CHAR(V_Rows_read)
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                    Load_Supplier_Rules.Write_File ( P_Err_File
                                                   , V_Record
                                                   , P_Prc_Msgs_Rec
                                                   , V_Success
                                                   );
                    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                END IF; --}
                --
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , Pgmloc||C_Indent || V_Error_Message
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                Load_Supplier_Rules.Write_File ( P_Err_File
                                               , Pgmloc||C_Indent || V_Error_Message
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                V_Row_Errors := TRUE;
             --} WHEN OTHERS THEN
        END; --} BEGIN
        --
        IF W_Record_Status IS NULL
        THEN
            V_Rows_Skip := V_Rows_Skip + 1;
            IF G_Tracing
            THEN
                pgmloc             := 7200;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , '######### SKIPPED ########'
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF;
        ELSIF W_Record_Status = C_Modified
        THEN
            V_Secs_Upd := V_Secs_Upd + 1;
        END IF;
        --
<< Next_Row >>
        --
        pgmloc := 7210;
        P_Prc_Msgs_Rec.Value_01 := V_Rows_Read;
        P_Prc_Msgs_Rec.Value_02 := V_Rows_Skip;
        P_Prc_Msgs_Rec.Value_03 := V_Rows_Err ;
        P_Prc_Msgs_Rec.Value_04 := V_Secs_Ins ;
        P_Prc_Msgs_Rec.Value_05 := V_Secs_Upd ;
        P_Prc_Msgs_Rec.Value_06 := V_Tick_Ins ;
        P_Prc_Msgs_Rec.Value_07 := V_Trd_Ins  ;
        P_Prc_Msgs_Rec.Value_08 := V_Mat_Ins  ;
        P_Prc_Msgs_Rec.Value_09 := V_PrcN_Ins ;
        --
        pgmloc := 7220;
        Load_Supplier_Rules.Update_Prc_Msgs ( G_Program_ID
                                            , G_System_Date
                                            , P_Prc_Msgs_Rec
                                            , V_Success
                                            );
        pgmloc := 7230;
        COMMIT;
        --
        IF V_Row_Errors
        THEN --{
-- Si c' est une erreur, le previous n'est pas bon!
            R_Fri_Ticker  := NULL;
            R_Fri_Id      := NULL;
            W_Cusip       := NULL;
            W_Issuer      := NULL;
            W_Long_Name_E := NULL;
            W_Series      := NULL;
            W_Class       := NULL;
            W_Created     := FALSE;
            IF NOT V_Row_Tracked
            THEN --{
                V_Row_Tracked := TRUE;
                pgmloc             := 7240;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , NULL
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                pgmloc             := 7250;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , 'Processing Row # '
                                              || TO_CHAR(V_Rows_read)
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF; --}
            IF NOT V_Err_Tracked
            THEN --{
                V_Err_Tracked := TRUE;
                pgmloc             := 7260;
                Load_Supplier_Rules.Write_File ( P_Err_File
                                               , 'Processing Row # '
                                              || TO_CHAR(V_Rows_read)
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
                pgmloc             := 7270;
                Load_Supplier_Rules.Write_File ( P_Log_File
                                               , V_Record
                                               , P_Prc_Msgs_Rec
                                               , V_Success
                                               );
                IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            END IF; --}
            pgmloc := 7280;
            G_FMessage_Code := G_Message_Code || '210';
            Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                              , G_Del_Flag
                                              , V_Message
                                              );
            --
            V_Message := C_Indent || V_Message;
            --
            pgmloc := 7290;
            Load_Supplier_Rules.Write_File ( P_Log_File
                                           , V_Message
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
            pgmloc := 7300;
            Load_Supplier_Rules.Write_File ( P_Err_File
                                           , V_Message
                                           , P_Prc_Msgs_Rec
                                           , V_Success
                                           );
            IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        END IF; --} IF V_Row_Errors
        Previous_Fri_Ticker  := R_Fri_Ticker;
        Previous_Fri_ID      := R_Fri_Id;
        Previous_Cusip       := W_Cusip;
        Previous_Issuer      := W_Issuer;
        Previous_Long_Name_E := W_Long_Name_E;
        Previous_Series      := W_Series;
        Previous_Class       := W_Class;
        Previous_Created     := W_Created;
    END LOOP; --} WHILE NOT End_Of_File
    --
    -- Summary Report
    --
    pgmloc := 7310;
    G_FMessage_Code := G_Message_Code || '300';
    R_Field :=  '';
    pgmloc := 7320;
    Load_Supplier_Rules.Write_File ( P_Log_File
                                   , NULL
                                   , P_Prc_Msgs_Rec
                                   , V_Success
                                   );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    FOR X IN 1..6
    LOOP --{
        pgmloc := 7330;
        Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                          , G_Del_Flag
                                          , R_Field
                                          , V_Message
                                          );
        --
        V_Message := C_Indent || V_Message;
        --
        pgmloc := 7340;
        Load_Supplier_Rules.Write_File ( P_Log_File
                                       , V_Message
                                       , P_Prc_Msgs_Rec
                                       , V_Success
                                       );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        G_FMessage_Code := G_Message_Code || TO_CHAR(300+(5*X));
        IF X = 2 Then R_Field := NVL( V_Rows_Read, 0 ); End IF;
        IF X = 3 Then R_Field := NVL( V_Rows_Skip, 0 ); End IF;
        IF X = 4 Then R_Field := NVL( V_Rows_Err , 0 ); End IF;
        IF X = 5 Then R_Field := NVL( V_Secs_Ins , 0 ); End IF;
        --}
    END LOOP;
    pgmloc := 7350;
    Load_Supplier_Rules.Write_File ( P_Log_File
                                   , NULL
                                   , P_Prc_Msgs_Rec
                                   , V_Success
                                   );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    -- Report all FRI IDs created
    --
    pgmloc := 7360;
    --
    FOR Fri_Ids_Rec
     IN Get_New_Tickers ( V_FRI_Ticker_Start, V_FRI_Ticker_End )
    LOOP
        --{
        Load_Supplier_Rules.Write_File ( P_Log_File
                                       , C_Indent
                                      || 'Issuer : '
                                      || Fri_IDS_Rec.Issuer_Code     || ' '
                                      || 'Security : '
                                      || Fri_IDS_Rec.FRI_ID          || ' '
                                      || Fri_IDS_Rec.Issue_Name      || ' '
                                      || Fri_IDS_Rec.Issuer_Name     || ' '
                                      || Fri_IDS_Rec.Income_Currency
                                       , P_Prc_Msgs_Rec
                                       , V_Success
                                       );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
-- double spaced
        Load_Supplier_Rules.Write_File ( P_Log_File
                                       , null
                                       , P_Prc_Msgs_Rec
                                       , V_Success
                                       );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
        --}
    END Loop;
    --
    P_Prc_Msgs_Rec.Successfull_Flag := 'Y';
    P_Success := V_Success;
    GOTO Upd_Prc_Msg;
    --
    --
<<GET_OUT>>
    pgmloc := 7370;
    IF V_Create_Sec
    THEN
        P_Prc_Msgs_Rec.Err_Text := 'Problem with Creating Securities';
    ELSE
        P_Prc_Msgs_Rec.Err_Text := 'Problem with Validating Securities';
    END IF;
    --
    --
<<Upd_Prc_Msg>>
    --
    pgmloc := 7380;
    P_Prc_Msgs_Rec.Value_01 := V_Rows_Read;
    P_Prc_Msgs_Rec.Value_02 := V_Rows_Skip;
    P_Prc_Msgs_Rec.Value_03 := V_Rows_Err ;
    P_Prc_Msgs_Rec.Value_04 := V_Secs_Ins ;
    P_Prc_Msgs_Rec.Value_05 := V_Secs_Upd ;
    P_Prc_Msgs_Rec.Value_06 := V_Tick_Ins ;
    P_Prc_Msgs_Rec.Value_07 := V_Trd_Ins  ;
    P_Prc_Msgs_Rec.Value_08 := V_Mat_Ins  ;
    P_Prc_Msgs_Rec.Value_09 := V_PrcN_Ins ;
    --
    Pgmloc := 7390;
    Load_Supplier_Rules.Update_Prc_Msgs ( G_Program_ID
                                        , G_System_Date
                                        , P_Prc_Msgs_Rec
                                        , V_Success
                                        );
    pgmloc := 7400;
    COMMIT;
EXCEPTION
    WHEN OTHERS
    THEN --{
        P_Prc_Msgs_Rec.Msg_Text_2       := 'Error occured in process ' ||
                                           ' @pgmloc=' || TO_CHAR(pgmloc);
        P_Prc_Msgs_Rec.Err_Text         := SUBSTR(SQLERRM(SQLCODE),1,255);
        P_Prc_Msgs_Rec.Successfull_Flag := 'N';
        P_Prc_Msgs_Rec.Value_01         := V_Rows_Read;
        P_Prc_Msgs_Rec.Value_02         := V_Rows_Skip;
        P_Prc_Msgs_Rec.Value_03         := V_Rows_Err ;
        P_Prc_Msgs_Rec.Value_04         := V_Secs_Ins ;
        P_Prc_Msgs_Rec.Value_05         := V_Secs_Upd ;
        P_Prc_Msgs_Rec.Value_06         := V_Tick_Ins ;
        P_Prc_Msgs_Rec.Value_07         := V_Trd_Ins  ;
        P_Prc_Msgs_Rec.Value_08         := V_Mat_Ins  ;
        P_Prc_Msgs_Rec.Value_09         := V_PrcN_Ins ;
        --
        Load_Supplier_Rules.Update_Prc_Msgs ( G_Program_ID
                                            , G_System_Date
                                            , P_Prc_Msgs_Rec
                                            , V_Success
                                            );
        --
        Load_Supplier_Rules.Write_File ( P_Log_File
                                       , P_Prc_Msgs_Rec.Msg_Text_2
                                       , P_Prc_Msgs_Rec
                                       , V_Success
                                       );
        --
        Load_Supplier_Rules.Write_File ( P_Log_File
                                       , 'Program Crashed.  Call Programming.'
                                       , P_Prc_Msgs_Rec
                                       , V_Success
                                       );
        --
        Commit;
        P_Success := FALSE;
        --
        RAISE;
    --} WHEN OTHERS THEN
    --}}
END Validate_Or_Create;
---
------------------------------------------------------------------------------
---
PROCEDURE Batch
        ( P_Program_Id    IN         Ops_Scripts.Program_Id%TYPE
        , P_Success       OUT NOCOPY BOOLEAN
        , P_Message       OUT NOCOPY VARCHAR2
        ) IS
--{
    Pricing_Msgs_Rec     Pricing_Msgs%ROWTYPE;
    V_Success            BOOLEAN;
    V_Kind_Parm          VARCHAR2(20);
    V_Script_Parm        VARCHAR2(20);
    V_Program_Id         Ops_Scripts.Program_Id%TYPE;
    V_Create_Sec         BOOLEAN;
    V_Create_Issuer      BOOLEAN;
    V_Message            VARCHAR2(255);
    V_Field              VARCHAR2(128);
    Unsupported_Kind     EXCEPTION;
    V_Wrap_Up_Success    BOOLEAN;
    V_Tracing            VARCHAR2(128);
    --
    Copy_File_Name       Supplier_Running_Parms.File_Name%TYPE;
    Data_File_Name       Supplier_Running_Parms.File_Name%TYPE;
    Log_File_Name        Supplier_Running_Parms.File_Name%TYPE;
    Err_File_Name        Supplier_Running_Parms.File_Name%TYPE;
    --
    Dir_Name             VARCHAR2(100);
    DSS_Rec              Data_Supply_Schedule%ROWTYPE;
    IDS_Rec              Identification_Systems%ROWTYPE;
    --
    V_Securities         BOOLEAN := TRUE;
    V_Schedule           BOOLEAN := FALSE;
    V_Tickers            BOOLEAN := FALSE;
    V_Status             BOOLEAN := FALSE;
-- Ajout - JSL - 25 septembre 2015
    V_Universe           BOOLEAN := FALSE;
-- Fin d'ajout - JSL - 25 septembre 2015
    --
BEGIN
    --{
    pgmloc  := 7410;
    --
    -- Initialize Variables
    --
    P_Success          := FALSE;
    P_Message          := NULL;
    --
    G_Program_ID       := P_Program_Id;
    G_System_Date      := Sysdate;
    --
    -- Initialize Pricing Msgs
    --
    pgmloc  := 7420;
    Load_Supplier_Rules.Initialize_Prc_Msgs ( G_Program_Id
                                            , G_System_date
                                            , G_Site_Date
                                            , Pricing_Msgs_Rec
                                            , V_Success
                                            );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;

-- Ajout - JSL - 15 decembre 2015
    pgmloc := 7425;
    G_All_Parms := Scripts.Get_All_Parms( P_Program_Id );
-- Fin d'ajout - JSL - 15 decembre 2015
    --
    pgmloc := 7430;
    V_Kind_Parm := NVL( UPPER( Scripts.Get_Arg_Value( G_Program_ID
                                                      , 'KIND'
                                                      )
                               )
                        , 'FOREIGN_BONDS'
                        );
    IF V_Kind_Parm = 'MUNI_BONDS'
    THEN
        Initialize_Muni_Bonds( Pricing_Msgs_Rec, V_Create_Issuer, V_Success );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    ELSIF V_Kind_Parm = 'FOREIGN_BONDS'
    THEN
        Initialize_Fgn_Bonds( Pricing_Msgs_Rec, V_Create_Issuer, V_Success );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    ELSIF V_Kind_Parm = 'MUTUAL_FUNDS'
    THEN
        Initialize_Mutual_Funds( Pricing_Msgs_Rec, V_Create_Issuer, V_Success );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    ELSIF V_Kind_Parm = 'FOREIGN_STOCKS'
    THEN
        Initialize_Fgn_Stocks( Pricing_Msgs_Rec, V_Create_Issuer, V_Success );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    ELSIF V_Kind_Parm = 'INDEX'
    THEN
        Initialize_Index( Pricing_Msgs_Rec, V_Create_Issuer, V_Success );
        IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    ELSIF V_Kind_Parm = C_Auto
    THEN
        G_Header  := C_Header;
        G_Footer  := C_Footer;
        G_Auto    := TRUE;
        V_Success := TRUE;
        pgmloc := 7440;
        V_Script_Parm := NVL( UPPER( Scripts.Get_Arg_Value( G_Program_ID
                                                          , 'Issuer'
                                                          )
                                   )
                            , 'VALIDATE'
                            );
        --
        -- Do I Validate Only or Validate/Create Securities
        IF V_Script_Parm = 'CREATE'
        THEN
            V_Create_Issuer := TRUE;
        ELSE
            V_Create_Issuer := FALSE;
        END IF;
        pgmloc := 7450;
        V_Script_Parm := NVL( UPPER( Scripts.Get_Arg_Value( G_Program_ID
                                                          , 'Table'
                                                          )
                                   )
                            , 'SECURITIES'
                            );
        IF V_Script_Parm = 'SECURITIES'
        THEN
          V_Securities := TRUE;
          V_Schedule   := FALSE;
          V_Tickers    := FALSE;
          V_Status     := FALSE;
        ELSIF V_Script_Parm = 'SCHEDULES'
        THEN
          V_Securities := FALSE;
          V_Schedule   := TRUE;
          V_Tickers    := FALSE;
          V_Status     := FALSE;
        ELSIF V_Script_Parm = 'TICKERS'
        THEN
          V_Securities := FALSE;
          V_Schedule   := FALSE;
          V_Tickers    := TRUE;
          V_Status     := FALSE;
        ELSIF V_Script_Parm = 'STATUSES'
        THEN
          V_Securities := FALSE;
          V_Schedule   := FALSE;
          V_Tickers    := FALSE;
          V_Status     := TRUE;
-- Ajout - JSL - 25 septembre 2015
        ELSIF V_Script_Parm = 'UNIVERSE'
        THEN
          V_Securities := FALSE;
          V_Schedule   := FALSE;
          V_Tickers    := FALSE;
          V_Status     := FALSE;
          V_Universe   := TRUE;
-- Fin d'ajout - JSL - 25 septembre 2015
        END IF;
    ELSE
        RAISE Unsupported_Kind;
    END IF;
    --
    pgmloc := 7460;
    V_Program_ID  := NVL( SCRIPTS.Get_Arg_Value ( G_Program_id
                                                , 'PGMID'
                                                )
                        , G_Program_ID
                        );
    pgmloc := 7470;
    V_Script_Parm := NVL( UPPER( Scripts.Get_Arg_Value( G_Program_ID
                                                      , 'PARM'
                                                      )
                               )
                        , 'VALIDATE'
                        );
    --
    -- Do I Validate Only or Validate/Create Securities
    IF V_Script_Parm = 'CREATESEC'
    THEN
        V_Create_Sec := TRUE;
    ELSE
        V_Create_Sec := FALSE;
    END IF;
    pgmloc := 7480;
    V_Tracing := Scripts.Get_Arg_Value( G_Program_ID, 'Tracing' );
    IF V_Tracing IS NULL
    THEN
        G_Tracing := FALSE;
    ELSE
        G_Tracing := TRUE;
    END IF;
    --
    -- Getting the Supplier info for the fetched Program_ID
    --
    pgmloc := 7490;
    Load_Supplier_Rules.Get_Supplier_Info ( V_Program_Id
                                          , Pricing_Msgs_Rec
                                          , DSS_Rec
                                          , IDS_Rec
                                          , V_Success
                                          );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    --
    pgmloc := 7500;
    --
    -- Getting different messages for Create/Validate
    --
    --
    IF V_Create_Sec THEN
        -- Creating Securities For:
        G_FMessage_Code := G_Message_Code || '002';
    ELSE
        -- Validating Securities For:
        G_FMessage_Code := G_Message_Code || '004';
    END IF;
    --
    pgmloc := 7510;
    Manage_Messages.Get_Oasis_Message ( G_FMessage_Code
                                      , G_Del_Flag
                                      , DSS_Rec.Data_Supplier_Code
                                      , DSS_Rec.Delivery_Time
                                      , V_Message
                                      );
    --
    pgmloc := 7520;
    Pricing_Msgs_Rec.Msg_Text := V_Message;
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
    --
    pgmloc := 7530;
    Load_Supplier_Rules.Update_Prc_Msgs ( G_Program_ID
                                        , G_System_Date
                                        , Pricing_Msgs_Rec
                                        , V_Success
                                        );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    pgmloc := 7540;
    Load_Supplier_Rules.Get_File_Names ( DSS_Rec.Data_Supplier_Code
                                       , DSS_Rec.Delivery_Time
                                       , Pricing_Msgs_Rec
                                       , Data_File_Name
                                       , Log_File_Name
                                       , Copy_File_Name
                                       , Dir_Name
                                       , V_Success
                                       );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    Pgmloc := 7550;
    Err_File_Name := SUBSTR( Log_File_Name, 1, LENGTH( Log_File_Name ) - 4 )
                  || '.err' ;
    --
    pgmloc := 7560;
    ---------------------------------------
    --  Open the Log file in the Write mode
    ---------------------------------------
    Load_Supplier_Rules.Open_File
                             ( Log_File_Name
                             , Dir_Name
                             , 'W'
                             , Pricing_Msgs_Rec
                             , Log_File
                             , V_Success
                             );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    pgmloc := 7570;
    Load_Supplier_Rules.Write_File ( Log_File
                                   , 'Program ID: '
                                  || TO_CHAR( G_Program_id )
                                  || ' '
                                  || 'Supplier is: '
                                  || DSS_Rec.Data_Supplier_Code
                                  || ' '
                                  || DSS_Rec.Delivery_Time
                                   , Pricing_Msgs_Rec
                                   , V_Success
                                   );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    pgmloc := 7580;
    Load_Supplier_Rules.Write_File ( Log_File
                                   , 'Started at '
                                  || TO_CHAR( G_System_Date
                                            , 'dd-mon-yyyy hh24:mi:ss'
                                            )
                                   , Pricing_Msgs_Rec
                                   , V_Success
                                   );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    pgmloc := 7590;
    Data_File_Name := NVL( Scripts.Get_Arg_Value( G_Program_ID, 'Data_File' )
                         , Data_File_Name );
    --
    pgmloc := 7600;
    Load_Supplier_Rules.Write_File ( Log_File
                                   , 'Using Data from file : '''
                                   || Dir_Name
                                   || G_Dir_Separator
                                   || Data_File_Name
                                   || ''''
                                   , Pricing_Msgs_Rec
                                   , V_Success
                                   );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    pgmloc := 7610;
    Load_Supplier_Rules.Write_File ( Log_File
                                   , 'Lines in error are written to '''
                                   || Dir_Name
                                   || G_Dir_Separator
                                   || Err_File_Name
                                   || ''''
                                   , Pricing_Msgs_Rec
                                   , V_Success
                                   );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    -------------------------------------------
    --  Open the Supplier File in the Read mode
    -------------------------------------------
    pgmloc := 7620;
    Load_Supplier_Rules.Open_File ( Data_File_Name
                                  , Dir_Name
                                  , 'R'
                                  , Pricing_Msgs_Rec
                                  , Data_File
                                  , V_Success
                                  );
    IF ( NOT V_SUCCESS )
    THEN
        pgmloc := 7630;
        Load_Supplier_Rules.Write_File ( Log_File
                                       , 'Unable to open the data file'
                                       , Pricing_Msgs_Rec
                                       , V_Success
                                       );
        GOTO GET_OUT;
    END IF;
    --
    pgmloc := 7640;
    ---------------------------------------
    --  Open the Error file in the Write mode
    ---------------------------------------
    Load_Supplier_Rules.Open_File
                             ( Err_File_Name
                             , Dir_Name
                             , 'W'
                             , Pricing_Msgs_Rec
                             , Err_File
                             , V_Success
                             );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    -- Process all Lines find in File of Foreign Bonds
    --
    IF V_Securities
    THEN
      pgmloc := 7650;
      Validate_or_Create ( Data_File
                         , Log_File
                         , Err_File
                         , V_Create_Sec
                         , V_Create_Issuer
                         , Pricing_Msgs_Rec
                         , V_Success
                         );
      IF NOT V_Success THEN GOTO Get_Out; END IF;
    ELSIF V_Schedule
       OR V_Tickers
       OR V_Status
    THEN
      pgmloc := 7660;
      Add_Schedules ( V_Create_Sec
                    , V_Success
                    );
      IF NOT V_Success THEN GOTO Get_Out; END IF;
-- Ajout - JSL - 25 septembre 2015
    ELSIF V_Universe
    THEN
      pgmloc := 7662;
      Add_Universes ( V_Create_Sec
                    , V_Success
                    );
      IF NOT V_Success THEN GOTO Get_Out; END IF;
-- Fin d'ajout - JSL - 25 septembre 2015
    END IF;

    -------------------------------------
    --  Closing both the files at the end
    -------------------------------------
    pgmloc := 7670;
    Load_Supplier_Rules.File_Close( Data_File
                                  , Pricing_Msgs_Rec
                                  , V_Success
                                  );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    pgmloc := 7680;
    Load_Supplier_Rules.File_Close( Log_File
                                  , Pricing_Msgs_Rec
                                  , V_Success
                                  );
    IF ( NOT V_SUCCESS ) THEN GOTO GET_OUT; END IF;
    --
    V_Wrap_Up_Success := TRUE;
    --
    --
    Manage_Tms_Info.Set_To_Status( Pricing_Msgs_Rec.Program_Id, 'READY' );
    pgmloc := 7750;
    --
    -- Renaming the Data File after Creating
    -- A copy will be done instead. Otherwise, a chmod 777 will be required
    --
    IF V_Create_Sec
    THEN
        pgmloc := 7760;
        Manage_Utl_File.Copy_File ( Constants.Get_Dir_Name
                                  , Data_File_Name
                                  , Constants.Get_Dir_Name
                                  , Data_File_Name
                                 || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')
                                  , V_Success
                                  , V_Message
                                  );
        IF NOT V_Success THEN
            Pricing_Msgs_Rec.Err_Text := Substr(Pricing_Msgs_Rec.Err_Text
                                     ||'-'||V_Message||'@'||pgmloc, 1, 255);
            V_Wrap_Up_Success := FALSE;
        END IF; -- Success flag check
    END IF;  -- V_Create_Sec flag check
    pgmloc := 7770;
    --
    -- making a copy of the log file
    --
    pgmloc := 7780;
    Manage_Utl_File.Copy_File ( Constants.Get_Dir_Name
                              , Log_File_Name
                              , Constants.Get_Dir_Name
                              , Log_File_Name
                             || TO_CHAR(SYSDATE,'YYYYMMDDHH24MISS')
                              , V_Success
                              , V_Message
                              );
    IF NOT V_Success
    THEN
        Pricing_Msgs_Rec.Err_Text := Substr(Pricing_Msgs_Rec.Err_Text
                                   ||'-'||V_Message||'@'||pgmloc, 1, 255);
        V_Wrap_Up_Success := FALSE;
    END IF;
    --
    IF NOT V_Wrap_Up_Success
    THEN
        GOTO Get_Out;
    END IF;
    --
    Pricing_Msgs_Rec.Successfull_Flag := 'Y';
    GOTO Upd_Prc_Msg;
    --
<<GET_OUT>>
    pgmloc := 7790;
    IF V_Create_Sec
    THEN
        Pricing_Msgs_Rec.Err_Text := 'Problem with Creation of Securities';
    ELSE
        Pricing_Msgs_Rec.Err_Text := 'Problem with Validation of Securities';
    END IF;
<<GET_OUT_AFT>>
    pgmloc := 7800;
    Pricing_Msgs_Rec.Msg_Text_2       := 'Error while running the program';
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
--
<<Upd_Prc_Msg>>
    --
    pgmloc := 7810;
    P_Message := Pricing_Msgs_Rec.Err_Text;
    --
    pgmloc := 7820;
    Load_Supplier_Rules.Update_Prc_Msgs( G_Program_ID
                                       , G_System_Date
                                       , Pricing_Msgs_Rec
                                       , V_Success
                                       );
    pgmloc := 7830;
    commit;
    P_Success := V_Success;
    --
EXCEPTION
    WHEN OTHERS
    THEN
        Pricing_Msgs_Rec.Msg_Text_2       := 'Error occured in process ' ||
                                           ' @pgmloc=' || TO_CHAR(pgmloc);
        Pricing_Msgs_Rec.Err_Text         := SUBSTR(SQLERRM(SQLCODE),1,255);
        Pricing_Msgs_Rec.Successfull_Flag := 'N';
        --
        Load_Supplier_Rules.Update_Prc_Msgs ( G_Program_ID
                                            , G_System_Date
                                            , Pricing_Msgs_Rec
                                            , V_Success
                                            );
        --
        Load_Supplier_Rules.Write_File ( Log_File
                                       , 'Program Crashed.  Call Programming.'
                                       , Pricing_Msgs_Rec
                                       , V_Success
                                       );
        --
        Load_Supplier_Rules.File_Close( Data_File
                                      , Pricing_Msgs_Rec
                                      , V_Success
                                      );
        --
        Load_Supplier_Rules.File_Close( Log_File
                                      , Pricing_Msgs_Rec
                                      , V_Success
                                      );
        --
        Commit;
        P_Success := FALSE;
        --
        RAISE;
        --
--}}
END Batch;
--
END Create_Securities;
/
show error
