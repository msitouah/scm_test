CREATE OR REPLACE package body &&owner..csv_to_msef
as --{
------------------------------------------------------------------------
--
-- mlz - 01mar2011 - generic load for fundamentals
-- mlz - 23jan2012 - Fix a bug (Delta - Memory Error)
-- mlz - ..mar2012 - added data filter
--                 - full time in log filename
--                 - changed root from 'assist' to dedicated folder
-- mlz - 02jul2013 - oasis-2527 - fix: use manage_prc_msgs_det.handle_t
-- mlz - 20oct2013 - oasis-1384 - filter old data (valuation_date)
-- mlz - 22oct2013 - oasis-1384 - fix: valuation_date can be null
-- mlz - 22oct2013 - oasis-1384 - add info to parent
-- mlz - 31oct2013 - oasis-1384 - consider data from 01jan2009
-- mlz - 05nov2013 - oasis-1384 - fixed logged param value for: START_DATE
-- mlz - 07jul2015 - oasis-5577 - if site_date changed during the execution
--                 - --> perform the next_program_id manually
-- mlz - 14aug2015 - (14-21aug2015, all tagged 14aug2015) 
--                 - oasis-5807 - allow to reload for a past date --> enhanced linux scripts
--                 - along with:
--                 - oasis-4965 - unzip must be done by the linux script
--                 - added the same procs/calls as for the datawarehouse
--                 - call 1:  get_logfile_info
--                 -    writes a linux script (to XMLDIR) to give the paramters: root_dir and logfile_name
--                 - call 2:  start_parent_and_get_filenames
--                 -    writes a linux script (to FDMT_ROOT) to give the paramters: zip_mask, out_filename
--                 - call 3: the load() entry point: added the date_of_msg, plus the p_load_date is now always used as is
-- mlz - 11sep2015 - oasis-5577 - make the START_DATE adjust itself
--                 - well, actually, split into 2 new parameters: START_DAY and START_NB_YEARS
-- mlz - 13sep2015 - oasis-5677 - forbid the job to end between 5:58 and 6:04
--                 - -- actually, it's more the triggering of the next job that must wait for the new day's completion
-- mlz - 15sep2015 - oasis-5974 - fix: unzip mask cannot be applied from the root directory
--                 - --> new param to linux script: zip_mask_root_dir
-- mlz - 15sep2015 - oasis-5974 - fix: if a child fail, the parent must also fail
-- mlz - 01feb2016 - oasis-5807 - fix: added or re-activated the LOAD_DATE_BYPASS parameter
--                 - added validation: load_date cannot be in the future
--
--------------------------------------------------------------------------
--
c_oasis_message      constant varchar2(30) := 'PROC-CSV_TO_MSEF-';
c_product_component  constant varchar2(30) := 'PROC-PRODUCT_COMPONENT-';
c_oasis_message_gen  constant varchar2(30) := 'PROC-GEN-';
--
c_read_mode          constant varchar2(2) := 'R';
c_write_mode         constant varchar2(2) := 'W';
--
c_file_max_line_size constant binary_integer := 32767;
--
c_msg_text_length    constant binary_integer := 68;
c_err_text_length    constant binary_integer := 255;
--
c_file_monthly       constant varchar2(7) := 'Monthly';
c_file_daily         constant varchar2(7) := 'Daily';
c_file_delta         constant varchar2(7) := 'Delta';
c_file_type_monthly  constant msef_daily_fundamentals.file_type%type := 'M';
c_file_type_daily    constant msef_daily_fundamentals.file_type%type := 'D';
c_file_type_delta    constant msef_daily_fundamentals.file_type%type := 'Z';
--
c_load_date_monthly  constant varchar2(7)  := 'YYYY-MM';
c_load_date_daily    constant varchar2(10) := 'YYYY-MM-DD';
--
--mlz - 14aug2015
c_oradir_default     constant varchar2(100) := 'XMLDIR';
--c_root_directory     constant varchar2(9) := 'FDMT_ROOT';
c_oradir_root        constant varchar2(9) := 'FDMT_ROOT';
g_current_oradir     varchar2(30);       
g_current_file       utl_file.file_type;       
c_date_of_msg_format constant varchar2(20) := 'yyyymmdd-hh24miss';
c_load_date_format   constant varchar2(20) := 'dd-mon-yyyy';
--endmlz - 14aug2015
g_logfile_separator    ops_scripts_parms.parm_value%type;
c_logfile_default_sep  constant varchar2(1) := chr(9);

pgmloc               binary_integer;
g_program_id         ops_scripts.program_id%type;
g_program_id_child   ops_scripts.program_id%type;
g_sid                varchar2(20);
--mlz - 14aug2015
--g_cntl_rec           product_component.r_parameters;
g_instance          varchar2(20);
--endmlz - 14aug2015
g_log_all            boolean;
g_log_into_table     boolean;
--mlz - 14aug2015
g_log_name           varchar2(1000);
g_log                utl_file.file_type;
--endmlz - 14aug2015
g_log_det            manage_prc_msgs_det.handle_t;
g_log_det_filename   varchar2(500);

g_start_date         date;
--mlz - 11sep2015
--c_start_date         constant date := to_date('2009-01-01', 'yyyy/mm/dd');
--c_start_date_format  constant varchar2(10) := 'yyyy-mm-dd';
c_start_day          constant varchar2(10) := '01-01';
c_start_day_format   constant varchar2(10) := 'mm-dd';
c_start_nb_years     constant binary_integer := 5;
--endmlz - 11sep2015

--mlz - 13sep2015
c_exclusion_time_start  constant varchar2(4) := '0558';
c_exclusion_time_end    constant varchar2(4) := '0604';
c_exclusion_time_format constant varchar2(6) := 'hh24mi';
--endmlz - 13sep2015

g_region_id          msef_daily_fundamentals.region_id%type;
g_file_type          msef_daily_fundamentals.file_type%type;
g_file_id            msef_daily_fundamentals.file_id%type;
g_current_file_dir   varchar2(1000);
g_user               varchar2(30);

g_load_limit         binary_integer;

g_filter_data        boolean;
g_skip_child         boolean;

--mlz - 14aug2015
--g_out_extension      varchar2(3);
--c_out_extension      constant varchar2(3) := 'csv';
g_out_filename      ops_scripts_parms.parm_value%type; 
c_out_filename      constant varchar2(50) := 'msef_data_$$PGMID$$_$$yyyy-mm-dd$$.csv';
g_current_filename  ops_scripts_parms.parm_value%type;
--endmlz - 14aug2015

--
type parameters
  is record
   ( file_type                msef_daily_fundamentals.file_type%type
   , region_id                msef_daily_fundamentals.region_id%type
   , file_id                  msef_daily_fundamentals.file_id%type
   );
--
--
--
type table_fields_tab_t is table of varchar2(2000) index by binary_integer;
type data_id_tab_t      is table of varchar2(1)    index by binary_integer;


g_separator          varchar2(10);
c_default_separator  constant varchar2(1) := '|';

c_commit_freq_def    constant binary_integer := 100;
g_commit_freq        integer;

c_nbdayskept         constant binary_integer := 35;
g_nbdayskept         binary_integer;

g_pricing_msgs        pricing_msgs%rowtype;
g_pricing_msgs_child1  pricing_msgs%rowtype;
g_at_least_one_child_failed boolean;

  type exec_res_rec_t is record (
    line_handled_error    boolean
  , ins_upd            varchar2(3)
  , field_name         varchar2(30)
  , remark             varchar2(1000)
  );
  g_load_date          date;
  --mlz - 14aug2015
  g_date_of_msg        pricing_msgs.date_of_msg%type;
  --endmlz - 14aug2015

  --mlz - 02jul2013
  --g_lh  manage_prc_msgs_det.handle_t;
  --endmlz - 02jul2013  

--------------------------------------------------------------------------------
handled_error        exception;
fatal_error          exception;
--------------------------------------------------------------------------------
g_files_to_load       binary_integer;
g_files_loaded        binary_integer;
g_files_error         binary_integer;
g_files_already_proc  binary_integer;
g_total_lines_ins     binary_integer;

g_line_read          binary_integer;
g_line_inserted      binary_integer;
g_line_error         binary_integer;
g_line_filtered      binary_integer;

g_histo_deleted      binary_integer;
g_current_deleted    binary_integer;

procedure reset_counters
is
begin

  g_files_to_load       := 0;
  g_files_loaded        := 0;
  g_files_error         := 0;
  g_files_already_proc  := 0;
  g_total_lines_ins     := 0;
  
end;
procedure reset_counters_child
is
begin

  g_line_read          := 0;
  g_line_inserted      := 0;
  g_line_error         := 0;
  g_line_filtered      := 0;
  

  g_histo_deleted      := 0;
  g_current_deleted    := 0;
end;


------------------------------------------------------------------------
procedure upd_prc_msgs( p_do_children in boolean := true )
as --{
--
begin --{
  g_pricing_msgs.value_01 := g_files_to_load;
  g_pricing_msgs.value_02 := g_files_loaded;
  g_pricing_msgs.value_03 := g_files_error;
  g_pricing_msgs.value_04 := g_files_already_proc;
  g_pricing_msgs.value_05 := g_total_lines_ins;

  global_file_updates.update_pricing_msgs( g_pricing_msgs.program_id
                                         , g_pricing_msgs.date_of_msg
                                         , true
                                         , g_pricing_msgs
                                         );
  commit;
  
  if p_do_children then
    g_pricing_msgs_child1.value_01 := g_line_read;
    g_pricing_msgs_child1.value_02 := g_line_inserted;
    g_pricing_msgs_child1.value_03 := g_line_error;
    g_pricing_msgs_child1.value_04 := g_line_filtered;

    g_pricing_msgs_child1.value_14 := g_histo_deleted;
    g_pricing_msgs_child1.value_15 := g_current_deleted;

    global_file_updates.update_pricing_msgs( g_pricing_msgs_child1.program_id
                                           , g_pricing_msgs_child1.date_of_msg
                                           , true
                                           , g_pricing_msgs_child1
                                           );
    commit;
  end if;
end upd_prc_msgs; --}}


--mlz - 14aug2015
-- procedure raise_fatal_error is
-- begin
--   --workaround needed as long as
--   --  dbms_utility.format_error_stack is unable to give the
--   --  correct error line number
--   raise fatal_error;
-- end;
procedure raise_fatal_error(p_do_raise in boolean) is
begin
  --workaround needed as long as
  --  dbms_utility.format_error_stack is unable to give the
  --  correct error line number

  --mlz - 22avr2014 - generalized usage: called also to check boolean-returned statuses
  if p_do_raise = true then
    raise fatal_error;
  end if;
end;
--endmlz - 14aug2015


--------------------------------------------------------------------------------
function get_oasis_msg( p_code in number
                      , p_1    in varchar2 default null
                      , p_2    in varchar2 default null
                      , p_3    in varchar2 default null
                      )
  return varchar2
is
  l_code varchar2(3);
  l_message varchar2(1000);
begin
  l_code := to_char(p_code, 'fm000');
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||l_code
                                   , 'N'
                                   , p_1
                                   , p_2
                                   , p_3
                                   , l_message
                                   );
  return l_message;
exception when others then
  return 'Err in get_msg for code: '||p_code||' //'
    ||dbms_utility.format_error_stack
    ||dbms_utility.format_error_backtrace
    ;
end;


--------------------------------------------------------------------------------
function get_oasis_msg_cmp( p_code in number
                          , p_1    in varchar2 default null
                          , p_2    in varchar2 default null
                          , p_3    in varchar2 default null
                          )
  return varchar2
is
  l_code varchar2(3);
  l_message varchar2(1000);
begin
  l_code := to_char(p_code, 'fm000');
  Manage_Messages.Get_Oasis_Message( c_product_component||l_code
                                   , 'N'
                                   , p_1
                                   , p_2
                                   , p_3
                                   , l_message
                                   );
  return l_message;
exception when others then
  return 'Err in get_msg for code: '||p_code||' //'
    ||dbms_utility.format_error_stack
    ||dbms_utility.format_error_backtrace
    ;
end;
function get_oasis_msg_gen( p_code in number
                          , p_1    in varchar2 default null
                          , p_2    in varchar2 default null
                          , p_3    in varchar2 default null
                          )
  return varchar2
is
  l_code varchar2(3);
  l_message varchar2(1000);
begin
  l_code := to_char(p_code, 'fm000');
  manage_messages.get_oasis_message( c_oasis_message_gen||l_code
                                   , 'N'
                                   , p_1
                                   , p_2
                                   , p_3
                                   , l_message
                                   );
  return l_message;
exception when others then
  return 'Err in get_msg for code: '||p_code||' //'
    ||dbms_utility.format_error_stack
    ||dbms_utility.format_error_backtrace
    ;
end;

--------------------------------------------------------------------------------
function get_error_msg( p_message in varchar2
                      , p_pricing_msgs_rec in out nocopy pricing_msgs%rowtype
                      )
  return varchar2
is
  v_tmp_message varchar2(4000);
begin
  --custom error supposed to be in: p_message
  --but some procedures put it in: pricing_msgs.err_text instead
  if p_message is not null then
    v_tmp_message := substr( p_message, 1, 4000);

  elsif p_pricing_msgs_rec.err_text is not null then
    v_tmp_message := p_pricing_msgs_rec.err_text;

  else
    v_tmp_message := substr( 
      dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      ,1,4000
      )
      ;
  end if;

  if instr(v_tmp_message, 'ORA-') = 0 then
    v_tmp_message := substr(
      v_tmp_message
      ||chr(10)||dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      ,1,4000
      )
      ;
  --else: error stack alreadu there...
  end if;

  return v_tmp_message;
end;


------------------------------------------------------------------------
function get_error_msg_parent
       ( p_message in varchar2
       )
  return varchar2
is
begin
  return get_error_msg( p_message, g_pricing_msgs );
end;


------------------------------------------------------------------------
function get_error_msg_child
       ( p_message in varchar2
       )
  return varchar2
is
begin
  return get_error_msg( p_message, g_pricing_msgs_child1 );
end;





--------------------------------------------------------------------------------


function translate_file_name (
    p_file_name            in  varchar2,
    p_sitedate_char_string in  varchar2,
    p_date_value           in  date,
    p_sysdate_char_string  in varchar2,
    p_pgmid                in number
  )
  return varchar2
as
  l_tmp varchar2(1000);
begin
  l_tmp := manage_file.translate_file_name(P_File_Name,P_SiteDate_Char_String,P_Date_Value,P_Sysdate_Char_String);

  --added some custom replacements
  l_tmp := replace(l_tmp, P_SiteDate_Char_String||'PGMID'||P_SiteDate_Char_String, nvl(p_pgmid, g_program_id));
  l_tmp := replace(l_tmp, P_SiteDate_Char_String||'FT'||P_SiteDate_Char_String, g_file_type);
  l_tmp := replace(l_tmp, P_SiteDate_Char_String||'REG'||P_SiteDate_Char_String, g_region_id);
  --l_tmp := replace(l_tmp, P_SiteDate_Char_String||'FID'||P_SiteDate_Char_String, g_file_id);

  return l_tmp;
end;
--mlz - 14aug2015
--------------------------------------------------------------------------------
function get_directory(p_dirname in varchar2, p_message out nocopy varchar2)
  return varchar2
is
  l_tmp varchar2(1000);
begin

  begin
    select directory_path
    into l_tmp
    from all_directories
    where directory_name = p_dirname
    ;

  exception
  when no_data_found then
    --v_tmp_message := 'Directory ['||p_cntl_rec.dir_name||'] not found!!';
    p_message := get_oasis_msg_gen(220, p_dirname);
    raise_fatal_error(p_do_raise => true);
  when others then
    p_message := replace(dbms_utility.format_error_stack||dbms_utility.format_error_backtrace, chr(10), ' ');
    raise_fatal_error(p_do_raise => true);
  end;

  return l_tmp;
end;
--endmlz - 14aug2015



--------------------------------------------------------------------------------
-- extracted from: product_component.process_beginning
-- to allow earlier log creation
procedure open_log(
  p_log_file_name in varchar2,
  p_directory     in varchar2
  )
is
  l_tmp_message varchar2(4000);
  l_tmp_success boolean;
begin
  Load_Supplier_Rules.Open_File( P_Log_File_Name
                               , p_directory
                               , 'A'
                               , g_pricing_msgs
                               , g_log
                               , l_tmp_success
                               );
  --IF ( NOT l_tmp_success ) THEN raise_fatal_error; END IF;
  raise_fatal_error(p_do_raise => not l_tmp_success);
  
  --mlz - 14aug2015
  -- g_Cntl_Rec.Log_File_Open := TRUE;
  -- Pgmloc := 1530;
  -- Load_Supplier_Rules.Write_File( g_Cntl_Rec.Log_File
  --                               , g_pricing_msgs.Msg_Text
  --                               , g_pricing_msgs
  --                               , l_tmp_success
  --                               );
  -- IF ( NOT l_tmp_success ) THEN raise_fatal_error; END IF;
  -- Pgmloc := 1540;
  -- Manage_Messages.Get_Oasis_Message( 'PROC-PRODUCT_COMPONENT-110'
  --                                  , 'N'
  --                                  , TO_CHAR( sysdate
  --                                           , 'dd-mon-yyyy hh24:mi:ss' )
  --                                  , l_tmp_message
  --                                  );
  -- Pgmloc := 1550;
  -- Load_Supplier_Rules.Write_File( g_Cntl_Rec.Log_File
  --                               , l_tmp_message
  --                               , g_pricing_msgs
  --                               , l_tmp_success
  --                               );
  -- IF ( NOT l_tmp_success ) THEN raise_fatal_error; END IF;
  --endmlz - 14aug2015
end;


procedure write_log(p_msg in varchar2)
is
  l_tmp_success boolean;
begin
  load_supplier_rules.write_file( g_log
                                , to_char(sysdate, 'yyyy/mm/dd hh24:mi:ss')||' - '||
                                  replace(p_msg, chr(10), ' ')
                                , g_pricing_msgs
                                , l_tmp_success
                                );
  --if not v_tmp_success then raise_fatal_error; end if;
  raise_fatal_error(p_do_raise => not l_tmp_success);
end;


procedure write_log_progress(p_msg in varchar2)
is
begin
  write_log( replace(p_msg, chr(10), ' ') );

  g_pricing_msgs.msg_text_2 := substr(p_msg, 1, 64);
  upd_prc_msgs(false);
end;


--------------------------------------------------------------------------------
--procedure write_log_det( p_line_nr in binary_integer
--                       , p_msg     in varchar2
--                       )
--is
----
--  v_line_skipped varchar2(1);
--  v_line_handled_error   varchar2(1);
--
--  v_tmp varchar2(32767);
--
--  v_success boolean;
--begin
--
----    if g_log_all = true
----      or
----      ( p_line_skipped = true
----        and p_exec_res_rec.line_handled_error = true
----      )
----    then
--
--      --
--      -- log all children under the parent
--      -->> distinct them by using program_id as context
--      --
--      manage_prc_msgs_det.write_log(
--        g_log_det,
--        --
--        replace(p_msg, chr(10), ' '),
--        g_pricing_msgs_child1.program_id, --context
--        null,
--        p_line_nr
--        );
--
----    end if;
--
--
--
--end;
procedure write_log_det( p_msg in varchar2
                       )
is
begin

  manage_prc_msgs_det.write_log(  g_log_det,
    g_program_id_child      ||g_logfile_separator||
    g_file_type             ||g_logfile_separator||
    g_region_id             ||g_logfile_separator||
    g_file_id               ||g_logfile_separator||
    g_line_read             ||g_logfile_separator||
    replace(p_msg, chr(10), ' ')
    );

end;


procedure write_log_det_init(
  p_err_message     out nocopy varchar2
  )
is
  v_tmp_success       boolean;

  l_filename          varchar2(100);
  v_pricing_msgs      pricing_msgs%rowtype;
  
  l_log_det           utl_file.file_type;
begin

  --
  -- 1. parameter: DET_LOG
  --
  declare
    l_parm ops_scripts_parms.parm_name%type := 'DET_LOG';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);

    if l_tmp is null
    then
      --no log filename --> log into table instead
      g_log_into_table := true;
    else
      g_log_into_table := false;
      l_filename := translate_file_name(l_tmp,'$$',sysdate,null,g_program_id_child);
      --mlz - 06jul2013
      g_log_det_filename := l_filename;
      --endmlz - 06jul2013
    end if;
  end;



  if not g_log_into_table then
    --
    -- 2. open det log file
    --
    l_log_det := null;
    load_supplier_rules.open_file( l_filename
                                 --mlz - 06jul2013
                                 --, c_oradir_name
                                 , c_oradir_root
                                 --endmlz - 06jul2013
                                 , 'W'
                                 , c_file_max_line_size
                                 , v_pricing_msgs
                                 , l_log_det
                                 , v_tmp_success
                                 );
    if ( not v_tmp_success ) then
      p_err_message := v_pricing_msgs.err_text;
      --raise_fatal_error;
      raise_fatal_error(p_do_raise => true);
    end if;

    --
    -- get log handle for file
    --
    g_log_det := manage_prc_msgs_det.get_log_to_file_handle(
      l_log_det,
      g_logfile_separator
      );


    manage_prc_msgs_det.write_log( g_log_det,
      'CHILD_PGMID'
      ||g_logfile_separator||
      'FILE_TYPE'
      ||g_logfile_separator||
      'REGION_ID'
      ||g_logfile_separator||
      'FILE_ID'
      ||g_logfile_separator||
      'LINE_NR'
      ||g_logfile_separator||
      'MSG'
      );
  else
    --
    -- get log handle for table
    --
    g_log_det := manage_prc_msgs_det.get_log_to_table_handle(
      g_pricing_msgs.date_of_msg,
      g_pricing_msgs.program_id,
      --g_pricing_msgs.last_user
      g_user
      );
  end if;


--exception when others then
--  v_tmp_message := get_error_msg(v_tmp_message);
--  raise_fatal_error;
end;


--------------------------------------------------------------------------------
function translate_file_id( p_file_id in varchar2)
  return varchar2
is
  l_tmp varchar2(100);
begin
  --
  -- if p_file_id from package_name side (real file name, tms parms, ...)
  -- >> eg. 'OperationRatiosAOR' and 'OperationRatiosRestate' 
  --        are both: OperationRatios
  --
  l_tmp := replace(p_file_id, 'AOR', '');
  l_tmp := replace(l_tmp, 'Restate', '');
  
  --
  -- if p_file_id from reference side (field list, sub-package name)
  -- >> eg. 'EarningReports (AOR, Restate)' must become EarningReports 
  --
  l_tmp := replace(l_tmp, '(AOR, Restate)', '');
  l_tmp := trim(l_tmp);
  
  --
  -- should be comparable now...
  --
  return l_tmp;
end;


--------------------------------------------------------------------------------
--mlz - 22oct2013
--procedure set_remarks(p_msg in varchar2)
procedure set_remarks(p_msg in varchar2, p_parent in boolean, p_pricing_msgs in out nocopy pricing_msgs%rowtype)
--endmlz - 22oct2013
is
  l_old pricing_msgs.remarks%type;
  l_prefix varchar2(100);
begin
  --mlz - 22oct2013
  if p_parent then
    l_prefix := '('||g_file_type||'/'||g_region_id||')';
  else
    l_prefix := '('||g_file_type||'/'||g_region_id||'/'||g_file_id||')';
  end if;
  --endmlz - 22oct2013
  
  --keep old (if any)
  l_old := p_pricing_msgs.remarks;
  if l_old is not null then
    --remove default prefix
    l_old := replace(l_old, l_prefix||' ', '');
    l_old := trim(l_old);
  end if;
    
  --if default prefix string changes --> check also in the exception block!!!
  p_pricing_msgs.remarks := l_prefix;

  if l_old is not null then
    --keep old remark
    p_pricing_msgs.remarks := p_pricing_msgs.remarks||' '||l_old;
    if p_msg is not null then
      p_pricing_msgs.remarks := substr(p_pricing_msgs.remarks||' /',1,255);
    end if;
  end if;
  --append new
  p_pricing_msgs.remarks := substr(p_pricing_msgs.remarks||' '||p_msg,1,255);
end;
--mlz - 22oct2013
procedure set_parent_remarks(p_msg in varchar2)
is
fct varchar2(100) := 'set_parent_remarks';
begin
  set_remarks(p_msg, true, g_pricing_msgs);
end;
procedure set_child_remarks(p_msg in varchar2)
is
fct varchar2(100) := 'set_child_remarks';
begin
  set_remarks(p_msg, false, g_pricing_msgs_child1);
end;
--endmlz - 22oct2013


--------------------------------------------------------------------------------
-- local version from load_supplier_rules
-- >> detect file_not_found
-- >> not an error
PROCEDURE Open_File(
          P_File_Name     IN     Supplier_Running_Parms.File_Name%TYPE
        , P_Dir_Name      IN     VARCHAR2
        , P_Mode          IN     VARCHAR2
        , P_Reclen        IN     binary_integer
        , Pricing_Msg_rec IN OUT nocopy Pricing_Msgs%ROWTYPE
        , P_File_handle   OUT    Utl_File.File_Type
        , Success         OUT    BOOLEAN
        ) IS
--{
  C_Oasis_Message    VARCHAR2(30) := 'PROC-LOAD_SUPPLIER_RULES-';
BEGIN
  --{
  Success := FALSE;
  Pgmloc := 1150;
  P_File_Handle := UTL_FILE.FOPEN( P_Dir_Name
                                 , P_File_Name
                                 , P_Mode
                                 , NVL( P_Reclen, 1024 )
                                 );
  Success := TRUE;
--
EXCEPTION
  WHEN UTL_FILE.INVALID_PATH
  THEN
    Manage_Messages.Get_Oasis_Message( 'PROC-GEN-005'
                                     , 'N'
                                     , Pricing_Msg_Rec.Err_Text
                                     );
  WHEN UTL_FILE.INVALID_MODE
  THEN
    Manage_Messages.Get_Oasis_Message( 'PROC-GEN-010'
                                     , 'N'
                                     , Pricing_Msg_Rec.Err_Text
                                     );
  WHEN UTL_FILE.INVALID_OPERATION
  THEN
    Manage_Messages.Get_Oasis_Message( 'PROC-GEN-025'
                                     , 'N'
                                     , Pricing_Msg_Rec.remarks --<<< remarks!!!
                                     );
    raise handled_error; -->> not an error

  WHEN UTL_FILE.INVALID_FILEHANDLE
  THEN
    Manage_Messages.Get_Oasis_Message( 'PROC-GEN-045'
                                     , 'N'
                                     , Pricing_Msg_Rec.Err_Text
                                     );
  WHEN OTHERS
  THEN
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'010'
                                     , 'N'
                                     , TO_CHAR( SQLCODE )
                                     , 'Open_File'
                                     , Pricing_Msg_Rec.Err_Text
                                     );
  --
END Open_File; --}}


--------------------------------------------------------------------------------
--mlz - 14aug2015
procedure get_parm_log
is
begin
  --
  -- parameter: LOG
  --
  declare
    l_parm ops_scripts_parms.parm_name%type := 'LOG';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);

    if l_tmp is null
    then
      --mlz - 11jun2012 - set a default name - log MUST always exists
      g_log_name := 'mstar-'||g_program_id||'_'||to_char(g_date_of_msg, 'yyyymmdd-hh24miss')||'.log';
      --endmlz - 11jun2012
    else
      g_log_name := translate_file_name(l_tmp,'$$',g_date_of_msg,null,null);
    end if;
  end;
end;
--endmlz - 14aug2015




--------------------------------------------------------------------------------
-- step 1 -
-- return the workdir and the logfile name as a linux script
-- For this call only, the output file (the script) will be in the common dir
--   (the only directory that is known in advance by the calling script)
--
procedure get_logfile_info
       ( p_program_id in ops_scripts.program_id%type
       , p_date_of_msg in varchar2
       --vv
       , p_outscript_filename in varchar2
       --^^
       , p_message       out nocopy varchar2
       )
is
  l_fh utl_file.file_type;
  l_root_dir varchar2(1000);
  l_logfile_name varchar2(100);

begin

  --param 1
  l_root_dir := get_directory(c_oradir_root, p_message);

  --param 2
  g_date_of_msg := to_date(p_date_of_msg, c_date_of_msg_format);  --logfile date = pricing_msgs.date_of_msg
  g_program_id := p_program_id;
  get_parm_log;


  --1st file produced in common directory ($DASSIST)
  l_fh := utl_file.fopen(c_oradir_default, p_outscript_filename, 'W', c_file_max_line_size);

  utl_file.put_line(l_fh, 'export root_dir='||l_root_dir);
  utl_file.put_line(l_fh, 'export logfile_name='||g_log_name);

  utl_file.fclose(l_fh);
end;



--------------------------------------------------------------------------------
procedure common_beginning(
  p_program_id  in ops_scripts.program_id%type,
  p_load_date   in date,
  p_date_of_msg in date,
  p_message     in out nocopy varchar2
) 
is

  cursor cur_prcmsgs_exi is
    select *
    from pricing_msgs pm
    where pm.program_id = g_program_id
    and pm.date_of_msg  = g_date_of_msg
    ;
  r_prcmsgs  cur_prcmsgs_exi%rowtype;


  --level 1 ....................................................................
  procedure check_pgmid_running( p_pgmid in ops_scripts.program_id%type )
  is
    l_allow_concurrent_run boolean;
    l_tab_pgmid  tt_varchar2_100 := tt_varchar2_100();
    
    cursor cur_prcmsgs is
      select *
      from pricing_msgs pm
      where 1=1
      and pm.program_id in (select to_number(column_value) from table(l_tab_pgmid))
      and pm.date_of_msg <> g_date_of_msg --exclude the line of the current job (=me)
      and successfull_flag = 'N'
      ;
    r_prcmsgs  cur_prcmsgs%rowtype;

    --level 2 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .    
    procedure get_parm_ALLOW_CONCUR_RUN
    is
      l_parm ops_scripts_parms.parm_name%type := 'ALLOW_CONCUR_RUN';
      l_tmp ops_scripts_parms.parm_value%type;
    begin
      l_tmp :=  upper( scripts.get_arg_value( g_program_id, l_parm ) );
      if l_tmp in ('Y','YES') then
        l_allow_concurrent_run := true;
      else
        l_allow_concurrent_run := false;
      end if;
      write_log('['||l_parm||'] --> '||case l_allow_concurrent_run when true then 'true' else 'false' end);
    end;
    
    --level 2 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    procedure get_parm_EXCL_LST_PGMID  
    is
      l_parm ops_scripts_parms.parm_name%type := 'EXCL_LST_PGMID';
      l_tmp binary_integer;
      
      cursor cur_pgm is
        select parm_value
        from ops_scripts_parms
        where program_id = g_program_id
        and parm_name like l_parm||'%'
        ;
    begin
      open cur_pgm;
      fetch cur_pgm bulk collect into l_tab_pgmid;
      close cur_pgm;
      
      if l_tab_pgmid.count = 0
      then
        write_log('No related job to exclude (no parameter: ['||l_parm||'%] found) --> continue'); 
      else
        --just validate a bit the list (to avoid the query to fail)
        for i in 1..l_tab_pgmid.count loop
          begin
            l_tmp := to_number(l_tab_pgmid(i));
            write_log('['||l_parm||'%] is ['||l_tab_pgmid(i)||']');
          exception when others then
            p_message := 'One value specified for the parameter ['||l_parm||'%] is invalid: ['||l_tab_pgmid(i)||'] // '
              ||dbms_utility.format_error_stack||dbms_utility.format_error_backtrace;
            write_log(p_message);
            raise_fatal_error(p_do_raise => true);
          end;
        end loop;
      end if;

      write_log('['||l_parm||'] is ['||l_tmp||']');
    end;
    --back to level 1...........................................................
  begin
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
write_log('@@ check_pgmid_running');
--$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    get_parm_ALLOW_CONCUR_RUN;  --for debugging/dvlp purposes
    
    if not l_allow_concurrent_run then
      if p_pgmid is null then
        --no pgmid passed --> get them (0 --> n) from the parameters 
        get_parm_EXCL_LST_PGMID;
      else
        l_tab_pgmid.extend(1);
        l_tab_pgmid(l_tab_pgmid.last) := p_pgmid;
      end if;
      
      open cur_prcmsgs;
      fetch cur_prcmsgs into r_prcmsgs;
      if cur_prcmsgs%found then
        close cur_prcmsgs;
        if p_pgmid = g_program_id then
          p_message := 'Cannot run, an instance of the same job is already/still running (date_of_msg ['||to_char(r_prcmsgs.date_of_msg, 'yyyy/mm/dd hh24:mi:ss')||'] ) --> stop!';
        else
          p_message := 'Cannot run, at least one another related job ['||r_prcmsgs.program_id||'] is already/still running (date_of_msg ['||to_char(r_prcmsgs.date_of_msg, 'yyyy/mm/dd hh24:mi:ss')||'] ) --> stop!';
        end if;
        write_log(p_message);
        raise_fatal_error( p_do_raise => true );
      end if;
      close cur_prcmsgs;
    end if;
  end;

  --level 1 ....................................................................
  procedure get_parm_out_filename
  is
    l_parm ops_scripts_parms.parm_name%type := 'OUT_FILENAME';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);
    if l_tmp is null then
      write_log(get_oasis_msg_gen(214, l_parm, c_out_filename)); --Parameter [%%%%%] not found --> using default: %%%%%
      l_tmp := c_out_filename;
    else
      write_log('Parameter ['||l_parm||'] is ['||l_tmp||']');
    end if;
    
    g_out_filename := translate_file_name(l_tmp, '$$', p_load_date, null, null);
    write_log('Parameter ['||l_parm||'] is ['||g_out_filename||'] (translated)');
  end;

  --level 1 ....................................................................
  procedure get_parm_commit_freq
  is
  fct varchar2(100) := 'get_parm_commit_freq';
    l_parm ops_scripts_parms.parm_name%type := 'COMMIT_FREQ';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);
    if l_tmp is null then
      write_log(get_oasis_msg_gen(214, l_parm, c_commit_freq_def)); --Parameter [%%%%%] not found --> using default: %%%%%
      g_commit_freq := c_commit_freq_def;
    else
      write_log('Parameter ['||l_parm||'] is ['||l_tmp||']');
      g_commit_freq := to_number(l_tmp);
    end if;

  exception when others then
    write_log( get_oasis_msg_gen(218, l_parm, l_tmp, c_commit_freq_def) ); --Parameter [%%%%%] has an invalid value [%%%%%] --> using default: %%%%%
    g_commit_freq := c_commit_freq_def;
  end;

  --level 1 ....................................................................
  procedure get_parm_file_type
  is
    l_parm ops_scripts_parms.parm_name%type := 'FILE_TYPE';
    tmp ops_scripts_parms.parm_value%type;
  begin
    tmp := scripts.get_arg_value( p_program_id, l_parm);
    if tmp is null then

      p_message := get_oasis_msg_gen(210, l_parm); --Parameter [%%%%%] not found (ops_scripts/_parms)
      --raise_fatal_error;
      raise_fatal_error(p_do_raise => true);
    end if;

    g_file_type := initcap(tmp); --First upper/rest lower
  end;

  --level 1 ....................................................................
  procedure get_parm_region_id
  is
    l_parm ops_scripts_parms.parm_name%type := 'REGION_ID';
    tmp ops_scripts_parms.parm_value%type;
  begin
    tmp := scripts.get_arg_value( p_program_id, l_parm);
    if tmp is null then
      p_message := get_oasis_msg_gen(210, l_parm); --Parameter [%%%%%] not found (ops_scripts/_parms)
      --raise_fatal_error;
      raise_fatal_error(p_do_raise => true);
    end if;

    g_region_id := upper(tmp); --all upper
  end;

  --level 1 ....................................................................
  procedure get_parm_load_date_bypass
  is
    l_parm ops_scripts_parms.parm_name%type := 'LOAD_DATE_BYPASS';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);
    if l_tmp is not null then
      begin
        g_load_date := to_date(l_tmp, 'yyyymmdd');
      exception when others then
        p_message := get_oasis_msg_gen(219, l_parm); --Parameter [%%%%%] has an invalid value [%%%%%]
        raise_fatal_error(p_do_raise => true);
      end;
      write_log('Param ['||l_parm||'] defined --> load_date is overriden by ['||g_load_date||'] ***!!');
   
    else
      write_log('Param ['||l_parm||'] NOT defined --> load_date stays ['||g_load_date||']');
      --bypass null or indefined
      -->> leave as is
    end if;
   
  end;

--level 0 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -  
begin

  p_message       :=  null;
  g_user          :=  constants.get_user_id;
  --
  g_pricing_msgs := null;
  --g_cntl_rec       := null;
  g_program_id     := p_program_id;
  g_program_id_child := null;
  
  --g_file_read := 0;
  
  
  if p_load_date is null then
    g_load_date := constants.get_site_date;
  else
    g_load_date := p_load_date;
  end if;

  
  g_date_of_msg := p_date_of_msg;

  --
  -- LOG
  --
  get_parm_log;

  --g_cntl_rec.open_files    := false;
  --g_cntl_rec.dir_name      := c_oradir_name;

  open_log(g_log_name, c_oradir_root);

  select SYS_CONTEXT('USERENV','SID'), sys_context('USERENV','instance')
  into g_sid, g_instance
  from dual
  ;
  write_log('SID ['||g_sid||'], INSTANCE['||g_instance||']');

  
  --load date override?
  --mlz - 01feb2016
  get_parm_load_date_bypass;
  if g_load_date > trunc(sysdate) then
    p_message := 'The load date cannot be in the future --> stop!';
    raise_fatal_error( p_do_raise => true );
  end if;
  --endmlz - 01feb2016

  --if p_commit_freq is null then
  --  g_commit_freq := c_commit_freq_def;
  --else
  --  g_commit_freq := p_commit_freq;
  --end if;
  --write_log('Commit freq is ['||g_commit_freq||']');
  --
  -- COMMIT_FREQ
  --
  get_parm_commit_freq;
  
  
  
  
  

  
  --
  -- remains of the old process_beginning()
  -- .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
  if g_date_of_msg is not null then
    --check if a line for that date does not already exists
    open cur_prcmsgs_exi;
    fetch cur_prcmsgs_exi into g_pricing_msgs;
    if cur_prcmsgs_exi%notfound then
      close cur_prcmsgs_exi;
      
      --the date can be specified even if the line does not exist! And yes this makes sense
      --because the very 1st operation (a job should do) is the creation of the log file
      --  (that contains that date in its name)
      --and the date of the log and the date_of_msg must match!!

      global_file_updates.Initialize_Pricing_Msgs( g_program_id
                                                 , g_date_of_msg
                                                 , g_load_date
                                                 , true  --upd break_points
                                                 , g_pricing_msgs
                                                 );
    else
      close cur_prcmsgs_exi;
      
      if g_pricing_msgs.successfull_flag <> 'N' then
        p_message := 'Pricing_msgs status ['||g_pricing_msgs.successfull_flag||'] is not [N]';
        raise_fatal_error( p_do_raise => true );
      end if;
      
      if g_pricing_msgs.date_of_prices <> g_load_date then
        p_message := 'Pricing_msgs price_date ['||to_char(g_pricing_msgs.date_of_prices, 'yyyy/mm/dd')||'] is not ['||to_char(g_load_date, 'yyyy/mm/dd')||']';
        raise_fatal_error( p_do_raise => true );
      end if;
    end if;
    
    
  else
    g_date_of_msg := sysdate;
    global_file_updates.Initialize_Pricing_Msgs( g_program_id
                                               , g_date_of_msg
                                               , g_load_date
                                               , true  --upd break_points
                                               , g_pricing_msgs
                                               );
  end if;

    
  g_pricing_msgs.successfull_flag := 'N';
  g_pricing_msgs.msg_text         := substr('Running // log ['||g_log_name||']',1,68);
  g_pricing_msgs.msg_text_2       := 'Program in progress';
  g_pricing_msgs.value_01         := 0;
  g_pricing_msgs.value_02         := 0;
  g_pricing_msgs.value_03         := 0;
  g_pricing_msgs.value_04         := 0;
  g_pricing_msgs.value_05         := 0;
  --
  global_file_updates.Update_Pricing_Msgs( g_program_id
                                         , g_pricing_msgs.date_of_msg
                                         , false  --upd break_points
                                         , g_pricing_msgs
                                         );

  update break_points
  set status      = 'PROG'
    , information = to_char( g_pricing_msgs.date_of_msg, 'dd-mon-yyyy hh24:mi:ss' )
    , nb_records  = g_pricing_msgs.value_01
    , last_user   = constants.get_user_id
    , last_stamp  = sysdate
  where program_id  = g_program_id
  ;
  -- .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .
  -- >>> end of the old process_beginning()
  --
  
  
  
  check_pgmid_running(g_program_id); --1. check if another instance in running
  check_pgmid_running(null);         --2. check if another related job in running (monthly vs daily, ...)


  

  --
  -- OUT_FILENAME
  --
  get_parm_OUT_FILENAME;


  --PARM
  get_parm_file_type;

  --PARM
  get_parm_region_id;


  g_current_file_dir := get_directory(c_oradir_root, p_message);




  
end;


--------------------------------------------------------------------------------
-- step 2 - starts the job (pricing_msgs) 
-- write the list of files to process (based on children)
-- and write parameters needed by the linux script into a file specified
procedure start_parent_and_get_filenames
        ( p_program_id    in ops_scripts.program_id%type
        , p_date_of_msg   in varchar2
        --vv
        , p_load_date     in varchar2
        , p_outscript_filename  in varchar2
        --^^
        , p_message       out nocopy varchar2
        ) 
is
  l_tmp_message       varchar2(4000);
  l_tmp_success       boolean;
  
  l_zipmask ops_scripts_parms.parm_value%type;
  l_zipmask_root_dir varchar2(1024);
  
  l_fh utl_file.file_type;
  l_tmp varchar2(1000);

  --level 1 ....................................................................
  procedure get_parm_ZIP_MASK
  is
    l_parm ops_scripts_parms.parm_name%type := 'ZIP_MASK';
    l_tmp ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);
    if l_tmp is null
    then
      l_tmp_message := get_oasis_msg_gen(210, l_parm); --Parameter [%%%%%] not found (ops_scripts/_parms)
      raise_fatal_error(p_do_raise => true);
    else
      l_zipmask := translate_file_name(l_tmp,'$$',p_load_date,null,null);
    end if;
  
    write_log('['||l_parm||'] is ['||l_zipmask||']');
  end;
  
begin

  p_message       :=  null;
  

  
  common_beginning(
    p_program_id, 
    to_date(p_load_date, c_load_date_format),
    to_date(p_date_of_msg, c_date_of_msg_format),
    l_tmp_message
    );



  --
  -- 1. write the filenames (to process) to a file
  --
  --write_log('Writing file list into ['||g_list_filename||']');
  --l_fh := utl_file.fopen(c_oradir_root, g_list_filename, 'W', c_file_max_line_size);
  --
  --for i in nvl(g_filename_tab.first, 0)..nvl(g_filename_tab.last,-1)
  --loop
  --  l_tmp := g_filename_tab(i);
  --  utl_file.put_line(l_fh, l_tmp, autoflush => true);
  --end loop;
  --
  --utl_file.fclose(l_fh);
  
  --no file list here, instead: use a mask to get the files
  
  --
  -- ZIP_MASK
  --
  get_parm_ZIP_MASK;

  --mlz - 15sep2015 - root directory needed to apply the zip mask
  --                - a bit quick and dirty here...
  l_zipmask_root_dir := g_current_file_dir||'/'||upper(g_file_type)||'/'||upper(g_region_id);
  --endmlz - 15sep2015


  --
  -- 2. write the paramters as a linux script
  --
  l_fh := utl_file.fopen(c_oradir_root, p_outscript_filename, 'W', c_file_max_line_size);

  utl_file.put_line(l_fh, 'export zip_mask='||l_zipmask);
  
  --mlz - 15sep2015 - needed to apply the zip_mask from the right place
  utl_file.put_line(l_fh, 'export zip_mask_root_dir='||l_zipmask_root_dir);
  --endmlz - 15sep2015

  --target filename to use when unzipping
  -- if there are many files inside the archive --> all will be extracted to that same file 
  utl_file.put_line(l_fh, 'export out_filename='||g_out_filename); 

  utl_file.fclose(l_fh);
  

  upd_prc_msgs(false); --parent only
  
  --mlz - 14aug2015
  --p_listfile_name := g_list_filename;
  if utl_file.is_open(g_log) then
    utl_file.fclose(g_log);
  end if;
  --endmlz - 14aug2015
exception
when others then
  --get msg FIRST to not loose stack
  l_tmp_message := get_error_msg_parent(l_tmp_message);

  g_pricing_msgs_child1.err_text := get_oasis_msg_gen(205);
  g_pricing_msgs.err_text := substr( l_tmp_message, 1, 255 );

  g_pricing_msgs.Msg_Text := get_oasis_msg_gen( 204 );
  --mlz - 14aug2015
  if g_log_name is not null then
    g_pricing_msgs.Msg_Text_2 := substr(get_oasis_msg_gen( 299, g_log_name ),1,68);
  else
  --endmlz - 14aug2015
    g_pricing_msgs.Msg_Text_2 := get_oasis_msg_gen( 206 );
  end if;


  if g_pricing_msgs.program_id is not null then
    upd_prc_msgs;
  end if;


  if ( utl_file.is_open( g_log ) )
  then
    begin
      write_log(l_tmp_message);
    exception when others then
      l_tmp_message := l_tmp_message
        ||' // '||dbms_utility.format_error_stack
        ||dbms_utility.format_error_backtrace
        ;
    end;
  end if;

  --mlz - 14aug2015 - no limit anymore
  --p_message := substr(l_tmp_message, 1, 255); --limit in load_supplier(256), in script clt_svr_ld_prc.sql: 255 !!!
  p_message := l_tmp_message;
  --endmlz - 14aug2015

  utl_file.fclose_all;
  
  raise;

end;





procedure cre_prc_msgs_child(p_do_commit in boolean := false)
is
begin

  global_file_updates.initialize_pricing_msgs(
      g_program_id_child
    , g_pricing_msgs.date_of_msg
    , g_pricing_msgs.date_of_prices
    , false
    , g_pricing_msgs_child1
    );


  if p_do_commit then
  --endmlz - 22oct2012
    upd_prc_msgs;
  end if;

end;
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- local version of product_component.process_beginning
-- >> only init tables, skip the 'file' part
--PROCEDURE Process_Beginning_child
--        ( P_Program_Id     IN            Ops_Scripts.Program_Id%TYPE
--        , p_parent_date    in            pricing_msgs.date_of_msg%type
--        , P_Load_Date      IN            pricing_msgs.date_of_prices%type
--        , P_Commit_Freq    IN            integer
--        , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
--        , P_Success           OUT        BOOLEAN
--        , P_Message           OUT        VARCHAR2
--        )
--IS --{
----
----
--V_Load_Date          DATE;
--V_On_Server          BOOLEAN := CONSTANTS.On_Server;
--Success              BOOLEAN;
--V_Message            VARCHAR2(256);
--V_Program_Id         ops_scripts.program_id%type;
---- Ajout - JSL - 16 decembre 2010
--V_Date               DATE;
---- Fin d'ajout - JSL - 16 decembre 2010
----
--V_Run_Parms          VARCHAR2(1);
----
--CURSOR Get_Run_Parms IS
--SELECT 'P'
--  FROM Product_Running_Parms PRP
-- WHERE PRP.Program_Id = V_Program_Id
--UNION
--SELECT 'S'
--  FROM Supplier_Running_Parms SRP
-- WHERE SRP. Program_ID = V_Program_Id;
----
--BEGIN --{
--  Pgmloc := 1160;
--  V_Load_Date      := NVL( P_Load_Date, Constants.Get_Site_Date );
--
--  v_date           := p_parent_date; --to easily match parent-child msg
--
--  -- Les scripts appelants sur Linux n'inserent pas les enfants.
--  -- Faire comme si nous n'etions pas sur le serveur et inserer ces messages.
--  IF P_Program_Id > 9999
--  THEN
--    V_On_Server := FALSE;
--  END IF;
--  IF V_On_Server
--  THEN --{
--    BEGIN --{
--      SELECT *
--        INTO Pricing_Msgs_Rec
--        FROM Pricing_Msgs PM
--       WHERE PM.Program_Id       = P_Program_Id
--         AND PM.Date_of_Prices   = V_Load_Date
--         AND PM.Successfull_Flag = 'N'
--         AND PM.Terminal_Id      = NVL( Constants.Get_Terminal_Id, 'N/A' )
--         AND PM.Date_Of_Msg      = (
--             SELECT MAX( PM1.Date_Of_Msg )
--               FROM Pricing_Msgs PM1
--              WHERE PM.Program_Id     = PM1.Program_Id
--                AND PM.Date_Of_Prices = PM1.Date_Of_Prices
--                AND PM.Terminal_Id    = PM1.Terminal_Id
--             );
--    EXCEPTION
--    WHEN NO_DATA_FOUND THEN
--      --V_Date := SYSDATE;
--      Load_Supplier_Rules.Initialize_Prc_Msgs( P_Program_Id
--                                             , V_Date
--                                             , V_Load_Date
--                                             , Pricing_Msgs_Rec
--                                             , Success
--                                             );
---- Modification - JSL - 16 decembre 2010
----   IF ( NOT SUCCESS ) THEN GOTO Get_Out; END IF;
--      IF SUCCESS
--      THEN --{
--        NULL;
--      ELSE --}{
--        IF V_Load_Date = Constants.Get_Site_Date
--        THEN --}
--          GOTO Get_Out;
--        ELSE --}{
--          V_Date := V_Date - 1 / (24*60*60);
--          Load_Supplier_Rules.Initialize_Prc_Msgs( P_Program_Id
--                                                 , V_Date
--                                                 , V_Load_Date
--                                                 , Pricing_Msgs_Rec
--                                                 , Success
--                                                 );
--          IF NOT SUCCESS THEN GOTO Get_Out; END IF;
--        END IF; --}
--      END IF; --}
---- Fin de modification - JSL - 16 decembre 2010
--    END; --}
--  ELSE --}{
--    ----------------------------------------
--    -- Initalization of the pricing messages
--    ----------------------------------------
--    Load_Supplier_Rules.Initialize_Prc_Msgs( P_Program_Id
--                                           --, SYSDATE
--                                           , v_date  --use parent date
--                                           , V_Load_Date
--                                           , Pricing_Msgs_Rec
--                                           , Success
--                                           );
--    IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
--  END IF; --}
--  Pgmloc := 1170;
--  Pricing_Msgs_Rec.Successfull_Flag := 'N';
--  Pricing_Msgs_Rec.Msg_Text         := 'Program id is '
--                                    || to_char(P_Program_id);
--  Pricing_Msgs_Rec.Msg_Text_2       := 'Program in progress';
--  Pricing_Msgs_Rec.Value_01         := 0;
--  Pricing_Msgs_Rec.Value_02         := 0;
--  Pricing_Msgs_Rec.Value_03         := 0;
--  Pricing_Msgs_Rec.Value_04         := 0;
--  Pricing_Msgs_Rec.Value_05         := 0;
--  --
--  Pgmloc := 1180;
--  Load_Supplier_Rules.Update_Prc_Msgs( P_Program_Id
--                                     , Pricing_Msgs_Rec.Date_of_Msg
--                                     , Pricing_Msgs_Rec
--                                     , Success
--                                     );
--  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
--  --
--  Pgmloc := 1190;
--  UPDATE Break_Points
--     SET Status      = 'PROG'
--       , Information = TO_CHAR( Pricing_Msgs_Rec.Date_of_Msg
--                              , 'dd-mon-yyyy hh24:mi:ss' )
--       , Nb_Records  = Pricing_Msgs_Rec.Value_01
--       , Last_User   = g_user
--       , Last_Stamp  = SYSDATE
--  WHERE  Program_Id  = P_Program_Id;
--  --
--<< Get_Out >>
--  --
--  P_Success := Success;
--  --
--EXCEPTION
--WHEN OTHERS THEN
--  P_Success := FALSE;
--  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'100'
--                                   , 'N'
--                                   , Pgmloc || ' ' ||TO_CHAR( SQLCODE )
--                                   , $$PLSQL_UNIT
--                                   , P_Message
--                                   );
--end;


--------------------------------------------------------------------------------


--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
procedure load_one_file
        ( p_delivery_date  in     msef_daily_fundamentals.delivery_date%type
        , p_region_id      in msef_daily_fundamentals.region_id%type
        , p_file_id        in msef_daily_fundamentals.file_id%type
        , p_file_type      in msef_daily_fundamentals.file_type%type
        --, p_cntl_rec       in out nocopy product_component.r_parameters
        --, p_pricing_msgs_rec in out nocopy pricing_msgs%rowtype
        , p_success           out boolean
        )
as --{
fct varchar2(100) := 'load_one_file';
--
  c_record_length constant binary_integer := 32767;

  l_tmp_message varchar2(4000);
  
  l_current_line_nr   binary_integer;


  l_fields            table_fields_tab_t;
  l_all_null          boolean;

  l_mdf_rec           msef_daily_fundamentals%rowtype;
  w_record            varchar2( 32767 );
  eof                 boolean;

  l_mod               binary_integer;
--  type tab_t   is table of msef_daily_fundamentals%rowtype;
--  tab                 tab_t := tab_t();
  
  l_field_nr_of_data_id   binary_integer;
  l_field_value           msef_daily_fundamentals.field_02%type;
  l_skipped_by_filter     boolean;
  
--  cursor cur_file_id is
--    select /*+ cache */ 
--      field_number
--    from msef_fundamentals_oasis_fields
--    where file_name = replace(replace(g_file_id, 'AOR',''), 'Restate','')
--    AND active_flag = 'Y'
--    ;
--  oasis_fields_tab  oasis_fields_tab_t;

  cursor cur_data_id is
    select data_id
    from msef_ff_sub_field_dest
    where file_id = g_file_id
    AND active_flag = 'Y'
    ;
  l_data_id_tab  data_id_tab_t;
  
  --mlz - 20oct2013
  l_field_nr_of_valuation_dt  binary_integer;
  l_tmp_dt                    date;
  --endmlz - 20oct2013
  
  --internal - level 1 .........................................................
  procedure write_log_det2(
    p_message in varchar2
    )
  is
  begin
    write_log_det(
      'DATA'
        ||g_logfile_separator||
      to_char(sysdate, 'yyyy/mm/dd hh24:mi:ss')
        ||g_logfile_separator||
      p_message
      );

  end;

  --level 1 ....................................................................
  procedure cleanup_before_load
  is
  

    l_start number;
    l_duration number;
    l_dt date;
    l_cnt binary_integer;
    
    l_delete_batch_size binary_integer;
    c_delete_batch_size constant binary_integer := 10000;

    --level 2 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    procedure get_parm_delete_batchsize
    is
      l_parm ops_scripts_parms.parm_name%type := 'DELETE_BATCHSIZE';
      l_tmp  ops_scripts_parms.parm_value%type;
    begin
      l_tmp := scripts.get_arg_value( g_program_id, l_parm);
      if l_tmp is null then
        l_delete_batch_size := c_delete_batch_size;
      else
        l_delete_batch_size := to_number(l_tmp);
        if l_delete_batch_size < 0 then
          l_delete_batch_size := c_delete_batch_size;
        end if;
      end if;

    exception when others then
      l_delete_batch_size := c_delete_batch_size;
    end;
    
    
  begin   --back to level 1 ....................................................
    
    l_start := DBMS_UTILITY.get_time;
    
    l_dt := g_load_date - g_nbdayskept;
    Pgmloc := 1200;
    write_log('   deleting old data (prior to '||to_char(l_dt, 'yyyy-mm-dd')||')');
    g_pricing_msgs_child1.msg_text_2 := get_oasis_msg(60);
    upd_prc_msgs(true);  --procedure does a COMMIT !!

    --
    -- Delete historical
    Pgmloc := 1210;
    loop
      get_parm_delete_batchsize;  --at each iteration --> can be changed at runtime
      delete
        from msef_daily_fundamentals a
       where file_type = g_file_type
         and region_id = g_region_id
         and file_id   = g_file_id
         and delivery_date < l_dt
         and rownum <= l_delete_batch_size
         ;
      l_cnt := sql%rowcount;
      exit when l_cnt = 0;
      g_histo_deleted := g_histo_deleted + l_cnt;
      upd_prc_msgs(true);  --procedure does a COMMIT !!
    end loop;

    Pgmloc := 1220;
    l_duration := ((DBMS_UTILITY.get_time - l_start)/100);
    write_log('     done in '||l_duration||' secs');




    l_start := DBMS_UTILITY.get_time;
    l_dt := g_load_date;
    Pgmloc := 1230;
    write_log('   deleting current data ('||to_char(l_dt, 'yyyy-mm-dd')||')');
    g_pricing_msgs_child1.msg_text_2 := get_oasis_msg(61);
    upd_prc_msgs(true);  --procedure does a COMMIT !!

    --
    -- Delete Current if already loaded..
    Pgmloc := 1240;
    loop
      get_parm_delete_batchsize;  --at each iteration --> can be changed at runtime
      delete
        from msef_daily_fundamentals a
       where file_type = g_file_type
         and region_id = g_region_id
         and file_id   = g_file_id
         and delivery_date = l_dt
         and rownum <= l_delete_batch_size
         ;
      l_cnt := sql%rowcount;
      exit when l_cnt = 0;
      g_current_deleted := g_current_deleted + l_cnt;
      upd_prc_msgs(true);  --procedure does a COMMIT !!
    end loop;
    Pgmloc := 1250;
    Pgmloc := 1260;

    l_duration := ((DBMS_UTILITY.get_time - l_start)/100);
    write_log('     done in '||l_duration||' secs');

    g_pricing_msgs_child1.msg_text_2 := get_oasis_msg(62);
    upd_prc_msgs(true);  --procedure does a COMMIT !!
    

--  exception when others then
--    p_message := get_error_msg_parent(l_tmp_message);
--    raise_fatal_error;
  end;

  --level 1 ....................................................................
  procedure string_to_table
          ( p_string         in     varchar2
          , p_separator      in     varchar2
          , p_table          in out nocopy table_fields_tab_t
          , pricing_msgs_rec in out nocopy pricing_msgs%rowtype
          , p_all_null          out boolean
          , p_success           out boolean
          ) as --{
  --
  v_success           boolean;
  v_position          binary_integer;
  v_previous_position binary_integer;
  v_index             binary_integer;
  --
  begin --{
    Pgmloc := 1060;
    p_all_null := true;
    p_success  := false;
    --
    Pgmloc := 1070;
    v_index := 0;
    p_table.delete;
    if p_string is null
    then --{
      null;
    else --}{
      v_previous_position := 0;
      for v_position in 1..length( p_string )
      loop --{
        Pgmloc := 1080;
        if substr( p_string, v_position, 1) = p_separator
        then --{
          Pgmloc := 1090;
          v_index := v_index + 1;
          --mlz - substr to 2000
          p_table( v_index ) := substr(trim( substr( p_string
                                            , v_previous_position + 1
                                            , v_position
                                              - ( v_previous_position + 1 )
                                            ))
                                       ,1,2000);
          Pgmloc := 1100;
          if p_table( v_index ) is not null
          then
            p_all_null := false;
          end if;
          Pgmloc := 1110;
          v_previous_position := v_position;
        end if; --}
      end loop; --}

      Pgmloc := 1120;
      -- Get the last field
      v_index := v_index + 1;
          --mlz - substr to 2000
      Pgmloc := 1130;
      p_table( v_index ) := substr(trim( substr( p_string, v_previous_position + 1 ) ) ,1,2000);
      if p_table( v_index ) is not null
      then
        p_all_null := false;
      end if;
    end if; --}
    Pgmloc := 1140;
    p_success  := true;
    --
  end string_to_table; --}}

  --level 1 ....................................................................
  function get_data_Id_index_in_file(
    --p_fields       in table_fields_tab_t,
    p_message      in out nocopy varchar2
    ) 
    return binary_integer
  is
    l_index binary_integer;
    cursor c_pos is
      select field_nr_data_id
      from msef_ff_files
      where file_id = g_file_id
      and active_flag = 'Y'
      ;
    r_pos  c_pos%rowtype;
  begin
    open c_pos;
    fetch c_pos into r_pos;
    if c_pos%notfound then
      close c_pos;
      p_message := 'No active file definition found for ['||g_file_id||'] in [msef_ff_files]';
      raise handled_error;
    end if;
    close c_pos;
    
    return r_pos.field_nr_data_id;
  end;


  --level 1 ....................................................................
  --mlz - 20oct2013
  function get_valuation_dt_index_in_file(
    --p_fields       in table_fields_tab_t,
    p_message      in out nocopy varchar2
    ) 
    return binary_integer
  is
    l_index binary_integer;
    
    --date used to filter (VALUATION_DATE) supposed to always be a STATIC field!!
    cursor c_pos is
      select field_nr
      from msef_ff_static_field_dest
      where file_id = g_file_id
      and active_flag = 'Y'
      and upper(oasis_column) = 'VALUATION_DATE'
      ;
    r_pos  c_pos%rowtype;
  begin
    open c_pos;
    fetch c_pos into r_pos;
    if c_pos%notfound then
      close c_pos;
      return null;  --not all file_id have valuation dates --> not an error...
    end if;
    close c_pos;
    
    return r_pos.field_nr;
  end;
  --endmlz - 20oct2013


  --level 1 ....................................................................
  procedure set_file_processed
  is
  begin
    
    Pgmloc := 1010;
    insert into msef_files_loaded
    (  
      MSEF_FUNDAMENTALS_REGION_CODE,
      FILE_TYPE,
      FILE_ID,
      FILE_DATE,
      CREATION_USER,
      CREATION_STAMP,
      LAST_USER,
      LAST_STAMP
    )
    values
    (
      g_region_id,
      g_file_type,
      g_file_id,
      g_load_date,
      g_user,
      sysdate,
      g_user,
      sysdate
    );
  end;


  --level 1 ....................................................................
  function is_file_processed
    return boolean
  is
  --
    l_dt_from date := to_date(to_char(g_load_date,'yyyymm')||'01','yyyymmdd');
    l_dt_to   date := to_date(to_char(add_months(g_load_date,1),'yyyymm')||'01','yyyymmdd')-1;
    
    cursor cur_prc is
      select file_date
      from msef_files_loaded
      where msef_fundamentals_region_code = g_region_id
      and file_type = g_file_type
      and file_id = g_file_id
      and file_date between l_dt_from and l_dt_to 
      ;
    r_prc  cur_prc%rowtype;
  begin
    Pgmloc := 1020;
    open cur_prc;
    Pgmloc := 1030;
    fetch cur_prc into r_prc;
    if cur_prc%notfound then
      Pgmloc := 1040;
      close cur_prc;
      return false;
    end if;
    Pgmloc := 1050;
    close cur_prc;
    return true;
  end;

--level 0 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
begin
  p_success := false;
  
  --just change of variable because it's misleading (out is from the unzip perspective)
  g_current_filename := g_out_filename;  

  

  Pgmloc := 1270;
  --mlz - 22oct2013
  set_parent_remarks(null);
  --endmlz - 22oct2013
  set_child_remarks(null);
  upd_prc_msgs;
  
  
  --
  -- log step
  --
    Pgmloc := 1280;
  write_log(' Prossessing file: '||p_file_type||'/'||p_region_id||'/'||p_file_id||'/'||g_current_filename);

  

  --
  -- if monthly, files aren't available all at one
  -- >> check if not already done
  --
  Pgmloc := 1290;
  if g_file_type = 'M' then
    if is_file_processed = true then
      Pgmloc := 1300;
      set_child_remarks(get_oasis_msg(40));
      --g_pricing_msgs_child1.remarks := get_oasis_msg(40)||' '||g_pricing_msgs_child1.remarks;
      Pgmloc := 1310;
      g_pricing_msgs_child1.successfull_flag := 'Y';
      Pgmloc := 1320;
      g_pricing_msgs_child1.msg_text := get_oasis_msg_cmp(120);
      Pgmloc := 1330;
      g_pricing_msgs_child1.msg_text_2 := get_oasis_msg_cmp(122);
      Pgmloc := 1340;
      upd_prc_msgs(true);  --procedure does a COMMIT !!

      g_files_already_proc := g_files_already_proc + 1;
      p_success := true;
      return;
    end if;
  end if;


  Pgmloc := 1350;
  --
  -- cleanup
  --   file_type / region_id / file_id
  --
  cleanup_before_load;

  
    Pgmloc := 1360;
  write_log('   Loading data');
  g_pricing_msgs_child1.msg_text_2 := get_oasis_msg(70);
  upd_prc_msgs(true);  --procedure does a COMMIT !!
  
  --
  -- load field list to filter input data
  if g_filter_data then
    --mlz - 20oct2013
    --
    -- filter 1: discard too old data (valuation_date)
    --
    l_field_nr_of_valuation_dt := get_valuation_dt_index_in_file( l_tmp_message );
    --endmlz - 20oct2013

    --
    -- filter 2: discard data_id not processed
    --
    l_data_id_tab.delete; --just to make sure
    
    for r in cur_data_id
    loop
      --the value is not important, only the existence in the array,
      --through the index
      l_data_id_tab(r.data_id) := 'X';
    end loop;
    
    --get position of dataId column if the file
    l_field_nr_of_data_id := get_data_Id_index_in_file( l_tmp_message );
  end if;

  Pgmloc := 1370;
  --
  -- open detail file
  -- (local version from load_supplier_rules)
  -- >> detect file_not_found -->> not an error
  --
  open_file( g_current_filename
           , g_current_oradir
           , c_read_mode
           , c_record_length
           , g_pricing_msgs_child1
           , g_current_file
           , p_success
           );
  --if not p_success then raise_fatal_error; end if;
  raise_fatal_error(p_do_raise => not p_success);

  l_mod := 0;
  --
  loop --{
    Pgmloc := 1380;

    load_supplier_rules.get_line( g_current_file
                                , g_pricing_msgs_child1
                                , w_record
                                , p_success
                                , eof
                                );
    exit when eof;
    --if ( not p_success ) then raise_fatal_error; end if;
    raise_fatal_error(p_do_raise => not p_success);
    g_line_read := g_line_read + 1;
    --
    
    l_mod := l_mod + 1; --modulo emulation

    Pgmloc := 1390;
    string_to_table( w_record
                   , g_separator
                   , l_fields
                   , g_pricing_msgs_child1
                   , l_all_null
                   , p_success
                   );
    --if ( not p_success ) then raise_fatal_error; end if;
    raise_fatal_error(p_do_raise => not p_success);
    if l_all_null then
      continue; ------>>
    end if;
    
    --
    -- optional action, keep only the data we will use to reduce volume
    --
    l_skipped_by_filter := false;
    --mlz - 20oct2013
    if g_filter_data and not l_skipped_by_filter
    then
      --
      -- filter 1
      --
      if l_field_nr_of_valuation_dt is not null then
        begin
          l_field_value := l_fields(l_field_nr_of_valuation_dt);
          --mlz - 22oct2013
          if l_field_value is not null then
          --endmlz - 22oct2013
            l_tmp_dt := to_date(l_field_value, 'yyyy-mm-dd');
            l_skipped_by_filter := (l_tmp_dt < g_start_date);
          end if;
        exception when others then
          null;
        end;
      end if;
    end if;
    --endmlz - 20oct2013
      
    if g_filter_data and not l_skipped_by_filter
    then
      --
      -- filter 2
      --
      if l_field_nr_of_data_id is not null --null = no filter possible
      then
        l_field_value := l_fields(l_field_nr_of_data_id);
        --if not in tab --> filter
        l_skipped_by_filter := not l_data_id_tab.exists(l_field_value);
      else
        l_skipped_by_filter := false;
      end if;
    end if;

    if l_skipped_by_filter then
      --count it
      g_line_filtered := g_line_filtered + 1;
    else
      Pgmloc := 1400;
      l_mdf_rec := null;
      for i in l_fields.first..l_fields.last
      loop
        Pgmloc := 1410;
        case i
          when 1  then l_mdf_rec.field_01 := l_fields(i);
          when 2  then l_mdf_rec.field_02 := l_fields(i);
          when 3  then l_mdf_rec.field_03 := l_fields(i);
          when 4  then l_mdf_rec.field_04 := l_fields(i);
          when 5  then l_mdf_rec.field_05 := l_fields(i);
          when 6  then l_mdf_rec.field_06 := l_fields(i);
          when 7  then l_mdf_rec.field_07 := l_fields(i);
          when 8  then l_mdf_rec.field_08 := l_fields(i);
          when 9  then l_mdf_rec.field_09 := l_fields(i);
          when 10 then l_mdf_rec.field_10 := l_fields(i);
          when 11 then l_mdf_rec.field_11 := l_fields(i);
          when 12 then l_mdf_rec.field_12 := l_fields(i);
          when 13 then l_mdf_rec.field_13 := l_fields(i);
          when 14 then l_mdf_rec.field_14 := l_fields(i);
          when 15 then l_mdf_rec.field_15 := l_fields(i);
          when 16 then l_mdf_rec.field_16 := l_fields(i);
          when 17 then l_mdf_rec.field_17 := l_fields(i);
          when 18 then l_mdf_rec.field_18 := l_fields(i);
          when 19 then l_mdf_rec.field_19 := l_fields(i);
          when 20 then l_mdf_rec.field_20 := l_fields(i);
        end case;
      end loop;

      Pgmloc := 1420;
      l_mdf_rec.region_id := p_region_id;
      l_mdf_rec.file_id := p_file_id;
      l_mdf_rec.delivery_date := p_delivery_date;
      l_mdf_rec.file_type := p_file_type;
      l_mdf_rec.line_number := g_line_read;

      Pgmloc := 1430;
      l_mdf_rec.creation_user := g_user;
      l_mdf_rec.creation_stamp := sysdate;
      l_mdf_rec.last_user := g_user;
      l_mdf_rec.last_stamp := sysdate;

      Pgmloc := 1440;
      declare
        l_cnt binary_integer;
      begin
        insert into msef_daily_fundamentals values l_mdf_rec;
        l_cnt := sql%rowcount;
        Pgmloc := 1450;
        g_line_inserted := g_line_inserted + l_cnt;
      exception when others then
        g_line_error := g_line_error + 1;
        
        write_log_det(  dbms_utility.format_error_stack||dbms_utility.format_error_backtrace );
      end;
--    tab.extend(1);
--    tab(tab.last) := l_mdf_rec;
--    
--    if tab.count >= 1000 then
--      FORALL i IN 1..tab.COUNT
--      INSERT INTO msef_daily_fundamentals VALUES tab(i);
--      
--      --?? g_pricing_msgs_child_rec.value_02 := g_pricing_msgs_child_rec.value_02 + 1;
--      --error management
--      tab.delete;
--    end if;
    end if;



    --
    Pgmloc := 1460;
    --if mod(g_pricing_msgs_child_rec.value_01, g_commit_freq) = 0
    if l_mod >= g_commit_freq
    then
      upd_prc_msgs(true);  --procedure does a COMMIT !!
      l_mod := 0;
      Pgmloc := 1470;
      if g_log_all then
        write_log( '     commit after: '||g_line_read  );
      end if;
    end if;
    --
    
    if g_line_read >= g_load_limit then
      --to limit the data, only for testing purposes...
      exit;
    end if;
  end loop; --}
  
  --
  -- Close the input file
  Pgmloc := 1480;
  load_supplier_rules.file_close( g_current_file
                                , g_pricing_msgs_child1
                                , p_success
                                );
  --if ( not p_success ) then raise_fatal_error; end if;
  raise_fatal_error(p_do_raise => not p_success);


  --
  -- Ending process
  Pgmloc := 1490;
  if g_file_type = 'M' then
    set_file_processed;
  end if;

  Pgmloc := 1500;
  g_pricing_msgs_child1.msg_text := get_oasis_msg_cmp(120);
  Pgmloc := 1510;
  g_pricing_msgs_child1.msg_text_2 := get_oasis_msg_cmp(122);
  g_pricing_msgs_child1.successfull_flag := 'Y';
  upd_prc_msgs(true);  --procedure does a COMMIT !!


  g_files_loaded := g_files_loaded + 1;

  Pgmloc := 1530;
  write_log(  '   done --> read: '||g_line_read ||' ins: '||g_line_inserted  );

exception
when handled_error then
  --1st get it from REMARKS
  if l_tmp_message is null 
  and g_pricing_msgs_child1.remarks not like '(%'
  then
    l_tmp_message := g_pricing_msgs_child1.remarks;
    g_pricing_msgs_child1.remarks := null;
  end if;
  
  --then, if still null, get full error
  if l_tmp_message is null then
    l_tmp_message := get_error_msg_child(l_tmp_message);
  end if;
  

  --for pricing_msgs:
  Pgmloc := 1540;
  g_pricing_msgs_child1.successfull_flag := 'Y';
  set_child_remarks(l_tmp_message);
  g_pricing_msgs_child1.msg_text := get_oasis_msg_cmp(120);
  g_pricing_msgs_child1.msg_text_2 := get_oasis_msg_cmp(122);
  upd_prc_msgs(true);

  g_files_error := g_files_error + 1;

  --complete msg with stack (if not already there)
  l_tmp_message := get_error_msg_child(l_tmp_message);
  write_log_det( l_tmp_message );
  write_log( l_tmp_message );
  Pgmloc := 1550;

  p_success := true;

when others
then
  Pgmloc := 1600;
  --get msg
  l_tmp_message := get_error_msg_child( l_tmp_message );
  --add location
  Pgmloc := 1610;
  if g_line_read = 0 then
    Pgmloc := 1620;
    --no line read -->> probably an open file error (file not found?)
    l_tmp_message :=
      'file['||g_current_file_dir||constants.Get_Dir_Seperator
      ||g_current_filename||'] '
      ||l_tmp_message
    ;
  else
    Pgmloc := 1630;
    --some line read -->> log line number also
    l_tmp_message :=
      to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss')
      ||'   Err@' || pgmloc
      || ' file['||g_current_file_dir||constants.get_dir_seperator
      || g_current_filename||']'
      || ' line['||g_line_read||'] '
      ||l_tmp_message
    ;
  end if;

  write_log( l_tmp_message );
  write_log_det( l_tmp_message );
  
  Pgmloc := 1640;
  if utl_file.is_open(g_current_file) then
    utl_file.fclose(g_current_file);
  end if;

  Pgmloc := 1650;
  --for pricing_msgs:
  g_pricing_msgs_child1.successfull_flag := 'N';
  g_pricing_msgs_child1.err_text := substr( l_tmp_message, 1, 255 );
  Pgmloc := 1660;
  upd_prc_msgs(true);

  --mlz - 15sep2015
  g_at_least_one_child_failed := true;
  --endmlz - 15sep2015

  Pgmloc := 1670;
  p_success := false;
end;


------------------------------------------------------------------------
procedure load_region
        ( p_program_id     in     ops_scripts.program_id%type
        --, p_cntl_rec       in out nocopy product_component.r_parameters
        , p_success           out boolean
        ) as --{
--
fct varchar2(100) := 'load_region';
--
  v_keep_for          binary_integer := 35;
  --
  v_country           constant varchar2(80) := 'WORLD';
  v_field             varchar2(80);
  v_field_name        varchar2(80);
  --
  v_min_date          date;
  v_max_date          date;
  --
  c_record_length constant binary_integer := 32767;
  --
  l_tmp_message       varchar2(4000);
  --
  w_record            varchar2( 32767 );
  w_file_part         binary_integer;
  w_prev_file_part    binary_integer;
  w_header            varchar2( 32767 );
  --
  --
  in_eof              boolean;
  eof                 boolean;
  element_count       binary_integer;
  length_per_file     binary_integer;
  --
  prev_count          binary_integer;
  commit_count        binary_integer;
  --
  v_program_id        ops_scripts.program_id%type;
  --
  v_file_code_start   binary_integer;
  v_look_for          varchar2(256);
  v_file_code         varchar2(256);
  --
  l_file_name         varchar2(1000);
  l_file_type         msef_daily_fundamentals.file_type%type;
  l_region_id         msef_daily_fundamentals.region_id%type;
  l_file_id           msef_daily_fundamentals.file_id%type;

  cursor get_children is
    select program_id
    from ops_scripts
    where program_id like p_program_id||'__' --only 6 digit children
    ;

  l_file_id_short    ops_scripts_parms.parm_value%type;

  --level 1 ....................................................................
  procedure get_parm_file_id
  is
    l_parm ops_scripts_parms.parm_name%type := 'FILE_ID';
    tmp ops_scripts_parms.parm_value%type;
  begin
    tmp := scripts.get_arg_value( g_program_id_child, l_parm);
    if tmp is null then
      
      l_tmp_message := get_oasis_msg(10, l_parm);  --v_tmp_message := 'Parameter ['||l_parm||'] not found (ops_scripts_parms)';
      --raise_fatal_error;
      raise_fatal_error(p_do_raise => true);
    end if;

    g_file_id := tmp; --upper/lowercase must be correct in param table !!!
  end;
  --level 1 ....................................................................
  procedure get_parm_file_id_short
  is
    l_parm ops_scripts_parms.parm_name%type := 'FILE_ID_SHORT';
    tmp ops_scripts_parms.parm_value%type;
  begin
    tmp := scripts.get_arg_value( g_program_id_child, l_parm);
    if tmp is null then
      
      l_tmp_message := get_oasis_msg(10, l_parm);  --v_tmp_message := 'Parameter ['||l_parm||'] not found (ops_scripts_parms)';
      --raise_fatal_error;
      raise_fatal_error(p_do_raise => true);
    end if;

    l_file_id_short := tmp;
  end;
  --level 1 ....................................................................
  PROCEDURE get_parm_filter_data
  is
    l_parm ops_scripts_parms.parm_name%type := 'FILTER_DATA';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp :=  upper( scripts.get_arg_value( g_program_id_child, l_parm ) );
    write_log('['||l_parm||'] is ['||l_tmp||']');
    if l_tmp in ('Y','YES') then
      g_filter_data := true;
    else
      g_filter_data := false;
    end if;
  end;

  --level 1 ....................................................................
  PROCEDURE get_parm_skip
  is
    l_parm ops_scripts_parms.parm_name%type := 'SKIP';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp :=  upper( scripts.get_arg_value( g_program_id_child, l_parm ) );
    write_log('['||l_parm||'] is ['||l_tmp||']');
    if l_tmp in ('Y','YES') then
      g_skip_child := true;
    else
      g_skip_child := false;
    end if;
  end;

  --level 1 ....................................................................
  --mlz - 13sep2015 - START_DATE (static) is replaced by: START_DAY+START_NB_YEARS (dyn)
  --procedure get_parm_start_date
  --is
  --  l_parm ops_scripts_parms.parm_name%type := 'START_DATE';
  --  l_tmp  ops_scripts_parms.parm_value%type;
  --begin
  --  begin
  --    l_tmp := scripts.get_arg_value( g_program_id_child, l_parm);
  --    if l_tmp is null then
  --      --mlz - 05nov2013
  --      --l_tmp_message := get_oasis_msg_gen(216, l_parm); --Child [%%%%%] parameter [%%%%%] not found --> using default: %%%%%
  --      --endmlz - 05nov2013
  --      g_start_date := c_start_date;
  --      --mlz - 05nov2013
  --      l_tmp_message := get_oasis_msg_gen(216, g_program_id_child, l_parm, to_char(g_start_date, c_start_date_format)); --Child [%%%%%] parameter [%%%%%] not found --> using default: %%%%%
  --      --endmlz - 05nov2013
  --    else
  --      g_start_date := to_date(l_tmp, c_start_date_format);
  --      l_tmp_message := '['||l_parm||'] is ['||l_tmp||']';
  --    end if;
  --
  --  exception when others then
  --    --mlz - 05nov2013
  --    --l_tmp_message := get_oasis_msg_gen(218, l_parm); --Parameter [%%%%%] has an invalid value [%%%%%] --> using default: %%%%%
  --    --endmlz - 05nov2013
  --    g_start_date := c_start_date;
  --    --mlz - 05nov2013
  --    l_tmp_message := get_oasis_msg_gen(218, l_parm, l_tmp, to_char(g_start_date, c_start_date_format)); --Parameter [%%%%%] has an invalid value [%%%%%] --> using default: %%%%%
  --    --endmlz - 05nov2013
  --  end;
  --
  --  write_log(l_tmp_message);
  --end;


  --level 1 ....................................................................
  function get_start_date
    return date
  is
    ll_start_day      date;
    ll_start_nb_years ops_Scripts_parms.parm_value%type;

    ll_start_date     date;

    --level 2 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    procedure get_parm_START_DAY
    is
      l_parm ops_scripts_parms.parm_name%type := 'START_DAY';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
      l_tmp := scripts.get_arg_value( g_program_id, l_parm);
      if l_tmp is null
      then
        write_log(get_oasis_msg_gen(214, l_parm, c_start_day)); --Parameter [%%%%%] not found --> using default: %%%%%
        l_tmp := c_start_day;
      end if;

      begin
        ll_start_day := to_date(l_tmp, c_start_day_format);
      exception when others then
        write_log(get_oasis_msg_gen(218, l_parm, l_tmp, c_start_day)); --Parameter [%%%%%] has an invalid value [%%%%%] --> using default: %%%%%
        l_tmp := c_start_day;
        ll_start_day := to_date(l_tmp, c_start_day_format);
      end;

      write_log('['||l_parm||'] is ['||l_tmp||']');
    end;

    --level 2 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    procedure get_parm_START_NB_YEARS
    is
      l_parm ops_scripts_parms.parm_name%type := 'START_NB_YEARS';
      l_tmp  ops_scripts_parms.parm_value%type;
    begin
      l_tmp := scripts.get_arg_value( g_program_id, l_parm);
      if l_tmp is null then
        write_log(get_oasis_msg_gen(214, l_parm, c_start_nb_years)); --Parameter [%%%%%] not found --> using default: %%%%%
        ll_start_nb_years := c_start_nb_years;
      else
        write_log('Parameter ['||l_parm||'] is ['||l_tmp||']');
        ll_start_nb_years := to_number(l_tmp);
      end if;

    exception when others then
      write_log( get_oasis_msg_gen(218, l_parm, l_tmp, c_start_nb_years) ); --Parameter [%%%%%] has an invalid value [%%%%%] --> using default: %%%%%
      ll_start_nb_years := c_start_nb_years;
    end;
  begin
    get_parm_start_day; --month-day without year --> default is the current year
    get_parm_start_nb_years;
    
    ll_start_date := add_months(ll_start_day, ll_start_nb_years * -12);  --years to substract
    return ll_start_date;
  end;

  procedure dummy_proc is
  begin
    null;
  end;
--level 0 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
begin --{
  --
  Pgmloc := 1680;
  l_tmp_message       := null;
  p_success       := false;
  --

  --detailed log is common to all file_id/packages
  -->> open it here
  write_log_det_init(l_tmp_message); --reads: LOG_DET parameter



  for r in get_children
  loop
    g_program_id_child := r.program_id;
    Pgmloc := 1690;

    --PARM
    get_parm_file_id;

    --write action on parent
    g_files_to_load := g_files_to_load + 1;
    g_pricing_msgs.msg_text_2 := get_oasis_msg(80, r.program_id, g_file_id);

    Pgmloc := 1700;
    upd_prc_msgs(false);

    --
    -- init new child
    --
    g_pricing_msgs_child1 := null;
    Pgmloc := 1710;
    cre_prc_msgs_child(true);
    
    Pgmloc := 1720;
    reset_counters_child;




    Pgmloc := 1730;

    --PARM
    --mlz - 13sep2015 - START_DATE (static) is replaced by: START_DAY+START_NB_YEARS (dyn)
    --get_parm_start_Date;
    g_start_date := get_start_date;
    --endmlz - 13sep2015
    
    --PARM
    get_parm_file_id_short;

    Pgmloc := 1760;
    --
    -- SEPARATOR
    -- custom one (if defined) on child pgmid
    -- otherwise -->> | (pipe)
    --
    g_separator := nvl( scripts.get_arg_value( r.program_id, 'SEPARATOR'), c_default_separator);

    Pgmloc := 1770;
    --(log file already opened) -->> specify directory for data files
    g_current_oradir :=
      'FDMT'||'_'
        ||upper(g_file_type)||'_'
        ||upper(g_region_id)||'_'
        ||upper(l_file_id_short)
        ; --ex. FDMT_D_NRA_VR

    Pgmloc := 1780;
    --mlz - 14aug2015
    -- begin
    --   select directory_path
    --   into g_current_file_dir
    --   from all_directories
    --   where directory_name = p_cntl_rec.dir_name
    --   ;
    -- exception when others then
    --   --v_tmp_message := 'Directory ['||p_cntl_rec.dir_name||'] not found!!';
    --   l_tmp_message := get_oasis_msg_gen(220, g_cntl_rec.dir_name);
    --   --raise_fatal_error;
    --   raise_fatal_error(p_do_raise => true);
    -- end;
    g_current_file_dir := get_directory(g_current_oradir, l_tmp_message);
    --endmlz - 14aug2015
    write_log('Directory ['||g_current_oradir||'] points to ['||g_current_file_dir||']');

    Pgmloc := 1730;
    --PARM
    get_parm_filter_data;
    
    --PARM
    get_parm_skip;

    

    if g_skip_child = false then
      --
      Pgmloc := 1790;
      load_one_file( g_load_date --p_delivery_date
                   , g_region_id
                   , g_file_id
                   , g_file_type
                   --, p_cntl_rec
                   --, g_pricing_msgs_child_rec --child
                   , p_success
                   );
      if p_success
      then
        --sum inserted lines
        g_total_lines_ins := g_total_lines_ins + g_line_inserted;
      end if;
      
    else

      set_child_remarks(null);
      g_pricing_msgs_child1.msg_text := get_oasis_msg_cmp(120);
      g_pricing_msgs_child1.msg_text_2 := 'Skipped by parameter';
      g_pricing_msgs_child1.successfull_flag := 'Y';
      
      upd_prc_msgs(true);
    end if;

  end loop;

  Pgmloc := 1800;
  g_pricing_msgs.msg_text_2 := null;
  Pgmloc := 1810;
  upd_prc_msgs(false);

  p_success := true;
  --
exception
when others
then
  --fatal error 
  if l_tmp_message is null and g_pricing_msgs_child1.err_text is not null
  then
    --error located in the child stucture
    --copy it to general msg 
    --as it will be stored into parent
    l_tmp_message := g_pricing_msgs_child1.err_text;
  end if;
  --get msg
  l_tmp_message := get_error_msg_parent( l_tmp_message );  --parent err proc!!!

  --for pricing_msgs:
  g_pricing_msgs.err_text := substr( l_tmp_message, 1, 255 );

  p_success := false;
end load_region; --}}


--------------------------------------------------------------------------------
procedure Load
        ( p_program_id  in ops_scripts.program_id%type
        , p_date_of_msg in varchar2
        --vv
        , p_load_date   in varchar2
        --^^
        , p_message     out nocopy varchar2
        ) as --{
--
fct varchar2(100) := 'files';
--

  l_tmp_success           boolean;
  l_tmp_message           varchar2(4000);


  cursor get_dss
       ( i_program_id ops_scripts.program_id%type
       ) is
    select *
    from data_supply_schedule
    where program_id = i_program_id
    ;
  r_dss  get_dss%rowtype;

  -- --level 1 ....................................................................
  -- --
  -- -- local version due to LOG file opened too late in the processing
  -- --
  -- PROCEDURE Process_Beginning
  --         ( P_Program_Id     IN            Ops_Scripts.Program_Id%TYPE
  --         , P_Load_Date      IN            DATE
  --         , P_Cntl_Rec       IN OUT NOCOPY product_component.R_Parameters
  --         , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
  --         , P_Success           OUT        BOOLEAN
  --         , P_Message           OUT        VARCHAR2
  --         ) IS --{
  -- --
  -- V_Load_Date          DATE;
  -- V_On_Server          BOOLEAN := CONSTANTS.On_Server;
  -- Success              BOOLEAN;
  -- V_Program_Id         INTEGER;
  -- -- Ajout - JSL - 16 decembre 2010
  -- V_Date               DATE;
  -- -- Fin d'ajout - JSL - 16 decembre 2010
  -- --
  -- CURSOR Get_Run_Parms IS
  -- SELECT 'P'
  --   FROM Product_Running_Parms PRP
  --  WHERE PRP.Program_Id = V_Program_Id
  -- UNION
  -- SELECT 'S'
  --   FROM Supplier_Running_Parms SRP
  --  WHERE SRP. Program_ID = V_Program_Id;
  -- --
  -- BEGIN --{
  --   Pgmloc := 1460;
  --   V_Load_Date      := NVL( P_Load_Date, Constants.Get_Site_Date );
  --   -- Les scripts appelants sur Linux n'inserent pas les enfants.
  --   -- Faire comme si nous n'etions pas sur le serveur et inserer ces messages.
  --   IF P_Program_Id > 9999
  --   THEN
  --     V_On_Server := FALSE;
  --   END IF;
  --   IF V_On_Server
  --   THEN --{
  --     BEGIN --{
  --       SELECT *
  --         INTO Pricing_Msgs_Rec
  --         FROM Pricing_Msgs PM
  --        WHERE PM.Program_Id       = P_Program_Id
  --          AND PM.Date_of_Prices   = V_Load_Date
  --          AND PM.Successfull_Flag = 'N'
  --          AND PM.Terminal_Id      = NVL( Constants.Get_Terminal_Id, 'N/A' )
  --          AND PM.Date_Of_Msg      = (
  --              SELECT MAX( PM1.Date_Of_Msg )
  --                FROM Pricing_Msgs PM1
  --               WHERE PM.Program_Id     = PM1.Program_Id
  --                 AND PM.Date_Of_Prices = PM1.Date_Of_Prices
  --                 AND PM.Terminal_Id    = PM1.Terminal_Id
  --              );
  --     EXCEPTION
  --     WHEN NO_DATA_FOUND THEN
  --       V_Date := SYSDATE;
  --       Load_Supplier_Rules.Initialize_Prc_Msgs( P_Program_Id
  --                                              , V_Date
  --                                              , V_Load_Date
  --                                              , Pricing_Msgs_Rec
  --                                              , Success
  --                                              );
  -- -- Modification - JSL - 16 decembre 2010
  -- --   IF ( NOT SUCCESS ) THEN GOTO Get_Out; END IF;
  --       IF SUCCESS
  --       THEN --{
  --         NULL;
  --       ELSE --}{
  --         IF V_Load_Date = Constants.Get_Site_Date
  --         THEN --}
  --           GOTO Get_Out;
  --         ELSE --}{
  --           V_Date := V_Date - 1 / (24*60*60);
  --           Load_Supplier_Rules.Initialize_Prc_Msgs( P_Program_Id
  --                                                  , V_Date
  --                                                  , V_Load_Date
  --                                                  , Pricing_Msgs_Rec
  --                                                  , Success
  --                                                  );
  --           IF NOT SUCCESS THEN GOTO Get_Out; END IF;
  --         END IF; --}
  --       END IF; --}
  -- -- Fin de modification - JSL - 16 decembre 2010
  --     END; --}
  --   ELSE --}{
  --     ----------------------------------------
  --     -- Initalization of the pricing messages
  --     ----------------------------------------
  --     Load_Supplier_Rules.Initialize_Prc_Msgs( P_Program_Id
  --                                            , SYSDATE
  --                                            , V_Load_Date
  --                                            , Pricing_Msgs_Rec
  --                                            , Success
  --                                            );
  --     IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --   END IF; --}
  --   Pgmloc := 1470;
  --   Pricing_Msgs_Rec.Successfull_Flag := 'N';
  --   Pricing_Msgs_Rec.Msg_Text         := 'Program id is '
  --                                     || to_char(P_Program_id);
  --   Pricing_Msgs_Rec.Msg_Text_2       := 'Program in progress';
  --   Pricing_Msgs_Rec.Value_01         := 0;
  --   Pricing_Msgs_Rec.Value_02         := 0;
  --   Pricing_Msgs_Rec.Value_03         := 0;
  --   Pricing_Msgs_Rec.Value_04         := 0;
  --   Pricing_Msgs_Rec.Value_05         := 0;
  --   --
  --   Pgmloc := 1480;
  --   Load_Supplier_Rules.Update_Prc_Msgs( P_Program_Id
  --                                      , Pricing_Msgs_Rec.Date_of_Msg
  --                                      , Pricing_Msgs_Rec
  --                                      , Success
  --                                      );
  --   IF ( NOT SUCCESS ) THEN GOTO Get_Out; END IF;
  --   --
  --   Pgmloc := 1490;
  --   UPDATE Break_Points
  --      SET Status      = 'PROG'
  --        , Information = TO_CHAR( Pricing_Msgs_Rec.Date_of_Msg
  --                               , 'dd-mon-yyyy hh24:mi:ss' )
  --        , Nb_Records  = Pricing_Msgs_Rec.Value_01
  --        , Last_User   = Constants.Get_User_Id
  --        , Last_Stamp  = SYSDATE
  --    WHERE Program_Id  = P_Program_Id;
  --   --
  --   Pgmloc := 1500;
  --   IF P_Cntl_Rec.Dir_Name IS NULL
  --   THEN
  --     P_Cntl_Rec.Dir_Name := Constants.Get_Dir_Name;
  --   END IF;
  --   --
  --   Pgmloc := 1510;
  -- 
  --   --
  --   -- rest of processing removed (don't need it here)
  --   --
  -- 
  -- << Get_Out >>
  --   --
  --   if success = false then
  --     --BUG to fix: err_text SHOULD be copied into p_message!!!!!!
  --     if p_message is null then
  --       p_message := pricing_msgs_rec.err_text;
  --     end if;
  --   end if;
  --   P_Success := Success;
  -- end;

  --level 1 ....................................................................
  PROCEDURE get_parm_logfile_sep 
  is
    l_parm ops_scripts_parms.parm_name%type := 'LOGFILE_SEP';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp :=  lower( scripts.get_arg_value( g_program_id, l_parm ) );
    g_logfile_separator := l_tmp;
    g_logfile_separator := replace(g_logfile_separator, '\t', chr(9));
    if g_logfile_separator is null then
      g_logfile_separator := c_logfile_default_sep;
    end if;
    write_log('['||l_parm||'] is ['||g_logfile_separator||']');
  end;
  
  --mlz - 14aug2015 - now outside (called also by get_logfile_info)
  -- --level 1 ....................................................................
  -- PROCEDURE get_parm_log 
  -- is
  --   l_parm ops_scripts_parms.parm_name%type := 'LOG';
  --   l_tmp  ops_scripts_parms.parm_value%type;
  -- begin
  --   l_tmp := scripts.get_arg_value( g_program_id, l_parm);
  -- 
  --   if l_tmp is null
  --   then
  --     g_cntl_rec.log_file_name := g_program_id||'-'||to_char(sysdate, 'yyyymmdd-hh24miss')||'.log';
  --   else
  --     g_cntl_rec.log_file_name := translate_file_name(l_tmp, '$$', sysdate, null, null);
  --   end if;
  -- end;
  --endmlz - 14aug2015

  --level 1 ....................................................................
  PROCEDURE get_parm_log_all 
  is
    l_parm ops_scripts_parms.parm_name%type := 'LOG_ALL';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp :=  upper( scripts.get_arg_value( g_program_id, l_parm ) );
    write_log('['||l_parm||'] is ['||l_tmp||']');
    if l_tmp in ('Y','YES') then
      g_log_all := true;
    else
      g_log_all := false;
    end if;
  end;

  --mlz - 15sep2015 - moved to common beginning
  ----level 1 ....................................................................
  --procedure get_parm_file_type
  --is
  --  l_parm ops_scripts_parms.parm_name%type := 'FILE_TYPE';
  --  tmp ops_scripts_parms.parm_value%type;
  --begin
  --  tmp := scripts.get_arg_value( p_program_id, l_parm);
  --  if tmp is null then
  --
  --    l_tmp_message := get_oasis_msg_gen(210, l_parm); --Parameter [%%%%%] not found (ops_scripts/_parms)
  --    --raise_fatal_error;
  --    raise_fatal_error(p_do_raise => true);
  --  end if;
  --
  --  g_file_type := initcap(tmp); --First upper/rest lower
  --end;
  --
  ----level 1 ....................................................................
  --procedure get_parm_region_id
  --is
  --  l_parm ops_scripts_parms.parm_name%type := 'REGION_ID';
  --  tmp ops_scripts_parms.parm_value%type;
  --begin
  --  tmp := scripts.get_arg_value( p_program_id, l_parm);
  --  if tmp is null then
  --    l_tmp_message := get_oasis_msg_gen(210, l_parm); --Parameter [%%%%%] not found (ops_scripts/_parms)
  --    --raise_fatal_error;
  --    raise_fatal_error(p_do_raise => true);
  --  end if;
  --
  --  g_region_id := upper(tmp); --all upper
  --end;
  --endmlz - 15sep2015

  --level 1 ....................................................................
  procedure get_parm_nb_days_kept
  is
    l_parm ops_scripts_parms.parm_name%type := 'NB_DAYS_KEPT';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);
    if l_tmp is null then
      write_log(get_oasis_msg_gen(214, l_parm, c_nbdayskept)); --Parameter [%%%%%] not found --> using default: %%%%%
      g_nbdayskept := c_nbdayskept;
    else
      g_nbdayskept := to_number(l_tmp);
      write_log('['||l_parm||'] is ['||g_nbdayskept||']');
    end if;

  exception when others then
    write_log(get_oasis_msg_gen(218, l_parm, c_nbdayskept)); --Parameter [%%%%%] has an invalid value [%%%%%] --> using default: %%%%%
    g_nbdayskept := c_nbdayskept;
  end;

  --level 1 ....................................................................
  procedure get_parm_load_limit 
  is
    l_parm ops_scripts_parms.parm_name%type := 'LOAD_LIMIT';
    l_tmp  ops_scripts_parms.parm_value%type;
  begin
    l_tmp := scripts.get_arg_value( g_program_id, l_parm);
    if l_tmp is null then
      g_load_limit := null;  --null = no limit
    else
      g_load_limit := to_number(l_tmp);
      write_log('['||l_parm||'] is ['||g_load_limit||']');
      --validation ??
      --if l_limit < 0 then
      --  --invalid
      --  log_step( get_oasis_msg_gen(218, l_parm, l_tmp, c_correction_limit) );
      --  l_limit := c_correction_limit;
      --end if;
    end if;

  exception when others then
    l_tmp_message := get_oasis_msg_gen(219, l_parm); --Parameter [%%%%%] has an invalid value [%%%%%]
    --raise_fatal_error;
    raise_fatal_error(p_do_raise => true);
  end;

  --level 1 ....................................................................
  procedure wait_if_within_excl_period
  is
    ll_exclusion_time_start  date;
    ll_exclusion_time_end    date;

    ll_secs_to_wait binary_integer;
    ll_tmp varchar2(1000);

    --level 2 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    procedure get_parm_EXCL_TIME_START
    is
      l_parm ops_scripts_parms.parm_name%type := 'EXCL_TIME_START';
      l_tmp ops_scripts_parms.parm_value%type;
    begin
      l_tmp := scripts.get_arg_value( g_program_id, l_parm);
      if l_tmp is null
      then
        write_log(get_oasis_msg_gen(214, l_parm, c_exclusion_time_start)); --Parameter [%%%%%] not found --> using default: %%%%%
        ll_exclusion_time_start := to_date(to_char(sysdate, 'yyyymmdd')||c_exclusion_time_start, 'yyyymmdd'||c_exclusion_time_format);
      else
        ll_exclusion_time_start := to_date(to_char(sysdate, 'yyyymmdd')||l_tmp, 'yyyymmdd'||c_exclusion_time_format);
      end if;

      write_log('['||l_parm||'] is ['||to_char(ll_exclusion_time_start, 'yyyy/mm/dd hh24:mi:ss')||']');
    exception when others then
      ll_exclusion_time_start := to_date(to_char(sysdate, 'yyyymmdd')||c_exclusion_time_start, 'yyyymmdd'||c_exclusion_time_format);
      write_log(get_oasis_msg_gen(218, l_parm, l_tmp, c_exclusion_time_start)); --Parameter [%%%%%] has an invalid value [%%%%%] --> using default: %%%%%
    end;
    --level 2 . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
    procedure get_parm_EXCL_TIME_END
    is
      l_parm ops_scripts_parms.parm_name%type := 'EXCL_TIME_END';
      l_tmp ops_scripts_parms.parm_value%type;
    begin
      l_tmp := scripts.get_arg_value( g_program_id, l_parm);
      if l_tmp is null
      then
        write_log(get_oasis_msg_gen(214, l_parm, c_exclusion_time_end)); --Parameter [%%%%%] not found --> using default: %%%%%
        ll_exclusion_time_end := to_date(to_char(sysdate, 'yyyymmdd')||c_exclusion_time_end, 'yyyymmdd'||c_exclusion_time_format);
      else
        ll_exclusion_time_end := to_date(to_char(sysdate, 'yyyymmdd')||l_tmp, 'yyyymmdd'||c_exclusion_time_format);
      end if;

      write_log('['||l_parm||'] is ['||to_char(ll_exclusion_time_end, 'yyyy/mm/dd hh24:mi:ss')||']');
    exception when others then
      ll_exclusion_time_end := to_date(to_char(sysdate, 'yyyymmdd')||c_exclusion_time_end, 'yyyymmdd'||c_exclusion_time_format);
      write_log(get_oasis_msg_gen(218, l_parm, l_tmp, c_exclusion_time_end)); --Parameter [%%%%%] has an invalid value [%%%%%] --> using default: %%%%%
    end;

  begin
    get_parm_EXCL_TIME_START;
    get_parm_EXCL_TIME_END;

    ll_secs_to_wait := 15;

    loop
      exit when sysdate < ll_exclusion_time_start;
      exit when sysdate > ll_exclusion_time_end;

      ll_tmp := 'Now ('||to_char(sysdate, 'yyyy/mm/dd hh24:mi:ss')||') is between ('||to_char(ll_exclusion_time_start, 'yyyy/mm/dd hh24:mi:ss')||') and ('||to_char(ll_exclusion_time_end, 'yyyy/mm/dd hh24:mi:ss')||') --> wait for '||ll_secs_to_wait||' seconds';

      write_log(ll_tmp);
      set_parent_remarks(ll_tmp);
      commit;

      dbms_lock.sleep(ll_secs_to_wait);
    end loop;

  end;
  --endmlz - 13sep2015

  --level 1 ....................................................................
  procedure next_program_id_done_manually
  is
  fct varchar2(100) := 'next_program_id_done_manually';
    l_current_site_date date;
    l_pgmid        ops_scripts.program_id%type;
    l_aft_tag      tms_running_parms.aft_tag_code%type;
    l_tms_cmd      aft_tags.tms_auto_command%type;
    l_queue        tms_running_parms.oasis_queue_type_code%type;
    r_oasis_queue  oasis_queue%rowtype;
    
    ll_tmp          varchar2(4000);
  begin
    select s.last_price_date
    into l_current_site_date
    from site_controls s
    ;

    if l_current_site_date = constants.get_site_date then
      write_log('Site date has not changed (still ['||to_char(l_current_site_date, 'yyyy/mm/dd')||'] --> let the regular mechanism (next_program_id) launch the maintenance');
    else
      write_log('Site date has changed (old ['||to_char(constants.get_site_date, 'yyyy/mm/dd')||'], new ['||to_char(l_current_site_date, 'yyyy/mm/dd')||'] --> launch the maintenance manually');

      --mlz - 13sep2015
      --update the site date for the session
      constants.g_site_date := l_current_site_date;
      --endmlz - 13sep2015

      --do the next_program manually (the system cannot handle a site date change for now)
      begin
        select next_program_id
        into l_pgmid
        from tms_running_parms
        where program_id = g_program_id
        ;
        write_log('  -->> maintenance pgmid ['||l_pgmid||']');
      exception when others then
        ll_tmp := 'tms_running_parms row not found for current pgmid ['||g_program_id||']??!! '||chr(10)||dbms_utility.format_error_stack||dbms_utility.format_error_backtrace;
        raise;
      end;
        
      if l_pgmid is null then
        ll_tmp := '  -->> maintenance pgmid not set!!!  --> cannot launch the maintenance!!!';
        raise program_error;  --not a real error! just get out
      end if;


      begin    
        select aft_tag_code, tms_auto_command, nvl(oasis_queue_type_code, 'A')
        into l_aft_tag, l_tms_cmd, l_queue
        from tms_running_parms, aft_tags
        where program_id = l_pgmid
        and aft_tag_code = code
        ;
        write_log('  -->> maintenance aft_tag ['||l_aft_tag||']');
      exception when others then
        ll_tmp := 'tms_running_parms row not found for maintenance pgmid ['||g_program_id||']??!! '||chr(10)||dbms_utility.format_error_stack||dbms_utility.format_error_backtrace;
        raise;
      end;
      
      r_oasis_queue := null;
      r_oasis_queue.oasis_job    := l_aft_tag;
      r_oasis_queue.site_date    := l_current_site_date;
      r_oasis_queue.window_starting_date  := sysdate;
      r_oasis_queue.tms_auto_command      := l_tms_cmd;
      r_oasis_queue.oasis_queue_type_code := l_queue;
      r_oasis_queue.submit_date  := null;
      r_oasis_queue.last_user    := g_user;
      r_oasis_queue.last_stamp   := sysdate;
      r_oasis_queue.program_id   := l_pgmid;
      r_oasis_queue.confirm_date := null;
      
      write_log('  -->> insert into oasis_queue');
      insert into oasis_queue values r_oasis_queue;
      commit;
      write_log('  -->> done');
      
    end if;
  exception when others then
    if ll_tmp is null then
      ll_tmp := dbms_utility.format_error_stack||dbms_utility.format_error_backtrace;
    end if;
    
    write_log(ll_tmp);
  end;

  --level 1 ....................................................................
  procedure dummy_proc is
  begin
    null;
  end;
--level 0 -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -
begin --{
  Pgmloc := 1820;
  --p_success       :=  false;
  p_message       :=  null;

  --mlz - 15sep2015
  g_at_least_one_child_failed := false;
  --endmlz - 15sep2015
  

  --mlz - 14aug2015 - in common_beginning now...
  --g_user := constants.get_user_id;
  --
  --g_pricing_msgs := null;
  --g_Cntl_Rec := null;
  --g_program_id := null;
  --
  --
  --
  --g_pricing_msgs := null;
  --g_pricing_msgs_child1 := null;  --just to make sure
  --
  --
  --if p_program_id > 9999
  --then
  --  g_program_id := trunc( p_program_id / 100 );
  --else
  --  g_program_id := p_program_id;
  --end if;
  --
  --
  ----PARM
  --get_parm_log;
  --
  ----set dir name for log file (will be changed later for data files !!!)
  --g_cntl_rec.dir_name      := c_oradir_root;
  --
  --begin
  --  select directory_path
  --  into g_current_file_dir
  --  from all_directories
  --  where directory_name = g_cntl_rec.dir_name
  --  ;
  --exception when others then
  --  --v_tmp_message := 'Directory ['||p_cntl_rec.dir_name||'] not found!!';
  --  l_tmp_message := get_oasis_msg_gen(220, g_cntl_rec.dir_name);
  --  --raise_fatal_error;
  --  raise_fatal_error(p_do_raise => true);
  --end;
  --g_current_file_dir := get_directory(g_current_oradir
  --
  --open_log(g_cntl_rec.log_file_name, g_cntl_rec.dir_name);
  --
  --
  --select SYS_CONTEXT('USERENV','SID')
  --into g_sid
  --from dual
  --;
  --write_log('SID ['||g_sid||']');
  --
  --
  --if p_load_date is null then
  --  g_load_date := constants.get_site_date;
  --else
  --  g_load_date := p_load_date;
  --end if;
  --
  --
  --Pgmloc := 0000;
  --if p_commit_freq is null then
  --  g_commit_freq := c_commit_freq_def;
  --else
  --  g_commit_freq := p_commit_freq;
  --end if;
  --write_log('Commit freq is ['||g_commit_freq||']');
  --
  --endmlz - 14aug2015
  
  reset_counters;



  Pgmloc := 1840;
  --mlz - 14aug2015 - now done in common_beginning
  -- --
  -- -- used local version to allow earlier LOG creation
  -- --
  -- process_beginning( g_program_id
  --                  , g_load_date
  --                  --, g_commit_freq
  --                  , g_cntl_rec
  --                  , g_pricing_msgs
  --                  , l_tmp_success
  --                  , l_tmp_message
  --                  );
  -- if not l_tmp_success then raise_fatal_error; end if;
  common_beginning(
    p_program_id, 
    to_date(p_load_date, c_load_date_format),
    to_date(p_date_of_msg, c_date_of_msg_format),
    l_tmp_message
    );
  --endmlz - 14aug2015

--  --mlz - 02jul2013
--  g_log_det := manage_prc_msgs_det.get_log_to_file_handle(
--    g_cntl_rec.log_file,
--    chr(9)
--    );
--  --endmlz - 02jul2013


  --PARM
  get_parm_log_all;
  get_parm_logfile_sep;

  --mlz - 15sep2015 - moved to common beginning
  ----PARM
  --get_parm_file_type;
  --
  ----PARM
  --get_parm_region_id;
  --endmlz - 15sep2015

  --PARM
  get_parm_nb_days_kept;

  --PARM
  get_parm_load_limit;

  Pgmloc := 1890;
  open  get_dss( g_program_id );
  Pgmloc := 1900;
  fetch get_dss into r_dss;
  Pgmloc := 1910;
  close get_dss;
  if r_dss.program_id is null
  then
    --g_pricing_msgs_rec.err_text := 'Data supply_schedule not found ?!';
    l_tmp_message := get_oasis_msg(30);
    --raise_fatal_error;
    raise_fatal_error(p_do_raise => true);
  end if;


  --mlz - 14aug2015 - now in parameter: OUT_FILENAME, init in common_beginning()
  -- --
  -- -- file name
  -- -- >> identical for all children (as they all have their own directory)
  -- --
  -- -- D and Z: <suppl>_<deltime>_YYYY-MM-DD.dat
  -- -- M:       <suppl>_<deltime>_YYYY-MM.dat
  -- case g_file_type
  -- when c_file_type_daily then
  --   g_cntl_rec.in_file_name := upper(r_dss.data_supplier_code)
  --                           || r_dss.delivery_time
  --                           || '_'
  --                           || to_char(g_load_date, c_load_date_daily)
  --                           || '.dat'
  --                            ;
  -- when c_file_type_monthly then
  --   --no DAY in the date!!
  --   g_cntl_rec.in_file_name := upper(r_dss.data_supplier_code)
  --                           || r_dss.delivery_time
  --                           || '_'
  --                           || to_char(g_load_date, c_load_date_monthly)
  --                           || '.dat'
  --                            ;
  -- when c_file_type_delta then
  --   g_cntl_rec.in_file_name := upper(r_dss.data_supplier_code)
  --                           || r_dss.delivery_time
  --                           || '_'
  --                           || to_char(g_load_date, c_load_date_daily)
  --                           || '.dat'
  --                            ;
  -- end case;
  --endmlz - 14aug2015

  --
  Pgmloc := 1920;
  load_region(  p_program_id
              --, g_cntl_rec
              , l_tmp_success
              );
  --if not l_tmp_success then raise_fatal_error; end if;
  raise_fatal_error(p_do_raise => not l_tmp_success);





  --
  --mlz - 14aug2015
  -- if g_cntl_rec.out_file_open
  -- then
  --   Pgmloc := 1930;
  --   load_supplier_rules.file_close( g_cntl_rec.out_file
  --                                 , g_pricing_msgs
  --                                 , l_tmp_success
  --                                 );
  --   --if ( not l_tmp_success ) then raise_fatal_error; end if;
  --   raise_fatal_error(p_do_raise => not l_tmp_success);
  --   g_cntl_rec.out_file_open := false;
  -- end if;
  --endmlz - 14aug2015
  
  
  --
  -----------------------------------------------------
  /* Set Rerun Date to NULL in Product_Running_Parms */
  /* and update last_run_date.                       */
  -----------------------------------------------------
  Pgmloc := 1940;
  if p_program_id = g_program_id
  then
    update product_running_parms
    set    rerun_date    = null,
           last_run_date = sysdate
    where  program_id    = p_program_id;
  end if;
  
  --mlz - 13sep2015
  wait_if_within_excl_period;
  --endmlz - 13sep2015

  --mlz - 07jul2015
  next_program_id_done_manually;
  --endmlz - 07jul2015

  --
  --mlz - 15sep2015
  if g_at_least_one_child_failed = false then
  --endmlz - 15sep2015
  --------------------------------------------
  /* Update pricing_msgs for the last time..*/
  --------------------------------------------
  Pgmloc := 1950;
  g_pricing_msgs.msg_text := get_oasis_msg_cmp( 120);
  Pgmloc := 1960;
  g_pricing_msgs.msg_text_2 := get_oasis_msg_cmp( 122);
  g_pricing_msgs.successfull_flag := 'Y';
  --p_success   := true;
  end if;

--<<upd_prc_msg>>
  Pgmloc := 1970;
  write_log( g_pricing_msgs.msg_text );
  Pgmloc := 1980;
  l_tmp_message := get_oasis_msg_cmp( 112, to_char( sysdate, 'dd-mon-yyyy hh24:mi:ss' ) );
  Pgmloc := 1990;
  write_log( l_tmp_message );
  Pgmloc := 2000;
  load_supplier_rules.file_close( g_log
                                , g_pricing_msgs
                                , l_tmp_success
                                );
  --
  Pgmloc := 2010;
  load_supplier_rules.update_prc_msgs( g_pricing_msgs.program_id
                                     , g_pricing_msgs.date_of_msg
                                     , g_pricing_msgs
                                     , l_tmp_success
                                     );
  Pgmloc := 2020;
  commit;
  --
EXCEPTION
WHEN OTHERS THEN
  --get msg FIRST to not loose stack
  l_tmp_message := get_error_msg_parent(l_tmp_message);

  g_pricing_msgs.Err_Text := substr( l_tmp_message, 1, 255 );

  g_pricing_msgs.remarks := 'Error at pgmloc ' || pgmloc;

  g_pricing_msgs.msg_text := get_oasis_msg_cmp(130);
  g_pricing_msgs.msg_text_2 := get_oasis_msg_cmp(132);
  g_pricing_msgs.successfull_flag := 'N';

  Upd_Prc_Msgs(false);
--


  if ( utl_file.is_open( g_log ) )
  then
    begin
      write_log( l_tmp_message );
    exception when others then
      l_tmp_message := l_tmp_message||' // '||dbms_utility.format_error_stack
        ||dbms_utility.format_error_backtrace
        ;
    end;
  end if;

  --P_Success := FALSE;
  P_Message := l_tmp_message;

  utl_file.fclose_all;
  
  --mlz - 14aug2015
  raise;
  --endmlz - 14aug2015

end; --}}
-----------------------------------------------------------------------
end csv_to_msef; --}
/
show error
