CREATE OR REPLACE PACKAGE BODY &&owner..Client IS
--
-- Ce package contient des procedures et fonctions varies
-- sur les clients
-- Marie (26 nov 1997)
-- Requete: 4261
--
-- SSI   09JUL2003  - Ajout functions
--                    - Get_Clt_List (de standalone function)
--                    - Get_Incl_Client_List (de standalone function)
--                    - Get_Real_Clt_List (Nouvelle function)
-- SA    25JUL2003  - Changed the function Get_Real_Clt_List
--                    Replaced one cursor that was slow by another one.
-- JSL - 20OCT2004  - Get_Real_Clt_List does not returns the SVS clients
--                    when sources does not exist for the security.
--
-- PSM - 30SEP2005  - Explicitly using 'ORDER BY' as 'Distinct' doesnot sort in
--                    new Database release 10gR2.
--
-- RV  - 04JUN2009  - Fix Validate_Client_FK - Only the not null need to
--                    be verified. Was working in 10gR2.
--
-- JSL - 26Jan2016  - OASIS-2662
--                  - Put the error message in error_text in 
--                    Validate_Clients_FK exception handler
--
------------------------------------------------------------------------
FUNCTION Get_Code
    (P_Fri_Client IN Clients.Fri_Client%TYPE)
RETURN VARCHAR2
IS
--
CURSOR Cur_Get_Clt_Code
   (P_Fri_Client Clients.Fri_Client%TYPE)
IS
   SELECT C.Code
     FROM Clients C
    WHERE C.Fri_Client = P_Fri_Client;
--
Out_Clt_Code  Clients.Code%TYPE;
--
BEGIN
--
   OPEN Cur_Get_Clt_Code(P_Fri_Client);
   FETCH Cur_Get_Clt_Code INTO Out_Clt_Code;
   CLOSE Cur_Get_Clt_Code;
   --
   RETURN Out_Clt_Code;
END Get_Code;
------------------------------------------------------------------------
FUNCTION GET_CLT_LIST(
     p_fri_ticker    Securities.Fri_ticker%TYPE,
     p_product_code  products.code%TYPE := NULL,
     p_sub_product   sub_products.sub_product_code%TYPE := NULL,
     p_source_code   sources.code%TYPE := NULL,
     p_fri_client    clients.fri_client%TYPE := NULL)
RETURN VARCHAR2 IS
--
-- Cette fonction ramene la liste des clients possedant le titre passe
-- en parametre. Si le produit (ou le produit et le sous produit) sont
-- passes, elle ramene les clients de ce produit (sous-produit)
-- possedant le titre. Sinon, elle ramene NULL
--
--
-- Ce curseur ramene tous les codes de clients possedant le titre
-- passe en parametre. Si un fri_client test recu en parametre, seul
-- ce client est considere.
--
-- Nous devons verifier la table Clients_Xref_Tables aussi
-- pour voir si il y une valeur dans le champs
-- Fri_Client_Xref
-- Marie (14 mars 1997)
-- Req: 3772
--
   CURSOR CUR_CLT_UNIV IS
   SELECT distinct
          B.code cde
   FROM   clients B,
          fri_security_status F,
          clients_univ A
   WHERE  A.fri_ticker              = p_fri_ticker
     AND  A.fri_security_status_code= F.code
     AND  F.delete_flag             = 'N'
     AND  A.fri_client              = B.fri_client
     AND  A.fri_client              = NVL(p_fri_client,A.fri_client)
     AND (p_source_code is null OR
             EXISTS
                 (SELECT 'X'
                    FROM CLIENTS_UNIV_SOURCES S
                   WHERE S.fri_client         = A.fri_client
                     AND S.client_security_id = A.client_security_id
                     AND (S.delete_flag      != 'N' OR
                          S.delete_flag IS NULL)
                     AND S.client_source_code = p_source_code) )
   ORDER BY 1;
--
-- Ce curseur ramene tous les codes des clients d'un produit/
-- sous-produit possedant le titre passe en parametre. Si un
-- fri_client est recu en parametre, seul ce client est considere
--
   CURSOR CUR_CLT_UNIV_PROD IS
   SELECT /*+ INDEX(C) */ distinct
          B.code cde
   FROM   product_running_parms C,
          clients B,
          fri_security_status   F,
          clients_univ A
   WHERE  A.fri_ticker              = p_fri_ticker
     AND  A.fri_security_status_code= F.code
     AND  F.delete_flag             = 'N'
     AND  A.fri_client              = B.fri_client
     AND  A.fri_client              = C.fri_client
     AND  A.fri_client              = NVL(p_fri_client,A.fri_client)
     AND  C.product_code            = p_product_code
     AND (C.sub_product_code = p_sub_product
         OR p_sub_product IS NULL)
     AND (p_source_code is null OR
             EXISTS
                 (SELECT 'X'
                    FROM CLIENTS_UNIV_SOURCES S
                   WHERE S.fri_client         = A.fri_client
                     AND S.client_security_id = A.client_security_id
                     AND (S.delete_flag      != 'N' OR
                          S.delete_flag IS NULL)
                     AND S.client_source_code = p_source_code) )
   ORDER BY 1;
--
--  Ce curseur regarde si le client detient la source passee en
--  parametre.
--
   v_clt_list VARCHAR2(1024);
   c_max_len  NUMBER:=1024;
--
BEGIN
--
-- Si les parametres Product et Sub_Product sont nulls,
-- On va chercher la liste de tous les clients ayant ce titre
--
   v_clt_list := '';
   IF p_product_code IS NULL AND p_sub_product IS NULL THEN
      FOR tmp_clt IN CUR_CLT_UNIV LOOP
         v_clt_list := v_clt_list || tmp_clt.cde || ' ';
         IF LENGTH(v_clt_list) > c_max_Len THEN
            EXIT;
         END IF;
      END LOOP;
   ELSE
      FOR tmp_clt IN CUR_CLT_UNIV_PROD LOOP
         v_clt_list := v_clt_list || tmp_clt.cde || ' ';
         IF LENGTH(v_clt_list) > c_max_len THEN
            EXIT;
         END IF;
      END LOOP;
   END IF;
   IF RTRIM(v_clt_list) = '' THEN
      v_clt_list := NULL;
   END IF;
   RETURN(v_clt_list);
   EXCEPTION
      WHEN OTHERS THEN RAISE;
END Get_Clt_List;
------------------------------------------------------------------------
FUNCTION GET_INCL_CLT_LIST(
     p_fri_ticker    Securities.Fri_ticker%TYPE,
     p_ext_prod_flag VARCHAR2 := 'Y',
     p_product_code  products.code%TYPE := NULL,
     p_sub_product   sub_products.sub_product_code%TYPE := NULL,
     p_source_code   sources.code%TYPE := NULL,
     p_fri_client    clients.fri_client%TYPE := NULL)
RETURN VARCHAR2 IS
--
-- Cette fonction ramene la liste des clients possedant le titre passe
-- en parametre. Si le produit (ou le produit et le sous produit) sont
-- passes, elle ramene les clients de ce produit (sous-produit)
-- possedant le titre. Sinon, elle ramene NULL
--
   CURSOR CUR_INCL_CLT_UNIV(p_incl_clt Clients.Fri_Client%TYPE) IS
     SELECT /*+ INDEX(R) */
            C.Code
       FROM Clients C, Products P,
            Product_Running_Parms R, Client_List_Products L
      WHERE L.Include_Fri_Client = p_incl_clt
        AND L.Active_Flag        = 'Y'
        AND R.Fri_Client         = L.Fri_Client
        AND R.Active_Flag        = 'Y'
        AND (p_ext_prod_flag = 'N' OR
               (P.Code         = R.Product_Code AND
                P.Pricing_Flag = 'Y'))
        AND C.Fri_Client   = L.Fri_Client
   UNION
     SELECT A.Code
       FROM Clients A
      WHERE NVL(A.Fri_Client_Universe,A.Fri_Client) = p_incl_clt
        AND EXISTS
              (SELECT /*+ INDEX(R) */ 'X'
                 FROM Products P, Product_Running_Parms R
                WHERE R.Fri_Client     = A.Fri_Client
                  AND R.Active_Flag    = 'Y'
                  AND (p_ext_prod_flag = 'N' OR
                       (P.Code         = R.Product_Code AND
                        P.Pricing_Flag = 'Y')));
--
-- Ce curseur ramene tous les codes de clients possedant le titre
-- passe en parametre. Si un fri_client test recu en parametre, seul
-- ce client est considere.
--
-- Nous devons verifier la table Clients_Xref_Tables aussi
-- pour voir si il y une valeure dans le champs Fri_Client_Xref
--
   CURSOR CUR_CLT_UNIV IS
   SELECT distinct A.Fri_Client
   FROM   fri_security_status F, clients_univ A
   WHERE  A.fri_ticker              = p_fri_ticker
     AND  A.fri_security_status_code= F.code
     AND  F.delete_flag             = 'N'
     AND  A.fri_client              = NVL(p_fri_client,A.fri_client)
     AND (p_source_code is null OR
             EXISTS
                 (SELECT 'X'
                    FROM CLIENTS_UNIV_SOURCES S
                   WHERE S.fri_client         = A.fri_client
                     AND S.client_security_id = A.client_security_id
                     AND (S.delete_flag      != 'N' OR
                          S.delete_flag IS NULL)
                     AND S.client_source_code = p_source_code) )
   ORDER BY 1;
--
-- Ce curseur ramene tous les codes des clients d'un produit/
-- sous-produit possedant le titre passe en parametre. Si un
-- fri_client est recu en parametre, seul ce client est considere
--
   CURSOR CUR_CLT_UNIV_PROD IS
   SELECT DISTINCT B.Fri_Client
   FROM   Product_Running_Parms C, Clients B,
          Fri_Security_Status   F, Clients_Univ A,
          Products P
   WHERE  A.fri_ticker              = p_fri_ticker
     AND  A.fri_security_status_code= F.code
     AND  F.delete_flag             = 'N'
     AND  A.fri_client              = B.fri_client
     AND  A.fri_client              = C.fri_client
     AND  A.fri_client              = NVL(p_fri_client,A.fri_client)
     AND  C.product_code            = p_product_code
     AND  (p_ext_prod_flag = 'N' OR
                (P.Code         = C.Product_Code AND
                 P.Pricing_Flag = 'Y'))
     AND (C.sub_product_code = p_sub_product
         OR p_sub_product IS NULL)
     AND (p_source_code is null OR
             EXISTS
                 (SELECT 'X'
                    FROM CLIENTS_UNIV_SOURCES S
                   WHERE S.fri_client         = A.fri_client
                     AND S.client_security_id = A.client_security_id
                     AND (S.delete_flag      != 'N' OR
                          S.delete_flag IS NULL)
                     AND S.client_source_code = p_source_code) )
     ORDER BY 1;
--
--  Ce curseur regarde si le client detient la source passee en
--  parametre.
--
   v_clt_list VARCHAR2(1024);
   c_max_Len  NUMBER:=1024;
--
   PROCEDURE Insert_String(dest_str  IN OUT  VARCHAR2,
                           symbol    IN      VARCHAR2,
                           delimiter IN      VARCHAR2,
                           max_len   IN      NUMBER) IS
      pos_found  BOOLEAN;
      start_pos  NUMBER;
      end_pos    NUMBER;
      trailer    VARCHAR2(1024); -- Doit etre au moins du meme ordre
                                 -- de grandeur que 'max_len' et/ou
                                 -- 'dest_str'.
   BEGIN
      -- On sort si on a atteint la longueur maximale ou
      -- si le 'symbol' existe deja dans 'dest_str'.
      --
      IF INSTR(dest_str,symbol||delimiter) <> 0 THEN
         GOTO Get_Out;
      END IF;
      IF LENGTH(dest_str) > max_len THEN
         GOTO Get_Out;
      END IF;
      --
      -- Traitement special pour la premiere fois
      --
      IF LENGTH(dest_str) = 0 THEN
         dest_str := symbol || delimiter;
         GOTO Get_Out;
      END IF;
      --
      -- Determiner la position de l'insertion
      --
      pos_found := FALSE;
      start_pos := 1;
      end_pos   := INSTR(dest_str,delimiter,start_pos,1);
      WHILE (end_pos <> 0) AND (NOT pos_found) LOOP
         IF SUBSTR(dest_str,start_pos,end_pos-start_pos)>symbol THEN
            pos_found := TRUE;
         ELSE
            start_pos := end_pos+1;
            IF start_pos > LENGTH(dest_str) THEN
               EXIT;
            END IF;
            end_pos   := INSTR(dest_str,delimiter,start_pos,1);
         END IF;
      END LOOP;
      --
      -- Inserer le symbole a la position calculee (avant start_pos)
      -- sinon, on l'insere a la fin.
      --
      IF pos_found THEN
         trailer  := SUBSTR(dest_str,start_pos,
                                     LENGTH(dest_str)-start_pos+1);
         IF (start_pos-1) = 0 THEN
            start_pos := 1;
         END IF;
         dest_str := SUBSTR(dest_str,1,start_pos-1) ||
                     symbol    ||
                     delimiter ||
                     trailer;
      ELSE
         dest_str := dest_str  ||
                     symbol    ||
                     delimiter;
      END IF;
   <<Get_Out>>
      NULL;
   END Insert_String;
BEGIN
--
-- Si les parametres Product et Sub_Product sont nulls,
-- On va chercher la liste de tous les clients ayant ce titre
--
   v_clt_list := '';
   IF p_product_code IS NULL AND p_sub_product IS NULL THEN
      FOR tmp_clt IN CUR_CLT_UNIV LOOP
         FOR tmp_incl_clt IN CUR_INCL_CLT_UNIV(
                                            tmp_clt.fri_client) LOOP
            insert_string(v_clt_list,tmp_incl_clt.code,' ',c_max_Len);
            IF LENGTH(v_clt_list) > c_max_Len THEN
               EXIT;
            END IF;
         END LOOP;
      END LOOP;
   ELSE
      FOR tmp_clt IN CUR_CLT_UNIV_PROD LOOP
         FOR tmp_incl_clt IN CUR_INCL_CLT_UNIV(
                                            tmp_clt.fri_client) LOOP
            insert_string(v_clt_list,tmp_incl_clt.code,' ',c_max_Len);
            IF LENGTH(v_clt_list) > c_max_len THEN
               EXIT;
            END IF;
         END LOOP;
      END LOOP;
   END IF;
   IF RTRIM(v_clt_list) = '' THEN
      v_clt_list := NULL;
   END IF;
   RETURN(v_clt_list);
   EXCEPTION
      WHEN OTHERS THEN RAISE;
END Get_Incl_Clt_List;
------------------------------------------------------------------------
FUNCTION GET_REAL_CLT_LIST(
     p_fri_ticker    Securities.Fri_ticker%TYPE,
     p_product_code  products.code%TYPE := NULL,
     p_sub_product   sub_products.sub_product_code%TYPE := NULL,
     p_source_code   sources.code%TYPE := NULL,
     p_fri_client    clients.fri_client%TYPE := NULL)
RETURN VARCHAR2 IS
--
-- Cette fonction ramene la liste des vrais clients possedant le titre
-- en parametre. Si le produit (ou le produit et le sous produit) sont
-- passes, elle ramene les clients de ce produit (sous-produit)
-- possedant le titre. Sinon, elle ramene NULL
--
--
-- Ce curseur ramene tous les codes de clients possedant le titre
-- passe en parametre. Si un fri_client est recu en parametre, seul
-- ce client est considere.
--
   CURSOR CUR_CLT_UNIV IS
   SELECT distinct
          B.code cde,
          B.fri_client
   FROM   clients B,
          fri_security_status F,
          clients_univ A
   WHERE  A.fri_ticker              = p_fri_ticker
     AND  A.fri_security_status_code= F.code
     AND  F.delete_flag             = 'N'
     AND  B.Real_Client_Flag        = 'Y'
     AND  A.fri_client              = B.fri_client
     AND  A.fri_client              = NVL(p_fri_client,A.fri_client)
     AND  ( p_source_code is null
         OR EXISTS (
            SELECT 'X'
              FROM CLIENTS_UNIV_SOURCES S
             WHERE S.fri_client         = A.fri_client
               AND S.client_security_id = A.client_security_id
               AND ( S.delete_flag      != 'N'
                  OR S.delete_flag IS NULL
                   )
               AND S.client_source_code = p_source_code
            )
-- Ajoute - JSL - 20 octobre 2004
         OR NOT EXISTS (
            SELECT 'X'
              FROM CLIENTS_UNIV_SOURCES S
             WHERE S.fri_client         = A.fri_client
               AND S.client_security_id = A.client_security_id
            )
-- Fin d'ajout - JSL - 20 octobre 2004
          )
   ORDER BY 1,2;
--
-- Ce curseur ramene tous les codes des clients d'un produit/
-- sous-produit possedant le titre passe en parametre. Si un
-- fri_client est recu en parametre, seul ce client est considere
--
--
-- CURSOR CUR_CLT_UNIV_PROD IS
-- SELECT distinct
--        Clt.code cde
-- FROM   product_running_parms C,
--        clients B,
--        clients Clt,
--        fri_security_status   F,
--        clients_univ A
-- WHERE  A.fri_ticker              = p_fri_ticker
--   AND  A.fri_security_status_code= F.code
--   AND  F.delete_flag             = 'N'
--   AND  A.fri_Client              = NVL(B.Fri_Client_Universe,B.Fri_Client)
--   AND  B.Fri_Client              = C.Fri_Client
--   AND  Clt.Fri_Client            = A.Fri_Client
--   AND  Clt.Real_Client_Flag      = 'Y'
--   AND  A.fri_client              = NVL(p_fri_client,A.fri_client)
--   AND  C.product_code            = p_product_code
--   AND (C.sub_product_code = p_sub_product
--       OR p_sub_product IS NULL)
--   AND (p_source_code is null OR
--           EXISTS
--               (SELECT 'X'
--                  FROM CLIENTS_UNIV_SOURCES S
--                 WHERE S.fri_client         = A.fri_client
--                   AND S.client_security_id = A.client_security_id
--                   AND (S.delete_flag      != 'N' OR
--                        S.delete_flag IS NULL)
--                   AND S.client_source_code = p_source_code) );
--
--  Ce curseur regarde si le client detient la source passee en
--  parametre.
--
-- Changed SA 25jul2003
-- Check if the client exists for the given product in the respective
-- client universe. The Select is split in order to enhance the speed.
--
   CURSOR CUR_CLT_UNIV_PROD ( inp_fri_client  IN NUMBER) IS
   SELECT 'X'
     FROM DUAL
    WHERE EXISTS (
          SELECT 1
            FROM Product_Running_Parms C,
                 Clients B
           WHERE inp_fri_client       = B.Fri_Client_Universe
             AND B.Fri_Client         = C.Fri_Client
             AND C.Product_Code       = p_product_code
             AND ( C.Sub_Product_Code = p_sub_product
                OR p_sub_product IS NULL
                 )
             AND C.Active_Flag = 'Y'
         )
       OR EXISTS (
          SELECT 'X'
            FROM Product_Running_Parms C,
                 Clients B
           WHERE inp_fri_client      = B.Fri_Client
             AND B.Fri_Client_Universe IS NULL
             AND B.Fri_Client        = C.Fri_Client
             AND C.Product_Code      = p_product_code
             AND (C.Sub_Product_Code = p_sub_product
                OR p_sub_product IS NULL
                 )
             AND C.Active_Flag = 'Y'
         );
--
   v_clt_list VARCHAR2(1024);
   c_max_len  NUMBER:=1024;
   v_fri_client  Clients.Fri_Client%TYPE;
   v_exists_flag VARCHAR2(1);
   v_clt_lst_len VARCHAR2( 1024);
--
BEGIN
--
-- Si les parametres Product et Sub_Product sont nulls,
-- On va chercher la liste de tous les clients ayant ce titre
--
   v_clt_list := '';
   IF p_product_code IS NULL AND p_sub_product IS NULL THEN
      FOR tmp_clt IN CUR_CLT_UNIV LOOP
         v_clt_lst_len := v_clt_lst_len + LENGTH(tmp_clt.cde) + 1;
         -- changed SA 25jul2003
         -- The maximum length of the list is checked before to avoid
         -- abnormal termination of the program.
         IF v_clt_lst_len > c_max_len THEN
            dbms_output.put_line('MAX length of ' || TO_CHAR(c_max_len) || ' reached for client list');
            EXIT;
         END IF;
         v_clt_list := v_clt_list || tmp_clt.cde || ' ';
      END LOOP;
   ELSE
-- Changed SA 25jul2003
--      FOR tmp_clt IN CUR_CLT_UNIV_PROD LOOP
      FOR tmp_clt IN CUR_CLT_UNIV LOOP
--         v_clt_list := v_clt_list || tmp_clt.cde || ' ';
         v_fri_client := tmp_clt.fri_client;
         --
         v_exists_flag := NULL;
         OPEN CUR_CLT_UNIV_PROD (v_fri_client);
         FETCH CUR_CLT_UNIV_PROD INTO v_exists_flag;
         CLOSE CUR_CLT_UNIV_PROD;
         --
         IF v_exists_flag = 'X'
         THEN
           v_clt_lst_len := v_clt_lst_len + LENGTH(tmp_clt.cde) + 1;
           -- changed SA 25jul2003
           -- The maximum length of the list is checked before to avoid
           -- abnormal termination of the program.
           IF v_clt_lst_len > c_max_len THEN
              dbms_output.put_line('MAX length of ' || TO_CHAR(c_max_len) || ' reached for client list');
              EXIT;
           END IF;
           v_clt_list := v_clt_list || tmp_clt.cde || ' ';
         END IF;
         --
      END LOOP;
   END IF;
   IF RTRIM(v_clt_list) = '' THEN
      v_clt_list := NULL;
   END IF;
   RETURN(v_clt_list);
   EXCEPTION
      WHEN OTHERS THEN RAISE;
END Get_Real_Clt_List;
------------------------------------------------------------------------
PROCEDURE Create_Source_Assignments
  (P_Program_Id          IN  Ops_Scripts.Program_Id%TYPE,
   P_Fri_Client          IN  Clients.Fri_Client%TYPE,
   P_Product_Code        IN  Products.Code%TYPE,
   P_Status              OUT NUMBER,
   P_Message             OUT VARCHAR2)
IS
--
--  Cette Procedure sert a creer toutes les rangees de Source_
--  Assignments pour le client et le produit specifie.
--  selon les marche definis dans Market_Assignments
--  Marie (20 nov 1997)
--
-- NA must be excluded from the list
-- Richard (12 Jan 2001)
CURSOR Cur_Get_Sources
IS
   SELECT  S.Code
     FROM  Sources S,
           Nations N,
           Markets M,
           Market_Assignments MA
    WHERE  MA.Fri_Client   = P_Fri_Client
      AND  MA.Product_Code = P_Product_Code
      AND  MA.Market_Code  = M.Code
      AND  MA.Active_Flag  = 'Y'
      AND  N.Market_Code   = M.Code
      AND  S.Nation_Code   = N.Code
      AND  S.Code         <> 'NA';
--
--
CURSOR Get_Client_Code
IS
   SELECT C.Code
     FROM Clients C
    WHERE C.Fri_Client = P_Fri_Client;
--
--  Declaration des Variables
--
Rows_Read               NUMBER := 0;
Rows_Inserted           NUMBER := 0;
Rows_Rejected           NUMBER := 0;
Rows_Updated            NUMBER := 0;
Records_Committed       NUMBER := 0;
--
pgmloc                  NUMBER;
Update_Break_Pts        BOOLEAN := FALSE;
Client_Code             Clients.Code%TYPE;
Set_Priority_Order      NUMBER := 999;
Set_Active_Flag         Yes_No.Code%TYPE := 'Y';
Pricing_Msg_Rec         Pricing_Msgs%ROWTYPE;
--
Success                 VARCHAR2(1) := 'Y';
Failure                 VARCHAR2(1) := 'N';
Current_Date            DATE := SYSDATE;
No_Data                 EXCEPTION;
--
BEGIN
   --
   pgmloc := 100;
   --
   OPEN Get_Client_Code;
   FETCH Get_Client_Code INTO Client_Code;
   CLOSE Get_Client_Code;
   --
   pgmloc := 110;
   Global_File_Updates.Initialize_Pricing_Msgs
     (P_Program_Id,
      Current_Date,
      Update_Break_Pts,
      Pricing_Msg_Rec);
   --
   Pricing_Msg_Rec.Msg_Text :=  'Creating Source Assignments for ' ||
                                'Client = '  || Client_Code || ', ' ||
                                'Product = ' || P_Product_Code;
   Pricing_Msg_Rec.Msg_Text_2 := 'Start of Program';
--
   pgmloc := 130;
   Global_File_Updates.Update_Pricing_Msgs
     (P_Program_Id,
      Current_Date,
      Update_Break_Pts,
      Pricing_Msg_Rec);
   Commit;
--
-- On va a chercher les bourses a creer
--
   pgmloc := 150;
   FOR Source_Rec IN Cur_Get_Sources
   LOOP
      --
      Rows_Read := Rows_Read + 1;
      INSERT INTO Source_Assignments (
         Fri_Client,
         Product_Code,
         Source_Code,
         Priority_Order,
         Active_Flag,
         Last_User,
         Last_Stamp)
     VALUES (
         P_Fri_Client,
         P_Product_Code,
         Source_Rec.Code,
         Set_Priority_Order,
         Set_Active_Flag,
         Constants.Get_User_Id,
         SYSDATE);
      Rows_Inserted := Rows_Inserted + 1;
    END LOOP;
--
   pgmloc       := 200;
   Success      := 'Y';
   Pricing_Msg_Rec.Msg_Text_2 := 'Completed Successfully';
--
   Pricing_Msg_Rec.Value_01 := Rows_Read;
   Pricing_Msg_Rec.Value_02 := Rows_Inserted;
   Pricing_Msg_Rec.Successfull_Flag := Success;
   Global_File_Updates.Update_Pricing_Msgs
       (P_Program_Id,
        Current_Date,
        Update_Break_Pts,
        Pricing_Msg_Rec);
   Commit;
--
EXCEPTION
--
WHEN No_Data
THEN
   Rollback;
   Pricing_Msg_Rec.Err_Text := 'No data to create source assignments' ||
                               ' from.';
   Pricing_Msg_Rec.Successfull_Flag := Failure;
   Global_File_Updates.Update_Pricing_Msgs
       (P_Program_Id,
        Current_Date,
        Update_Break_Pts,
        Pricing_Msg_Rec);
   Commit;
--
WHEN Dup_Val_On_Index
THEN
   Rollback;
   Pricing_Msg_Rec.Err_Text :=  'Source Assignments already exist '  ||
                                'for client = ' || client_code ||', '||
                                'product = '    || p_product_code;
   Pricing_Msg_Rec.Successfull_Flag := Failure;
   Global_File_Updates.Update_Pricing_Msgs
       (P_Program_Id,
        Current_Date,
        Update_Break_Pts,
        Pricing_Msg_Rec);
   Commit;
--
WHEN OTHERS
THEN
   Rollback;
   --
   -- Pout toute autre exceptions on choisit le message d'ORACLE et
   -- son Code
   --
   Pricing_Msg_Rec.Err_Text:= SUBSTR(SQLERRM(SQLCODE),1,68);
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Msg_Text_2:=
       'Unhandled Exception Occurred at pgmloc: ' ||
       TO_CHAR(pgmloc);
   Global_File_Updates.Update_Pricing_Msgs
       (P_Program_Id,
        Current_Date,
        Update_Break_Pts,
        Pricing_Msg_Rec);
   Commit;
--
END Create_Source_Assignments;
-----------------------------------------------------------------------
FUNCTION Client_From_Cl_Xref(
       P_Fri_Client         Clients.fri_client%TYPE,
       P_Table_Name         Clients_Xref_Tables.table_name%TYPE,
       P_Fri_Xref_Code      Clients_Xref.Fri_Xref_Code%TYPE)
RETURN Clients_Xref.Client_Xref_Code%TYPE
IS
   --
   Out_Client_Xref_Code Clients_Xref.Client_Xref_Code%TYPE;
   --
   BEGIN
   --
   SELECT NVL(X.Output_Client_Xref_Code,X.Client_Xref_Code)
     INTO Out_Client_Xref_Code
     FROM Clients_Xref X, Clients_Xref_Tables T
    WHERE T.fri_client = P_Fri_Client
      AND T.table_name = P_Table_Name
      AND X.fri_client = NVL(T.Fri_Client_Xref, T.Fri_Client)
      AND X.Fri_Xref_Code = P_Fri_Xref_Code;
--
      RETURN(Out_Client_Xref_Code);
      --
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
          Out_Client_Xref_Code := NULL;
          RETURN (Out_Client_Xref_Code);
--
   END Client_from_cl_xref;
-----------------------------------------------------------------------
FUNCTION Fri_From_Cl_Xref(
       P_Fri_Client         Clients.fri_client%TYPE,
       P_Table_Name         Clients_Xref_Tables.table_name%TYPE,
       P_Client_Xref_Code   Clients_Xref.Client_Xref_Code%TYPE,
       P_Client_Code        NUMBER := 0 )
RETURN Clients_Xref.Fri_Xref_Code%TYPE
IS
------------------------------------------------------------------------
-- Cette fonction recoit un code de client pour une table et la fonction
-- retourne le code fri correspondant. Notez bien que s'il y a plus
-- qu'un code fri pour un code client, un seul sera retourne.
-- Si le xref du client pointe vers un autre client alors c'est le
-- xref de l'autre client qui sera utilise.
--
-- Requete #4068
--
-- Je ne voie pas le code pour en retourner un seul
-- J'ajoute la fonction MIN
--
-- Richard (08 aout 1997)
--
-- J'ai envoye le
------------------------------------------------------------------------
   --
   Out_Fri_Xref_Code Clients_Xref.Fri_Xref_Code%TYPE;
   --
   BEGIN
   --
   -- Je rajoute MIN pour m'assurer qu'un seul est retourne
   -- le rapport p154a n'a pas fonctionne car plusieurs
   -- rangees ont ete retournees. Voir la requete #4068
   -- Richard (08 aout 1997)
   --
   IF P_Client_Code = 1 THEN
      --
      -- Si le code vient du fichier du client, il faut passer par
      -- le in.
      --
      SELECT MIN(X.Fri_Xref_Code)
        INTO Out_Fri_Xref_Code
        FROM Clients_Xref        X,
             Clients_Xref_Tables T
       WHERE T.fri_client       = P_Fri_Client
         AND T.table_name       = P_Table_Name
         AND X.fri_client       = NVL(T.Fri_Client_Xref, T.Fri_Client)
         AND NVL(X.Input_Client_Xref_Code,X.Client_Xref_Code) =
              P_Client_Xref_Code;
   ELSE
      SELECT MIN(X.Fri_Xref_Code)
        INTO Out_Fri_Xref_Code
        FROM Clients_Xref        X,
             Clients_Xref_Tables T
       WHERE T.fri_client       = P_Fri_Client
         AND T.table_name       = P_Table_Name
         AND X.fri_client       = NVL(T.Fri_Client_Xref, T.Fri_Client)
         AND X.Client_Xref_Code = P_Client_Xref_Code;
    END IF;
--
    RETURN(Out_Fri_Xref_Code);
    --
    EXCEPTION
       WHEN NO_DATA_FOUND THEN
          Out_Fri_Xref_Code := NULL;
          RETURN (Out_Fri_Xref_Code);
--
   END fri_from_cl_xref;
------------------------------------------------------------------------
-- Procedure to validate 'Fri_Clients_Universe' and 'Master_Fri_Client'
-- since the FK on these columns are dropped.
-- Phebe(14 Dec 2005).
--
PROCEDURE Validate_Clients_FK(
               P_Program_ID   IN   Ops_Scripts.Program_ID%TYPE,
               P_Success      OUT  BOOLEAN,
               P_Message      OUT  VARCHAR2)
AS
--
 V_System_Date     DATE;
 Pricing_Msg_Rec   Pricing_Msgs%ROWTYPE;
 V_Message_Code    Oasis_Messages.Code%TYPE;
 V_Del_Flag        Oasis_Messages.Delete_Flag%TYPE := 'N';
 V_Message         Oasis_Messages.Text_E%TYPE;
 V_Message_Val1    VARCHAR2(30);
 V_Message_Val2    VARCHAR2(30);
 V_Message_Val3    VARCHAR2(30);
 V_Clt_Code        Clients.Code%TYPE;
 V_Inv_Fri_Clt     NUMBER;
 Pgmloc            NUMBER;
 Invalid_Found     EXCEPTION;
--
 CURSOR Cur_Master_Fri_Client   IS 
 SELECT Code, Master_Fri_Client 
   FROM Clients
 -- Where clause added (Richard - 04 Jun 2009)
  WHERE Master_Fri_Client is not null
 -- End of Where clause added (Richard - 04 Jun 2009)
    and Master_Fri_Client NOT IN (SELECT B.Fri_Client
                                    FROM Clients B);
--
 CURSOR Cur_Fri_Client_Universe IS 
 SELECT Code, Fri_Client_Universe
   FROM Clients
 -- Where clause added (Richard - 04 Jun 2009)
  WHERE Fri_Client_Universe is not null
 -- End of Where clause added (Richard - 04 Jun 2009)
    AND Fri_Client_Universe NOT IN (SELECT B.Fri_Client
                                      FROM Clients B);
--
BEGIN
    --
    V_System_Date := SYSDATE;
    P_Success     := FALSE;
    P_Message     := '';
    V_Message_Code:= 'PROC-CLIENT-';
    Pgmloc        := 5;
    --
    --------------------------------------------------------
    -- Initalizing pricing_msgs
    --------------------------------------------------------
    Global_File_Updates.Initialize_Pricing_Msgs
               (P_Program_Id,
                V_System_Date,
                False,          
                Pricing_Msg_Rec); 
    Commit;
    --
    Pricing_Msg_Rec.Msg_Text         := 'Validating Master_Fri_Client and '||
                                        'Fri_Client_Universe in Clients';
    Pricing_Msg_Rec.Msg_Text_2       := 'In Progress.' ;
    Pricing_Msg_Rec.Successfull_Flag := 'N';
    --  
    --------------------------------------------------------
    -- Updating pricing_msgs
    --------------------------------------------------------
    Global_File_Updates.Update_Pricing_Msgs
               (P_Program_Id,
                V_System_Date,
                False,             
                Pricing_Msg_Rec);
    Commit;
    Pgmloc := 10;
    -------------------------------------------------------
    -- Validating Master_Fri_Client
    -------------------------------------------------------  
    OPEN Cur_Master_Fri_Client;
    FETCH Cur_Master_Fri_Client INTO V_Clt_Code, V_Inv_Fri_Clt;
    IF Cur_Master_Fri_Client%FOUND THEN
        V_Message_Code := V_Message_Code||'005';
        V_Message_Val1 := V_Clt_Code; 
        V_Message_Val2 :='Master_Fri_Client';
        V_Message_Val3 := TO_CHAR(V_Inv_Fri_Clt);
        Raise Invalid_Found;
    END IF;
    CLOSE Cur_Master_Fri_Client; 
    Pgmloc := 15;
    -------------------------------------------------------
    -- Validating Fri_Client_Universe
    -------------------------------------------------------  
    OPEN Cur_Fri_Client_Universe;
    FETCH Cur_Fri_Client_Universe INTO V_Clt_Code, V_Inv_Fri_Clt;
    IF Cur_Fri_Client_Universe%FOUND THEN
        V_Message_Code := V_Message_Code||'005';
        V_Message_Val1 := V_Clt_Code; 
        V_Message_Val2 :='Fri_Client_Universe';
        V_Message_Val3 := TO_CHAR(V_Inv_Fri_Clt);
        Raise Invalid_Found;
    END IF;
    CLOSE Cur_Fri_Client_Universe;
    --
    Pgmloc := 20;
    --------------------------------------------------------
    -- Updating pricing_msgs at the end of the program
    --------------------------------------------------------
    Pricing_Msg_Rec.Msg_Text_2       := 'Program Successfully Completed.';
    Pricing_Msg_Rec.Successfull_Flag := 'Y';
    Pricing_Msg_Rec.Remarks          := 'No Invalid Values Found';
    --
    Global_File_Updates.Update_Pricing_Msgs
               (P_Program_Id,
                V_System_Date,
                False,             
                Pricing_Msg_Rec);
    Commit;
    --
    P_Success := TRUE; 
    --
EXCEPTION
    WHEN Invalid_Found THEN
       Manage_Messages.Get_Oasis_Message
               (V_Message_Code,
                V_Del_Flag,
                V_Message_Val1,
                V_Message_Val2,
                V_Message_Val3,
                V_Message);
-- Modification - JSL - 26 janvier 2016
--     Pricing_Msg_Rec.Remarks    := V_Message;    
--     Pricing_Msg_Rec.Msg_Text_2 := 'Invalid Value Found!';
       Pricing_Msg_Rec.Err_Text   := V_Message;    
       Pricing_Msg_Rec.Remarks    := 'Invalid Value Found!';
-- Fin de modification - JSL - 26 janvier 2016
       --  
       Global_File_Updates.Update_Pricing_Msgs
               (P_Program_Id,
                V_System_Date,
                False,             
                Pricing_Msg_Rec);
       Commit;
       --
       P_Message := V_Message;
    WHEN OTHERS THEN         
       --  
       V_Message := SUBSTR(SQLERRM(SQLCODE),1,80);
       Pricing_Msg_Rec.Err_Text:= 'Error @Pgmloc: '||Pgmloc;
       Pricing_Msg_Rec.Remarks := V_Message;    
       Global_File_Updates.Update_Pricing_Msgs
               (P_Program_Id,
                V_System_Date,
                False,             
                Pricing_Msg_Rec);
       Commit;
       --
       P_Message := V_Message;
END Validate_Clients_FK;
-------------------------------------------------------------------------------
END Client;
/
show error
