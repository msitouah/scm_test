create or replace
PACKAGE BODY &&owner..CGI_Process IS --{
----------------------------------------------------------------------
--
-- Creation - JSL - 31 janvier 2008
--
-- SSI - 15FEB2008 - Apply a factor to quantity outstanding.
-- SSI - 20FEB2008 - Post DVS Extract.
-- SSI - 05MAR2008 - Post_DVS_Extract
--                 - Client XREF could use Xref of another client. Separate the
--                   fetch of Client frequency from dividend cursor.
-- JSL - 12mar2008 - Add a trim to the list of exchange as the code may be
--                   preceeded by a blank.
-- JSL - 17mar2008 - An initialisation is conflicting with the code.  Seems
--                   to a test initialisation that remained in.
--                 - Set a default format for number when getting them from
--                   SVS extraction.
-- JSL - 18mar2008 - Accept an identifier of up to 20 characters, to be set
--                   as client_security_short_name in the universe.
--                   CAVEAT : the client_security_id is still only 12 character
--                   long, i.e. the securities are unique on 12 only.
-- SSI - 19MAR2008 - Post_DVS_Extract - Differentiate dividend_note code
-- JSL - 25mar2008 - User send values followed by spaces, trim them!
-- JSL - 27mar2008 - More definition of fields 532 and 533 in post_extract
-- JSL - 01avr2008 - Added the accounting according to CGI.  This have been
--                   added into post_extract only.
-- JSL - 07avr2008 - Added processing for provider security identifier in
--                   post_extract, this is field code 1.
-- JSL - 08avr2008 - Accept the usage of another universe in post_extract.
--                 - Add accounting on individual securities.
-- JSL - 09avr2008 - Add loading of the TSX bonds into acc_cgi_except.  A new
--                   procedure is involved.
--                 - Add an action_code after the client_security_name in the
--                   pre_maintenance.  This allows us to do either a full
--                   universe maintenance or let the frequency be set to NF
--                   and add/change as daily the securities received.
-- JSL - 11avr2008 - Accept the usage of another universe in post_extract,
--                   should also includes the headers and footers.
-- Araisbel 16APR2008 Procedure Maintain_TSX: maintain  field tsx_prices_flag
--                    on table Acc_CGI_Info
--                  - Procedure Accounting_prep: Insert into table
--                    Acc_CGI_Stats_Out all details to be included into
--                    the monthly billing report
-- JSL - 21avr2008 - Added procedure univ_modified.  The procedure is to produce
--                   a report of securities added or that have the exchange
--                   priorities modified on the run date.  This procedure is
--                   planned to be run at night every day.  The analyst could
--                   then do the research the next day to price the securities.
-- JSL - 22avr2008 - In accounting, count only the billable clients on days
--                   where Canada (source TSE) is open.
--                 - Add accounting for DVS and SAS.
-- Araisbel 23APR2008 Procedure Pricing_Stats_Report: Generating pricing
--                    statistics report
-- JSL - 23avr2008 - In procedure univ_modified, send a message as no
--                   maintenance done if the datestamped tables do not exists.
-- JSL - 24avr2008 - Various fixes in Pricing_Stats_Reports.
-- JSL - 25avr2008 - Accounting_Prep : get the count by clients (billable per
--                   product per client) cumulate into master clients.
--                 - Pricing_Stats_Reports : second part master client should
--                   not have SVS.
-- SSI - 30APR2008 - Univ_Modified - Check if path is included in out file
--                   name for AFTed.
-- SSI - 30JUL2008 - Add new code 526 for mid or calculated mid.
-- JSL - 03mar2009 - Accept duplicate in load_acc_except.
-- SSI - 18NOV2009 - Post_DVS_Extract - Assign '__' to null dividend note and
--                                      dividend_type for null comparaison
--                                    - Move the check for last record after the
--                                      branch <<skip_rec>>
-- JSL - 04mai2010 - Allows for a totally empty file.
--
-- SSI - 06mai2010 - Fix last fix on 04mai, should test for V_Data_Line for last line process if any.
--
-- APX - 11Nov2010 - Removed 'A' when Added record after CANCELLATION 
--
-- APX - 05Dec2012 - Modified post_processor for DVS to correct missing dividend 
--
-- Miglena - 02Jun2014 - remove requirement for active_flag = 'Y' in cursor get_client 
--                    - when producing pricing reports
--
-- JSL - 04mar2015 - OASIS-5036
--                 - In pre_maintenance
--                   - add extraction of issuer, maturity date and currency from id= field
--                   - add those and cu= field to the resulting file.
--                 - remove old comments that add nothing to the comprehension
--                 - aftmake is obsolete since a long time and does nothing.
-- Mourad - 19Jan2016 - OASIS_6270 - Add script parameter <<RESTR_CURR>> 
--                    - to restrict on dividends currency field  
--
-- Miglena - 25Jan2016 - added constant for RESTR_CURR parameter
--
----------------------------------------------------------------------
--
  C_GEN_Message       Oasis_Messages.Code%TYPE:='PROC-GEN-';
  C_Oasis_Message     CONSTANT VARCHAR2(30) := 'PROC-CGI_PROCESS-';
--
  C_Def_Number_Fmt    CONSTANT VARCHAR2(60)
             := '99999999999999999999.99999999999999999999mi';
--
-- Miglena - 25Jan2016
  c_parm_name_curr    CONSTANT ops_scripts_parms.parm_name%TYPE := 'RESTR_CURR';
-- end Miglena 
--
--
  Table_Not_Found     EXCEPTION;
  PRAGMA EXCEPTION_INIT( Table_Not_Found, -942 );
--
TYPE R_parameters
  IS RECORD
   ( Dir_Name            Ops_Scripts.Script_Name%TYPE
   , In_File_Name        Ops_Scripts.Script_Name%TYPE
   , Copy_File_Name      Ops_Scripts.Script_Name%TYPE
   , Out_File_Name       Ops_Scripts.Script_Name%TYPE
   , Log_File_Name       Ops_Scripts.Script_Name%TYPE
   , In_File             Utl_File.File_Type
   , Out_File            Utl_File.File_Type
   , Log_File            Utl_File.File_Type
   , In_File_Open        BOOLEAN
   , Out_File_Open       BOOLEAN
   , Log_File_Open       BOOLEAN
   );
--
TYPE RTY_Fields
  IS RECORD
   ( Key_Word            VARCHAR2(16)
   , Field_Sequence      INTEGER
   , List_Value          VARCHAR2(256)
   );
TYPE TTY_Fields
  IS TABLE OF RTY_Fields
  INDEX BY BINARY_INTEGER;
--
TYPE Acc_Cgi_Stats_Type
  IS TABLE OF ACC_CGI_Stats%ROWTYPE
  INDEX BY ACC_CGI_Stats.CGI_Source_Code%TYPE;
G_Acc_Cgi_Stats_Init Acc_Cgi_Stats_Type;
--
  Pgmloc                     INTEGER;
--
  C_Header                   VARCHAR2(1):='H';
  C_Data                     VARCHAR2(1):='D';
  C_Trailer                  VARCHAR2(1):='T';
--
  G_Routine                  VARCHAR2(60);
--
  G_Fri_Client               Clients.Fri_Client%TYPE;
  G_Product_Code             Products.Code%TYPE;
  G_Record_Type              Record_Types.Record_Type_Code%TYPE;
  G_Component                Product_Components.Component_Code%TYPE;
--
  G_Delimiter                VARCHAR2(1);
  G_Out_Rec                  NUMBER:= 0;
--
  G_Outline                  NUMBER;
  TYPE Fmt_Rec IS TABLE OF Client_Format_Layouts%ROWTYPE INDEX BY Product_Field_Names.Code%TYPE;
--
-----------------------------------------------------------------------
--
-- Process_Beginning is the common part for all processes, e.g. write
-- the pricing message and get the client requirements.
--
-----------------------------------------------------------------------
PROCEDURE Process_Beginning(
          P_Program_Id     IN            Ops_Scripts.Program_Id%TYPE
        , P_Load_Date      IN            DATE
        , P_Commit_Freq    IN            INTEGER
        , P_PRP_Rec        IN OUT NOCOPY Product_Running_Parms%ROWTYPE
        , P_Cntl_Rec       IN OUT NOCOPY R_Parameters
        , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Success           OUT        BOOLEAN
        , P_Message           OUT        VARCHAR2
        ) IS --{
--
V_Load_Date          DATE;
V_On_Server          BOOLEAN := CONSTANTS.On_Server;
Success              BOOLEAN;
V_Message            VARCHAR2(256);
V_Program_Id         INTEGER;
--
BEGIN --{
  Pgmloc := 1010;
  V_Load_Date      := NVL( P_Load_Date, Constants.Get_Site_Date );
-- Les scripts appelants sur Linux n'inserent pas les enfants.
-- Faire comme si nous ne serions pas sur le serveur et inserer ces messages.
  IF P_Program_Id > 9999
  THEN
    V_On_Server := FALSE;
  END IF;
  IF V_On_Server
  THEN
    BEGIN
      SELECT *
        INTO Pricing_Msgs_Rec
        FROM Pricing_Msgs PM
       WHERE PM.Program_Id       = P_Program_Id
         AND PM.Date_of_Prices   = V_Load_Date
         AND PM.Successfull_Flag = 'N'
         AND PM.Terminal_Id      = NVL( Constants.Get_Terminal_Id, 'N/A' )
         AND PM.Date_Of_Msg      = (
             SELECT MAX( PM1.Date_Of_Msg )
               FROM Pricing_Msgs PM1
              WHERE PM.Program_Id     = PM1.Program_Id
                AND PM.Date_Of_Prices = PM1.Date_Of_Prices
                AND PM.Terminal_Id    = PM1.Terminal_Id
             );
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        load_supplier_rules.Initialize_Prc_Msgs( P_Program_Id
                                               , SYSDATE
                                               , V_Load_Date
                                               , Pricing_Msgs_Rec
                                               , Success
                                               );
        IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
    END;
  ELSE
    ----------------------------------------
    -- Initalization of the pricing messages
    ----------------------------------------
    Load_Supplier_rules.Initialize_Prc_Msgs( P_Program_Id
                                           , SYSDATE
                                           , V_Load_Date
                                           , Pricing_Msgs_Rec
                                           , Success
                                           );
    IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  END IF;
  pgmloc := 1020;
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
  Pricing_Msgs_Rec.Msg_Text         := 'Program id is '
                                    || to_char(P_Program_id);
  Pricing_Msgs_Rec.Msg_Text_2       := 'Program in progress';
  Pricing_Msgs_Rec.Value_01         := 0;
  Pricing_Msgs_Rec.Value_02         := 0;
  Pricing_Msgs_Rec.Value_03         := 0;
  Pricing_Msgs_Rec.Value_04         := 0;
  Pricing_Msgs_Rec.Value_05         := 0;
  Pricing_Msgs_Rec.Value_06         := 0;
  Pricing_Msgs_Rec.Value_07         := 0;
  Pricing_Msgs_Rec.Value_08         := 0;
  Pricing_Msgs_Rec.Value_09         := 0;
  Pricing_Msgs_Rec.Value_10         := 0;
  Pricing_Msgs_Rec.Value_11         := 0;
  Pricing_Msgs_Rec.Value_12         := 0;
  Pricing_Msgs_Rec.Value_13         := 0;
  Pricing_Msgs_Rec.Value_14         := 0;
  Pricing_Msgs_Rec.Value_15         := 0;
  Load_Supplier_Rules.Update_Prc_Msgs( P_Program_Id
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , Success
                                     );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 1030;
  UPDATE Break_Points
     SET Status      = 'PROG'
       , Information = TO_CHAR( Pricing_Msgs_Rec.Date_of_Msg
                              , 'dd-mon-yyyy hh24:mi:ss' )
       , Nb_Records  = Pricing_Msgs_Rec.Value_01
       , Last_User   = Constants.Get_User_Id
       , Last_Stamp  = SYSDATE
   WHERE Program_Id  = P_Program_Id;
  --
  Pgmloc := 1040;
  IF P_Cntl_Rec.Dir_Name IS NULL
  THEN
    P_Cntl_Rec.Dir_Name := Constants.Get_Dir_Name;
  END IF;
  --
  Pgmloc := 1050;
  IF P_Cntl_Rec.Log_File_Name IS NULL
  THEN
    P_Cntl_Rec.Log_File_Name := Scripts.Get_Arg_Value( P_Program_id, 'LOG' );
    IF P_Cntl_Rec.Log_File_Name IS NULL
    THEN
      P_Cntl_Rec.Log_File_Name := TRIM( TO_CHAR( P_Program_ID ) ) || '.log';
    END IF;
  END IF;
  --
  ---------------------------------------
  --  Open the Log file in the Write mode
  ---------------------------------------
  --
  pgmloc := 1060;
  Load_Supplier_Rules.Open_File( P_Cntl_Rec.Log_File_Name
                               , P_Cntl_Rec.Dir_Name
                               , 'W'
                               , Pricing_Msgs_Rec
                               , P_Cntl_Rec.Log_File
                               , Success
                               );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  P_Cntl_Rec.Log_File_Open := TRUE;
  pgmloc := 1070;
  load_supplier_Rules.Write_file( P_Cntl_Rec.Log_File
                                , Pricing_Msgs_Rec.Msg_Text
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  pgmloc := 1080;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'010'
                                   , 'N'
                                   , TO_CHAR( Pricing_Msgs_Rec.Date_of_Msg
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  pgmloc := 1090;
  Load_Supplier_Rules.Write_File( P_Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  --
  -------------------------------------------------------
  --  Get the product running parameters
  -------------------------------------------------------
  --
  V_Program_Id := P_Program_Id;
  IF V_Program_Id > 9999
  THEN
    V_Program_Id := V_Program_Id / 100;
  END IF;
  BEGIN
    Pgmloc := 1100;
    SELECT *
      INTO P_PRP_Rec
      FROM Product_Running_Parms PRP
     WHERE PRP.Program_Id = V_Program_Id;
  EXCEPTION
  WHEN OTHERS THEN
    pgmloc := 1110;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'020'
                                     , 'N'
                                     , Pricing_Msgs_Rec.Err_Text
                                     );
    pgmloc := 1120;
    Load_Supplier_Rules.Write_File( P_Cntl_Rec.Log_File
                                  , Pricing_Msgs_Rec.Err_Text
                                  , Pricing_Msgs_Rec
                                  , Success
                                  );
    Success :=  FALSE;
    GOTO GET_OUT;
  END;
  --
  --
  -------------------------------------------------------
  --  Copy the input file
  -------------------------------------------------------
  --
  Pgmloc := 1130;
  IF P_Cntl_Rec.In_File_Name IS NULL
  THEN
    pgmloc := 1140;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'030'
                                     , 'N'
                                     , Pricing_Msgs_Rec.Msg_Text_2
                                     );
    pgmloc := 1150;
    Load_Supplier_Rules.Write_File( P_Cntl_Rec.Log_File
                                  , Pricing_Msgs_Rec.Msg_Text_2
                                  , Pricing_Msgs_Rec
                                  , Success
                                  );
    GOTO Input_File_Opened;
  END IF;
  --
  Pgmloc := 1160;
  IF P_Cntl_Rec.Copy_File_Name IS NULL
  THEN
    P_Cntl_Rec.Copy_File_Name := Scripts.Get_Arg_Value( P_Program_id, 'COPY' );
    IF P_Cntl_Rec.Copy_File_Name IS NULL
    THEN
      P_Cntl_Rec.Copy_File_Name := TRIM( P_Cntl_Rec.In_File_Name ) || '.cpy';
    END IF;
  END IF;
  --
  pgmloc := 1170;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'040'
                                   , 'N'
                                   , P_Cntl_Rec.Dir_Name
                                  || Constants.Get_Dir_Seperator
                                  || P_Cntl_Rec.In_File_Name
                                   , P_Cntl_Rec.Dir_Name
                                  || Constants.Get_Dir_Seperator
                                  || P_Cntl_Rec.Copy_File_Name
                                   , V_Message
                                   );
  pgmloc := 1180;
  Load_Supplier_Rules.Write_File( P_Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , Success
                                );
  --
  Pgmloc := 1190;
  Load_Supplier_Rules.File_Copy( P_Cntl_Rec.In_File_Name
                               , P_Cntl_Rec.Copy_File_Name
                               , P_Cntl_Rec.Dir_Name
                               , Pricing_Msgs_Rec
                               , Success
                               );
  --
  -------------------------------------------------------
  -- Open the copied file as Input
  -------------------------------------------------------
  --
  Pgmloc := 1200;
  Load_Supplier_Rules.Open_File( P_Cntl_Rec.Copy_File_Name
                               , P_Cntl_Rec.Dir_Name
                               , 'R'
                               , Pricing_Msgs_Rec
                               , P_Cntl_Rec.In_File
                               , Success
                               );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  P_Cntl_Rec.In_File_Open := TRUE;
  pgmloc := 1210;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'050'
                                   , 'N'
                                   , P_Cntl_Rec.Dir_Name
                                  || Constants.Get_Dir_Seperator
                                  || P_Cntl_Rec.Copy_File_Name
                                   , 'Input'
                                   , V_Message
                                   );
  pgmloc := 1220;
  Load_Supplier_Rules.Write_File( P_Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
<< Input_File_Opened >>
  --
  --
  -------------------------------------------------------
  -- Open the output file
  -------------------------------------------------------
  --
  Pgmloc := 1230;
  IF P_Cntl_Rec.Out_File_Name IS NULL
  THEN
    P_Cntl_Rec.Out_File_Name := Scripts.Get_Arg_Value( P_Program_id, 'OUT' );
    IF P_Cntl_Rec.Out_File_Name IS NULL
    THEN
      P_Cntl_Rec.Out_File_Name := TRIM( P_Cntl_Rec.In_File_Name ) || '.out';
    END IF;
  END IF;
  Pgmloc := 1240;
  Load_Supplier_Rules.Open_File( P_Cntl_Rec.Out_File_Name
                               , P_Cntl_Rec.Dir_Name
                               , 'W'
                               , Pricing_Msgs_Rec
                               , P_Cntl_Rec.Out_File
                               , Success
                               );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
  P_Cntl_Rec.Out_File_Open := TRUE;
  pgmloc := 1250;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'050'
                                   , 'N'
                                   , P_Cntl_Rec.Dir_Name
                                  || Constants.Get_Dir_Seperator
                                  || P_Cntl_Rec.Out_File_Name
                                   , 'Output'
                                   , V_Message
                                   );
  pgmloc := 1260;
  Load_Supplier_Rules.Write_File( P_Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , Success
                                );
  IF ( NOT SUCCESS ) THEN GOTO GET_OUT; END IF;
<< Get_Out >>
  P_Success := Success;
EXCEPTION
WHEN OTHERS THEN
   DBMS_OUTPUT.PUT_LINE( 'ERROR AT LOC '||PGMLOC );
   RAISE;
END Process_Beginning; --}}
--
--
-----------------------------------------------------------------------
--
-- Separate the diverse fields from the_line into a table of keywords
-- and their assocaited values.
--
-----------------------------------------------------------------------
--
PROCEDURE Break_by_Fields
        ( Data_Rec   IN            VARCHAR2
        , T_Fields   IN OUT NOCOPY TTY_Fields
        , P_Success     OUT        BOOLEAN
        , V_Message     OUT        VARCHAR2
        ) IS --{
--
C_List_Separator  CONSTANT VARCHAR2(1) := ';';
C_Value_Separator CONSTANT VARCHAR2(1) := ',';
C_Equal_Sign      CONSTANT VARCHAR2(1) := '=';
C_End_Line        CONSTANT VARCHAR2(1) := '|';
--
V_Keyword_End     INTEGER;
V_Value_Start     INTEGER;
V_Value_End       INTEGER;
V_Current_Start   INTEGER;
V_Current_End     INTEGER;
V_Keyword         VARCHAR2(16);
V_Sequence        INTEGER;
V_List_Value      VARCHAR2(256);
V_Value           VARCHAR2(256);
--
T_Empty_Fields    TTY_Fields;
V_Field_Number    INTEGER;
V_Data_Rec        VARCHAR2(2048);
--
BEGIN --{
  T_Fields := T_Empty_Fields;
  V_Data_Rec := TRIM( Data_Rec );
  IF V_Data_Rec IS NULL
  THEN
    GOTO Get_Out;
  END IF;
  -- Remove the Cntrl-M for file read as DOS.
  IF SUBSTR( V_Data_Rec, LENGTH( V_Data_Rec ), 1 ) = CHR(13)
  THEN
    V_Data_Rec := SUBSTR( V_Data_Rec, 1, LENGTH( V_Data_Rec ) - 1 );
  END IF;
  IF SUBSTR( V_Data_Rec, 1, 2 ) = C_End_Line || C_List_Separator
  THEN
    GOTO Get_Out;
  END IF;
  V_Current_End    := 0;
  V_Field_Number   := 0;
  WHILE V_Current_End < LENGTH( V_Data_Rec )
  LOOP --{
    V_Current_Start := V_Current_End + 1;
    WHILE SUBSTR( V_Data_Rec, V_Current_Start, 1 ) = C_List_Separator
    LOOP
      V_Current_Start := V_Current_Start + 1;
    END LOOP;
    IF SUBSTR( V_Data_Rec, V_Current_Start, 1 ) = C_End_Line
    THEN --{
      V_Current_End := LENGTH( V_Data_Rec );
    ELSE --}{
      V_Current_End := INSTR( V_Data_Rec, C_List_Separator, V_Current_Start, 1 );
      IF V_Current_End <= 0
      THEN
        V_Current_End := LENGTH( V_Data_Rec );
      ELSE
        V_Current_End := V_Current_End - 1;
      END IF;
      V_Keyword_End := INSTR( V_Data_Rec, C_Equal_Sign, V_Current_Start, 1 );
      V_Keyword     := SUBSTR( V_Data_Rec
                             , V_Current_Start
                             , V_Keyword_End
                             - V_Current_Start
                             + 1
                             );
      V_List_Value  := SUBSTR( V_Data_Rec
                             , V_Keyword_End
                             + 1
                             , V_Current_End
                             - V_Keyword_End
                             );
      V_Sequence    := 0;
      -- Insert the whole list as sequence zero
      -- Header is using the whole list
      -- Details are using the break down.
      V_Field_Number := V_Field_Number + 1;
-- User send values followed by spaces, trim them!
      T_Fields( V_Field_Number ).Key_Word :=  TRIM( V_Keyword );
      T_Fields( V_Field_Number ).Field_Sequence := V_Sequence;
      T_Fields( V_Field_Number ).List_Value :=  TRIM( V_List_Value );
      --
      V_Value_End   := 0;
      WHILE V_Value_End < LENGTH( V_List_Value )
      LOOP --{
        V_Value_Start := V_Value_End + 1;
        WHILE SUBSTR( V_List_Value, V_Value_Start, 1 ) = C_Value_Separator
        LOOP
          V_Value_Start := V_Value_Start + 1;
        END LOOP;
        V_Value_End := INSTR( V_List_Value, C_Value_Separator, V_Value_Start, 1 );
        IF V_Value_End <= 0
        THEN
          V_Value_End := LENGTH( V_List_Value );
        ELSE
          V_Value_End := V_Value_End - 1;
        END IF;
        V_Field_Number := V_Field_Number + 1;
        V_Sequence   := V_Sequence   + 1;
        V_Value      := SUBSTR( V_List_Value
                              , V_Value_Start
                              , V_Value_End
                              - V_Value_Start
                              + 1
                              );
-- User send values followed by spaces, trim them!
        T_Fields( V_Field_Number ).Key_Word := TRIM( V_Keyword );
        T_Fields( V_Field_Number ).Field_Sequence := V_Sequence;
        T_Fields( V_Field_Number ).List_Value := TRIM( V_Value );
      END LOOP; --} V_Value_End < Length
    END IF; --} if |;
  END LOOP; --} V_Current_End < Length
<< Get_Out >>
  NULL;
END Break_by_Fields; --}}
--
--
-----------------------------------------------------------------------
--
-- Pre_Maintenance is to keep the information about securities and
-- reformat so the maintenance program can handle the file.
--
-----------------------------------------------------------------------
--
PROCEDURE Pre_Maintenance(
          P_Program_Id    IN     Ops_Scripts.Program_Id%TYPE
        , P_In_File_Name  IN     VARCHAR2
        , P_Out_File_Name IN     VARCHAR2
        , P_Commit_Freq   IN     INTEGER
        , P_Success          OUT BOOLEAN
        , P_Message          OUT VARCHAR2
        ) IS --{
--
C_FRI_ID            CONSTANT Identification_Systems.Code%TYPE := 'X';
C_FOOTER            CONSTANT VARCHAR2(2) := '|;';
C_Pricing_Frequency_Value CONSTANT Frequencies.Value%TYPE := 0;
-- Ajout - JSL - 04 mars 2015
C_US_Currency       CONSTANT Currencies.Currency%TYPE := 'USD';
-- Fin d'ajout - JSL - 04 mars 2015
--
Pricing_Msgs_Rec             Pricing_Msgs%ROWTYPE;
PRP                          Product_Running_Parms%ROWTYPE;
Cntl_Rec                     R_Parameters;
V_Message                    VARCHAR2(256);
T_Fields                     TTY_Fields;
Data_Rec                     VARCHAR2(1024);
V_Data_Line                  VARCHAR2(1024);
End_Of_File                  BOOLEAN;
V_Client_Security_Id         CGI_File_Info.CGI_Value%TYPE;
V_Client_Security_Id_3       CGI_File_Info.CGI_Value%TYPE;
-- Ajout - JSL - 04 mars 2015
V_Client_Cusip               CGI_File_Info.CGI_Value%TYPE;
V_Client_mat_date            DATE;
V_Client_Issuer              CGI_File_Info.CGI_Value%TYPE;
V_Client_Currency            CGI_File_Info.CGI_Value%TYPE;
V_ident_regexp               ops_scripts_parms.Parm_Value%TYPE;
V_mat_date_format            ops_scripts_parms.Parm_Value%TYPE;
V_mat_date_Exp               PLS_INTEGER;
V_Issuer_Exp                 PLS_INTEGER;
V_currency_Exp               PLS_INTEGER;
-- Fin d'ajout - JSL - 04 mars 2015
V_Client_Security_Desc       Clients_Univ.Client_Security_Name%TYPE;
ID_Ticker                    Tickers.ticker%TYPE;
V_Fri_Source_Code            Sources.Code%TYPE;
V_Composite_Desc             Clients_Xref.Output_Client_Xref_Code%TYPE;
--
R_CFI                        CGI_File_Info%ROWTYPE;
R_CF                         CGI_Fields%ROWTYPE;
R_CE                         CGI_Exchanges%ROWTYPE;
--
CURSOR CUR_Get_Fri_Id
     ( I_CGI_Keyword VARCHAR2
     , I_CGI_Ticker  VARCHAR2
     ) IS
SELECT T.Ticker
  FROM CGI_Id_Xref CIX
     , Tickers     T
     , Tickers     TT
 WHERE CIX.CGI_Keyword                = I_CGI_keyword
   AND CIX.Identification_System_Code = TT.Identification_System_Code
   AND TT.Ticker                      = I_CGI_Ticker
   AND T.Fri_Ticker                   = TT.Fri_Ticker
   AND T.Identification_System_Code   = 'X'
ORDER BY Sort_Order;
--
BEGIN --{
  Pgmloc := 1270;
  P_Success := TRUE;
  --
  -------------------------------
  --  create the pricing message,
  --  open the log
  --  copy the input file
  --  open the copied file
  --  open the output file
  -------------------------------
  --
  Pgmloc := 1280;
  Cntl_rec.In_File_Name  := P_In_File_Name;
  Cntl_rec.Out_File_Name := P_Out_File_Name;
  Cntl_Rec.In_File_Open  := FALSE;
  Cntl_Rec.Out_File_Open := FALSE;
  Cntl_Rec.Log_File_Open := FALSE;
  Pgmloc := 1290;
  Process_Beginning( P_Program_Id
                   , Constants.Get_Site_Date
                   , P_Commit_Freq
                   , PRP
                   , Cntl_Rec
                   , Pricing_Msgs_Rec
                   , P_Success
                   , P_Message
                   );
  IF NOT P_Success THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 1300;
  COMMIT;
  --
  -- Ajout - JSL - 04 mars 2015
  -- Get parameters
  V_ident_regexp             := NULL;
  V_mat_date_format          := NULL;
  V_mat_date_Exp             := NULL;
  V_Issuer_Exp               := NULL;
  V_currency_Exp             := NULL;
  Pgmloc := 1341;
  BEGIN
    V_Ident_regexp := scripts.get_arg_value( P_Program_Id
                                           , 'IDENT_REGEXP'
                                           );
  EXCEPTION
  WHEN OTHERS THEN NULL;
  END;
  Pgmloc := 1342;
  BEGIN
    V_mat_date_Format := scripts.get_arg_value( P_Program_Id
                                              , 'MAT_DATE_FORMAT'
                                              );
  EXCEPTION
  WHEN OTHERS THEN NULL;
  END;
  Pgmloc := 1343;
  BEGIN
    V_mat_date_exp := TO_NUMBER( scripts.get_arg_value( P_Program_Id
                                                      , 'MAT_DATE_EXP'
                                                      )
                               );
    IF V_Mat_Date_Exp > 0
    THEN
      NULL;
    ELSE
      V_Mat_Date_Exp := NULL;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN NULL;
  END;
  Pgmloc := 1344;
  BEGIN
    V_Issuer_exp := TO_NUMBER( scripts.get_arg_value( P_Program_Id
                                                    , 'ISSUER_EXP'
                                                    )
                               );
    IF V_Issuer_Exp > 0
    THEN
      NULL;
    ELSE
      V_Issuer_Exp := NULL;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN NULL;
  END;
  Pgmloc := 1345;
  BEGIN
    V_Currency_exp := TO_NUMBER( scripts.get_arg_value( P_Program_Id
                                                      , 'CURRENCY_EXP'
                                                    )
                               );
    IF V_Currency_Exp > 0
    THEN
      NULL;
    ELSE
      V_Currency_Exp := NULL;
    END IF;
  EXCEPTION
  WHEN OTHERS THEN NULL;
  END;
  -- Fin d'ajout - JSL - 04 mars 2015
  --
  ----------------------------------------------------------------------------
  -- We have committed the messages up to now
  -- From here on this is everything or nothing
  ----------------------------------------------------------------------------
  --
  Pgmloc := 1310;
  DELETE CGI_File_Info
   WHERE Fri_CLient = PRP.Fri_Client;
  Pricing_Msgs_Rec.Value_11 := SQL%ROWCOUNT;
  --
  Pgmloc := 1320;
  DELETE CGI_Fields
   WHERE Fri_Client = PRP.Fri_Client;
  Pricing_Msgs_Rec.Value_12 := SQL%ROWCOUNT;
  --
  Pgmloc := 1330;
  DELETE CGI_Exchanges
   WHERE Fri_Client = PRP.Fri_Client;
  Pricing_Msgs_Rec.Value_13 := SQL%ROWCOUNT;
  --
  Pgmloc := 1340;
  UPDATE Clients_Univ
     SET Pricing_Frequency_Value = C_Pricing_Frequency_Value
   WHERE Fri_Client = PRP.Fri_Client
     AND Fri_Reserved_Flag <> 'Y';
  --
  END_of_File := FALSE;
  --
  WHILE NOT End_of_File
  LOOP --{
    pgmloc := 1350;
    Load_Supplier_Rules.Get_Line( Cntl_Rec.In_File
                                , Pricing_Msgs_Rec
                                , Data_Rec
                                , P_Success
                                , End_Of_File
                                );
    IF NOT P_Success   THEN GOTO GET_OUT;  END IF;
    IF     End_Of_File THEN GOTO END_LOOP; END IF;
    Pricing_Msgs_Rec.Value_01 := Pricing_Msgs_Rec.Value_01 + 1;
    Break_by_Fields( Data_Rec
                   , T_Fields
                   , P_Success
                   , V_Message
                   );
    Pgmloc := 1360;
    IF T_Fields.Count = 0 THEN GOTO End_Loop; END IF;
    Pgmloc := 1370;
    V_Client_Security_Id   := NULL;
    V_Client_Security_Id_3 := NULL;
    V_Client_Security_Desc := NULL;
    ID_Ticker              := NULL;
-- Ajout - JSL - 04 mars 2015
    V_Client_Cusip         := NULL;
    V_Client_mat_date      := NULL;
    V_Client_Issuer        := NULL;
    V_Client_Currency      := NULL;
-- Fin d'ajout - JSL - 04 mars 2015
    FOR R_Fields IN T_Fields.FIRST .. T_Fields.LAST
    LOOP --{
      IF  T_Fields( R_Fields ).Field_Sequence = 0
      THEN
        IF  T_Fields( R_Fields ).Key_Word = 'id='
        THEN
          V_Client_Security_Id := T_Fields( R_Fields ).List_Value;
          V_Client_Security_Id_3 := 'LO|'||T_Fields( R_Fields ).List_Value;
        ELSIF V_Client_Security_Id IS NULL
        AND T_Fields( R_Fields ).key_Word IN ('cu=', 'is=', 'se=', 'tk=')
        THEN
          V_Client_Security_Id := T_Fields( R_Fields ).List_Value;
          V_Client_Security_Id_3
            := UPPER( SUBSTR( T_Fields( R_Fields ).Key_Word, 1, 2 ) )
            || '|'
            || T_Fields( R_Fields ).List_Value;
        END IF;
      END IF;
    END LOOP; --}
    --
    Pgmloc := 1380;
    FOR R_Fields IN T_Fields.FIRST .. T_Fields.LAST
    LOOP --{
      IF Pricing_Msgs_Rec.Value_01 = 1
      THEN --{
        -- Header record
        -- For each keyword found
        -- set the key and value into the table
        Pgmloc := 1390;
        IF T_Fields( R_Fields ).Field_Sequence = 0
        THEN --{
          R_CFI.Fri_Client  := PRP.Fri_Client;
          R_CFI.CGI_Keyword := T_Fields( R_Fields ).Key_Word;
          R_CFI.CGI_Value   := T_Fields( R_Fields ).List_Value;
          R_CFI.Last_User   := Constants.Get_User_Id;
          R_CFI.Last_Stamp  := SYSDATE;
          Pgmloc := 1400;
          INSERT
            INTO CGI_FILE_INFO
          VALUES R_CFI;
          Pricing_Msgs_Rec.Value_06 := Pricing_Msgs_Rec.Value_06 + SQL%ROWCOUNT;
        END IF; --} Field_secquence = 0
      ELSE --}{ First record
        IF T_Fields( R_Fields ).Field_Sequence != 0
        THEN --{
          -- Security description record
          IF T_Fields( R_Fields ).Key_Word IN ('es=', 'ep=')
          THEN --{
            pgmloc := 1410;
            R_CE.Fri_Client            := PRP.Fri_Client;
            R_CE.Client_security_Id    := V_Client_Security_Id;
            R_CE.Priority_Number       := T_Fields( R_Fields ).Field_Sequence;
            R_CE.FRI_Source_Code       := TRIM( T_Fields( R_Fields ).List_Value );
            R_CE.Composite_Prices_Flag := NULL;
            R_CE.Last_User             := Constants.Get_User_Id;
            R_CE.Last_Stamp            := SYSDATE;
            Pgmloc := 1420;
            INSERT
              INTO CGI_Exchanges
            VALUES R_CE;
          ELSE --}{
            pgmloc := 1430;
            R_CF.Fri_Client         := PRP.Fri_CLient;
            R_CF.Client_Security_Id := V_Client_Security_Id;
            R_CF.Field_Type         := T_Fields( R_Fields ).Key_Word;
            R_CF.Sequence_Number    := T_Fields( R_Fields ).Field_Sequence;
            R_CF.CGI_Field_Number   := T_Fields( R_Fields ).List_Value;
            R_CF.Last_User          := Constants.Get_User_Id;
            R_CF.Last_Stamp         := SYSDATE;
            pgmloc := 1440;
            INSERT
              INTO CGI_Fields
            VALUES R_CF;
            Pricing_Msgs_Rec.Value_06 := Pricing_Msgs_Rec.Value_06 + SQL%ROWCOUNT;
          END IF; --} keyword
        ELSE --}{
          IF T_Fields( R_Fields ).Key_Word IN ( 'cu=', 'se=', 'tk=', 'is=', 'id=' )
          THEN
-- Ajout - JSL - 04 mars 2015
            IF T_Fields( R_Fields ).Key_Word = 'cu=' 
            THEN
              V_Client_Cusip         := T_Fields( R_Fields ).List_Value;
            ELSIF T_Fields( R_Fields ).Key_Word = 'id=' 
            THEN
              IF V_Ident_regexp IS NOT NULL
              THEN
                IF regexp_instr( TRIM(T_Fields( R_Fields ).List_Value)
                               , v_ident_regexp
                               ) > 0
                THEN
                  IF V_Issuer_Exp IS NOT NULL
                  THEN
                    V_Client_Issuer := regexp_substr( TRIM(T_Fields( R_Fields ).List_Value)
                                                    , v_ident_regexp
                                                    , 1, 1, null
                                                    , v_issuer_exp
                                                    );
                  END IF;
                  IF V_currency_Exp IS NOT NULL
                  THEN
                    V_Client_currency := regexp_substr( TRIM(T_Fields( R_Fields ).List_Value)
                                                    , v_ident_regexp
                                                    , 1, 1, null
                                                    , v_currency_exp
                                                    );
                    IF UPPER( v_client_currency ) = 'U'
                    THEN
                      V_Client_Currency := C_US_Currency;
                    ELSE
                      V_Client_Currency := NULL;
                    END IF;
                  END IF;
                  IF  V_Mat_Date_Exp    IS NOT NULL
                  AND V_Mat_Date_Format IS NOT NULL
                  THEN
                    BEGIN
                      V_Client_Mat_Date := TO_DATE( regexp_substr( TRIM(T_Fields( R_Fields ).List_Value)
                                                                 , v_ident_regexp
                                                                 , 1, 1, null
                                                                 , v_Mat_Date_exp
                                                                 )
                                                  , V_Mat_Date_Format
                                                  );
                    EXCEPTION
                    WHEN OTHERS THEN NULL;
                    END;
                  END IF;
                END IF; -- Found regexp_instr
              END IF; -- have a regexp
            END IF;
-- Fin d'ajout - JSL - 04 mars 2015
            IF ID_Ticker IS NULL
            THEN
              Pgmloc := 1450;
              OPEN CUR_Get_Fri_Id ( T_Fields( R_Fields ).Key_Word
                                  , T_Fields( R_Fields ).List_Value
                                  );
              Pgmloc := 1460;
              FETCH CUR_Get_Fri_Id
               INTO ID_Ticker;
              Pgmloc := 1470;
              CLOSE CUR_Get_Fri_Id;
            END IF;
            IF V_Client_Security_Desc IS NOT NULL
            THEN
              V_Client_Security_Desc := V_Client_Security_Desc || ',';
            END IF;
            V_Client_Security_Desc := V_Client_Security_Desc
                                   || T_Fields( R_Fields ).Key_Word
                                   || T_Fields( R_Fields ).List_Value;
          END IF;
        END IF; --} Field Sequence!= 0
      END IF; --} First record
    END LOOP; --}
    --
    IF Pricing_Msgs_Rec.Value_01 > 1
    THEN --{
      Pgmloc := 1480;
      V_Data_Line := 'D'
                  || ' '
                  || RPAD( NVL( V_Client_Security_Id, ' ' ), 15 )
                  || ' '
                  || RPAD( NVL( ID_Ticker, ' ' ), 6 )
                  || ' '
                  || RPAD( NVL( V_Client_Security_Id_3, ' ' ), 23 )
                  || ' '
                  || TO_CHAR( Pricing_Msgs_Rec.Value_01, '000009mi' )
                  || ' '
                  || RPAD( NVL( V_Client_Security_Desc, ' ' ), 120 )
                  || ' A'
-- Ajout - JSL - 04 mars 2015
                  || ' '
                  || RPAD( NVL( V_Client_Cusip, ' ' ), 15 )
                  || ' '
                  || RPAD( NVL( to_char( v_client_mat_date, 'dd-mon-yyyy' ), ' ' ), 11 )
                  || ' '
                  || RPAD( NVL( V_Client_Currency, ' ' ), 3 )
                  || ' '
                  || RPAD( NVL( V_Client_Issuer, ' '), 10 )
-- Fin d'ajout - JSL - 04 mars 2015
                   ;
      Pgmloc := 1490;
      Load_Supplier_Rules.Write_file( Cntl_Rec.Out_File
                                    , V_Data_Line
                                    , Pricing_Msgs_Rec
                                    , P_Success
                                    );
      IF NOT P_Success THEN GOTO Get_Out; END IF;
      Pricing_Msgs_Rec.Value_02 := Pricing_Msgs_Rec.Value_02 + 1;
      IF ID_Ticker IS NOT NULL
      THEN
        Pricing_Msgs_Rec.Value_03 := Pricing_Msgs_Rec.Value_03 + 1;
      END IF;
    END IF; --} First record
<< End_Loop >>
    NULL;
  END LOOP; --} while not end_of_file
  --
  pgmloc := 1500;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'012'
                                   , 'N'
                                   , TO_CHAR( SYSDATE
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  pgmloc := 1510;
  load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 1520;
  Load_Supplier_Rules.File_Close_All( Pricing_Msgs_Rec
                                    , P_Success
                                    );
  Cntl_Rec.Log_File_Open := FALSE;
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 1530;
  UPDATE Break_Points
     SET Status     = 'DONE'
       , Nb_Records = Pricing_Msgs_Rec.Value_01
       , Last_User  = Constants.Get_User_Id
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  pgmloc := 1540;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'060'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  pgmloc := 1550;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'062'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  GOTO Upd_Prc_Msg;
  --
<<GET_OUT>>
  pgmloc := 1560;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'070'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'072'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
--
<<Upd_Prc_Msg>>
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  --
  pgmloc := 1570;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 1580;
  commit;
--
EXCEPTION
WHEN OTHERS THEN
  P_Message := SUBSTR( 'At Pgmloc : '
                     || TO_CHAR( Pgmloc )
                     || ' '
                     || SUBSTR( SQLERRM( SQLCODE ), 1, 255 )
                     , 1
                     , 255
                     );
  IF Pricing_Msgs_Rec.Err_Text IS NULL
  THEN
    Pricing_Msgs_Rec.Err_Text := P_Message;
  END IF;
  Pgmloc := 1590;
  ROLLBACK;
  Pgmloc := 1600;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 1610;
  commit;
  IF Cntl_Rec.Log_File_Open
  THEN
    pgmloc := 1620;
    load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                  , Pricing_Msgs_Rec.Msg_Text
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
  END IF;
  --
  Load_Supplier_Rules.File_Close_All( Pricing_msgs_Rec
                                    , P_Success
                                    );
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  P_Success := FALSE;
  --
END Pre_Maintenance; --}}
-----------------------------------------------------------------------
PROCEDURE Post_Extract(
          P_Program_Id    IN     Ops_Scripts.Program_Id%TYPE
        , P_In_File_Name  IN     VARCHAR2
        , P_Out_File_Name IN     VARCHAR2
        , P_Commit_Freq   IN     INTEGER
        , P_Success          OUT BOOLEAN
        , P_Message          OUT VARCHAR2
        ) IS --{
--
C_Field_Separator     CONSTANT VARCHAR2(1) := '|';
--
Pricing_Msgs_Rec             Pricing_Msgs%ROWTYPE;
PRP                          Product_Running_Parms%ROWTYPE;
Cntl_Rec                     R_Parameters;
V_Message                    VARCHAR2(256);
End_Of_File                  BOOLEAN;
Data_Rec                     VARCHAR2(1024);
V_Data_Line                  VARCHAR2(1024);
V_Client_Security_Id         Clients_Univ.Client_Security_Id%TYPE;
Field_Value                  VARCHAR2(128);
Field_Price                  VARCHAR2(128);
Field_Date                   VARCHAR2(128);
V_Prefered_Price             VARCHAR2(128);
V_Prefered_Date              VARCHAR2(128);
V_Program_id                 INTEGER;
Parent_Date_Of_Msg           DATE;
--
-- S - Start position
-- L - Length
-- A changer pour un table indexe par code!
S_Client_Security_id         INTEGER := 1;
L_Client_Security_Id         INTEGER := 12;
S_Main_Id                    INTEGER := 14;
L_Main_Id                    INTEGER := 23;
S_Fri_Ticker                 INTEGER := 38;
L_Fri_Ticker                 INTEGER := 9;
S_Exchange                   INTEGER := 48;
L_Exchange                   INTEGER := 4;
S_Price_Iso_Cy               INTEGER := 53;
L_Price_Iso_Cy               INTEGER := 3;
Pad_Length                   INTEGER := 512;
--
-- Variable used by get one line (read ahead)
-- This is a cache.  do not move, unless you made it global!
V_One_Line                   VARCHAR2(512) := NULL;
V_One_End_Of_File            BOOLEAN       := FALSE;
--
C_NSIN_NOT_FOUND             CONSTANT VARCHAR2(4) := '-TCK';
C_CHECK_EXCHANGE             CONSTANT VARCHAR2(4) := '-EXC';
V_Acc_Cgi_Stats              Acc_Cgi_Stats_Type;
R_Acc_Cgi_Stats              Acc_Cgi_Stats%ROWTYPE;
V_Cgi_Source_Code            Acc_Cgi_Stats.CGI_Source_Code%TYPE;
--
V_Do_Accounting              BOOLEAN;
V_Fri_Ticker                 INTEGER;
--
CURSOR C_Fields
     ( I_Fri_Client         CGI_Fields.Fri_Client%TYPE
     , I_Client_Security_Id CGI_Fields.Client_Security_Id%TYPE
     , I_Field_Type         CGI_Fields.Field_Type%TYPE
     ) IS
SELECT CGI.*
  FROM CGI_Fields CGI
     , Clients    C
 WHERE C.Fri_CLient       = I_Fri_Client
   AND CGI.Fri_Client     = NVL( C.Fri_Client_Universe, C.Fri_Client )
   AND Client_Security_Id = I_Client_Security_Id
   AND Field_Type         = I_Field_Type
ORDER BY Sequence_Number;
--
PROCEDURE Get_One_Line
        ( In_File          IN     Utl_File.File_Type
        , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , V_Data_Line         OUT VARCHAR2
        , P_Success           OUT BOOLEAN
        , End_Of_File         OUT BOOLEAN
        ) IS --{
--
Prev_Key              VARCHAR2(32);
Current_Key           VARCHAR2(32);
S_Adj_Flag            INTEGER := 440;
L_Adj_Flag            INTEGER := 1;
--
BEGIN --{
  P_Success := TRUE;
  End_Of_File := V_One_End_Of_File;
  --
  IF NOT End_Of_File
  THEN
    Prev_Key := NULL;
    IF V_One_Line IS NOT NULL
    THEN
      Prev_Key := SUBSTR( V_One_Line, S_Main_Id, L_Main_Id );
      IF SUBSTR( V_One_Line, S_Adj_Flag, L_Adj_Flag ) = 'A'
      THEN
        V_Data_Line := RPAD( ' ', Pad_Length )
                    || RPAD( V_One_Line, Pad_Length );
      ELSE
        V_Data_Line := RPAD( V_One_Line, Pad_Length + Pad_Length );
      END IF;
    END IF;
-- Does a cache and align the two records into one
-- Unadjusted and adjusted.
    WHILE TRUE
    LOOP --{
      Load_Supplier_Rules.Get_Line( In_File
                                  , Pricing_Msgs_Rec
                                  , V_One_Line
                                  , P_Success
                                  , V_One_End_Of_File
                                  );
      IF NOT P_Success     THEN GOTO GET_OUT; END IF;
      IF  V_One_End_Of_File
      AND V_Data_Line IS NULL
      THEN
        -- Currently at end of file and no prior line
        -- file is empty
        End_of_File := TRUE;
        GOTO Get_Out;
      END IF;
      IF V_One_End_Of_File THEN GOTO GET_OUT; END IF;
      Pricing_Msgs_Rec.Value_03 := Pricing_Msgs_Rec.Value_03 + 1;
      Current_Key := SUBSTR( V_One_Line, S_Main_Id, L_Main_ID );
      IF Current_Key <> NVL( Prev_Key, Current_Key )
      THEN
        GOTO GET_OUT;
      END IF;
      Prev_Key := Current_Key;
      IF SUBSTR( V_One_Line, S_Adj_Flag, L_Adj_Flag ) = 'A'
      THEN
        V_Data_Line := SUBSTR( V_Data_Line, 1, Pad_Length )
                    || RPAD( V_One_Line, Pad_Length );
      ELSE
        V_Data_Line := RPAD( V_One_Line, Pad_Length )
                    || SUBSTR( V_Data_Line, Pad_Length+1, Pad_Length );
      END IF;
    END LOOP; --}
    IF SUBSTR( V_Data_Line, S_Main_ID, L_Main_Id ) = RPAD( ' ', L_Main_ID )
    THEN
      -- Assuming that the main id, fri_ticker, exchange and price_iso_cy
      -- are contiguous and starting at column 1
      V_Data_Line := SUBSTR( V_Data_Line
                           , Pad_Length + 1
                           , ( S_Price_Iso_Cy + L_Price_Iso_Cy ) - 1
                           )
                  || SUBSTR( V_Data_Line
                           , ( S_Price_Iso_Cy + L_Price_Iso_Cy )
                           , ( Pad_Length + Pad_Length )
                           - ( S_Price_Iso_Cy + L_Price_Iso_Cy )
                           );
    END IF;
  END IF;
<< Get_Out >>
  NULL;
END Get_One_Line; --}}
--
--
--
PROCEDURE Get_Provider_Identifier
        ( V_Data_Line    IN  VARCHAR2
        , V_Field_Value  OUT VARCHAR2
        ) IS --{
--
V_Fri_Ticker      Securities.Fri_Ticker%TYPE;
V_Exchange        CGI_Sources_Xref.CGI_Source_Code%TYPE;
V_Price_Iso_Cy    Currencies.Iso_Currency%TYPE;
--
V_Id_1            Identification_Systems.Code%TYPE;
V_Id_2            Identification_Systems.Code%TYPE;
V_Id_3            Identification_Systems.Code%TYPE;
V_Id_4            Identification_Systems.Code%TYPE;
--
CURSOR C_Get_Identifier
     ( I_Fri_Ticker   Securities.Fri_Ticker%TYPE
     , I_Id_Syst_1    Identification_Systems.Code%TYPE
     , I_Id_Syst_2    Identification_Systems.Code%TYPE
     , I_Id_Syst_3    Identification_Systems.Code%TYPE
     , I_Id_Syst_4    Identification_Systems.Code%TYPE
     ) IS
SELECT Ticker
  FROM Tickers
 WHERE Fri_Ticker = I_FRI_Ticker
   AND Ticker NOT LIKE 'TMP%'
   AND Identification_System_Code IN ( I_Id_Syst_1
                                     , I_Id_Syst_2
                                     , I_Id_Syst_3
                                     , I_Id_Syst_4
                                     )
ORDER BY DECODE( Identification_System_Code
               , I_Id_Syst_1, 1
               , I_Id_Syst_2, 2
               , I_Id_Syst_3, 3
               , I_Id_Syst_4, 4
               , 5 );
--
BEGIN --{
  Pgmloc := 1630;
  V_Field_Value  := NULL;
  V_Fri_Ticker   := TO_NUMBER( SUBSTR( V_Data_Line
                                     , S_Fri_Ticker
                                     , L_Fri_Ticker
                                     ) );
  V_Exchange     := SUBSTR( V_Data_Line, S_Exchange, L_Exchange );
  V_Price_Iso_Cy := SUBSTR( V_Data_Line, S_Price_Iso_Cy, L_Price_Iso_Cy );
  IF V_Exchange IN ( '5003' )
  THEN
    Pgmloc := 1640;
    V_Id_1 := '#';
    V_Id_2 := NULL;
    V_Id_3 := NULL;
    V_Id_4 := NULL;
  ELSIF V_Exchange IN ('5006', '5007' )
  THEN
    Pgmloc := 1650;
    V_Id_1 := 'C';
    V_Id_2 := 'D';
    V_Id_3 := 'H';
    V_Id_4 := 'W';
  ELSE
    Pgmloc := 1660;
    V_Id_1 := 'O';
    V_Id_2 := 'Z';
    V_Id_3 := 'D';
    V_Id_4 := 'C';
  END IF;
  Pgmloc := 1670;
  OPEN C_Get_Identifier ( V_Fri_Ticker, V_Id_1, V_Id_2, V_Id_3, V_Id_4 );
  Pgmloc := 1680;
  FETCH C_Get_Identifier
   INTO V_Field_Value;
  Pgmloc := 1690;
  CLOSE C_Get_Identifier;
END Get_Provider_Identifier; --}}
--
--
--
PROCEDURE One_Field_Value
        ( V_Data_Line    IN  VARCHAR2
        , V_Field_Number IN  VARCHAR2
        , V_Field_Value  OUT VARCHAR2
        , V_Field_Price  OUT VARCHAR2
        , V_Field_Date   OUT VARCHAR2
        ) IS --{
--
C_Qty_Factor                 NUMBER  := 1000;
--
S_Price_Date                 INTEGER := 57;
L_Price_Date                 INTEGER := 10;
S_Price                      INTEGER := 68;
L_Price                      INTEGER := 18;
S_Yield                      INTEGER := 87;
L_Yield                      INTEGER := 18;
S_Open                       INTEGER := 106;
L_Open                       INTEGER := 18;
S_High                       INTEGER := 125;
L_High                       INTEGER := 18;
S_Low                        INTEGER := 144;
L_Low                        INTEGER := 18;
S_Volume                     INTEGER := 163;
L_Volume                     INTEGER := 15;
S_Bid_Date                   INTEGER := 179;
L_Bid_Date                   INTEGER := 10;
S_Bid                        INTEGER := 190;
L_Bid                        INTEGER := 18;
S_Bid_Yield                  INTEGER := 209;
L_Bid_Yield                  INTEGER := 18;
S_Ask_Date                   INTEGER := 228;
L_Ask_Date                   INTEGER := 10;
S_Ask                        INTEGER := 239;
L_Ask                        INTEGER := 18;
S_Ask_Yield                  INTEGER := 258;
L_Ask_Yield                  INTEGER := 18;
S_Mid_Date                   INTEGER := 277;
L_Mid_Date                   INTEGER := 10;
S_Mid                        INTEGER := 288;
L_Mid                        INTEGER := 18;
S_Mid_Yield                  INTEGER := 307;
L_Mid_Yield                  INTEGER := 18;
S_Maturity_Date              INTEGER := 326;
L_Maturity_Date              INTEGER := 10;
S_Coupon                     INTEGER := 337;
L_Coupon                     INTEGER := 13;
S_Pool_Factor                INTEGER := 351;
L_Pool_Factor                INTEGER := 18;
S_Duration                   INTEGER := 370;
L_Duration                   INTEGER := 18;
S_Sec_Name                   INTEGER := 389;
L_Sec_Name                   INTEGER := 40;
V_Field_Value_1              VARCHAR2(100);
V_Field_Value_2              VARCHAR2(100);
V_Field_Value_3              VARCHAR2(100);
V_Field_Value_4              VARCHAR2(100);
--
BEGIN --{
  V_Field_Value := NULL;
  V_Field_Price := NULL;
  V_Field_Date  := NULL;
  Pgmloc := 1700;
  CASE V_Field_Number --{
    WHEN '1'   THEN NULL; -- provider security id
                    Get_Provider_Identifier ( V_Data_Line, V_Field_Value );
    WHEN '13'  THEN Pgmloc := 1710;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Bid
                                                 , L_Bid
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Bid_Date
                                                   , L_Bid_Date
                                                   ) );
                    END IF;
    WHEN '14'  THEN Pgmloc := 1720;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Ask
                                                 , L_Ask
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Ask_Date
                                                   , L_Ask_Date
                                                   ) );
                    END IF;
    WHEN '501' THEN Pgmloc := 1730;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Bid_Yield
                                                 , L_Bid_Yield
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Bid_Date
                                                   , L_Bid_Date
                                                   ) );
                    END IF;
    WHEN '502' THEN Pgmloc := 1740;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Ask_Yield
                                                 , L_Ask_Yield
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Ask_Date
                                                   , L_Ask_Date
                                                   ) );
                    END IF;
    WHEN '506' THEN Pgmloc := 1750;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Bid
                                                 , L_Bid
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Bid_Date
                                                   , L_Bid_Date
                                                   ) );
                    END IF;
    WHEN '507' THEN Pgmloc := 1760;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Ask
                                                 , L_Ask
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Ask_Date
                                                   , L_Ask_Date
                                                   ) );
                    END IF;
-- 508 needs more specification!
    WHEN '508' THEN Pgmloc := 1770;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Bid_Date
                                                 , L_Bid_Date
                                                 ) );
    WHEN '510' THEN Pgmloc := 1780;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Price
                                                 , L_Price
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price_Date
                                                   , L_Price_Date
                                                   ) );
                    END IF;
    WHEN '511' THEN Pgmloc := 1790;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Open
                                                 , L_Open
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price_Date
                                                   , L_Price_Date
                                                   ) );
                    END IF;
    WHEN '512' THEN Pgmloc := 1800;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_High
                                                 , L_High
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price_Date
                                                   , L_Price_Date
                                                   ) );
                    END IF;
    WHEN '513' THEN Pgmloc := 1810;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Low
                                                 , L_Low
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price_Date
                                                   , L_Price_Date
                                                   ) );
                    END IF;
    WHEN '514' THEN Pgmloc := 1820;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Price_Date
                                                 , L_Price_Date
                                                 ) );
                    V_Field_Price := TRIM( SUBSTR( V_Data_Line
                                                 , S_Price
                                                 , L_Price
                                                 ) );
                    IF V_Field_Price IS NOT NULL
                    THEN
                      IF ( SUBSTR( V_Field_Price, LENGTH( V_Field_Price ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Price := V_Field_Price || ' ';
                      END IF;
                      V_Field_Price := TO_CHAR( TO_NUMBER( V_Field_Price
                                                         , C_Def_Number_Fmt ) );
                    END IF;
                    V_Field_Date  := V_Field_Value;
    WHEN '517' THEN Pgmloc := 1830;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Volume
                                                 , L_Volume
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price_Date
                                                   , L_Price_Date
                                                   ) );
                    END IF;
    WHEN '520' THEN Pgmloc := 1840;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Mid
                                                 , L_Mid
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Mid_Date
                                                   , L_Mid_Date
                                                   ) );
                    END IF;
    WHEN '521' THEN Pgmloc := 1850; -- Date of net asset
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Price_Date
                                                 , L_Price_Date
                                                 ) );
                    V_Field_Price := TRIM( SUBSTR( V_Data_Line
                                                 , S_Price
                                                 , L_Price
                                                 ) );
                    IF V_Field_Price IS NOT NULL
                    THEN
                      IF ( SUBSTR( V_Field_Price, LENGTH( V_Field_Price ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Price := V_Field_Price || ' ';
                      END IF;
                      V_Field_Price := TO_CHAR( TO_NUMBER( V_Field_Price
                                                         , C_Def_Number_Fmt ) );
                    END IF;
                    V_Field_Date  := V_Field_Value;
    WHEN '526' THEN Pgmloc := 1840;
--    Mid or Calculated mid
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Mid
                                                 , L_Mid
                                                 ) );
                    IF V_Field_Value IS NOT NULL
                    THEN  --{ we have a mid
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Mid_Date
                                                   , L_Mid_Date
                                                   ) );
                    ELSE -- }{ -- we have no mid - Try calculating one
                      V_Field_Value_1 := TRIM( SUBSTR( V_Data_Line
                                                   , S_Bid
                                                   , L_Bid
                                                   ) );
                      V_Field_Value_2 := TRIM( SUBSTR( V_Data_Line
                                                   , S_Ask
                                                   , L_Ask
                                                   ) );
                      V_Field_Value_3 := TRIM( SUBSTR( V_Data_Line
                                                   , S_Bid_Date
                                                   , L_Bid_Date
                                                   ) );
                      V_Field_Value_4 := TRIM( SUBSTR( V_Data_Line
                                                   , S_Ask_Date
                                                   , L_Ask_Date
                                                   ) );
                      IF V_Field_Value_1 IS NULL OR
                         V_Field_Value_2 IS NULL OR
                         NVL(V_Field_Value_3,'NULL3') <> 
                         NVL(V_Field_Value_4,'NULL4')
                      THEN --{
                        V_Field_Price := NULL;
                        V_Field_Date  := NULL;
                      ELSE --}{
                        IF ( SUBSTR( V_Field_Value_1, LENGTH( V_Field_Value_1 ), 1 )
                           NOT IN (' ', '-', '+') )
                        THEN
                           V_Field_Value_1 := V_Field_Value_1 || ' ';
                        END IF;
                        IF ( SUBSTR( V_Field_Value_2, LENGTH( V_Field_Value_2 ), 1 )
                           NOT IN (' ', '-', '+') )
                        THEN
                           V_Field_Value_2 := V_Field_Value_2 || ' ';
                        END IF;
                        V_Field_Price := TO_CHAR(
                           (TO_NUMBER(V_Field_Value_1, C_Def_Number_Fmt) +
                            TO_NUMBER(V_Field_Value_2, C_Def_Number_Fmt)) / 2);
                        V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                     , S_Bid_Date
                                                     , L_Bid_Date
                                                     ) );
                        V_Field_Value := V_Field_Price;
                      END IF; --}
                    END IF; --}
    WHEN '532' THEN Pgmloc := 1860;
                    IF ( TRIM( SUBSTR( V_Data_Line
                                     , S_Exchange
                                     , L_Exchange
                                     ) ) IN ( '5006', '5007' ) )
                    THEN
                      -- bid from initram
                      V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                   , S_Bid
                                                   , L_Bid
                                                   ) );
                      IF V_Field_Value IS NULL
                      THEN
                        V_Field_Price := NULL;
                        V_Field_Date  := NULL;
                      ELSE
                        IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                           NOT IN (' ', '-', '+') )
                        THEN
                           V_Field_Value := V_Field_Value || ' ';
                        END IF;
                        V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                           , C_Def_Number_Fmt ) );
                        V_Field_Price := V_Field_Value;
                        V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                     , S_Bid_Date
                                                     , L_Bid_Date
                                                     ) );
                      END IF;
                    ELSIF ( TRIM( SUBSTR( V_Data_Line
                                        , S_Exchange
                                        , L_Exchange
                                        ) ) IN ( '5003' ) )
                    THEN
                      -- Price from WG MBS
                      V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price
                                                   , L_Price
                                                   ) );
                      IF V_Field_Value IS NULL
                      THEN
                        V_Field_Price := NULL;
                        V_Field_Date  := NULL;
                      ELSE
                        IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                           NOT IN (' ', '-', '+') )
                        THEN
                           V_Field_Value := V_Field_Value || ' ';
                        END IF;
                        V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                           , C_Def_Number_Fmt ) );
                        V_Field_Price := V_Field_Value;
                        V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                     , S_Price_Date
                                                     , L_Price_Date
                                                     ) );
                      END IF;
                    ELSE
                      V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price
                                                   , L_Price
                                                   ) );
                      IF V_Field_Value IS NULL
                      THEN
                        V_Field_Price := NULL;
                        V_Field_Date  := NULL;
                      ELSE
                        IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                           NOT IN (' ', '-', '+') )
                        THEN
                           V_Field_Value := V_Field_Value || ' ';
                        END IF;
                        V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                           , C_Def_Number_Fmt ) );
                        V_Field_Price := V_Field_Value;
                        V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                     , S_Price_Date
                                                     , L_Price_Date
                                                     ) );
                      END IF;
                    END IF;
    WHEN '533' THEN Pgmloc := 1870;
                    IF ( TRIM( SUBSTR( V_Data_Line
                                     , S_Exchange
                                     , L_Exchange
                                     ) ) IN ( '5006', '5007' ) )
                    THEN
                      -- adjusted bid from initram
                      V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                   , S_Bid + Pad_Length
                                                   , L_Bid
                                                   ) );
                      IF V_Field_Value IS NULL
                      THEN
                        V_Field_Price := NULL;
                        V_Field_Date  := NULL;
                      ELSE
                        IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                           NOT IN (' ', '-', '+') )
                        THEN
                           V_Field_Value := V_Field_Value || ' ';
                        END IF;
                        V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                           , C_Def_Number_Fmt ) );
                        V_Field_Price := V_Field_Value;
                        V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                     , S_Bid_Date + Pad_Length
                                                     , L_Bid_Date
                                                     ) );
                      END IF;
                    ELSIF ( TRIM( SUBSTR( V_Data_Line
                                        , S_Exchange
                                        , L_Exchange
                                        ) ) IN ( '5003' ) )
                    THEN
                      -- net price from WG MBS
                      V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price + Pad_Length
                                                   , L_Price
                                                   ) );
                      IF V_Field_Value IS NULL
                      THEN
                        V_Field_Price := NULL;
                        V_Field_Date  := NULL;
                      ELSE
                        IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                           NOT IN (' ', '-', '+') )
                        THEN
                           V_Field_Value := V_Field_Value || ' ';
                        END IF;
                        V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                           , C_Def_Number_Fmt ) );
                        V_Field_Price := V_Field_Value;
                        V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                     , S_Price_Date + Pad_Length
                                                     , L_Price_Date
                                                     ) );
                      END IF;
                    ELSE
                      V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price + Pad_Length
                                                   , L_Price
                                                   ) );
                      IF V_Field_Value IS NULL
                      THEN
                        V_Field_Price := NULL;
                        V_Field_Date  := NULL;
                      ELSE
                        IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                           NOT IN (' ', '-', '+') )
                        THEN
                           V_Field_Value := V_Field_Value || ' ';
                        END IF;
                        V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                           , C_Def_Number_Fmt ) );
                        V_Field_Price := V_Field_Value;
                        V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                     , S_Price_Date + Pad_Length
                                                     , L_Price_Date
                                                     ) );
                      END IF;
                    END IF;
    WHEN '656' THEN Pgmloc := 1880;
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Price
                                                 , L_Price
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price_Date
                                                   , L_Price_Date
                                                   ) );
                    END IF;
    WHEN '719' THEN Pgmloc := 1890; -- net asset value
                    V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                 , S_Price
                                                 , L_Price
                                                 ) );
                    IF V_Field_Value IS NULL
                    THEN
                      V_Field_Price := NULL;
                      V_Field_Date  := NULL;
                    ELSE
                      IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                         NOT IN (' ', '-', '+') )
                      THEN
                         V_Field_Value := V_Field_Value || ' ';
                      END IF;
                      V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                         , C_Def_Number_Fmt ) );
                      V_Field_Price := V_Field_Value;
                      V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                   , S_Price_Date
                                                   , L_Price_Date
                                                   ) );
                    END IF;
    WHEN '1008' THEN Pgmloc := 1900;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Price_Iso_Cy
                                                  , L_Price_Iso_Cy
                                                  ) );
    WHEN '1013' THEN Pgmloc := 1910;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Sec_Name
                                                  , L_Sec_Name
                                                  ) );
    WHEN '1014' THEN Pgmloc := 1920;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Sec_Name
                                                  , L_Sec_Name
                                                  ) );
    WHEN '1045' THEN NULL; -- Highest value for this year or period.
    WHEN '1046' THEN NULL; -- Year high date
    WHEN '1047' THEN NULL; -- Lowest value for this year or period.
    WHEN '1048' THEN NULL; -- Year low date
    WHEN '1207' THEN -- Share outstanding
                     BEGIN
--                --  Always apply a factor to Quantity outstanding
                       SELECT TO_CHAR( MAX( QUANTITY ) / C_Qty_Factor)
                         INTO V_Field_Value
                         FROM Quantity_Outstanding QO
                        WHERE QO.FRI_Ticker = TO_NUMBER(
                                              TRIM( SUBSTR( V_Data_Line
                                                          , S_Fri_Ticker
                                                          , L_Fri_Ticker
                                                          ) ) )
                          AND QO.Data_Approved_Flag = 'Y'
                          AND QO.Effective_Date = (
                              SELECT MAX( QO1.Effective_Date )
                                FROM Quantity_Outstanding QO1
                               WHERE QO1.Fri_Ticker = QO.Fri_Ticker
                                 AND QO1.Data_Approved_Flag = 'Y'
                                 AND QO1.Effective_Date <=
                                     Constants.Get_Site_Date
                              )
                         ;
                     EXCEPTION
                       WHEN NO_DATA_FOUND THEN NULL;
                     END;
    WHEN '1326' THEN Pgmloc := 1930;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Yield
                                                  , L_Yield
                                                  ) );
                     IF V_Field_Value IS NULL
                     THEN
                       V_Field_Price := NULL;
                       V_Field_Date  := NULL;
                     ELSE
                       IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                         V_Field_Value := V_Field_Value || ' ';
                       END IF;
                       V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                          , C_Def_Number_Fmt ) );
                       V_Field_Price := V_Field_Value;
                       V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                    , S_Price_Date
                                                    , L_Price_Date
                                                    ) );
                     END IF;
    WHEN '1328' THEN Pgmloc := 1940;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Yield
                                                  , L_Yield
                                                  ) );
                     IF V_Field_Value IS NULL
                     THEN
                       V_Field_Price := NULL;
                       V_Field_Date  := NULL;
                     ELSE
                       IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                         V_Field_Value := V_Field_Value || ' ';
                       END IF;
                       V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                          , C_Def_Number_Fmt ) );
                       V_Field_Price := V_Field_Value;
                       V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                    , S_Price_Date
                                                    , L_Price_Date
                                                    ) );
                     END IF;
    WHEN '1500' THEN Pgmloc := 1950;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Maturity_Date
                                                  , L_Maturity_Date
                                                  ) );
    WHEN '1501' THEN Pgmloc := 1960;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Coupon
                                                  , L_Coupon
                                                  ) );
                     IF V_Field_Value IS NOT NULL
                     THEN
                       IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                          V_Field_Value := V_Field_Value || ' ';
                       END IF;
                       V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                          , C_Def_Number_Fmt ) );
                     END IF;
    WHEN '1521' THEN Pgmloc := 1970;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Pool_Factor
                                                  , L_Pool_Factor
                                                  ) );
                     IF V_Field_Value IS NULL
                     THEN
                       V_Field_Price := NULL;
                       V_Field_Date  := NULL;
                     ELSE
                       IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                          V_Field_Value := V_Field_Value || ' ';
                       END IF;
                       V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                          , C_Def_Number_Fmt ) );
                       V_Field_Price := V_Field_Value;
                       V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                    , S_Price_Date
                                                    , L_Price_Date
                                                    ) );
                     END IF;
    WHEN '1523' THEN Pgmloc := 1980;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Yield
                                                  , L_Yield
                                                  ) );
                     IF V_Field_Value IS NULL
                     THEN
                       V_Field_Price := NULL;
                       V_Field_Date  := NULL;
                     ELSE
                       IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                          V_Field_Value := V_Field_Value || ' ';
                       END IF;
                       V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                          , C_Def_Number_Fmt ) );
                       V_Field_Price := V_Field_Value;
                       V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                    , S_Price_Date
                                                    , L_Price_Date
                                                    ) );
                     END IF;
    WHEN '1524' THEN Pgmloc := 1990;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Yield
                                                  , L_Yield
                                                  ) );
                     IF V_Field_Value IS NULL
                     THEN
                       V_Field_Price := NULL;
                       V_Field_Date  := NULL;
                     ELSE
                       IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                          V_Field_Value := V_Field_Value || ' ';
                       END IF;
                       V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                          , C_Def_Number_Fmt ) );
                       V_Field_Price := V_Field_Value;
                       V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                    , S_Price_Date
                                                    , L_Price_Date
                                                    ) );
                     END IF;
    WHEN '1529' THEN Pgmloc := 2000;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Duration
                                                  , L_Duration
                                                  ) );
                     IF V_Field_Value IS NULL
                     THEN
                       V_Field_Price := NULL;
                       V_Field_Date  := NULL;
                     ELSE
                       IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                          V_Field_Value := V_Field_Value || ' ';
                       END IF;
                       V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                          , C_Def_Number_Fmt ) );
                       V_Field_Price := V_Field_Value;
                       V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                    , S_Price_Date
                                                    , L_Price_Date
                                                    ) );
                     END IF;
    WHEN '1600' THEN Pgmloc := 2010;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Maturity_Date
                                                  , L_Maturity_Date
                                                  ) );
    WHEN '1605' THEN NULL; -- Strike price
    WHEN '2651' THEN Pgmloc := 2020;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Price
                                                  , L_Price
                                                  ) );
                     IF V_Field_Value IS NULL
                     THEN
                       V_Field_Price := NULL;
                       V_Field_Date  := NULL;
                     ELSE
                       IF ( SUBSTR( V_Field_Value, LENGTH( V_Field_Value ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                          V_Field_Value := V_Field_Value || ' ';
                       END IF;
                       V_Field_Value := TO_CHAR( TO_NUMBER( V_Field_Value
                                                          , C_Def_Number_Fmt ) );
                       V_Field_Price := V_Field_Value;
                       V_Field_Date  := TRIM( SUBSTR( V_Data_Line
                                                    , S_Price_Date
                                                    , L_Price_Date
                                                    ) );
                     END IF;
    WHEN '2662' THEN Pgmloc := 2030;
                     V_Field_Value := TRIM( SUBSTR( V_Data_Line
                                                  , S_Price_Date
                                                  , L_Price_Date
                                                  ) );
                     V_Field_Price := TRIM( SUBSTR( V_Data_Line
                                                  , S_Price
                                                  , L_Price
                                                  ) );
                     IF V_Field_Price IS NOT NULL
                     THEN
                       IF ( SUBSTR( V_Field_Price, LENGTH( V_Field_Price ), 1 )
                          NOT IN (' ', '-', '+') )
                       THEN
                          V_Field_Price := V_Field_Price || ' ';
                       END IF;
                       V_Field_Price := TO_CHAR( TO_NUMBER( V_Field_Price
                                                          , C_Def_Number_Fmt ) );
                     END IF;
                     V_Field_Date  := V_Field_Value;
    WHEN '3000' THEN Pgmloc := 2040;
                     V_Field_Value := V_Prefered_Price;
    WHEN '3001' THEN Pgmloc := 2050;
                     V_Field_Value := V_Prefered_Date;
    ELSE Field_Value := V_Field_Number || '_NA';
  END CASE; --}
  --
EXCEPTION
WHEN OTHERS THEN
  dbms_output.put_line ( SUBSTR( 'At Pgmloc : '
                     || TO_CHAR( Pgmloc )
                     || ' '
                     || SUBSTR( SQLERRM( SQLCODE ), 1, 255 )
                     , 1
                     , 255
                     ) );
  raise;
  --
END One_Field_Value; --}}
--
--
--
BEGIN --{
  Pgmloc := 2060;
  P_Success := TRUE;
  --
  -------------------------------
  --  create the pricing message,
  --  open the log
  --  copy the input file
  --  open the copied file
  --  open the output file
  -------------------------------
  --
  Pgmloc := 2070;
  Cntl_Rec.In_File_Name  := P_In_File_Name;
  Cntl_Rec.Out_File_Name := P_Out_File_Name;
  Cntl_Rec.In_File_Open  := FALSE;
  Cntl_Rec.Out_File_Open := FALSE;
  Cntl_Rec.Log_File_Open := FALSE;
  Pgmloc := 2080;
  Process_Beginning( P_Program_Id
                   , Constants.Get_Site_Date
                   , P_Commit_Freq
                   , PRP
                   , Cntl_Rec
                   , Pricing_Msgs_Rec
                   , P_Success
                   , P_Message
                   );
  IF NOT P_Success THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 2090;
  COMMIT;
  --
  IF Pricing_Msgs_Rec.Date_Of_Prices = Constants.Get_Site_Date
  THEN
    V_Do_Accounting := TRUE;
  ELSE
    V_Do_Accounting := FALSE;
  END IF;
  --
  Pgmloc := 2100;
  V_Acc_Cgi_Stats := G_Acc_Cgi_Stats_Init;
  -- charger le format - etre sur que les champs sont la ou nous les attendons
  -- hardcode : reference entre les champs de cgi et ceux de fri
  -- ecrire le header
  BEGIN
    Pgmloc := 2110;
    SELECT CGI_Value
      INTO Data_Rec
      FROM CGI_File_Info CFI
         , Clients       C
     WHERE C.Fri_Client   = PRP.Fri_Client
       AND CFI.Fri_Client = NVL( C.Fri_Client_Universe, C.Fri_Client )
       AND CGI_Keyword    = 'li=';
    pgmloc := 2120;
    load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                  , Data_Rec
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
    IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
    Pricing_Msgs_Rec.Value_02 := Pricing_Msgs_Rec.Value_02 + 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    NULL;
  END;
  -- obtenir la date de debut du parent
  V_Program_Id := P_Program_Id;
  IF V_Program_Id > 9999
  THEN
    V_Program_Id := V_Program_Id / 100;
  END IF;
  Pgmloc := 2130;
  SELECT MAX( Date_of_Msg )
    INTO Parent_Date_of_Msg
    FROM Pricing_msgs
   WHERE Program_Id = V_Program_Id;
  --
  Pgmloc := 2140;
  Data_Rec := 'start time:'
           || TO_CHAR( NVL( Parent_Date_Of_Msg, Pricing_Msgs_Rec.Date_of_Msg )
                     , 'Dy Mon DD Hh24:MI:SS YYYY' );
  Pgmloc := 2150;
  load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                , Data_Rec
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  Pricing_Msgs_Rec.Value_02 := Pricing_Msgs_Rec.Value_02 + 1;
  --
  IF V_Do_Accounting
  THEN --{
    pgmloc := 2160;
    DELETE FROM ACC_CGI_Stats
     WHERE FRI_CLient       = PRP.FRI_CLient
       AND Product_Code     = PRP.Product_Code
       AND Sub_Product_Code = PRP.Sub_Product_Code
       AND Valuation_Date   = Pricing_Msgs_Rec.Date_of_Prices;
    Pgmloc := 2170;
    COMMIT;
    Pgmloc := 2180;
    DELETE Acc_CGI_Info
     WHERE Fri_Client       = PRP.Fri_Client
       AND Product_Code     = PRP.Product_Code
       AND Sub_Product_Code = PRP.Sub_Product_Code
       AND Valuation_Date   = Constants.Get_Site_Date;
    Pgmloc := 2190;
    COMMIT;
  END IF; --}
  --
  -- pour chaque ligne
  --
  END_of_File := FALSE;
  WHILE NOT End_of_File
  LOOP --{
    pgmloc := 2200;
    get_one_line( Cntl_Rec.In_File
                , Pricing_Msgs_Rec
                , V_Data_Line
                , P_Success
                , End_Of_File
                );
    IF NOT P_Success   THEN GOTO GET_OUT;  END IF;
    IF     End_Of_File THEN GOTO END_LOOP; END IF;
    Pricing_Msgs_Rec.Value_01 := Pricing_Msgs_Rec.Value_01 + 1;
    --
    V_Client_Security_Id := TRIM( SUBSTR( V_Data_Line
                                        , S_Client_Security_Id
                                        , L_Client_Security_Id
                                        ) );
    Data_Rec := TRIM( SUBSTR( V_Data_Line, S_Main_Id, L_Main_id ) );
  --   verifier que nous avons un ticker sinon erreur NSIN_NOT_FOUND
    Pgmloc := 2210;
    V_Fri_Ticker := TO_NUMBER( TRIM( SUBSTR( V_Data_Line
                                           , S_Fri_Ticker
                                           , L_Fri_Ticker
                                           ) ) );
    IF ( TRIM( SUBSTR( V_Data_Line, S_FRI_Ticker, L_Fri_Ticker ) ) IS NULL )
    THEN
      Data_Rec := Data_Rec
               || C_Field_Separator
               || 'NSIN_NOT_FOUND';
      IF V_Do_Accounting
      THEN --{
        IF V_Acc_Cgi_Stats.EXISTS( C_NSIN_NOT_FOUND )
        THEN
          V_Acc_Cgi_Stats( C_NSIN_NOT_FOUND ).Gross_Total
            := V_Acc_Cgi_Stats( C_NSIN_NOT_FOUND ).Gross_Total + 1;
          V_Acc_Cgi_Stats( C_NSIN_NOT_FOUND ).Net_Total
            := V_Acc_Cgi_Stats( C_NSIN_NOT_FOUND ).Net_Total + 1;
        ELSE
          R_Acc_Cgi_Stats.Fri_Client          := PRP.Fri_Client;
          R_Acc_Cgi_Stats.Product_Code        := PRP.Product_Code;
          R_Acc_Cgi_Stats.Sub_product_Code    := PRP.Sub_product_Code;
          R_Acc_Cgi_Stats.Cgi_Source_Code     := C_NSIN_NOT_FOUND;
          R_Acc_Cgi_Stats.Valuation_Date      := Pricing_Msgs_Rec.Date_of_Prices;
          R_Acc_Cgi_Stats.Gross_Total         := 1;
          R_Acc_Cgi_Stats.Net_Total           := 1;
          R_Acc_Cgi_Stats.Valuation_Date_BC   := NULL;
          R_Acc_Cgi_Stats.Last_User           := Constants.Get_User_id;
          R_Acc_Cgi_Stats.Last_Stamp          := SYSDATE;
          V_Acc_Cgi_Stats( C_NSIN_NOT_FOUND ) := R_Acc_Cgi_Stats;
        END IF;
        Pgmloc := 2220;
        INSERT
          INTO Acc_CGI_Info
             ( Fri_Client
             , Product_Code
             , Sub_Product_Code
             , CGI_Source_Code
             , Valuation_Date
             , Client_Security_Id
             , Fri_Ticker
             , Valuation_Date_BC
             , Last_User
             , Last_Stamp
             )
        VALUES
             ( PRP.Fri_Client
             , PRP.Product_Code
             , PRP.Sub_Product_Code
             , C_NSIN_NOT_FOUND
             , Pricing_Msgs_Rec.Date_of_Prices
             , V_Client_Security_Id
             , V_Fri_Ticker
             , NULL
             , Constants.Get_User_id
             , SYSDATE
             );
      END IF; --}
      GOTO End_Loop;
    END IF;
  --   verifier que nous avons une bourse sinon erreur CHECK_EXCHANGE
    IF ( TRIM( SUBSTR( V_Data_Line, S_Exchange, L_Exchange ) ) IS NULL )
    THEN
      Data_Rec := Data_Rec
               || C_Field_Separator
               || 'CHECK_EXCHANGE';
      IF V_Do_Accounting
      THEN --{
        IF V_Acc_Cgi_Stats.EXISTS( C_CHECK_EXCHANGE )
        THEN
          V_Acc_Cgi_Stats( C_CHECK_EXCHANGE ).Gross_Total
            := V_Acc_Cgi_Stats( C_CHECK_EXCHANGE ).Gross_Total + 1;
          V_Acc_Cgi_Stats( C_CHECK_EXCHANGE ).Net_Total
            := V_Acc_Cgi_Stats( C_CHECK_EXCHANGE ).Net_Total + 1;
        ELSE
          R_Acc_Cgi_Stats.Fri_Client          := PRP.Fri_Client;
          R_Acc_Cgi_Stats.Product_Code        := PRP.Product_Code;
          R_Acc_Cgi_Stats.Sub_product_Code    := PRP.Sub_product_Code;
          R_Acc_Cgi_Stats.Cgi_Source_Code     := C_CHECK_EXCHANGE;
          R_Acc_Cgi_Stats.Valuation_Date      := Pricing_Msgs_Rec.Date_of_Prices;
          R_Acc_Cgi_Stats.Gross_Total         := 1;
          R_Acc_Cgi_Stats.Net_Total           := 1;
          R_Acc_Cgi_Stats.Valuation_Date_BC   := NULL;
          R_Acc_Cgi_Stats.Last_User           := Constants.Get_User_id;
          R_Acc_Cgi_Stats.Last_Stamp          := SYSDATE;
          V_Acc_Cgi_Stats( C_CHECK_EXCHANGE ) := R_Acc_Cgi_Stats;
        END IF;
        Pgmloc := 2230;
        INSERT
          INTO Acc_CGI_Info
             ( Fri_Client
             , Product_Code
             , Sub_Product_Code
             , CGI_Source_Code
             , Valuation_Date
             , Client_Security_Id
             , Fri_Ticker
             , Valuation_Date_BC
             , Last_User
             , Last_Stamp
             )
        VALUES
             ( PRP.Fri_Client
             , PRP.Product_Code
             , PRP.Sub_Product_Code
             , C_CHECK_EXCHANGE
             , Pricing_Msgs_Rec.Date_of_Prices
             , V_Client_Security_Id
             , V_Fri_Ticker
             , NULL
             , Constants.Get_User_id
             , SYSDATE
             );
      END IF; --}
      GOTO End_Loop;
    END IF;
    Data_Rec := Data_Rec
             || C_Field_Separator
             || TRIM( SUBSTR( V_Data_Line, S_Exchange, L_Exchange ) )
             || C_Field_Separator
             || 'CS';
    IF V_Do_Accounting
    THEN --{
      V_CGI_Source_Code := TRIM( SUBSTR( V_Data_Line, S_Exchange, L_Exchange ) );
      IF V_Acc_Cgi_Stats.EXISTS( V_CGI_Source_Code )
      THEN
        V_Acc_Cgi_Stats( V_CGI_Source_Code ).Gross_Total
          := V_Acc_Cgi_Stats( V_CGI_Source_Code ).Gross_Total + 1;
        V_Acc_Cgi_Stats( V_CGI_Source_Code ).Net_Total
          := V_Acc_Cgi_Stats( V_CGI_Source_Code ).Net_Total + 1;
      ELSE
        R_Acc_Cgi_Stats.Fri_Client          := PRP.Fri_Client;
        R_Acc_Cgi_Stats.Product_Code        := PRP.Product_Code;
        R_Acc_Cgi_Stats.Sub_product_Code    := PRP.Sub_product_Code;
        R_Acc_Cgi_Stats.Cgi_Source_Code     := V_CGI_Source_Code;
        R_Acc_Cgi_Stats.Valuation_Date      := Pricing_Msgs_Rec.Date_of_Prices;
        R_Acc_Cgi_Stats.Gross_Total         := 1;
        R_Acc_Cgi_Stats.Net_Total           := 1;
        R_Acc_Cgi_Stats.Valuation_Date_BC   := NULL;
        R_Acc_Cgi_Stats.Last_User           := Constants.Get_User_id;
        R_Acc_Cgi_Stats.Last_Stamp          := SYSDATE;
        V_Acc_Cgi_Stats( V_CGI_Source_Code ) := R_Acc_Cgi_Stats;
      END IF;
      Pgmloc := 2240;
      INSERT
        INTO Acc_CGI_Info
           ( Fri_Client
           , Product_Code
           , Sub_Product_Code
           , CGI_Source_Code
           , Valuation_Date
           , Client_Security_Id
           , Fri_Ticker
           , Valuation_Date_BC
           , Last_User
           , Last_Stamp
           )
      VALUES
           ( PRP.Fri_Client
           , PRP.Product_Code
           , PRP.Sub_Product_Code
           , V_CGI_Source_Code
           , Pricing_Msgs_Rec.Date_of_Prices
           , V_Client_Security_Id
           , V_Fri_Ticker
           , NULL
           , Constants.Get_User_id
           , SYSDATE
           );
    END IF; --}
  --   verifier si nous avons un champ 3000 ou 3001 dans of
  --   si oui : selection a appliquer
    V_Prefered_Date  := NULL;
    V_Prefered_Price := NULL;
    FOR R_Fields IN C_Fields( PRP.Fri_Client
                            , V_Client_Security_Id
                            , 'pf=' )
    LOOP --{
      pgmloc := 2250;
      Field_Value := NULL;
      Field_Price := NULL;
      Field_Date  := NULL;
      One_Field_Value ( V_Data_Line
                      , R_Fields.CGI_Field_Number
                      , Field_Value
                      , Field_Price
                      , Field_Date
                      );
      IF V_Prefered_Price IS NULL
      OR V_Prefered_Date  IS NULL
      THEN
        V_Prefered_Date  := Field_Date;
        V_Prefered_Price := Field_Price;
      END IF;
    END LOOP; --} For fields in of=
    FOR R_Fields IN C_Fields( PRP.Fri_Client
                            , V_Client_Security_Id
                            , 'of=' )
    LOOP --{
      Field_Value := NULL;
      Field_Price := NULL;
      Field_Date  := NULL;
      Pgmloc := 2260;
      One_Field_Value ( V_Data_Line
                      , R_Fields.CGI_Field_Number
                      , Field_Value
                      , Field_Price
                      , Field_Date
                      );
      IF Field_Value IS NULL
      THEN
        Field_Value := 'NO_' || R_Fields.CGI_Field_Number || '_DATA';
      END IF;
      Data_Rec := Data_Rec || C_Field_Separator || Field_Value;
  --   pour chaque champ dans of
  --     lien avec le champ fri
  --     erreur possible NO_xxx_DATA
  --     si champ inconnu xxx_NA
    END LOOP; --} For fields in of=
<< End_Loop >>
    IF NOT End_Of_File
    THEN
      Pgmloc := 2270;
      load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                    , Data_Rec
                                    , Pricing_Msgs_Rec
                                    , P_Success
                                    );
      IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
      Pricing_Msgs_Rec.Value_02 := Pricing_Msgs_Rec.Value_02 + 1;
    END IF;
  END LOOP; --} while not end_of_file
  -- ecrire ligne Record count
  Pgmloc := 2280;
  Data_Rec := 'RECORD COUNT: '
           || Pricing_Msgs_Rec.Value_01;
  load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                , Data_Rec
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  Pricing_Msgs_Rec.Value_02 := Pricing_Msgs_Rec.Value_02 + 1;
  -- ecrire ligne end time: sysdate
  Pgmloc := 2290;
  Data_Rec := 'end time:'
           || TO_CHAR( SYSDATE
                     , 'Dy Mon DD Hh24:MI:SS YYYY' );
  load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                , Data_Rec
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  Pricing_Msgs_Rec.Value_02 := Pricing_Msgs_Rec.Value_02 + 1;
  -- ecrire ti=
  BEGIN
    Pgmloc := 2300;
    SELECT CGI_Value
      INTO Data_Rec
      FROM CGI_File_Info CFI
         , Clients       C
     WHERE C.Fri_Client   = PRP.Fri_Client
       AND CFI.Fri_Client = NVL( C.Fri_Client_Universe, C.Fri_Client )
       AND CGI_Keyword    = 'ti=';
    pgmloc := 2310;
    load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                  , Data_Rec
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
    IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
    Pricing_Msgs_Rec.Value_02 := Pricing_Msgs_Rec.Value_02 + 1;
  EXCEPTION
  WHEN NO_DATA_FOUND THEN
    NULL;
  END;
  --
  IF V_Acc_CGI_Stats.COUNT > 0
  THEN --{
    Pgmloc := 2320;
    V_Cgi_Source_Code := V_ACC_CGI_STATS.FIRST;
    WHILE TRUE
    LOOP --{
      Pgmloc := 2330;
      R_Acc_Cgi_Stats := V_Acc_Cgi_Stats( V_Cgi_Source_Code );
      pgmloc := 2340;
      INSERT INTO ACC_CGI_Stats
      VALUES R_Acc_Cgi_Stats;
      Pgmloc := 2350;
      V_Cgi_Source_Code := V_Acc_Cgi_Stats.NEXT( V_Cgi_Source_Code );
      --
      EXIT WHEN V_Cgi_Source_Code IS NULL;
      --
    END LOOP; --} while true
  END IF; --} if count > 0
  --
  pgmloc := 2360;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'012'
                                   , 'N'
                                   , TO_CHAR( SYSDATE
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  pgmloc := 2370;
  load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 2380;
  Load_Supplier_Rules.File_Close_All( Pricing_Msgs_Rec
                                    , P_Success
                                    );
  Cntl_Rec.Log_File_Open := FALSE;
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 2390;
  UPDATE Break_Points
     SET Status     = 'DONE'
       , Nb_Records = Pricing_Msgs_Rec.Value_01
       , Last_User  = Constants.Get_User_Id
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  pgmloc := 2400;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'060'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  pgmloc := 2410;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'062'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  GOTO Upd_Prc_Msg;
  --
<<GET_OUT>>
  pgmloc := 2420;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'070'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'072'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
--
<<Upd_Prc_Msg>>
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  --
  pgmloc := 2430;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 2440;
  commit;
--
EXCEPTION
WHEN OTHERS THEN
  P_Message := SUBSTR( 'At Pgmloc : '
                     || TO_CHAR( Pgmloc )
                     || ' '
                     || SUBSTR( SQLERRM( SQLCODE ), 1, 255 )
                     , 1
                     , 255
                     );
  IF Pricing_Msgs_Rec.Err_Text IS NULL
  THEN
    Pricing_Msgs_Rec.Err_Text := P_Message;
  END IF;
  Pgmloc := 2450;
  ROLLBACK;
  Pgmloc := 2460;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 2470;
  commit;
  IF Cntl_Rec.Log_File_Open
  THEN
    pgmloc := 2480;
    load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                  , Pricing_Msgs_Rec.Msg_Text
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
  END IF;
  --
  Load_Supplier_Rules.File_Close_All( Pricing_msgs_Rec
                                    , P_Success
                                    );
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  P_Success := FALSE;
  --
END Post_Extract; --}}
-----------------------------------------------------------------------
PROCEDURE Define_Fields (P_Field_Name_Code IN     VARCHAR2
                        ,P_Rec_Field       IN OUT Fmt_Rec
                        )
IS
-- CURSOR to get the format lamaut of a given record type, for a field name code
-- parameter keyword
--
   V_Rec_Fld            Client_Format_Layouts%ROWTYPE;
--
   CURSOR Get_Fld IS
   SELECT *
   FROM   Client_Format_Layouts
   Where  Fri_Client=G_Fri_Client
   AND    Product_Code=G_Product_Code
   AND    Record_Type_Code=G_Record_Type
   AND    Component_Code = NVL(G_Component,Component_Code)
   AND    Product_Field_Name_Code = P_Field_Name_Code
   ORDER BY Starting_Byte;
--
BEGIN
   pgmloc := 2490;
   P_Rec_Field(P_Field_Name_Code) := V_Rec_Fld;
--
   pgmloc := 2500;
   OPEN Get_Fld;
   pgmloc := 2510;
   FETCH Get_Fld INTO V_Rec_Fld;
   IF Get_Fld%FOUND
   THEN
      pgmloc := 2520;
      P_Rec_Field(P_Field_Name_Code) := V_Rec_Fld;
--    dbms_output.put_line(P_field_Name_Code || ' ' || P_Rec_Field(P_Field_Name_Code).Starting_Byte);
   END IF;
   pgmloc := 2530;
   CLOSE Get_Fld;
END Define_Fields;
--------------------------------------------------------------------------------
PROCEDURE Write_DVS_Rec (P_Out_File IN      Utl_File.File_Type
                        ,P_Out_Rec  IN      VARCHAR2
                        ,P_EOF_Str  IN      VARCHAR2
                        ,P_Status   IN      VARCHAR2
                        ,P_Success      OUT BOOLEAN
                        ,P_Message      OUT VARCHAR2)
AS
--
C_Fld_Add_Del       Product_Field_Names.Code%TYPE:='ADD_UPD_DEL';
Fld_Rec             Fmt_Rec;
V_Fld               VARCHAR2(512);
V_Outfld            VARCHAR2(512);
V_Out_Rec           VARCHAR2(1024):=NULL;
V_Tmp_Rec           VARCHAR2(1024):=NULL;
V_Out_Rec_Canc      VARCHAR2(1024):=NULL;
V_Out_Len           NUMBER;
V_Regexp            VARCHAR2(30);
--
CURSOR Get_Num_Fld IS
SELECT Starting_Byte,Field_Length
FROM Client_Format_Layouts L
    ,Product_Field_Names N
WHERE L.Fri_Client = G_Fri_Client
AND   L.Product_Code = G_Product_Code
AND   L.Component_Code = G_Component
AND   L.Record_Type_Code = G_Record_Type
AND   L.Product_Field_Name_COde = N.Code
AND   N.Field_Datatype = 'NUMBER'
order by 1;
--
BEGIN
   G_Routine := 'WRITE_OUT_REC';
   P_Success := FALSE;
   P_MEssage := NULL;
-- Fix numberic field format
   V_Out_Rec := P_Out_Rec;
   V_Out_Rec_Canc := P_Out_Rec;
   For Fld IN Get_Num_Fld
   LOOP
      V_Fld := SUBSTR(V_out_Rec,Fld.Starting_Byte,Fld.Field_Length);
      V_Outfld := LPAD(TRIM(trailing '.' FROM
                       TRIM(BOTH '0' FROM TRIM(V_Fld))),Fld.Field_Length);
      V_Tmp_Rec := NULL;
      IF Fld.Starting_Byte > 1
      THEN
         V_Tmp_Rec := SUBSTR(V_Out_Rec,1,Fld.Starting_Byte-1);
      END IF;
      V_Tmp_Rec := V_Tmp_Rec || V_Outfld;
      IF Fld.Starting_Byte+Fld.Field_Length <= LENGTH(P_Out_Rec)
      THEN
         V_Tmp_Rec := V_Tmp_Rec ||
                      SUBSTR(V_Out_Rec,Fld.Starting_Byte+Fld.Field_Length);
      END IF;
      V_Out_Rec := V_Tmp_Rec;
   END LOOP;
--
-- Up to here, format is still fixed
--
   IF P_Status IS NOT NULL
   THEN
      Define_Fields(C_Fld_Add_Del,Fld_Rec);
--
      IF INSTR(P_Status,'D')>0 OR INSTR(P_Status,'U')>0
      THEN
         pgmloc := 2540;
--
         V_Out_Rec_Canc := SUBSTR(V_Out_Rec,1,
                                  Fld_Rec(C_Fld_Add_Del).Starting_Byte-1) ||
                           'CANC' ||
                           SUBSTR(V_Out_Rec, Fld_Rec(C_Fld_Add_Del).Starting_Byte+1);
--
      END IF;
      V_Out_Rec := SUBSTR(V_Out_Rec,1,
                         Fld_Rec(C_Fld_Add_Del).Starting_Byte-1) ||
                   SUBSTR(V_Out_Rec, Fld_Rec(C_Fld_Add_Del).Starting_Byte+1);
--
   END IF;
-- Remove all blanks before and after Delimiter
   V_Regexp := '( ){0,}\' || G_Delimiter || '( ){0,}';
   V_Out_Rec := REGEXP_REPLACE(V_Out_Rec, V_RegExp ,G_Delimiter);
   V_Out_Rec_Canc := REGEXP_REPLACE(V_Out_Rec_Canc, V_RegExp ,G_Delimiter);
--
-- Record written until EOL string
   IF P_EOF_Str IS NOT NULL
   THEN
      V_Out_Len := INSTR(V_Out_Rec,P_EOF_Str)-1;
      IF V_Out_Len > 1
      THEN
         V_Out_Rec := SUBSTR(V_Out_Rec,1,V_Out_Len);
      END IF;
      V_Out_Len := INSTR(V_Out_Rec_Canc,P_EOF_Str)-1;
      IF V_Out_Len > 1
      THEN
         V_Out_Rec_Canc := SUBSTR(V_Out_Rec_Canc,1,V_Out_Len);
      END IF;
   END IF;
--
   IF INSTR(P_Status,'D')>0 OR INSTR(P_Status,'U')>0
   THEN
      pgmloc := 2550;
      Manage_Utl_File.Write_File(P_Out_File
                                ,V_Out_Rec_Canc
                                ,P_Success
                                ,P_message);
      G_Outline := G_Outline + 1;
   END IF;
   IF INSTR(P_Status,'A')>0 OR
      G_Record_Type IN (C_Header,C_Trailer)
   THEN
      pgmloc := 2560;
      Manage_Utl_File.Write_File(P_Out_File
                                ,V_Out_Rec
                                ,P_Success
                                ,P_message);
      G_Outline := G_Outline + 1;
   END IF;
--
   P_Success := TRUE;
   EXCEPTION
   WHEN OTHERS THEN
      Manage_Messages.Get_Oasis_Message
                            ( C_GEN_Message||'070'
                            , 'N'
                            , pgmloc || ' ' ||
                              G_Routine || ' ' ||
                              SUBSTR( SQLERRM( SQLCODE ), 1, 400 )
                            , P_Message
                            );
END Write_DVS_Rec;
--------------------------------------------------------------------------------
Procedure Post_DVS_Extract
         (P_Program_ID       IN     OPS_Scripts.Program_ID%TYPE
         ,P_Fri_Client       IN     Clients.Fri_Client%TYPE
         ,P_Product_Code     IN     Products.Code%TYPE
         ,P_In_File_Name     IN     VARCHAR2
         ,P_Out_File_Name    IN     VARCHAR2
         ,P_Delimiter        IN     VARCHAR2
         ,P_Success             OUT BOOLEAN
         ,P_Message             OUT NOCOPY VARCHAR2)
AS
   TYPE Rec_Num_Type IS TABLE OF NUMBER INDEX BY VARCHAR2(1);
   TYPE Src_Num_Type IS TABLE OF NUMBER INDEX BY VARCHAR2(4);
   TYPE Src_List_Type IS TABLE OF Sources.Code%TYPE INDEX BY BINARY_INTEGER;
--
--
   C_Cash_Div                   VARCHAR2(1):='D';
   C_Splits                     VARCHAR2(1):='S';
   C_Stk_Div                    VARCHAR2(1):='X';
--
   C_Delim                      VARCHAR2(1) := '|';
   C_Def_Freq                   VARCHAR2(4) := 'NOA';
   -- Format_Output parameter
   C_Output_SEQ                 VARCHAR2(20):='#SEQ#';
   C_Output_Freq                VARCHAR2(20):='#FREQ#';
   C_Output_Component           VARCHAR2(20):='#CMP=';
   C_Output_EOL                 VARCHAR2(20):='#EOL#';
   -- Field_Name_Code parameter
   C_RecType                    Product_Field_Names.Code%TYPE:='RECORD_TYPE';
   C_Fld_ID                     Product_Field_Names.Code%TYPE:='CLT_SEC_ID';
   C_Fld_Ex_Date                Product_Field_Names.Code%TYPE:='EX_DATE';
   C_Fld_EXCH                   Product_Field_Names.Code%TYPE:='EXCHANGE';
   C_Fld_Add_Del                Product_Field_Names.Code%TYPE:='ADD_UPD_DEL';
   C_Fld_Div_Type               Product_Field_Names.Code%TYPE:='DIV_TYPE';
   C_Fld_Div_Note               Product_Field_Names.Code%TYPE:='DIV_NOTE';
   C_Fld_currency               Product_Field_Names.Code%TYPE:='CASH_DIV_ISO'; -- Mourad OASIS_6270 19Jan2016
--
   V_In_rec                     VARCHAR2(512);
   V_Out_rec                    VARCHAR2(512);
   V_Inline                     NUMBER;
--
   V_Component                  Product_Components.Component_Code%TYPE;
   V_Prev_Component             Product_Components.Component_Code%TYPE;
   V_Record_Type                Record_Types.Record_Type_Code%TYPE;
   V_Max_Rec                    NUMBER;
   V_Num_Field                  NUMBER;
   n                            NUMBER;
   V_Skipline                   NUMBER;
   V_Delim                      VARCHAR2(1);
--
   In_File                      Utl_File.File_Type;
   Out_File                     Utl_File.File_Type;
--
   V_Total_Lines                NUMBER;
   V_Rec_Num_Tab                Rec_Num_Type;
   V_Src_Num                    Src_Num_Type;
   V_Init_Src                   Src_Num_Type;
   V_ID                         VARCHAR2(15);
   V_Prev_ID                    VARCHAR2(15);
   V_Ex_Date                    DATE;
   V_Prev_Ex_Date               DATE;
   V_Src                        Sources.Code%TYPE;
   V_Exchange                   Sources.Code%TYPE;
   V_Prev_Exchange              Sources.Code%TYPE;
   V_Div_Type                   Dividend_Types.Code%TYPE;
   V_Prev_Div_Type              Dividend_Types.Code%TYPE;
   V_Div_Note                   Dividend_Notes.Code%TYPE;
   V_Prev_Div_Note              Dividend_Notes.Code%TYPE;
   v_cash_div_iso               dividend_schedule.amount_currency%TYPE; --OASIS-6270 Mourad 19Jan2016
   V_Field                      VARCHAR2(1024);
   V_Fri_Client                 Clients.Fri_Client%TYPE;
   V_Fri_Ticker                 Tickers.Fri_Ticker%TYPE;
   V_Priority                   NUMBER;
   V_Add_Del                    VARCHAR2(10);
   V_Status                     VARCHAR2(10);
   V_Seq                        NUMBER;
   V_Freq                       VARCHAR2(4);
   V_App_Order                  NUMBER;
   EOF                          BOOLEAN;
   Src_Defined                  BOOLEAN;
   V_New_Rec                    BOOLEAN;
   pos                          NUMBER;
   len                          NUMBER;
   V_Success                    BOOLEAN;
   V_Message                    VARCHAR2(512);
   v_parm_value                 ops_scripts_parms.parm_value%TYPE;              --OASIS-6270 Mourad 19Jan2016
   v_length                     number := 0;                                    --OASIS-6270 Mourad 19Jan2016
   v_count                      number := 0;                                    --OASIS-6270 Mourad 19Jan2016
   v_value                      varchar2(500) := null;                          --OASIS-6270 Mourad 19Jan2016
--
   Fld_Rec                      Fmt_Rec;
   Init_Fld_Rec                 Fmt_Rec;
--
-- AP 05Dec2012  Keep id and component from previous read line 
   V_Component_Bef              Product_Components.Component_Code%TYPE;
   V_ID_Bef                     VARCHAR2(15);
-- CURSOR to get the number of records per record type
   CURSOR REC_TYPE_CUR IS
   SELECT L.Record_Type_Code
         ,MAX(L.Record_Position)
   FROM   Client_Format_Layouts L
   WHERE  L.Fri_Client = P_Fri_Client
   AND    L.Product_Code= P_Product_Code
   GROUP BY L.Record_Type_Code
   ORDER BY DECODE(L.Record_Type_Code,'H',1,
                                    'D',2,
                                    'T',3,4);
--
-- CURSOR Get Security
--
CURSOR Get_Client_Sec(I_ID   Clients_UNiv.Client_Security_ID%TYPE) IS
SELECT DISTINCT U.Fri_Client,U.Fri_Ticker
FROM  Client_List_Products L
     ,Clients_Univ U
WHERE L.Fri_Client = P_Fri_Client
AND   L.Product_Code = P_Product_Code
AND   L.Active_Flag = 'Y'
AND   L.Include_Fri_Client = U.Fri_CLient
AND   U.Client_Security_ID = I_ID
UNION
SELECT DISTINCT U.Fri_Client,U.Fri_Ticker
FROM  Clients C
     ,Clients_Univ U
WHERE C.Fri_Client = P_Fri_Client
AND   U.Fri_Client = NVL(C.Fri_Client_Universe,C.Fri_Client)
AND   U.Client_Security_ID = I_ID
order by 1;
--
-- CURSOR to get the list of sources to be extracted for the client
--
CURSOR Get_Src_List IS
SELECT DISTINCT X.STP_Source_Code,S.Priority_Number
FROM  CGI_Sources_Xref X
     ,CGI_Exchanges S
WHERE S.Fri_Client = V_Fri_Client
AND   S.Client_Security_ID = V_ID
AND   X.CGI_Source_Code = S.fri_Source_Code
AND   X.Source_or_supplier = 'S'
Order by Priority_number;
--
-- Get dividend info
-- IF no Xref for frequency value, would be default to frequency code
--
CURSOR Get_Div_Info
IS
SELECT  D.Statpro_Seqnum
-- Seperate fetch of frequency from cursor
       ,DECODE(A.App_Flag,'Y',1,2)
FROM    Dividend_Schedule D
       ,Approved_Flags    A
WHERE   V_Component IN ('D','X','Z')
AND     D.Fri_Ticker = V_Fri_Ticker
AND     D.Ex_Date    = V_Ex_date
AND     D.Source_Code = V_Exchange
AND     D.Dividend_Type_Code = V_Div_Type
AND     D.Dividend_Note_Code = V_Div_Note
AND     D.Data_Approved_Flag = A.Code
UNION
SELECT  D.Statpro_Seqnum
       ,DECODE(A.App_Flag,'Y',1,2)
FROM    Splits            D
       ,Approved_Flags    A
WHERE   V_Component = 'S'
AND     D.Fri_Ticker = V_Fri_Ticker
AND     D.Ex_Date    = V_Ex_date
AND     D.Source_Code = V_Exchange
AND     D.Data_Approved_Flag = A.Code
ORDER BY 2
;
CURSOR Get_Clt_Freq
IS
SELECT NVL(NVL(X.Output_Client_Xref_Code,X.Client_Xref_Code),C_Def_FREQ)
FROM    Securities        S
       ,Clients_Xref_Tables XT
       ,Clients_Xref      X
       ,Frequencies       F
WHERE   V_Component IN ('D','X','Z')
AND     S.Fri_Ticker = V_Fri_Ticker
AND     F.Value = NVL(S.Income_Frequency_Value,0)
AND     XT.Fri_Client = P_Fri_Client
AND     XT.Table_Name = 'FREQUENCIES'
AND     X.Fri_Client = NVL(XT.Fri_Client_Xref,XT.Fri_Client)
AND     X.Table_Name = 'FREQUENCIES'
AND     X.Fri_Xref_Code = S.Income_Frequency_Value;
--
-- CURSOR to get the format layout of a given record type, for an output format
-- parameter keyword
--
   CURSOR CUR_Get_Fld (I_Format VARCHAR2) IS
   SELECT *
   FROM   Client_Format_Layouts
   Where  Fri_Client=P_Fri_Client
   AND    Product_Code=P_Product_Code
   AND    Record_Type_Code=V_Record_Type
   AND    Component_Code = V_Component
   AND    INSTR(Output_Format,I_Format,1) > 0
   ORDER BY Starting_Byte;
--
   Out_Fld_Rec          Client_Format_Layouts%ROWTYPE;
   
-- OASIS-6270 mourad 19Jan2016
  CURSOR get_currency_cur(c_cy currencies.iso_currency%TYPE) IS
    SELECT COUNT (*) cnt
      FROM currencies
     WHERE iso_currency = c_cy;
   
   TYPE tab_parm_values IS 
     TABLE OF ops_scripts_parms.parm_value%type
     INDEX BY ops_scripts_parms.parm_value%type;
   t_parm_values tab_parm_values;   
-- end OASIS-6270 mourad 19Jan2016 
--
BEGIN  
    -- OASIS-6270 mourad 19Jan2016
    v_parm_value := UPPER (scripts.get_arg_value (P_Program_ID, c_parm_name_curr));
    
    IF v_parm_value is not null then      
      v_parm_value := replace(v_parm_value || chr(44),chr(32), ''); -- Add the delimiter and remove all spaces
      v_length     := REGEXP_COUNT(v_parm_value, ',', 1);  -- How many currencies in the parameter 
      FOR i IN 1..v_length LOOP 
        v_value  := TRIM (REGEXP_SUBSTR (v_parm_value, '[^,]+', 1, i)); -- get currency one by one from parms value
        FOR cy_rec IN get_currency_cur(v_value) LOOP -- check if the currency is valid
          v_count := cy_rec.cnt;
        END LOOP;
          
        if v_count > 0 then -- if currency is valid  
          t_parm_values(v_value) :=  v_value;
        end if;   
      END LOOP;    
    END IF;  
    -- end OASIS-6270 mourad 19Jan2016
--
    V_Success := FALSE;
    V_Message := NULL;
    G_Delimiter := NVL(P_Delimiter,C_Delim);
    V_Inline  := 0;
--
    G_Fri_Client := P_Fri_Client;
    G_Product_Code := P_Product_Code;
    G_Outline      := 0;
--
    pgmloc := 2570;
    Utl_File.FClose_all;
--  dbms_output.put_line('Opening Input file');
    Manage_Utl_File.Open_File(P_In_File_Name
                             ,'R'
                             ,In_File
                             ,V_Success
                             ,V_Message);
    IF NOT V_Success THEN GOTO Get_Out; END IF;
--
-- Preliminary reading of the input file
--
    pgmloc := 2580;
    V_Total_Lines := 0;
    LOOP -- { Read file
       pgmloc := 2590;
       Manage_Utl_File.Get_Line(In_File
                               ,V_In_Rec
                               ,EOF
                               ,V_Success
                               ,V_Message);
       IF NOT V_Success THEN GOTO Get_Out; END IF;
       EXIT WHEN EOF;
       V_Total_Lines := V_Total_Lines + 1;
       pgmloc := 2600;
    END LOOP; -- }
    --
    -- Close and reopen file for processing
    --
    pgmloc := 2610;
    Manage_Utl_File.Close_File(In_File
                              ,V_Success
                              ,V_Message);
    pgmloc := 2620;
    Manage_Utl_File.Open_File(P_In_File_Name
                             ,'R'
                             ,In_File
                             ,V_Success
                             ,V_Message);
--
--  Open ouput file
--
    pgmloc := 2630;
    Manage_Utl_File.Open_File(P_Out_File_Name
                             ,'W'
                             ,Out_File
                             ,V_Success
                             ,V_Message);
    --
    IF NOT V_Success THEN GOTO Get_Out; END IF;
    --
    -- If file is empty
    -- There's nothing to be done - get out as a success
    IF V_Total_Lines = 0
    THEN
       V_Success := TRUE;
       GOTO Get_Out;
    END IF;
--
--  Get number of records per record type in Client_Format_Layouts
--
    pgmloc := 2640;
    V_Rec_Num_Tab(C_Header):=0;
    V_Rec_Num_Tab(C_Data  ):=0;
    V_Rec_Num_Tab(C_Trailer):=0;
    pgmloc := 2650;
    OPEN Rec_Type_Cur;
    pgmloc := 2660;
    LOOP  -- { LOOP Rec_Type_Cur
       pgmloc := 2670;
       FETCH Rec_Type_CUR into V_Record_Type
                              ,V_Max_Rec;
       EXIT WHEN Rec_Type_Cur%NOTFOUND;
       pgmloc := 2680;
       V_Rec_Num_Tab(V_Record_Type) := V_Max_Rec;
    END LOOP;  --}
    Close Rec_Type_Cur;
    IF NOT V_Rec_Num_Tab.EXISTS(C_Header)
    THEN
       V_Rec_Num_Tab(C_Header) := 0;
    END IF;
    IF NOT V_Rec_Num_Tab.EXISTS(C_Trailer)
    THEN
       V_Rec_Num_Tab(C_Trailer) := 0;
    END IF;
    pgmloc := 2690;
--  Number of data line(s) in body
    V_Rec_Num_Tab(C_Data) := V_Total_Lines - V_Rec_Num_Tab(C_Header)
                                          - V_Rec_Num_Tab(C_Trailer);

--  dbms_output.put_line('Num Data lines: ' || V_Rec_Num_Tab(C_Data));
--
--  Go through file in the order record type 'H','D','T'
--
    V_Record_Type := NULL;
    V_Component := NULL;
    Src_Defined := FALSE;
    LOOP  --{
       pgmloc := 2700;
       IF V_Record_Type IS NULL AND
          V_Rec_Num_Tab(C_Header) > 0
       THEN -- Header
          pgmloc := 2710;
          V_Max_Rec := V_Rec_Num_Tab(C_Header);
          V_Record_Type := C_Header;
       ELSIF NVL(V_Record_Type,'H') = 'H' AND
             V_Rec_Num_Tab(C_Data) > 0
       THEN  -- Data
          pgmloc := 2720;
          V_Max_Rec := V_Rec_Num_Tab(C_Data);
          V_Record_Type := C_Data;
       ELSE  -- Trailer
          pgmloc := 2730;
          V_Max_Rec := V_Rec_Num_Tab(C_Trailer);
          V_Record_Type := C_Trailer;
       END IF;
       pgmloc := 2740;
       G_Record_Type := V_Record_Type;
--
--     Process record within the record type
--
       V_Out_Rec        := NULL;
       V_Prev_ID        := NULL;
       V_Prev_Component := NULL;
       V_Prev_Exchange  := NULL;
       V_Prev_Ex_Date   := NULL;
       V_Div_Type       := NULL;
       V_Prev_Div_Type  := NULL;
       V_Div_Note       := NULL;
       V_Prev_Div_Note  := NULL;
       V_Status         := NULL;
       v_cash_div_iso   := NULL;    -- Mourad OASIS_6270 19Jan2016
--
       V_Component_Bef  := NULL;
       V_ID_Bef         := NULL;
       For Rec_Num IN 1 .. V_Max_Rec
       LOOP  --{
          pgmloc := 2750;
          Manage_Utl_File.Get_Line(In_File
                                  ,V_In_Rec
                                  ,EOF
                                  ,V_Success
                                  ,V_Message);
          IF NOT V_Success THEN GOTO Get_Out; END IF;
          IF EOF
          THEN
             -- File end before end of format - this should not happen
             Manage_Messages.Get_Oasis_Message
                            ( C_OASIS_Message||'074'
                            , 'N'
                            , P_Message
                            );
             GOTO Get_Out;
          END IF;
          pgmloc := 2760;
          V_Inline := V_Inline + 1;
--
          IF V_Record_Type = 'H'
          THEN
          -- HEADER
             pgmloc := 2770;
             V_Out_Rec := V_In_Rec;
             Write_DVS_Rec(Out_File
                          ,V_Out_Rec
                          ,C_Output_EOL
                          ,V_Status
                          ,V_Success
                          ,V_Message);
             IF NOT V_Success THEN GOTO Get_Out; END IF;
          ELSIF V_Record_Type = 'T'
          THEN
          -- Trailer
             pgmloc := 2780;
             V_Out_Rec := V_In_Rec;
             Write_DVS_Rec(Out_File
                          ,V_Out_Rec
                          ,C_Output_EOL
                          ,V_Status
                          ,V_Success
                          ,V_Message);
             IF NOT V_Success THEN GOTO Get_Out; END IF;
          ELSIF V_Record_Type = 'D'
          THEN
          -- { DATA
             V_New_Rec := FALSE;
             pgmloc := 2790;
             V_Component := SUBSTR(V_In_Rec
                                  ,INSTR(V_In_Rec,C_Output_Component)+
                                   LENGTH(C_Output_Component)
                                  ,1);
--           dbms_output.put_line('Component is ' || V_Component );
             pgmloc := 2800;
             G_Component := V_Component;
--
-- AP 05Dec2012 - Evaluate change of component against previous read line not previous output record
--              A change of component refreshes the field positions  
            IF V_Component <> NVL(V_Component_Bef,'.')
                THEN
                pgmloc := 2810;
                V_New_Rec := TRUE;
                pgmloc := 2820;
                Define_Fields(C_Fld_Id,Fld_Rec);
                pgmloc := 2830;
                Define_Fields(C_Fld_Ex_Date,Fld_Rec);
                pgmloc := 2840;
                Define_Fields(C_Fld_Exch,Fld_Rec);
                pgmloc := 2850;
                Define_Fields(C_Fld_Add_Del,Fld_Rec);
                pgmloc := 2860;
                Define_Fields(C_Fld_Div_Type,Fld_Rec);
                pgmloc := 2870;
                Define_Fields(C_Fld_Div_Note,Fld_Rec);
                pgmloc := 2875;                
                Define_Fields(C_Fld_currency,Fld_Rec); -- Mourad OASIS_6270 19Jan2016
             END IF;
            -- Mourad OASIS_6270 19Jan2016
              v_cash_div_iso := TRIM(SUBSTR(V_In_Rec,Fld_Rec(C_Fld_currency).Starting_Byte,
                                            Fld_Rec(C_Fld_currency).Field_Length)); 
                                            
             IF t_parm_values.COUNT > 0 THEN
                IF NOT t_parm_values.exists(v_cash_div_iso) THEN
                   GOTO Skip_Rec;
                END IF;
             END IF;      
            -- END Mourad OASIS_6270 19Jan2016
             
             V_Id := TRIM(SUBSTR(V_In_Rec,Fld_Rec(C_Fld_ID).Starting_Byte
                                   ,Fld_Rec(C_Fld_ID).Field_Length));
--           dbms_output.put_line('V_ID is ' || V_ID);
             pgmloc := 2880;
             V_Exchange := TRIM(SUBSTR(V_In_Rec
                                     ,Fld_Rec(C_Fld_Exch).Starting_Byte
                                     ,Fld_Rec(C_Fld_Exch).Field_Length));
--           dbms_output.put_line('V_Exchange is ' || V_Exchange);
             pgmloc := 2890;
             V_Ex_Date := TO_DATE(SUBSTR(V_In_Rec
                                       ,Fld_Rec(C_Fld_Ex_Date).Starting_Byte
                                       ,Fld_Rec(C_Fld_Ex_Date).Field_Length)
                                ,Fld_Rec(C_Fld_Ex_Date).Output_Format);
--           dbms_output.put_line('V_Ex_Date is ' || V_Ex_Date);
             pgmloc := 2900;
             V_Add_Del := TRIM(SUBSTR(V_In_Rec
                                     ,Fld_Rec(C_Fld_Add_Del).Starting_Byte
                                     ,Fld_Rec(C_Fld_Add_Del).Field_Length));
--           dbms_output.put_line('V_Add_Del is ' || V_Add_Del);
             pgmloc := 2910;
-- Assign '__' for null codes
             V_Div_Type := NVL(TRIM(SUBSTR(V_In_Rec
                                     ,Fld_Rec(C_Fld_Div_Type).Starting_Byte
                                     ,Fld_Rec(C_Fld_Div_Type).Field_Length))
                               ,'__');
             pgmloc := 2920;
             V_Div_Note := NVL(TRIM(SUBSTR(V_In_Rec
                                     ,Fld_Rec(C_Fld_Div_Note).Starting_Byte
                                     ,Fld_Rec(C_Fld_Div_Note).Field_Length))
                                 ,'__');
--
             IF (   V_Component <> V_Prev_Component
                 OR V_ID        <> V_Prev_Id
                 OR V_Ex_Date   <> V_Prev_Ex_date
                 OR V_Div_Note  <> V_Prev_Div_Note
                 OR V_Div_Type  <> V_Prev_Div_Type)
             AND V_Out_Rec IS NOT NULL
             THEN
                pgmloc := 2930;
--              dbms_output.put_line('Write outrec 1 ' || V_Status);
--
--           -- Reset the global component to finish writing the current record
                G_Component := SUBSTR(V_Out_Rec
                                     ,INSTR(V_Out_Rec,C_Output_Component)+
                                      LENGTH(C_Output_Component)
                                     ,1);
                Write_DVS_Rec(Out_File
                             ,V_Out_Rec
                             ,C_Output_EOL
                             ,V_Status
                             ,V_Success
                             ,V_Message);
                IF NOT V_Success THEN GOTO Get_Out; END IF;
                pgmloc := 2940;
--           -- Reset component_Code
                G_Component := V_Component;
                V_Out_Rec := V_In_Rec;
                V_New_Rec := TRUE;
                V_Status  := V_Add_Del;
             END IF;
--        --
--        -- Get all info for this ID
--
-- APX 05Dec2012  - Compare Id against previous read line. 
--                A change of ID refreshes the sources list  
--             IF V_ID <> NVL(V_Prev_Id,'.NULL.')
             IF V_ID <> NVL(V_Id_Bef,'.NULL.')              
             THEN  -- { If a new sec
--           -- Initialization
                pgmloc := 2950;
--              dbms_output.put_line('Get src List ');
                V_Src_Num    := V_Init_Src;
                V_Fri_Ticker := NULL;
                V_Status     := V_Add_Del;
--           -- Get the corresponding client_univ and fri_Ticker
                pgmloc := 2960;
                OPEN Get_Client_Sec(V_ID);
                pgmloc := 2970;
                FETCH Get_Client_Sec INTO V_Fri_Client,V_Fri_Ticker;
                IF Get_Client_Sec%NOTFOUND
                THEN
--           --    This should not happen
                   pgmloc := 2980;
                   CLOSE Get_Client_Sec;
                   GOTO Skip_Rec;
                END IF;
                pgmloc := 2990;
                CLOSE Get_Client_Sec;
--              dbms_output.put_line('Client.fri_ticker:' || V_Fri_Client ||
--                                               ' ' || V_Fri_Ticker);
--              dbms_output.put_line('Get src List ');
                pgmloc := 3000;
                OPEN Get_Src_List;
                LOOP
                   pgmloc := 3010;
                   FETCH Get_Src_List INTO V_Src,V_Priority;
                   EXIT WHEN Get_Src_List%NOTFOUND;
                   pgmloc := 3020;
                   V_Src_Num(V_Src) := V_Priority;
--                 dbms_output.put_line('ID src priority:' || V_Src || ' ' ||
--                                       V_Priority);
                END LOOP;
                pgmloc := 3030;
                CLOSE Get_Src_List;
--           -- Skip if no source is defined for this security
                IF V_Src_Num.COUNT = 0
                THEN
                   V_Out_Rec := NULL;
                   V_Status  := NULL;
--                 dbms_output.put_line('No src priority, record skipped');
                   GOTO Skip_Rec;
                ELSE
                   V_New_Rec := TRUE;
                   V_Out_Rec := V_In_Rec;
                   V_Status  := V_Add_Del;
                END IF;
             END IF;  -- } Change of sec
--
--          --  Match source
--
             IF NOT V_Src_Num.Exists(V_Exchange)
             THEN
                IF V_Out_Rec=V_In_Rec
                THEN
                   pgmloc := 3040;
                   V_Out_Rec := NULL;
                END IF;
--              dbms_output.put_line('Exchange skipped ' || V_Exchange);
                GOTO Skip_Rec;
             ELSIF V_Src_Num.Exists(V_Exchange)
             THEN
-- AP 05DEC2012 - In case previous lines did not have valid sources
                IF V_Out_Rec IS NULL THEN 
                   V_New_Rec := TRUE;
                   V_Out_Rec := V_In_Rec;
                   V_Status  := V_Add_Del;
                END IF;                          
                IF V_ID = V_Prev_ID AND
                   V_Div_Type = V_Prev_Div_Type AND
                   V_Div_Note = V_Prev_Div_Note
                THEN
                   IF V_Src_Num(V_Exchange) < V_Src_Num(V_Prev_Exchange) OR
                      (V_Exchange=V_Prev_Exchange AND V_Add_Del = 'A')
                   THEN
                      pgmloc := 3050;
                      IF V_Exchange = V_Prev_Exchange THEN
                          IF V_Status = 'D' THEN
--                              Finish writing the current record when there is an Add after Cancellation
                                pgmloc := 2940;
                                Write_DVS_Rec(Out_File
                                             ,V_Out_Rec
                                             ,C_Output_EOL
                                             ,V_Status
                                             ,V_Success
                                             ,V_Message);
                                IF NOT V_Success THEN GOTO Get_Out; END IF;
                                V_Status  := V_Add_Del; -- Reset status
                          END IF;
                          V_Status := V_Status || V_Add_Del;
                      END IF;
                      V_Out_Rec := V_In_Rec;
                      V_New_Rec := TRUE;
--                    dbms_output.put_line('Exchange kept ' || V_Exchange);
                   END IF;
                END IF;
             END IF;
--
             IF V_New_Rec
             THEN
                pgmloc := 3060;
                OPEN Get_Div_Info;
                pgmloc := 3070;
--          -- SSI - Seperate fetch of frequency from cursor
                FETCH Get_Div_Info INTO V_Seq,V_App_Order;
--           -- Skip if no dividend info is found this security
                IF Get_Div_Info%NOTFOUND
                THEN
                   CLOSE Get_Div_Info;
                   IF V_In_Rec = V_Out_Rec
                   THEN
                      V_Out_Rec := NULL;
                   END IF;
                   GOTO Skip_Rec;
                END IF;
                pgmloc := 3080;
                CLOSE Get_Div_Info;
--
                pgmloc := 3090;
                OPEN Cur_Get_Fld(C_Output_Seq);
                pgmloc := 3100;
                FETCH Cur_Get_Fld INTO Out_Fld_Rec;
                pgmloc := 3110;
                IF Cur_Get_Fld%FOUND
                THEN
                   pgmloc := 3120;
                   V_Out_Rec := SUBSTR(V_Out_Rec,1,Out_Fld_Rec.Starting_Byte-1)
                                || LPAD(TO_CHAR(V_Seq),Out_Fld_Rec.Field_Length,'0')
                                ||  SUBSTR(V_Out_Rec,Out_Fld_Rec.Starting_Byte+
                                                     Out_Fld_Rec.Field_Length);
                END IF;
                pgmloc := 3130;
                CLOSE Cur_Get_Fld;
--
                pgmloc := 3140;
                OPEN Get_Clt_Freq;
                pgmloc := 3150;
                FETCH Get_Clt_Freq INTO V_Freq;
                IF Get_Clt_Freq%NOTFOUND
                THEN
                   V_Freq := C_Def_Freq;
                END IF;
                pgmloc := 3160;
                CLOSE Get_Clt_Freq;
--
                pgmloc := 3170;
                OPEN Cur_Get_Fld(C_Output_Freq);
                pgmloc := 3180;
                FETCH Cur_Get_Fld INTO Out_Fld_Rec;
                pgmloc := 3190;
                IF Cur_Get_Fld%FOUND
                THEN
                   pgmloc := 3200;
                   V_Out_Rec := SUBSTR(V_Out_Rec,1,Out_Fld_Rec.Starting_Byte-1)
                                || RPAD(V_Freq, Out_Fld_Rec.Field_Length)
                                ||  SUBSTR(V_Out_Rec,Out_Fld_Rec.Starting_Byte+
                                                     Out_Fld_Rec.Field_Length);
                END IF;
                pgmloc := 3210;
                CLOSE Cur_Get_Fld;
             END IF;
--
--           Write if it is the last record
             pgmloc := 3220;
                pgmloc := 3240;
                V_Prev_Component := V_Component;
                V_Prev_ID := V_ID;
                V_Prev_Exchange := V_Exchange;
                V_Prev_Ex_Date  := V_Ex_Date;
                V_Prev_Div_Type := V_Div_Type;
                V_Prev_Div_Note := V_Div_Note;
<<Skip_Rec>>
-- AP 05Dec2012 - Saves component and Id anterior line
                V_Component_Bef := V_Component;
                V_ID_Bef := V_ID;
--           Write if it is the last record
             IF Rec_Num=V_Max_Rec AND
                V_Out_Rec IS NOT NULL
             THEN
                pgmloc := 3230;
--              dbms_output.put_line('Write outrec 2 ' || V_Status);
                Write_DVS_Rec(Out_File
                             ,V_Out_Rec
                             ,C_Output_EOL
                             ,V_Status
                             ,V_Success
                             ,V_Message);
                IF NOT V_Success THEN GOTO Get_Out; END IF;
             END IF;
             pgmloc := 3250;
          END IF;  -- }  -- DATA record
       END LOOP;  --} Lines within each record type
       EXIT WHEN V_Record_Type=C_Trailer;
    END LOOP;  --} Each record
--
    pgmloc := 3260;
    V_Success := TRUE;
<<Get_Out>>
    pgmloc := 3270;
    P_Success := V_Success;
    P_Message := V_Message;
    pgmloc := 3280;
    Manage_Utl_File.Close_File(In_File
                              ,V_Success
                              ,V_Message);
    pgmloc := 3290;
    Manage_Utl_File.Close_File(Out_File
                              ,V_Success
                              ,V_Message);
   EXCEPTION
   WHEN OTHERS THEN
      Manage_Messages.Get_Oasis_Message
                            ( C_GEN_Message||'070'
                            , 'N'
                            , pgmloc || ' ' ||
                              G_Routine || ' ' ||
                              SUBSTR( SQLERRM( SQLCODE ), 1, 400 )
                            , P_Message
                            );
END Post_DVS_Extract;
--
--
-----------------------------------------------------------------------
--
-- Load_Acc_Except is to load the accounting exception for CGI.  Those
-- exception are to be invoiced as TSX bonds instead of as Statpro or
-- other suppliers, thus they are also known as TSX Bonds.
--
-----------------------------------------------------------------------
--
PROCEDURE Load_Acc_Except(
          P_Program_Id    IN     Ops_Scripts.Program_Id%TYPE
        , P_In_File_Name  IN     VARCHAR2
        , P_Commit_Freq   IN     INTEGER
        , P_Success          OUT BOOLEAN
        , P_Message          OUT VARCHAR2
        ) IS --{
--
C_No_Data           CONSTANT VARCHAR2(32) := 'No TSX Bonds Priced';
--
Pricing_Msgs_Rec             Pricing_Msgs%ROWTYPE;
PRP                          Product_Running_Parms%ROWTYPE;
Cntl_Rec                     R_Parameters;
V_Message                    VARCHAR2(256);
Data_Rec                     VARCHAR2(1024);
End_Of_File                  BOOLEAN;
--
R_ACE                        Acc_CGI_Except%ROWTYPE;
--
BEGIN --{
  --
  Pgmloc := 3300;
  P_Success := TRUE;
  --
  -------------------------------
  --  create the pricing message,
  --  open the log
  --  copy the input file
  --  open the copied file
  --  open the output file
  -------------------------------
  --
  Pgmloc := 3310;
  Cntl_Rec.In_File_Name  := P_In_File_Name;
  Cntl_Rec.Out_File_Name := NULL;
  Cntl_Rec.In_File_Open  := FALSE;
  Cntl_Rec.Out_File_Open := FALSE;
  Cntl_Rec.Log_File_Open := FALSE;
  Pgmloc := 3320;
  Process_Beginning( P_Program_Id
                   , Constants.Get_Site_Date
                   , P_Commit_Freq
                   , PRP
                   , Cntl_Rec
                   , Pricing_Msgs_Rec
                   , P_Success
                   , P_Message
                   );
  IF NOT P_Success THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 3330;
  COMMIT;
  --
  ----------------------------------------------------------------------------
  -- We have committed the messages up to now
  -- From here on this is everything or nothing
  ----------------------------------------------------------------------------
  --
  Pgmloc := 3340;
  DELETE Acc_CGI_Except
   WHERE Fri_Client     = PRP.Fri_Client
     AND Valuation_Date = Pricing_Msgs_Rec.Date_of_Prices;
  Pricing_Msgs_Rec.Value_03 := SQL%ROWCOUNT;
  --
  R_ACE.Fri_Client        := PRP.Fri_Client;
  R_ACE.Valuation_Date    := Pricing_Msgs_Rec.Date_Of_Prices;
  R_ACE.Valuation_Date_BC := NULL;
  R_ACE.Last_User         := Constants.Get_User_Id;
  --
  END_of_File := FALSE;
  WHILE NOT End_of_File
  LOOP --{
    pgmloc := 3350;
    Load_Supplier_Rules.Get_Line( Cntl_Rec.In_File
                                , Pricing_Msgs_Rec
                                , Data_Rec
                                , P_Success
                                , End_Of_File
                                );
    IF NOT P_Success   THEN GOTO GET_OUT;  END IF;
    IF     End_Of_File THEN GOTO END_LOOP; END IF;
    Pricing_Msgs_Rec.Value_01 := Pricing_Msgs_Rec.Value_01 + 1;
    IF UPPER( SUBSTR( Data_Rec, 1, LENGTH( C_No_Data ) ) )
       = UPPER( C_No_Data ) THEN GOTO END_LOOP; END IF;
    Pgmloc := 3360;
    R_ACE.Client_Security_Id := SUBSTR( Data_Rec, 1, 12 );
    R_ACE.Last_Stamp         := SYSDATE;
    BEGIN --{
      Pgmloc := 3370;
      INSERT INTO Acc_CGI_Except VALUES R_ACE;
      Pricing_Msgs_Rec.Value_02 := Pricing_Msgs_Rec.Value_02 + 1;
    EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
      NULL;
    END; --}
<< End_Loop >>
    NULL;
  END LOOP; --} while not end_of_file
  --
  pgmloc := 3380;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'012'
                                   , 'N'
                                   , TO_CHAR( SYSDATE
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  pgmloc := 3390;
  load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 3400;
  Load_Supplier_Rules.File_Close_All( Pricing_Msgs_Rec
                                    , P_Success
                                    );
  Cntl_Rec.Log_File_Open := FALSE;
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 3410;
  UPDATE Break_Points
     SET Status     = 'DONE'
       , Nb_Records = Pricing_Msgs_Rec.Value_01
       , Last_User  = Constants.Get_User_Id
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  pgmloc := 3420;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'060'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  pgmloc := 3430;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'062'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  GOTO Upd_Prc_Msg;
  --
<<GET_OUT>>
  pgmloc := 3440;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'070'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'072'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
--
<<Upd_Prc_Msg>>
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  --
  pgmloc := 3450;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 3460;
  commit;
--
EXCEPTION
WHEN OTHERS THEN
  P_Message := SUBSTR( 'At Pgmloc : '
                     || TO_CHAR( Pgmloc )
                     || ' '
                     || SUBSTR( SQLERRM( SQLCODE ), 1, 255 )
                     , 1
                     , 255
                     );
  IF Pricing_Msgs_Rec.Err_Text IS NULL
  THEN
    Pricing_Msgs_Rec.Err_Text := P_Message;
  END IF;
  Pgmloc := 3470;
  ROLLBACK;
  Pgmloc := 3480;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 3490;
  commit;
  IF Cntl_Rec.Log_File_Open
  THEN
    pgmloc := 3500;
    load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                  , Pricing_Msgs_Rec.Msg_Text
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
  END IF;
  --
  Load_Supplier_Rules.File_Close_All( Pricing_msgs_Rec
                                    , P_Success
                                    );
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  P_Success := FALSE;
  --
END Load_Acc_Except; --}}
--------------------------------------------------------------------------------
  --
  -- Procedure written by Araisbel 16 April 2008
  --
  PROCEDURE Maintain_TSX
  (P_Program_Id     IN  OPS_Scripts.Program_id%TYPE,
   P_Starting_Date  IN  DATE,
   P_Ending_Date    IN  DATE,
   P_Log_File_Name  IN  VARCHAR2,
   P_SUCCESS        OUT BOOLEAN,
   P_MESSAGE        OUT VARCHAR2) IS
   --
   v_fri_client     acc_cgi_info.fri_client%TYPE;
   --
   -- Counters
   --
   v_rows_upd        NUMBER := 0;
   v_total_rows_upd  NUMBER := 0;
   v_rows_inst       NUMBER := 0;
   v_rows_inst_loop  NUMBER := 0;
   v_total_rows_inst NUMBER := 0;
   v_count_cgi_info  NUMBER := 0;
   -- Constants
   c_product         VARCHAR(3) := 'SVS';
   c_source_code     VARCHAR(4) := '-SCO';
   --
   V_File_Handle     UTL_FILE.FILE_TYPE;
   V_Log_Success     BOOLEAN;
   V_Log_Message     VARCHAR2(256);
   Update_Break_Pts  BOOLEAN     := TRUE;
   V_sysdate         DATE        := SYSDATE;
   Pricing_Msgs_Rec  Pricing_Msgs%ROWTYPE;
   --
   Invalid_File      EXCEPTION;
   --
   -- Get_Acc_CGI_Except
   --
   CURSOR Get_Acc_CGI_Except IS 
   SELECT fri_client , client_security_id,valuation_date
   FROM   Acc_CGI_Except  
   WHERE  valuation_date BETWEEN P_Starting_Date AND P_Ending_Date
   ORDER BY 1,2;
   --
   -- Get_Products
   --
   CURSOR Get_products IS
   SELECT P.fri_client, P.product_code, P.sub_product_code
   FROM  product_running_parms P
        ,cgi_clients C
   WHERE P.fri_client = C.fri_client
   AND   grouping_fri_client =  v_fri_client
   AND   P.active_flag = 'Y'
   AND   P.product_code  = c_product
   ORDER BY 1,2,3;
   --
  BEGIN
    Pgmloc := 3510;
    P_SUCCESS        := FALSE;
    --
    Pgmloc := 3520;
    Global_File_Updates.Initialize_Pricing_Msgs
                         (P_Program_id,
                          V_SYSDATE,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    COMMIT;
    Pgmloc := 3530;
    Pricing_Msgs_Rec.Msg_Text := 'Procedure Maintain_TSX.';
    Pricing_Msgs_Rec.Msg_Text_2 := 'From '  || 
                                  TO_CHAR(P_Starting_Date,'DD-Mon-YYYY') ||
                                  ' To '   || 
                                  TO_CHAR(P_Ending_Date,'DD-MON-YYYY');
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
    Pricing_Msgs_Rec.Value_01 := 0;
    Pricing_Msgs_Rec.Value_02 := 0;
    Pricing_Msgs_Rec.Value_03 := '';
    Pricing_Msgs_Rec.Value_04 := '';
    Pricing_Msgs_Rec.Value_05 := '';
    Pricing_Msgs_Rec.Value_06 := '';
    Pricing_Msgs_Rec.Value_07 := '';
    Pricing_Msgs_Rec.Value_08 := '';
    Pricing_Msgs_Rec.Value_09 := '';
    Pricing_Msgs_Rec.Value_10 := '';
    Pricing_Msgs_Rec.Value_11 := '';
    Pricing_Msgs_Rec.Value_12 := '';
    --
    Pgmloc := 3540;
    Global_File_Updates.Update_Pricing_Msgs
                         (P_Program_Id,
                          V_Sysdate,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    COMMIT;
    --
    --  OPEN log file 
    --  
    Pgmloc := 3550;
    Manage_Utl_File.Open_File (P_Log_File_Name, 'W', V_File_Handle,
                               V_Log_Success, V_Log_Message);
    --
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3560;
    Manage_Utl_File.Write_File(V_File_Handle,
                               'Starting Date is ' ||
                               to_char(sysdate, 'DD-Mon-YYYY HH24:MI:SS'),
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3570;
    Manage_Utl_File.Write_File(V_File_Handle,
                               Pricing_Msgs_Rec.Msg_Text,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3580;
    Manage_Utl_File.Write_File(V_File_Handle,
                               Pricing_Msgs_Rec.Msg_Text_2,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    -- 
    Pgmloc := 3590;
    --
    -- Opening cursor to process all Acc_CGI_Except
    --
    FOR Reg1 IN Get_Acc_CGI_Except LOOP
       --
       v_fri_client     := reg1.fri_client;
       v_count_cgi_info := 0;
       v_rows_inst      := 0;
       v_rows_inst_loop := 0;
       v_rows_upd       := 0;
       --
       Pgmloc := 3600;
       --
       -- verifying if exists records in acc_cgi_info
       --
       SELECT count(*)
       INTO  v_count_cgi_info
       FROM  acc_cgi_info A
            ,cgi_clients  C
       WHERE A.fri_client         = C.fri_client
       AND   grouping_fri_client  = Reg1.fri_client
       AND   A.client_security_id = Reg1.client_security_id
       AND   A.valuation_date     = reg1.valuation_date;
       --
       Pgmloc := 3610;
       --
       -- If exists records in table acc_cgi_info set field tsx_prices_flag to Y
       --
       IF v_count_cgi_info != 0 THEN
          UPDATE acc_cgi_info A
          SET    A.tsx_prices_flag = 'Y',
                 A.last_user       = Constants.get_user_id,
                 A.last_stamp      = sysdate
          WHERE  A.fri_client         IN (SELECT C.fri_client
                                          FROM cgi_clients C
                                          WHERE grouping_fri_client = Reg1.fri_client)
          AND    A.client_security_id        = Reg1.client_security_id
          AND    A.valuation_date            = reg1.valuation_date
          AND    nvl(A.TSX_Prices_Flag,'N') != 'Y';
          --
          v_rows_upd := SQL%rowcount; 
          v_total_rows_upd := v_total_rows_upd + v_rows_upd; 
          --
          -- If doesn't exist records in table acc_cgi_info insert new rows
          --
      ELSE
          --
          Pgmloc := 3620;
          --
          -- opening cursor Get_Products
          -- 
          FOR Reg2 IN Get_Products LOOP
              --
              INSERT INTO acc_cgi_info (fri_client, product_code, sub_product_code
                                       ,cgi_source_code,valuation_date
                                       ,client_security_id, tsx_prices_flag
                                       ,last_user, last_stamp)
              VALUES(Reg2.fri_client,reg2.product_code, reg2.sub_product_code
                    ,c_source_code,reg1.valuation_date,reg1.client_security_id
                    ,'Y',Constants.Get_User_Id, SYSDATE);
              --
              v_rows_inst       := SQL%rowcount;
              v_rows_inst_loop  := V_rows_inst_loop  + v_rows_inst;
              v_total_rows_inst := v_total_rows_inst + v_rows_inst;
              --
          END LOOP; -- Get_Products
          --
      END IF;
      --
      Pgmloc := 3630;
      --
      Manage_Utl_File.Write_File(V_File_Handle,
                               'Master_Fri_Client: ' ||Reg1.fri_client||
                               '  Security ID: ' ||rpad(Reg1.client_security_id,15)||
                               '  # Rows Updated: ' ||v_rows_upd||
                               '  # Rows Inserted:'||v_rows_inst_loop,
                               V_Log_Success, V_Log_Message);
      IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
      --
    END LOOP;  -- Get_Acc_CGI_Except
    --
    Pgmloc := 3640;
    --
    Manage_Utl_File.Write_File(V_File_Handle,
                               'Total Rows Updated: ' ||v_total_rows_upd,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3650;
    --
    Manage_Utl_File.Write_File(V_File_Handle,
                               'Total Rows Inserted: ' ||v_total_rows_inst,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3660;
    --
    Manage_Utl_File.Write_File(V_File_Handle,
                               'Ending Date is ' ||
                               to_char(sysdate, 'DD-Mon-YYYY HH24:MI:SS'),
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3670;
    --
    Manage_Utl_File.Close_File(V_File_Handle, V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3680;
    -- dbms_output.put_line( 'pgmloc :'||pgmloc);
    Pricing_Msgs_Rec.Successfull_Flag := 'Y';
    Pricing_Msgs_Rec.Remarks          := 'Process finished successfully.';
    Pricing_Msgs_Rec.Value_01 := v_total_rows_upd;
    Pricing_Msgs_Rec.Value_02 := v_total_rows_inst;
    --Pricing_Msgs_Rec.Value_03 := ''; 
    --Pricing_Msgs_Rec.Value_04 := ''; 
    --Pricing_Msgs_Rec.Value_05 := ''; 
    --Pricing_Msgs_Rec.Value_06 := ''; 
    --Pricing_Msgs_Rec.Value_07 := ''; 
    --Pricing_Msgs_Rec.Value_08 := '';
    --Pricing_Msgs_Rec.Value_09 := '';
    --Pricing_Msgs_Rec.Value_10 := '';
    --Pricing_Msgs_Rec.Value_11 := '';
    --Pricing_Msgs_Rec.Value_12 := '';
    --
    Global_File_Updates.Update_Pricing_Msgs
                         (P_Program_Id,
                          V_Sysdate,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    Pgmloc := 3690;
    P_SUCCESS := TRUE;
    P_Message := '';
    --
    -- Update Product_Running_Parms
    --    set Last_Run_Date = Sysdate
    --  Where Program_ID = P_Program_Id;
    --
    Commit;
    --
EXCEPTION
    WHEN Invalid_File THEN
       P_Message := V_Log_Message;
       Pricing_Msgs_Rec.Msg_Text_2 := 'Error occured in process ' ||
                                        ' @pgmloc=' ||
                                        TO_CHAR(pgmloc);
       Pricing_Msgs_Rec.Err_Text := P_Message;
       Global_File_Updates.Update_Pricing_Msgs
                               (P_Program_Id,
                                v_Sysdate,
                                Update_Break_Pts,
                                Pricing_Msgs_Rec);
       COMMIT;
       RAISE;
    --
    WHEN OTHERS THEN
       Pricing_Msgs_Rec.Msg_Text_2 := 'Error occured in process ' ||
                                     ' @pgmloc=' ||
                                     TO_CHAR(pgmloc);
       Pricing_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
       Global_File_Updates.Update_Pricing_Msgs
                               (P_Program_Id,
                                v_Sysdate,
                                Update_Break_Pts,
                                Pricing_Msgs_Rec);
       COMMIT;
       RAISE;
  END Maintain_TSX;
-------------------------------------------------------------------------------
  --
  -- Procedure writen by Araisbel April 2008
  --
  PROCEDURE Accounting_Prep
  (P_Program_Id     IN  OPS_Scripts.Program_id%TYPE,
   P_Starting_Date  IN  DATE,
   P_Ending_Date    IN  DATE,
   P_Log_File_Name  IN  VARCHAR2,
   P_SUCCESS        OUT BOOLEAN,
   P_MESSAGE        OUT VARCHAR2) IS
   --{
   v_fri_client     CGI_Clients.fri_client%TYPE;
   V_Master_Fri_Client     CGI_Clients.Fri_Client%TYPE;
   v_client_code    Clients.code%TYPE;
   v_source_code    CGI_Sources.code%TYPE;
   v_product_code   Acc_CGI_stats.product_code%TYPE;
   v_gross_total    Acc_CGI_Stats.gross_total%TYPE;
   v_net_total      Acc_CGI_Stats.net_total%TYPE;
   -- 
   v_rows_read      NUMBER := 0;
   v_rows_inserted  NUMBER := 0;
   v_Gross_Req      NUMBER;
   v_Net_Req        NUMBER;
   v_gross_priced   NUMBER;
   v_net_priced     NUMBER;
   v_gross_not_prcd NUMBER;
   v_net_not_prcd   NUMBER;
   v_gross_mkt      NUMBER;
   v_net_mkt        NUMBER;
   v_gross_other    NUMBER;
   v_net_other      NUMBER;
   v_gross_client   NUMBER;
   v_net_client     NUMBER;
   -- Constants
   c_all_req        VARCHAR2(4) := '-ALL'; -- Total number of request
   c_not_prc        VARCHAR2(4) := '-NPR'; -- Total number not priced
   c_prc            VARCHAR2(4) := '-PRC'; -- Total number priced
   c_SCO            VARCHAR2(4) := '-SCO'; -- TSX (Scotia) Bonds
   --
   V_File_Handle    UTL_FILE.FILE_TYPE;
   V_Log_Success    BOOLEAN;
   V_Log_Message    VARCHAR2(256);
   Update_Break_Pts  BOOLEAN     := FALSE;
   V_sysdate         DATE        := SYSDATE;
   Pricing_Msgs_Rec  Pricing_Msgs%ROWTYPE;
   --
   Invalid_File     EXCEPTION;
   Different_Amount EXCEPTION;
   --
   V_Dir_Name      VARCHAR2(60) := Constants.Get_dir_name;
   --
   W_Date           DATE;
   W_Gross_Total    INTEGER;
   W_Net_Total      INTEGER;
   W_Dates_Total    INTEGER;
   W_Dummy          VARCHAR2(2);
   --
   -- Get_Products
   --
   CURSOR Get_Products IS
   SELECT DISTINCT PRP.product_code
     FROM Products              P
        , Product_Running_Parms PRP
        , Clients               C
        , Clients               MC
    WHERE MC.Code             = 'CGIS'
      AND C.Master_Fri_Client = MC.Fri_Client
      AND PRP.Fri_Client      = C.Fri_Client
      AND PRP.Active_Flag     = 'Y'
      AND P.Code              = PRP.Product_Code   
--    The 3 only functional products
      AND P.Code IN ('SVS','DVS','SAS')
      AND P.Maint_Flag        = 'N'
   ORDER BY 1;
   --
   -- Get_CGI_Clients
   --
   CURSOR Get_CGI_Clients
        ( I_Product_Code   Products.Code%TYPE
        ) IS 
   SELECT DISTINCT M.Fri_Client Master_Fri_Client
        , M.Code
   FROM   Clients               M
        , CGI_Clients           G
        , Clients               C
        , Product_Running_Parms PRP
   WHERE  M.Fri_Client          = G.Master_Fri_Client
   AND    G.fri_client          = C.fri_client
   AND    G.active_flag         = 'Y'
   AND    PRP.Fri_Client        = C.Fri_Client
   AND    PRP.Product_Code      = I_Product_Code
   ORDER BY 1;
   --
   --
   -- Get_CGI_Clients
   --
   CURSOR Get_Client_Per_Product
        ( I_Fri_Client        Clients.Fri_Client%TYPE
        , I_Product_Code      Products.Code%TYPE
        ) IS 
   SELECT DISTINCT C.Fri_Client
        , C.Code
   FROM   CGI_Clients           G
        , Clients               C
        , Product_Running_Parms PRP
   WHERE  G.Master_Fri_Client   = I_Fri_Client
   AND    G.Fri_Client          = C.fri_client
   AND    G.active_flag         = 'Y'
   AND    PRP.Fri_Client        = C.Fri_Client
   AND    PRP.Product_Code      = I_Product_Code
   ORDER BY 1;
   --
   -- Get_CGI_Sources
   --
   CURSOR Get_CGI_Sources IS
   SELECT NVL(master_cgi_source_code,code) master_source
   FROM   CGI_Sources
   WHERE  active_flag = 'Y'
   GROUP BY NVL(master_cgi_source_code,code)
   ORDER BY 1;
   --
   -- Get_All_Request
   --
   CURSOR Get_All_Request
        ( I_Date         DATE
        , I_Product_Code Products.Code%TYPE
        , I_Fri_Client   Clients.Fri_Client%TYPE
        ) IS
   SELECT COUNT(DECODE(cgi_source_code,c_sco,NULL,A.fri_client)) GROSS_TOTAL , 
          COUNT(A.fri_client) NET_TOTAL
   FROM ACC_CGI_Info A , 
        CGI_Clients  D 
   WHERE A.valuation_date      = I_Date
   AND   A.product_code        = I_Product_Code
   AND   A.fri_client          = D.fri_client
   AND   D.active_flag         = 'Y'
   AND   D.fri_client          = I_Fri_Client;
   --
   -- Get_Not_Priced
   --
   CURSOR Get_Not_Priced
        ( I_Date         DATE
        , I_Product_Code Products.Code%TYPE
        , I_Fri_Client   Clients.Fri_Client%TYPE
        ) IS
   SELECT  COUNT(DECODE(cgi_source_code,c_sco,NULL,A.fri_client)) GROSS_TOTAL, 
           COUNT(DECODE(NVL(tsx_prices_flag,'N'),'N',A.fri_client,NULL)) NET_TOTAL
   FROM ACC_CGI_Info A, 
        CGI_Clients  D
   WHERE A.valuation_date         = I_Date
   AND   A.product_code           = I_Product_Code
   AND   A.fri_client             = d.fri_client
   AND   D.active_flag            = 'Y'
   AND   A.Master_CGI_Source_Code = c_not_prc
   AND   D.fri_client             = I_Fri_Client;
   --
   -- Get_Priced
   --
   CURSOR Get_Priced_Gross
        ( I_Date         DATE
        , I_Product_Code Products.Code%TYPE
        , I_Fri_Client   Clients.Fri_Client%TYPE
        ) IS
   SELECT COUNT(A.fri_client) GROSS_TOTAL 
   FROM ACC_CGI_Info A, 
        CGI_Clients  D
   WHERE A.valuation_date          = I_Date
   AND   A.product_code            = I_Product_Code
   AND   A.fri_client              = D.fri_client
   AND   A.Master_CGI_Source_Code != c_not_prc
   AND   A.cgi_source_code        != c_sco
   AND   D.active_flag             = 'Y'
   AND   D.fri_client              = I_Fri_Client;
   --
   CURSOR Get_Priced_Net
        ( I_Date         DATE
        , I_Product_Code Products.Code%TYPE
        , I_Fri_Client   Clients.Fri_Client%TYPE
        ) IS
   SELECT COUNT(A.fri_client) NET_TOTAL
   FROM ACC_CGI_Info A, 
        CGI_Clients  D
   WHERE A.valuation_date          = I_Date
   AND   A.product_code            = I_Product_Code
   AND   A.fri_client              = D.fri_client
   AND   ( A.Master_CGI_Source_Code   != c_not_prc OR
           nvl(A.TSX_Prices_Flag, 'N') = 'Y' )
   AND   D.active_flag             = 'Y'
   AND   D.fri_client              = I_Fri_Client;
   --
   -- Get_Others_Sources
   --
   CURSOR Get_Other_Sources_Gross
        ( I_Date         DATE
        , I_Product_Code Products.Code%TYPE
        , I_Fri_Client   Clients.Fri_Client%TYPE
        , I_Source_Code  CGI_Sources.Code%TYPE
        ) IS
   SELECT COUNT(A.fri_client) GROSS_TOTAL
   FROM   ACC_CGI_Info A, 
          Clients      C,
          CGI_Clients  D,
          CGI_Sources  S
   WHERE A.valuation_date         = I_Date
   AND   A.product_code           = I_Product_Code
   AND   D.fri_client             = I_Fri_Client
   AND   nvl(S.master_CGI_source_code,S.Code) = I_source_code
   AND   A.Fri_Client             = D.fri_client
   AND   C.fri_client             = nvl(d.master_fri_client,D.fri_client)
   AND   D.active_flag            = 'Y'
   AND   A.Master_CGI_Source_Code = S.code
   AND   A.cgi_source_code       != c_sco
   AND   S.active_flag            = 'Y'
   AND   S.Pricing_Flag           = 'Y';
   --
   CURSOR Get_Other_Sources_Net
        ( I_Date         DATE
        , I_Product_Code Products.Code%TYPE
        , I_Fri_Client   Clients.Fri_Client%TYPE
        , I_Source_Code  CGI_Sources.Code%TYPE
        ) IS
   SELECT COUNT(A.fri_client) NET_TOTAL
   FROM   ACC_CGI_Info A, 
          Clients      C,
          CGI_Clients  D,
          CGI_Sources  S
   WHERE A.valuation_date         = I_Date
   AND   A.product_code           = I_Product_Code
   AND   D.fri_client             = I_Fri_Client
   AND   nvl(S.master_CGI_source_code,S.Code) = I_Source_Code
   AND   A.Fri_Client             = D.fri_client
   AND   C.fri_client             = nvl(d.master_fri_client,D.fri_client)
   AND   D.active_flag            = 'Y'
   AND   Decode(nvl(A.TSX_Prices_Flag,'N'), 'Y', C_SCO,
                 A.Master_CGI_Source_Code) = S.Code
   AND   S.active_flag            = 'Y'
   AND   S.Pricing_Flag           = 'Y';
   --
   CURSOR Is_Product_Billable_On_Date
        ( I_Fri_CLient     Clients.Fri_Client%TYPE
        , I_Product_Code   Products.Code%TYPE
        , I_Starting_Date  DATE
        ) IS
   SELECT 'Y'
     FROM ACC_Parms    P
    WHERE P.Fri_Client = I_Fri_Client
      AND P.Product_Code = I_Product_Code
      AND I_Starting_Date BETWEEN P.Starting_Date
                              AND NVL( P.Ending_Date, SYSDATE )
-- Not a Canadian Holiday
      AND Holiday.Is_It_Open( I_Starting_Date
                            , 'SOURCE'
                            , 'TSE'
                            , NULL
                            ) IS NULL
      AND P.Billable_Flag = 'Y';
   --
   CURSOR Get_Count_DVS_SAS
        ( I_Fri_CLient     Clients.Fri_Client%TYPE
        , I_Product_Code   Products.Code%TYPE
        , I_Valuation_Date DATE
        ) IS
   SELECT SUM( Total_Priced )              Total_Priced
        , COUNT( DISTINCT Valuation_Date ) Count_Date
     FROM Acc_Clt_Stats A
    WHERE A.Fri_Client     = I_Fri_Client
      AND A.Product_Code   = I_Product_Code
      AND A.valuation_date = I_Valuation_Date;
   --
  BEGIN --{
    Pgmloc := 3700;
    P_SUCCESS        := FALSE;
    v_SYSDATE        := SYSDATE;
    Update_Break_Pts := TRUE;
    --
    Pgmloc := 3710;
    Global_File_Updates.Initialize_Pricing_Msgs
                         (P_Program_id,
                          v_SYSDATE,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    COMMIT;
    Pgmloc := 3720;
    Pricing_Msgs_Rec.Msg_Text := 'Creating Acc CGI Stats Out.';
    Pricing_Msgs_Rec.Msg_Text_2 := 'From '  || 
                                  TO_CHAR(P_Starting_Date,'DD-Mon-YYYY') ||
                                  ' To '   || 
                                  TO_CHAR(P_Ending_Date,'DD-MON-YYYY');
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
    Pricing_Msgs_Rec.Value_01 := 0;
    Pricing_Msgs_Rec.Value_02 := 0;
    Pricing_Msgs_Rec.Value_03 := '';
    Pricing_Msgs_Rec.Value_04 := '';
    Pricing_Msgs_Rec.Value_05 := '';
    Pricing_Msgs_Rec.Value_06 := '';
    Pricing_Msgs_Rec.Value_07 := '';
    Pricing_Msgs_Rec.Value_08 := '';
    Pricing_Msgs_Rec.Value_09 := '';
    Pricing_Msgs_Rec.Value_10 := '';
    Pricing_Msgs_Rec.Value_11 := '';
    Pricing_Msgs_Rec.Value_12 := '';
    --
    Pgmloc := 3730;
    Global_File_Updates.Update_Pricing_Msgs
                         (P_Program_Id,
                          v_Sysdate,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    COMMIT;
    --
    --  OPEN log file 
    --  
    Pgmloc := 3740;
    Manage_Utl_File.Open_File (P_Log_File_Name, 'W', V_File_Handle,
                               V_Log_Success, V_Log_Message);
    --
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3750;
    Manage_Utl_File.Write_File(V_File_Handle,
                               'Starting Date is ' ||
                               to_char(sysdate, 'DD-Mon-YYYY HH24:MI:SS'),
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3760;
    Manage_Utl_File.Write_File(V_File_Handle,
                               Pricing_Msgs_Rec.Msg_Text,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 3770;
    Manage_Utl_File.Write_File(V_File_Handle,
                               Pricing_Msgs_Rec.Msg_Text_2,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    -- 
    Pgmloc := 3780;
    -- Proceed with a Clean UP.
    Delete acc_CGI_Stats_Out 
     where Starting_Date = P_Starting_Date
       and Ending_Date   = P_Ending_Date;
    Pricing_Msgs_Rec.Value_03 := SQL%ROWCOUNT;
    Pgmloc := 3790;
    Commit;
    --
    -- Opening cursor to process all products
    --
    Pgmloc := 3800;
    FOR rec_prd IN Get_Products LOOP --{
        --
        Pgmloc := 3810;
        v_product_code := rec_prd.product_code;
        --
        Pgmloc := 3820;
        --
        -- Opening cursor to process all active master clients
        --
        FOR Rec_client IN Get_CGI_Clients( V_Product_Code ) LOOP --{
           --
           Pgmloc := 3830;
           v_fri_client     := Rec_client.master_fri_client;
           v_client_code    := Rec_client.code;
           v_Gross_Req      := 0;
           v_Net_Req        := 0;
           v_gross_priced   := 0;
           v_net_priced     := 0;
           v_gross_not_prcd := 0;
           v_net_not_prcd   := 0;
           v_gross_mkt      := 0;
           v_net_mkt        := 0;
           v_gross_other    := 0;
           v_net_other      := 0;
           v_gross_client   := 0;
           v_net_client     := 0;
           v_gross_total    := 0;
           v_net_total      := 0;
           W_Gross_Total    := 0;
           W_Net_Total      := 0;
           W_Dates_Total    := 0;
           --
           Pgmloc := 3840;
           --
           Manage_Utl_File.Write_File(V_File_Handle,
                                   'Master_Client: ' ||v_client_code,
                                   V_Log_Success, V_Log_Message);
           IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
           --
           Pgmloc := 3850;
           --
           Manage_Utl_File.Write_File(V_File_Handle,
                                   '    Product_code: ' ||v_product_code,
                                   V_Log_Success, V_Log_Message);
           IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
           --
           IF V_Product_Code IN ( 'DVS', 'SAS' )
           THEN --{
              Pgmloc := 3860;
              Manage_Utl_File.Write_File(V_File_Handle,
                             'DVS/SAS - Master_Client: ' || v_client_code,
                             V_Log_Success, V_Log_Message);
              Pgmloc := 3870;
              FOR Clt IN Get_Client_Per_Product ( V_Fri_Client, V_Product_Code )
              LOOP --{
                 Pgmloc := 3880;
                 Manage_Utl_File.Write_File(V_File_Handle,
                                            'Client: ' || Clt.Code,
                                            V_Log_Success, V_Log_Message);
                 IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
                 W_Date := P_Starting_Date;
                 WHILE W_Date <= P_Ending_Date
                 LOOP --{
                    Pgmloc := 3890;
                    OPEN Is_Product_Billable_On_Date ( Clt.Fri_Client
                                                     , V_Product_Code
                                                     , W_Date );
                    Pgmloc := 3900;
                    W_Dummy := NULL;
                    FETCH Is_Product_Billable_On_Date
                     INTO W_Dummy;
                    Pgmloc := 3910;
                    CLOSE Is_Product_Billable_On_Date;
                    IF W_Dummy = 'Y'
                    THEN --{ Billable
                       V_Source_Code := c_all_req;
                       Pgmloc := 3920;
                       --
                       FOR V_Rec IN Get_Count_DVS_SAS ( Clt.Fri_Client
                                                      , V_Product_Code
                                                      , W_Date
                                                      )
                       LOOP --{
                          EXIT WHEN NVL( V_Rec.Total_Priced, 0 ) = 0
                                 OR NVL( V_Rec.Count_Date  , 0 ) = 0;
                          Pgmloc := 3930;
                          V_Gross_Total := V_Gross_Total + V_Rec.Total_Priced;
                          W_Dates_Total := W_Dates_Total + V_Rec.Count_Date;
                          --
                       END LOOP; --} For rec in get_count_dvs_sas
                    END IF; --}
                    W_Date := Holiday.Get_Next_Open_Date( W_Date
                                                        , 'SOURCE'
                                                        , 'TSE'
                                                        , NULL 
                                                        );
                 END LOOP; --} For rec in get_count_dvs_sas
              END LOOP; --} For Clt in get_client_per_product
              If W_Dates_Total > 0
              Then
                V_Gross_Total := V_Gross_Total / W_Dates_Total;
              END IF;
              V_Net_Total := V_Gross_Total;
              IF NVL( V_Net_Total, 0 ) = 0
              THEN --{
                 NULL;
              ELSE --}{
                 -- Inserting into table Acc_CGI_Stats_Out
                 --
                 Pgmloc := 3940;
                 INSERT
                   INTO acc_cgi_stats_out
                      ( fri_client
                      , starting_date,         ending_date
                      , product_code,          cgi_source_code
                      , gross_total,           net_total
                      , last_user,             last_stamp
                      )
                 VALUES
                      ( v_fri_client
                      , p_starting_date,       p_ending_date
                      , v_product_code,        v_source_code
                      , v_gross_total,         v_net_total
                      , Constants.Get_User_Id, SYSDATE
                      );
                 --
                 v_rows_inserted := v_rows_inserted + 1;   
              END IF; --} If <> 0;
           ELSE --}{ V_Product_Code in dvs, sas
              --
              Pgmloc := 3950;
              --
              -- Opening cursor to process all master source code
              --
              FOR Rec_src IN Get_CGI_Sources LOOP --{
                 --
                 v_source_code := Rec_src.master_source;
                 v_gross_total  := 0;
                 v_net_total    := 0;
                 Pgmloc := 3960;
                 FOR Clt
                  IN Get_Client_Per_Product ( V_Fri_Client, V_Product_Code )
                 LOOP --{
                    Pgmloc := 3970;
                    Manage_Utl_File.Write_File(V_File_Handle,
                                               'Client: ' || Clt.Code,
                                               V_Log_Success, V_Log_Message);
                    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
                    W_Date := P_Starting_Date;
                    WHILE W_Date <= P_Ending_Date
                    LOOP --{
                       Pgmloc := 3980;
                       OPEN Is_Product_Billable_On_Date ( Clt.Fri_Client
                                                        , V_Product_Code
                                                        , W_Date );
                       Pgmloc := 3990;
                       W_Dummy := NULL;
                       FETCH Is_Product_Billable_On_Date
                        INTO W_Dummy;
                       Pgmloc := 4000;
                       CLOSE Is_Product_Billable_On_Date;
                       IF W_Dummy = 'Y'
                       THEN --{ Billable
                          --
                          Pgmloc := 4010;
                          --
                          -- Processing total number of request
                          -- if CGI_Source_Code = '-ALL'
                          --
                          IF Rec_src.master_source = c_all_req
                          THEN --{ Total Number of Requests
                             --
                             Pgmloc := 4020;
                             OPEN  Get_All_Request ( W_Date
                                                   , V_Product_Code
                                                   , Clt.Fri_Client );
                             Pgmloc := 4030;
                             FETCH Get_All_Request
                              INTO W_gross_total, W_net_total;
                             v_rows_read    := v_rows_read + 1;
                             Pgmloc := 4040;
                             CLOSE Get_All_Request;
                             V_Gross_Total  := V_Gross_Total + W_Gross_Total;
                             V_Net_Total    := V_Net_Total + W_Net_Total;
                             v_Gross_Req    := v_Gross_Req + v_gross_total;
                             v_Net_Req      := v_Net_Req   + v_net_total;
                             --
                             Pgmloc := 4050;
                             --
                             -- Processing total number not priced
                             -- if CGI_Source_Code = '-NPR'
                             --
                          ELSIF Rec_src.master_source = c_not_prc
                          THEN --}{ Total Number Not Priced
                             --
                             Pgmloc := 4060;
                             OPEN  Get_Not_Priced( W_Date
                                                 , V_Product_Code
                                                 , Clt.Fri_Client );
                             Pgmloc := 4070;
                             FETCH Get_Not_Priced
                              INTO W_gross_total, W_net_total;
                             v_rows_read    := v_rows_read + 1;
                             Pgmloc := 4080;
                             CLOSE Get_Not_Priced;
                             --
                             V_Gross_Total  := V_Gross_Total + W_Gross_Total;
                             V_Net_Total    := V_Net_Total + W_Net_Total;
                             v_gross_not_prcd :=  v_gross_not_prcd + v_gross_total ;
                             v_net_not_prcd   :=  v_net_not_prcd   + v_net_total;        
                             --
                             Pgmloc := 4090;
                             --
                             -- Processing total number priced
                             -- if CGI_Source_Code = '-PRC'
                             --
                          ELSIF Rec_src.master_source = c_prc
                          THEN --}{ Total Number Priced 
                             --
                             Pgmloc := 4100;
                             OPEN  Get_Priced_Gross( W_Date
                                                   , V_Product_Code
                                                   , Clt.Fri_Client );
                             Pgmloc := 4110;
                             FETCH Get_Priced_Gross INTO W_gross_total;
                             Pgmloc := 4120;
                             CLOSE Get_Priced_Gross;
                             Pgmloc := 4130;
                             OPEN  Get_Priced_Net( W_Date
                                                 , V_Product_Code
                                                 , Clt.Fri_Client );
                             Pgmloc := 4140;
                             FETCH Get_Priced_Net INTO W_net_total;
                             Pgmloc := 4150;
                             CLOSE Get_Priced_Net;
                             v_rows_read    := v_rows_read + 1;
                             --
                             V_Gross_Total  := V_Gross_Total + W_Gross_Total;
                             V_Net_Total    := V_Net_Total + W_Net_Total;
                             v_gross_priced :=  v_gross_priced + v_gross_total ;
                             v_net_priced   :=  v_net_priced   + v_net_total;
                             --
                             Pgmloc := 4160;
                             --
                             -- Processing all other sources 
                             --
                          ELSE --}{ All other CGI Sources
                             --
                             Pgmloc := 4170;
                             OPEN  Get_Other_Sources_gross( W_Date
                                                          , V_Product_Code
                                                          , Clt.Fri_Client
                                                          , V_Source_Code );
                             Pgmloc := 4180;
                             FETCH Get_Other_Sources_gross INTO W_gross_total;
                             Pgmloc := 4190;
                             CLOSE Get_Other_Sources_gross;
                             Pgmloc := 4200;
                             OPEN  Get_Other_Sources_net( W_Date
                                                        , V_Product_Code
                                                        , Clt.Fri_Client
                                                        , V_Source_Code );
                             Pgmloc := 4210;
                             FETCH Get_Other_Sources_net INTO W_net_total;
                             Pgmloc := 4220;
                             CLOSE Get_Other_Sources_net;
                             v_rows_read    := v_rows_read + 1;
                             --
                             V_Gross_Total  := V_Gross_Total + W_Gross_Total;
                             V_Net_Total    := V_Net_Total + W_Net_Total;
                             v_gross_other    := v_gross_other + v_gross_total;
                             v_net_other      := v_net_other   + v_net_total;                         
                             --
                          END IF;
                       END IF; --}
                       W_Date := Holiday.Get_Next_Open_Date( W_Date
                                                           , 'SOURCE'
                                                           , 'TSE'
                                                           , NULL 
                                                           );
                    END LOOP; --} For rec in get_count_dvs_sas
                 END LOOP; --} For clt in get_client_per_product
                 --
                 Pgmloc := 4230;
                 --
                 -- Inserting into table Acc_CGI_Stats_Out
                 --
                 INSERT INTO acc_cgi_stats_out
                        (fri_client, starting_date, ending_date, product_code
                        ,cgi_source_code, gross_total, net_total, last_user, last_stamp)
                 VALUES (v_fri_client, p_starting_date, p_ending_date, v_product_code
                        ,v_source_code, v_gross_total, v_net_total,Constants.Get_User_Id
                        ,SYSDATE);
                 --
                 v_rows_inserted := v_rows_inserted + 1;   
                 v_gross_client  := v_gross_client  + v_gross_total;
                 v_net_client    := v_net_client    + v_net_total;
                 --
                 Pgmloc := 4240;
                 --
                 Manage_Utl_File.Write_File(V_File_Handle,
                                      '        ' ||
                                      '  Master_Source: '   ||v_source_code||
                                      '  Gross: '           ||lpad(v_Gross_total,10)||
                                      '  Net: '             ||lpad(v_Net_Total,10),
                                      V_Log_Success, V_Log_Message);
                 IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
                 --
              END LOOP; --} CGI_Sources
           END IF; --} V_Product_Code in dvs, sas
           --
           Pgmloc := 4250;
           Manage_Utl_File.Write_File(V_File_Handle,
                                   '                            ' ||
                                   '  TOTALS: '   ||lpad(v_gross_client,10)||
                                   '       '||lpad(v_net_client,10),
                                   V_Log_Success, V_Log_Message);
              IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
           --
           Pgmloc := 4260;
           Manage_Utl_File.Write_File(V_File_Handle,
                                   '    '||
                                   '  Gross Requested:  ' ||lpad(v_Gross_Req,10)||
                                   '  Gross Not Priced: '||lpad(v_gross_not_prcd,10)||
                                   '  Gross Priced:     '||lpad(v_gross_priced,10),
                                   V_Log_Success, V_Log_Message);
           IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
           --
           Pgmloc := 4270;
           Manage_Utl_File.Write_File(V_File_Handle,
                                   '    '||
                                   '  Net requested:    ' ||lpad(v_Net_Req,10)||
                                   '  Net Not Priced:   '||lpad(v_Net_not_prcd,10)||
                                   '  Net Priced:       '||lpad(v_Net_priced,10),
                                   V_Log_Success, V_Log_Message);
           IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
           --
           Pgmloc := 4280;
           Manage_Utl_File.Write_File(V_File_Handle,
                                   '    '||
                                   '  Gross Other:      '||lpad(v_gross_other,10)||
                                   '  Net Other:        '||lpad(v_net_other,10),
                                   V_Log_Success, V_Log_Message);
           IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
           --
           Pgmloc := 4290;
           Manage_Utl_File.Write_File(V_File_Handle,
                                   ' ',
                                   V_Log_Success, V_Log_Message);
           IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
           --
           -- The value returned by cursor get_all_request 
           -- must be equal to  cursor get_not_priced + cursor get_priced
           -- The value returned by cursor get_all_request 
           -- must be equal to  cursor get_others_sources 
           --
           IF  v_Gross_Req       != v_gross_not_prcd + v_gross_priced
               OR v_Net_Req      != v_net_not_prcd   + v_net_priced 
               OR v_gross_priced != v_gross_other
               OR v_net_priced   != v_net_other 
           THEN
             RAISE Different_Amount;
           END IF;
           --
           COMMIT;       
           --
        END LOOP; -- CGI_Clients
        --
    END LOOP; -- Get_Products
    --
    Pgmloc := 4300;
    --
    Manage_Utl_File.Write_File(V_File_Handle,
                              '# Records Inserted: ' ||v_rows_inserted,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 4310;
    --
    Manage_Utl_File.Write_File(V_File_Handle,
                               'Ending Date is ' ||
                               to_char(sysdate, 'DD-Mon-YYYY HH24:MI:SS'),
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 4320;
    --
    Manage_Utl_File.Close_File(V_File_Handle, V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 4330;
    --
    Pricing_Msgs_Rec.Successfull_Flag := 'Y';
    Pricing_Msgs_Rec.Remarks          := 'Log File is: ' || V_Dir_Name ||
                                         '/'             || p_log_file_name;
    Pricing_Msgs_Rec.Value_01 := v_rows_read;
    Pricing_Msgs_Rec.Value_02 := v_rows_inserted; 
    --Pricing_Msgs_Rec.Value_03 := ''; 
    --Pricing_Msgs_Rec.Value_04 := ''; 
    --Pricing_Msgs_Rec.Value_05 := ''; 
    --Pricing_Msgs_Rec.Value_06 := ''; 
    --Pricing_Msgs_Rec.Value_07 := ''; 
    --Pricing_Msgs_Rec.Value_08 := '';
    --Pricing_Msgs_Rec.Value_09 := '';
    --Pricing_Msgs_Rec.Value_10 := '';
    --Pricing_Msgs_Rec.Value_11 := '';
    --Pricing_Msgs_Rec.Value_12 := '';
    --
    Global_File_Updates.Update_Pricing_Msgs
                         (P_Program_Id,
                          v_Sysdate,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    Pgmloc := 4340;
    P_SUCCESS := TRUE;
    P_Message := '';
    COMMIT;
    --
EXCEPTION
    WHEN Invalid_File THEN
       P_Message := V_Log_Message;
       Pricing_Msgs_Rec.Msg_Text_2 := 'Error occured in process ' ||
                                        ' @pgmloc=' ||
                                        TO_CHAR(pgmloc);
       Pricing_Msgs_Rec.Value_01 := v_rows_read;
       Pricing_Msgs_Rec.Value_02 := v_rows_inserted;
       Pricing_Msgs_Rec.Err_Text := P_Message;
       Global_File_Updates.Update_Pricing_Msgs
                               (P_Program_Id,
                                v_Sysdate,
                                Update_Break_Pts,
                                Pricing_Msgs_Rec);
       COMMIT;
       RAISE;
    --
    WHEN Different_Amount THEN
       P_Message := V_Log_Message;
       Pricing_Msgs_Rec.Msg_Text_2 := 'Different Amounts Master_Client=' ||
                                      v_client_code||
                                        ' @pgmloc=' ||
                                        TO_CHAR(pgmloc);
       Pricing_Msgs_Rec.Remarks          := 'Check file: '||V_Dir_Name||'/'||p_log_file_name;
       Pricing_Msgs_Rec.Value_01 := v_rows_read;
       Pricing_Msgs_Rec.Value_02 := v_rows_inserted; 
       Pricing_Msgs_Rec.Err_Text := P_Message;
       Global_File_Updates.Update_Pricing_Msgs
                               (P_Program_Id,
                                v_Sysdate,
                                Update_Break_Pts,
                                Pricing_Msgs_Rec);
       COMMIT;
       RAISE;
    --
    WHEN OTHERS THEN
       Pricing_Msgs_Rec.Msg_Text_2 := 'Error occured in process ' ||
                                     ' @pgmloc=' ||
                                     TO_CHAR(pgmloc);
       Pricing_Msgs_Rec.Value_01 := v_rows_read;
       Pricing_Msgs_Rec.Value_02 := v_rows_inserted;
       Pricing_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
       Global_File_Updates.Update_Pricing_Msgs
                               (P_Program_Id,
                                V_Sysdate,
                                Update_Break_Pts,
                                Pricing_Msgs_Rec);
       COMMIT;
       RAISE;
  END Accounting_Prep;
--------------------------------------------------------------------------------
PROCEDURE Univ_Modified (
          P_Program_Id    IN     Ops_Scripts.Program_Id%TYPE
        , P_Out_File_Name IN     VARCHAR2
        , P_Commit_Freq   IN     INTEGER
        , P_Success          OUT BOOLEAN
        , P_Message          OUT VARCHAR2
        ) IS --{
--
C_Space             CONSTANT VARCHAR2(1) := ' ';
--
Pricing_Msgs_Rec             Pricing_Msgs%ROWTYPE;
PRP                          Product_Running_Parms%ROWTYPE;
Cntl_Rec                     R_Parameters;
V_Message                    VARCHAR2(256);
Data_Rec                     VARCHAR2(1024);
--
R_C                          Clients%ROWTYPE;
R_CU                         Clients_Univ%ROWTYPE;
R_CE                         CGI_Exchanges%ROWTYPE;
--
V_Cursor_Text                VARCHAR2(2048);
V_Table_Name                 VARCHAR2(60);
V_Out_File_Name              VARCHAR2(60);
--
-- TYPE CU_Cur_Type IS REF CURSOR RETURN Clients_Univ%ROWTYPE;
TYPE CU_Cur_Type IS REF CURSOR;
CU_Cursor                    CU_Cur_Type;
--
BEGIN --{
  --
  Pgmloc := 4350;
  P_Success := TRUE;
  --
  -------------------------------
  --  create the pricing message,
  --  open the log
  --  copy the input file
  --  open the copied file
  --  open the output file
  -------------------------------
  --
  Pgmloc := 4360;
  Cntl_Rec.In_File_Name  := NULL;
  Cntl_Rec.Out_File_Name := P_Out_File_Name;
  Cntl_Rec.In_File_Open  := FALSE;
  Cntl_Rec.Out_File_Open := FALSE;
  Cntl_Rec.Log_File_Open := FALSE;
  Pgmloc := 4370;
  Process_Beginning( P_Program_Id
                   , Constants.Get_Site_Date
                   , P_Commit_Freq
                   , PRP
                   , Cntl_Rec
                   , Pricing_Msgs_Rec
                   , P_Success
                   , P_Message
                   );
  IF NOT P_Success THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 4380;
  COMMIT;
  --
  ----------------------------------------------------------------------------
  -- We have committed the messages up to now
  -- From here on this is everything or nothing
  ----------------------------------------------------------------------------
  --
  Pgmloc := 4390;
  SELECT *
    INTO R_C
    FROM Clients
   WHERE Fri_Client = PRP.Fri_Client;
  --
  --
  pgmloc := 4400;
  -- Getting the new securities
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'080'
                                   , 'N'
                                   , V_Message
                                   );
  pgmloc := 4410;
  load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  Pricing_Msgs_Rec.Msg_Text_2 := V_Message;
  pgmloc := 4420;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 4430;
  commit;
  -- Header :
  Pgmloc := 4440;
  Data_Rec := RPAD( 'Security ID', 12 )
           || C_Space
           || RPAD( 'Description', 30 )
           || C_Space
           || 'Exchanges'
            ;
  pgmloc := 4450;
  load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                , Data_Rec
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  Pgmloc := 4460;
  V_Table_Name  := R_C.Code
                || '_CGI_Exchanges_'
                || TO_CHAR( Pricing_Msgs_Rec.Date_of_Prices, 'YYYYMMDD' )
                 ;
  Pgmloc := 4470;
  V_Cursor_Text := 'SELECT *'
                || '  FROM Clients_Univ CU'
                || ' WHERE CU.Fri_Client = '||TO_CHAR( PRP.Fri_Client )
                || '   AND ('
                || '       SELECT MAX( CE.Priority_Number )'
                || '         FROM CGI_Exchanges CE'
                || '        WHERE CE.Fri_Client = CU.Fri_Client'
                || '          AND CE.Client_Security_Id '
                || '              = CU.Client_Security_id'
                || '       ) != ('
                || '       SELECT NVL( MAX( SU.Priority_Number), 0 )'
                || '         FROM '||V_Table_Name||' SU'
                || '        WHERE CU.Fri_Client = SU.Fri_Client'
                || '          AND CU.Client_Security_Id '
                || '              = SU.Client_Security_Id'
                || '       )'
                || ' UNION '
                || 'SELECT *'
                || '  FROM Clients_Univ CU'
                || ' WHERE CU.Fri_Client = '||TO_CHAR( PRP.Fri_Client )
                || '   AND EXISTS ('
                || '       SELECT 1'
                || '         FROM CGI_Exchanges CE'
                || '            , '||V_Table_Name||' SU'
                || '        WHERE CE.Fri_Client     = CU.Fri_Client'
                || '          AND CE.Client_Security_Id '
                || '              = CU.Client_Security_id'
                || '          AND CU.Fri_Client     = SU.Fri_Client'
                || '          AND CU.Client_Security_Id '
                || '              = SU.Client_Security_Id'
                || '          AND CE.Priority_Number  = SU.Priority_Number'
                || '          AND CE.Fri_Source_Code <> SU.Fri_Source_Code'
                || '       ) '
                || 'ORDER BY 2, 3' -- fri_client and client_security_id
                 ;
-- Ajout - JSL - 23 avril 2008
  BEGIN --{
-- Fin d'ajout - JSL - 23 avril 2008
    Pgmloc := 4480;
    OPEN CU_Cursor FOR V_Cursor_Text;
    LOOP --{
      Pgmloc := 4490;
      FETCH CU_Cursor INTO R_CU;
      EXIT WHEN CU_Cursor%NOTFOUND;
      Pgmloc := 4500;
      Pricing_Msgs_Rec.Value_02 := NVL( Pricing_Msgs_Rec.Value_02, 0 )
                                 + 1;
      Pgmloc := 4510;
      Data_Rec := RPAD( R_CU.Client_Security_Id, 12 )
               || C_Space
               || RPAD( SUBSTR( R_CU.Client_Security_Name, 1, 30 ), 30 )
               || C_Space;
      Pgmloc := 4520;
      FOR R_CE IN ( SELECT *
                      FROM CGI_Exchanges CE
                     WHERE CE.Fri_Client = PRP.Fri_Client
                       AND CE.Client_Security_Id = R_Cu.Client_Security_Id
                    ORDER BY PRIORITY_NUMBER
                  )
      LOOP --{
        Pgmloc := 4530;
        Data_Rec := Data_Rec
                 || R_Ce.Fri_Source_Code
                 || C_Space;
      END LOOP; --} For in cgi_exchanges
      pgmloc := 4540;
      load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                    , Data_Rec
                                    , Pricing_Msgs_Rec
                                    , P_Success
                                    );
      IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
      Pricing_Msgs_Rec.Value_01 := NVL( Pricing_Msgs_Rec.Value_01, 0 )
                                 + 1;
    END LOOP; --} For in clients_univ
    Pgmloc := 4550;
    CLOSE CU_Cursor;
  EXCEPTION
  WHEN Table_Not_Found THEN
    pgmloc := 4560;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'090'
                                     , 'N'
                                     , TO_CHAR( Pricing_Msgs_Rec.Date_of_Prices
                                              , 'dd-mon-yyyy' )
                                     , Data_Rec
                                     );
    load_supplier_Rules.Write_file( Cntl_Rec.Out_File
                                  , Data_Rec
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
    IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
    Pricing_Msgs_Rec.Value_01 := NVL( Pricing_Msgs_Rec.Value_01, 0 )
                               + 1;
  END; --}
  --
  pgmloc := 4570;
  Manage_Utl_File.Close_File( Cntl_Rec.Out_File
                            , P_Success
                            , Pricing_Msgs_Rec.Err_Text
                            );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  V_Out_File_Name := Cntl_Rec.Out_File_Name;
  IF INSTR(V_Out_File_Name,Constants.Get_Dir_Name,1)=0
  THEN
     V_Out_File_Name := Cntl_Rec.Dir_Name || Constants.Get_Dir_Seperator
                    || Cntl_Rec.Out_File_Name;
  END IF; 
-- 
  Pricing_Msgs_Rec.Remarks := 'File generated ' || V_out_File_Name;
-- Modification - JSL - 04 mars 2015
--   pgmloc := 4580;
--   AFT_Process.AFTMAKE( 'FILE'
--                      , V_Out_File_Name
--                      , p_success
--                      , Pricing_Msgs_Rec.Err_Text
--                      );
--   IF NOT P_Success THEN GOTO Get_Out; END IF;
-- Modification - JSL - 04 mars 2015
  --
  pgmloc := 4590;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'012'
                                   , 'N'
                                   , TO_CHAR( SYSDATE
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  pgmloc := 4600;
  load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 4610;
  Load_Supplier_Rules.File_Close_All( Pricing_Msgs_Rec
                                    , P_Success
                                    );
  Cntl_Rec.Log_File_Open := FALSE;
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  pgmloc := 4620;
  UPDATE Break_Points
     SET Status     = 'DONE'
       , Nb_Records = Pricing_Msgs_Rec.Value_01
       , Last_User  = Constants.Get_User_Id
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  pgmloc := 4630;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'060'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  pgmloc := 4640;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'062'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  GOTO Upd_Prc_Msg;
  --
<<GET_OUT>>
  pgmloc := 4650;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'070'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'072'
                                   , 'N'
                                   , SYSDATE
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
--
<<Upd_Prc_Msg>>
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  --
  pgmloc := 4660;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 4670;
  commit;
--
EXCEPTION
WHEN OTHERS THEN
  P_Message := SUBSTR( 'At Pgmloc : '
                     || TO_CHAR( Pgmloc )
                     || ' '
                     || SUBSTR( SQLERRM( SQLCODE ), 1, 255 )
                     , 1
                     , 255
                     );
  IF Pricing_Msgs_Rec.Err_Text IS NULL
  THEN
    Pricing_Msgs_Rec.Err_Text := P_Message;
  END IF;
  Pgmloc := 4680;
  ROLLBACK;
  Pgmloc := 4690;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  pgmloc := 4700;
  commit;
  IF Cntl_Rec.Log_File_Open
  THEN
    pgmloc := 4710;
    load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                  , Pricing_Msgs_Rec.Msg_Text
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
  END IF;
  --
  Load_Supplier_Rules.File_Close_All( Pricing_msgs_Rec
                                    , P_Success
                                    );
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  P_Success := FALSE;
END Univ_Modified; --}}
------------------------------------------------------------------------
  --
  -- Procedure writen by Araisbel 22 April 2008
  --
  PROCEDURE Pricing_Stats_Report
  (P_Program_Id        IN  OPS_Scripts.Program_id%TYPE,
   P_Starting_Date     IN  DATE,
   P_Ending_Date       IN  DATE,
   P_File_Name         IN  VARCHAR2,
   P_SUCCESS           OUT BOOLEAN,
   P_MESSAGE           OUT VARCHAR2) IS --{
   --
   C_Line_Per_Page     CONSTANT INTEGER := 61; -- Don't ask, that we received!
   --
   v_grouping_client   cgi_clients.grouping_fri_client%TYPE;
   v_fri_client        cgi_clients.fri_client%TYPE;
   v_length            NUMBER;
   v_clients_read      NUMBER  := 0;
   v_page              NUMBER  := 0;
   v_count_client      NUMBER  := 0;
   --
   V_Dir_Name          VARCHAR2(60) := Constants.Get_dir_name;
   V_File_Handle       UTL_FILE.FILE_TYPE;
   V_File_Success      BOOLEAN;
   V_File_Message      VARCHAR2(256);
   V_Log_Handle        UTL_FILE.FILE_TYPE;
   V_Log_File_Name     VARCHAR2(100);
   V_Log_Success       BOOLEAN;
   V_Log_Message       VARCHAR2(256);
   Update_Break_Pts    BOOLEAN     := FALSE;
   V_sysdate           DATE        := SYSDATE;
   Pricing_Msgs_Rec    Pricing_Msgs%ROWTYPE;
   --
   Invalid_File     EXCEPTION;
   -- AFT Variables
   V_Client          Clients.Code%TYPE;
   V_Product         Products.Code%TYPE;
   V_Sub_Product     Sub_Products.Sub_Product_Code%TYPE;
   V_Date            DATE;
   V_AFT_Flag        YES_NO.Code%TYPE;
-- Modification - JSL - 04 mars 2015
--    Invalid_AFT       EXCEPTION;
-- Fin de modification - JSL - 04 mars 2015
   --
   V_Line_in_Page      INTEGER;
   V_Parent_Pgm_Id     OPS_Scripts.Program_Id%TYPE;
   V_Program_Id        OPS_Scripts.Program_Id%TYPE;
   --
   -- Get_Client
   --
   CURSOR Get_Client IS
   SELECT C.grouping_fri_client
         ,G.text_e grouping_text
         ,MAX(C.sort_order) sort_order
         ,C.master_fri_client 
         ,C1.text_e master_text
   FROM   cgi_clients C
         ,cgi_clients C1
         ,cgi_client_groups G
   WHERE  C.grouping_fri_client = G.fri_client
   AND    C.master_fri_client   = C1.fri_client
   -- Miglena - 02Jun2014
   --AND    C.active_flag         = 'Y'
   --AND    C1.active_flag        = 'Y'
   -- end Miglena
   AND    EXISTS (
          SELECT 1
            FROM Acc_Cgi_Stats_Out O
           WHERE O.Starting_Date >= P_Starting_Date 
             AND O.Ending_Date   <= P_Ending_Date
             AND O.Fri_Client     = C.Fri_Client
             AND O.Product_Code   = 'SVS'
          )
   GROUP BY C.grouping_fri_client, G.text_e, C.master_fri_client,C1.text_e
   ORDER BY MAX(C.sort_order);
   --
   -- Get_Acc_cgi_stats
   --
   CURSOR Get_Acc_CGI_Stats IS
   SELECT O.cgi_source_code
         ,S.text_e
         ,SUM(O.gross_total) gross
         ,SUM(O.net_total) net
         ,S.SORT_ORDER
   FROM  acc_cgi_stats_out O
        ,cgi_sources S
   WHERE O.starting_date    >= P_Starting_Date 
   AND   O.ending_date      <= P_Ending_Date
   AND   O.cgi_source_code  = S.code
   AND   O.fri_client       = v_fri_client
   -- SVS ONLY (Richard - 28Apr2008)
   AND   O.Product_Code     = 'SVS'
   GROUP BY   O.cgi_source_code,S.text_e,S.SORT_ORDER
   ORDER BY  S.sort_order;
   --
   -- Get_Request
   --
   CURSOR Get_Request IS
   SELECT G.Text_E         Text_E
        , MAX(O.Net_Total) Number_Req
   FROM  CGI_Clients       C
       , CGI_Client_Groups G
       , Acc_CGI_Stats_Out O
   WHERE O.Starting_Date    >= P_Starting_Date 
     AND O.Ending_Date      <= P_Ending_Date
     AND O.Product_Code IN ('SAS','DVS')
     AND C.Fri_Client        = O.Fri_Client
     AND G.Fri_Client        = C.Master_Fri_Client
     AND NOT EXISTS (
         SELECT 1
           FROM Acc_CGI_Stats_Out O1
              , CGI_Clients       C1
          WHERE O1.Starting_Date     = O.Starting_Date
            AND O1.Ending_Date       = O.Ending_Date
            AND O1.Product_Code      = 'SVS'
            AND C1.Master_Fri_Client = C.Master_Fri_Client
         )
   GROUP BY G.Text_E
   ORDER BY 1;
   --
  BEGIN --{
    Pgmloc := 4720;
    P_SUCCESS        := FALSE;
    v_SYSDATE        := SYSDATE;
    Update_Break_Pts := FALSE;
    --   
    -- Get Log File name
    --
    v_length        := LENGTH(P_File_Name);
    V_log_File_Name := substr(P_File_Name,1,(v_length-3))||'log';
    --
    V_Program_Id    := P_Program_Id;
    IF V_Program_Id > 9999
    THEN
      V_Parent_Pgm_Id := V_Program_Id / 100;
    ELSE
      V_Parent_Pgm_Id := V_Program_Id;
    END IF;
    --
    Pgmloc := 4730;
    Global_File_Updates.Initialize_Pricing_Msgs
                         (P_Program_id,
                          v_SYSDATE,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    Pgmloc := 4740;
    COMMIT;
    Pgmloc := 4750;
    Pricing_Msgs_Rec.Msg_Text := 'Creating Pricing Stats Report.';
    Pricing_Msgs_Rec.Msg_Text_2 := 'From '  || 
                                  TO_CHAR(P_Starting_Date,'DD-Mon-YYYY') ||
                                  ' To '   || 
                                  TO_CHAR(P_Ending_Date,'DD-Mon-YYYY');
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
    Pricing_Msgs_Rec.Value_01 := 0;
    Pricing_Msgs_Rec.Value_02 := '';
    Pricing_Msgs_Rec.Value_03 := '';
    Pricing_Msgs_Rec.Value_04 := '';
    Pricing_Msgs_Rec.Value_05 := '';
    Pricing_Msgs_Rec.Value_06 := '';
    Pricing_Msgs_Rec.Value_07 := '';
    Pricing_Msgs_Rec.Value_08 := '';
    Pricing_Msgs_Rec.Value_09 := '';
    Pricing_Msgs_Rec.Value_10 := '';
    Pricing_Msgs_Rec.Value_11 := '';
    Pricing_Msgs_Rec.Value_12 := '';
    --
    Pgmloc := 4760;
    Global_File_Updates.Update_Pricing_Msgs
                         (P_Program_Id,
                          v_Sysdate,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    Pgmloc := 4770;
    COMMIT;
    --
    --  OPENING Log file 
    --  
    Pgmloc := 4780;
    Manage_Utl_File.Open_File (V_Log_File_Name, 'W', V_Log_Handle,
                               V_Log_Success, V_Log_Message);
    --
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 4790;
    Manage_Utl_File.Write_File(V_Log_Handle,
                               'Starting Date is ' ||
                               to_char(sysdate, 'DD-Mon-YYYY HH24:MI:SS'),
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 4800;
    Manage_Utl_File.Write_File(V_Log_Handle,
                               Pricing_Msgs_Rec.Msg_Text,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 4810;
    Manage_Utl_File.Write_File(V_Log_Handle,
                               Pricing_Msgs_Rec.Msg_Text_2,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    -- 
    --
    --  OPENING Report File 
    --  
    Pgmloc := 4820;
    Manage_Utl_File.Open_File (P_File_Name, 'W', V_File_Handle,
                               V_File_Success, V_File_Message);
    --
    IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
    -- 
    -- Processing Client
    --
    Pgmloc := 4830;
    FOR Rec_Client IN Get_Client LOOP --{
        -- 
        Pgmloc := 4840;
        v_fri_client  := Rec_Client.master_fri_client;
        v_clients_read := v_clients_read + 1;
        v_count_client := v_count_client + 1;
        --
        IF v_count_client = 2 THEN
           v_count_client := 0;
        END IF;
        --
        IF v_count_client = 1 THEN
           --
           -- Header
           --
           v_page := v_page + 1;
           V_Line_in_Page := 0;
           --
           Pgmloc := 4850;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      to_char(sysdate, 'Mon/DD/YYYY HH24:MI')||
                                      '               '||
                                      ' S T A T P R O ',
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4860;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      'PAGE '||v_page,
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4870;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      ' ',
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4880;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      'price_stats             Pricing Statistics Report',
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4890;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      '                  For   '||
                                      to_char(p_starting_date, 'Mon/DD/YYYY')||
                                      '   To   '||
                                      to_char(p_ending_date, 'Mon/DD/YYYY'),
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4900;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      '                           Holidays Excluded ',
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4910;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      '',
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4920;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      '                            BILLABLE CLIENTS ',
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4930;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      '',
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --  
        END IF;
        --
        Pgmloc := 4940;
        IF NVL(v_grouping_client,1.12) != Rec_Client.grouping_fri_client THEN
           --
           -- writing group
           --
           Pgmloc := 4950;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      ' FOR CLIENT GROUP: '||Rec_Client.grouping_text,
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
           Pgmloc := 4960;
           Manage_Utl_File.Write_File(V_File_Handle,
                                      ' ',
                                      V_File_Success, V_File_Message);
           IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
           V_Line_in_Page := V_Line_In_Page + 1;
           --
        END IF;
        --
        Pgmloc := 4970;
        v_grouping_client := Rec_Client.grouping_fri_client;
        --
        -- writing master client
        --
        Pgmloc := 4980;
        Manage_Utl_File.Write_File(V_File_Handle,
                                   '   '||Rec_Client.master_text,
                                   V_File_Success, V_File_Message);
        IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
        V_Line_in_Page := V_Line_In_Page + 1;
        --
        Pgmloc := 4990;
        Manage_Utl_File.Write_File(V_File_Handle,
                                   '   -----------------------------------',
                                   V_File_Success, V_File_Message);
        IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
        V_Line_in_Page := V_Line_In_Page + 1;
        --
        Pgmloc := 5000;
        Manage_Utl_File.Write_File(V_File_Handle,
                                   ' ',
                                   V_File_Success, V_File_Message);
        IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
        V_Line_in_Page := V_Line_In_Page + 1;
        --
        -- Opening cursor Get_Acc_CGI_Stats
        --
        Pgmloc := 5010;
        FOR rec_stats IN Get_Acc_CGI_Stats LOOP
            --
            -- writing datails
            --
            Pgmloc := 5020;
            Manage_Utl_File.Write_File(V_File_Handle,
                                   '        '
                                 ||rpad(Rec_stats.text_e,35)
                                 ||lpad(TO_CHAR(Rec_stats.net,'999,999,990')
                                       ,20),
                                   V_File_Success, V_File_Message);
            IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
            V_Line_in_Page := V_Line_In_Page + 1;
            --
        END LOOP;
        --
        Pgmloc := 5030;
        Manage_Utl_File.Write_File(V_File_Handle,
                                   '',
                                   V_File_Success, V_File_Message);
        IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
        V_Line_in_Page := V_Line_In_Page + 1;
        --
        Pgmloc := 5040;
        Manage_Utl_File.Write_File(V_File_Handle,
                                   '',
                                   V_File_Success, V_File_Message);
        IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
        V_Line_in_Page := V_Line_In_Page + 1;
        --
        -- page break
        IF v_count_client = 0 THEN 
           WHILE V_Line_In_Page < C_Line_Per_Page
           LOOP
              Pgmloc := 5050;
              Manage_Utl_File.Write_File(V_File_Handle,
                                         ' ',
                                         V_File_Success, V_File_Message);
              IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
              V_Line_in_Page := V_Line_In_Page + 1;
              --
           END LOOP;
       END IF;
    END LOOP; --}
    --
    -- Closing TXT File
    --
    Pgmloc := 5060;
    --
    Manage_Utl_File.Write_File(V_File_Handle,
                               'TOTAL NUMBER OF CLIENTS --->    '||v_clients_read,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 5070;
    Manage_Utl_File.Write_File(V_File_Handle,
                              ' ',
                               V_File_Success, V_File_Message);
    IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 5080;
    Manage_Utl_File.Write_File(V_File_Handle,
                               ' ',
                               V_File_Success, V_File_Message);
    IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 5090;
    Manage_Utl_File.Write_File(V_File_Handle,
                               '               *------ End of Report ------*' ,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    -- 
    Pgmloc := 5100;
    FOR Reg in 1.. 10 LOOP
        Pgmloc := 5110;
        Manage_Utl_File.Write_File(V_File_Handle,
                                   ' ',
                                   V_File_Success, V_File_Message);
        IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
        --
    END LOOP;
    --
    Pgmloc := 5120;
    Manage_Utl_File.Write_File(V_File_Handle,
                               'NUMBER OF REQUESTS (PER DAY) FOR CORPORATE ACTION ONLY CLIENTS: ',
                               V_File_Success, V_File_Message);
    IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 5130;
    Manage_Utl_File.Write_File(V_File_Handle,
                               ' ',
                               V_File_Success, V_File_Message);
    IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
    --
    -- Number of request 
    --
    Pgmloc := 5140;
    FOR req IN Get_Request LOOP
        --
        Pgmloc := 5150;
        Manage_Utl_File.Write_File( V_File_Handle,
                                    Req.Text_E
                                 || ' - '
                                 || Req.Number_Req,
                                    V_File_Success, V_File_Message);
        IF NOT V_File_Success THEN RAISE Invalid_File; END IF;
        --
    END LOOP;
    --
    Pgmloc := 5160;
    --
    Pricing_Msgs_Rec.Successfull_Flag := 'Y';
    Pricing_Msgs_Rec.Remarks          := 'File Generated at '
                                      || V_Dir_Name
                                      || Constants.Get_dir_Seperator
                                      || p_file_name;
    Pricing_Msgs_Rec.Value_01 := v_clients_read;
    --
    Pgmloc := 5170;
    --
    Manage_Utl_File.Write_File(V_Log_Handle,
                               'TOTAL NUMBER OF CLIENTS: '||v_clients_read,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 5180;
    --
    Manage_Utl_File.Write_File(V_Log_Handle,
                               Pricing_Msgs_Rec.Remarks,
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 5190;
    Global_File_Updates.Update_Pricing_Msgs
                         (P_Program_Id,
                          v_Sysdate,
                          Update_Break_Pts,
                          Pricing_Msgs_Rec);
    Pgmloc := 5200;
    P_SUCCESS := TRUE;
    P_Message := '';
    Pgmloc := 5210;
    COMMIT;
    --
-- Modification - JSL - 04 mars 2015
--     --  Calling to AFT_Process
--     --
--     Pgmloc := 5220;
--     Extract_Info.Get_Running_Info(V_Parent_Pgm_Id
--                                  ,V_Client
--                                  ,V_Fri_Client
--                                  ,V_Product
--                                  ,V_Sub_Product
--                                  ,V_Date
--                                  ,V_Aft_Flag
--                                  ,Pricing_Msgs_Rec
--                                  ,P_Success);
--     IF NOT P_Success THEN Raise Invalid_AFT; END IF;
--     --
--     Pgmloc := 5230;
--     --
--     Manage_Utl_File.Write_File(V_Log_Handle,
--                                'DB MODE: ' ||
--                                Constants.Get_DB_Mode,
--                                V_Log_Success, V_Log_Message);
--     IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
--     --
--     Pgmloc := 5240;
--     --
--     IF V_AFT_Flag            = 'Y'    AND
--        Constants.Get_DB_Mode = 'PROD'
--     THEN
--        --
--        Pgmloc := 5250;
--        Pricing_Msgs_Rec.Msg_Text_2 := 'Adding call to AFT_Process ';
--        Global_File_Updates.Update_Pricing_Msgs
--                          (P_Program_Id,
--                           v_Sysdate,
--                           Update_Break_Pts,
--                           Pricing_Msgs_Rec);
--        Pgmloc := 5260;
--        Commit;
--        --
--        Pgmloc := 5270;
--        --
--        Manage_Utl_File.Write_File(V_Log_Handle,
--                                   Pricing_Msgs_Rec.Msg_Text_2,
--                                   V_Log_Success, V_Log_Message);
--        IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
--        --
--        Pgmloc := 5280;
--        --
--        -- To Check that file name already doesn't have path in it
--        --
--        IF instr(P_File_Name, Constants.Get_Dir_Seperator) = 0 THEN
--             Pgmloc := 5290;
--             AFT_Process.AFTMAKE('FILE'
--                                , V_Dir_Name ||Constants.Get_Dir_Seperator||
--                                 P_File_Name
--                                ,P_Success
--                                ,P_Message);
--        ELSE
--             Pgmloc := 5300;
--             AFT_Process.AFTMAKE('FILE'
--                                ,P_File_Name
--                                ,P_Success
--                                ,P_Message);
--         END IF;
--         IF NOT P_Success THEN
--            Pricing_Msgs_Rec.Msg_Text_2 := 'Problem in AFT';
--            Pricing_Msgs_Rec.Err_Text   := P_Message;
--            Raise Invalid_AFT;
--         END IF;
--     END IF;
-- Fin de modification - JSL - 04 mars 2015
    --
    -- Closing Log File
    --
    Pgmloc := 5310;
    --
    Manage_Utl_File.Write_File(V_Log_Handle,
                               'Ending Date is ' ||
                               to_char(sysdate, 'DD-Mon-YYYY HH24:MI:SS'),
                               V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
    Pgmloc := 5320;
    --
    Manage_Utl_File.Close_File(V_Log_Handle, V_Log_Success, V_Log_Message);
    IF NOT V_Log_Success THEN RAISE Invalid_File; END IF;
    --
EXCEPTION
    WHEN Invalid_File THEN
       P_Message := V_Log_Message;
       Pricing_Msgs_Rec.Value_01 := v_clients_read;
       Pricing_Msgs_Rec.Msg_Text_2 := 'Error occured in process ' ||
                                        ' @pgmloc=' ||
                                        TO_CHAR(pgmloc);
       Pricing_Msgs_Rec.Err_Text := P_Message;
       Pgmloc := 5330;
       Global_File_Updates.Update_Pricing_Msgs
                               (P_Program_Id,
                                v_Sysdate,
                                Update_Break_Pts,
                                Pricing_Msgs_Rec);
       Pgmloc := 5340;
       COMMIT;
       Pgmloc := 5350;
       RAISE;
    --
-- Modification - JSL - 04 mars 2015
--     WHEN Invalid_AFT THEN
--        Pricing_Msgs_Rec.Value_01 := v_clients_read;
--        Pricing_Msgs_Rec.Msg_Text_2 := 'Error occured in calling AFT Process ' ||
--                                      ' @pgmloc=' ||
--                                      TO_CHAR(pgmloc);
--        Pricing_Msgs_Rec.Err_Text := P_Message;
--        Pgmloc := 5360;
--        Global_File_Updates.Update_Pricing_Msgs
--                                (P_Program_Id,
--                                 v_Sysdate,
--                                 Update_Break_Pts,
--                                 Pricing_Msgs_Rec);
--        Pgmloc := 5370;
--        COMMIT;
--        Pgmloc := 5380;
--        RAISE;
-- Fin de modification - JSL - 04 mars 2015
    --
    WHEN OTHERS THEN
       Pricing_Msgs_Rec.Value_01 := v_clients_read;
       Pricing_Msgs_Rec.Msg_Text_2 := 'Error occured in process ' ||
                                     ' @pgmloc=' ||
                                     TO_CHAR(pgmloc);
       Pricing_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
       Pgmloc := 5390;
       Global_File_Updates.Update_Pricing_Msgs
                               (P_Program_Id,
                                V_Sysdate,
                                Update_Break_Pts,
                                Pricing_Msgs_Rec);
       Pgmloc := 5400;
       COMMIT;
       Pgmloc := 5410;
       RAISE;
    --
  END Pricing_Stats_Report; --}}
------------------------------------------------------------------------
END CGI_Process; --}
/
show error
