CREATE OR REPLACE PACKAGE BODY &&owner..Generate_Interest_Payments
AS
--
-- Phebe   - 09 Oct 2003 - Using Constants.Get_Dir_Seperator to get the
--                         Directory and File seperator.('/'or'\').
--     JSL - 23 nov 2007 - Generate the interest payment for all interest
--                         paying securities, not only the ones with the
--                         IB (Interest Bearing) pattern code.
--                       - Generate the interest payment on a wider window
--                         centered on the run date.
--
--     RV  - 24 nov 2007 - If a security is Extendible, then the payment dates
--                         are not generated if we already passed the Std
--                         Maturity Date.
--
-- Araisbel 25-NOV-2008  Adding new procedure CAF_Interest_Payment
--
-- Richard  09-NOV-2009  This procedure will not delete any more
--                       any Interest Payments (from Interest_Schedule)
--
--     JSL - 01 jun 2010 - Accept the Caf interest payment for Canadian issuers
--                         only.
--
--     JSL - 17 aug 2010 - Accept the CAF Interest payment for all issuers.
--                       - Keep generating the payment dates even after a CAF.
--                       - Treat only interest schedule for supplier STP.
--                       - Not all lines are identified.
--
--     JSL - 26 aug 2010 - Do not try to find the income date for bonds
--                         paying more than once a month.
--                         - Add_months do not accept partial months
--                         - What is the difference between biweekly and
--                           twice a month?  What is the date to use?
--                         - With Rob perez and agreed upon, we skip those!
--
--     JSL - 07 sep 2010 - Do not insert payment before the first issue date!
--                       - Do not insert payment after the last maturity date!
--                       - Use proper size buffer for error handling.
--                         Get_Oasis_Message use a 200 character buffer.
--
--     JSL - 14 sep 2010 - Add the same basic rules as applied by Initram
--                         i.e. Accrued interest, price_mod_duration and
--                         dirty_price become null if the clean price is 70
--                         or less.
--
--     JSL - 13 oct 2010 - Add the synonyms on official sources.
--
--     JSL - 18 oct 2010 - Correction to retrieval of primary source
--                         mainly for historical calculation
--                       - Correction to finding the drop in accrued interest
--                         There must be a drop and the previous accrued
--                         interest must not be zero.
--
--     JSL - 05 nov 2010 - When the security is maturing or being called,
--                         the accrued interest is forced to zero, if not
--                         already.
--                       - Get payment on weekend when the security is
--                         maturing or being called.
--
--     mlz -    mar 2011 - extract common code 
--                         from: interest_payment_schedule
--                           to: generate_int_sch_for_one
--                       - new procedure: interest_payment_sch_to_recalc to:
--                         *re-generate schedule for 1 security, triggered
--                          by row in prices_to_recalc, code GENINT
--                         *(re)populate the ex-dates/amounts for 1 security
--                          also triggered by prices_to_Recalc, code: POPEXD
--     JSL - 10 mai 2011 - manage that the ex-date never be greater than the
--                         maturity date.
--     JSL - 13 mai 2011 - check constraint sur interest schedule : the
--                         income date must be greater than or equal to
--                         January 1st, 1991.
--     JSL - 09 mai 2012 - Les fonctions sur synonymes sont maintenant dans le
--                         package pricing_sources.
--     JSL - 08 jul 2014 - OASIS-4010
--                       - Ajouter des logs (differents niveaux avec et sans
--                         longueur d'execution)
--
-- mlz - 15nov2015 - oasis-6222 - performance tuning
--
-----------------------------------------------------------------------
--
-- Ajout - JSL - 17 aout 2010
C_STP             CONSTANT Data_Suppliers.Code%TYPE := 'STPM';
-- Fin d'ajout - JSL - 17 aout 2010
C_Oasis_Message   CONSTANT VARCHAR2(30) := 'PROC-GEN_INT_PAYMENTS-';
c_genint          constant prices_to_recalc.recalc_type_code%type := 'GENINT';
c_popexdate       constant prices_to_recalc.recalc_type_code%type := 'POPEXD';
--
-- Maximum number of months in the future to produce the interest schedule
-- This maximum apply to all securities, including the perpetual.
C_Max_Months_In_Future CONSTANT INTEGER := 120;  -- 10 years!
c_line            constant varchar2(80) :=
                          '-------------------------------------------------';
--
G_User            User_List.User_ID%TYPE := Constants.G_User_Id;
e_program_error   exception;
--
Pgmloc            INTEGER;
--
c_root_table      constant varchar2(30) := 'OFF_SECURITY_PRICES';
C_Cutoff_Date     CONSTANT DATE := to_date('01-jan-2005', 'dd-mon-yyyy');
G_Prc_Cutoff_Date          DATE := to_date('01-jan-2005', 'dd-mon-yyyy');
c_default_sdai    constant number := 0.9;
--
-- Ajout - JSL - 08 juillet 2014
G_All_Parms                     scripts.parms_table_type;
G_log_handle                    manage_prc_msgs_det.handle_t;
C_Sep                  constant varchar2(1) := chr(9);
G_Get_Timing                    boolean := FALSE;
G_Start_Time                    number;
G_End_Time                      number;
G_Elapsed_Time                  number;
G_Monitor_Level                 integer;
-- Fin d'ajout - JSL - 08 juillet 2014
--
g_new_count            boolean := false;
--
c_cnt_sch_ins          constant integer := 1;
c_cnt_sch_del          constant integer := 2;
c_cnt_sch_calc_parms   constant integer := 3;
c_cnt_sch_sec_affected constant integer := 4;
--
c_cnt_sch_ski_inactive constant integer := 5;
c_cnt_sch_ski_no_calc  constant integer := 6;
c_cnt_sch_ski_no_int_sch_flag  constant integer := 7;
c_cnt_sch_ski_no_mat_dt        constant integer := 8;
c_cnt_sch_ski_in_dt_aft_mat_dt constant integer := 9;
c_cnt_sch_ski_no_rates_found   constant integer := 10;
c_cnt_sch_ski_pay_dt_gt_max_dt constant integer := 11;
c_cnt_sch_ski_wrong_freq       constant integer := 12;
c_cnt_Popexd_Inserted          constant integer := 13;
c_cnt_GenInt_Read              constant integer := 14;
c_cnt_GenInt_already_done      constant integer := 15;
--
c_cnt_PopExd_Read              constant integer := 21;
c_cnt_caf_ski_pri              constant integer := 22;
c_cnt_caf_ski_adj              constant integer := 23;
c_cnt_caf_ski_acc_neg          constant integer := 24;
c_cnt_caf_ski_zero_div         constant integer := 25;
c_cnt_caf_ski_no_sch           constant integer := 26;
c_cnt_caf_ski_min_gt_max       constant integer := 27;
c_cnt_popexd_already_Done      constant integer := 29;
c_cnt_popexd_Treated           constant integer := 30;
c_sec_affected                 constant integer := 31;
c_cnt_popexd_Cleaned           constant integer := 32;
c_cnt_popexd_Updated           constant integer := 33;

procedure count_line_processed(
  p_code             in number,
  p_Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE,
  p_cnt              IN integer := null
  )
is
  l_cnt integer;
begin
  l_cnt := nvl(p_cnt, 1);

--Pgmloc := 1010;
  case p_code
  when c_cnt_sch_ins then
--  Pgmloc := 1020;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_07 := nvl(p_Pricing_Msgs_Rec.value_07, 0) +l_cnt;
    else
      p_Pricing_Msgs_Rec.value_01 := nvl(p_Pricing_Msgs_Rec.value_01, 0) +l_cnt;
    end if;
  when c_cnt_sch_del then
    Pgmloc := 1030;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_06 := nvl(p_Pricing_Msgs_Rec.value_06, 0) +l_cnt;
    else
      p_Pricing_Msgs_Rec.value_02 := nvl(p_Pricing_Msgs_Rec.value_02, 0) +l_cnt;
    end if;
  when c_cnt_sch_calc_parms then
    Pgmloc := 1040;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_08 := nvl(p_Pricing_Msgs_Rec.value_08, 0) +l_cnt;
    else
      p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;
  when c_cnt_sch_sec_affected then
    Pgmloc := 1050;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_04 := nvl(p_Pricing_Msgs_Rec.value_04, 0) +l_cnt;
    else
      p_Pricing_Msgs_Rec.value_04 := nvl(p_Pricing_Msgs_Rec.value_04, 0) +l_cnt;
    end if;
  --
  when c_cnt_GENINT_Read         then
--  Pgmloc := 1060;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_01 := nvl(p_Pricing_Msgs_Rec.value_01, 0) +l_cnt;
    end if;
  when c_cnt_GenInt_already_Done then
    Pgmloc := 1070;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_02 := nvl(p_Pricing_Msgs_Rec.value_02, 0) +l_cnt;
    end if;
  when c_cnt_popexd_Inserted     then
    Pgmloc := 1080;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_09 := nvl(p_Pricing_Msgs_Rec.value_09, 0) +l_cnt;
    end if;
  when c_cnt_sch_ski_inactive then
    Pgmloc := 1090;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;
  when c_cnt_sch_ski_no_calc then
    Pgmloc := 1100;
    if g_new_count then
    p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;
  when c_cnt_sch_ski_no_int_sch_flag then
    Pgmloc := 1110;
    if g_new_count then
    p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;
  when c_cnt_sch_ski_no_mat_dt then
    Pgmloc := 1120;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;
  when c_cnt_sch_ski_in_dt_aft_mat_dt then
    Pgmloc := 1130;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;
  when c_cnt_sch_ski_no_rates_found then
    Pgmloc := 1140;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;
  when c_cnt_sch_ski_pay_dt_gt_max_dt then
    Pgmloc := 1150;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;
  when c_cnt_sch_ski_wrong_freq then
    Pgmloc := 1160;
    if g_new_count then
      p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
    end if;

  --MNT / CAF
  when c_cnt_PopExd_Read then
    Pgmloc := 1170;
    p_Pricing_Msgs_Rec.value_01 := nvl(p_Pricing_Msgs_Rec.value_01, 0) +l_cnt;
  when c_cnt_popexd_already_done then
    Pgmloc := 1180;
    p_Pricing_Msgs_Rec.value_02 := nvl(p_Pricing_Msgs_Rec.value_02, 0) +l_cnt;
  when c_cnt_caf_ski_pri then
    Pgmloc := 1190;
    p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
  when c_cnt_caf_ski_adj then
    Pgmloc := 1200;
    p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
  when c_cnt_caf_ski_acc_neg then
    Pgmloc := 1210;
    p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
  when c_cnt_caf_ski_zero_div then
    Pgmloc := 1220;
    p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
  when c_cnt_caf_ski_no_sch then
    Pgmloc := 1230;
    p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
  when c_cnt_caf_ski_min_gt_max then
    Pgmloc := 1240;
    p_Pricing_Msgs_Rec.value_03 := nvl(p_Pricing_Msgs_Rec.value_03, 0) +l_cnt;
  when c_cnt_PopExd_Treated     then
    Pgmloc := 1250;
    p_Pricing_Msgs_Rec.value_04 := nvl(p_Pricing_Msgs_Rec.value_04, 0) +l_cnt;
  when c_cnt_PopExd_Cleaned     then
    Pgmloc := 1260;
    p_Pricing_Msgs_Rec.value_06 := nvl(p_Pricing_Msgs_Rec.value_06, 0) +l_cnt;
  when c_cnt_PopExd_Updated     then
    Pgmloc := 1270;
    p_Pricing_Msgs_Rec.value_07 := nvl(p_Pricing_Msgs_Rec.value_07, 0) +l_cnt;
--
-- ATTENTION
  when c_Sec_Affected then
    Pgmloc := 1280;
    p_Pricing_Msgs_Rec.value_04 := nvl(p_cnt, 0);
-- ATTENTION
--
  else
    null;
  end case;
end;

--
-- Ajout - JSL - 08 juillet 2014
-------------------------------------------------------------------------
PROCEDURE Log_Parameters
        ( Parameters       IN OUT NOCOPY Scripts.Parms_Table_Type
        , Cntl_Rec         IN OUT NOCOPY Product_Component.R_Parameters
        , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        ) IS --{
V_Parm           Ops_Scripts_Parms%ROWTYPE;
V_Success        BOOLEAN;
BEGIN
  IF Cntl_Rec.Log_File_Open
  THEN
    Pgmloc := 1010;
    Load_Supplier_Rules.Write_File( Cntl_Rec.Log_File
                                  , 'Parameters :'
                                  , Pricing_Msgs_Rec
                                  , V_Success
                                  );
    IF Parameters.COUNT > 0
    THEN
      V_Parm.Parm_Name := Parameters.FIRST;
      LOOP
        V_Parm := Parameters( V_Parm.Parm_Name );
        Load_Supplier_Rules.Write_File( Cntl_Rec.Log_File
                                      , 'Name: <'
                                     || V_Parm.Parm_Name
                                     || '>  value : <'
                                     || V_Parm.Parm_Value
                                     || '>'
                                      , Pricing_Msgs_Rec
                                      , V_Success
                                      );
        EXIT WHEN V_Parm.Parm_Name = Parameters.LAST;
        V_Parm.Parm_Name := Parameters.NEXT( V_Parm.Parm_Name );
      END LOOP;
    ELSE
      Pgmloc := 1020;
      Load_Supplier_Rules.Write_File( Cntl_Rec.Log_File
                                    , 'NONE'
                                    , Pricing_Msgs_Rec
                                    , V_Success
                                    );
    END IF;
  END IF;
END Log_Parameters; --}}
-- Fin d'ajout - JSL - 08 juillet 2014



procedure generate_int_sch_for_one(
  p_fri_ticker         in securities.fri_ticker%type
  , p_log_file        in utl_file.file_type
  , p_income_date      in date
  , p_Inc_Freq_Value   in Frequencies.Value%TYPE
  , p_min_date_to_keep in date
  , p_max_date         in date
  , p_affected         out boolean
  , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
  , P_Success           OUT BOOLEAN
  )
is
  V_Payment_Date                 DATE;

  V_Day                          VARCHAR2(2);
  V_Month_Year                   VARCHAR2(12);
  V_Next_Payment_Date            DATE;
  V_First_Payment_Date           DATE;

  v_min_date                     date;
  v_max_date                     date;
  v_stop_date                    date;

  v_success                      boolean;
  v_message                      pricing_msgs.err_text%type;

  CURSOR Cur_I_Calc_Parms   IS
  SELECT Floating_Rate_Flag,
         Generate_Int_Sch_Flag,
         End_Of_Month_Flag
  FROM   Interest_Calc_Parms
  WHERE  Fri_Ticker = p_Fri_Ticker
  ;
  r_i_calc_parms  cur_i_calc_parms%rowtype;


  --
  -- Rewritten by Richard
  -- the Ext are not processed. Another old Bug.
  -- If the security is still Active, then we need to fetch the next
  -- Extendible date / Richard 24 Nov 2007
  CURSOR Cur_Mat_Date     IS
  -- SELECT M.Maturity_Date
  SELECT min(M.Maturity_Date) as Mat_Date
  FROM   Maturity_Schedule M,
         Maturity_Types    T
  WHERE  M.Fri_Ticker               = p_Fri_Ticker
  AND    M.Maturity_Type_Code       = T.Code
  AND    M.Maturity_Date           >= p_Income_Date
  AND    T.Maturity_Short_Type_Code IN ('E','S')
  ;
  r_mat_date  cur_mat_date%rowtype;


  -- Ajout - JSL - 07 septembre 2010
  CURSOR Cur_Min_Issue_Date
       ( I_Fri_Ticker securities.Fri_Ticker%TYPE
       ) IS
  SELECT MIN( Issue_Date )
  FROM   Issues
  WHERE  Fri_Ticker = I_Fri_Ticker
  ;
  V_Min_Issue_Date DATE;


  CURSOR Cur_Max_Maturity_Date
       ( I_Fri_Ticker securities.Fri_Ticker%TYPE
       ) IS
  SELECT Max( Maturity_Date )
  FROM   Maturity_Schedule
  WHERE  Fri_Ticker = I_Fri_Ticker
  ;
  V_Max_Maturity_Date DATE;
  -- Fin d'ajout - JSL - 07 septembre 2010


  CURSOR Cur_Max_First_Payment_Date(P_Date DATE)   IS
  SELECT MAX(First_Payment_Date)
  FROM   Interest_Income_Rates
  WHERE  Fri_Ticker          = p_Fri_Ticker
  AND    First_Payment_Date <= P_Date
  ;
  CURSOR Cur_Min_First_Payment_Date(P_Date DATE)   IS
  SELECT MIN(First_Payment_Date)
  FROM   Interest_Income_Rates
  WHERE  First_Payment_Date > P_Date
  AND    Fri_Ticker         = p_Fri_Ticker
  ;
  CURSOR Cur_First_Payment_Date    IS
  SELECT First_Payment_Date
  FROM   Interest_Income_Rates
  WHERE  Fri_Ticker = p_Fri_Ticker
  AND    First_Payment_Date BETWEEN (V_Next_Payment_Date - 15)
                                AND (V_Next_Payment_Date + 15)
  ;
  CURSOR Cur_Is_There_An_Entry     IS
  SELECT 'X'
  FROM   Interest_Schedule
  WHERE  Fri_Ticker = p_Fri_Ticker
  ;
  r_is_there_an_entry  Cur_Is_There_An_Entry%rowtype;

  CURSOR Cur_Get_Activity
       ( I_Fri_Ticker Securities.Fri_Ticker%TYPE
       ) IS
  SELECT S.Trading_Status_Code
       , S.Date_Of_Status
       , T.Active_Flag
    FROM Securities S
       , Trading_Status T
   WHERE S.Fri_Ticker = I_Fri_Ticker
     AND T.Code       = S.Trading_Status_Code;
  R_Get_Activity     Cur_Get_Activity%ROWTYPE;

  procedure close_all_cursors is
  begin
    if Cur_Is_There_An_Entry%isopen then
      close Cur_Is_There_An_Entry;
    end if;
    if Cur_First_Payment_Date%isopen then
      close Cur_First_Payment_Date;
    end if;
    if Cur_Min_First_Payment_Date%isopen then
      close Cur_Min_First_Payment_Date;
    end if;
    if Cur_Max_First_Payment_Date%isopen then
      close Cur_Max_First_Payment_Date;
    end if;
    if Cur_Max_Maturity_Date%isopen then
      close Cur_Max_Maturity_Date;
    end if;
    if Cur_Min_Issue_Date%isopen then
      close Cur_Min_Issue_Date;
    end if;
    if Cur_Mat_Date%isopen then
      close Cur_Mat_Date;
    end if;
    if Cur_I_Calc_Parms%isopen then
      close Cur_I_Calc_Parms;
    end if;
    if Cur_Get_Activity%isopen then
      close Cur_Get_Activity;
    end if;
  end;

begin
  p_success := false;

  p_affected := false;
  Pgmloc := 1290;
  OPEN  Cur_Get_Activity( P_Fri_Ticker );
  Pgmloc := 1300;
  FETCH Cur_Get_Activity INTO R_Get_Activity;
  Pgmloc := 1310;
  CLOSE Cur_Get_Activity;
  --
  Pgmloc := 1320;
  IF R_Get_Activity.Active_Flag = 'Y'
  THEN
    V_Stop_Date := add_months(Constants.Get_Site_Date,C_Max_Months_In_Future);
  ELSE
    V_Stop_Date := R_Get_Activity.Date_Of_Status + 1;
  END IF;
  --
  Pgmloc    := 1330;
  OPEN Cur_I_Calc_Parms;
  Pgmloc := 1340;
  FETCH Cur_I_Calc_Parms INTO r_i_calc_parms;
  IF Cur_I_Calc_Parms%NOTFOUND
  THEN
     --
     --  Write messages into a file for those securities without
     --  interest calc parms records.
     --
     V_Message := 'No Interest Calc Parms found '||
                  'for FRI ID: ' || Security.Get_Fri_ID (P_Fri_Ticker);
     Pgmloc := 1350;
     CLOSE Cur_I_Calc_Parms;
     Pgmloc := 1360;
     manage_utl_file.write_file( p_log_file
                               , V_Message
                               , v_success
                               , pricing_msgs_rec.err_text
                               );
     if not v_success then raise e_program_error; end if;

     count_line_processed(c_cnt_sch_ski_no_calc, pricing_msgs_rec, 1);
     GOTO Kignore;
     --
     Pgmloc    := 1370;
  end if;
  Pgmloc := 1380;
  CLOSE Cur_I_Calc_Parms;

  -- G_New_Count is TRUE when the procedure is called
  -- from GENINT, but will be FALSE during the generation
  -- of interest schedule of the 6th.
  Pgmloc := 1390;
  IF r_i_calc_parms.Generate_Int_Sch_Flag = 'Y'
  OR g_new_count
  THEN
    NULL;
  ELSE
    V_Message := 'Int_Sch_Flag <> Y '||
                'for FRI ID: ' || Security.Get_Fri_ID (P_Fri_Ticker);
    Pgmloc := 1400;
    manage_utl_file.write_file( p_log_file
                             , V_Message
                             , v_success
                             , pricing_msgs_rec.err_text
                             );
    if not v_success then raise e_program_error; end if;

    count_line_processed(c_cnt_sch_ski_no_int_sch_flag, pricing_msgs_rec, 1);
    GOTO Kignore;
  END IF;
  --
  --
  -- Get maturity date for that security.
  --
  Pgmloc    := 1410;
  OPEN Cur_Mat_Date;
  Pgmloc    := 1420;
  FETCH Cur_Mat_Date INTO r_mat_date;
  --
  Pgmloc    := 1430;
  IF Cur_Mat_Date%NOTFOUND THEN
     V_Message := 'No Maturity Date Found ' ||
                 'for FRI ID: ' || Security.Get_Fri_ID (P_Fri_Ticker);
     Pgmloc  := 1440;
     CLOSE Cur_Mat_Date;
     Pgmloc  := 1450;
     manage_utl_file.write_file( p_log_file
                               , V_Message
                               , v_success
                               , pricing_msgs_rec.err_text
                               );
    if not v_success then raise e_program_error; end if;
    count_line_processed(c_cnt_sch_ski_no_mat_dt, pricing_msgs_rec, 1);
    GOTO Kignore;
  END IF;
  Pgmloc    := 1460;
  CLOSE Cur_Mat_Date;
  Pgmloc    := 1470;


  -- Verifying the maturity_date is missing
  -- Richard (05 Juin 2001)
  IF r_mat_date.Mat_Date <= p_Income_Date
  THEN
    V_Message := 'Income date ['||p_Income_Date||']'
       ||' not before maturity date ['||r_mat_date.Mat_Date||']'
       ||' for FRI ID: ' || Security.Get_Fri_ID (P_Fri_Ticker);
    Pgmloc  := 1480;
    manage_utl_file.write_file( p_log_file
                              , V_Message
                              , v_success
                              , pricing_msgs_rec.err_text
                              );
    if not v_success then raise e_program_error; end if;

    count_line_processed(c_cnt_sch_ski_in_dt_aft_mat_dt, pricing_msgs_rec, 1);

    GOTO Kignore;
  END IF;
  --


  -- Ajout - JSL - 07 septembre 2010
  Pgmloc := 1490;
  OPEN Cur_Min_Issue_Date( p_Fri_Ticker );
  Pgmloc := 1500;
  V_Min_Issue_Date := NULL;
  FETCH Cur_Min_Issue_Date INTO V_Min_Issue_Date;
  Pgmloc := 1510;
  CLOSE Cur_Min_Issue_Date;
  V_Min_Issue_Date := NVL( V_Min_Issue_Date, p_Min_Date_To_Keep );


  --
  Pgmloc := 1520;
  OPEN  Cur_Max_Maturity_Date( p_Fri_Ticker );
  Pgmloc := 1530;
  V_Max_Maturity_Date := NULL;
  FETCH Cur_Max_Maturity_Date INTO V_Max_Maturity_Date;
  Pgmloc := 1540;
  CLOSE Cur_Max_Maturity_Date;
  V_Max_Maturity_Date := NVL( V_Max_Maturity_Date, p_Max_Date );


  --mlz - mar2011
  v_min_date := nvl(p_Min_Date_To_Keep, v_min_issue_date);
  v_max_date := nvl(p_max_date, V_Max_Maturity_Date);
  --end mlz


  -- Fin d'ajout - JSL - 07 septembre 2010
  --
  -- Delete All rows
  -- Richard (04 Juin 2001)
  --
  -- Not anymore (Richard (09Nov2009)
  -- DELETE Interest_Schedule
  -- WHERE  Fri_Ticker  =  V_Fri_Ticker;
  --
  -- IF SQL%ROWCOUNT > 0
  -- THEN
  --   V_Affected := True;
  -- END IF;
  -- V_Count_Int_Sch_Del := V_Count_Int_Sch_Del + SQL%ROWCOUNT;
  -- END Of Fix (Richard (09Nov2009)
  --

  --mlz
  --G_Value_02 := V_Count_Int_Sch_Del;
  -- pricing_msgs_rec.value_02 := 0;
  --endmlz


  --
  Pgmloc    := 1550;
  OPEN Cur_Max_First_Payment_Date(v_min_date);
  Pgmloc    := 1560;
  v_payment_date := NULL;
  FETCH Cur_Max_First_Payment_Date  INTO V_Payment_Date;
  Pgmloc    := 1570;
  CLOSE Cur_Max_First_Payment_Date;

  --
  Pgmloc  := 1580;
  IF V_Payment_Date IS NULL
  THEN
    Pgmloc    := 1590;
    OPEN Cur_Min_First_Payment_Date(v_min_date);
    Pgmloc    := 1600;
    FETCH Cur_Min_First_Payment_Date INTO V_Payment_Date;
    Pgmloc    := 1610;
    CLOSE Cur_Min_First_Payment_Date;
  END IF;


  Pgmloc    := 1620;
  IF  V_Payment_Date IS NULL  THEN
     V_Message := 'No Income Rates found '||
                 'for FRI ID: ' || Security.Get_Fri_ID (P_Fri_Ticker);
     Pgmloc := 1630;
     manage_utl_file.write_file( p_log_file
                               , V_Message
                               , v_success
                               , pricing_msgs_rec.err_text
                               );
    if not v_success then raise e_program_error; end if;


     count_line_processed(c_cnt_sch_ski_no_rates_found, pricing_msgs_rec, 1);

     GOTO Kignore;
  END IF;

  --
  IF V_Payment_Date > v_Max_Date
  THEN
     V_Message := 'V_Payment_Date ['||V_Payment_Date||']'
       ||' after max_date ['||v_Max_Date||']'
       ||' for FRI ID: ' || Security.Get_Fri_ID (P_Fri_Ticker);
     Pgmloc := 1640;
     manage_utl_file.write_file( p_log_file
                               , V_Message
                               , v_success
                               , pricing_msgs_rec.err_text
                               );
    if not v_success then raise e_program_error; end if;
    count_line_processed(c_cnt_sch_ski_pay_dt_gt_max_dt, pricing_msgs_rec, 1);
    GOTO Kignore;
  END IF;
  --
  Pgmloc    := 1650;
  IF V_Payment_Date between
    v_min_date And v_Max_Date
  -- Ajout - JSL - 07 septembre 2010
  AND V_Payment_Date >= V_Min_Issue_Date
  AND V_Payment_Date <= V_Max_Maturity_Date
  -- Fin d'ajout - JSL - 07 septembre 2010
  THEN
    -- Ajout - JSL - 13 mai 2011
    IF V_Payment_Date > '01-jan-1991'
    THEN
    -- Fin d'ajout - JSL - 13 mai 2011
    -- Only if it does not exists (Richard (09Nov2009)
      BEGIN
        Pgmloc    := 1660;
        INSERT INTO Interest_Schedule
              (Fri_Ticker,
               Income_Date,
               Data_Supplier_Code,
               Payment_Flag,
               Last_User,
               Last_Stamp)
        VALUES (p_Fri_Ticker,
               V_Payment_Date,
               C_STP,
               'Y',
               G_User,
               Sysdate);
        IF SQL%ROWCOUNT > 0
        THEN
          p_Affected := True;
        END IF;
        count_line_processed(c_cnt_sch_ins, pricing_msgs_rec, SQL%ROWCOUNT);
        --
      EXCEPTION
         WHEN DUP_VAL_ON_INDEX THEN NULL;
      END;
    -- Ajout - JSL - 13 mai 2011
    END IF; -- V_Payment_Date > '01-jan-1991'
    -- Fin d'ajout - JSL - 13 mai 2011
   --
  End If;




  --
  -- Generating the next payment date:
  --
  Pgmloc := 1670;
  V_Day  := TO_CHAR(V_Payment_Date, 'DD');
  --
  -- Keep the payment day the same day of the month.If the day is
  -- greater than the last day of that month, get the last day of
  -- the month.
  --
  Loop
    --
    -- After discussion with Richard, Stop producing the schedule if
    -- more than 10 years from now, for all securities, including perpetual.
    exit when v_payment_Date >= V_Stop_Date;
    Pgmloc    := 1680;
    V_Month_Year := TO_CHAR(ADD_MONTHS(V_Payment_Date,
                        (12 / p_Inc_Freq_Value)),'MON-YYYY');
    BEGIN
      Pgmloc    := 1690;
      V_Next_Payment_Date := TO_DATE(V_Day||'-'||V_Month_Year,
                                      'DD-MON-YYYY');
    EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE = -1839
      THEN
        V_Next_Payment_Date :=
          ADD_MONTHS(LAST_DAY(V_Payment_Date) ,(12 / p_Inc_Freq_Value));
      ELSE
        V_Message := 'Error: Fri_ID='  || Security.Get_Fri_ID (P_Fri_Ticker) ||
                   ' - Error Code: ' || to_char(Sqlcode);
       manage_utl_file.write_file( p_log_file
                                 , V_Message
                                 , v_success
                                 , pricing_msgs_rec.err_text
                                 );
       RAISE;
      END IF;
    END;
    --
    -- Get the last day of the month if end of month flag is Y.
    --
    Pgmloc    := 1700;
    IF r_i_calc_parms.End_Of_Month_Flag = 'Y'
    THEN
       V_Next_Payment_Date := Last_Day(V_Next_Payment_Date);
    END IF;
    --
    -- Setting the Next payment date to the maturity date when
    -- the next payment date is within 15 days of the mat date
    --
    IF V_Next_Payment_Date BETWEEN (r_mat_date.Mat_Date - 15)
                           AND     (r_mat_date.Mat_Date + 15)
    THEN
      V_Next_Payment_Date := r_mat_date.Mat_Date;
    END IF;
    --
    Pgmloc  := 1710;
    OPEN Cur_First_Payment_Date;
    Pgmloc    := 1720;
    FETCH  Cur_First_Payment_Date INTO V_First_Payment_Date;
    --
    IF Cur_First_Payment_Date%FOUND  THEN
       V_Next_Payment_Date := V_First_Payment_Date;
    END IF;
    Pgmloc    := 1730;
    CLOSE Cur_First_Payment_Date;
    --
    -- When the new payment date is later than the maximum date,
    -- then processing that security is stopped.
    --
    IF V_Next_Payment_Date > v_Max_Date
    OR V_Next_Payment_Date > r_mat_date.Mat_Date
    OR V_Next_Payment_Date > V_Stop_Date
    THEN
      GOTO End_Loop;
    END IF;
    --
    Pgmloc    := 1740;
    IF V_Next_Payment_Date < v_Min_Date
    -- Ajout - JSL - 07 septembre 2010
    OR V_Next_Payment_Date < V_Min_Issue_Date
    OR V_Next_Payment_Date > V_Max_Maturity_Date
    -- Fin d'ajout - JSL - 07 septembre 2010
    THEN
      GOTO Next_Loop;
    END IF;
    --
    Pgmloc    := 1750;
    -- Inserting into interest_schedule using the generated date
    -- as the income date.
    --
    -- Ajout - JSL - 13 mai 2011
    IF V_Payment_Date > '01-jan-1991'
    THEN
    -- Fin d'ajout - JSL - 13 mai 2011
      BEGIN
        Pgmloc    := 1760;
        INSERT INTO Interest_Schedule
        (
          Fri_Ticker,
          Income_Date,
          Data_Supplier_Code,
          Payment_Flag,
          Last_User,
          Last_Stamp
        )
        VALUES
        (
          p_Fri_Ticker,
          V_Next_Payment_Date,
          C_STP,
          'Y',
          G_User,
          Sysdate
        );
        --
        IF SQL%ROWCOUNT > 0
        THEN
          p_Affected := True;
        END IF;
        --
        count_line_processed(c_cnt_sch_Ins, pricing_msgs_rec, SQL%ROWCOUNT);
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN NULL;
      END;
    -- Ajout - JSL - 13 mai 2011
    END IF; -- V_Payment_Date > '01-jan-1991'
    -- Fin d'ajout - JSL - 13 mai 2011
<<Next_Loop >>
    Pgmloc    := 1770;
    V_Payment_Date := V_Next_Payment_Date;
    IF V_Payment_Date >= r_mat_date.Mat_Date
    THEN
      GOTO End_Loop;
    END IF;
  END Loop;
  --
<< End_Loop >>
  --
  Pgmloc    := 1780;
  OPEN Cur_Is_There_An_Entry;
  Pgmloc    := 1790;
  FETCH Cur_Is_There_An_Entry INTO r_Is_There_An_Entry;
  --
  Pgmloc    := 1800;
  IF Cur_Is_There_An_Entry%FOUND THEN
     Pgmloc    := 1810;
     UPDATE Interest_Calc_Parms
     SET    Interest_Schedule_Flag = 'Y',
            Last_User  =  G_User,
            Last_Stamp =  SYSDATE
     WHERE  Fri_Ticker = p_Fri_Ticker
     AND    Interest_Schedule_Flag = 'N';
  ELSE
     Pgmloc    := 1820;
     UPDATE Interest_Calc_Parms
     SET    Interest_Schedule_Flag = 'N',
            Last_User  =  G_User,
            Last_Stamp =  SYSDATE
     WHERE  Fri_Ticker = p_Fri_Ticker
     AND    Interest_Schedule_Flag = 'Y';
  END IF;
  --
  IF SQL%ROWCOUNT > 0
  THEN
     p_Affected := True;
     count_line_processed(c_cnt_sch_calc_parms, pricing_msgs_rec, SQL%ROWCOUNT);
  END IF;
  Pgmloc    := 1830;
  CLOSE Cur_Is_There_An_Entry;

<<kignore>>
  p_success := true;

exception
  when e_program_error then
    if pricing_msgs_rec.err_text is null then
      pricing_msgs_rec.err_text := v_message;
    end if;
    pricing_msgs_rec.err_text := substr(
      pricing_msgs_rec.err_text||' '||$$plsql_unit||'@'||pgmloc
      ,1,255);
    close_all_cursors;
    p_success := false;

  when others then
    pricing_msgs_rec.err_text := substr(
      'Err@'||pgmloc||' '
      ||'Fri_Ticker : '''||P_Fri_Ticker||''' '
      ||dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      ,1,255);
    close_all_cursors;
    p_success := false;
end;


------------------------------------------------------------------------
-- This procedure is to generate the interest payment schedule according
-- to the security definition. Only regular payments are generated.
-- It also opens a file in the given directory and writes the messages
-- for some of those securities which haven't been processed.
--
-- Program ID can be Null
-- Richard - 03 may 2001
--
PROCEDURE Interest_Payment_Schedule
                    (P_Program_Id       IN  Ops_Scripts.Program_Id%TYPE,
                     P_Number_Of_Months IN  NUMBER,
                     P_Dir_Name         IN  VARCHAR2,
                     P_Fri_Ticker       IN  Securities.Fri_Ticker%TYPE,
                     P_Commit_Count     IN  NUMBER,
                     P_Success          OUT Yes_No.Code%TYPE,
                     P_Message          OUT VARCHAR2)

AS
--
 c_Message_Code    constant VARCHAR2(30) := 'PROC-GEN_INT_PAYMENTS-';
 Pricing_Msgs_Rec  Pricing_Msgs%ROWTYPE;
 v_success         boolean;
 h_log_file                     Utl_File.File_Type;
 V_Record                       VARCHAR2(200);
 V_File_Name                    Ops_Scripts.Script_Name%TYPE;
 V_Dir_Name                     Ops_Scripts.Script_Name%TYPE;
 V_Fri_ID                       Tickers.Ticker%TYPE;
 -- On a TEST Database, it may not work
 -- Must be initialized to Site Date which make more sense
 -- Richard (24 Nov 2007)
 V_Income_Date                  DATE   := Constants.Get_Site_Date;
 V_System_Date                  DATE   := SYSDATE;
 c_Min_Months         constant  NUMBER := 12;
--
 V_Max_Date                     DATE;
 V_Min_Date_To_Keep             DATE;
 V_Num_Of_Months                NUMBER;

 V_Count_Securities             NUMBER := 0;
 V_Max_Inc_Date                 DATE;

 NUM_OF_MONTHS_OUT_OF_RANGE     EXCEPTION;
-- Modification - JSL - 07 septembre 2010
-- V_Message                      VARCHAR2(200);
  v_message                      pricing_msgs.err_text%type;
-- Fin de modification - JSL - 07 septembre 2010
 V_Commit                       NUMBER := 0;
 V_Affected                     Boolean;

--
CURSOR Cur_Get_Sec  IS
SELECT s.Fri_Ticker ,
       s.Income_Frequency_Value as Inc_Freq_Value,
       f.Code                   as Freq_Code,
       f.Annual_Validity_Flag   as Annual_Valid_Flag,
       ts.Active_Flag
FROM   Securities                s,
       Trading_Status            ts,
       Frequencies               f
WHERE  s.Fri_Ticker            = DECODE(P_Fri_Ticker, Null,
                                        s.Fri_Ticker, P_FRI_Ticker)
AND    s.Trading_Status_Code    = ts.Code
AND    s.Income_Frequency_Value = f.Value
AND    s.Income_Type_Code       = 'I'
AND    ts.Active_Flag           = 'Y'
AND    f.Annual_Validity_Flag   = 'Y'
Order by s.Fri_Ticker
;
r_get_sec  cur_get_sec%rowtype;
--
--
CURSOR Cur_Max_Inc_Date( i_fri_ticker securities.fri_ticker%type)     IS
SELECT Max(Income_Date)
FROM   Interest_Schedule
WHERE  Fri_Ticker = i_Fri_Ticker
AND    Data_Supplier_Code = C_STP;
--
--
--
BEGIN

  --
  Pgmloc         := 1840;
  P_Success      := 'N';
  --
  V_FRI_ID       := 'out';
  IF P_Fri_Ticker is not null
  THEN
   V_Fri_ID := Security.Get_Fri_ID (P_Fri_Ticker);
  END IF;
  --
  V_File_Name    := 'gen_int_sch_' || V_FRI_ID        || '.log';

  Pgmloc    := 1850;
  manage_utl_file.open_file(v_file_name, 'W', h_log_file, v_success, v_message);
  if not v_success then raise e_program_error; end if;
  --


  Pgmloc  := 1860;
  --
  V_Num_Of_Months := P_Number_of_Months;
  IF V_Num_Of_Months < c_Min_Months
  THEN
   V_Num_Of_Months := c_Min_Months;
  END IF;

  --
  -- The number of months over which the payments are generated
  -- must be between 1 and 150.
  --
  IF V_Num_Of_Months NOT BETWEEN 1 AND 150
  THEN
   RAISE NUM_OF_MONTHS_OUT_OF_RANGE;
  END IF;
  --
  V_Max_Date          := ADD_MONTHS(V_Income_Date, V_Num_Of_Months);
  V_Min_Date_To_Keep  := ADD_MONTHS(Trunc(V_Income_Date), - V_Num_of_Months);

  Pgmloc  := 1870;
  --
  -- Initialise Pricing_Msgs.
  --
  -- Program ID could be Null
  -- Richard (03 may 2001)
  --
  IF P_Program_ID is not null
  THEN

    Pgmloc    := 1880;
    Global_File_Updates.Initialize_Pricing_Msgs
        (P_Program_Id,
         V_System_Date,
         FALSE,
         Pricing_Msgs_Rec);
    --
    -- Update Pricing_Msgs.
    --
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
    Pricing_Msgs_Rec.Msg_Text   := 'Generating the Interest ' ||
                                  'Payment Schedule.';
    Pricing_Msgs_Rec.Msg_Text_2 := 'In progress..';
    Pricing_Msgs_Rec.Value_01   :=  0;
    Pricing_Msgs_Rec.Value_02   :=  0;
    Pricing_Msgs_Rec.Value_03   :=  0;
    Pricing_Msgs_Rec.Value_04   :=  0;
    --
    Pgmloc    := 1890;

    Global_File_Updates.Update_Pricing_Msgs
        (P_Program_Id,
         V_System_Date,
         FALSE,
         Pricing_Msgs_Rec);
    Pgmloc    := 1900;
    --
    IF P_Commit_Count > 0   THEN
       COMMIT;
    END IF;
    --
  END IF;


  --
  -- Get the securities
  --
  Pgmloc   := 1910;
  V_Commit := 0;

  OPEN Cur_Get_Sec;
  LOOP
    Pgmloc    := 1920;
    FETCH Cur_Get_Sec INTO r_get_sec ;
    EXIT WHEN Cur_Get_Sec%NOTFOUND;

    Pgmloc    := 1930;
    V_Affected := False;
    V_Fri_ID := Security.Get_Fri_ID ( r_get_sec.Fri_Ticker);

    --
    -- The inactive securities are not processed.
    --
    IF r_get_sec.Active_Flag <>'Y' THEN
       GOTO Kignore;
    END IF;
    --
    Pgmloc   := 1940;
    IF nvl( r_get_sec.Inc_Freq_Value,   999) NOT BETWEEN 1 AND 12
      OR NVL(r_get_sec.Annual_Valid_Flag,'N') <> 'Y'
    THEN
      Pgmloc    := 1950;
      V_Record := 'Frequency ' || r_get_sec.Freq_Code || ' cannot be'||
                  ' processed for FRI ID: '   || V_Fri_ID;
      Utl_File.Put_Line(h_log_file, V_Record);
      Utl_File.Fflush(h_log_file);
      GOTO Kignore;
      --
    END IF;
    --
    -- Program ID could be Null
    -- Richard (03 may 2001)
    --
    Pgmloc   := 1960;
    IF P_Program_ID is not null
    THEN
      Pricing_Msgs_Rec.Msg_Text_2       :=
        'Processing Fri_Ticker: ' || r_get_sec.Fri_Ticker;

      Pgmloc    := 1970;
      Global_File_Updates.Update_Pricing_Msgs
          (P_Program_Id,
           V_System_Date,
           FALSE,
           Pricing_Msgs_Rec);
      --
    END IF;
    --
    Pgmloc   := 1980;
    IF P_Commit_Count >  0 AND
      V_Commit       >= P_Commit_Count
    THEN
      Commit;
      V_Commit := 0;
    END IF;
    --

    Pgmloc    := 1990;
    generate_int_sch_for_one( r_get_sec.fri_ticker
                            , h_log_file
                            , v_income_date
                            , r_get_sec.inc_freq_value
                            , v_min_date_to_keep
                            , v_max_date
                            , v_affected
                            , Pricing_Msgs_Rec
                            , v_success
                            );
    if not v_success then raise e_program_error; end if;
    Pgmloc    := 2000;
    --
<<Kignore>>
    --
    V_Commit:= V_Commit+ 1;
    --
    If V_Affected
    Then
      count_line_processed(c_cnt_sch_sec_affected, pricing_msgs_rec, 1);
    END IF;
    --
    IF P_Program_ID IS NOT NULL THEN
      --
      -- Program ID could be Null
      -- Richard (03 may 2001)
      --
      Pgmloc    := 2010;
      Pricing_Msgs_Rec.Msg_Text_2 := 'Program in Progress.';
      --
      Pgmloc    := 2020;
      Global_File_Updates.Update_Pricing_Msgs
        (P_Program_Id,
         V_System_Date,
         FALSE,
         Pricing_Msgs_Rec);
      --
    END IF;
  END LOOP;
  Pgmloc    := 2030;
  CLOSE Cur_Get_Sec;
  --
  Pgmloc    := 2040;
  Utl_File.Fclose(h_log_file);



  --
  -- Updating the Pricing_Msgs after the program is completed.
  --
  -- Program ID could be Null
  -- Richard (03 may 2001)
  --
  IF P_Program_ID is not null
  THEN
    Pgmloc    := 2050;
    --
    Pricing_Msgs_Rec.Successfull_Flag := 'Y';
    Pricing_Msgs_Rec.Msg_Text_2 := 'Program Successfully Completed.';
    Pricing_Msgs_Rec.Remarks    := 'Securities not processed in: '||
                                              V_File_Name;
    --
    Pgmloc    := 2060;
    Global_File_Updates.Update_Pricing_Msgs
         (P_Program_Id,
          V_System_Date,
          FALSE,
          Pricing_Msgs_Rec);
    --
    IF P_Commit_Count >  0  THEN
      COMMIT;
    END IF;
    --
  END IF;
  P_Success := 'Y';
  --
EXCEPTION
  WHEN e_program_error then
    --Handle_Exceptions(pricing_msgs_rec.err_text, Pricing_Msgs_Rec, V_Message);
    if pricing_msgs_rec.err_text is null then
      pricing_msgs_rec.err_text := v_message;
    end if;
    IF P_Program_ID is not null
    THEN
      global_file_updates.update_pricing_msgs( p_program_id
                                             , pricing_msgs_rec.date_of_msg
                                             , false
                                             , pricing_msgs_rec
                                             );
    end if;
    if Cur_Get_Sec%isopen then
      close Cur_Get_Sec;
    end if;
    if utl_file.is_open(h_log_file) then
      utl_file.fclose(h_log_file);
    end if;

    P_Message := pricing_msgs_rec.err_text;
    P_Success := 'N';

  WHEN OTHERS THEN
    pricing_msgs_rec.err_text := substr(
      'err@'||pgmloc||' '
      ||dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      ,1,255);
    IF P_Program_ID is not null
    THEN
      global_file_updates.update_pricing_msgs( p_program_id
                                             , pricing_msgs_rec.date_of_msg
                                             , false
                                             , pricing_msgs_rec
                                             );
    end if;

    if Cur_Get_Sec%isopen then
      close Cur_Get_Sec;
    end if;
    if utl_file.is_open(h_log_file) then
      utl_file.fclose(h_log_file);
    end if;

    P_Message := pricing_msgs_rec.err_text;
    P_Success := 'N';

END Interest_Payment_Schedule;




--------------------------------------------------------------------------------
-- log file opened by calling procedure, param: p_cntl_rec.log_file
-- log file closed by calling procedure !!!
PROCEDURE interest_payment_sch_to_recalc
        ( P_Program_Id    IN     Ops_Scripts.Program_Id%TYPE
        , P_Commit_Count  IN     NUMBER
        , P_Cntl_Rec      IN OUT NOCOPY Product_Component.R_Parameters
        , P_Record_Read      OUT INTEGER
        , P_Success          OUT Yes_No.Code%TYPE
        , P_Message          OUT VARCHAR2
        )

AS
--
  c_Message_Code    constant VARCHAR2(30) := 'PROC-GEN_INT_PAYMENTS-';
  Pricing_Msgs_Rec  Pricing_Msgs%ROWTYPE;
  v_success         boolean;
  V_Fri_ID                       Tickers.Ticker%TYPE;
  Previous_Fri_Ticker            Securities.Fri_Ticker%TYPE;

  V_System_Date                  DATE   := SYSDATE;
--
  V_Max_Date                     DATE;
  V_Min_Date_To_Keep             DATE;
  V_Num_Of_Months                NUMBER;

  V_Count_Securities             NUMBER := 0;
  V_Max_Inc_Date                 DATE;

  NUM_OF_MONTHS_OUT_OF_RANGE     EXCEPTION;
  v_message                      pricing_msgs.err_text%type;
  V_Sql_Msg                      VARCHAR2(255);
  V_Commit                       NUMBER := 0;
  V_Affected                     Boolean;
  v_deleted_rowcount             number;
  V_P_to_R                       Prices_to_Recalc%ROWTYPE;

--

  CURSOR Cur_Securities_To_Recalc
       ( I_Cutoff_Date DATE
       ) IS
  SELECT R.*
       , R.Rowid
    FROM Prices_to_Recalc R
   WHERE Recalc_Type_Code = c_genint
     AND Updated_Date IS NULL
     AND Effective_Date = (
         SELECT MIN( Effective_Date )
           FROM Prices_to_Recalc R1
          WHERE R1.Fri_Ticker = R.Fri_Ticker
            AND R1.Recalc_Type_Code = R.Recalc_Type_Code
            AND R1.Updated_Date IS NULL
         )
     AND Effective_Date >= I_Cutoff_Date
  ORDER BY Last_Stamp
  ;

  CURSOR Cur_Get_Sec( i_fri_ticker securities.fri_ticker%type )  IS
  SELECT s.Fri_Ticker ,
         s.Income_Frequency_Value as Inc_Freq_Value,
         f.Code                   as Freq_Code,
         f.Annual_Validity_Flag   as Annual_Valid_Flag,
         ts.Active_Flag,
         s.security_type_code,
         Indicated_Annual_Rate,
         Income_Frequency_Value,
         primary_currency,
         Income_Type_Code
  FROM   Securities                s,
         Trading_Status            ts,
         Frequencies               f
  WHERE  s.Fri_Ticker            = i_fri_ticker
  AND    s.Trading_Status_Code    = ts.Code
  AND    s.Income_Frequency_Value = f.Value
  ;
  r_get_sec  cur_get_sec%rowtype;

  procedure close_all_cursors is
  begin
    if Cur_Get_Sec%isopen then
      close Cur_Get_Sec;
    end if;

  end;
BEGIN
  P_Record_Read := 0;

  --
  Pgmloc         := 2070;
  P_Success      := 'N';

  --interest_payment_schedule only counts 01 --> 04
  --> to limit common procedure (generate_int_sch_for_one) counts,
  --- g_new_count is false by default
  g_new_count    := true;


  --
  --
  -- Initialise Pricing_Msgs.
  --
  Pgmloc  := 2080;
  Global_File_Updates.Initialize_Pricing_Msgs
      (P_Program_Id,
       V_System_Date,
       FALSE,
       Pricing_Msgs_Rec);
  --
  -- Update Pricing_Msgs.
  --
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
  Pricing_Msgs_Rec.Msg_Text   := 'Generating the Interest ' ||
                                'Payment Schedule.';
  Pricing_Msgs_Rec.Msg_Text_2 := 'In progress..';

  Pricing_Msgs_Rec.Value_01   :=  0;
  Pricing_Msgs_Rec.Value_02   :=  0;
  Pricing_Msgs_Rec.Value_03   :=  0;
  Pricing_Msgs_Rec.Value_04   :=  0;
  Pricing_Msgs_Rec.Value_05   :=  0;
  Pricing_Msgs_Rec.Value_06   :=  0;
  Pricing_Msgs_Rec.Value_07   :=  0;
  Pricing_Msgs_Rec.Value_08   :=  0;
  Pricing_Msgs_Rec.Value_09   :=  0;
  Pricing_Msgs_Rec.Value_10   :=  0;
  Pricing_Msgs_Rec.Value_11   :=  0;
  Pricing_Msgs_Rec.Value_12   :=  0;
  --
  Pgmloc    := 2090;
  Global_File_Updates.Update_Pricing_Msgs
      (P_Program_Id,
       V_System_Date,
       FALSE,
       Pricing_Msgs_Rec);
  Pgmloc    := 2100;
  --
  COMMIT;
  --


  --fake savepoint, only to have it existing for the exception handler...
  --  real one in cursor loop below
  Pgmloc         := 2110;
  savepoint a;


  Pgmloc         := 2120;
  Load_supplier_Rules.Write_file( p_cntl_rec.log_file
                                , c_line||chr(10)||Pricing_Msgs_Rec.Msg_Text
                                , Pricing_Msgs_Rec
                                , v_Success
                                );
  if not v_success then
    pricing_msgs_rec.remarks := pricing_msgs_rec.err_text;
    raise e_program_error; ------>>>>
  end if;



  --
  -- Get the securities
  --
  Pgmloc   := 2130;
  V_Commit := 0;
  Previous_Fri_Ticker := NULL;

  Pgmloc         := 2140;
  FOR STR_Rec IN Cur_Securities_To_Recalc( G_Prc_Cutoff_Date )
  LOOP --{

    Pgmloc         := 2150;
    P_Record_Read := P_Record_Read + 1;
    count_line_processed(c_cnt_genint_read, pricing_msgs_rec, 1);

    Pgmloc   := 2160;
    IF P_Commit_Count >  0 AND V_Commit >= P_Commit_Count
    THEN
     Commit;
     V_Commit := 0;
    END IF;

    Pgmloc         := 2170;
    savepoint a;

    IF STR_Rec.Fri_Ticker = Previous_Fri_Ticker
    THEN
      count_line_processed(c_cnt_genint_already_done, pricing_msgs_rec, 1);
      GOTO Kignore; ------>>>>
    END IF;
    Pgmloc := 2180;
    V_P_to_R.Completed_Date := NULL;
    BEGIN
      SELECT *
        INTO V_P_to_R
        FROM Prices_To_Recalc
       WHERE Rowid = STR_Rec.Rowid
             ;
    EXCEPTION
    WHEN No_Data_Found
    THEN
      NULL;
    END;
    IF V_P_to_R.Completed_Date IS NOT NULL
    THEN
      count_line_processed(c_cnt_genint_already_done, pricing_msgs_rec, 1);
      GOTO KIgnore;
    END IF;

    Previous_Fri_Ticker := STR_Rec.Fri_Ticker;


    Pgmloc         := 2190;
    open Cur_Get_Sec( STR_Rec.fri_ticker );
    Pgmloc         := 2200;
    FETCH Cur_Get_Sec INTO r_get_sec;
    Pgmloc         := 2210;
    if Cur_Get_Sec%notfound then

      v_message := 'Security / trading status not found for fri_ticker: '
                   ||STR_Rec.fri_ticker||' ???';
      Pgmloc         := 2220;
      manage_utl_file.write_file
                    ( p_cntl_rec.log_file
                    , v_message
                    , v_success
                    , pricing_msgs_rec.err_Text
                    );
      if not v_success then
        close Cur_Get_Sec;
        pricing_msgs_rec.remarks := pricing_msgs_rec.err_Text;
        raise e_program_error;
      end if;

--    --set back real error msg
--      pricing_msgs_rec.err_Text := v_message;
--
--      raise e_program_error; ------>>>>
      Pgmloc         := 2230;
      Close Cur_Get_Sec;
      GOTO KIgnore;
    end if;
    Pgmloc         := 2240;
    close Cur_Get_Sec;

    V_Affected := False;
    V_Fri_ID := Security.Get_Fri_ID ( r_get_sec.Fri_Ticker);

    --
    -- The inactive securities are not processed.
    -- Even if the security is now inactive, we still have to
    -- regenerate the interest schedule.
    --
--  IF r_get_sec.Active_Flag <>'Y' THEN
--    count_line_processed(c_cnt_sch_ski_inactive, pricing_msgs_rec, 1);
--    GOTO Kignore;    ------>>>>
--  END IF;


    Pgmloc   := 2250;
    IF nvl( r_get_sec.Inc_Freq_Value,   999) NOT BETWEEN 1 AND 12
      OR NVL(r_get_sec.Annual_Valid_Flag,'N') <> 'Y'
    THEN
      v_message := 'Frequency ' || r_get_sec.Freq_Code || ' cannot be'||
                  ' processed for FRI ID: '   || V_Fri_ID;
      Pgmloc         := 2260;
      manage_utl_file.write_file( p_cntl_rec.log_file
                                , v_message
                                , v_success
                                , pricing_msgs_rec.err_text
                                );
      if not v_success then raise e_program_error; end if;

      count_line_processed(c_cnt_sch_ski_wrong_freq, pricing_msgs_rec, 1);

      GOTO Mark_Done; ------>>>>
      --
    END IF;


    Pgmloc   := 2270;
    IF P_Program_ID is not null
    THEN
      Pricing_Msgs_Rec.Msg_Text_2 := 'Processing Fri_Ticker: '
                                     || r_get_sec.Fri_Ticker;
      Pgmloc         := 2280;
      Global_File_Updates.Update_Pricing_Msgs
          (P_Program_Id,
           V_System_Date,
           FALSE,
           Pricing_Msgs_Rec);
     --
    END IF;
    --
    -- remove existing schedule lines
    --
    Pgmloc   := 2290;
    DELETE Interest_Schedule
    WHERE  Fri_Ticker         = STR_Rec.Fri_Ticker
      AND  Data_Supplier_Code = C_STP
    ;
    v_deleted_rowcount := sql%rowcount;

    IF v_deleted_rowcount > 0
    THEN
      V_Affected := True;
    END IF;
    --V_Count_Int_Sch_Del := V_Count_Int_Sch_Del + SQL%ROWCOUNT;
    count_line_processed(c_cnt_sch_del, pricing_msgs_rec, v_deleted_rowcount);

    Pgmloc   := 2300;
    if r_get_sec.Income_Type_Code = 'I' then
      --
      -- regenerate all schedule for that security
      -- only if the security pays interests
      --
      Pgmloc   := 2310;
      generate_int_sch_for_one( r_get_sec.fri_ticker
                              , p_cntl_rec.log_file
                              --, STR_Rec.effective_date
                              ,null --date not important: regenerate ALL!
                              , r_get_sec.inc_freq_value
                              , v_min_date_to_keep
                              , v_max_date
                              , v_affected
                              , Pricing_Msgs_Rec
                              , v_success
                              );
      if not v_success then raise e_program_error; end if;

      --
      -- The completion of new schedules with amounts will be done
      -- in the second step of the calling routine.  For now insert
      -- the record to tell it to do it.
      --
      IF V_Affected
      THEN
        Pgmloc   := 2320;
        insert into prices_to_recalc
             ( fri_ticker
             , recalc_type_code
             , effective_date
             , updated_date
             , completed_date
             , last_user
             , last_stamp
             )
        values
             ( r_get_sec.fri_ticker
             , c_popexdate
             , STR_Rec.effective_date
             , null
             , null
             , constants.get_user_id
             , sysdate
             );
        count_line_processed(c_cnt_popexd_inserted, pricing_msgs_rec, 1);
      END IF; -- IF V_Affected
    end if; -- Income_Type_Code = 'I'

<< Mark_done >>

    Pgmloc := 2330;
    --
    UPDATE Prices_to_Recalc
       SET Updated_Date = SYSDATE
         , Completed_Date = SYSDATE
    WHERE Fri_Ticker = Previous_Fri_Ticker
    AND Recalc_Type_Code = c_genint
    AND Updated_Date IS NULL
    ;
    if sql%rowcount = 0 then
      v_message := 'Prices_to_Recalc NOT updated??';
      Pgmloc   := 2340;
      manage_utl_file.write_file( p_cntl_rec.log_file
                                , v_message
                                , v_success
                                , pricing_msgs_rec.err_text
                                );
      if not v_success then
        pricing_msgs_rec.remarks := pricing_msgs_rec.err_Text;
      end if;

      --set back real error msg
      pricing_msgs_rec.err_Text := v_message;
      raise e_program_error; ------>>>>
    end if;


<<Kignore>>
    Pgmloc   := 2350;
    V_Commit:= V_Commit+ 1;
    --
    If V_Affected
    Then
     V_Count_Securities := V_Count_Securities + 1;
     count_line_processed( c_sec_affected, Pricing_Msgs_Rec, V_Count_Securities);
    END IF;
    --
    Pgmloc    := 2360;
    Pricing_Msgs_Rec.Msg_Text_2 := 'Program in Progress.';
    --
    Pgmloc    := 2370;
    Global_File_Updates.Update_Pricing_Msgs
      (P_Program_Id,
       V_System_Date,
       FALSE,
       Pricing_Msgs_Rec);
    --
  END LOOP;

  Pgmloc   := 2380;
  savepoint a;



  -- Updating the Pricing_Msgs after the program is completed.
  --
  Pgmloc    := 2390;
  --
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  Pricing_Msgs_Rec.Msg_Text_2 := 'Program Successfully Completed.';
  Pricing_Msgs_Rec.Remarks    := 'Securities not processed in: '||
                                            p_cntl_rec.log_file_name;

  --
  Pgmloc   := 2400;
  Global_File_Updates.Update_Pricing_Msgs
      (P_Program_Id,
       V_System_Date,
       FALSE,
       Pricing_Msgs_Rec);
  --
  Pgmloc   := 2410;
  COMMIT;


  ----------------------------
  -- process terminating at...
  ----------------------------
  Pgmloc := 2420;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'122'
                                   , 'N'
                                   , TO_CHAR( SYSDATE
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  Pgmloc := 2430;
  load_supplier_Rules.Write_file( p_Cntl_Rec.Log_File
                                , 'Done... '||V_Message
                                , Pricing_Msgs_Rec
                                , v_Success
                                );
  IF ( NOT v_SUCCESS ) THEN raise e_program_error; END IF;

  Pgmloc   := 2440;
  COMMIT;

  P_Success := 'Y';

EXCEPTION
  WHEN e_program_error then
    rollback to savepoint a;

    if pricing_msgs_rec.err_text is null then
      pricing_msgs_rec.err_text := v_message;
    end if;
    pricing_msgs_rec.err_text := substr(
      pricing_msgs_rec.err_text||' '||$$plsql_unit||'@'||pgmloc
      ,1,255);
    Pgmloc := 2450;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'170'
                                     , 'N'
                                     , Pricing_Msgs_Rec.Msg_Text
                                     );
    Pgmloc := 2460;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'172'
                                     , 'N'
                                     , Pricing_Msgs_Rec.Msg_Text_2
                                     );
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
    global_file_updates.update_pricing_msgs( p_program_id
                                           , pricing_msgs_rec.date_of_msg
                                           , false
                                           , pricing_msgs_rec
                                           );

    close_all_cursors;

    commit;
    P_Message := pricing_msgs_rec.err_text;
    P_Success := 'N';

  WHEN OTHERS THEN
    pricing_msgs_rec.err_text := substr(
      'err@'||pgmloc||' '
      ||dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      ,1,255);
    rollback to savepoint a;
    global_file_updates.update_pricing_msgs( p_program_id
                                           , pricing_msgs_rec.date_of_msg
                                           , false
                                           , pricing_msgs_rec
                                           );

    close_all_cursors;

    commit;
    P_Message := pricing_msgs_rec.err_text;
    P_Success := 'N';


END interest_payment_sch_to_recalc;


--------------------------------------------------------------------------------
-- To Get the most prioritary price supplier
------------------------------------------------------------------------
PROCEDURE Get_One_Price
        ( File_Date        IN     DATE
        , P_Fri_Ticker     IN     Securities.Fri_Ticker%TYPE
        , P_Source         IN     Sources.Code%TYPE
        , P_Currency       IN     Currencies.Currency%TYPE
        , P_Product_Code   IN     Products.Code%TYPE
        , P_Sub_Prod_Code  IN     Sub_Products.Sub_product_Code%TYPE
        , P_Security_Prices   OUT Security_Prices%ROWTYPE
        , Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , Success             OUT BOOLEAN
        ) IS --{
--
 R1_Security_Prices  Security_Prices%ROWTYPE;
 C_Old_Date          CONSTANT DATE         := TO_DATE( '01-jan-1901'
                                                      , 'dd-mon-yyyy' );
--
CURSOR Get_Security_Prices
     ( I_Product_Code  Products.Code%TYPE
     , I_Sub_Prod_Code Sub_Products.Sub_product_Code%TYPE
     , I_Date          DATE
     , I_Fri_Ticker    Securities.Fri_Ticker%TYPE
     , I_Source        Sources.Code%TYPE
     , I_Currency      Currencies.Currency%TYPE
     ) IS
--mlz - 15nov2015
--SELECT SP.*
SELECT /*+ index( sp security_prices_x_02 ) */
       SP.*
--endmlz - 15nov2015
  FROM Security_Prices   SP
     , Product_Suppliers PS
 WHERE PS.Product_Code            = I_Product_Code
   AND PS.Sub_Product_Code        = I_Sub_Prod_Code
   AND PS.Active_Flag             = 'Y'
   AND SP.Fri_Ticker              = I_Fri_Ticker
-- Modification - JSL - 13 octobre 2010
-- AND SP.Source_Code             = I_Source
-- AND Official_Prices.Source_Synonym( SP.Source_Code ) = I_Source
   AND Pricing_Sources.Get_Synonym( SP.Source_Code ) = I_Source
-- Fin de modification - JSL - 13 octobre 2010
   AND SP.Price_Currency          = I_Currency
   AND SP.Data_Supplier_Code      = PS.Data_Supplier_Code
   AND SP.Delivery_Date           = TO_DATE( TO_CHAR( I_DATE, 'YYYYMMDD' )
                                          || PS.Delivery_Time
                                           , 'YYYYMMDDHH24MI'
                                           )
ORDER BY PS.Priority_Order;
--
BEGIN --{
  --
  Success := FALSE;
  --
  Pgmloc := 2470;
  Load_Supplier_Rules.Initialize_SP_Rec( R1_Security_Prices
                                       , Pricing_Msgs_Rec
                                       , Success
                                       );
  IF NOT Success THEN GOTO Get_Out; END IF;
  Pgmloc := 2480;
  FOR R_Security_Prices
   IN Get_Security_Prices( P_Product_Code
                         , P_Sub_Prod_Code
                         , File_Date
                         , P_Fri_Ticker
-- Modification - JSL - 13 octobre 2010
--                   --  , P_Source
--                       , Official_Prices.Source_Synonym( P_Source )
                         , Pricing_Sources.Get_Synonym( P_Source )
-- Fin de modification - JSL - 13 octobre 2010
                         , P_Currency
                         )
  LOOP --{
    Pgmloc := 2490;
    IF R1_Security_Prices.Fri_Ticker IS NULL
    THEN
      R1_Security_Prices := R_Security_Prices;
    ELSIF NVL( R1_Security_Prices.Price_Date, C_Old_Date )
          < NVL( R_Security_Prices.Price_Date, C_Old_Date )
    THEN
      R1_Security_Prices := R_Security_Prices;
    ELSIF NVL( R1_Security_Prices.Price_Date, C_Old_Date )
          = NVL( R_Security_Prices.Price_Date, C_Old_Date )
    THEN
      IF NVL( R1_Security_Prices.Bid_Date, C_Old_Date )
         < NVL( R_Security_Prices.Bid_Date, C_Old_Date )
      THEN
        R1_Security_Prices := R_Security_Prices;
      ELSIF NVL( R1_Security_Prices.Bid_Date, C_Old_Date )
            = NVL( R_Security_Prices.Bid_Date, C_Old_Date )
      THEN
        IF NVL( R1_Security_Prices.Ask_Date, C_Old_Date )
           < NVL( R_Security_Prices.Ask_Date, C_Old_Date )
        THEN
          R1_Security_Prices := R_Security_Prices;
        END IF;
      END IF;
    END IF;
  END LOOP; --}
  P_Security_Prices := R1_Security_Prices;
  --
  Pgmloc   := 2500;
-- Ajout - JSL - 14 septembre 2010
  IF P_Security_Prices.Bid < 70
  THEN
    P_Security_Prices.Accrued_Interest   := NULL;
    P_Security_Prices.Term_in_Years      := NULL;
    P_Security_Prices.Settlement_Date    := NULL;
    P_Security_Prices.Settlement_Date    := NULL;
    P_Security_Prices.Dirty_Price        := NULL;
    P_Security_Prices.Price_Mod_Duration := NULL;
    P_Security_Prices.Bid_Dirty_Price    := NULL;
    P_Security_Prices.Bid_Mod_Duration   := NULL;
    P_Security_Prices.Ask_Dirty_Price    := NULL;
    P_Security_Prices.Ask_Mod_Duration   := NULL;
    P_Security_Prices.Mid_Dirty_Price    := NULL;
    P_Security_Prices.Mid_Mod_Duration   := NULL;
  END IF;
-- Fin d'ajout - JSL - 14 septembre 2010
  --
  Pgmloc   := 2510;
  Success := TRUE;
<< Get_Out >>
  Pgmloc   := 2520;
  NULL;
--
EXCEPTION
  WHEN NO_DATA_FOUND THEN
dbms_output.put_line('Get_One_Price nodatafound : ' || P_Fri_Ticker
                               || ' ' || P_Source
                               || ' ' || P_Currency );
    P_Security_Prices := R1_Security_Prices;
    Success := TRUE;
  WHEN OTHERS THEN
dbms_output.put_line('Get_One_Price when other : ' || P_Fri_Ticker
                               || ' ' || P_Source
                               || ' ' || P_Currency
                               || ' ' || SQLCODE );
    Pricing_Msgs_Rec.Err_Text := SUBSTR(SQLERRM(SQLCODE),1,255);
    Success := FALSE;
--
END Get_One_Price; --}}
------------------------------------------------------------------------

--
FUNCTION Get_Next_Payment_Date
       ( P_Fri_Ticker             Securities.Fri_Ticker%TYPE
       , P_Value_Date             DATE
       , P_Fri_ID                 Tickers.Ticker%TYPE
       , P_Income_Frequency_Value Frequencies.Value%TYPE
       , P_Log_File               Utl_File.File_Type
       , P_Skip            IN OUT BOOLEAN
       , P_Success            OUT BOOLEAN
       , P_Message            OUT VARCHAR2
       ) RETURN DATE IS --{
--
Pricing_Msgs_Rec  Pricing_Msgs%ROWTYPE;
--
V_Mat_Date         DATE;
V_Next_Pay_Date    DATE;
V_End_Month_Flag   Interest_Calc_Parms.End_Of_Month_Flag%TYPE;
V_Payment_Date     DATE;
V_Day              VARCHAR2(2);
V_Month_Year       VARCHAR2(12);
V_First_Pay_Date   DATE;
V_Message          VARCHAR2(256);
--
C_Num_Months       NUMBER := 12;
--
--
CURSOR Get_From_Interest_Schedule
     ( I_Fri_Ticker Securities.Fri_Ticker%TYPE
     , I_Value_Date DATE
     ) IS
SELECT MIN( Income_Date )
  FROM Interest_Schedule
 WHERE Fri_Ticker   = I_Fri_Ticker
   AND Income_Date >= I_Value_Date
   AND Income_Date <= ADD_MONTHS( I_Value_Date, 1 )
   AND Data_Supplier_Code = C_STP;
--
------------------------
-- Cursor Get_Mat_Date
------------------------
CURSOR Get_Mat_Date
     ( I_Fri_Ticker  Securities.Fri_Ticker%TYPE
     , I_Value_Date  DATE
     ) IS
SELECT MIN( M.Maturity_Date )
  FROM Maturity_Schedule M
     , Maturity_Types    T
 WHERE M.Fri_Ticker               = I_Fri_Ticker
   AND M.Maturity_Type_Code       = T.Code
   AND M.Maturity_Date           >= I_Value_Date
   AND T.Maturity_Short_Type_Code IN ('E','S');
--
---------------------------------
-- CURSOR Get_Max_First_Pay_Date
---------------------------------
CURSOR Get_Max_First_Pay_Date
     ( I_Fri_Ticker  Securities.Fri_Ticker%TYPE
     , I_Value_Date  DATE
     ) IS
SELECT MAX( First_Payment_Date )
  FROM Interest_Income_Rates
 WHERE Fri_Ticker          = I_Fri_Ticker
   AND First_Payment_Date <= I_Value_Date ;
--
----------------------------------
-- CURSOR Get_Min_First_Pay_Date
---------------------------------
CURSOR Get_Min_First_Pay_Date
     ( I_Fri_Ticker  Securities.Fri_Ticker%TYPE
     , I_Value_Date  DATE
     ) IS
SELECT MIN( First_Payment_Date )
  FROM Interest_Income_Rates
 WHERE First_Payment_Date >= I_Value_Date
   AND Fri_Ticker          = I_Fri_Ticker;
--
---------------------------------
-- CURSOR Get_First_Payment_Date
---------------------------------
CURSOR Get_First_Payment_Date
     ( I_Fri_Ticker  Securities.Fri_Ticker%TYPE
     , I_Value_Date  DATE
     ) IS
SELECT First_Payment_Date
  FROM Interest_Income_Rates
 WHERE Fri_Ticker = i_Fri_Ticker
   AND First_Payment_Date BETWEEN (I_Value_Date - 15)
                              AND (I_Value_Date + 15);
--
BEGIN  --{
   --
   Pgmloc := 2530;
   V_Next_Pay_Date := NULL;
   --
   -- Ajout - JSL - 26 aout 2010
   IF P_Income_Frequency_Value NOT BETWEEN 1 AND 12
   THEN
      Pgmloc   := 2540;
      P_Skip := TRUE;
      RETURN NULL;
   END IF;
   -- Fin d'ajout - JSL - 26 aout 2010
   --
   Pgmloc   := 2550;
   OPEN Get_From_Interest_Schedule( P_Fri_Ticker, P_Value_Date );
   Pgmloc := 2560;
   FETCH Get_From_Interest_Schedule INTO V_Next_Pay_Date;
   Pgmloc := 2570;
   CLOSE Get_From_Interest_Schedule;
   IF V_Next_Pay_Date IS NOT NULL
   THEN
     Pgmloc   := 2580;
     RETURN V_Next_Pay_Date;
   END IF;
   --
   Pgmloc := 2590;
   -----------------------------
   -- Getting the maturity date
   -----------------------------
   OPEN Get_Mat_Date(P_Fri_Ticker, P_Value_Date);
   Pgmloc := 2600;
   FETCH Get_Mat_Date INTO V_Mat_Date;
   Pgmloc := 2610;
   IF Get_Mat_Date%NOTFOUND THEN
      Pgmloc := 2620;
      CLOSE Get_Mat_Date;
      Pgmloc := 2630;
      --
      P_Skip := TRUE;
      --
      Pgmloc := 2640;
      RETURN NULL;
      --
   END IF;
   Pgmloc := 2650;
   CLOSE Get_Mat_Date;
   Pgmloc := 2660;
   IF V_Mat_Date <= P_Value_Date THEN
      Pgmloc := 2670;
      --
      P_Skip := TRUE;
      --
      RETURN NULL;
   END IF;
   ---------------------------------------------------------
   -- Setting End_Of_Month_Flag = 'Y' if the maturity date
   -- is the last day of the maturity month
   ---------------------------------------------------------
   Pgmloc := 2680;
   IF V_Mat_Date = LAST_DAY(V_Mat_Date) THEN
      V_End_Month_Flag := 'Y';
   ELSE
      V_End_Month_Flag := 'N';
   END IF;
   Pgmloc := 2690;
   ----------------------------------------------------------------
   -- Getting maximun first payment date <= current date - 1 month
   ----------------------------------------------------------------
   OPEN Get_Max_First_Pay_Date(P_Fri_Ticker
                              ,ADD_MONTHS(P_Value_Date,-1));
   Pgmloc := 2700;
   V_Payment_Date := NULL;
   FETCH Get_Max_First_Pay_Date  INTO V_Payment_Date;
   Pgmloc := 2710;
   CLOSE Get_Max_First_Pay_Date;
   ------------------------------------------------------------------
   -- If max first payment date not found then get min first payment
   -- date >= current dat - 1 month
   ------------------------------------------------------------------
   Pgmloc  := 2720;
   IF V_Payment_Date IS NULL THEN
      Pgmloc := 2730;
      OPEN Get_Min_First_Pay_Date(P_Fri_Ticker
                                 ,ADD_MONTHS(P_Value_Date,-1));
      Pgmloc := 2740;
      FETCH Get_Min_First_Pay_Date INTO V_Payment_Date;
      Pgmloc := 2750;
      CLOSE Get_Min_First_Pay_Date;
   END IF;
   Pgmloc := 2760;
   ------------------------------------------------------------------
   -- If min first payment not found, loop until date < current date
   ------------------------------------------------------------------
   IF V_Payment_Date IS NULL
   THEN
      V_Payment_Date := V_Mat_Date;
      Pgmloc := 2770;
      WHILE  V_Payment_Date > P_Value_Date LOOP
         V_Payment_Date := ADD_MONTHS( V_Payment_Date
                                     , - ( 12 / P_Income_Frequency_Value ) );
      END LOOP;
      --
   END IF;
   -------------------------------------------------------
   -- V_Payment_Date should be greather than P_Value_Date
   -------------------------------------------------------
   Pgmloc := 2780;
   IF V_Payment_Date <= P_Value_Date THEN
      WHILE  V_Payment_Date < P_Value_Date LOOP
         V_Payment_Date := ADD_MONTHS(V_Payment_Date
                                     , + (12 / P_Income_Frequency_Value));
      END LOOP;
   END IF;
   -----------------------------------
   --  Getting the next payment date
   ------------------------------------
   Pgmloc := 2790;
   V_Day  := TO_CHAR(V_Payment_Date, 'DD');
   --
   -- Keep the payment day the same day of the month.If the day is
   -- greater than the last day of that month, get the last day of
   -- the month.
   --
   V_Month_Year := TO_CHAR(V_Payment_Date,'-MON-YYYY');
   --
   Pgmloc := 2800;
   BEGIN --{
      V_Next_Pay_Date := TO_DATE(V_Day||V_Month_Year,'DD-MON-YYYY');
   EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE = -1839 THEN
         V_Next_Pay_Date := LAST_DAY(V_Payment_Date);
      ELSE
         Pgmloc := 2810;
         Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'165'
                                          , 'N'
                                          , ' - Error Code: '
                                          || to_char(Sqlcode)
                                          , P_Fri_ID
                                          , V_Message
                                          );
         Pgmloc := 2820;
         Load_supplier_Rules.Write_file( P_Log_File
                                       , V_Message
                                       , Pricing_Msgs_Rec
                                       , P_Success
                                       );
         IF ( NOT P_SUCCESS ) THEN RETURN NULL; END IF;
         RAISE;
      END IF;
   END; --}
   Pgmloc := 2830;
   --
   -- Get the last day of the month if end of month flag is Y.
   --
   IF V_End_Month_Flag = 'Y' THEN
      V_Next_Pay_Date := LAST_DAY(V_Next_Pay_Date);
   END IF;
   Pgmloc := 2840;
   --
   -- Setting the Next payment date to the maturity date when
   -- the next payment date is within 15 days of the mat date
   --
   IF V_Next_Pay_Date BETWEEN (V_Mat_Date - 15)
                      AND     (V_Mat_Date + 15) THEN
      V_Next_Pay_Date := V_Mat_Date;
   END IF;
   Pgmloc  := 2850;
   OPEN Get_First_Payment_Date (P_Fri_Ticker,V_Next_Pay_Date);
   Pgmloc  := 2860;
   FETCH Get_First_Payment_Date INTO V_First_Pay_Date;
   --
   Pgmloc := 2870;
   IF Get_First_Payment_Date%FOUND  THEN
      V_Next_Pay_Date := V_First_Pay_Date;
   END IF;
   Pgmloc  := 2880;
   CLOSE Get_First_Payment_Date;
   --
   Pgmloc    := 2890;
   --
   RETURN V_Next_Pay_Date;
   --
END Get_Next_Payment_Date; --}}


------------------------------------------------------------------------
--
-- CAF 1 - update int_sch if accrued interest difference found
-- loop all securities, for 1 day (p_value_date)
--
PROCEDURE CAF_Interest_Payment
        ( P_Program_Id    IN     Ops_Scripts.Program_Id%TYPE
        , P_Value_Date    IN     DATE
        , P_Commit_Freq   IN     INTEGER
        , P_Success          OUT BOOLEAN
        , P_Message          OUT VARCHAR2
        ) IS --{
--
--------------
-- Constants
--------------
C_Data_Supp        CONSTANT Data_Suppliers.Code%TYPE   := 'STP';
C_Del_Time         CONSTANT Supplier_Running_Parms.Delivery_Time%TYPE := '2000';
C_Form_Set_Code    CONSTANT Formulae_Set.Code%TYPE     := 'INT_PAY';
C_Form_Gen_Code    CONSTANT Formulae_Generic.Code%TYPE := 'INTEREST_PAYMENT';
C_Product_Code     CONSTANT Products.Code%TYPE                 := 'CAF';
C_Sub_Prd_Code     CONSTANT Sub_Products.Sub_Product_Code%TYPE := 'NA';
C_Data_Supp_Code   CONSTANT Data_Suppliers.Code%TYPE           := 'FRI';
--
--------------
-- Variables
--------------
V_Commit_Freq      NUMBER;
-- Ajout - JSL - 08 juillet 2014
V_Commit_Count     NUMBER;
-- Fin d'ajout - JSL - 08 juillet 2014
V_Message          VARCHAR2(256);
V_Date_Fmt         VARCHAR2(11) := 'DD-MON-YYYY';
V_Del_Fmt          VARCHAR2(22) := 'DD-MON-YYYY HH24:MI:SS';
V_Del_Date         DATE;
Success            BOOLEAN;
V_Price_Rec_T      Security_Prices%ROWTYPE;
V_Price_Rec_T1     Security_Prices%ROWTYPE;
V_Adj_Prc_Flag     Sources.Abs_Adjust_Prices_Flag%TYPE;
V_Prc_Date_T1      DATE;
V_Int_Calc_Parms   Interest_Calc_Parms%ROWTYPE;
V_Fri_ID           Tickers.Ticker%TYPE;
V_Next_Pay_Date    DATE;
V_SDAI             NUMBER;
V_Skip             BOOLEAN;
V_Value_Date       DATE;
-- Ajout - JSL - 05 novembre 2010
W_Value_Date       DATE;
W_Last_Open_Date   DATE;
-- Ajout - JSL - 05 novembre 2010
T_Values           Formulae.T_Formulae;
PRP                Product_Running_Parms%ROWTYPE;
--mlz - 09mar2011
--Cntl_Rec           R_Parameters;
Cntl_Rec           product_component.R_Parameters;
Pricing_Msgs_Rec   Pricing_Msgs%ROWTYPE;
--endmlz - 09mar2011
V_Dynamic          BOOLEAN;
--
------------
-- Counters
------------
V_Rows_Read        NUMBER := 0;
V_Rows_Ins         NUMBER := 0;
V_Rows_Rej         NUMBER := 0;
V_Rows_Upd         NUMBER := 0;
--
---------------------------------------------------------------
--Cursor Get_Sec fetch only active securities paying interests
---------------------------------------------------------------
-- Modification - JSL - 18 octobre 2010
-- CURSOR Get_Sec IS
-- SELECT A.Fri_Ticker
--      , A.Income_Frequency_Value
--      , C.Code
--      , C.Annual_Validity_Flag
--      , B.Active_Flag
--      , A.Primary_Source_Code
--      , A.Primary_Currency
--      , A.Indicated_Annual_Rate
--   FROM Securities                A
--      , Trading_Status            B
--      , Frequencies               C
-- -- Ajout - JSL - 01 juin 2010
-- --   , Issuers                   I
-- -- Fin d'ajout - JSL - 01 juin 2010
--  WHERE A.Trading_Status_Code    = B.Code
--    AND A.Income_Frequency_Value = C.Value
--    AND A.Income_Type_Code       = 'I'
--    AND B.Active_Flag            = 'Y'
--    AND C.Annual_Validity_Flag   = 'Y'
-- -- Ajout - JSL - 01 juin 2010
-- -- Accept only the Canadian Issuers
-- -- AND A.Fri_Issuer             = I.Fri_Issuer
-- -- AND I.Nation_Code            = 'CDA'
-- -- Fin d'ajout - JSL - 01 juin 2010
-- ORDER BY A.Fri_Ticker;
CURSOR Get_Sec
     ( I_Value_Date DATE
-- Ajout - JSL - 05 novembre 2010
     , I_Previous_Date DATE
-- Fin d'ajout - JSL - 05 novembre 2010
     ) IS
--mlz - 15nov2015
--SELECT A.Fri_Ticker
SELECT /*+ index( sts security_trading_status_x_01 ) */
       A.Fri_Ticker
--endmlz - 15nov2015
     , A.Income_Frequency_Value
     , C.Code
     , C.Annual_Validity_Flag
     , B.Active_Flag
     , STS.Source_Code    Primary_Source_Code
     , STS.Price_Currency Primary_Currency
     , A.Indicated_Annual_Rate
-- Ajout - JSL - 05 novembre 2010
     , A.Trading_Status_Code
     , A.Date_Of_Status
-- Fin d'ajout - JSL - 05 novembre 2010
  FROM Securities                A
     , Trading_Status            B
     , Frequencies               C
     , Security_Trading_Status   STS
 WHERE A.Trading_Status_Code    = B.Code
   AND A.Income_Frequency_Value = C.Value
   AND A.Income_Type_Code       = 'I'
-- Modification - JSL - 05 novembre 2010
-- AND B.Active_Flag            = 'Y'
   AND ( B.Active_Flag          = 'Y'
      OR A.Date_Of_Status      >= I_Previous_Date
       )
-- Fin de modification - JSL - 05 novembre 2010
   AND C.Annual_Validity_Flag   = 'Y'
   AND STS.Fri_Ticker           = A.Fri_Ticker
   AND STS.Primary_Market_Flag  = 'Y'
   AND STS.Date_Of_Status       = (
       --mlz - 15nov2015
       --SELECT MAX( STS1.Date_Of_Status )
       SELECT /*+ index( sts1 security_trading_status ) */
              MAX( STS1.Date_Of_Status )
       --endmlz - 15nov2015
         FROM Security_Trading_Status STS1
        WHERE STS1.Fri_Ticker          = STS.Fri_Ticker
          AND STS1.Primary_Market_Flag = 'Y'
          AND STS1.Date_Of_Status     <= I_Value_Date
       )
ORDER BY A.Fri_Ticker;
-- Fin de modification - JSL - 18 octobre 2010
--
-------------------------------------------------------------------
-- Cursor Get_Adj_Flag verify if the source contain adjusted prices
-------------------------------------------------------------------
CURSOR Get_Adj_Flag
     ( I_Source_Code Sources.Code%TYPE
     ) IS
SELECT Abs_Adjust_Prices_Flag
  FROM Sources
 WHERE Code = I_Source_Code;
--
-----------------------------
-- Cursor Get_Int_Calc_Parms
-----------------------------
CURSOR Get_Int_Calc_Parms
     ( I_Fri_Ticker Interest_Calc_Parms.Fri_Ticker%TYPE
     ) IS
SELECT *
  FROM Interest_Calc_Parms
 WHERE Fri_Ticker = I_Fri_Ticker;
--
BEGIN --{
  --
  Pgmloc := 2900;
  V_Commit_Freq := NVL(P_Commit_Freq,500);
  P_Success := TRUE;
  V_Value_Date := TRUNC(NVL(P_Value_Date,Constants.Get_Site_Date));
  V_Del_Date   := TO_DATE(TO_CHAR(V_Value_Date,V_Date_Fmt)||C_Del_Time
                         ,V_Del_Fmt);
-- Modification - JSL - 18 octobre 2010
-- Code deplace plus bas
------------------------------------------------------------
---- Get argument for sudden disminution in accrued interes
------------------------------------------------------------
--V_SDAI := TO_NUMBER(Scripts.Get_Arg_Value(p_program_id, 'SDAI'));
-- Fin de modification - JSL - 18 octobre 2010
  --
  V_Rows_Read    := 0;
  V_Rows_Ins     := 0;
  V_Rows_Rej     := 0;
  V_Rows_Upd     := 0;
  --
  -------------------------------
  --  create the pricing message,
  --  open the log
  --  copy the input file
  --  open the copied file
  --  open the output file
  -------------------------------
  --
  Pgmloc := 2910;
  Cntl_Rec.In_File_Name  := NULL;
  Cntl_Rec.Out_File_Name := NULL;
  Cntl_Rec.In_File_Open  := FALSE;
  Cntl_Rec.Out_File_Open := FALSE;
  Cntl_Rec.Log_File_Open := FALSE;

-- Ajout - JSL - 08 juillet 2014
  G_Start_Time := DBMS_UTILITY.Get_Time;
-- Fin d'ajout - JSL - 08 juillet 2014

  Pgmloc := 2920;
  --mlz - 09mar2011
  --Process_Beginning(
  product_component.Process_Beginning(
  --endmlz - 09mar2011
                                        P_Program_Id
                                      , V_Value_Date
                                      --mlz - 09mar2011
                                      --, P_Commit_Freq
                                      , v_Commit_Freq
                                      --endmlz - 09mar2011
                                      --, PRP
                                      , Cntl_Rec
                                      , Pricing_Msgs_Rec
                                      , P_Success
                                      , P_Message
                                      );
  IF NOT P_Success THEN GOTO GET_OUT; END IF;
-- Ajout - JSL - 08 juillet 2014
  G_End_Time := DBMS_UTILITY.Get_Time;
-- Fin d'ajout - JSL - 08 juillet 2014
  --
  Pgmloc := 2930;
  COMMIT;
  --
-- Ajout - JSL - 08 juillet 2014
  Pgmloc := 2932;
  G_All_Parms := Scripts.Get_All_Parms( P_Program_Id );
  IF G_All_Parms.exists( 'VERBOSE')
  THEN
    Pgmloc := 2934;
    Log_Parameters( G_All_Parms, Cntl_Rec, Pricing_Msgs_Rec );
    Pgmloc := 2936;
    Begin
      G_Monitor_Level := 0;
      G_Monitor_level := to_number( G_all_Parms( 'VERBOSE' ).parm_value );
    exception
    when others then null;
    end;
    IF G_Monitor_level < 0
    THEN
      G_Monitor_Level := 0;
    ELSIF G_Monitor_Level > 4
    THEN
      G_Monitor_Level := 4;
    END IF;
    IF G_Monitor_Level IN ( 2, 4 )
    THEN
      G_Get_Timing := TRUE;
    ELSE
      G_Get_Timing := FALSE;
    END IF;
  ELSE
    G_Monitor_Level := 0;
    G_Get_Timing    := FALSE;
  END IF;
  IF G_Monitor_Level > 0
  THEN
    Pgmloc := 2938;
    G_log_handle := manage_prc_msgs_det.get_log_to_file_handle( Cntl_Rec.Log_File , C_Sep );
    Pgmloc := 2940;
    manage_prc_msgs_det.write_log( G_Log_Handle, 'It took '||(G_End_Time - G_Start_Time)/100.0||' to begin the process.' );
    manage_prc_msgs_det.write_log( G_Log_Handle, null );
    manage_prc_msgs_det.write_log( G_Log_Handle, 'Commit are to be done at '||v_commit_Freq||'.' );
    manage_prc_msgs_det.write_log( G_Log_Handle, null );
  END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
  --
-- Modification - JSL - 18 octobre 2010
-- Code deplace de plus haut et modifie
  ----------------------------------------------------------
  -- Get argument for sudden diminution in accrued interes
  ----------------------------------------------------------
  V_SDAI := TO_NUMBER( Scripts.Get_Arg_Value( p_program_id, 'SDAI' ) );
  IF V_SDAI > 0
  AND V_SDAI < 1
  THEN --{
    NULL;
  ELSE --}{
    Pgmloc := 2940;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'015'
                                     , 'N'
                                     , V_SDAI
                                     , V_Message
                                     );
    Pgmloc := 2950;
    Load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                  , V_Message
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
    Pricing_Msgs_Rec.Remarks := V_Message;
    P_SUCCESS := FALSE;
    GOTO GET_OUT;
  END IF; --}
-- Fin de modification - JSL - 18 octobre 2010
  --
-- Ajout - JSL - 05 novembre 2010
  --
  Pgmloc := 2960;
-- Ajout - JSL - 08 juillet 2014
  IF G_Monitor_Level > 2
  THEN
    manage_prc_msgs_det.write_log( G_Log_Handle, 'Getting W_Last_Open_Date at line '||$$PLSQL_LINE );
  END IF;
  IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
  W_Last_Open_Date
      := Holiday.Get_Actual_Last_Open_Date( V_Value_Date
                                          , 'SOURCE'
                                          , 'FRI'
                                          , NULL
                                          );
-- Ajout - JSL - 08 juillet 2014
  IF G_Monitor_Level > 2 AND G_Get_Timing
  THEN
    G_End_Time := DBMS_UTILITY.Get_Time;
    manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
  END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
  --
-- Fin d'ajout - JSL - 05 novembre 2010
  ------------------------------------
  -- Processing Securities
  ------------------------------------
-- Ajout - JSL - 08 juillet 2014
  IF G_Monitor_Level > 0
  THEN
    manage_prc_msgs_det.write_log( G_Log_Handle, 'Opening G_Sec at line '||$$PLSQL_LINE );
  END IF;
  IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
  V_Commit_Count := 0;
-- Fin d'ajout - JSL - 08 juillet 2014
  Pgmloc := 2970;
-- Modification - JSL - 05 novembre 2010
--FOR Sec_Rec IN Get_Sec( V_Value_Date )
  FOR Sec_Rec IN Get_Sec( V_Value_Date, W_Last_Open_Date )
-- Fin de modification - JSL - 05 novembre 2010
  LOOP --{
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2 AND G_Get_Timing
    THEN
      G_End_Time := DBMS_UTILITY.Get_Time;
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
    END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    Pgmloc := 2980;
    ---------------------------------
    -- Counting Securities read
    ---------------------------------
    V_Rows_Read := V_Rows_Read + 1;
    V_Fri_ID := Security.Get_Fri_ID (Sec_Rec.Fri_Ticker);
    --
-- Ajout - JSL - 08 juillet 2014
    V_Commit_Count := V_Commit_Count + 1;
    IF V_Commit_Count >= V_Commit_Freq
    THEN
-- Fin d'ajout - JSL - 08 juillet 2014
      Pgmloc := 2990;
      Pricing_Msgs_Rec.Remarks          := V_Fri_Id;
      Pricing_Msgs_Rec.Value_01         := V_Rows_Read;
      Pricing_Msgs_Rec.Value_02         := V_Rows_Ins;
      Pricing_Msgs_Rec.Value_03         := V_Rows_Rej;
      Pricing_Msgs_Rec.Value_04         := V_Rows_Upd;
      --
      Pgmloc := 3000;
-- Ajout - JSL - 08 juillet 2014
      IF G_Monitor_Level > 2
      THEN
        manage_prc_msgs_det.write_log( G_Log_Handle, 'Committing at line '||$$PLSQL_LINE );
      END IF;
      IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
      load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                         , Pricing_Msgs_Rec.Date_of_Msg
                                         , Pricing_Msgs_Rec
                                         , Success
                                         );
      Pgmloc := 3010;
      COMMIT;
-- Ajout - JSL - 08 juillet 2014
      IF G_Monitor_Level > 2 AND G_Get_Timing
      THEN
        G_End_Time := DBMS_UTILITY.Get_Time;
        manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
      END IF;
      V_Commit_Count := 0;
    END IF;
    IF G_Monitor_Level > 2
    THEN
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Security identifier is <'||V_Fri_Id||'>');
    END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    --
    ----------------------------------------------------------------------
    -- IF Primary_Source_Code or primary_currency are null skip the record
    ----------------------------------------------------------------------
    Pgmloc := 3020;
    IF Sec_Rec.Primary_Source_Code IS NULL
    OR Sec_Rec.Primary_Currency    IS NULL
    THEN  --{
-- Ajout - JSL - 08 juillet 2014
      IF G_Monitor_Level > 2
      THEN
        manage_prc_msgs_det.write_log( G_Log_Handle, 'Rejected - no primary  fri_id <'||V_Fri_Id||'>');
      END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
      Pgmloc := 3030;
      --cnt: skip pri
      ---------------------------------
      -- Counting Rows rejected
      ---------------------------------
      V_Rows_Rej := V_Rows_Rej + 1;
      --
      GOTO Next_Sec;
    END IF; --}
    -----------------------------------------------------------
    -- If the source contain adjusted prices ignore the record
    -----------------------------------------------------------
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2
    THEN
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Getting adjustement flag at line '||$$PLSQL_LINE );
    END IF;
    IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    Pgmloc := 3040;
    OPEN Get_Adj_Flag(Sec_Rec.Primary_Source_Code);
    Pgmloc := 3050;
    FETCH Get_Adj_Flag INTO V_Adj_Prc_Flag;
    Pgmloc := 3060;
    CLOSE Get_Adj_Flag;
    Pgmloc := 3070;
    IF V_Adj_Prc_Flag = 'Y'
    THEN --{
-- Ajout - JSL - 08 juillet 2014
      IF G_Monitor_Level > 2
      THEN
        manage_prc_msgs_det.write_log( G_Log_Handle, 'Rejected - adjusted source  fri_id <'||V_Fri_Id||'>');
      END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
      Pgmloc := 3080;
      --cnt: skip adj
      ---------------------------------
      -- Counting Rows rejected
      ---------------------------------
      V_Rows_Rej := V_Rows_Rej + 1;
      GOTO Next_Sec;
    END IF; --}
    --
-- Ajout - JSL - 05 novembre 2010
    Pgmloc := 3090;
    IF Sec_Rec.Active_Flag = 'Y'
    THEN
      W_Value_Date := V_Value_Date;
    ELSE
      W_Value_Date := Sec_Rec.Date_Of_Status;
    END IF;
-- Fin d'ajout - JSL - 05 novembre 2010
    --
    -------------------------------------
    -- Processing Securities_Prices at T
    -------------------------------------
    Pgmloc := 3100;
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2
    THEN
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Getting one price at line '||$$PLSQL_LINE );
    END IF;
    IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
-- Modification - JSL - 05 novembre 2010
--  Get_One_Price( V_Value_Date
    Get_One_Price( W_Value_Date
-- Fin de modification - JSL - 05 novembre 2010
                 , Sec_Rec.Fri_Ticker
                 , Sec_Rec.Primary_Source_Code
                 , Sec_Rec.Primary_Currency
                 , C_Product_Code
                 , C_Sub_Prd_Code
                 , V_Price_Rec_T
                 , Pricing_Msgs_Rec
                 , P_Success
                 );
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2 AND G_Get_Timing
    THEN
      G_End_Time := DBMS_UTILITY.Get_Time;
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
    END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    IF NOT P_SUCCESS THEN GOTO GET_OUT; END IF;
    ---------------------------------------------------------------
    -- If price_date != delivery_date or accrued_interest is null
    -- ignore the record
    ---------------------------------------------------------------
    Pgmloc := 3110;
    IF TRUNC(V_Price_Rec_T.Price_Date) != TRUNC(V_Price_Rec_T.Delivery_Date)
    OR V_Price_Rec_T.Accrued_Interest IS NULL
    THEN --{
-- Ajout - JSL - 08 juillet 2014
      IF G_Monitor_Level > 2
      THEN
        manage_prc_msgs_det.write_log( G_Log_Handle, 'Rejected - wrong price date or no accrued interest  fri_id <'||V_Fri_Id||'>  price date <'||trunc(V_Price_Rec_t.Price_Date)||'>  delivery_date <'||TRUNC(V_Price_Rec_T.Delivery_Date)||'>');
      END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
      Pgmloc := 3120;
      --cnt: price dt / acc
      ---------------------------------
      -- Counting Rows rejected
      ---------------------------------
      V_Rows_Rej := V_Rows_Rej + 1;
      --
      GOTO Next_Sec;
    END IF; --}
    --
-- Ajout - JSL - 05 novembre 2010
    Pgmloc := 3130;
    IF Sec_Rec.Active_Flag = 'Y'
    THEN
      NULL;
    ELSIF V_Price_Rec_T.Price_Date = Sec_Rec.Date_of_Status
    THEN
      V_Price_Rec_T.Accrued_Interest := 0;
    END IF;
-- Fin d'ajout - JSL - 05 novembre 2010
    --
    -----------------------------------------------------
    -- verifying if interest schedule has been generated
    -----------------------------------------------------
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2
    THEN
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Getting interest calc parms at line '||$$PLSQL_LINE );
    END IF;
    IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    Pgmloc := 3140;
    OPEN Get_Int_Calc_Parms(Sec_Rec.Fri_Ticker);
-- Ajout - JSL - 18 octobre 2010
    V_Int_Calc_Parms.Interest_Schedule_Flag := 'Y';
-- Fin d'ajout - JSL - 18 octobre 2010
    Pgmloc := 3150;
    FETCH Get_Int_Calc_Parms INTO V_Int_Calc_Parms;
    Pgmloc := 3160;
    CLOSE Get_Int_Calc_Parms;
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2 AND G_Get_Timing
    THEN
      G_End_Time := DBMS_UTILITY.Get_Time;
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
    END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    -------------------------
    -- Get Previous open Date
    -------------------------
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2
    THEN
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Getting actual last open date at line '||$$PLSQL_LINE );
    END IF;
    IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    Pgmloc := 3170;
    V_Prc_Date_T1
-- Modification - JSL - 05 novembre 2010
--    := Holiday.Get_Actual_Last_Open_Date( V_Value_Date
      := Holiday.Get_Actual_Last_Open_Date( W_Value_Date
-- Fin de modification - JSL - 05 novembre 2010
                                          , 'SOURCE'
                                          , Sec_Rec.Primary_Source_Code
                                          , NULL
                                          );
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2 AND G_Get_Timing
    THEN
      G_End_Time := DBMS_UTILITY.Get_Time;
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
    END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    --
    ----------------------------------------------------
    -- Processing Securities_Prices at T1 (Previous Day)
    ----------------------------------------------------
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2
    THEN
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Getting one price at line '||$$PLSQL_LINE );
    END IF;
    IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    Pgmloc := 3180;
    Get_One_Price( V_Prc_Date_T1
                 , Sec_Rec.Fri_Ticker
                 , Sec_Rec.Primary_Source_Code
                 , Sec_Rec.Primary_Currency
                 , C_Product_Code
                 , C_Sub_Prd_Code
                 , V_Price_Rec_T1
                 , Pricing_Msgs_Rec
                 , P_Success
                 );
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2 AND G_Get_Timing
    THEN
      G_End_Time := DBMS_UTILITY.Get_Time;
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
    END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
    IF NOT P_SUCCESS THEN GOTO GET_OUT; END IF;
    ---------------------------------------------------------------
    -- If  previous price_date !=  previous delivery_date or
    -- accrued_interest is null, then ignore the record
    ---------------------------------------------------------------
    Pgmloc := 3190;
    IF TRUNC(V_Price_Rec_T1.Price_Date) != TRUNC(V_Price_Rec_T1.Delivery_Date)
    OR V_Price_Rec_T1.Accrued_Interest IS NULL
    THEN
      Pgmloc := 3200;
      --cnt: price / adj
      ---------------------------------
      -- Counting Rows rejected
      ---------------------------------
      V_Rows_Rej := V_Rows_Rej + 1;
-- Ajout - JSL - 08 juillet 2014
      IF G_Monitor_Level > 2
      THEN
        manage_prc_msgs_det.write_log( G_Log_Handle, 'Rejected - wrong previous price date or no accrued interest  fri_id <'||V_Fri_Id||'>');
      END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
      --
      GOTO Next_Sec;
    END IF;
    --
    Pgmloc := 3210;
    ---------------------------------------
    --  Calculate The CAF Interest Payment
    ---------------------------------------
    BEGIN --{
-- Modification - JSL - 18 octobre 2010
--    IF ( ( V_Price_Rec_T1.Accrued_Interest
--         - V_Price_Rec_T.Accrued_Interest
--         )
--       / V_Price_Rec_T1.Accrued_Interest
--       ) > V_SDAI
      IF  V_Price_Rec_T1.Accrued_Interest <= 0
      THEN
        Pgmloc := 3220;
-- Ajout - JSL - 08 juillet 2014
        IF G_Monitor_Level > 2
        THEN
          manage_prc_msgs_det.write_log( G_Log_Handle, 'Rejected - accrued interest <= 0  fri_id <'||V_Fri_Id||'>');
        END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
        V_Rows_Rej := V_Rows_Rej + 1;
      ELSIF ( V_Price_Rec_T1.Accrued_Interest
            - V_Price_Rec_T.Accrued_Interest
            ) > ( V_SDAI * V_Price_Rec_T1.Accrued_Interest )
-- Fin de modification - JSL - 18 octobre 2010
      THEN --{
        Pgmloc := 3230;
        ---------------------------------------
        --  Getting Coupon Value per unit CP_T
        ---------------------------------------
        T_Values.DELETE();
        --
        T_Values('AI_T')   := V_Price_Rec_T.Accrued_Interest;
        T_Values('AI_T1')  := V_Price_Rec_T1.Accrued_Interest;
        T_Values('CPV_T')  := Sec_Rec.Indicated_Annual_Rate;
        T_Values('FREQ_T') := Sec_Rec.Income_Frequency_Value;
        --
-- Ajout - JSL - 08 juillet 2014
        IF G_Monitor_Level > 2
        THEN
          manage_prc_msgs_det.write_log( G_Log_Handle, 'Applying formula at line '||$$PLSQL_LINE );
        END IF;
        IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
        Pgmloc := 3240;
        Formulae.Apply( C_Data_Supp_Code
                      , C_Form_Set_Code
                      , C_Form_Gen_Code
                      , T_Values
                      , V_Dynamic
                      , P_Success
                      , P_Message
                      );
-- Ajout - JSL - 08 juillet 2014
        IF G_Monitor_Level > 2 AND G_Get_Timing
        THEN
          G_End_Time := DBMS_UTILITY.Get_Time;
          manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
        END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
        IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
        ---------------------------------
        -- Getting theorical payment date
        ---------------------------------
        Pgmloc := 3250;
        V_Skip := FALSE;
-- Ajout - JSL - 08 juillet 2014
        IF G_Monitor_Level > 2
        THEN
          manage_prc_msgs_det.write_log( G_Log_Handle, 'Getting next payment date at line '||$$PLSQL_LINE );
        END IF;
        IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
        V_Next_Pay_Date := Get_Next_Payment_Date( Sec_Rec.Fri_Ticker
-- Modification - JSL - 05 novembre 2010
--                                     --       , V_Value_Date
                                                , W_Value_Date
-- Fin de modification - JSL - 05 novembre 2010
                                                , V_Fri_ID
                                                , Sec_Rec.Income_Frequency_Value
                                                , Cntl_Rec.Log_File
                                                , V_Skip
                                                , P_Success
                                                , P_Message
                                                );
-- Ajout - JSL - 08 juillet 2014
        IF G_Monitor_Level > 2 AND G_Get_Timing
        THEN
          G_End_Time := DBMS_UTILITY.Get_Time;
          manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
        END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
        IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
        IF V_Skip THEN
          --cnt: skip next pay dt
          ---------------------------------
          -- Counting Rows rejected
          ---------------------------------
          V_Rows_Rej := V_Rows_Rej + 1;
-- Ajout - JSL - 08 juillet 2014
          IF G_Monitor_Level > 2
          THEN
            manage_prc_msgs_det.write_log( G_Log_Handle, 'Rejected - v_skip from get_next_ayment_date  fri_id <'||V_Fri_Id||'>');
          END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
          --
          GOTO Next_Sec;
        END IF;
dbms_output.put_line('Inserting... '
                   ||Sec_Rec.Fri_Ticker
                   || ' '
                   ||V_Next_Pay_Date
                   || ' '
                   || T_Values('CP_T')
                   || ' '
                   || Sec_Rec.Primary_Currency
                   || ' '
                   || W_Value_Date
                    );
-- Ajout - JSL - 08 juillet 2014
        IF G_Monitor_Level > 2
        THEN
          manage_prc_msgs_det.write_log( G_Log_Handle, 'setting the payment at line '||$$PLSQL_LINE );
        END IF;
        IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
        BEGIN --{
          Pgmloc := 3260;
          UPDATE Interest_Schedule
             SET Interest_Amount   = T_Values( 'CP_T' )
               , Interest_Currency = Sec_Rec.Primary_Currency
               , Payment_Flag      = 'Y'
  -- Modification - JSL - 05 novembre 2010
  --           , Ex_Date           = V_Value_Date
               , Ex_Date           = W_Value_Date
  -- Fin de modification - JSL - 05 novembre 2010
               , Last_User         = Constants.Get_User_Id
               , Last_Stamp        = SYSDATE
           WHERE Fri_Ticker  = Sec_Rec.Fri_Ticker
             AND Income_Date = V_Next_Pay_Date
             AND Data_Supplier_Code = C_STP
             ;
          IF SQL%RowCount > 0
          THEN --{
            --cnt: int sch
            V_Rows_Upd := V_Rows_Upd + 1;
          ELSE --}{
            ----------------------------------------------
            -- Inserting Record in table Interest_Schedule
            ----------------------------------------------
            Pgmloc := 3270;
            INSERT
              INTO Interest_Schedule
                       (Fri_Ticker,         Income_Date
                       ,Data_Supplier_Code
                       ,Capital_Amount,     Capital_Currency
                       ,Interest_Amount,    Interest_Currency
                       ,Payment_Flag,       Ex_Date
                       ,Last_User,          Last_Stamp
                       )
            VALUES
                       (Sec_Rec.Fri_Ticker,  V_Next_Pay_Date
                       ,C_STP
                       ,NULL,                NULL
                       ,T_Values('CP_T'),    Sec_Rec.Primary_Currency
  -- Modification - JSL - 05 novembre 2010
  --                   ,'Y',                 W_Value_Date
                       ,'Y',                 V_Value_Date
  -- Fin de modification - JSL - 05 novembre 2010
                       ,G_User,              SYSDATE
                       );
            --cnt: ins int_sch
            V_Rows_Ins := V_Rows_Ins + 1;
          END IF; --}
          Pgmloc := 3280;
  -- Modification - JSL - 18 octobre 2010
  --      IF V_Int_Calc_Parms.Generate_Int_Sch_Flag  = 'Y'
  --      OR V_Int_Calc_Parms.Interest_Schedule_Flag = 'N'
          IF V_Int_Calc_Parms.Interest_Schedule_Flag = 'N'
  -- Fin de modification - JSL - 18 octobre 2010
          THEN
            Pgmloc := 3290;
            UPDATE Interest_Calc_Parms
               SET Interest_Schedule_Flag = 'Y'
  -- Do not stop the generation as they are required by SAS
  --             , Generate_Int_Sch_Flag  = 'N'
                 , Last_User              = Constants.Get_User_Id
                 , Last_Stamp             = SYSDATE
             WHERE Fri_Ticker             = Sec_Rec.Fri_Ticker;
             --cnt: int_calc_parms?
          END IF;
        EXCEPTION
        WHEN OTHERS THEN
dbms_output.put_line('SQLCODE : '||SQLCODE ||'  Pgmloc '||pgmloc);
dbms_output.put_line('NOT INSERTED Inserting... '
                   ||Sec_Rec.Fri_Ticker
                   || ' '
                   ||V_Next_Pay_Date
                   || ' '
                   || T_Values('CP_T')
                   || ' '
                   || Sec_Rec.Primary_Currency
                   || ' '
                   || V_Value_Date
                    );
        END; --}
-- Ajout - JSL - 08 juillet 2014
        IF G_Monitor_Level > 2 AND G_Get_Timing
        THEN
          G_End_Time := DBMS_UTILITY.Get_Time;
          manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
        END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
      ELSE --}{
        ---------------------------------
        -- Counting Rows rejected
        ---------------------------------
        Pgmloc := 3300;
        --
        --cnt: rej other
        V_Rows_Rej := V_Rows_Rej + 1;
-- Ajout - JSL - 08 juillet 2014
        IF G_Monitor_Level > 2
        THEN
          manage_prc_msgs_det.write_log( G_Log_Handle, 'Rejected - delta in accrued interest not big enough  fri_id <'||V_Fri_Id||'>');
        END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
        --
      END IF; --}
    EXCEPTION
    WHEN ZERO_DIVIDE THEN
      Pgmloc := 3310;
      ---------------------------------
      -- Counting Rows rejected
      ---------------------------------
      V_Rows_Rej := V_Rows_Rej + 1;
      --
      GOTO Next_Sec;
    END; --}
    --
<< Next_Sec >>
    NULL;
-- Ajout - JSL - 08 juillet 2014
    IF G_Monitor_Level > 2
    THEN
      manage_prc_msgs_det.write_log( G_Log_Handle, 'Looping for next security');
    END IF;
    IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
  END LOOP; --} Get_Sec
  --
  Pgmloc := 3320;
-- Ajout - JSL - 08 juillet 2014
  Pricing_Msgs_Rec.Remarks          := NULL;
  Pricing_Msgs_Rec.Value_01         := V_Rows_Read;
  Pricing_Msgs_Rec.Value_02         := V_Rows_Ins;
  Pricing_Msgs_Rec.Value_03         := V_Rows_Rej;
  Pricing_Msgs_Rec.Value_04         := V_Rows_Upd;
  --
  Pgmloc := 3322;
  IF G_Monitor_Level > 2
  THEN
    manage_prc_msgs_det.write_log( G_Log_Handle, 'Committing at line '||$$PLSQL_LINE );
  END IF;
  IF G_Get_Timing THEN G_Start_Time := DBMS_UTILITY.Get_Time; END IF;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , Success
                                     );
  Pgmloc := 3324;
  COMMIT;
  IF G_Monitor_Level > 2 AND G_Get_Timing
  THEN
    G_End_Time := DBMS_UTILITY.Get_Time;
    manage_prc_msgs_det.write_log( G_Log_Handle, 'Took '||(G_End_Time - G_Start_Time )/100.0||' seconds.' );
  END IF;
-- Fin d'ajout - JSL - 08 juillet 2014
  --
  P_Success := TRUE;
  P_Message := NULL;
  -----------------------------------------------------------------------------
  -- Adding to log file # records read,inserted,updated,corrected and rejected
  -----------------------------------------------------------------------------
  Pgmloc := 3330;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'175'
                                   , 'N'
                                   , 'Read'
                                   , V_Rows_Read
                                   , V_Message
                                   );
  Pgmloc := 3340;
  Load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 3350;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'175'
                                   , 'N'
                                   , 'Inserted'
                                   , V_Rows_Ins
                                   , V_Message
                                   );
  Pgmloc := 3360;
  Load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 3370;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'175'
                                   , 'N'
                                   , 'Rejected'
                                   , V_Rows_Rej
                                   , V_Message
                                   );
  Pgmloc := 3380;
  Load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  ----------------------------
  -- process terminating at...
  ----------------------------
  Pgmloc := 3390;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'122'
                                   , 'N'
                                   , TO_CHAR( SYSDATE
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  Pgmloc := 3400;
  load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , P_Success
                                );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 3410;
  Cntl_Rec.Log_File_Open := FALSE;
  Load_Supplier_Rules.File_Close_All( Pricing_Msgs_Rec
                                    , P_Success
                                    );
  IF ( NOT P_SUCCESS ) THEN GOTO GET_OUT; END IF;
  --
  Pgmloc := 3420;
  UPDATE Break_Points
     SET Status     = 'DONE'
       , Nb_Records = Pricing_Msgs_Rec.Value_01
       , Last_User  = G_User
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  Pgmloc := 3430;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'160'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  Pgmloc := 3440;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'162'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  GOTO Upd_Prc_Msg;
  --
<<GET_OUT>>
  Pgmloc := 3450;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'170'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  Pgmloc := 3460;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'172'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
  --
<<Upd_Prc_Msg>>
  --
  Pgmloc := 3470;
  P_Message := NVL( P_Message, Pricing_Msgs_Rec.Err_Text );
  Pricing_Msgs_Rec.Value_01         := V_Rows_Read;
  Pricing_Msgs_Rec.Value_02         := V_Rows_Ins;
  Pricing_Msgs_Rec.Value_03         := V_Rows_Rej;
  Pricing_Msgs_Rec.Value_04         := V_Rows_Upd;
  --
  Pgmloc := 3480;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , Success
                                     );
  Pgmloc := 3490;
  COMMIT;
  --
EXCEPTION
WHEN OTHERS THEN
  P_Message := SUBSTR( 'At Pgmloc : '
                    || TO_CHAR( Pgmloc )
                    || ' '
                    || SUBSTR( SQLERRM( SQLCODE ), 1, 255 )
                     , 1
                     , 255
                     );
  IF Pricing_Msgs_Rec.Err_Text IS NULL THEN
    Pricing_Msgs_Rec.Err_Text := P_Message;
  END IF;
  Pgmloc := 3500;
  ROLLBACK;
  Pgmloc := 3510;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , P_Success
                                     );
  Pgmloc := 3520;
  commit;
  IF Cntl_Rec.Log_File_Open THEN
    Pgmloc := 3530;
    load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                  , Pricing_Msgs_Rec.Msg_Text
                                  , Pricing_Msgs_Rec
                                  , P_Success
                                  );
  END IF;
  --
  Load_Supplier_Rules.File_Close_All( Pricing_msgs_Rec
                                    , P_Success
                                    );
  --
  P_Message := Pricing_Msgs_Rec.Err_Text;
  P_Success := FALSE;
  --
END CAF_Interest_Payment; --}}
--



procedure populate_ex_date_for_one
        ( p_fri_ticker            in     securities.fri_ticker%type
        , p_log_file              in     utl_file.file_type
        , p_income_date           in     date
        , p_Inc_Freq_Value        in     securities.income_frequency_value%type
        , p_security_type         in     securities.security_type_code%type
        , p_Indicated_Annual_Rate in     securities.Indicated_Annual_Rate%type
        , p_primary_currency      in     securities.primary_currency%type
        , p_sdai                  in     number
        , p_fri_id                in     tickers.ticker%type
        , p_Db_Parms              in out nocopy ARCHIVES.DB_PARMS
        , Pricing_Msgs_Rec        IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        , P_Success                  OUT BOOLEAN
        )
is --{
  C_Form_Set_Code    CONSTANT Formulae_Set.Code%TYPE     := 'INT_PAY';
  C_Form_Gen_Code    CONSTANT Formulae_Generic.Code%TYPE := 'INTEREST_PAYMENT';
  C_Data_Supp_Code   CONSTANT Data_Suppliers.Code%TYPE   := 'FRI';
  V_Dynamic          BOOLEAN;
  V_Next_Pay_Date    DATE;

  v_parent_program_id  ops_scripts.program_id%type;


  v_supplier_code    Data_Suppliers.Code%TYPE;
  v_delivery_time    Data_Supply_Schedule.Delivery_Time%TYPE;
  v_max_dt           date;
  v_success          boolean := true;
  v_skip             boolean := false;
  v_message          pricing_msgs.err_text%type;

  v_cur_dt           date;
-- Ajout - JSL - 10 mai 2011
  v_prev_dt          date;
-- Fin d'ajout - JSL - 10 mai 2011
  v_Unadj_Price_Rec  OBJ_SECURITY_PRICES;
  v_Adj_Price_Rec    OBJ_SECURITY_PRICES;

  v_accrued_t        security_prices.Accrued_Interest%type;
  v_accrued_t1       security_prices.Accrued_Interest%type;

  T_Values           Formulae.T_Formulae;

  v_init_case        boolean;

  V_Row_Count        INTEGER;
  V_Rows_Upd         NUMBER := 0;

  cursor cur_dt(i_fri_ticker tickers.fri_ticker%type) is
    select min(income_date) as min_dt
         , max(income_date) as max_dt
    from interest_schedule a where 1=1
    and fri_ticker = i_fri_ticker
    ;
  r_dt  cur_dt%rowtype;

  CURSOR Get_From_Interest_Schedule
       ( I_Fri_Ticker Securities.Fri_Ticker%TYPE
       , I_Value_Date DATE
       )
  IS
    SELECT MIN( Income_Date ) as next_interest_Date
    FROM Interest_Schedule
    WHERE Fri_Ticker   = I_Fri_Ticker
    AND Income_Date >= I_Value_Date
    AND Income_Date <= ADD_MONTHS( I_Value_Date, 1 )
    AND Data_Supplier_Code = C_STP
    ;
  R_From_Interest_Schedule  Get_From_Interest_Schedule%rowtype;

  procedure close_all_cursors is
  begin
    if Get_From_Interest_Schedule%isopen then
      close Get_From_Interest_Schedule;
    end if;
    if cur_dt%isopen then
      close cur_dt;
    end if;
  end;
begin --{
  Pgmloc := 3540;
  p_success := false;

  if pricing_msgs_rec.program_id > 9999 then
    v_parent_program_id := trunc(pricing_msgs_rec.program_id/100);
  else
    v_parent_program_id := pricing_msgs_rec.program_id;
  end if;

  Pgmloc := 3550;
  open cur_dt(p_fri_ticker);
  Pgmloc := 3560;
  fetch cur_dt into r_dt;
  close cur_dt;
  Pgmloc := 3570;
  if r_dt.min_dt is null then
    --no schedule --> skipped
    v_message := 'no schedule for FRI_ID: '||security.get_fri_id(p_fri_ticker);
    Pgmloc := 3580;
    Load_supplier_Rules.Write_file( p_log_file
                                  , v_message
                                  , Pricing_Msgs_Rec
                                  , v_Success
                                  );
    if not v_success then
      pricing_msgs_rec.remarks := pricing_msgs_rec.err_text;
    end if;
    count_line_processed(c_cnt_caf_ski_no_sch, pricing_msgs_rec, 1);

    goto End_Loop; ------>>>>
  end if;




  Pgmloc := 3590;
--  official_prices2.get_supplier_info(
  official_prices.get_supplier_info( p_security_type
                                    , v_supplier_code
                                    , v_delivery_time
                                    , pricing_msgs_rec
                                    , v_success
                                    );
  IF NOT v_Success THEN raise e_program_error; END IF;

  Pgmloc := 3600;
  official_prices.set_supplier_delivery_info(
                                               v_supplier_code
                                             , v_delivery_time
                                             );


  Pgmloc := 3610;
  v_cur_dt := p_income_date;

  if v_cur_dt < r_dt.min_dt then
    v_cur_dt := r_dt.min_dt;
  end if;

  v_max_dt := constants.get_site_date;
  if v_max_dt > r_dt.max_dt then
    v_max_dt := r_dt.max_dt;
  end if;

  if v_cur_dt > v_max_dt then
    Pgmloc := 3620;
-- Modification - JSL - 10 mai 2011
--  v_message := 'Start date ['||v_max_dt||']'
    v_message := 'Start date ['||v_cur_dt||']'
-- fin de modification - JSL - 10 mai 2011
      ||' is after end date ['||v_max_dt||'] '
      ||' for fri_id: '||security.get_fri_id(p_fri_ticker);

    Pgmloc := 3630;

    Load_supplier_Rules.Write_file( P_Log_File
                                 , V_Message
                                 , Pricing_Msgs_Rec
                                 , v_Success
                                 );
    if not v_success then
      pricing_msgs_rec.remarks := pricing_msgs_rec.err_text;
    end if;
    count_line_processed(c_cnt_caf_ski_min_gt_max, pricing_msgs_rec, 1);

    goto End_Loop; ------>>>>
  end if;

  --
  --reset prices and ex dates before re-calculation
  --
  Pgmloc := 3640;
  UPDATE Interest_Schedule
     SET Interest_Amount   = null
       , Interest_Currency = null
       , Ex_Date           = null
       , Last_User         = Constants.Get_User_Id
       , Last_Stamp        = SYSDATE
   WHERE Fri_Ticker  = p_Fri_Ticker
     AND Income_Date >= v_cur_dt
     AND Data_Supplier_Code = C_STP
     AND Ex_Date is not null
     ;
  count_line_processed( c_cnt_popexd_cleaned, pricing_msgs_rec, sql%rowcount );


  Pgmloc := 3650;
  --Start from 1 month before the "event"
  v_cur_dt := add_months(v_cur_dt, -1);
  v_init_case := true;
-- Ajout - JSL - 10 mai 2011
  v_prev_dt := v_cur_dt;
-- Fin d'ajout - JSL - 10 mai 2011

  --for i in 1..100
  Pgmloc := 3660;
  loop
    exit when v_cur_dt > v_max_dt;


    Pgmloc := 3670;
    OFFICIAL_PRICES.GET_PRICE( P_Fri_Ticker
                              , v_cur_dt
                              , P_Db_Parms
                              , v_Unadj_Price_Rec
                              , v_Adj_Price_Rec
                              , Pricing_Msgs_Rec
                              , v_Success
                              );
    if not v_success then
      raise e_program_error;
    end if;

    Pgmloc := 3680;
    if v_init_case then
      v_init_case := false;
      v_accrued_t1 := v_UNADJ_PRICE_REC.Accrued_Interest;
      goto next_rec; ------>>>>>>
    elsif v_UNADJ_PRICE_REC.Accrued_Interest is null then
      v_accrued_t1 := null;
      goto next_rec; ------>>>>>>
    elsif v_accrued_t1 is null then
      v_accrued_t1 := v_UNADJ_PRICE_REC.Accrued_Interest;
      goto next_rec; ------>>>>>>
    else
      v_accrued_t := v_UNADJ_PRICE_REC.Accrued_Interest;
    end if;


    Pgmloc := 3690;
    ---------------------------------------
    --  Calculate The CAF Interest Payment
    ---------------------------------------
    BEGIN --{
      IF  v_accrued_t1 <= 0
      THEN
        Pgmloc := 3700;
        --V_Rows_Rej := V_Rows_Rej + 1;
--      count_line_processed(c_cnt_caf_ski_acc_neg, pricing_msgs_rec, 1);
        goto next_rec; ------>>>>>>
      ELSIF ( v_accrued_t1 - v_accrued_t) > ( p_SDAI * v_accrued_t1 )
      THEN --{
        Pgmloc := 3710;
        ---------------------------------------
        --  Getting Coupon Value per unit CP_T
        ---------------------------------------
        T_Values.DELETE();
        --
        T_Values('AI_T')   := v_accrued_t;
        T_Values('AI_T1')  := v_accrued_t1;
        T_Values('CPV_T')  := p_Indicated_Annual_Rate;
        T_Values('FREQ_T') := p_Inc_Freq_Value;
        --
        Pgmloc := 3720;
        Formulae.Apply( C_Data_Supp_Code
                      , C_Form_Set_Code
                      , C_Form_Gen_Code
                      , T_Values
                      , V_Dynamic
                      , v_Success
                      , V_Message
                      );
        IF ( NOT v_SUCCESS ) THEN
--        raise e_program_error;
          Pgmloc := 3730;
          Load_supplier_Rules.Write_file( P_Log_File
                                       , p_Fri_Id||' '||V_Message
                                       , Pricing_Msgs_Rec
                                       , v_Success
                                       );
          goto next_rec;
        END IF;

        --
        -- retrieve next payment
        --
        Pgmloc := 3740;
        open Get_From_Interest_Schedule( P_Fri_Ticker, v_cur_dt);
        Pgmloc := 3750;
        fetch Get_From_Interest_Schedule into R_From_Interest_Schedule;
        Pgmloc := 3760;
        if Get_From_Interest_Schedule%notfound
          or R_From_Interest_Schedule.next_interest_Date is null
        then
          close Get_From_Interest_Schedule;
          Pgmloc := 3770;
          Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'251'
                                          , 'N'
                                          , v_cur_dt
                                          , p_Fri_ID
                                          , V_Message
                                          );
          Pgmloc := 3780;

          Load_supplier_Rules.Write_file( P_Log_File
                                       , V_Message
                                       , Pricing_Msgs_Rec
                                       , v_Success
                                       );
--        if not v_success then
--          pricing_msgs_rec.remarks := pricing_msgs_rec.err_text;
--        end if;
--        pricing_msgs_rec.err_text := v_message;
--        raise e_program_error;
          goto next_rec;
        end if;
        Pgmloc := 3790;
        close Get_From_Interest_Schedule;




        Pgmloc := 3800;
        UPDATE Interest_Schedule
           SET Interest_Amount   = T_Values( 'CP_T' )
             , Interest_Currency = p_Primary_Currency
             , Payment_Flag      = 'Y'
             , Ex_Date           = v_cur_dt
             , Last_User         = Constants.Get_User_Id
             , Last_Stamp        = SYSDATE
         WHERE Fri_Ticker  = p_Fri_Ticker
           AND Income_Date = R_From_Interest_Schedule.next_interest_Date
           AND Data_Supplier_Code = C_STP
           ;
        V_Row_Count := SQL%ROWCOUNT;
        IF V_Row_Count > 0
        THEN --{
          V_Rows_Upd := V_Rows_Upd + 1;
          count_line_processed( c_cnt_popexd_updated, pricing_msgs_rec, V_Row_Count );
        ELSE --}{
          v_message := 'No update ?! for fri_id: '||p_fri_id
            ||' and date: '
            ||to_char(R_From_Interest_Schedule.next_interest_Date, 'yyyy/mm/dd')
            ;
          Load_supplier_Rules.Write_file( P_Log_File
                                       , V_Message
                                       , Pricing_Msgs_Rec
                                       , v_Success
                                       );
          raise e_program_error;
        end if;


      ELSE --}{

        ---------------------------------
        -- Counting Rows rejected
        ---------------------------------
        Pgmloc := 3810;
        --
        --V_Rows_Rej := V_Rows_Rej + 1;
        --no count here, loop based on dates, not securities
        null;
        --
      END IF; --}
    EXCEPTION
    WHEN ZERO_DIVIDE THEN

      Pgmloc := 3820;
      ---------------------------------
      -- Counting Rows rejected
      ---------------------------------
      --V_Rows_Rej := V_Rows_Rej + 1;
--    count_line_processed(c_cnt_caf_ski_zero_div, pricing_msgs_rec, 1);

      --
      GOTO next_rec;
    END; --}



<<next_rec>>
    Pgmloc := 3830;
    v_cur_dt := holiday.Get_Next_Date_by_Pgmid( v_cur_dt
                                              , v_parent_program_id);
-- Ajout - JSL - 10 mai 2011
    IF  V_Cur_Dt      > V_Max_Dt
    AND V_Prev_Dt + 1 < V_Cur_Dt
    -- Max date is during the week-end or on an holiday
    THEN
      -- Empecher que la prochaine fois, nous ayons la meme condition
      V_Prev_Dt := V_Cur_Dt;
      V_Cur_Dt  := V_Max_Dt;
    ELSE
      V_Prev_Dt := TRUNC( V_Unadj_Price_Rec.Delivery_Date );
    END IF;
-- Fin d'ajout - JSL - 10 mai 2011
    v_accrued_t1 := v_UNADJ_PRICE_REC.Accrued_Interest;
  end loop;
  Pgmloc := 3840;
  count_line_processed( c_cnt_popexd_treated, pricing_msgs_rec, 1 );

<<End_Loop>>

  Pgmloc := 3850;
  p_success := true;


exception
  when e_program_error then
    if pricing_msgs_rec.err_text is null then
      pricing_msgs_rec.err_text := v_message;
    end if;
    pricing_msgs_rec.err_text := substr(
      pricing_msgs_rec.err_text||' '||$$plsql_unit||'@'||pgmloc
      ,1,255);
    close_all_cursors;
    p_success := false;

  when others then
    pricing_msgs_rec.err_text := substr(
      'Err@'||pgmloc||' '
      ||'Fri_Ticker : '''||P_Fri_Ticker||''' '
      ||dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      ,1,255);
    close_all_cursors;
    p_success := false;
end; --}}



------------------------------------------------------------------------
--
-- CAF 2 - update ALL int_sch with accrued interest difference found
-- process securities in prices_to_recalc, with code POPEXD
--
-- log file opened by calling procedure, param: p_cntl_rec.log_file
-- log file closed by calling procedure !!!
procedure ex_date_to_recalc
        ( P_Program_Id    IN     Ops_Scripts.Program_Id%TYPE
        , P_Value_Date    IN     DATE
        , P_Commit_Freq   IN     INTEGER
        , P_Cntl_Rec      IN OUT NOCOPY Product_Component.R_Parameters
        , P_Record_Read      OUT INTEGER
        , P_Success          OUT BOOLEAN
        , P_Message          OUT VARCHAR2
        )
IS --{
  Pricing_Msgs_Rec     Pricing_Msgs%ROWTYPE;
  V_Commit_Freq        NUMBER;
  v_success            boolean := true;
  V_Message            pricing_msgs.err_text%type;
  V_Value_Date         DATE;
  V_SDAI               NUMBER;
  V_Fri_ID             Tickers.Ticker%TYPE;
  Previous_Fri_Ticker  Securities.Fri_Ticker%TYPE;
  V_Adj_Prc_Flag       Sources.Abs_Adjust_Prices_Flag%TYPE;

  Commit_Count         INTEGER;
  V_System_Date        DATE   := SYSDATE;
  v_parent_program_id  ops_scripts.program_id%type;
  V_P_to_R             Prices_to_Recalc%ROWTYPE;
  v_Db_Parms           ARCHIVES.DB_PARMS;



  CURSOR Cur_Securities_To_Recalc
       ( I_Cutoff_Date DATE
       ) IS
  SELECT R.*
       , R.Rowid
    FROM Prices_to_Recalc R
   WHERE Recalc_Type_Code = c_popexdate  --populate ex_date
     AND Updated_Date IS NULL
     AND Effective_Date = (
         SELECT MIN( Effective_Date )
           FROM Prices_to_Recalc R1
          WHERE R1.Fri_Ticker = R.Fri_Ticker
            AND R1.Recalc_Type_Code = R.Recalc_Type_Code
            AND R1.Updated_Date IS NULL
         )
     AND Effective_Date >= I_Cutoff_Date
  ORDER BY Last_Stamp
  ;


  CURSOR Cur_Get_Sec( i_fri_ticker securities.fri_ticker%type )  IS
  SELECT s.Fri_Ticker ,
         s.Income_Frequency_Value as Inc_Freq_Value,
         f.Code                   as Freq_Code,
         f.Annual_Validity_Flag   as Annual_Valid_Flag,
         ts.Active_Flag,
         s.security_type_code,
         s.Primary_Source_Code,
         s.Primary_Currency,
         s.Indicated_Annual_Rate
  FROM   Securities                s,
         Trading_Status            ts,
         Frequencies               f
  WHERE  s.Fri_Ticker            = i_fri_ticker
  AND    s.Trading_Status_Code    = ts.Code
  AND    s.Income_Frequency_Value = f.Value
  ;
  r_get_sec  cur_get_sec%rowtype;
  -------------------------------------------------------------------
  -- Cursor Get_Adj_Flag verify if the source contain adjusted prices
  -------------------------------------------------------------------
  CURSOR Get_Adj_Flag
       ( I_Source_Code Sources.Code%TYPE
       ) IS
  SELECT Abs_Adjust_Prices_Flag
    FROM Sources
   WHERE Code = I_Source_Code;
  --
  -----------------------------
  -- Cursor Get_Int_Calc_Parms
  -----------------------------
  CURSOR Get_Int_Calc_Parms
       ( I_Fri_Ticker Interest_Calc_Parms.Fri_Ticker%TYPE
       ) IS
  SELECT *
    FROM Interest_Calc_Parms
   WHERE Fri_Ticker = I_Fri_Ticker;

begin --{

  P_Record_Read := 0;

  Pgmloc := 3860;
  P_Success := false;
  P_Message := NULL;
  V_Commit_Freq := NVL(P_Commit_Freq,1);  --process is long --> default 1
  if p_program_id > 9999 then
    v_parent_program_id := trunc(p_program_id/100);
  else
    v_parent_program_id := p_program_id;
  end if;

  Commit_Count := 0;

  --fake savepoint, only to have it existing for the exception handler...
  --  real one in cursor loop below
  Pgmloc := 3870;
  savepoint a;

  Pgmloc := 3880;
  V_Value_Date := TRUNC(NVL(P_Value_Date,Constants.Get_Site_Date));


  --
  Pgmloc := 3890;
  Load_Supplier_Rules.Initialize_Prc_Msgs( P_Program_Id
                                         , V_System_Date
                                         , p_value_date
                                         , Pricing_Msgs_Rec
                                         , v_Success
                                         );
  IF NOT v_Success THEN raise e_program_error; END IF;
  --
  Pricing_Msgs_Rec.Successfull_Flag := 'N';
  Pricing_Msgs_Rec.Msg_Text         := 'Program id is '
                                    || to_char(P_Program_id);
  Pricing_Msgs_Rec.Msg_Text_2       := 'Program in progress';

  Pricing_Msgs_Rec.Value_01 := 0;
  Pricing_Msgs_Rec.Value_02 := 0;
  Pricing_Msgs_Rec.Value_03 := 0;
  Pricing_Msgs_Rec.Value_04 := 0;
  Pricing_Msgs_Rec.Value_05 := 0;
  Pricing_Msgs_Rec.Value_06 := 0;
  --
  Pgmloc    := 3900;
  Global_File_Updates.Update_Pricing_Msgs
      (P_Program_Id,
       V_System_Date,
       FALSE,
       Pricing_Msgs_Rec);
  Pgmloc := 3910;
  COMMIT;

  --fake savepoint, only to have it existing for the exception handler...
  --  real one in cursor loop below
  Pgmloc := 3920;
  savepoint a;

  Pgmloc    := 3930;
  Load_supplier_Rules.Write_file( p_cntl_rec.log_file
                                , c_line||chr(10)||Pricing_Msgs_Rec.Msg_Text_2
                                , Pricing_Msgs_Rec
                                , v_Success
                                );
  if not v_success then raise e_program_error; end if;



  ----------------------------------------------------------
  -- Get argument for sudden diminution in accrued interest
  ----------------------------------------------------------
  V_SDAI := TO_NUMBER( Scripts.Get_Arg_Value( v_parent_program_id, 'SDAI' ) );
  IF V_SDAI > 0
  AND V_SDAI < 1
  THEN --{
    NULL;
  ELSE --}{
    Pgmloc := 3940;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'015'
                                     , 'N'
                                     , V_SDAI
                                     , V_Message
                                     );

    Pgmloc := 3950;
    Load_supplier_Rules.Write_file( p_Cntl_Rec.Log_File
                                  , V_Message
                                  , Pricing_Msgs_Rec
                                  , v_Success
                                  );
    if not v_success then
      pricing_msgs_rec.remarks := pricing_msgs_rec.err_text;

    end if;
    Pricing_Msgs_Rec.err_text := V_Message;
    raise e_program_error;
  END IF; --}

  --
  -- Initialize / configure calls to official_prices.get_price
  --
  Pgmloc := 3960;
  v_db_parms.root_table := c_root_table;
  Pgmloc := 3970;
  Archives.Get_DB_Parms( v_DB_Parms
                       , v_Success
                       , Pricing_Msgs_Rec.Err_Text
                       );
  IF NOT v_Success THEN raise e_program_error; END IF;



  Pgmloc := 3980;
  FOR STR_Rec IN Cur_Securities_To_Recalc( G_Prc_Cutoff_Date )
-- Fin de modification - JSL - 05 novembre 2010
  LOOP --{
    Pgmloc := 3990;
    P_Record_Read := P_Record_Read + 1;
    count_line_processed(c_cnt_Popexd_Read, pricing_msgs_rec, 1);

    Pgmloc := 4000;
    savepoint a;

    IF STR_Rec.Fri_Ticker = Previous_Fri_Ticker
    THEN
      --skip duplicates
      count_line_processed(c_cnt_Popexd_Already_Done, pricing_msgs_rec, 1);
      GOTO Next_Sec; ------>>>>
    END IF;
    Pgmloc := 4010;
    V_P_to_R.Completed_Date := NULL;
    BEGIN
      SELECT *
        INTO V_P_to_R
        FROM Prices_To_Recalc
       WHERE Rowid = STR_Rec.Rowid
             ;
    EXCEPTION
    WHEN No_Data_Found
    THEN
      NULL;
    END;
    Pgmloc := 4020;
    IF V_P_to_R.Completed_Date IS NOT NULL
    THEN
      count_line_processed(c_cnt_Popexd_Already_Done, pricing_msgs_rec, 1);
      GOTO Next_Sec; ------>>>>
    END IF;

    Previous_Fri_Ticker := STR_Rec.Fri_Ticker;

    --get security info
    Pgmloc := 4030;
    open Cur_Get_Sec( STR_Rec.fri_ticker );
    Pgmloc := 4040;
    FETCH Cur_Get_Sec INTO r_get_sec;
    if Cur_Get_Sec%notfound then
      close Cur_Get_Sec;

      manage_utl_file.write_file
                    ( p_Cntl_Rec.log_file,
                      'Security / trading status not found for fri_ticker: '
                      ||STR_Rec.fri_ticker||' ???'
                    , v_success
                    , pricing_msgs_rec.err_text
                    );
      if not v_success then
        pricing_msgs_rec.remarks := pricing_msgs_rec.err_text;
      end if;
      raise e_program_error;
    end if;
    Pgmloc := 4050;
    close Cur_Get_Sec;

    ----------------------------------------------------------------------
    -- IF Primary_Source_Code or primary_currency are null skip the record
    ----------------------------------------------------------------------
    Pgmloc := 4060;
    IF r_get_sec.Primary_Source_Code IS NULL
    OR r_get_sec.Primary_Currency    IS NULL
    THEN  --{
      Pgmloc := 4070;
      ---------------------------------
      -- Counting Rows rejected
      ---------------------------------
      --V_Rows_Rej := V_Rows_Rej + 1;
      count_line_processed(c_cnt_caf_ski_pri, pricing_msgs_rec, 1);
      --
-- Modification - JSL - 24 mai 2011
--    GOTO Next_Sec;
      GOTO Sec_Is_Done;
-- Fin de modification - JSL - 24 mai 2011
    END IF; --}


    -----------------------------------------------------------
    -- If the source contain adjusted prices ignore the record
    -----------------------------------------------------------
    Pgmloc := 4080;
    OPEN Get_Adj_Flag(r_get_sec.Primary_Source_Code);
    Pgmloc := 4090;
    FETCH Get_Adj_Flag INTO V_Adj_Prc_Flag;
    Pgmloc := 4100;
    CLOSE Get_Adj_Flag;
    Pgmloc := 4110;
    IF V_Adj_Prc_Flag = 'Y'
    THEN --{
      Pgmloc := 4120;
      ---------------------------------
      -- Counting Rows rejected
      ---------------------------------
      --V_Rows_Rej := V_Rows_Rej + 1;
      count_line_processed(c_cnt_caf_ski_adj, pricing_msgs_rec, 1);

-- Modification - JSL - 24 mai 2011
--    GOTO Next_Sec;
      GOTO Sec_Is_Done;
-- Fin de modification - JSL - 24 mai 2011
    END IF; --}
    --


    --V_Affected := False;
    Pgmloc := 4130;
    V_Fri_ID := Security.Get_Fri_ID ( r_get_sec.Fri_Ticker);


    --recalculates amounts for 1 security
    Pgmloc := 4140;
    populate_ex_date_for_one( r_get_sec.Fri_Ticker
                            , p_Cntl_Rec.Log_File
                            , STR_Rec.effective_date
                            , r_get_sec.Inc_Freq_Value
                            , r_get_sec.security_type_code
                            , r_get_sec.Indicated_Annual_Rate
                            , r_get_sec.primary_currency
                            , v_sdai
                            , v_fri_id
                            , v_db_parms
                            , Pricing_Msgs_Rec
                            , v_Success
                            );
    if not v_success then raise e_program_error; end if;
    --
<< Sec_Is_Done >>
    --
    Pgmloc := 4150;
    UPDATE Prices_to_Recalc
    SET Updated_Date = SYSDATE
      , Completed_Date = SYSDATE
    WHERE Fri_Ticker = Previous_Fri_Ticker
    AND Recalc_Type_Code = c_popexdate
    AND Updated_Date IS NULL
    ;
    if sql%rowcount = 0 then
      v_message := 'Prices_to_Recalc NOT updated??';
      manage_utl_file.write_file( p_cntl_rec.log_file
                                , v_message
                                , v_success
                                , pricing_msgs_rec.err_text
                                );
      if not v_success then
        pricing_msgs_rec.remarks := pricing_msgs_rec.err_Text;
      end if;

      --set back real error msg
      pricing_msgs_rec.err_Text := v_message;
      raise e_program_error; ------>>>>
    end if;


    --
    Pgmloc := 4160;
    load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                       , Pricing_Msgs_Rec.Date_of_Msg
                                       , Pricing_Msgs_Rec
                                       , v_Success
                                       );
    Commit_Count := Commit_Count + 1;
    IF Commit_Count >= v_Commit_Freq
    THEN
      Pgmloc := 4170;
      COMMIT;
      commit_count := 0;
    end if;
    --



<< Next_Sec >>
    NULL;
  END LOOP; --} Get_Sec
  --

  Pgmloc := 4180;
  savepoint a;


  ----------------------------
  -- process terminating at...
  ----------------------------
  Pgmloc := 4190;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'122'
                                   , 'N'
                                   , TO_CHAR( SYSDATE
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  Pgmloc := 4200;
  load_supplier_Rules.Write_file( p_Cntl_Rec.Log_File
                                , 'Done... '||V_Message
                                , Pricing_Msgs_Rec
                                , v_Success
                                );
  IF ( NOT v_SUCCESS ) THEN raise e_program_error; END IF;
  --
  --file closed by the calling proc...



  --
  Pgmloc := 4210;
  UPDATE Break_Points
     SET Status     = 'DONE'
       , Nb_Records = Pricing_Msgs_Rec.Value_01
       , Last_User  = G_User
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  Pgmloc := 4220;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'160'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  Pgmloc := 4230;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'162'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';
  p_success := true;
  GOTO Upd_Prc_Msg;
  --
<<Upd_Prc_Msg>>
  --
  Pgmloc := 4240;


  if not p_success then
    P_Message := Pricing_Msgs_Rec.Err_Text;
  end if;
  --
  Pgmloc := 4250;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , V_Success
                                     );
  Pgmloc := 4260;
  COMMIT;
  --
EXCEPTION
  when e_program_error then
    rollback to savepoint a;

    if pricing_msgs_rec.err_text is null then
      pricing_msgs_rec.err_text := v_message;
    end if;

    pricing_msgs_rec.err_text := substr(
      pricing_msgs_rec.err_text||' '||$$plsql_unit||'@'||pgmloc
      ,1,255);
    p_message := pricing_msgs_rec.err_text;

    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'170'
                                     , 'N'
                                     , Pricing_Msgs_Rec.Msg_Text
                                     );
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'172'
                                     , 'N'
                                     , Pricing_Msgs_Rec.Msg_Text_2
                                     );
    Pricing_Msgs_Rec.Successfull_Flag := 'N';

    load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                       , Pricing_Msgs_Rec.Date_of_Msg
                                       , Pricing_Msgs_Rec
                                       , v_Success
                                       );
    commit;
    p_success := false;

WHEN OTHERS THEN
  v_Message := substr(
    'Err@'|| Pgmloc || ' '
    ||'Fri_Ticker : '''||Previous_Fri_Ticker||''' '
    || dbms_utility.format_error_stack
    || dbms_utility.format_error_backtrace
    ,1,255);
  rollback to savepoint a;

  IF Pricing_Msgs_Rec.Err_Text IS NULL THEN
    Pricing_Msgs_Rec.Err_Text := v_Message;
  END IF;
  Pgmloc := 4270;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , v_Success
                                     );
  Pgmloc := 4280;
  commit;

  P_Message := Pricing_Msgs_Rec.Err_Text;
  P_Success := FALSE;
  --
end; --}}

procedure interest_payment_to_recalc
        ( P_Program_Id    IN     Ops_Scripts.Program_Id%TYPE
        , P_Value_Date    IN     DATE
        , P_Commit_Freq   IN     INTEGER
        , P_Success          OUT BOOLEAN
        , P_Message          OUT VARCHAR2
        )
is
  V_Commit_Freq      NUMBER;
  V_Value_Date       DATE;
  Cntl_Rec           product_component.R_Parameters;
  Pricing_Msgs_Rec   Pricing_Msgs%ROWTYPE;

  v_child_genint     integer;
  v_child_popexdate  integer;
  v_success_yes_no   Yes_No.Code%TYPE;
  v_success          boolean := true;
  v_message          pricing_msgs.err_text%type;
  V_Record_Read      INTEGER;

  V_Program_Id       Ops_Scripts.Program_Id%TYPE;

begin
  Pgmloc := 4290;
  P_Success := false;

  V_Commit_Freq := NVL(P_Commit_Freq,500);

  V_Value_Date := TRUNC(NVL(P_Value_Date,Constants.Get_Site_Date));

  cntl_rec.Open_Files := false;

  Pgmloc := 4300;
  product_component.Process_Beginning(  P_Program_Id
                                      , V_Value_Date
                                      , v_Commit_Freq
                                      , Cntl_Rec
                                      , Pricing_Msgs_Rec
                                      , v_success
                                      , v_message
                                      );
  IF NOT v_Success THEN raise e_program_error; END IF;

  declare
    tmp ops_scripts_parms.parm_value%type;
    nm  ops_scripts_parms.parm_name%type;
  begin
    Pgmloc := 4310;
    nm := 'CHILD_GENINT';
    tmp := Scripts.Get_Arg_Value( P_Program_Id, nm);
    if tmp is null then
      raise e_program_error;
    end if;
    v_child_genint := P_Program_Id * 100 + tmp;

    Pgmloc := 4320;
    nm := 'CHILD_POPEXD';
    tmp := Scripts.Get_Arg_Value( P_Program_Id, nm);
    if tmp is null then
      raise e_program_error;
    end if;
    v_child_popexdate := P_Program_Id * 100 + tmp;
  exception
    when others then
      pricing_msgs_rec.err_text := 'Param ['||nm||'] missing or incorrect ['||tmp||']';
      raise;
  end;


  Pgmloc := 4330;
  --
  -- call 1: genint
  --
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'252'
                                   , 'N'
                                   , v_child_genint
                                   , V_Message
                                   );
  Pgmloc := 4340;
  Load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , v_Success
                                );
  if not v_success then raise e_program_error; end if;

  pricing_msgs_rec.remarks := v_message;
  Pgmloc := 4350;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                   , Pricing_Msgs_Rec.Date_of_Msg
                                   , Pricing_Msgs_Rec
                                   , v_Success
                                   );
  if not v_success then raise e_program_error; end if;

  --msg if interest_payment_sch_to_recalc failure
  pricing_msgs_rec.err_text := pricing_msgs_rec.remarks;
  pricing_msgs_rec.remarks := ' '; --blank!! because of nvl in prov

  Pgmloc := 4360;

  interest_payment_sch_to_recalc( v_child_genint
                                , v_Commit_Freq
                                , cntl_rec
                                , V_Record_Read
                                , v_success_yes_no
                                , v_Message
                                );
  if v_success_yes_no <> 'Y' then raise e_program_error; end if;
  pricing_msgs_rec.err_text := null;
  pricing_msgs_rec.Value_01 := V_Record_Read;




  Pgmloc := 4370;
  --
  -- call 2: popexdate
  --
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'253'
                                   , 'N'
                                   , v_child_popexdate
                                   , V_Message
                                   );
  Pgmloc := 4380;
  Load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , v_Success
                                );
  if not v_success then raise e_program_error; end if;

  pricing_msgs_rec.remarks := v_message;
  Pgmloc := 4390;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                   , Pricing_Msgs_Rec.Date_of_Msg
                                   , Pricing_Msgs_Rec
                                   , v_Success
                                   );
  if not v_success then raise e_program_error; end if;

  --msg if CAF_Interest_Payment_to_recalc failure
  pricing_msgs_rec.err_text := pricing_msgs_rec.remarks;
  pricing_msgs_rec.remarks := ' '; --blank!! because of nvl in prov

  Pgmloc := 4400;

  ex_date_to_recalc( v_child_popexdate
                    , v_Value_Date
                    , v_Commit_Freq
                    , cntl_rec
                    , V_Record_Read
                    , v_Success
                    , v_Message
                    ) ;
  if not v_success then raise e_program_error; end if;
  pricing_msgs_rec.err_text := null;
  pricing_msgs_rec.Value_02 := V_Record_Read;





  ----------------------------
  -- process terminating at...
  ----------------------------
  Pgmloc := 4410;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'122'
                                   , 'N'
                                   , TO_CHAR( SYSDATE
                                            , 'dd-mon-yyyy hh24:mi:ss' )
                                   , V_Message
                                   );
  Pgmloc := 4420;
  load_supplier_Rules.Write_file( Cntl_Rec.Log_File
                                , V_Message
                                , Pricing_Msgs_Rec
                                , v_Success
                                );
  IF ( NOT v_SUCCESS ) THEN raise e_program_error; END IF;

  Pgmloc := 4430;
  --
  -- ending
  --
  Pgmloc := 4440;
  UPDATE Break_Points
     SET Status     = 'DONE'
       , Nb_Records = Pricing_Msgs_Rec.Value_01
       , Last_User  = G_User
       , Last_Stamp = SYSDATE
   WHERE Program_Id = P_Program_Id;
  --
  Pgmloc := 4450;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'160'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text
                                   );
  Pgmloc := 4460;
  Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'162'
                                   , 'N'
                                   , Pricing_Msgs_Rec.Msg_Text_2
                                   );
  Pricing_Msgs_Rec.Successfull_Flag := 'Y';



  Pgmloc := 4470;
  P_Message := NVL( P_Message, Pricing_Msgs_Rec.Err_Text );
  --
  Pgmloc := 4480;
  load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                     , Pricing_Msgs_Rec.Date_of_Msg
                                     , Pricing_Msgs_Rec
                                     , v_Success
                                     );
  p_success := true;
  Pgmloc := 4490;
  COMMIT;

exception
  when e_program_error then
    if pricing_msgs_rec.err_text is null then
      pricing_msgs_rec.err_text := v_message;
    end if;
    pricing_msgs_rec.err_text := substr(
      pricing_msgs_rec.err_text||' '||$$plsql_unit||'@'||pgmloc
      ,1,255);
    p_message := pricing_msgs_rec.err_text;
    Pgmloc := 4500;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'170'
                                     , 'N'
                                     , Pricing_Msgs_Rec.Msg_Text
                                     );
    Pgmloc := 4510;
    Manage_Messages.Get_Oasis_Message( C_Oasis_Message||'172'
                                     , 'N'
                                     , Pricing_Msgs_Rec.Msg_Text_2
                                     );
    Pricing_Msgs_Rec.Successfull_Flag := 'N';
    load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                       , Pricing_Msgs_Rec.Date_of_Msg
                                       , Pricing_Msgs_Rec
                                       , v_Success
                                       );
    if utl_file.is_open(cntl_rec.log_file) then
      utl_file.fclose(cntl_rec.log_file);
    end if;
    commit;
    p_success := false;

  when others then
    pricing_msgs_rec.err_text := substr(
      'Err@'||pgmloc||' '
      ||dbms_utility.format_error_stack
      ||dbms_utility.format_error_backtrace
      ,1,255);
    p_message := pricing_msgs_rec.err_text;
    load_supplier_rules.Update_Prc_Msgs( P_Program_ID
                                       , Pricing_Msgs_Rec.Date_of_Msg
                                       , Pricing_Msgs_Rec
                                       , v_Success
                                       );
    commit;
    p_success := false;
end;


----

END Generate_Interest_Payments;
/
show error
