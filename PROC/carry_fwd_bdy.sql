CREATE OR REPLACE PACKAGE BODY &&owner..carry_fwd AS
--
-- Corps du package contenant les procedures et autres objets
-- necessaire aux divers carry forward a appliquer
--
-- Codage initial
-- JFCYR 17 nov 1995 req:2727
--
-- Enlever les references aux champs XXX_date_bc pour activer les
-- declencheurs
-- JFCYR 1 dec 1995 req:3017
--
-- Ajout du champ CF_DELIVERY_TIME de la table DATA_SUPPLY_SCHEDULE.
-- Ajustement du code pour tenir compte de ce champ.
-- JFCYR 11 dec 1995 req:3030
--
--   -- Modifs pour carry Fwd des Prix --
-- Si le titre est inactif dans Securities, On copie le prix.
-- Sinon
--   si le titre est inactif dans Security_Trading_Status,
--      Si le select suivant ne ramene rien, On copie le prix.
--      Sinon on ne copie pas le prix.
--   Sinon on copie le prix
--  ** Select a faire **
--  Select 'x'
--  from Security_Prices
--  where fri_ticker = fri_ticker courant
--    and source_code != source_code courant
--    and delivery_date = previous delivery_date
--    and price_date >= price_date de la range traitee;
--
-- JFCYR 24 jan 1996 req:2637-3030
--
-- Ajout du champ Updated_Flag
-- JFCYR 24 Fev 1996 req:3157
--
-- The function 'Validate_Supplier_Chain' has been moved
-- to package : 'Pricing'. So the package name is added wherever the
-- function is used. Phebe(05aug03).
--
-- The function 'Is_It_Intra_Day' has been moved to package
-- 'Supplier'. So the package name is added wherever the function is
-- used.(Phebe, 27 Aug 2003).
--
-- Carrying Forward Active Curves Only
-- Richard (10 Mar 2004)
--
-- The message of Pricing_Msgs.Err_Text is assigned to Remarks column
-- when the successfull_flag is 'Y'.
-- Phebe (14 Jul 2004)
--
-- SSI - 28OCT2005 - Put in global variables the cutoff dates of the table to
--                   so that we don't have to fetch it for each security
--
-- PSM - 18May2006 - Using Oasis_Messages to standardize OutPut Messages.
--
-- JSL - 10nov2006 - Procedure CFW_Reverse_Price_Or_Exch modifiee :
--                   Inserer le prix si la date de prix n'existe pas et que ce
--                   n'est pas avant le cut-off date.  Cela signifie que nous
--                   faisons un reverse carry forward du premier prix
--                   disponible.
--
-- JSL - 23avr2007 - Ajout des champs bid/ask/mid yield.
--
-- RV  - 17Jan2008 - New field added: Exchange_Rates.Ask
--
-- Araisbel 25JAN2008 - 24 fields added on teble Security_Prices
--                   dirty_price,price_mod_duration, bid_dirty_price,
--                   bid_mod_duration, ask_dirty_price, ask_mod_duration,
--                   mid_dirty_price, mid_mod_duration, accrued_interest,
--                   term_in_years, settlement_date, maturity_date,
--                   maturity_type, index_ratio, rpb_factor,effective_date_factor,
--                   next_chg_date_factor, benchmark_code, spread,
--                   spread_from_generic, spread_from_libor, convexity, pvbp,
--                   average_life_date
--
-- JSL - 02jan2009 - Faked Carry forward on GICS_XREF.  The data type needs
--                   a carry forward, but there is nothing to carry forward.
--
-- JSL - 13jan2009 - Correction in On_Security_Prices.  Initialise the
--                   temporary fields at each iteration through the loop.
--
-- JSL - 19aug2009 - Correction in On_Security_Prices.  At maturity, set the
--                   dirty price to par, accrued interest, terms in years and
--                   modified duration to zero and the maturity date.
--
-- RV  - 17oct2009 - New fields to carry Fwd - Prices (Market_Cap),
--                 - Yields (Ask_Yield)
--
-- JSL - 14nov2009 - Show statistics when deleting during a full restart.
--
-- JSL - 12jan2010 - Added field IFRS_Code in Security_Prices.
--
-- SSI - 18Jan2010 - CFW_YIELD_CURVES
--                 - Maintaining new tables yield_curves_not_priced and
--                   yield_curves_price_dates.
--
-- SSI - 08FEB2010 - CFW_YIELD_CURVES
--                 - While checking for yield date to insert must be
--                   in the future, should check for Restart date, not
--                   delivery_date.
--
-- JSL - 19feb2010 - Added hint in retrieval of Security_Prices.
--
-- JSL - 18aug2010 - Do not set the last price to par is the par is more
--                   than 1 % off the actual price.  Also set the bid, ask and
--                   mid to par if they were available.
--
-- JSL - 09nov2010 - Set the last price at 100% when more than 1% of par, but
--                   within 1% of 100.
--
-- mlz - jan2010   - check stp2000 / mf2000 or other
--
-- RV              - 09Aug2011 - 2 new fields added
--                             - High_52W and Low_52W
--
-- JSL - 29nov2011 - Adding 5 more fields.
--
-- JSL - 02mar2012 - Adding carry forward on ds_security_prices.
--                 - The package definition do not change.
--                 - The supplier c_dual_supplier_code is assumed to
--                   supply only ds_security_prices.
--                 - The routine on_security_prices determines if the
--                   package is acting on security_prices or ds_security_prices.
--                 - The routines about security_prices got duplicated and
--                   prototyped for ds_security_prices.
--
-- JSL - 31jul2012 - Use source synonyms on ds_security_prices when finding
--                   the trading status just like the load program.
--
-- RV  - 06oct2012 - Fix the patch created by Sylvain on July 31st.
--
-- JSL - 19dec2013 - OASIS-2536
--                 - Accept 7 days a week loads.
--
-- JSL - 07jan2014 - OASIS-2536
--                 - Do a fast carry forward on week-end and when Montreal
--                   office is closed.  Skip over the intra-day and make the
--                   last of the chain to use the last of the previous day.
--                   This should make the process a lot faster and avoid putting
--                   a few millions rows of north-american carried forward
--                   prices in the database per weekend day.
--
-- JSL - 06mai2014 - OASIS-3773
--                 - Move the validation of supplier chain from the select to
--                   within the loop when doing the selection.
--                 - Do a select * for the rows we have to carry forward, thus
--                   avoiding the necessity to redo the fetch when the carry
--                   forward is accepted.
--
-- JSL - 10sep2014 - OASIS-4238
--                 - Some carry forward will be scheduled through TMS.  We have
--                   to reset the in/out queue to unscheduled at the end of
--                   execution, otherwise the job will restart at their specified
--                   running time even if the carry forward is already done, e.g.
--                   on holidays and during the week-end.
-- mlz - 26nov2014 - oasis-4487 - remove reference/use of synonym proc
--
-- JSL - 14jul2015 - OASIS-5524
--                 - Fallback for hourly suppliers, carry forward from the same
--                   hourly delivery when the supplier is on holiday, if the
--                   supplier have the drp flag set.
--                 - Remove old comments (before 2014) that are adding nothing
--                   to the comprehension of the code.
-- mlz - 04nov2015 - oasis-6241 - added some hints
--
-- JSL - 19Nov2015 - OASIS-6293
--                 - Added more hints to use the second index of security prices
--                   when not all fields of the first index are known.
--
-------------------------------------------------------------------------------
--
Min_Ratio   CONSTANT NUMBER := 0.99;  -- Ratio par_value/price
Max_Ratio   CONSTANT NUMBER := 1.01;
--
-- Ce founisseur n'est pas dans security_prices mais dans DS_Security_Prices.
C_Dual_Supplier_Code CONSTANT Data_Suppliers.Code%TYPE := 'STPD';
--
-- Ajout - JSL - 06 mai 2014
C_Time               CONSTANT VARCHAR2(6) := 'HH24MI';
-- Fin d'ajout - JSL - 06 mai 2014
--
--
-- Les variables et autres objets globaux seront mis Ici
--
    TICKER               SECURITIES.FRI_TICKER%TYPE;
    SC_CODE              SOURCES.CODE%TYPE;
    NAT_CODE             Nations.CODE%TYPE;
    CURRENCY             CURRENCIES.CURRENCY%TYPE;
    DEL_DATE             DATE;
    P_Temp_Date          DATE;
    SUPPLIER_CODE        DATA_SUPPLIERS.CODE%TYPE;
    CFW_PRICE_TYPE       CHAR(1);
    EXCH_CODE            CHAR(1) := 'C';
    SECURITY_PATTERN     SECURITY_PATTERNS.CODE%TYPE;
    INITIALS             USER_LIST.USER_ID%TYPE;
    CFW_COUNT            NUMBER;
    STATUS               NUMBER;
    pgmloc               NUMBER := 0;
    G_Exch_Arch_Cutoff   DATE:=NULL;
    G_Prc_Arch_Cutoff    DATE:=NULL;
    --
    -- Ajoute record pour pouvoir faire l'appelle a Global.
    -- Initialize/Update Pricing Msgs
    Pricing_Msg_Rec      Pricing_Msgs%ROWTYPE;
    G_Program_Id         OPS_Scripts.Program_Id%TYPE;
    G_System_Date        DATE;
--
    -- Variables required to standardize Output Messages. (Phebe,18May2006).
    G_Err_Code           Oasis_Messages.Code%TYPE:= 'PROC-CARRY_FWD-';
    G_Msg_Code           Oasis_Messages.Code%TYPE;
    G_Delete_Flag        Oasis_Messages.Delete_Flag%TYPE := 'N';
--
--    Le Cursor CUR_GET_SEC_INFO va etre utiliser par les procedures
--    qui feront les carry forwards.  Il va chercher les valeurs
--    actuels du record dans la table Security_Prices.
--
    -- Ajoute l'origine de prix pour pouvoir la mettre a jour
    -- correctement lors de la re-application du CFW
    --
    CURSOR CUR_GET_SEC_INFO IS
    SELECT /*+ ALL_ROWS */
           PRICE_DATE,
           PRICE,
           VOLUME,
           HIGH,
           LOW,
           OPEN_PRICE,
           YIELD,
           BOARD_LOT_FLAG,
           LAST_TRADE_TIME,
           NUMBER_OF_TRANS,
           BID_DATE,        BID, Bid_Yield,
           ASK_DATE,        ASK, Ask_Yield,
           MID_DATE,        MID, Mid_Yield,
           PRICE_ORIGIN_CODE,
           DIRTY_PRICE,
           PRICE_MOD_DURATION,
           BID_DIRTY_PRICE,
           BID_MOD_DURATION,
           ASK_DIRTY_PRICE,
           ASK_MOD_DURATION,
           MID_DIRTY_PRICE,
           MID_MOD_DURATION,
           ACCRUED_INTEREST,
           TERM_IN_YEARS,
           SETTLEMENT_DATE,
           MATURITY_DATE,
           MATURITY_TYPE,
           INDEX_RATIO,
           RPB_FACTOR,
           EFFECTIVE_DATE_FACTOR,
           NEXT_CHG_DATE_FACTOR,
           BENCHMARK_CODE,
           SPREAD,
           SPREAD_FROM_GENERIC,
           SPREAD_FROM_LIBOR,
           CONVEXITY,
           PVBP,
           AVERAGE_LIFE_DATE,
           MARKET_CAP
         , IFRS_Code
         , High_52w
         , Low_52w
         , Official_Close
         , Trade_Count
         , Close_Cumulative_Volume
         , Close_Cumulative_Volume_Status
         , Close_Cumulative_Volume_Date
    FROM   Security_Prices
    WHERE  FRI_TICKER         = TICKER
    AND    SOURCE_CODE        = SC_CODE
    AND    PRICE_CURRENCY     = CURRENCY
    AND    DELIVERY_DATE      = DEL_DATE
    AND    DATA_SUPPLIER_CODE = SUPPLIER_CODE;
--
--    Le Cursor CUR_GET_EXCH_INFO est necessaire pour aller
--    chercher les champs qui ont ete modifiee dans le
--    cas d'un changement a un taux de change
--
    CURSOR CUR_GET_EXCH_INFO IS
    SELECT PRICE_DATE, PRICE, ASK
    FROM   EXCHANGE_RATES
    WHERE  FRI_TICKER         = TICKER
    AND    SOURCE_CODE        = SC_CODE
    AND    PRICE_CURRENCY     = CURRENCY
    AND    DELIVERY_DATE      = DEL_DATE
    AND    DATA_SUPPLIER_CODE = SUPPLIER_CODE;
--
-- Le curseur CUR_GET_DS_SEC_INFO va etre utiliser par les procedures
-- qui feront les carry forwards.  Il va chercher les valeurs
-- actuels du record dans la table DS_Security_Prices.
--
    CURSOR CUR_GET_DS_SEC_INFO
         ( I_FRI_Ticker Securities.FRI_Ticker%TYPE
         , I_Nation     Nations.Code%TYPE
         , I_Currency   Currencies.Currency%TYPE
         , I_Del_Date   DATE
         , I_Supplier   Data_Suppliers.Code%TYPE
         ) IS
    SELECT /*+ ALL_ROWS */
           *
      FROM DS_Security_Prices
     WHERE FRI_TICKER         = I_Fri_Ticker
       AND NATION_CODE        = I_Nation
       AND PRICE_CURRENCY     = I_Currency
       AND DELIVERY_DATE      = I_DEL_DATE
       AND DATA_SUPPLIER_CODE = I_SUPPLIER;
--
    DS_SP_VALUES         CUR_GET_DS_SEC_INFO%ROWTYPE;
--
    SP_VALUES         CUR_GET_SEC_INFO%ROWTYPE;
    ER_VALUES         CUR_GET_EXCH_INFO%ROWTYPE;
--
-- Ajout - JSL - 14 juillet 2015
CURSOR GET_DRP_Info
     ( I_Data_Supplier   Data_Suppliers.Code%TYPE
     ) IS
SELECT Unchain_cfw_on_holiday_Flag
  FROM Data_Suppliers
 WHERE Code = I_Data_Supplier;

Unchain_Flag           Yes_No.Code%TYPE;
-- Fin d'ajout - JSL - 14 juillet 2015
--
------------------------------------------------------------------------
PROCEDURE CFW_PRICE_OR_EXCH_APPLY;
PROCEDURE CFW_REV_PRICE_OR_EXCH_APPLY;
--
-- Ajout - JSL - 10 septembre 2014
------------------------------------------------------------------------
-- Set the tms queue in out to unscheduled if not already completed.
------------------------------------------------------------------------
PROCEDURE Set_Tms_to_Unscheduled
        ( Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        ) IS --{

V_Success Yes_No.Code%TYPE;
V_Message VARCHAR2(256);

CURSOR GET_TMS_QUEUE_INOUT
     ( I_Program_Id Ops_Scripts.Program_Id%TYPE
     , I_Site_Date  DATE
     ) IS
SELECT TQIO.*
  FROM TMS_Queue_InOut TQIO
     , TMS_Running_Parms TRP
 WHERE TRP.Program_Id = I_Program_Id
   AND TQIO.Aft_Tag_Code = TRP.Aft_Tag_Code
   AND TQIO.Site_Date  = I_Site_Date
       ;
TQIO_Row Get_Tms_Queue_InOut%ROWTYPE;

BEGIN --{
   
   Pgmloc := 9010;
   OPEN Get_Tms_Queue_InOut( Pricing_Msgs_Rec.Program_Id, Pricing_Msgs_Rec.Date_Of_Prices );

   Pgmloc := 9020;
   FETCH Get_Tms_Queue_InOut INTO TQIO_Row;
   IF Get_Tms_Queue_InOut%FOUND
   THEN
      NULL;
   ELSE
      Pgmloc := 9030;
      CLOSE Get_Tms_Queue_InOut;
      GOTO Get_Out;
   END IF;

   Pgmloc := 9040;
   CLOSE Get_Tms_Queue_InOut;

   Pgmloc := 9050;
   IF TQIO_Row.TMS_Status_Code IN (' CANCELLED', 'SUCCESS', 'UNSCHEDULED')
   THEN
      GOTO Get_Out;
   END IF;

   -- Set the status to unscheduled
   Manage_Tms_Info.Update_tms_by_api( TQIO_Row.Aft_Tag_Code
                                    , Pricing_Msgs_Rec.Date_Of_Prices
                                    , 'TMS_STATUS_CODE'
                                    , 'UNSCHEDULED'
                                    , NULL
                                    , V_Success
                                    , V_Message
                                    );
   IF V_Success = 'Y'
   THEN
      NULL;
   ELSE
      Raise Program_Error;
   END IF;

   -- Set the status to unscheduled
   Manage_Tms_Info.Update_tms_by_api( TQIO_Row.Aft_Tag_Code
                                    , Pricing_Msgs_Rec.Date_Of_Prices
                                    , 'REMARK'
                                    , 'Program executed starting at '
                                    || TO_CHAR( Pricing_Msgs_Rec.Date_of_Msg, 'dd-mon-yyyy hh24:mi:ss' )
                                    || ' and completed at '
                                    || TO_CHAR( Sysdate, 'dd-mon-yyyy hh24:mi:ss' )
                                    , NULL
                                    , V_Success
                                    , V_Message
                                    );
   IF V_Success = 'Y'
   THEN
      NULL;
   ELSE
      Raise Program_Error;
   END IF;

   COMMIT;

<< Get_Out >>
   NULL;

EXCEPTION
WHEN OTHERS THEN
   -- This procedure should not fail.  Even if we cannot do anything, do not fail!
   IF Get_Tms_Queue_InOut%ISOPEN
   THEN
      CLOSE Get_Tms_Queue_InOut;
   END IF;
   ROLLBACK;

END Set_Tms_To_Unscheduled; --}}
--
--
--
------------------------------------------------------------------------
-- Send email
------------------------------------------------------------------------
PROCEDURE Send_Email
        ( Pricing_Msgs_Rec IN OUT NOCOPY Pricing_Msgs%ROWTYPE
        ) IS --{

EMail_Program_Id   Ops_Scripts.Program_Id%TYPE;
EMail_File         utl_File.File_type;
EMail_File_Name    varchar2(60);
Dir_Name           varchar2(60);

V_Success BOOLEAN;
V_Message VARCHAR2(1000);

BEGIN --{

   Pgmloc := 9510;
   EMail_Program_Id := TO_NUMBER( Scripts.get_arg_value( Pricing_Msgs_Rec.Program_Id
                                                       , 'PMGID_EMAIL') );

   IF Email_Program_Id IS NULL
   THEN
      -- No Email to send
      GOTO Get_Out;
   END IF;
   
   -- Create log file as none exists
   Pgmloc := 9520;
   EMail_File_Name := TRIM( TO_CHAR( Pricing_Msgs_Rec.Program_Id ) )
                   || '_'
                   || TO_CHAR( Pricing_Msgs_Rec.Date_Of_Msg, 'YYYYMMDDHH24MISS' )
                   || '.log'
                    ;
   Pgmloc := 9530;
   Email_File := utl_file.fopen( Constants.Get_Dir_Name()
                               , Email_File_Name
                               , 'W'
                               );
                               
   Pgmloc := 9520;
   -- Put in the log file the carry forward supplier code and delivery time
   Utl_file.put_line( Email_File
                    , 'Carry forward failure'
                    , TRUE
                    );
   Utl_file.put_line( Email_File
                    , 'Program id is :' || Pricing_msgs_Rec.Program_Id
                    , TRUE
                    );
   Utl_file.put_line( Email_File
                    , 'Program started at :' 
                   || TO_CHAR( Pricing_msgs_Rec.Date_Of_Msg, 'dd-mon-yyyy hh24:mi:ss' )
                    , TRUE
                    );
   -- Add the pricing messages text 1, 2, error and remark
   Utl_file.put_line( Email_File
                    , Pricing_msgs_Rec.Msg_Text
                    , TRUE
                    );
   Utl_file.put_line( Email_File
                    , Pricing_msgs_Rec.Msg_Text_2
                    , TRUE
                    );
   Utl_file.put_line( Email_File
                    , Pricing_msgs_Rec.Err_text
                    , TRUE
                    );
   Utl_file.put_line( Email_File
                    , Pricing_msgs_Rec.Remarks
                    , TRUE
                    );
   -- close the file
   Pgmloc := 9540;
   utl_file.fClose( Email_File );

   Pgmloc := 9530;
   -- Call TMS utilities send tms email
   -- A commit is done in package
   Tms_Utilities.Send_Tms_Email( Email_Program_Id
                               , Email_File_Name
                               , NULL     -- Oracle directory reference
                               , V_Success
                               , V_Message
                               );

<< Get_Out >>
   NULL;

EXCEPTION
WHEN OTHERS THEN
   -- This procedure should not fail.  Even if we cannot do anything, do not fail!
   IF utl_file.is_open( EMail_File )
   THEN
      utl_file.Fclose( Email_File );
   END IF;
   ROLLBACK;

END Send_Email; --}}
-- Fin d'ajout - JSL - 10 septembre 2014
--
-- Definition des procedures et fonctions
--
------------------------------------------------------------------------
PROCEDURE On_Exch_Rates
    (p_program_id     IN   ops_scripts.program_id%TYPE,
     p_delivery_date  IN   DATE,
     p_supplier_code  IN   DATA_SUPPLIERS.CODE%TYPE,
     p_delivery_time  IN   VARCHAR2,
     p_commit_count   IN   NUMBER   := 64,
     p_restart_flag   IN   VARCHAR2 := 'N',
     p_restart_date   IN   DATE     := NULL,
     P_FROM_SCREEN    IN   VARCHAR2 := NULL)
IS
--{
  bad_previous_cf        EXCEPTION;
  cannot_restart         EXCEPTION;
  inv_prc_date           EXCEPTION;
  p_rows_ins             NUMBER :=0;
  rows_inserted          NUMBER :=0;
  rows_read              NUMBER :=0;
  rows_rejected          NUMBER :=0;
  rows_deleted           NUMBER :=0;
  temp_pgm_id            OPS_Scripts.Program_Id%TYPE;
  temp_first_run_date    DATE;
  temp_buffer            CHAR(255);
  restart_date           DATE;
  cfw_date               DATE := NULL;
  temp_succ_flag         yes_no.code%TYPE;
  user_id                user_list.user_id%TYPE :=Constants.Get_User_Id;
--
  v_delivery_date        DATE;
  previous_delivery_date DATE;
  v_cf_delivery_time     data_supply_schedule.cf_delivery_time%TYPE;
  Handled_Raise          BOOLEAN := FALSE;
  Price_Origin_Code      Price_Origins.Code%TYPE;
  Intra_Day              BOOLEAN;
--
CURSOR Cur_Get_CFW_Date
IS
   SELECT MAX(Delivery_Date)
     FROM Exchange_Rates
    WHERE Data_Supplier_Code = P_Supplier_Code
      AND Delivery_Date      < P_Restart_Date;
--
-- Not Run Yet Flag cannot be looked at
--
-- The Successfull_Flag = Y must be the one to be fetched
-- This is the only known success in Oasis
--
CURSOR Cur_Previous_CFW_Done
IS
   SELECT PM.Successfull_Flag
     FROM Pricing_Msgs      PM
    WHERE PM.Program_Id       = Temp_Pgm_Id
      AND PM.Date_of_Prices   = TRUNC(Previous_Delivery_Date)
      AND PM.Date_Of_Msg      =
          (SELECT MAX(PM2.Date_of_Msg)
           FROM  Pricing_Msgs      PM2,
                 Successfull_Flags SF2
           WHERE PM2.Program_id       = PM.Program_id
            AND  PM2.Date_of_Prices   = PM.Date_of_Prices
            AND  PM2.Successfull_Flag = SF2.Code
            AND  SF2.Not_Run_Yet_Flag = 'N' );
--
-- Ce curseur verifie si il existe des rangees dans Exchange Rates
-- avec un delivery date plus recent que le Restart Date
--
--
-- Modifie pour permettre des Restart meme s'il existe des rangees
-- pour le meme fournisseur, tant que le CF_Delivery_Time de ces
-- rangees ne pointe pas sur les rangees sur lesquelles le Restart se
-- fera
--
CURSOR Cur_Rows_Before_Restart (
   P_Restart_Date       DATE,
   P_Data_Supplier_Code Data_Suppliers.Code%TYPE,
   P_Del_Time           Data_Supply_Schedule.Delivery_Time%TYPE)
IS
  SELECT DISTINCT 'X'
    FROM Exchange_Rates ER,
         Data_Supply_Schedule DSS
   WHERE ER.Delivery_Date        >  P_Restart_Date
     AND ER.Data_Supplier_Code   =  P_Data_Supplier_Code
     AND DSS.Data_Supplier_Code  =  ER.Data_Supplier_Code
     AND DSS.Delivery_Time       =  to_char(ER.delivery_date,'hh24mi')
     AND DSS.Delivery_Time       <> P_Del_Time
     AND DSS.CF_Delivery_Time    =  P_Del_Time
  UNION
  SELECT DISTINCT 'X'
    FROM Exchange_Rates ER,
         Data_Supply_Schedule DSS
   WHERE ER.Delivery_Date       >  (P_Restart_Date+1)
     AND ER.Data_Supplier_Code  =  P_Data_Supplier_Code
     AND DSS.Data_Supplier_Code =  ER.Data_Supplier_Code
     AND DSS.Delivery_Time      =  to_char(ER.delivery_date,'hh24mi')
     AND DSS.CF_Delivery_Time   =  P_Del_Time;
--
--
-- Ce curseur va chercher les enr de EXCHANGE_RATES sur lesquels on
-- veut appliquer le CARRY FORWARD
--
-- Modifie pour que le statut du titre soit verifie avec
-- Date_of_Status <= Last_Price_Date
--
Last_Price_Date DATE := Constants.Get_Site_Date;
--
CURSOR Exchange_Details
IS
  SELECT A.fri_ticker,
         A.source_code,
         A.price_currency,
         A.delivery_date,
         A.data_supplier_code,
         A.price_date,
         A.price,
         A.Ask,
         A.last_user,
         A.last_stamp
    FROM trading_status C,
         security_trading_status B,
         exchange_rates A
   WHERE A.data_supplier_code  = p_supplier_code
     AND A.delivery_date       = previous_delivery_date
     AND A.fri_ticker          = B.fri_ticker
     AND A.source_code         = B.source_code
     AND A.price_currency      = B.price_currency
     AND B.date_of_status      =
         (SELECT MAX(Y.date_of_status)
            FROM security_trading_status Y
           WHERE Y.fri_ticker     = A.fri_ticker
             AND Y.source_code    = A.source_code
             AND Y.price_currency = A.price_currency
             AND Y.Date_of_Status <= Last_Price_Date)
     AND B.trading_status_code = C.code
     AND C.active_flag         = 'Y'
  Order by A.fri_ticker,
           A.Delivery_Date,
           A.Source_Code;
--
  end_of_loop BOOLEAN := FALSE;
  tmp_status  break_points.status%TYPE;
--
  no_data     EXCEPTION;
--
BEGIN
--{
-- Start of Job
--
   pgmloc        := 10;
   User_Id       := Constants.Get_User_Id;
   G_Program_Id  := P_Program_Id;
   G_System_Date := SYSDATE;
   --
   Global_File_Updates.Initialize_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   Pricing_Msg_Rec.Msg_Text :=
     'CFW on Exchange Rates for:'|| p_supplier_code || ', ' ||
      to_char(p_delivery_date,'dd-mon-yyyy') || ' ' || p_delivery_time;
   --
   Pricing_Msg_Rec.Msg_Text_2       := 'Start of Program';
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   --
   IF P_Restart_Flag = 'Y'
   THEN
     Pricing_Msg_Rec.Date_of_Prices := P_Restart_Date;
   END IF;
   --
   Global_File_Updates.Update_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
--
-- Aller chercher le dernier status
--
   pgmloc := 20;
   SELECT status
   INTO   tmp_status
   FROM   break_points
   WHERE  program_id = p_program_id;
--
-- Validation des parametres Data_supplier et Delivery time
-- Si ce n'est pas valide, l'exception NO_DATA_FOUND est levee
-- automatiquement
--
   pgmloc := 30;
--
   SELECT  cf_delivery_time
     INTO  v_cf_delivery_time
     FROM  data_supply_schedule
    WHERE  data_supplier_code = p_supplier_code
      AND  delivery_time      = p_delivery_time;
--
   pgmloc := 40;
   IF Constants.G_Site_Date <> TRUNC(p_delivery_date) THEN
      Pricing_Msg_Rec.Msg_Text_2 := 'Error while validating parameters';
      Pricing_Msg_Rec.Err_Text   :=
         'Invalid price date parameter. Must be equal to New Day.';
      RAISE inv_prc_date;
   END IF;
--
-- On set la delivery_date
--
   v_delivery_date :=
           to_date(to_char(p_delivery_date,'dd-mon-yyyy') || ' ' ||
           p_delivery_time,'DD-MON-YYYY HH24MI');
--
   --
   -- Nous devons verifier si les prix sont des Intra-Day
   -- pour assigner le Price_Origin_Code
   Intra_Day := Supplier.Is_It_Intra_Day(P_Supplier_Code,
                                         P_Delivery_Time);
   --
   IF Intra_Day
   THEN
      Price_Origin_Code := 'I';
   ELSE
      Price_Origin_Code := 'C';
   END IF;
   --
   pgmloc := 50;
   --
   -- modifie pour utiliser le previous_price_date de Site_Controls
   -- au lieu de faire un max
   --
   IF P_Restart_Flag = 'N'
   THEN
      Restart_Date:= V_Delivery_Date;
      --
      IF V_CF_Delivery_Time < P_Delivery_Time
      THEN
         Previous_Delivery_Date :=
          to_date(to_char(Constants.G_Site_Date,'DD-MON-YYYY') ||
          ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      ELSE
         Previous_Delivery_Date :=
          to_date(to_char(Constants.G_Previous_Site_Date,'DD-MON-YYYY') ||
          ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      END IF;
      --
   ELSE
      --
      IF P_Restart_Date = Constants.G_Site_Date
      THEN
         CFW_Date := Constants.G_Previous_Site_Date;
      ELSE
         OPEN Cur_Get_Cfw_Date;
         FETCH Cur_Get_Cfw_Date INTO CFW_Date;
         CLOSE Cur_Get_Cfw_Date;
      END IF;
      --
      Restart_Date :=
        TO_DATE(TO_CHAR(P_Restart_Date,'DD-MON-YYYY')  || ' ' ||
        P_Delivery_Time,'DD-MON-YYYY HH24MI');
      --
      IF V_CF_Delivery_Time < P_Delivery_Time
      THEN
         Previous_Delivery_Date :=
          to_date(to_char(P_Restart_Date,'DD-MON-YYYY') ||
          ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      ELSE
         Previous_Delivery_Date :=
          to_date(to_char(CFW_Date,'DD-MON-YYYY') ||
          ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      END IF;
   END IF;
   --
   -- Nous devons maintenant s'assurer que le CFWD de la date
   -- Previous_Delivery_Date a ete execute
   -- Marie (12nov1996)
   --
   pgmloc := 60;
   --
   SELECT SRP.Program_Id , SRP.First_Run_Date
     INTO Temp_Pgm_Id, Temp_First_Run_date
     FROM Supplier_Running_Parms SRP
    WHERE SRP.Data_Supplier_Code = P_Supplier_Code
      AND SRP.Delivery_Time      = V_CF_Delivery_Time
      AND SRP.Product_Code       = 'CFW';
   --
   IF Temp_First_Run_Date != P_Delivery_Date THEN
      OPEN Cur_Previous_CFW_Done;
      FETCH Cur_Previous_CFW_Done INTO Temp_Succ_Flag;
      IF Cur_Previous_CFW_Done%FOUND
      THEN
         CLOSE Cur_Previous_CFW_Done;
         IF Temp_Succ_Flag != 'Y'
         THEN
            Raise Bad_Previous_cf;
         END IF;
      ELSE
         CLOSE Cur_Previous_CFW_Done;
         Raise Bad_Previous_cf;
      END IF;
   END IF;
--
-- Si c'est un restart, on enleve tous les enr. pour le supplier et
-- delivery time
--
   pgmloc := 70;
   IF p_restart_flag = 'Y' OR tmp_status = 'FRESTART'
   THEN
      IF p_restart_flag = 'Y'
      THEN
         OPEN Cur_Rows_Before_Restart(Restart_Date,
                                      P_Supplier_Code,
                                      P_Delivery_Time);
         FETCH Cur_Rows_Before_Restart INTO Temp_Buffer;
         --
         IF Cur_Rows_Before_Restart%FOUND
         THEN
            CLOSE Cur_Rows_Before_Restart;
            Pricing_Msg_Rec.Err_Text:=
              'Cannot Restart Carry Fwd, Rates exist past restart date';
            RAISE Cannot_Restart;
         END IF;
         CLOSE Cur_Rows_Before_Restart;
      END IF;
      --
      WHILE NOT (end_of_loop) LOOP
         DELETE FROM exchange_rates
          WHERE delivery_date = restart_date
           and data_supplier_code = p_supplier_code
           AND rownum <= p_commit_count;
--
         IF SQL%ROWCOUNT < p_commit_count THEN
            end_of_loop := TRUE;
         END IF;
         rows_deleted := rows_deleted + SQL%ROWCOUNT;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Global_File_Updates.Update_Pricing_Msgs( Pricing_Msg_Rec.Program_Id
                                                , Pricing_Msg_Rec.Date_of_Msg
                                                , FALSE
                                                , Pricing_Msg_Rec
                                                );
         COMMIT;
      END LOOP;
   END IF;
--
-- Fetch Options and apply Carry Forward - Using new Price Date
--
   pgmloc := 80;
   FOR exchange_data IN exchange_details LOOP
      rows_read := rows_read + 1;
--
      exchange_data.delivery_date := restart_date;
--
      --
      -- Ajoute nouveau champs Price_Origin_Code qui sera mis
      -- a 'C' ou 'I' pour indiquer que cette rangee de prix a ete
      -- creee par le program de Carry Forward
      --
      pgmloc := 90;
      INSERT INTO exchange_rates (fri_ticker, source_code,
             price_currency, delivery_date, data_supplier_code,
             price_date, price, Ask, updated_flag, price_origin_code,
             last_user, last_stamp)
      SELECT exchange_data.fri_ticker,
             exchange_data.source_code,
             exchange_data.price_currency,
             exchange_data.delivery_date,
             exchange_data.data_supplier_code,
             exchange_data.price_date,
             exchange_data.price,
             exchange_data.ask,
             'N',
             Price_Origin_Code,
             user_id,
             SYSDATE
        FROM dual
       WHERE NOT EXISTS
              (SELECT 'X'
               FROM exchange_rates SS
               WHERE SS.fri_ticker      = exchange_data.fri_ticker
               AND SS.source_code       = exchange_data.source_code
               AND SS.price_currency    = exchange_data.price_currency
               AND SS.delivery_date     = exchange_data.delivery_date
               AND SS.data_supplier_code=
                   exchange_data.data_supplier_code);
--
-- SQLROWCOUNT will be equal to 1 - If no bugs.
-- Add 1 to rejected rows if Insert failed.
--
      pgmloc := 100;
      IF SQL%ROWCOUNT > 0 THEN
         rows_inserted := rows_inserted + SQL%ROWCOUNT;
         p_rows_ins    := p_rows_ins    + SQL%ROWCOUNT;
         IF p_rows_ins >= p_commit_count THEN
            pgmloc := 110;
            --
            Pricing_Msg_Rec.Value_01 :=  Rows_Read;
            Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
            Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
            Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
            Pricing_Msg_Rec.Msg_Text_2 :=
              'Processing exchange rates records';
            --
            Global_File_Updates.Update_Pricing_Msgs
              (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
            COMMIT;
            p_rows_ins := 0;
         END IF;
      ELSE
         rows_rejected := rows_rejected + 1;
      END IF;
--
   END LOOP;
--
-- nous mettons a jour le Last_Run_Date de supplier_running_parms
--
   UPDATE Supplier_Running_Parms SRP
      SET Last_Run_Date  = sysdate
    WHERE SRP.Program_Id = P_Program_Id;
--
-- If #read = #reject ---> Carry Forward is run for a 2nd time
--
   pgmloc := 120;
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
   --
   -- Si aucune rangee a ete lu, on veut faire un raise pour avoir
   -- le message approprie
   IF rows_read = 0
   THEN
      RAISE No_Data;
   END IF;
   --
   IF rows_read = rows_rejected
   THEN
      Pricing_Msg_Rec.Msg_Text_2  :=
        'WARNING - Carry Forward was already DONE';
      --
   ELSE
      Pricing_Msg_Rec.Msg_Text_2  := 'Carry Forward DONE ';
      IF P_Restart_Flag = 'Y'
      THEN
         Pricing_Msg_Rec.Msg_Text_2 := Pricing_Msg_Rec.Msg_Text_2 ||
           'Restarted for ' || to_char(P_Restart_Date,'DD-MON-YYYY');
      END IF;
   END IF;
   --
   pgmloc := 130;
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
    (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
    --
   COMMIT;
   --
-- Ajout - JSL - 10 septembre 2014
   Set_Tms_to_Unscheduled( Pricing_Msg_Rec );
-- Fin d'ajout- JSL - 10 septembre 2014
--
EXCEPTION
--
-- Le dernier carry forward n'a pas fonctionne. On arrete pour permettre
-- une verification.
--
WHEN Bad_Previous_Cf
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Msg_Text    :=
     'Previous CF '                             ||
      to_char(Previous_Delivery_Date,'ddmonyyyy') ||
     ' didn''t work.Create a rec succ Y in '    ||
     'Prcg Msg';
   --
   Pricing_Msg_Rec.Msg_Text_2  :=
     'OR Change the Dates in Site_Controls (Take a copy of the table)';
   --
   Pricing_Msg_Rec.Err_Text    :=
     'Recompile the proc and run the Carry Fwd of '     ||
     to_char(Previous_Delivery_Date,'ddmonyyyy') ||  ' '  ||
     V_CF_Delivery_Time;
   --
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN Cannot_Restart
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN inv_prc_date
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
---
WHEN no_data_found
THEN
--
   Pricing_Msg_Rec.Err_Text := 'No Data Found.  Probable Cause ' ||
                                'Pgm Id ' || P_Program_ID ||
                               ' missing from Break_Points';
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   --
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN no_data
THEN
--
   -- Pricing_Msg_Rec.Err_Text := 'No data to apply carry forward on';
   Pricing_Msg_Rec.Remarks     := 'No data to apply carry forward on';
   IF Temp_First_Run_Date = P_Delivery_Date
   THEN
      -- Pricing_Msg_Rec.Err_Text := Pricing_Msg_Rec.Err_Text ||
      Pricing_Msg_Rec.Remarks     := Pricing_Msg_Rec.Remarks  ||
                                     ' - First Carry Fwd';
   END IF;
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
--
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
-- Pour toutes les autres exceptions, on met le message d'oracle
--
WHEN others
THEN
   IF NOT(Handled_Raise)
   THEN
      -- Err_Text is 255 characters long (not 68)
      -- Pricing_Msg_Rec.Err_Text := substr(SQLERRM(SQLCODE),1,68);
      Pricing_Msg_Rec.Err_Text    := substr(SQLERRM(SQLCODE),1,255);
      Pricing_Msg_Rec.Msg_Text_2  :=
        'Unhandled exception occurred at pgmloc:'||to_char(pgmloc);
      Pricing_Msg_Rec.Successfull_Flag := 'N';
      Pricing_Msg_Rec.Value_01 :=  Rows_Read;
      Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
      Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
      Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
      --
      Global_File_Updates.Update_Pricing_Msgs
        (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   END IF;
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   IF NOT(Handled_Raise)
   THEN
      -- We may be doing a rollback in send email...
      Send_Email( Pricing_Msg_Rec );
   END IF;
-- Fin d'ajout - JSL - 10 septembre 2014
   RAISE;
--}}
END on_exch_rates;
--
------------------------------------------------------------------------
PROCEDURE On_Gics
    (p_program_id     IN   ops_scripts.program_id%TYPE,
     p_delivery_date  IN   DATE,
     p_supplier_code  IN   DATA_SUPPLIERS.CODE%TYPE,
     p_delivery_time  IN   VARCHAR2,
     p_commit_count   IN   NUMBER   := 64,
     p_restart_flag   IN   VARCHAR2 := 'N',
     p_restart_date   IN   DATE     := NULL,
     P_FROM_SCREEN    IN   VARCHAR2 := NULL)
IS
--{
  --
  C_Gic_Comp          CONSTANT VARCHAR2(30) := 'GIC_COMP';
  C_Gic_Secur         CONSTANT VARCHAR2(30) := 'GIC_SECUR';
  C_Gic_Xref          CONSTANT VARCHAR2(30) := 'GIC_XREF';
  --
  bad_previous_cf        EXCEPTION;
  cannot_restart         EXCEPTION;
  inv_prc_date           EXCEPTION;
  p_rows_ins             NUMBER :=0;
  rows_inserted          NUMBER :=0;
  rows_read              NUMBER :=0;
  rows_rejected          NUMBER :=0;
  rows_deleted           NUMBER :=0;
  temp_pgm_id            OPS_Scripts.Program_Id%TYPE;
  temp_first_run_date    DATE;
  temp_buffer            CHAR(255);
  restart_date           DATE;
  cfw_date               DATE := NULL;
  temp_succ_flag         yes_no.code%TYPE;
  user_id                user_list.user_id%TYPE :=Constants.Get_User_Id;
--
  v_delivery_date        DATE;
  previous_delivery_date DATE;
  v_cf_delivery_time     data_supply_schedule.cf_delivery_time%TYPE;
  Handled_Raise          BOOLEAN := FALSE;
  Price_Origin_Code      Price_Origins.Code%TYPE;
  Intra_Day              BOOLEAN;
--
CURSOR Cur_Get_CFW_Date_Comp
     ( P_Restart_Date    DATE
     ) IS
   SELECT MAX(Delivery_Date)
     FROM Gic_Comp
    WHERE Delivery_Date      < P_Restart_Date;
--
CURSOR Cur_Get_CFW_Date_Secur
     ( P_Restart_Date  DATE
     ) IS
   SELECT MAX(Delivery_Date)
     FROM Gic_Secur
    WHERE Delivery_Date      < P_Restart_Date;
--
CURSOR Cur_Previous_CFW_Done
     ( P_Program_Id           Ops_Scripts.Program_Id%TYPE
     , Previous_Delivery_Date DATE
     ) IS
   SELECT PM.Successfull_Flag
     FROM Pricing_Msgs      PM
    WHERE PM.Program_Id       = P_Program_Id
      AND PM.Date_of_Prices   = TRUNC(Previous_Delivery_Date)
      AND PM.Date_Of_Msg      =
          (SELECT MAX(PM2.Date_of_Msg)
           FROM  Pricing_Msgs      PM2,
                 Successfull_Flags SF2
           WHERE PM2.Program_id       = PM.Program_id
            AND  PM2.Date_of_Prices   = PM.Date_of_Prices
            AND  PM2.Successfull_Flag = SF2.Code
            AND  SF2.Not_Run_Yet_Flag = 'N' );
--
-- Ce curseur verifie s'il existe des rangees dans Gic Comp
-- avec un delivery date plus recent que le Restart Date
--
CURSOR Cur_Comp_Before_Restart
     ( P_Restart_Date       DATE
     ) IS
  SELECT DISTINCT 'X'
    FROM Gic_Comp
   WHERE Delivery_Date       >  P_Restart_Date;
--
-- Ce curseur verifie s'il existe des rangees dans Gic Secur
-- avec un delivery date plus recent que le Restart Date
--
CURSOR Cur_Secur_Before_Restart
     ( P_Restart_Date       DATE
     ) IS
  SELECT DISTINCT 'X'
    FROM Gic_Secur
   WHERE Delivery_Date       >  P_Restart_Date;
--
--
-- Ce curseur va chercher les enr de Gic Comp sur lesquels on
-- veut appliquer le CARRY FORWARD
--
Last_Price_Date DATE := Constants.Get_Site_Date;
--
CURSOR Gic_Comp_Details
     ( Previous_Delivery_Date DATE
     ) IS
  SELECT A.*
    FROM Gic_Comp A
   WHERE A.delivery_date       = previous_delivery_date
  Order by A.GVKey,
           A.Delivery_Date;
--
CURSOR Gic_Secur_Details
     ( Previous_Delivery_Date DATE
     ) IS
  SELECT A.*
    FROM Gic_Secur A
   WHERE A.delivery_date       = previous_delivery_date
  Order by A.GVKey,
           A.iid,
           A.Delivery_Date;
--
  end_of_loop BOOLEAN := FALSE;
  tmp_status  break_points.status%TYPE;
--
  no_data     EXCEPTION;
  Gic_Table   VARCHAR2(60);
--
BEGIN
--{
-- Start of Job
--
   pgmloc        := 140;
   User_Id       := Constants.Get_User_Id;
   G_Program_Id  := P_Program_Id;
   G_System_Date := SYSDATE;
   --
   pgmloc        := 150;
   Global_File_Updates.Initialize_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   pgmloc := 160;
   Gic_Table := UPPER( NVL( Scripts.Get_Arg_Value( P_Program_id, 'CFW' )
                          , 'NOT SUPPORTED' ) );
   IF Gic_Table IN ( C_GIC_COMP, C_GIC_SECUR, C_GIC_XREF )
   THEN
      Pricing_Msg_Rec.Msg_Text :=
        'CFW on '||Gic_Table||' for:'|| p_supplier_code || ', ' ||
         to_char(p_delivery_date,'dd-mon-yyyy') || ' ' || p_delivery_time;
   ELSE
      Pricing_Msg_Rec.Msg_Text := 'CFW NOT SUPPORTED : '''||Gic_Table||'''';
   END IF;
   --
   Pricing_Msg_Rec.Msg_Text_2       := 'Start of Program';
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   --
   IF P_Restart_Flag = 'Y'
   THEN
      Pricing_Msg_Rec.Date_of_Prices := P_Restart_Date;
   END IF;
   --
   pgmloc := 170;
   Global_File_Updates.Update_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   pgmloc := 180;
   COMMIT;
   --
   IF Gic_Table NOT IN ( C_Gic_Comp, C_Gic_Secur )
   THEN
      IF Gic_Table IN ( C_Gic_XREF )
      THEN
        Pricing_Msg_Rec.Successfull_Flag := 'Y';
      END IF;
      GOTO Get_Out;
   END IF;
--
-- Aller chercher le dernier status
--
   pgmloc := 190;
   SELECT status
   INTO   tmp_status
   FROM   break_points
   WHERE  program_id = p_program_id;
--
-- Validation des parametres Data_supplier et Delivery time
-- Si ce n'est pas valide, l'exception NO_DATA_FOUND est levee
-- automatiquement
--
   pgmloc := 200;
   SELECT  cf_delivery_time
     INTO  v_cf_delivery_time
     FROM  data_supply_schedule
    WHERE  data_supplier_code = p_supplier_code
      AND  delivery_time      = p_delivery_time;
--
   pgmloc := 210;
   IF Constants.G_Site_Date <> TRUNC(p_delivery_date) THEN
      Pricing_Msg_Rec.Msg_Text_2 := 'Error while validating parameters';
      Pricing_Msg_Rec.Err_Text   :=
         'Invalid price date parameter. Must be equal to New Day.';
      RAISE inv_prc_date;
   END IF;
--
-- On set la delivery_date
--
   v_delivery_date := p_delivery_date;
--
   pgmloc := 220;
   IF P_Restart_Flag = 'N'
   THEN
      Restart_Date:= V_Delivery_Date;
      Previous_Delivery_Date := Constants.G_Previous_Site_Date;
   ELSE
      IF P_Restart_Date = Constants.G_Site_Date
      THEN
         CFW_Date := Constants.G_Previous_Site_Date;
      ELSE
         IF Gic_Table = C_Gic_Comp
         THEN
            Pgmloc := 230;
            OPEN  Cur_Get_Cfw_Date_Comp( P_Restart_Date );
            Pgmloc := 240;
            FETCH Cur_Get_Cfw_Date_Comp INTO CFW_Date;
            Pgmloc := 250;
            CLOSE Cur_Get_Cfw_Date_Comp;
         ELSE
            Pgmloc := 260;
            OPEN  Cur_Get_Cfw_Date_Secur( P_Restart_Date );
            Pgmloc := 270;
            FETCH Cur_Get_Cfw_Date_Secur INTO CFW_Date;
            Pgmloc := 280;
            CLOSE Cur_Get_Cfw_Date_Secur;
         END IF;
      END IF;
      --
      Restart_Date := P_Restart_Date;
      Previous_Delivery_Date := CFW_Date;
   END IF;
   --
   -- Nous devons maintenant s'assurer que le CFWD de la date
   -- Previous_Delivery_Date ait ete execute
   --
   pgmloc := 290;
   SELECT SRP.First_Run_Date
     INTO Temp_First_Run_date
     FROM Supplier_Running_Parms SRP
    WHERE SRP.Program_Id = P_Program_Id;
   --
   IF Temp_First_Run_Date != P_Delivery_Date
   THEN
      Pgmloc := 300;
      OPEN Cur_Previous_CFW_Done( G_Program_Id, Previous_Delivery_Date );
      Pgmloc := 310;
      FETCH Cur_Previous_CFW_Done INTO Temp_Succ_Flag;
      IF Cur_Previous_CFW_Done%FOUND
      THEN
         Pgmloc := 320;
         CLOSE Cur_Previous_CFW_Done;
         IF Temp_Succ_Flag != 'Y'
         THEN
            Raise Bad_Previous_cf;
         END IF;
      ELSE
         Pgmloc := 330;
         CLOSE Cur_Previous_CFW_Done;
         Raise Bad_Previous_cf;
      END IF;
   END IF;
--
-- Si c'est un restart, on enleve tous les enr. pour le supplier et
-- delivery time
--
   pgmloc := 340;
   IF p_restart_flag = 'Y'
   OR tmp_status = 'FRESTART'
   THEN --{
      IF p_restart_flag = 'Y'
      THEN --{
         IF Gic_Table = C_Gic_Comp
         THEN --{
            pgmloc := 350;
            OPEN Cur_Comp_Before_Restart( Restart_Date );
            pgmloc := 360;
            FETCH Cur_Comp_Before_Restart INTO Temp_Buffer;
            IF Cur_Comp_Before_Restart%FOUND
            THEN
               pgmloc := 370;
               CLOSE Cur_Comp_Before_Restart;
               Pricing_Msg_Rec.Err_Text := 'Cannot Restart Carry Fwd, '
                                        || C_Gic_Comp
                                        || ' exist past restart date';
               RAISE Cannot_Restart;
            END IF;
            pgmloc := 380;
            CLOSE Cur_Comp_Before_Restart;
         ELSE --}{ Gic_Table = C_Gic_Secur
            pgmloc := 390;
            OPEN Cur_Secur_Before_Restart( Restart_Date );
            pgmloc := 400;
            FETCH Cur_Secur_Before_Restart INTO Temp_Buffer;
            IF Cur_Secur_Before_Restart%FOUND
            THEN
               pgmloc := 410;
               CLOSE Cur_Secur_Before_Restart;
               Pricing_Msg_Rec.Err_Text := 'Cannot Restart Carry Fwd, '
                                        || C_Gic_Comp
                                        || ' exist past restart date';
               RAISE Cannot_Restart;
            END IF;
            pgmloc := 420;
            CLOSE Cur_Secur_Before_Restart;
         END IF; --}  C_Gic_Secur
      END IF; --} P_restart_Flag
      --
      end_of_loop := FALSE;
      WHILE NOT (end_of_loop)
      LOOP --{
         IF Gic_Table = C_GIC_COMP
         THEN
            pgmloc := 430;
            DELETE FROM Gic_Comp
             WHERE delivery_date = restart_date
               AND rownum       <= p_commit_count;
         ELSE
            pgmloc := 440;
            DELETE FROM Gic_Secur
             WHERE delivery_date = restart_date
               AND rownum       <= p_commit_count;
         END IF;
--
         IF SQL%ROWCOUNT < p_commit_count THEN
            end_of_loop := TRUE;
         END IF;
         rows_deleted := rows_deleted + SQL%ROWCOUNT;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         pgmloc := 450;
         Global_File_Updates.Update_Pricing_Msgs
          (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
         pgmloc := 460;
         COMMIT;
      END LOOP; --}
   END IF; --} FRESTART
--
-- Fetch rows and apply Carry Forward - Using new Price Date
--
   IF Gic_Table = C_Gic_Comp
   THEN --{
      pgmloc := 470;
      FOR gic_comp_data IN gic_comp_details( Previous_Delivery_Date )
      LOOP --{
         rows_read := rows_read + 1;
         Gic_Comp_Data.Delivery_Date := Restart_Date;
         pgmloc := 480;
         INSERT
           INTO Gic_Comp
              ( GVKey,                       Conm
              , Constat,                     Fic
              , GGroup,                      GInd
              , GSector,                     GSubind
              , Gvkey_T,                     Loc
              , Fri_Issuer
              , Incorporation_Nation_Code
              , Residence_Nation_Code
              , Creation_Date,               Last_Available_Date
              , Delivery_Date,               Delivery_Date_BC
              , Last_User,                   Last_Stamp
              )
         SELECT Gic_Comp_Data.GVKey,         Gic_Comp_Data.Conm
              , Gic_Comp_Data.Constat,       Gic_Comp_Data.Fic
              , Gic_Comp_Data.GGroup,        Gic_Comp_Data.GInd
              , Gic_Comp_Data.GSector,       Gic_Comp_Data.GSubind
              , Gic_Comp_Data.Gvkey_T,       Gic_Comp_Data.Loc
              , Gic_Comp_Data.Fri_Issuer
              , Gic_Comp_Data.Incorporation_Nation_Code
              , Gic_Comp_Data.Residence_Nation_Code
              , Gic_Comp_Data.Creation_Date, Gic_Comp_Data.Last_Available_Date
              , Gic_Comp_Data.Delivery_Date, NULL
              , user_id,                     SYSDATE
           FROM dual
          WHERE NOT EXISTS (
                SELECT 'X'
                  FROM Gic_Comp GC
                 WHERE GC.GVKEY         = Gic_Comp_Data.GVKEY
                   AND GC.Delivery_Date = Gic_Comp_Data.Delivery_Date
                );
         pgmloc := 490;
         IF SQL%ROWCOUNT > 0 THEN
            rows_inserted := rows_inserted + SQL%ROWCOUNT;
            p_rows_ins    := p_rows_ins    + SQL%ROWCOUNT;
            IF p_rows_ins >= p_commit_count THEN
               pgmloc := 500;
               Pricing_Msg_Rec.Value_01 :=  Rows_Read;
               Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
               Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
               Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
               Pricing_Msg_Rec.Msg_Text_2 :=
                 'Processing ' || C_Gic_Comp || ' records';
               --
               pgmloc := 510;
               Global_File_Updates.Update_Pricing_Msgs
                 (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
               pgmloc := 520;
               COMMIT;
               p_rows_ins := 0;
            END IF;
         ELSE
            rows_rejected := rows_rejected + 1;
         END IF;
      END LOOP; --}
   ELSE --}{ Gic_Secur
      pgmloc := 530;
      FOR Gic_Secur_Data IN Gic_Secur_Details( Previous_Delivery_Date )
      LOOP --{
         rows_read := rows_read + 1;
         Gic_Secur_Data.Delivery_Date := Restart_Date;
         pgmloc := 540;
         INSERT
           INTO Gic_Secur
              ( GVKEY,                        IID
              , Cusip,                        DSCI
              , Exchg
              , GVKEY_T,                      IID_T
              , Isin,                         SecStat
              , Sedol,                        Tic
              , TPCI,                         Fri_Ticker
              , Source_Code,                  Security_Type_Code
              , Creation_Date,                Last_Available_Date
              , Delivery_Date,                Delivery_Date_BC
              , Last_User,                    Last_Stamp
              )
         SELECT Gic_Secur_Data.GVKEY,         Gic_Secur_Data.IID
              , Gic_Secur_Data.Cusip,         Gic_Secur_Data.DSCI
              , Gic_Secur_Data.Exchg
              , Gic_Secur_Data.GVKEY_T,       Gic_Secur_Data.IID_T
              , Gic_Secur_Data.Isin,          Gic_Secur_Data.SecStat
              , Gic_Secur_Data.Sedol,         Gic_Secur_Data.Tic
              , Gic_Secur_Data.TPCI,          Gic_Secur_Data.Fri_Ticker
              , Gic_Secur_Data.Source_Code,   Gic_Secur_Data.Security_Type_Code
              , Gic_Secur_Data.Creation_Date, Gic_Secur_Data.Last_Available_Date
              , Gic_Secur_Data.Delivery_Date, NULL
              , user_id,                      SYSDATE
           FROM dual
          WHERE NOT EXISTS (
                SELECT 'X'
                  FROM Gic_Secur GS
                 WHERE GS.GVKEY         = Gic_Secur_Data.GVKEY
                   AND GS.IID           = Gic_Secur_Data.IID
                   AND GS.Delivery_Date = Gic_Secur_Data.Delivery_Date
                );
         pgmloc := 550;
         IF SQL%ROWCOUNT > 0 THEN
            rows_inserted := rows_inserted + SQL%ROWCOUNT;
            p_rows_ins    := p_rows_ins    + SQL%ROWCOUNT;
            IF p_rows_ins >= p_commit_count THEN
               pgmloc := 560;
               Pricing_Msg_Rec.Value_01 :=  Rows_Read;
               Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
               Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
               Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
               Pricing_Msg_Rec.Msg_Text_2 :=
                 'Processing ' || C_GIC_SECUR || ' records';
               pgmloc := 570;
               Global_File_Updates.Update_Pricing_Msgs
                 (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
               pgmloc := 580;
               COMMIT;
               p_rows_ins := 0;
            END IF;
         ELSE
            rows_rejected := rows_rejected + 1;
         END IF;
      END LOOP; --}
   END IF; --} if Gic_secur
--
--
-- nous mettons a jour le Last_Run_Date de supplier_running_parms
--
   UPDATE Supplier_Running_Parms SRP
      SET Last_Run_Date  = sysdate
    WHERE SRP.Program_Id = P_Program_Id;
--
-- If #read = #reject ---> Carry Forward is run for a 2nd time
--
   pgmloc := 590;
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
   --
   -- Si aucune rangee a ete lu, on veut faire un raise pour avoir
   -- le message approprie
   IF rows_read = 0
   THEN
      RAISE No_Data;
   END IF;
   --
   IF rows_read = rows_rejected
   THEN
      Pricing_Msg_Rec.Msg_Text_2  :=
        'WARNING - Carry Forward was already DONE';
      --
   ELSE
      Pricing_Msg_Rec.Msg_Text_2  := 'Carry Forward DONE ';
      IF P_Restart_Flag = 'Y'
      THEN
         Pricing_Msg_Rec.Msg_Text_2 := Pricing_Msg_Rec.Msg_Text_2 ||
           'Restarted for ' || to_char(P_Restart_Date,'DD-MON-YYYY');
      END IF;
   END IF;
   --
<< Get_out >>
   --
   pgmloc := 600;
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
    (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
    --
   COMMIT;
   --
-- Ajout - JSL - 10 septembre 2014
   Set_Tms_to_Unscheduled( Pricing_Msg_Rec );
-- Fin d'ajout- JSL - 10 septembre 2014
--
EXCEPTION
--
-- Le dernier carry forward n'a pas fonctionne. On arrete pour permettre
-- une verification.
--
WHEN Bad_Previous_Cf
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Msg_Text    :=
     'Previous CF '                             ||
      to_char(Previous_Delivery_Date,'ddmonyyyy') ||
     ' didn''t work.Create a rec succ Y in '    ||
     'Prcg Msg';
   --
   Pricing_Msg_Rec.Msg_Text_2  :=
     'OR Change the Dates in Site_Controls (Take a copy of the table)';
   --
   Pricing_Msg_Rec.Err_Text    :=
     'Recompile the proc and run the Carry Fwd of '     ||
     to_char(Previous_Delivery_Date,'ddmonyyyy') ||  ' '  ||
     V_CF_Delivery_Time;
   --
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN Cannot_Restart
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN inv_prc_date
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
---
WHEN no_data_found
THEN
--
   Pricing_Msg_Rec.Err_Text := 'No Data Found.  Probable Cause ' ||
                                'Pgm Id ' || P_Program_ID ||
                               ' missing from Break_Points';
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN no_data
THEN
--
   Pricing_Msg_Rec.Remarks     := 'No data to apply carry forward on';
   IF Temp_First_Run_Date = P_Delivery_Date
   THEN
      Pricing_Msg_Rec.Remarks     := Pricing_Msg_Rec.Remarks  ||
                                     ' - First Carry Fwd';
   END IF;
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
--
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
-- Pour toutes les autres exceptions, on met le message d'oracle
--
WHEN others
THEN
   IF NOT(Handled_Raise)
   THEN
      -- Err_Text is 255 characters long (not 68)
      -- Pricing_Msg_Rec.Err_Text := substr(SQLERRM(SQLCODE),1,68);
      Pricing_Msg_Rec.Err_Text    := substr(SQLERRM(SQLCODE),1,255);
      Pricing_Msg_Rec.Msg_Text_2  :=
        'Unhandled exception occurred at pgmloc:'||to_char(pgmloc);
      Pricing_Msg_Rec.Successfull_Flag := 'N';
      Pricing_Msg_Rec.Value_01 :=  Rows_Read;
      Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
      Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
      Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
      --
      Global_File_Updates.Update_Pricing_Msgs
        (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   END IF;
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   IF NOT(Handled_Raise)
   THEN
      -- We may be doing a rollback in send email...
      Send_Email( Pricing_Msg_Rec );
   END IF;
-- Fin d'ajout - JSL - 10 septembre 2014
   RAISE;
--}}
END on_gics;
--
-----------------------------------------------------------------------
--
-- Procedure pour appliquer les CFW sur DS_Security_Prices
--
PROCEDURE On_DS_Security_Prices
        ( p_program_id     IN   ops_scripts.program_id%TYPE
        , p_delivery_date  IN   DATE
        , p_supplier_code  IN   DATA_SUPPLIERS.CODE%TYPE
        , p_delivery_time  IN   VARCHAR2
        , p_commit_count   IN   NUMBER
        , p_restart_flag   IN   VARCHAR2
        , p_restart_date   IN   DATE     := NULL
        , p_load_pgm_id    IN   ops_scripts.program_id%TYPE
        , P_FROM_SCREEN    IN   VARCHAR2 := NULL
        ) IS --{
--
inv_prc_date           exception;
cannot_restart         EXCEPTION;
Invalid_zero_par       EXCEPTION;
p_rows_ins             NUMBER :=0;
rows_inserted          NUMBER :=0;
rows_read              NUMBER :=0;
rows_rejected          NUMBER :=0;
rows_skip              NUMBER :=0;
rows_deleted           NUMBER :=0;
rows_updated           NUMBER :=0;
Handled_Raise          BOOLEAN := FALSE;
V_Del_Flag             VARCHAR2(1) := 'N';
V_Message              VARCHAR2(1000) := 'NULL';
V_Message_Code         Oasis_Messages.Code%TYPE;
V_Message_Val1         Varchar2(80);
--
-- Variables pour la mise a jour de DS_Security_Prices selon
-- le champs Constant_Prices_Flag, si le champs = 'Y' les dates qui
-- ne sont pas nuls, seront assigne TRUNC(Delivery_Date)
--
temp_origin            Price_Origins.Code%Type;
temp_updated_flag      yes_no.code%Type;
temp_price_date        DATE;
temp_bid_date          DATE;
temp_ask_date          DATE;
temp_mid_date          DATE;
--
temp_pgm_id            OPS_Scripts.Program_Id%TYPE;
temp_first_run_date    DATE;
restart_date           DATE;
cfw_date               DATE := NULL;
temp_succ_flag         yes_no.code%TYPE;
temp_buffer            CHAR(255);
Price_Origin_Code      Price_Origins.Code%TYPE;
Intra_Day              BOOLEAN;
--
Last_Fri_Ticker        Securities.Fri_Ticker%TYPE := 0;
--
char_delivery_time     CHAR(25);
user_id                user_list.user_id%TYPE := Constants.Get_User_Id;
restart_status         break_points.status%TYPE;
--
v_delivery_date        DATE;
previous_delivery_date DATE;
--
v_cf_delivery_time     data_supply_schedule.cf_delivery_time%TYPE;
--
Final_Status           Trading_Status.Code%TYPE;
At_Par_Flag            Yes_No.Code%TYPE;
temp_del_Date          DATE;
Par_Origin             Price_Origins.Code%TYPE:='R';
--
CURSOR Cur_Get_CFW_Date
     ( I_Supplier_Code Data_Suppliers.Code%TYPE
     , I_Restart_Date  DATE
     ) IS
SELECT MAX(Delivery_Date)
  FROM DS_Security_Prices
 WHERE Data_Supplier_Code = I_Supplier_Code
   AND Delivery_Date      < I_Restart_Date;
--
-- Ce curseur va chercher les enr de Security_Prices sur lesquels on
-- veut appliquer le CARRY FORWARD
--
CURSOR PRICING_DETAILS
     ( I_Last_Fri_Ticker        Securities.Fri_Ticker%TYPE
     , I_Data_Supplier          Data_Suppliers.Code%TYPE
     , I_Previous_Delivery_Date DATE
     ) IS
SELECT /*+ ALL_ROWS */
       A.*
     , S.Security_Type_Code
     , S.Income_Frequency_Value
     , S.Trading_Status_Code
     , S.Date_Of_Status
     , S.Par_Value
     , S.Par_Currency
     , T.Active_Flag
     , D.Accept_Zero_Prices_Flag
  FROM DS_Security_Prices   A
     , SECURITIES           S
     , TRADING_STATUS       T
     , Data_supply_Schedule D
 WHERE A.DATA_SUPPLIER_CODE  = I_Data_Supplier
   AND A.Fri_Ticker          > I_Last_Fri_Ticker
   AND A.DELIVERY_DATE       = I_PREVIOUS_DELIVERY_DATE
   AND S.Fri_Ticker          = A.Fri_Ticker
   AND S.Trading_Status_Code = T.Code
   AND D.Data_Supplier_Code  = A.Data_Supplier_Code
   AND D.Delivery_Time       = To_Char(A.Delivery_Date,'HH24MI')
Order by A.Delivery_Date,
         A.Fri_Ticker,
         A.Nation_Code;
--
-- Curseur ajoutes pour verifier l'activite d'une titre et determiner
-- s'il faut oui ou non copier la rangee
--
Pricing_Data Pricing_Details%ROWTYPE;
--
Last_Price_Date DATE := Constants.Get_Site_Date;
--
CURSOR CUR_TRD_ACT
     ( p_fri_ticker securities.fri_ticker%TYPE
     , p_source     sources.code%TYPE
     , p_currency   currencies.currency%TYPE
     , In_Date      DATE
     ) IS
SELECT A.active_flag
  FROM trading_status A, security_trading_status B
 WHERE B.fri_ticker                                 = p_fri_ticker
   --mlz - 26nov2014
   --AND pricing_sources.get_synonym( B.source_code ) = p_source
   AND B.source_code = p_source
   --endmlz - 26nov2014
   AND B.price_currency                             = p_currency
   AND B.date_of_status = (
       SELECT MAX(Y.date_of_status)
         FROM security_trading_status Y
        WHERE Y.fri_ticker      = B.fri_ticker
          AND Y.source_code     = B.source_code
          AND Y.price_currency  = B.price_currency
          AND Y.Date_Of_Status <= In_Date
       )
   AND B.trading_status_code = A.code;
--
v_active_flag      yes_no.code%TYPE;
v_last_active_flag yes_no.code%TYPE;
v_dummy            VARCHAR2(1);
ok_to_copy         BOOLEAN;
--
CURSOR CUR_PRICE_PRESENT
     ( p_fri_ticker        securities.fri_ticker%TYPE
     , p_Nation            Nations.code%TYPE
     , P_Previous_Delivery DATE
     , p_date_cour         DATE
     ) IS
SELECT 'x'
  FROM DS_Security_Prices
 WHERE fri_ticker    =  p_fri_ticker
   AND Nation_code   != p_Nation
   AND delivery_date =  P_Previous_Delivery
   AND price_date    >= p_date_cour;
--
-- Modifie pour permettre des Restart meme s'il existe des rangees
-- pour le meme fournisseur, tant que le CF_Delivery_Time de ces
-- rangees ne pointe pas sur les rangees sur lesquelles le Restart se
-- fera
--
CURSOR Cur_Rows_Before_Restart
     ( P_Restart_Date       DATE
     , P_Data_Supplier_Code Data_Suppliers.Code%TYPE
     , P_Del_Time           Data_Supply_Schedule.Delivery_Time%TYPE
     ) IS
SELECT DISTINCT 'X'
  FROM DS_Security_Prices SP
     , Data_Supply_Schedule DSS
 WHERE SP.Delivery_Date        >  P_Restart_Date
   AND SP.Data_Supplier_Code   =  P_Data_Supplier_Code
   AND DSS.Data_Supplier_Code  =  SP.Data_Supplier_Code
   AND DSS.Delivery_Time       =  to_char(SP.delivery_date,'hh24mi')
   AND DSS.Delivery_Time       <> P_Del_Time
   AND DSS.CF_Delivery_Time    =  P_Del_Time
UNION
SELECT DISTINCT 'X'
  FROM DS_Security_Prices SP
     , Data_Supply_Schedule DSS
 WHERE SP.Delivery_Date       >  (P_Restart_Date+1)
   AND SP.Data_Supplier_Code  =  P_Data_Supplier_Code
   AND DSS.Data_Supplier_Code =  SP.Data_Supplier_Code
   AND DSS.Delivery_Time      =  to_char(SP.delivery_date,'hh24mi')
   AND DSS.CF_Delivery_Time   =  P_Del_Time;
--
-- Not Run Yet Flag cannot be looked at
-- The Successfull_Flag = Y must be the one to be fetched
-- This is the only known success in Oasis
--
CURSOR Cur_Previous_CFW_Done
     ( I_Program_Id        Ops_Scripts.Program_Id%TYPE
     , I_Previous_Delivery DATE
     ) IS
SELECT PM.Successfull_Flag
  FROM Pricing_Msgs      PM
 WHERE PM.Program_Id       = I_Program_Id
   AND PM.Date_of_Prices   = TRUNC(I_Previous_Delivery)
   AND PM.Date_Of_Msg      = (
       SELECT MAX(PM2.Date_of_Msg)
         FROM Pricing_Msgs      PM2
            , Successfull_Flags SF2
        WHERE PM2.Program_id       = PM.Program_id
          AND PM2.Date_of_Prices   = PM.Date_of_Prices
          AND PM2.Successfull_Flag = SF2.Code
          AND SF2.Not_Run_Yet_Flag = 'N'
       );
--
-- cursor pour aller chercher le champs Constant_Prices_Flag
-- de Securities
--
CURSOR Get_Constant_Prices_Flag
     ( P_Fri_Ticker Securities.Fri_Ticker%TYPE
     ) IS
SELECT S.Constant_Prices_Flag
  FROM Securities S
 WHERE S.Fri_Ticker = P_Fri_Ticker;
--
-- New cursor to get the information from security_patterns
--
CURSOR Get_Sec_Pattern
     ( Sec_Type    Security_Types.Code%TYPE
     , Freq        Frequencies.Value%TYPE
     ) IS
SELECT Final_Trading_Status_Code
     , Last_Price_At_Par_Flag
  From Security_Patterns
 Where Code = Security.Get_Sec_Type_Pattern(Sec_Type, Freq);
--
END_OF_LOOP          BOOLEAN:=FALSE;
Constant_Prices_Flag Yes_No.Code%TYPE;
--
Temp_Field           BOOLEAN:=FALSE;
--
no_data         EXCEPTION;
Bad_Previous_Cf EXCEPTION;
--
BEGIN --{
   --
   -- Start of Job
   --
   User_Id       := Constants.Get_User_Id;
   G_Program_Id  := P_Program_Id;
   G_System_Date := SYSDATE;
   --
   Global_File_Updates.Initialize_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   Pricing_Msg_Rec.Msg_Text :=
     'CFW on Security Prices for:'|| p_supplier_code || ', ' ||
      to_char(p_delivery_date,'dd-mon-yyyy') || ' ' || p_delivery_time;
   --
   Pricing_Msg_Rec.Msg_Text_2       := 'Start of Program';
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   --
   IF P_Restart_Flag = 'Y'
   THEN
      Pricing_Msg_Rec.Date_of_Prices := P_Restart_Date;
   END IF;
   --
   Global_File_Updates.Update_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   pgmloc := 610;
   COMMIT;
   --
   -- Aller chercher le dernier status
   --
   IF p_load_pgm_id IS NOT NULL
   THEN
      pgmloc := 620;
      SELECT status
        INTO restart_status
        FROM break_points
       WHERE program_id = p_load_pgm_id;
   ELSE
      restart_status := NULL;
   END IF;
   --
   -- Validation des parametres Data_supplier et Delivery time
   -- Si ce n'est pas valide, l'exception NO_DATA_FOUND est levee
   -- automatiquement
   --
   pgmloc := 630;
   SELECT cf_delivery_time
     INTO v_cf_delivery_time
     FROM data_supply_schedule
    WHERE data_supplier_code = p_supplier_code
      AND delivery_time = p_delivery_time;
   --
   pgmloc := 640;
   IF Constants.G_Site_Date <> TRUNC(p_delivery_date)
   THEN
      Pricing_Msg_Rec.Msg_Text_2 := 'Error while validating parameters';
      Pricing_Msg_Rec.Err_Text   :=
         'Invalid price date parameter. Must be equal to New Day.';
      RAISE inv_prc_date;
   END IF;
   --
   -- On set la delivery_date
   --
   v_delivery_date :=
      to_date(to_char(p_delivery_date,'dd-mon-yyyy') || ' ' ||
      p_delivery_time,'DD-MON-YYYY HH24MI');
   --
   -- Nous devons verifier si les prix sont des Intra-Day
   -- pour assigner le Price_Origin_Code
   Intra_Day := Supplier.Is_It_Intra_Day(P_Supplier_Code, P_Delivery_Time);
   --
   IF Intra_Day
   THEN
      Price_Origin_Code := 'I';
   ELSE
      Price_Origin_Code := 'C';
   END IF;
   --
   pgmloc := 650;
   IF P_Restart_Flag = 'N'
   THEN
      Restart_Date:= v_delivery_date;
      --
      IF V_CF_Delivery_Time < P_Delivery_Time
      THEN
         Previous_Delivery_Date :=
            to_date(to_char(Constants.G_Site_Date,'DD-MON-YYYY') ||
              ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      ELSE
         Previous_Delivery_Date :=
            to_date(to_char(Constants.G_Previous_Site_Date,'DD-MON-YYYY') ||
              ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      END IF;
      --
   ELSE
      --
      IF P_Restart_Date = Constants.G_Site_Date
      THEN
         CFW_Date := Constants.G_Previous_Site_Date;
      ELSE
         -- Le Cursor sera remplace par la nouvelle fonction
         -- Holiday.Get_Last_Open_Date
         CFW_Date := Holiday.Get_Last_Open_Date(
            Constants.G_Previous_Site_Date,'HOLCLASS',NULL,'STP');
      END IF;
      --
      Restart_Date :=
        TO_DATE(TO_CHAR(P_Restart_Date,'DD-MON-YYYY')  || ' ' ||
        P_Delivery_Time,'DD-MON-YYYY HH24MI');
      --
      IF V_CF_Delivery_Time < P_Delivery_Time
      THEN
         Previous_Delivery_Date :=
            to_date(to_char(P_Restart_Date,'DD-MON-YYYY') ||
            ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      ELSE
         Previous_Delivery_Date :=
            to_date(to_char(CFW_Date,'DD-MON-YYYY') ||
            ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      END IF;
   END IF;
   --
   -- Nous devons maintenant s'assurer que le CFWD de la date
   -- Previous_Delivery_Date a ete execute
   --
   SELECT SRP.Program_Id
        , SRP.First_Run_Date
     INTO Temp_Pgm_Id
        , Temp_First_Run_date
     FROM Supplier_Running_Parms SRP
    WHERE SRP.Data_Supplier_Code = P_Supplier_Code
      AND SRP.Delivery_Time      = V_CF_Delivery_Time
      AND SRP.Product_Code       = 'CFW';
   --
   IF Temp_First_Run_Date != P_Delivery_Date
   THEN
      OPEN Cur_Previous_CFW_Done( Temp_Pgm_Id, Previous_Delivery_Date );
      FETCH Cur_Previous_CFW_Done INTO Temp_Succ_Flag;
      IF Cur_Previous_CFW_Done%FOUND
      THEN
         CLOSE Cur_Previous_CFW_Done;
         IF Temp_Succ_Flag != 'Y'
         THEN
            Raise Bad_Previous_cf;
         END IF;
      ELSE
         CLOSE Cur_Previous_CFW_Done;
         Raise Bad_Previous_cf;
      END IF;
   END IF;
   --
   -- Si c'est un restart, on enleve tous les enr. pour le supplier et
   -- delivery time
   --
   pgmloc := 670;
   IF p_restart_flag = 'Y'
   OR restart_status = 'FRESTART'
   THEN
      IF p_restart_flag = 'Y'
      THEN
         --
         -- Nous devons s'assurer qu'il n'y a plus de rangee
         -- dans Security Prices plus recente que le restart date
         --
         OPEN Cur_Rows_Before_Restart( Restart_Date
                                     , P_Supplier_Code
                                     , P_Delivery_Time
                                     );
         FETCH Cur_Rows_Before_Restart
          INTO Temp_Buffer;
         IF Cur_Rows_Before_Restart%FOUND
         THEN
            CLOSE Cur_Rows_Before_Restart;
            Pricing_Msg_Rec.Err_Text:=
              'Cannot Restart Carry Fwd, Prices exist past restart date';
            RAISE Cannot_Restart;
         END IF;
         CLOSE Cur_Rows_Before_Restart;
      END IF;
      --
      WHILE NOT (end_of_loop)
      LOOP
         DELETE FROM DS_Security_Prices
          WHERE delivery_date      = restart_date
            AND data_supplier_code = p_supplier_code
            AND rownum            <= p_commit_count;
         --
         IF SQL%ROWCOUNT < p_commit_count
         THEN
            end_of_loop := TRUE;
         END IF;
         Rows_Deleted := Rows_Deleted + SQL%ROWCOUNT;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Global_File_Updates.Update_Pricing_Msgs( Pricing_Msg_Rec.Program_Id
                                                , Pricing_Msg_Rec.Date_of_Msg
                                                , FALSE
                                                , Pricing_Msg_Rec
                                                );
         COMMIT;
      END LOOP;
   END IF;
   --
   -- Fetch Options and apply Carry Forward - Using new Price Date
   -- Un LOOP est utilise pour pouvoir fermer et re-ouvrir le Cursor l'hors
   -- d'un commit
   --
   OPEN Pricing_Details(Last_Fri_Ticker, P_Supplier_Code, Previous_Delivery_Date);
   LOOP
      --
      FETCH Pricing_Details INTO Pricing_Data;
      IF Pricing_Details%NOTFOUND
      THEN
         EXIT;
      END IF;
      pgmloc := 680;
      --
      if official_prices.process_security( p_supplier_code
                                         , p_delivery_time
                                         , Pricing_Data.security_type_code
                                         ) = false
      then
         rows_skip := rows_skip + 1;
         GOTO End_Of_Loop;
      end if;
      --
      -- on verifie si un commit est necessaire
      --
      IF  P_Rows_Ins >= P_Commit_Count
      AND Pricing_Data.Fri_Ticker <> Last_Fri_Ticker
      THEN
         pgmloc := 690;
         CLOSE Pricing_Details;
         -- nous mettons a jour les table Pricing_Msgs et Break Points
         --
         Pricing_Msg_Rec.Value_01 :=  Rows_Read;
         Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
         Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
         Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Pricing_Msg_Rec.Msg_Text_2 :=
            'Processing security prices records';
         --
         Global_File_Updates.Update_Pricing_Msgs
            (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
         COMMIT;
         --
         p_rows_ins := 0;
         --
         COMMIT;
         pgmloc := 700;
         OPEN Pricing_Details(Last_Fri_Ticker, P_Supplier_Code, Previous_Delivery_Date);
         --
         -- La rangee doit etre refetchee (Si non, 1 rangee dans les
         -- Rejected a chaque batch
         -- Richard (18 Oct 96)
         FETCH Pricing_Details INTO Pricing_Data;
         IF Pricing_Details%NOTFOUND
         THEN
            pgmloc := 710;
            EXIT;
         END IF;
      END IF;
      rows_read := rows_read + 1;
      --
      -- On verifie la condition pour copier
      --
      ok_to_copy := FALSE;
      v_active_Flag := Pricing_data.Active_Flag;
      IF v_active_flag = 'N' THEN
         pgmloc := 730;
         ok_to_copy := TRUE;
      ELSE
         pgmloc := 740;
         OPEN CUR_TRD_ACT( pricing_data.fri_ticker
                         --mlz - 26nov2014
                         --, pricing_sources.get_Synonym( pricing_data.source_code )
                         , pricing_data.source_code
                         --endmlz - 26nov2014
                         , pricing_data.price_currency
                         , Last_Price_Date
                         );
         FETCH CUR_TRD_ACT
          INTO v_active_flag;
         IF CUR_TRD_ACT%NOTFOUND
         THEN
            CLOSE CUR_TRD_ACT;
            V_Message_Code := 'PROC-CARRY_FWD-010';
            V_Message_Val1 := Security.Get_Fri_Id(Pricing_Data.Fri_Ticker);
            Manage_Messages.Get_Oasis_Message
                        (V_Message_Code,
                         V_Del_Flag,
                         V_Message_Val1
                  ||' '||pricing_data.source_code
                  ||' '||pricing_data.price_currency,
                         V_Message);
            Pricing_Msg_Rec.Err_Text := V_Message;
            RAISE no_data_found;
         END IF;
         CLOSE CUR_TRD_ACT;
         IF v_active_flag = 'N'
         THEN
            pgmloc := 750;
            ok_to_copy := TRUE;
         ELSE
            pgmloc := 760;
            ok_to_copy := TRUE;
         END IF;
      END IF;
      --
      IF ok_to_copy THEN
         pgmloc := 770;
         pricing_data.delivery_date :=  restart_date;
         --
         OPEN Get_Constant_Prices_Flag(Pricing_Data.Fri_Ticker);
         FETCH Get_Constant_Prices_Flag
          INTO Constant_Prices_Flag;
         CLOSE Get_Constant_Prices_Flag;
         --
         temp_Origin       := Price_Origin_Code;
         temp_Updated_Flag := 'N';
         temp_field        := FALSE;
         --
         -- Initialise the fields to the previous day values
         -- The constant price may set them to current date.
         Temp_Price_Date := Pricing_Data.Price_Date;
         Temp_Bid_Date   := Pricing_Data.Bid_Date;
         Temp_Mid_Date   := Pricing_Data.Mid_Date;
         Temp_Ask_Date   := Pricing_Data.Ask_Date;
         --
         IF Constant_Prices_Flag = 'Y' AND
            V_Active_Flag        = 'Y'
         THEN
            temp_Origin := 'F';
            --
            -- Checking if the source and the supplier are open.
            IF  (Holiday.Is_it_Open(P_Delivery_Date,
                                  'SOURCE',
                                   Pricing_Data.Source_Code,
                                   NULL) IS NOT NULL)
            OR
                (Holiday.Is_it_Open
                   (P_Delivery_Date,
                   'SUPPLIER',
                    Pricing_Data.Data_Supplier_Code,
                    TO_CHAR(Pricing_Data.Delivery_Date,'HH24MI'))
                    IS NOT NULL)
            THEN
               Temp_Field := TRUE;
               GOTO SKIP;
            END IF;
            --
            temp_Updated_Flag := 'Y';
            --
            IF Pricing_Data.Price_Date IS NOT NULL
            THEN
               Temp_Price_Date := TRUNC(Pricing_Data.Delivery_Date);
            END IF;
            --
            IF Pricing_Data.Bid_Date IS NOT NULL
            THEN
               Temp_Bid_Date := TRUNC(Pricing_Data.Delivery_Date);
            END IF;
            --
            IF Pricing_Data.Ask_Date IS NOT NULL
            THEN
               Temp_Ask_Date := TRUNC(Pricing_Data.Delivery_Date);
            END IF;
            --
            IF Pricing_Data.Mid_Date IS NOT NULL
            THEN
               Temp_Mid_Date := TRUNC(Pricing_Data.Delivery_Date);
            END IF;
            --
            <<SKIP>>
            NULL;
         ELSE
            Temp_Field := TRUE;
         END IF;
         --
         -- If we had a week-end or FRI holiday between previous site
         -- date and current site date
         -- Process of inactive securities turned inactive during that
         -- period
         -- Insert new price record with Price equal to Par and
         -- Price date = Maturity_date where applicable
         --
         pgmloc := 780;
         IF  Pricing_Data.Active_Flag = 'N'
         AND Pricing_Data.Price_Currency = Pricing_Data.Par_Currency
         THEN    -- {
            --
            -- Week-end or FRI closed days
            --
            pgmloc := 790;
--          One or more days closed and date of status is within the closed period
            IF (   TRUNC(Restart_Date)-TRUNC(Previous_Delivery_Date) > 1
               AND Pricing_Data.Date_Of_Status BETWEEN TRUNC(Previous_Delivery_Date+1)
                                                 AND TRUNC(Restart_Date-1)
               )
--          Date of status is today, but the supplier and/or source is closed
             OR (   Pricing_Data.Date_Of_Status = TRUNC( Restart_Date )
                AND (  holiday.is_it_open( trunc(restart_date), 'SUPPLIER', P_Supplier_Code, p_delivery_time ) IS NOT NULL
                    OR holiday.Is_it_open( trunc(restart_date), 'SOURCE', Pricing_Data.Source_Code, NULL ) IS NOT NULL
                    )
                )
            THEN   -- {
               pgmloc := 800;
               -- Check if the security was active on source on previous
               -- site date
               OPEN CUR_TRD_ACT(pricing_data.fri_ticker,
                                --mlz - 26nov2014
                                --pricing_sources.get_Synonym( pricing_data.source_code ),
                                pricing_data.source_code,
                                --endmlz - 26nov2014
                                pricing_data.price_currency,
                                TRUNC(Previous_Delivery_Date));
               FETCH CUR_TRD_ACT
                INTO v_last_active_flag;
               IF CUR_TRD_ACT%NOTFOUND
               THEN   -- {
                  pgmloc := 810;
                  CLOSE CUR_TRD_ACT;
                  V_Message_Code := 'PROC-CARRY_FWD-010';
                  V_Message_Val1 := Security.Get_Fri_Id(Pricing_Data.Fri_Ticker);
                  Manage_Messages.Get_Oasis_Message
                              (V_Message_Code,
                               V_Del_Flag,
                               -- Fix this patch (RV - 06 Oct 2012)
                               V_Message_Val1
                        ||' '||pricing_data.source_code
                        ||' '||pricing_data.price_currency,
                               -- End Fix this patch (RV - 06 Oct 2012)
                               V_Message);
                  Pricing_Msg_Rec.Err_Text := V_Message;
                  RAISE no_data_found;
               END IF;   --}
               CLOSE CUR_TRD_ACT;
               -- If it was active and just turned inactive during the
               -- week end or FRI holiday, then try to add the last
               -- price with par value if applicable
               IF V_Last_Active_Flag = 'Y'
               THEN   --{
                  pgmloc := 820;
                  OPEN Get_Sec_Pattern(Pricing_Data.Security_Type_Code,
                                       Pricing_Data.Income_Frequency_Value);
                  FETCH Get_Sec_Pattern INTO Final_Status,
                                             At_Par_Flag;
                  CLOSE Get_Sec_Pattern;
                  Pgmloc := 822;
                  IF  At_Par_Flag = 'Y'
                  AND Final_Status = Pricing_Data.Trading_status_Code
                  THEN --{
                     IF Pricing_Data.Par_Value = 0
                     THEN
                        NULL;
                     ELSE
                        IF NVL( Pricing_Data.Price, Pricing_Data.Bid )
                              / Pricing_Data.Par_Value
                           BETWEEN Min_Ratio AND Max_Ratio
                        THEN
                           NULL;
                        ELSIF NVL( Pricing_Data.Price, Pricing_Data.Bid )
                                 / 100.0
                              BETWEEN Min_Ratio AND Max_Ratio
                        THEN
                           Pricing_Data.Par_Value := 100.0;
                        ELSE
                           -- Par value is too far from price to set it.
                           At_Par_Flag := 'N';
                        END IF;
                     END IF;
                  END IF; --}
                  pgmloc :=830;
                  IF  At_Par_Flag = 'Y'
                  AND Final_Status = Pricing_Data.Trading_status_Code
                  THEN    -- {
                     BEGIN
                        -- if par value is 0 and this supplier cannot accept 0
                        -- prices, then abort
                        pgmloc :=840;
                        IF  Pricing_Data.Accept_Zero_Prices_Flag='N'
                        AND Pricing_Data.Par_Value = 0
                        THEN
                           RAISE Invalid_Zero_par;
                        END IF;
                        --
                        -- Set new values in Security_Prices
                        --
                        pgmloc :=850;
                        Pricing_Data.Delivery_Date :=
                            TO_DATE(TO_CHAR(Pricing_Data.Date_Of_Status,
                                            'DDMONYYYY') || ' ' ||
                                            P_Delivery_Time,
                                            'DDMONYYYY HH24MI');
                        temp_price_date     := Pricing_Data.Date_Of_Status;
                        pricing_data.price  := Pricing_Data.Par_Value;
                        pricing_data.volume           := NULL;
                        pricing_data.high             := NULL;
                        pricing_data.low              := NULL;
                        pricing_data.open_price       := NULL;
                        pricing_data.yield            := NULL;
                        pricing_data.board_lot_flag   := NULL;
                        pricing_data.last_trade_time  := NULL;
                        pricing_data.number_of_trans  := NULL;
                        pricing_data.Market_Cap       := NULL;
                        IF Pricing_Data.Dirty_Price IS NOT NULL
                        THEN
                           Pricing_Data.Dirty_Price := Pricing_Data.Par_Value;
                           Pricing_Data.Accrued_Interest   := 0;
                           Pricing_Data.Price_Mod_Duration := 0;
                           Pricing_Data.Term_in_Years      := 0;
                           Pricing_Data.Maturity_Date
                             := TO_CHAR( Pricing_Data.Date_of_Status
                                       , 'DD-MON-YYYY'
                                       );
                        END IF;
                        IF Pricing_Data.Bid IS NOT NULL
                        THEN
                           temp_bid_date          := Pricing_Data.Date_Of_Status;
                           pricing_data.bid       := Pricing_Data.Par_Value;
                           pricing_data.bid_yield := NULL;
                        END IF;
                        IF Pricing_Data.Bid_Dirty_Price IS NOT NULL
                        THEN
                           Pricing_Data.Bid_Dirty_Price := Pricing_Data.Par_Value;
                           Pricing_Data.Bid_Mod_Duration := 0;
                        END IF;
                        IF Pricing_Data.Ask IS NOT NULL
                        THEN
                           temp_ask_date          := Pricing_Data.Date_Of_Status;
                           pricing_data.ask       := Pricing_Data.Par_Value;
                           pricing_data.ask_yield := NULL;
                        END IF;
                        IF Pricing_Data.Ask_Dirty_Price IS NOT NULL
                        THEN
                           Pricing_Data.Ask_Dirty_Price := Pricing_Data.Par_Value;
                           Pricing_Data.Ask_Mod_Duration := 0;
                        END IF;
                        IF Pricing_Data.Mid IS NOT NULL
                        THEN
                           temp_mid_date          := Pricing_Data.Date_Of_Status;
                           pricing_data.mid       := Pricing_Data.Par_Value;
                           pricing_data.mid_yield := NULL;
                        END IF;
                        IF Pricing_Data.Mid_Dirty_Price IS NOT NULL
                        THEN
                           Pricing_Data.Mid_Dirty_Price := Pricing_Data.Par_Value;
                           Pricing_Data.Mid_Mod_Duration := 0;
                        END IF;
                        --
                        -- INSERT NEW pricing record at par
                        --
                        pgmloc :=860;
                        INSERT INTO DS_Security_Prices
                             ( fri_ticker
                             , nation_code
                             , source_code
                             , price_currency
                             , delivery_date
                             , data_supplier_code
                             , price_date
                             , price
                             , volume
                             , high
                             , low
                             , open_price
                             , yield
                             , board_lot_flag
                             , last_trade_time
                             , bid_date
                             , bid
                             , bid_yield
                             , ask_date
                             , ask
                             , ask_yield
                             , mid_date
                             , mid
                             , mid_yield
                             , number_of_trans
                             , user_key_code
                             , updated_flag
                             , price_origin_code
                             , last_user
                             , last_stamp
                             , dirty_price
                             , price_mod_duration
                             , bid_dirty_price
                             , bid_mod_duration
                             , ask_dirty_price
                             , ask_mod_duration
                             , mid_dirty_price
                             , mid_mod_duration
                             , accrued_interest
                             , term_in_years
                             , settlement_date
                             , maturity_date
                             , maturity_type
                             , index_ratio
                             , rpb_factor
                             , effective_date_factor
                             , next_chg_date_factor
                             , benchmark_code
                             , spread
                             , spread_from_generic
                             , spread_from_libor
                             , convexity
                             , pvbp
                             , average_life_date
                             , Market_Cap 
                             , IFRS_Code 
                             , High_52w 
                             , Low_52w 
                             , Official_Close 
                             , Trade_Count 
                             , Close_Cumulative_Volume 
                             , Close_Cumulative_Volume_Status 
                             , Close_Cumulative_Volume_Date
                             )
                        SELECT pricing_data.fri_ticker
                             , pricing_data.Nation_code
                             , pricing_data.source_code
                             , pricing_data.price_currency
                             , Pricing_Data.Delivery_Date
                             , pricing_data.data_supplier_code
                             , temp_Price_Date
                             , Pricing_Data.Price
                             , pricing_data.volume
                             , pricing_data.high
                             , pricing_data.low
                             , pricing_data.open_price
                             , pricing_data.yield
                             , pricing_data.board_lot_flag
                             , pricing_data.last_trade_time
                             , temp_bid_date
                             , pricing_data.bid
                             , pricing_data.bid_yield
                             , temp_ask_date
                             , pricing_data.ask
                             , pricing_data.ask_yield
                             , temp_mid_date
                             , pricing_data.mid
                             , pricing_data.mid_yield
                             , pricing_data.Number_Of_Trans
                             , Pricing_Data.User_Key_Code
                             , 'Y'
                             , Par_Origin  -- Price origin - new code
                             , user_id
                             , SYSDATE
                             , Pricing_Data.dirty_price
                             , Pricing_Data.price_mod_duration
                             , Pricing_Data.bid_dirty_price
                             , Pricing_Data.bid_mod_duration
                             , Pricing_Data.ask_dirty_price
                             , Pricing_Data.ask_mod_duration
                             , Pricing_Data.mid_dirty_price
                             , Pricing_Data.mid_mod_duration
                             , Pricing_Data.accrued_interest
                             , Pricing_Data.term_in_years
                             , Pricing_Data.settlement_date
                             , Pricing_Data.maturity_date
                             , Pricing_Data.maturity_type
                             , Pricing_Data.index_ratio
                             , Pricing_Data.rpb_factor
                             , Pricing_Data.effective_date_factor
                             , Pricing_Data.next_chg_date_factor
                             , Pricing_Data.benchmark_code
                             , Pricing_Data.spread
                             , Pricing_Data.spread_from_generic
                             , Pricing_Data.spread_from_libor
                             , Pricing_Data.convexity
                             , Pricing_Data.pvbp
                             , Pricing_Data.average_life_date
                             , Pricing_Data.Market_Cap 
                             , Pricing_Data.IFRS_Code 
                             , Pricing_Data.High_52w 
                             , Pricing_Data.Low_52w 
                             , Pricing_Data.Official_Close 
                             , Pricing_Data.Trade_Count 
                             , Pricing_Data.Close_Cumulative_Volume 
                             , Pricing_Data.Close_Cumulative_Volume_Status 
                             , Pricing_Data.Close_Cumulative_Volume_Date
                          FROM dual;
                        rows_inserted := rows_inserted + SQL%ROWCOUNT;
                        p_rows_ins    := p_rows_ins    + SQL%ROWCOUNT;
                     EXCEPTION
                     -- IF already created, either manually or from last run
                     -- in case of a restart
                     WHEN DUP_VAL_ON_INDEX THEN
                        pgmloc :=870;
                        UPDATE DS_Security_Prices
                           Set Price_Date        = Temp_Price_Date
                             , Price             = Pricing_Data.Price
                             , Volume            = Pricing_Data.Volume
                             , High              = Pricing_Data.High
                             , Low               = Pricing_Data.Low
                             , Open_Price        = Pricing_Data.Open_Price
                             , Yield             = Pricing_Data.Yield
                             , Board_Lot_Flag    = Pricing_Data.Board_Lot_Flag
                             , Last_Trade_Time   = Pricing_Data.Last_Trade_Time
                             , Number_of_Trans   = Pricing_Data.Number_of_Trans
                             , User_Key_Code     = Pricing_Data.User_Key_Code
                             , Updated_Flag      = 'Y'
                             , Price_Origin_Code = Par_Origin
                             , Last_User         = user_id
                             , Last_Stamp        = SYSDATE
                             , Accrued_Interest  = Pricing_Data.Accrued_Interest
                             , Maturity_Date     = Pricing_Data.Maturity_Date
                             , Term_in_Years     = Pricing_Data.Term_in_Years
                             , Dirty_Price       = Pricing_Data.Dirty_Price
                             , Price_Mod_Duration= Pricing_Data.Price_Mod_Duration
                             , Market_Cap        = Pricing_Data.Market_Cap 
                             , IFRS_Code         = Pricing_Data.IFRS_Code 
                             , High_52w          = Pricing_Data.High_52w 
                             , Low_52w           = Pricing_Data.Low_52w 
                             , Official_Close    = Pricing_Data.Official_Close 
                             , Trade_Count       = Pricing_Data.Trade_Count 
                             , Close_Cumulative_Volume
                                                 = Pricing_Data.Close_Cumulative_Volume 
                             , Close_Cumulative_Volume_Status
                                                 = Pricing_Data.Close_Cumulative_Volume_Status 
                             , Close_Cumulative_Volume_Date
                                                 = Pricing_Data.Close_Cumulative_Volume_Date
                        WHERE Fri_Ticker         = Pricing_Data.Fri_Ticker
                        AND   Data_Supplier_Code = P_Supplier_Code
                        AND   Nation_Code        = Pricing_Data.Nation_Code
                        AND   delivery_Date      = Pricing_Data.Delivery_Date
                        AND   Price_Currency     = Pricing_Data.Price_Currency;
                        rows_Updated := rows_Updated + SQL%ROWCOUNT;
                        Handled_Raise := TRUE;
                     WHEN Invalid_Zero_Par THEN
                        Pricing_Msg_Rec.Err_Text    :=
                          To_Char(Pricing_Data.Fri_Ticker) ||
                          ' Creation of CFW for matured issues - ' ||
                          'Zero par value cannot be inserted in ' ||
                          'Security_Prices for supplier ' ||
                        Pricing_Data.Data_Supplier_Code || ' ' ||
                        P_Delivery_Time;
                        Pricing_Msg_Rec.Successfull_Flag := 'N';
                        Pricing_Msg_Rec.Value_01 :=  Rows_Read;
                        Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
                        Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
                        Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
                        Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
                        Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
                        --
                        Global_File_Updates.Update_Pricing_Msgs
                          (G_Program_Id, G_System_Date, TRUE,
                           Pricing_Msg_Rec);
                        --
                        COMMIT;
                        Handled_Raise := TRUE;
                        RAISE;
                     WHEN OTHERS THEN
                        IF NOT(Handled_Raise)
                        THEN
                           Pricing_Msg_Rec.Err_Text    :=
                              substr(SQLERRM(SQLCODE),1,255);
                           Pricing_Msg_Rec.Msg_Text_2  :=
                             'Unhandled exception occurred at pgmloc:'||
                             to_char(pgmloc);
                           Pricing_Msg_Rec.Successfull_Flag := 'N';
                           Pricing_Msg_Rec.Value_01 :=  Rows_Read;
                           Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
                           Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
                           Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
                           Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
                           Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
                           --
                           Global_File_Updates.Update_Pricing_Msgs
                             (G_Program_Id, G_System_Date, TRUE,
                              Pricing_Msg_Rec);
                        END IF;
                        --
                        COMMIT;
-- Ajout - JSL - 10 septembre 2014
                        IF NOT(Handled_Raise)
                        THEN
                           -- We may be doing a rollback in send email...
                           Send_Email( Pricing_Msg_Rec );
                        END IF;
-- Fin d'ajout - JSL - 10 septembre 2014
                        RAISE;
                     END;   -- End block
                  END IF;   --}  Insert at par price
               END IF;   --}  If was active on previous price date
            END IF;  -- } Case II - Week-end or FRI closed in between
         END IF;   -- } Process of inactive securities
         --
         pgmloc :=880;
         Pricing_Data.Delivery_Date :=
               TO_DATE(TO_CHAR(Restart_Date,
                               'DDMONYYYY') || ' ' ||
                               P_Delivery_Time,
                               'DDMONYYYY HH24MI');
         pgmloc :=890;
         INSERT INTO DS_Security_Prices
              ( fri_ticker
              , source_code
              , nation_code
              , price_currency
              , delivery_date
              , data_supplier_code
              , price_date
              , price
              , volume
              , high
              , low
              , open_price
              , yield
              , board_lot_flag
              , last_trade_time
              , bid_date
              , bid
              , bid_yield
              , ask_date
              , ask
              , ask_yield
              , mid_date
              , mid
              , mid_yield
              , number_of_trans
              , user_key_code
              , updated_flag
              , price_origin_code
              , last_user
              , last_stamp
              , dirty_price
              , price_mod_duration
              , bid_dirty_price
              , bid_mod_duration
              , ask_dirty_price
              , ask_mod_duration
              , mid_dirty_price
              , mid_mod_duration
              , accrued_interest
              , term_in_years
              , settlement_date
              , maturity_date
              , maturity_type
              , index_ratio
              , rpb_factor
              , effective_date_factor
              , next_chg_date_factor
              , benchmark_code
              , spread
              , spread_from_generic
              , spread_from_libor
              , convexity
              , pvbp
              , average_life_date
              , market_cap 
              , IFRS_Code 
              , High_52w 
              , Low_52w 
              , Official_Close 
              , Trade_Count 
              , Close_Cumulative_Volume 
              , Close_Cumulative_Volume_Status 
              , Close_Cumulative_Volume_Date
              )
         SELECT pricing_data.fri_ticker
              , pricing_data.source_code
              , pricing_data.nation_code
              , pricing_data.price_currency
              , pricing_data.delivery_date
              , pricing_data.data_supplier_code
              , temp_price_date
              , pricing_data.price
              , pricing_data.volume
              , pricing_data.high
              , pricing_data.low
              , pricing_data.open_price
              , pricing_data.yield
              , pricing_data.board_lot_flag
              , pricing_data.last_trade_time
              , temp_bid_date
              , pricing_data.bid
              , pricing_data.bid_yield
              , temp_ask_date
              , pricing_data.ask
              , pricing_data.ask_yield
              , temp_mid_date
              , pricing_data.mid
              , pricing_data.mid_yield
              , pricing_data.number_of_trans
              , pricing_data.user_key_code
              , temp_updated_flag
              , Temp_Origin
              , user_id
              , SYSDATE
              , Pricing_Data.dirty_price
              , Pricing_Data.price_mod_duration
              , Pricing_Data.bid_dirty_price
              , Pricing_Data.bid_mod_duration
              , Pricing_Data.ask_dirty_price
              , Pricing_Data.ask_mod_duration
              , Pricing_Data.mid_dirty_price
              , Pricing_Data.mid_mod_duration
              , Pricing_Data.accrued_interest
              , Pricing_Data.term_in_years
              , Pricing_Data.settlement_date
              , Pricing_Data.maturity_date
              , Pricing_Data.maturity_type
              , Pricing_Data.index_ratio
              , Pricing_Data.rpb_factor
              , Pricing_Data.effective_date_factor
              , Pricing_Data.next_chg_date_factor
              , Pricing_Data.benchmark_code
              , Pricing_Data.spread
              , Pricing_Data.spread_from_generic
              , Pricing_Data.spread_from_libor
              , Pricing_Data.convexity
              , Pricing_Data.pvbp
              , Pricing_Data.average_life_date
              , Pricing_Data.Market_Cap 
              , Pricing_Data.IFRS_Code 
              , Pricing_Data.High_52w 
              , Pricing_Data.Low_52w 
              , Pricing_Data.Official_Close 
              , Pricing_Data.Trade_Count 
              , Pricing_Data.Close_Cumulative_Volume 
              , Pricing_Data.Close_Cumulative_Volume_Status 
              , Pricing_Data.Close_Cumulative_Volume_Date
           FROM dual
          WHERE NOT EXISTS (
                SELECT 'X'
                  FROM DS_Security_Prices SS
                 WHERE SS.fri_ticker         = pricing_data.fri_ticker
                   AND SS.nation_code        = pricing_data.nation_code
                   AND SS.price_currency     = pricing_data.price_currency
                   AND SS.delivery_date      = pricing_data.delivery_date
                   AND SS.data_supplier_code = pricing_data.data_supplier_code
                );
         --
         -- SQLROWCOUNT will be equal to 1 - If no bugs.
         -- Add 1 to rejected rows if Insert failed.
         --
         IF SQL%ROWCOUNT > 0 THEN
            pgmloc := 900;
            rows_inserted := rows_inserted + SQL%ROWCOUNT;
            p_rows_ins    := p_rows_ins    + SQL%ROWCOUNT;
         ELSE
            rows_rejected := rows_rejected + 1;
         END IF;
      ELSE
         rows_skip := rows_skip + 1;
      END IF;
<< END_OF_LOOP >>
      Last_Fri_Ticker := Pricing_Data.Fri_Ticker;
   END LOOP;
   CLOSE Pricing_Details;
   --
   -- nous mettons a jour le Last_Run_Date de supplier_running_parms
   --
   UPDATE Supplier_Running_Parms SRP
      SET Last_Run_Date  = sysdate
    WHERE SRP.Program_Id = P_Program_Id;
   --
   -- If #read = #reject ---> Carry Forward is run for a 2nd time
   --
   pgmloc := 910;
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
   -- Si aucune rangee a ete lu, on veut faire un raise pour avoir
   -- le message approprie
   IF rows_read = 0
   THEN
      RAISE No_Data;
   END IF;
   --
   IF rows_read = rows_rejected THEN
      Pricing_Msg_Rec.Msg_Text_2 :=
        'WARNING - Carry Fwd was already DONE';
   ELSE
      Pricing_Msg_Rec.Msg_Text_2 := 'Carry Fwd DONE ';
      IF P_Restart_Flag = 'Y'
      THEN
         Pricing_Msg_Rec.Msg_Text_2 := Pricing_Msg_Rec.Msg_Text_2 ||
          'Restarted for ' || to_char(P_Restart_Date,'DD-MON-YYYY');
      END IF;
   END IF;
   --
   pgmloc := 920;
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
    (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
   --
-- Ajout - JSL - 10 septembre 2014
   Set_Tms_to_Unscheduled( Pricing_Msg_Rec );
-- Fin d'ajout- JSL - 10 septembre 2014
   --
EXCEPTION
--
WHEN inv_prc_date
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N'
   OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN no_data_found
THEN
   -- The error message is intialized at the time of raising the
   -- exception
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   --
   IF p_from_screen = 'N'
   OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN no_data
THEN
   Pricing_Msg_Rec.Remarks     := 'No data to apply carry forward on';
   --
   IF Temp_First_Run_Date = P_Delivery_Date
   THEN
      Pricing_Msg_Rec.Remarks  := Pricing_Msg_Rec.Remarks  ||
                                 ' - First Carry Fwd';
   END IF;
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   --
   IF p_from_screen = 'N'
   OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
-- Pour toutes les autres exceptions, on met le message d'oracle
--
WHEN Bad_Previous_Cf
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Msg_Text    := 'Previous CF '
                               || to_char(Previous_Delivery_Date,'ddmonyyyy')
                               || ' didn''t work.Create a rec succ Y in '
                               || 'Prcg Msg';
   --
   Pricing_Msg_Rec.Msg_Text_2  :=
     'OR Change the Dates in Site_Controls (Take a copy of the table)';
   --
   Pricing_Msg_Rec.Err_Text    :=
     'Recompile the proc and run the Carry Fwd of '     ||
     to_char(Previous_Delivery_Date,'ddmonyyyy') || ' '   ||
     V_CF_Delivery_Time;
   --
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N'
   OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN Cannot_Restart
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N'
   OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN others
THEN
   IF NOT(Handled_Raise)
   THEN
      Pricing_Msg_Rec.Err_Text    := substr(SQLERRM(SQLCODE),1,255);
      Pricing_Msg_Rec.Msg_Text_2  :=
        'Unhandled exception occurred at pgmloc:'||to_char(pgmloc);
      Pricing_Msg_Rec.Successfull_Flag := 'N';
      Pricing_Msg_Rec.Value_01 :=  Rows_Read;
      Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
      Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
      Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
      Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
      --
      Global_File_Updates.Update_Pricing_Msgs
        (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   END IF;
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   IF NOT(Handled_Raise)
   THEN
      -- We may be doing a rollback in send email...
      Send_Email( Pricing_Msg_Rec );
   END IF;
-- Fin d'ajout - JSL - 10 septembre 2014
   RAISE;
--}}
END on_DS_Security_Prices;
--
-----------------------------------------------------------------------
--
-- Procedure pour appliquer les CFW sur Security_Prices
--
PROCEDURE On_Security_Prices
    (p_program_id     IN   ops_scripts.program_id%TYPE,
     p_delivery_date  IN   DATE,
     p_supplier_code  IN   DATA_SUPPLIERS.CODE%TYPE,
     p_delivery_time  IN   VARCHAR2,
     p_commit_count   IN   NUMBER,
     p_restart_flag   IN   VARCHAR2,
     p_restart_date   IN   DATE     := NULL,
     p_load_pgm_id    IN   ops_scripts.program_id%TYPE,
     P_FROM_SCREEN    IN   VARCHAR2 := NULL)
IS
--{
  inv_prc_date         exception;
  cannot_restart       EXCEPTION;
  Invalid_zero_par     EXCEPTION;
  p_rows_ins           NUMBER :=0;
  rows_inserted        NUMBER :=0;
  rows_read            NUMBER :=0;
  rows_rejected        NUMBER :=0;
  rows_skip            NUMBER :=0;
  rows_deleted         NUMBER :=0;
  rows_updated         NUMBER :=0;
  Handled_Raise        BOOLEAN := FALSE;
  V_Del_Flag           VARCHAR2(1) := 'N';
  V_Message            VARCHAR2(1000) := 'NULL';
  V_Message_Code       Oasis_Messages.Code%TYPE;
  V_Message_Val1       Varchar2(80);
  --
  -- Nouvelles variables pour la mise a jour de Security_Prices selon
  -- le champs Constant_Prices_Flag, si le champs = 'Y' les dates qui
  -- ne sont pas nuls, seront assigne TRUNC(Delivery_Date)
  --
  temp_origin          Price_Origins.Code%Type;
  temp_updated_flag    yes_no.code%Type;
  temp_price_date      DATE;
  temp_bid_date        DATE;
  temp_ask_date        DATE;
  temp_mid_date        DATE;
  --
  temp_pgm_id           OPS_Scripts.Program_Id%TYPE;
  temp_first_run_date   DATE;
  restart_date          DATE;
  cfw_date              DATE := NULL;
  temp_succ_flag        yes_no.code%TYPE;
  temp_buffer           CHAR(255);
  Price_Origin_Code     Price_Origins.Code%TYPE;
  Intra_Day             BOOLEAN;
  Is_Fri_Open           Holiday_Classes.Code%TYPE;
  --
  -- ajoute une nouvelle variable qui tiendra compte d'ou on est
  -- rendu dans le Cursor pour qu'on puisse fermer le cursor
  -- lorsqu'on fait un commit
  --
  Last_Fri_Ticker      Securities.Fri_Ticker%TYPE := 0;
  --
  char_delivery_time   CHAR(25);
  user_id              user_list.user_id%TYPE := Constants.Get_User_Id;
  restart_status       break_points.status%TYPE;
--
  v_delivery_date        DATE;
  previous_delivery_date DATE;
--
  v_cf_delivery_time     data_supply_schedule.cf_delivery_time%TYPE;
--
  Final_Status           Trading_Status.Code%TYPE;
  At_Par_Flag            Yes_No.Code%TYPE;
  temp_del_Date          DATE;
  Par_Origin             Price_Origins.Code%TYPE:='R';
--
CURSOR Cur_Get_CFW_Date
IS
   SELECT MAX(Delivery_Date)
     FROM Security_Prices
    WHERE Data_Supplier_Code = P_Supplier_Code
      AND Delivery_Date      < P_Restart_Date;
--
-- Ce curseur va chercher les enr de Security_Prices sur lesquels on
-- veut appliquer le CARRY FORWARD
--
-- Omettre VSE et ASE pour pourmettre la conversion a CDNX
-- Richard (25 Nov 1999) - Conversion prevue pour le 28 Nov 1999.
--
-- Join with Securities and trading status to get the
-- additional information required to adjust prices at par
-- for securities just turned inactive
--
CURSOR PRICING_DETAILS (P_Last_Fri_Ticker Securities.Fri_Ticker%TYPE)
IS
      SELECT /*+ ALL_ROWS */
             A.FRI_TICKER,
             A.SOURCE_CODE,
             A.PRICE_CURRENCY,
             A.DELIVERY_DATE,
             A.DATA_SUPPLIER_CODE,
             A.PRICE_DATE,
             A.PRICE,
             A.VOLUME,
             A.HIGH,
             A.LOW,
             A.OPEN_PRICE,
             A.YIELD,
             A.BOARD_LOT_FLAG,
             A.LAST_TRADE_TIME,
             A.BID_DATE,
             A.BID,
             A.BID_YIELD,
             A.ASK_DATE,
             A.ASK,
             A.ASK_YIELD,
             A.MID_DATE,
             A.MID,
             A.MID_YIELD,
             A.NUMBER_OF_TRANS,
             A.USER_KEY_CODE,
             A.LAST_USER,
             A.LAST_STAMP,
             S.Security_Type_Code,
             S.Income_Frequency_Value,
             S.Trading_Status_Code,
             S.Date_Of_Status,
             S.Par_Value,
             S.Par_Currency,
             T.Active_Flag,
             D.Accept_Zero_Prices_Flag,
             A.Dirty_Price,
             A.Price_Mod_Duration,
             A.Bid_Dirty_Price,
             A.Bid_Mod_Duration,
             A.Ask_Dirty_Price,
             A.Ask_Mod_Duration,
             A.Mid_Dirty_Price,
             A.Mid_Mod_Duration,
             A.Accrued_Interest,
             A.Term_in_Years,
             A.Settlement_Date,
             A.Maturity_Date,
             A.Maturity_Type,
             A.Index_Ratio,
             A.Rpb_Factor,
             A.Effective_Date_Factor,
             A.Next_Chg_Date_Factor,
             A.Benchmark_Code,
             A.Spread,
             A.Spread_From_Generic,
             A.Spread_From_Libor,
             A.Convexity,
             A.Pvbp,
             A.Average_Life_Date,
             A.Market_Cap
           , A.IFRS_Code
           , A.High_52w
           , A.Low_52w
           , A.Official_Close
           , A.Trade_Count
           , A.Close_Cumulative_Volume
           , A.Close_Cumulative_Volume_Status
           , A.Close_Cumulative_Volume_Date
        FROM Security_Prices A ,
             SECURITIES      S,
             TRADING_STATUS  T,
             Data_supply_Schedule D
       WHERE A.DATA_SUPPLIER_CODE = p_supplier_code
         AND A.Fri_Ticker         > P_Last_Fri_Ticker
         AND A.DELIVERY_DATE      = PREVIOUS_DELIVERY_DATE
         AND A.SOURCE_CODE NOT IN ('ASE','VSE')
         AND S.Fri_Ticker         = A.Fri_Ticker
         AND S.Trading_Status_Code=T.Code
         AND D.Data_Supplier_Code = A.Data_Supplier_Code
         AND D.Delivery_Time = To_Char(A.Delivery_Date,'HH24MI')
       Order by A.Delivery_Date,
                A.Fri_Ticker,
                A.Source_Code;
--
-- Curseur ajoutes pour verifier l'activite d'une titre et determiner
-- s'il faut oui ou non copier la rangee
--
Pricing_Data Pricing_Details%ROWTYPE;
--
-- Modifie pour que le statut du titre soit verifie avec
-- Date_of_Status <= Last_Price_Date
--
Last_Price_Date DATE := Constants.Get_Site_Date;
--
   CURSOR CUR_TRD_ACT(
       p_fri_ticker securities.fri_ticker%TYPE,
       p_source     sources.code%TYPE,
       p_currency   currencies.currency%TYPE,
       In_Date      DATE
      ) IS
      SELECT A.active_flag
        FROM trading_status A, security_trading_status B
       WHERE B.fri_ticker         = p_fri_ticker
         AND B.source_code        = p_source
         AND B.price_currency     = p_currency
         AND B.date_of_status = (
                   SELECT MAX(Y.date_of_status)
                     FROM security_trading_status Y
                    WHERE Y.fri_ticker      = B.fri_ticker
                      AND Y.source_code     = B.source_code
                      AND Y.price_currency  = B.price_currency
                      AND Y.Date_Of_Status <= In_Date)
         AND B.trading_status_code = A.code;
--
   v_active_flag yes_no.code%TYPE;
   v_last_active_flag yes_no.code%TYPE;
   v_dummy       VARCHAR2(1);
   ok_to_copy    BOOLEAN;
--
   CURSOR CUR_PRICE_PRESENT(
       p_fri_ticker securities.fri_ticker%TYPE,
       p_source     sources.code%TYPE,
       p_date_cour  DATE) IS
      SELECT 'x'
      FROM   Security_Prices
      WHERE  fri_ticker    =  p_fri_ticker
        AND  source_code   != p_source
        AND  delivery_date =  PREVIOUS_DELIVERY_DATE
        AND  price_date    >= p_date_cour;
--
-- Modifie pour permettre des Restart meme s'il existe des rangees
-- pour le meme fournisseur, tant que le CF_Delivery_Time de ces
-- rangees ne pointe pas sur les rangees sur lesquelles le Restart se
-- fera
--
CURSOR Cur_Rows_Before_Restart (
   P_Restart_Date       DATE,
   P_Data_Supplier_Code Data_Suppliers.Code%TYPE,
   P_Del_Time           Data_Supply_Schedule.Delivery_Time%TYPE)
IS
-- Modification - JSL - 19 novembre 2015
--   SELECT DISTINCT 'X'
--     FROM Security_Prices SP ,
--          Data_Supply_Schedule DSS
--    WHERE SP.Delivery_Date        >  P_Restart_Date
--      AND SP.Data_Supplier_Code   =  P_Data_Supplier_Code
--      AND DSS.Data_Supplier_Code  =  SP.Data_Supplier_Code
--      AND DSS.Delivery_Time       =  to_char(SP.delivery_date,'hh24mi')
--      AND DSS.Delivery_Time       <> P_Del_Time
--      AND DSS.CF_Delivery_Time    =  P_Del_Time
--   UNION
--   SELECT DISTINCT 'X'
--     FROM Security_Prices SP ,
--          Data_Supply_Schedule DSS
--    WHERE SP.Delivery_Date       >  (P_Restart_Date+1)
--      AND SP.Data_Supplier_Code  =  P_Data_Supplier_Code
--      AND DSS.Data_Supplier_Code =  SP.Data_Supplier_Code
--      AND DSS.Delivery_Time      =  to_char(SP.delivery_date,'hh24mi')
--      AND DSS.CF_Delivery_Time   =  P_Del_Time;
  SELECT 'X'
    FROM DUAL
   WHERE EXISTS (
         SELECT 'X'
           FROM Security_Prices SP ,
                Data_Supply_Schedule DSS
          WHERE SP.Delivery_Date        >  P_Restart_Date
            AND SP.Data_Supplier_Code   =  P_Data_Supplier_Code
            AND DSS.Data_Supplier_Code  =  SP.Data_Supplier_Code
            AND DSS.Delivery_Time       =  to_char(SP.delivery_date,'hh24mi')
            AND DSS.Delivery_Time       <> P_Del_Time
            AND DSS.CF_Delivery_Time    =  P_Del_Time
         )
      OR EXISTS (
         SELECT 'X'
           FROM Security_Prices SP ,
                Data_Supply_Schedule DSS
          WHERE SP.Delivery_Date       >  (P_Restart_Date+1)
            AND SP.Data_Supplier_Code  =  P_Data_Supplier_Code
            AND DSS.Data_Supplier_Code =  SP.Data_Supplier_Code
            AND DSS.Delivery_Time      =  to_char(SP.delivery_date,'hh24mi')
            AND DSS.CF_Delivery_Time   =  P_Del_Time
         );
-- Fin de modification - JSL - 19 novembre 2015
--
-- Not Run Yet Flag cannot be looked at
--
-- The Successfull_Flag = Y must be the one to be fetched
-- This is the only known success in Oasis
--
CURSOR Cur_Previous_CFW_Done
IS
   SELECT PM.Successfull_Flag
     FROM Pricing_Msgs      PM
    WHERE PM.Program_Id       = Temp_Pgm_Id
      AND PM.Date_of_Prices   = TRUNC(Previous_Delivery_Date)
      AND PM.Date_Of_Msg      =
          (SELECT MAX(PM2.Date_of_Msg)
           FROM  Pricing_Msgs      PM2,
                 Successfull_Flags SF2
           WHERE PM2.Program_id       = PM.Program_id
            AND  PM2.Date_of_Prices   = PM.Date_of_Prices
            AND  PM2.Successfull_Flag = SF2.Code
            AND  SF2.Not_Run_Yet_Flag = 'N' );
--
-- Nouveau cursor pour aller chercher le champs Constant_Prices_Flag
-- de Securities
--
CURSOR Get_Constant_Prices_Flag
   (P_Fri_Ticker Securities.Fri_Ticker%TYPE)
IS
   SELECT S.Constant_Prices_Flag
     FROM Securities S
    WHERE S.Fri_Ticker = P_Fri_Ticker;
--
-- New cursor to get the information from security_patterns
--
CURSOR Get_Sec_Pattern (Sec_Type    Security_Types.Code%TYPE,
                        Freq        Frequencies.Value%TYPE)
IS
   SELECT Final_Trading_Status_Code,
          Last_Price_At_Par_Flag
   From   Security_Patterns
   Where  Code = Security.Get_Sec_Type_Pattern
                    (Sec_Type,
                     Freq);
--
  END_OF_LOOP          BOOLEAN:=FALSE;
  Constant_Prices_Flag Yes_No.Code%TYPE;
--
  Temp_Field           BOOLEAN:=FALSE;
--
   no_data         EXCEPTION;
   Bad_Previous_Cf EXCEPTION;
--
BEGIN
--{
-- Start of Job
--
   IF P_Supplier_Code = C_Dual_Supplier_Code
   THEN
      BEGIN
         On_DS_Security_Prices( p_program_id
                              , p_delivery_date
                              , p_supplier_code
                              , p_delivery_time
                              , p_commit_count
                              , p_restart_flag
                              , p_restart_date
                              , p_load_pgm_id
                              , P_FROM_SCREEN
                              );
      EXCEPTION
      WHEN OTHERS THEN
        Handled_Raise := TRUE;
        RAISE;
      END;
      GOTO Fin_de_Programme;
   END IF;
   User_Id       := Constants.Get_User_Id;
   G_Program_Id  := P_Program_Id;
   G_System_Date := SYSDATE;
   --
   Global_File_Updates.Initialize_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   Pricing_Msg_Rec.Msg_Text :=
     'CFW on Security Prices for:'|| p_supplier_code || ', ' ||
      to_char(p_delivery_date,'dd-mon-yyyy') || ' ' || p_delivery_time;
   --
   Pricing_Msg_Rec.Msg_Text_2       := 'Start of Program';
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   --
   IF P_Restart_Flag = 'Y'
   THEN
      Pricing_Msg_Rec.Date_of_Prices := P_Restart_Date;
   END IF;
   --
   Global_File_Updates.Update_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   pgmloc := 610;
   --
   COMMIT;
--
-- Aller chercher le dernier status
--
   IF p_load_pgm_id IS NOT NULL THEN
      pgmloc := 620;
      SELECT status
      INTO   restart_status
      FROM   break_points
      WHERE  program_id = p_load_pgm_id;
   ELSE
      restart_status := NULL;
   END IF;
--
-- Validation des parametres Data_supplier et Delivery time
-- Si ce n'est pas valide, l'exception NO_DATA_FOUND est levee
-- automatiquement
--
   pgmloc := 630;
   SELECT cf_delivery_time
   INTO v_cf_delivery_time
   FROM data_supply_schedule
   WHERE data_supplier_code = p_supplier_code
     AND delivery_time = p_delivery_time;
--
   pgmloc := 640;
   IF Constants.G_Site_Date <> TRUNC(p_delivery_date) THEN
      Pricing_Msg_Rec.Msg_Text_2 := 'Error while validating parameters';
      Pricing_Msg_Rec.Err_Text   :=
         'Invalid price date parameter. Must be equal to New Day.';
      RAISE inv_prc_date;
   END IF;
--
-- On set la delivery_date
--
   v_delivery_date :=
      to_date(to_char(p_delivery_date,'dd-mon-yyyy') || ' ' ||
      p_delivery_time,'DD-MON-YYYY HH24MI');
   --
   -- Nous devons verifier si les prix sont des Intra-Day
   -- pour assigner le Price_Origin_Code
   -- Marie (23 avril 1997)
   -- Req: 3857
   Intra_Day := Supplier.Is_It_Intra_Day(P_Supplier_Code,
                                         P_Delivery_Time);
   --
   IF Intra_Day
   THEN
      Price_Origin_Code := 'I';
   ELSE
      Price_Origin_Code := 'C';
   END IF;
   --
   Is_Fri_Open := Holiday.Is_It_Open( p_delivery_date, 'HOLCLASS', null, 'FRI' );
-- Ajout - JSL - 14 juillet 2015
-- Est-ce que l'on doit accepter un drp?
   Unchain_Flag := 'N';
   Pgmloc := 642;
   OPEN  Get_DRP_Info( P_Supplier_Code );
   Pgmloc := 643;
   FETCH Get_DRP_Info INTO Unchain_Flag;
   Pgmloc := 644;
   CLOSE Get_DRP_Info;
-- Fin d'ajout - JSL - 14 juillet 2015
-- Is it open?
-- -- This is depending on the load!
   --
   pgmloc := 650;
   --
   -- modifie pour utiliser le previous_price_date de Site_Controls
   -- au lieu de faire un max
   --
   IF P_Restart_Flag = 'N'
   THEN
      Restart_Date:= v_delivery_date;
      --
-- Ajout - JSL - 14 juillet 2015
      IF  Unchain_Flag = 'Y'
      AND ( Holiday.Is_It_Open( v_delivery_date, 'SUPPLIER', P_Supplier_Code, P_Delivery_Time ) IS NOT NULL
         OR Is_Fri_Open IS NOT NULL
          )
      THEN
         Previous_Delivery_Date :=
                  to_date(to_char(Constants.G_Previous_Site_Date,'DD-MON-YYYY') ||
                    ' ' || P_Delivery_Time,'DD-MON-YYYY HH24MI');
-- Fin d'ajout - JSL - 14 juillet 2015
      ELSIF V_CF_Delivery_Time < P_Delivery_Time
      THEN
-- Ajout - JSL - 07 janvier 2014
         IF Is_Fri_Open IS NOT NULL
         THEN
            -- Montreal office is closed
            IF  Intra_Day
            THEN
               Previous_Delivery_Date :=
                  to_date(to_char(Constants.G_Site_Date,'DD-MON-YYYY') ||
                    ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
            ELSE
               Previous_Delivery_Date :=
                  to_date(to_char(Constants.G_Previous_Site_Date,'DD-MON-YYYY') ||
                    ' ' || P_Delivery_Time,'DD-MON-YYYY HH24MI');
            END IF;
         ELSE
-- Fin d'ajout - JSL - 07 janvier 2014
            Previous_Delivery_Date :=
               to_date(to_char(Constants.G_Site_Date,'DD-MON-YYYY') ||
                 ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
-- Ajout - JSL - 07 janvier 2014
         END IF;
-- Fin d'ajout - JSL - 07 janvier 2014
      ELSE
         Previous_Delivery_Date :=
            to_date(to_char(Constants.G_Previous_Site_Date,'DD-MON-YYYY') ||
              ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      END IF;
      --
   ELSE
      --
      IF P_Restart_Date = Constants.G_Site_Date
      THEN
         CFW_Date := Constants.G_Previous_Site_Date;
      ELSE
         CFW_Date := Holiday.Get_Last_Open_Date(
            Constants.G_Previous_Site_Date,'HOLCLASS',NULL,'STP');
      END IF;
      --
      Restart_Date :=
        TO_DATE(TO_CHAR(P_Restart_Date,'DD-MON-YYYY')  || ' ' ||
        P_Delivery_Time,'DD-MON-YYYY HH24MI');
      --
-- Ajout - JSL - 14 juillet 2015
      IF  Unchain_Flag = 'Y'
      AND ( Holiday.Is_It_Open( v_delivery_date, 'SUPPLIER', P_Supplier_Code, P_Delivery_Time ) IS NOT NULL
         OR Is_Fri_Open IS NOT NULL
          )
      THEN
         Previous_Delivery_Date :=
                  to_date(to_char(CFW_Date,'DD-MON-YYYY') ||
                    ' ' || P_Delivery_Time,'DD-MON-YYYY HH24MI');
-- Fin d'ajout - JSL - 14 juillet 2015
      ELSIF V_CF_Delivery_Time < P_Delivery_Time
      THEN
         IF Is_Fri_Open IS NOT NULL
         THEN
            -- Montreal office is closed
            IF  Intra_Day
            THEN
               Previous_Delivery_Date :=
                  to_date(to_char(P_Restart_Date,'DD-MON-YYYY') ||
                   ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
            ELSE
               Previous_Delivery_Date :=
                  to_date(to_char(CFW_Date,'DD-MON-YYYY') ||
                   ' ' || P_Delivery_Time,'DD-MON-YYYY HH24MI');
            END IF;
         ELSE
-- Fin d'ajout - JSL - 07 janvier 2014
            Previous_Delivery_Date :=
               to_date(to_char(P_Restart_Date,'DD-MON-YYYY') ||
                ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
-- Ajout - JSL - 07 janvier 2014
         END IF;
-- Fin d'ajout - JSL - 07 janvier 2014
      ELSE
         Previous_Delivery_Date :=
            to_date(to_char(CFW_Date,'DD-MON-YYYY') ||
             ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
      END IF;
   END IF;
   --
   -- Nous devons maintenant s'assurer que le CFWD de la date
   -- Previous_Delivery_Date a ete execute
   --
   SELECT SRP.Program_Id , SRP.First_Run_Date
     INTO Temp_Pgm_Id, Temp_First_Run_date
     FROM Supplier_Running_Parms SRP
    WHERE SRP.Data_Supplier_Code = P_Supplier_Code
      AND SRP.Delivery_Time      = V_CF_Delivery_Time
      AND SRP.Product_Code       = 'CFW';
   --
   IF Temp_First_Run_Date != P_Delivery_Date THEN
      OPEN Cur_Previous_CFW_Done;
      FETCH Cur_Previous_CFW_Done INTO Temp_Succ_Flag;
      IF Cur_Previous_CFW_Done%FOUND
      THEN
         CLOSE Cur_Previous_CFW_Done;
         IF Temp_Succ_Flag != 'Y'
         THEN
            Raise Bad_Previous_cf;
         END IF;
      ELSE
         CLOSE Cur_Previous_CFW_Done;
         Raise Bad_Previous_cf;
      END IF;
   END IF;
--
   pgmloc := 660;
-- Ajout - JSL - 14 juillet 2015
   IF  Unchain_Flag = 'Y'
   THEN
      NULL;
-- Fin d'ajout - JSL - 14 juillet 2014
-- Ajout - JSL - 07 janvier 2014
   ELSIF Intra_Day
   AND Is_Fri_Open IS NOT NULL
   THEN
      Pricing_Msg_Rec.Remarks := 'Intraday and we are closed';
      GOTO Carry_Forward_Done;
   END IF;
-- Fin d'ajout - JSL - 07 janvier 2014
--
-- Si c'est un restart, on enleve tous les enr. pour le supplier et
-- delivery time
--
   pgmloc := 670;
   IF p_restart_flag = 'Y' OR restart_status = 'FRESTART'
   THEN
      IF p_restart_flag = 'Y'
      THEN
         --
         -- Nous devons s'assurer qu'il n'y a plus de rangee
         -- dans Security Prices plus recente que le restart date
         -- req: 3635
         -- Marie (31dec1996)
         --
         OPEN Cur_Rows_Before_Restart(Restart_Date,
                                      P_Supplier_Code,
                                      P_Delivery_Time);
         FETCH Cur_Rows_Before_Restart INTO Temp_Buffer;
         IF Cur_Rows_Before_Restart%FOUND
         THEN
            CLOSE Cur_Rows_Before_Restart;
            Pricing_Msg_Rec.Err_Text:=
              'Cannot Restart Carry Fwd, Prices exist past restart date';
            RAISE Cannot_Restart;
         END IF;
         CLOSE Cur_Rows_Before_Restart;
      END IF;
      --
      WHILE NOT (end_of_loop) LOOP
         DELETE FROM Security_Prices
         WHERE delivery_date = restart_date
         AND data_supplier_code = p_supplier_code
         AND rownum <= p_commit_count;
--
         IF SQL%ROWCOUNT < p_commit_count THEN
            end_of_loop := TRUE;
         END IF;
         Rows_Deleted := Rows_Deleted + SQL%ROWCOUNT;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Global_File_Updates.Update_Pricing_Msgs( Pricing_Msg_Rec.Program_Id
                                                , Pricing_Msg_Rec.Date_of_Msg
                                                , FALSE
                                                , Pricing_Msg_Rec
                                                );
         COMMIT;
      END LOOP;
   END IF;
   --
   -- Fetch Options and apply Carry Forward - Using new Price Date
   --
   -- Remplace le FOR LOOP pour un LOOP pour pouvoir fermer et
   -- re-ouvrir le Cursor l'hors d'un commit
   --
   OPEN Pricing_Details(Last_Fri_Ticker);
   LOOP
      --
      FETCH Pricing_Details INTO Pricing_Data;
      IF Pricing_Details%NOTFOUND
      THEN
         EXIT;
      END IF;
      pgmloc := 680;
      --
      -- is the security to be processed??
      --for now: discard MF for STP2000 and non-MF for MF2000
      --no filter for others
      if official_prices.process_security(
           p_supplier_code, p_delivery_time, Pricing_Data.security_type_code
         ) = false
      then
         rows_skip := rows_skip + 1;
         GOTO End_Of_Loop;
      end if;
      --
      --
      -- on verifie si un commit est necessaire
      --
      IF  P_Rows_Ins >= P_Commit_Count
      AND Pricing_Data.Fri_Ticker <> Last_Fri_Ticker
      THEN
         pgmloc := 690;
         CLOSE Pricing_Details;
         -- nous mettons a jour les table Pricing_Msgs et Break Points
         --
         Pricing_Msg_Rec.Value_01 :=  Rows_Read;
         Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
         Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
         Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Pricing_Msg_Rec.Msg_Text_2 :=
            'Processing security prices records';
         --
         Global_File_Updates.Update_Pricing_Msgs
            (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
         COMMIT;
         --
         p_rows_ins := 0;
         --
         COMMIT;
         pgmloc := 700;
         OPEN Pricing_Details(Last_Fri_Ticker);
         --
         -- La rangee doit etre refetchee (Si non, 1 rangee dans les
         -- Rejected a chaque batch
         FETCH Pricing_Details INTO Pricing_Data;
         IF Pricing_Details%NOTFOUND
         THEN
            pgmloc := 710;
            EXIT;
         END IF;
      END IF;
      rows_read := rows_read + 1;
      --
      -- On verifie la condition pour copier
      --
      ok_to_copy := FALSE;
--
      v_active_Flag := Pricing_data.Active_Flag;
--
      IF v_active_flag = 'N' THEN
         pgmloc := 730;
         ok_to_copy := TRUE;
      ELSE
         pgmloc := 740;
         OPEN CUR_TRD_ACT(pricing_data.fri_ticker,
                          pricing_data.source_code,
                          pricing_data.price_currency,
                          Last_Price_Date);
         FETCH CUR_TRD_ACT INTO v_active_flag;
         IF CUR_TRD_ACT%NOTFOUND THEN
            CLOSE CUR_TRD_ACT;
            V_Message_Code := 'PROC-CARRY_FWD-010';
            V_Message_Val1 := Security.Get_Fri_Id(Pricing_Data.Fri_Ticker);
            Manage_Messages.Get_Oasis_Message
                        (V_Message_Code,
                         V_Del_Flag,
                         V_Message_Val1
                  ||' '||pricing_data.source_code
                  ||' '||pricing_data.price_currency,
                         V_Message);
            Pricing_Msg_Rec.Err_Text := V_Message;
            RAISE no_data_found;
         END IF;
         CLOSE CUR_TRD_ACT;
         IF v_active_flag = 'N' THEN
            pgmloc := 750;
            ok_to_copy := TRUE;
         ELSE
            pgmloc := 760;
            ok_to_copy := TRUE;
         END IF;
      END IF;
--
      IF ok_to_copy THEN
         pgmloc := 770;
         pricing_data.delivery_date :=  restart_date;
         -- Pour sauvegarder le code INACTV, cet enonce ne doit pas
         -- etre executer.
         -- Principalement utilser par les pgm de calculs.
         -- A partir du 2ieme dans la chaine, si un titre est arrive
         -- a echeance lors d'un conge, le code INACTV se perdait.
         --
         -- Ajoute nouveau champs Price_Origin_Code qui sera mis
         -- a 'C' ou 'IC' pour indiquer que cette rangee de prix a ete
         -- creee par le program de Carry Forward
         --
         -- Nous allons chercher le Constant_Prices_Flag
         --
         OPEN Get_Constant_Prices_Flag(Pricing_Data.Fri_Ticker);
         FETCH Get_Constant_Prices_Flag INTO Constant_Prices_Flag;
         CLOSE Get_Constant_Prices_Flag;
         --
         temp_Origin       := Price_Origin_Code;
         temp_Updated_Flag := 'N';
         temp_field        := FALSE;
         --
-- Initialise the fields to the previous day values
-- The constant price may set them to current date.
         Temp_Price_Date := Pricing_Data.Price_Date;
         Temp_Bid_Date   := Pricing_Data.Bid_Date;
         Temp_Mid_Date   := Pricing_Data.Mid_Date;
         Temp_Ask_Date   := Pricing_Data.Ask_Date;
         --
         IF Constant_Prices_Flag = 'Y' AND
            V_Active_Flag        = 'Y'
         THEN
            temp_Origin := 'F';
            --
            -- Checking if the source and the supplier are open.
            --
            IF  (Holiday.Is_it_Open(P_Delivery_Date,
                                  'SOURCE',
                                   Pricing_Data.Source_Code,
                                   NULL) IS NOT NULL)
            OR
                (Holiday.Is_it_Open
                   (P_Delivery_Date,
                   'SUPPLIER',
                    Pricing_Data.Data_Supplier_Code,
                    TO_CHAR(Pricing_Data.Delivery_Date,'HH24MI'))
                    IS NOT NULL)
            THEN
               Temp_Field := TRUE;
               GOTO SKIP;
            END IF;
            --
            temp_Updated_Flag := 'Y';
            --
            IF Pricing_Data.Price_Date IS NOT NULL
            THEN
               Temp_Price_Date := TRUNC(Pricing_Data.Delivery_Date);
            END IF;
            --
            IF Pricing_Data.Bid_Date IS NOT NULL
            THEN
               Temp_Bid_Date := TRUNC(Pricing_Data.Delivery_Date);
            END IF;
            --
            IF Pricing_Data.Ask_Date IS NOT NULL
            THEN
               Temp_Ask_Date := TRUNC(Pricing_Data.Delivery_Date);
            END IF;
            --
            IF Pricing_Data.Mid_Date IS NOT NULL
            THEN
               Temp_Mid_Date := TRUNC(Pricing_Data.Delivery_Date);
            END IF;
            --
            <<SKIP>>
            NULL;
         ELSE
            Temp_Field := TRUE;
         END IF;
         --
         --
         -- If we had a week-end or FRI holiday between previous site
         -- date and current site date
         -- Process of inactive securities turned inactive during that
         -- period
         -- Insert new price record with Price equal to Par and
         -- Price date = Maturity_date where applicable
         --
         pgmloc := 780;
         IF Pricing_Data.Active_Flag = 'N' AND
            Pricing_Data.Price_Currency = Pricing_Data.Par_Currency
         THEN    -- {
         --
         -- Week-end or FRI closed days
         --
            pgmloc := 790;
            IF  TRUNC(Restart_Date)-TRUNC(Previous_Delivery_Date)>1 AND
                 Pricing_Data.Date_Of_Status BETWEEN
                           TRUNC(Previous_Delivery_Date+1) AND
                           TRUNC(Restart_Date-1)
            THEN   -- {
               pgmloc := 800;
       --      Check if the security was active on source on previous
       --      site date     (SSI - 01NOV2002)
               OPEN CUR_TRD_ACT(pricing_data.fri_ticker,
                                pricing_data.source_code,
                                pricing_data.price_currency,
                                TRUNC(Previous_Delivery_Date));
               FETCH CUR_TRD_ACT INTO v_last_active_flag;
               IF CUR_TRD_ACT%NOTFOUND
               THEN   -- {
                  pgmloc := 810;
                  CLOSE CUR_TRD_ACT;
                  V_Message_Code := 'PROC-CARRY_FWD-010';
                  V_Message_Val1 := Security.Get_Fri_Id(Pricing_Data.Fri_Ticker);
                  Manage_Messages.Get_Oasis_Message
                              (V_Message_Code,
                               V_Del_Flag,
                               V_Message_Val1,
                               V_Message);
                  Pricing_Msg_Rec.Err_Text := V_Message;
                  RAISE no_data_found;
               END IF;   --}
               CLOSE CUR_TRD_ACT;
               -- If it was active and just turned inactive during the
               -- week end or FRI holiday, then try to add the last
               -- price with par value if applicable
               IF V_Last_Active_Flag = 'Y'
               THEN   --{
                  pgmloc := 820;
                  OPEN Get_Sec_Pattern(Pricing_Data.Security_Type_Code,
                                       Pricing_Data.Income_Frequency_Value);
                  FETCH Get_Sec_Pattern INTO Final_Status,
                                             At_Par_Flag;
                  CLOSE Get_Sec_Pattern;
                  Pgmloc := 822;
                  IF  At_Par_Flag = 'Y'
                  AND Final_Status = Pricing_Data.Trading_status_Code
                  THEN --{
                    IF Pricing_Data.Par_Value = 0
                    THEN
                      NULL;
                    ELSE
                      IF NVL( Pricing_Data.Price, Pricing_Data.Bid )
                         / Pricing_Data.Par_Value
                         BETWEEN Min_Ratio AND Max_Ratio
                      THEN
                        NULL;
                      ELSIF NVL( Pricing_Data.Price, Pricing_Data.Bid )
                         / 100.0
                         BETWEEN Min_Ratio AND Max_Ratio
                      THEN
                        Pricing_Data.Par_Value := 100.0;
                      ELSE
                        -- Par value is too far from price to set it.
                        At_Par_Flag := 'N';
                      END IF;
                    END IF;
                  END IF; --}
                  pgmloc :=830;
                  IF At_Par_Flag = 'Y' AND
                     Final_Status = Pricing_Data.Trading_status_Code
                  THEN    -- {
                     BEGIN
                     -- if par value is 0 and this supplier cannot accept 0
                     -- prices, then abort
                        pgmloc :=840;
                        IF  Pricing_Data.Accept_Zero_Prices_Flag='N' AND
                            Pricing_Data.Par_Value = 0
                        THEN
                           RAISE Invalid_Zero_par;
                        END IF;
--
-- Set new values in Security_Prices
--
                        pgmloc :=850;
                        Pricing_Data.Delivery_Date :=
                            TO_DATE(TO_CHAR(Pricing_Data.Date_Of_Status,
                                            'DDMONYYYY') || ' ' ||
                                            P_Delivery_Time,
                                            'DDMONYYYY HH24MI');
                        temp_price_date     := Pricing_Data.Date_Of_Status;
                        pricing_data.price  := Pricing_Data.Par_Value;
                        pricing_data.volume           := NULL;
                        pricing_data.high             := NULL;
                        pricing_data.low              := NULL;
                        pricing_data.open_price       := NULL;
                        pricing_data.yield            := NULL;
                        pricing_data.board_lot_flag   := NULL;
                        pricing_data.last_trade_time  := NULL;
                        pricing_data.number_of_trans  := NULL;
                        pricing_data.Market_Cap       := NULL;
                        IF Pricing_Data.Dirty_Price IS NOT NULL
                        THEN
                          Pricing_Data.Dirty_Price := Pricing_Data.Par_Value;
                          Pricing_Data.Accrued_Interest   := 0;
                          Pricing_Data.Price_Mod_Duration := 0;
                          Pricing_Data.Term_in_Years      := 0;
                          Pricing_Data.Maturity_Date
                            := TO_CHAR( Pricing_Data.Date_of_Status
                                      , 'DD-MON-YYYY'
                                      );
                        END IF;
                        IF Pricing_Data.Bid IS NOT NULL
                        THEN
                          temp_bid_date          := Pricing_Data.Date_Of_Status;
                          pricing_data.bid       := Pricing_Data.Par_Value;
                          pricing_data.bid_yield := NULL;
                        END IF;
                        IF Pricing_Data.Bid_Dirty_Price IS NOT NULL
                        THEN
                          Pricing_Data.Bid_Dirty_Price := Pricing_Data.Par_Value;
                          Pricing_Data.Bid_Mod_Duration := 0;
                        END IF;
                        IF Pricing_Data.Ask IS NOT NULL
                        THEN
                          temp_ask_date          := Pricing_Data.Date_Of_Status;
                          pricing_data.ask       := Pricing_Data.Par_Value;
                          pricing_data.ask_yield := NULL;
                        END IF;
                        IF Pricing_Data.Ask_Dirty_Price IS NOT NULL
                        THEN
                          Pricing_Data.Ask_Dirty_Price := Pricing_Data.Par_Value;
                          Pricing_Data.Ask_Mod_Duration := 0;
                        END IF;
                        IF Pricing_Data.Mid IS NOT NULL
                        THEN
                          temp_mid_date          := Pricing_Data.Date_Of_Status;
                          pricing_data.mid       := Pricing_Data.Par_Value;
                          pricing_data.mid_yield := NULL;
                        END IF;
                        IF Pricing_Data.Mid_Dirty_Price IS NOT NULL
                        THEN
                          Pricing_Data.Mid_Dirty_Price := Pricing_Data.Par_Value;
                          Pricing_Data.Mid_Mod_Duration := 0;
                        END IF;
--
--                  INSERT NEW pricing record at par
--
                        pgmloc :=860;
                        INSERT INTO Security_Prices (fri_ticker, source_code,
                           price_currency,delivery_date,data_supplier_code,
                           price_date,price,volume,high,low,open_price,
                           yield,board_lot_flag,last_trade_time,
                           bid_date,bid,bid_yield,ask_date,ask,ask_yield,
                           mid_date,mid,mid_yield,number_of_trans,
                           user_key_code,updated_flag,price_origin_code,
                           last_user,last_stamp,
                           dirty_price,price_mod_duration,bid_dirty_price,
                           bid_mod_duration,ask_dirty_price,ask_mod_duration,
                           mid_dirty_price,mid_mod_duration,accrued_interest,
                           term_in_years,settlement_date,maturity_date,
                           maturity_type,index_ratio,rpb_factor,
                           effective_date_factor,next_chg_date_factor,
                           benchmark_code,spread,spread_from_generic,
                           spread_from_libor,convexity,pvbp,average_life_date,
                           Market_Cap
                         , IFRS_Code
                         , High_52w
                         , Low_52w
                         , Official_Close
                         , Trade_Count
                         , Close_Cumulative_Volume
                         , Close_Cumulative_Volume_Status
                         , Close_Cumulative_Volume_Date
                           )
                        SELECT pricing_data.fri_ticker,
                               pricing_data.source_code,
                               pricing_data.price_currency,
                               Pricing_Data.Delivery_Date,
                               pricing_data.data_supplier_code,
                               temp_Price_Date,
                               Pricing_Data.Price,
                               pricing_data.volume,
                               pricing_data.high,
                               pricing_data.low,
                               pricing_data.open_price,
                               pricing_data.yield,
                               pricing_data.board_lot_flag,
                               pricing_data.last_trade_time,
                               temp_bid_date,
                               pricing_data.bid,
                               pricing_data.bid_yield,
                               temp_ask_date,
                               pricing_data.ask,
                               pricing_data.ask_yield,
                               temp_mid_date,
                               pricing_data.mid,
                               pricing_data.mid_yield,
                               pricing_data.Number_Of_Trans,
                               Pricing_Data.User_Key_Code,
                               'Y',
                               Par_Origin,
                               user_id,
                               SYSDATE,
                               Pricing_Data.dirty_price,
                               Pricing_Data.price_mod_duration,
                               Pricing_Data.bid_dirty_price,
                               Pricing_Data.bid_mod_duration,
                               Pricing_Data.ask_dirty_price,
                               Pricing_Data.ask_mod_duration,
                               Pricing_Data.mid_dirty_price,
                               Pricing_Data.mid_mod_duration,
                               Pricing_Data.accrued_interest,
                               Pricing_Data.term_in_years,
                               Pricing_Data.settlement_date,
                               Pricing_Data.maturity_date,
                               Pricing_Data.maturity_type,
                               Pricing_Data.index_ratio,
                               Pricing_Data.rpb_factor,
                               Pricing_Data.effective_date_factor,
                               Pricing_Data.next_chg_date_factor,
                               Pricing_Data.benchmark_code,
                               Pricing_Data.spread,
                               Pricing_Data.spread_from_generic,
                               Pricing_Data.spread_from_libor,
                               Pricing_Data.convexity,
                               Pricing_Data.pvbp,
                               Pricing_Data.average_life_date,
                               Pricing_Data.Market_Cap
                             , Pricing_Data.IFRS_Code
                             , Pricing_Data.High_52w
                             , Pricing_Data.Low_52w
                             , Pricing_Data.Official_Close
                             , Pricing_Data.Trade_Count
                             , Pricing_Data.Close_Cumulative_Volume
                             , Pricing_Data.Close_Cumulative_Volume_Status
                             , Pricing_Data.Close_Cumulative_Volume_Date
                          FROM dual;
                        rows_inserted := rows_inserted + SQL%ROWCOUNT;
                        p_rows_ins    := p_rows_ins    + SQL%ROWCOUNT;
                     EXCEPTION
                     -- IF already created, either manually or from last run
                     -- in case of a restart
                     WHEN DUP_VAL_ON_INDEX THEN
                        pgmloc :=870;
                        UPDATE Security_Prices Set
                            Price_Date      = Temp_Price_Date,
                            Price           = Pricing_Data.Price,
                            Volume          = Pricing_Data.Volume,
                            High            = Pricing_Data.High,
                            Low             = Pricing_Data.Low,
                            Open_Price      = Pricing_Data.Open_Price,
                            Yield           = Pricing_Data.Yield,
                            Board_Lot_Flag  = Pricing_Data.Board_Lot_Flag,
                            Last_Trade_Time = Pricing_Data.Last_Trade_Time,
                            Number_of_Trans = Pricing_Data.Number_of_Trans,
                            User_Key_Code   = Pricing_Data.User_Key_Code,
                            Updated_Flag    = 'Y',
                            Price_Origin_Code = Par_Origin,
                            Last_User         = user_id,
                            Last_Stamp        = SYSDATE,
                            Accrued_Interest  = Pricing_Data.Accrued_Interest,
                            Maturity_Date     = Pricing_Data.Maturity_Date,
                            Term_in_Years     = Pricing_Data.Term_in_Years,
                            Dirty_Price       = Pricing_Data.Dirty_Price,
                            Price_Mod_Duration= Pricing_Data.Price_Mod_Duration,
                            Market_Cap        = Pricing_Data.Market_Cap
                          , IFRS_Code         = Pricing_Data.IFRS_Code
                          , High_52w          = Pricing_Data.High_52w
                          , Low_52w           = Pricing_Data.Low_52w
                          , Official_Close    = Pricing_Data.Official_Close
                          , Trade_Count       = Pricing_Data.Trade_Count
                          , Close_Cumulative_Volume
                                              = Pricing_Data.Close_Cumulative_Volume
                          , Close_Cumulative_Volume_Status
                                              = Pricing_Data.Close_Cumulative_Volume_Status
                          , Close_Cumulative_Volume_Date
                                              = Pricing_Data.Close_Cumulative_Volume_Date
                        WHERE Fri_Ticker     = Pricing_Data.Fri_Ticker
                        AND   Data_Supplier_Code = P_Supplier_Code
                        AND   Source_Code    = Pricing_Data.Source_Code
                        AND   delivery_Date  = Pricing_Data.Delivery_Date
                        AND   Price_Currency = Pricing_Data.Price_Currency;
                        rows_Updated := rows_Updated + SQL%ROWCOUNT;
                        Handled_Raise := TRUE;
                     WHEN Invalid_Zero_Par THEN
                        Pricing_Msg_Rec.Err_Text    :=
                          To_Char(Pricing_Data.Fri_Ticker) ||
                          ' Creation of CFW for matured issues - ' ||
                          'Zero par value cannot be inserted in ' ||
                          'Security_Prices for supplier ' ||
                        Pricing_Data.Data_Supplier_Code || ' ' ||
                        P_Delivery_Time;
                        Pricing_Msg_Rec.Successfull_Flag := 'N';
                        Pricing_Msg_Rec.Value_01 :=  Rows_Read;
                        Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
                        Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
                        Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
                        Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
                        Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
                        --
                        Global_File_Updates.Update_Pricing_Msgs
                          (G_Program_Id, G_System_Date, TRUE,
                           Pricing_Msg_Rec);
                        --
                        COMMIT;
                        Handled_Raise := TRUE;
                        RAISE;
                     WHEN OTHERS THEN
                        IF NOT(Handled_Raise)
                        THEN
                           -- Err_Text is 255 characters long (not 68)
                           Pricing_Msg_Rec.Err_Text    :=
                              substr(SQLERRM(SQLCODE),1,255);
                           Pricing_Msg_Rec.Msg_Text_2  :=
                             'Unhandled exception occurred at pgmloc:'||
                             to_char(pgmloc);
                           Pricing_Msg_Rec.Successfull_Flag := 'N';
                           Pricing_Msg_Rec.Value_01 :=  Rows_Read;
                           Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
                           Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
                           Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
                           Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
                           Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
                           --
                           Global_File_Updates.Update_Pricing_Msgs
                             (G_Program_Id, G_System_Date, TRUE,
                              Pricing_Msg_Rec);
                        END IF;
                        --
                        COMMIT;
-- Ajout - JSL - 10 septembre 2014
                        IF NOT(Handled_Raise)
                        THEN
                           -- We may be doing a rollback in send email...
                           Send_Email( Pricing_Msg_Rec );
                        END IF;
-- Fin d'ajout - JSL - 10 septembre 2014
                        RAISE;
                     END;   -- End block
                  END IF;   --}  Insert at par price
               END IF;   --}  If was active on previous price date
            END IF;  -- } Case II - Week-end or FRI closed in between
         END IF;   -- } Process of inactive securities
--
         pgmloc :=880;
         Pricing_Data.Delivery_Date :=
               TO_DATE(TO_CHAR(Restart_Date,
                               'DDMONYYYY') || ' ' ||
                               P_Delivery_Time,
                               'DDMONYYYY HH24MI');
         pgmloc :=890;
         INSERT INTO Security_Prices (fri_ticker, source_code,
            price_currency,delivery_date,data_supplier_code,
            price_date,price,volume,high,low,open_price,
            yield,board_lot_flag,last_trade_time,bid_date,bid,bid_yield,
            ask_date,ask,ask_yield,mid_date,mid,mid_yield,number_of_trans,
            user_key_code,updated_flag,price_origin_code,
            last_user,last_stamp,
            dirty_price,price_mod_duration,bid_dirty_price,bid_mod_duration,
            ask_dirty_price,ask_mod_duration,mid_dirty_price,mid_mod_duration,
            accrued_interest,term_in_years,settlement_date,maturity_date,
            maturity_type,index_ratio,rpb_factor,effective_date_factor,
            next_chg_date_factor,benchmark_code,spread,spread_from_generic,
            spread_from_libor,convexity,pvbp,average_life_date,market_cap
          , IFRS_Code
          , High_52w
          , Low_52w
          , Official_Close
          , Trade_Count
          , Close_Cumulative_Volume
          , Close_Cumulative_Volume_Status
          , Close_Cumulative_Volume_Date
            )
         SELECT pricing_data.fri_ticker,
            pricing_data.source_code,
            pricing_data.price_currency,
            pricing_data.delivery_date,
            pricing_data.data_supplier_code,
            temp_price_date,
            pricing_data.price,
            pricing_data.volume,
            pricing_data.high,
            pricing_data.low,
            pricing_data.open_price,
            pricing_data.yield,
            pricing_data.board_lot_flag,
            pricing_data.last_trade_time,
            temp_bid_date,
            pricing_data.bid,
            pricing_data.bid_yield,
            temp_ask_date,
            pricing_data.ask,
            pricing_data.ask_yield,
            temp_mid_date,
            pricing_data.mid,
            pricing_data.mid_yield,
            pricing_data.number_of_trans,
            pricing_data.user_key_code,
            temp_updated_flag,
            Temp_Origin,
            user_id,
            SYSDATE,
            Pricing_Data.dirty_price,
            Pricing_Data.price_mod_duration,
            Pricing_Data.bid_dirty_price,
            Pricing_Data.bid_mod_duration,
            Pricing_Data.ask_dirty_price,
            Pricing_Data.ask_mod_duration,
            Pricing_Data.mid_dirty_price,
            Pricing_Data.mid_mod_duration,
            Pricing_Data.accrued_interest,
            Pricing_Data.term_in_years,
            Pricing_Data.settlement_date,
            Pricing_Data.maturity_date,
            Pricing_Data.maturity_type,
            Pricing_Data.index_ratio,
            Pricing_Data.rpb_factor,
            Pricing_Data.effective_date_factor,
            Pricing_Data.next_chg_date_factor,
            Pricing_Data.benchmark_code,
            Pricing_Data.spread,
            Pricing_Data.spread_from_generic,
            Pricing_Data.spread_from_libor,
            Pricing_Data.convexity,
            Pricing_Data.pvbp,
            Pricing_Data.average_life_date,
            Pricing_Data.Market_Cap
          , Pricing_Data.IFRS_Code
          , Pricing_Data.High_52w
          , Pricing_Data.Low_52w
          , Pricing_Data.Official_Close
          , Pricing_Data.Trade_Count
          , Pricing_Data.Close_Cumulative_Volume
          , Pricing_Data.Close_Cumulative_Volume_Status
          , Pricing_Data.Close_Cumulative_Volume_Date
           FROM dual
          WHERE NOT EXISTS
                (SELECT 'X'
                 FROM Security_Prices SS
                 WHERE SS.fri_ticker   = pricing_data.fri_ticker
                 AND SS.source_code    = pricing_data.source_code
                 AND SS.price_currency = pricing_data.price_currency
                 AND SS.delivery_date  = pricing_data.delivery_date
                 AND SS.data_supplier_code =
                     pricing_data.data_supplier_code);
         --
         -- SQLROWCOUNT will be equal to 1 - If no bugs.
         -- Add 1 to rejected rows if Insert failed.
         --
         IF SQL%ROWCOUNT > 0 THEN
            pgmloc := 900;
            rows_inserted := rows_inserted + SQL%ROWCOUNT;
            p_rows_ins    := p_rows_ins    + SQL%ROWCOUNT;
         ELSE
            rows_rejected := rows_rejected + 1;
         END IF;
      ELSE
         rows_skip := rows_skip + 1;
      END IF;
<< END_OF_LOOP >>
      Last_Fri_Ticker := Pricing_Data.Fri_Ticker;
   END LOOP;
   CLOSE Pricing_Details;
   --
-- Ajout - JSL - 07 janvier 2014
<< Carry_Forward_Done >>
-- Fin d'ajout - JSL - 07 janvier 2014
--
-- nous mettons a jour le Last_Run_Date de supplier_running_parms
--
   UPDATE Supplier_Running_Parms SRP
      SET Last_Run_Date  = sysdate
    WHERE SRP.Program_Id = P_Program_Id;
--
-- If #read = #reject ---> Carry Forward is run for a 2nd time
--
   pgmloc := 910;
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
-- Ajout - JSL - 07 janvier 2014
   IF  Intra_Day
   AND Is_Fri_Open IS NOT NULL
   THEN
      NULL;
   ELSE
-- Fin d'ajout - JSL - 07 janvier 2014
      -- Si aucune rangee a ete lu, on veut faire un raise pour avoir
      -- le message approprie
      IF rows_read = 0
      THEN
         RAISE No_Data;
      END IF;
      --
      IF rows_read = rows_rejected THEN
         Pricing_Msg_Rec.Msg_Text_2 :=
           'WARNING - Carry Fwd was already DONE';
      ELSE
         Pricing_Msg_Rec.Msg_Text_2 := 'Carry Fwd DONE ';
         IF P_Restart_Flag = 'Y'
         THEN
            Pricing_Msg_Rec.Msg_Text_2 := Pricing_Msg_Rec.Msg_Text_2 ||
             'Restarted for ' || to_char(P_Restart_Date,'DD-MON-YYYY');
         END IF;
      END IF;
-- Ajout - JSL - 07 janvier 2014
   END IF;
-- Fin d'ajout - JSL - 07 janvier 2014
--
   pgmloc := 920;
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
    (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
<< Fin_de_Programme >>
   COMMIT;
   --
-- Ajout - JSL - 10 septembre 2014
   Set_Tms_to_Unscheduled( Pricing_Msg_Rec );
-- Fin d'ajout- JSL - 10 septembre 2014
--
EXCEPTION
--
WHEN inv_prc_date
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN no_data_found
THEN
--
   -- The error message is intialized at the time of raising the
   -- exception
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   --
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
-- Pour toutes les autres exceptions, on met le message d'oracle
WHEN no_data
THEN
--
   Pricing_Msg_Rec.Remarks     := 'No data to apply carry forward on';
   --
   IF Temp_First_Run_Date = P_Delivery_Date
   THEN
      Pricing_Msg_Rec.Remarks  := Pricing_Msg_Rec.Remarks  ||
                                 ' - First Carry Fwd';
   END IF;
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   --
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
-- Pour toutes les autres exceptions, on met le message d'oracle
--
WHEN Bad_Previous_Cf
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Msg_Text    :=
     'Previous CF '                             ||
      to_char(Previous_Delivery_Date,'ddmonyyyy') ||
     ' didn''t work.Create a rec succ Y in '    ||
     'Prcg Msg';
   --
   Pricing_Msg_Rec.Msg_Text_2  :=
     'OR Change the Dates in Site_Controls (Take a copy of the table)';
   --
   Pricing_Msg_Rec.Err_Text    :=
     'Recompile the proc and run the Carry Fwd of '     ||
     to_char(Previous_Delivery_Date,'ddmonyyyy') || ' '   ||
     V_CF_Delivery_Time;
   --
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN Cannot_Restart
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN others
THEN
   IF NOT(Handled_Raise)
   THEN
      -- Err_Text is 255 characters long (not 68)
      Pricing_Msg_Rec.Err_Text    := substr(SQLERRM(SQLCODE),1,255);
      Pricing_Msg_Rec.Msg_Text_2  :=
        'Unhandled exception occurred at pgmloc:'||to_char(pgmloc);
      Pricing_Msg_Rec.Successfull_Flag := 'N';
      Pricing_Msg_Rec.Value_01 :=  Rows_Read;
      Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
      Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
      Pricing_Msg_Rec.Value_05 :=  Rows_Skip;
      Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
      --
      Global_File_Updates.Update_Pricing_Msgs
        (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   END IF;
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   IF NOT(Handled_Raise)
   THEN
      -- We may be doing a rollback in send email...
      Send_Email( Pricing_Msg_Rec );
   END IF;
-- Fin d'ajout - JSL - 10 septembre 2014
   RAISE;
--}}
END on_Security_Prices;
-----------------------------------------------------------------------
--
PROCEDURE CFW_PRICE_OR_EXCH
   (P_FRI_TICKER         IN  SECURITIES.FRI_TICKER%TYPE,
    P_SOURCE_CODE        IN  SOURCES.CODE%TYPE,
    P_PRICE_CURRENCY     IN  CURRENCIES.CURRENCY%TYPE,
    P_DELIVERY_DATE      IN  DATE,
    P_DATA_SUPPLIER_CODE IN  DATA_SUPPLIERS.CODE%TYPE,
    P_CFW_TYPE           IN  CHAR,
    P_CFW_COUNT         OUT  NUMBER,
    P_STATUS            OUT  NUMBER,
    P_MESSAGE           OUT  VARCHAR2)
IS
--{
INTRA_DAY            BOOLEAN;    -- si intra-day ou non
LANGUAGE             LANGUAGES.CODE%TYPE;
SECURITY_TYPE        SECURITY_TYPES.CODE%TYPE;
FREQUENCY_VALUE      FREQUENCIES.VALUE%TYPE;
INTRA_DAY_PRICE_EXCH EXCEPTION;
NOT_ON_FILE          EXCEPTION;
INVALID_TYPE         EXCEPTION;
--
BEGIN
--{
   P_CFW_COUNT := 0;
--    On valide le type du prix
--
      pgmloc := 930;
   IF (P_CFW_TYPE!='A') AND (P_CFW_TYPE!='B') AND (P_CFW_TYPE!='M')
   AND (P_CFW_TYPE!='P') THEN
      RAISE INVALID_TYPE;
   END IF;
--
   CFW_COUNT      := 0;
   P_STATUS       := 0;
   P_MESSAGE      := NULL;
   CFW_PRICE_TYPE := P_CFW_TYPE;
   TICKER         := P_FRI_TICKER;
   SC_CODE        := P_SOURCE_CODE;
   CURRENCY       := P_PRICE_CURRENCY;
   DEL_DATE       := P_DELIVERY_DATE;
   SUPPLIER_CODE  := P_DATA_SUPPLIER_CODE;
--
   pgmloc := 940;
   SELECT LANGUAGE_CODE , USER_ID
     INTO LANGUAGE , INITIALS
     FROM USER_LIST
    WHERE USER_NAME = USER;
--
--    On doit verifier si le ticker appartient a un prix ou a un taux
--
   pgmloc := 950;
   SELECT SECURITY_TYPE_CODE , INCOME_FREQUENCY_VALUE
     INTO SECURITY_TYPE, FREQUENCY_VALUE
     FROM SECURITIES
    WHERE FRI_TICKER = TICKER;
--
   pgmloc := 960;
   SECURITY_PATTERN := Security.GET_SEC_TYPE_PATTERN
                             (SECURITY_TYPE,
                              FREQUENCY_VALUE);
--
--    Verifie si le record existe avant de continuer ...
--
   IF SECURITY_PATTERN = EXCH_CODE THEN
      pgmloc := 970;
      OPEN CUR_GET_EXCH_INFO;
      FETCH CUR_GET_EXCH_INFO INTO ER_VALUES;
      IF CUR_GET_EXCH_INFO%NOTFOUND THEN
         CLOSE CUR_GET_EXCH_INFO;
         RAISE NOT_ON_FILE;
      END IF;
      CLOSE CUR_GET_EXCH_INFO;
   ELSE
      pgmloc := 980;
      OPEN CUR_GET_SEC_INFO;
      FETCH CUR_GET_SEC_INFO INTO SP_VALUES;
      IF CUR_GET_SEC_INFO%NOTFOUND THEN
         CLOSE CUR_GET_SEC_INFO;
         RAISE NOT_ON_FILE;
      END IF;
      CLOSE CUR_GET_SEC_INFO;
   END IF;
--
   pgmloc := 990;
   INTRA_DAY := Supplier.IS_IT_INTRA_DAY(SUPPLIER_CODE,
                                            TO_CHAR(DEL_DATE,
                                                          'HH24MI'));
   IF INTRA_DAY  THEN
      RAISE INTRA_DAY_PRICE_EXCH;
   END IF;
--
--    Si prix/taux n'est pas Intra-Day, on continue ...
--    on appelle la procedure pour faire le Carry Forward
--
   pgmloc := 1000;
   CFW_PRICE_OR_EXCH_APPLY;
   P_CFW_COUNT := CFW_COUNT;
   P_STATUS := STATUS;
   IF STATUS = 2 THEN
      G_Msg_Code :=  G_Err_Code||'020';
      Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
   END IF;
--
--    Exceptions
--
EXCEPTION
WHEN INTRA_DAY_PRICE_EXCH THEN
   P_STATUS := 3;
   G_Msg_Code :=  G_Err_Code||'030';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
   --
WHEN NOT_ON_FILE THEN
   P_STATUS := 1;
   G_Msg_Code :=  G_Err_Code||'040';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
   --
WHEN INVALID_TYPE THEN
   P_STATUS := 2;
   G_Msg_Code :=  G_Err_Code||'050';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
   --
WHEN OTHERS THEN
   P_STATUS := 2;
   G_Msg_Code := G_Err_Code||'020';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
   P_Message := P_Message||' Pgmloc= '||to_char(Pgmloc);
   --
   RAISE;
--}}
END CFW_PRICE_OR_EXCH;
-----------------------------------------------------------------------
--
-- Codage fait par Richard,
-- Pour permettre de reappliquer de facon globale le CFW des prix
-- pour un fournisseur
-- Utilise par FDS 1200
--
--
PROCEDURE CFW_PRICE_ALL
   (P_DELIVERY_DATE      IN  DATE,
    P_DATA_SUPPLIER_CODE IN  DATA_SUPPLIERS.CODE%TYPE,
    P_CFW_TYPE           IN  CHAR,
    P_COMMIT_COUNT       IN  NUMBER,
    P_PROGRAM_ID_PRICES  IN  Ops_Scripts.Program_ID%Type,
    P_SYSTEM_DATE        IN  DATE,
    P_CFW_COUNT         OUT  NUMBER,
    P_STATUS            OUT  NUMBER,
    P_MESSAGE           OUT  VARCHAR2)
IS
   --{
   INTRA_DAY            BOOLEAN;    -- si intra-day ou non
   LANGUAGE             LANGUAGES.CODE%TYPE;
   INTRA_DAY_PRICE_EXCH EXCEPTION;
   INVALID_TYPE         EXCEPTION;
   T_Commit_Count       Number;
   V_Commit_Count       Number;
   --
   CURSOR Get_All_Securities  IS
      SELECT P.Fri_Ticker,
             P.Source_Code,
             P.Price_Currency
        From Security_Prices P
       Where P.Data_Supplier_Code = Supplier_Code
         And P.Delivery_Date      = Del_Date
   Order by 1,2,3;
   --
   All_Secs Get_All_Securities%ROWTYPE;
   --
BEGIN
   --{
   P_CFW_COUNT := 0;
   --
   -- on valide le type du prix
   --
   pgmloc := 1010;
   IF (P_CFW_TYPE != 'A') AND
      (P_CFW_TYPE != 'B') AND
      (P_CFW_TYPE != 'M') AND
      (P_CFW_TYPE != 'P')
   THEN
      RAISE INVALID_TYPE;
   END IF;
   --
   CFW_COUNT      := 0;
   P_STATUS       := 0;
   P_MESSAGE      := NULL;
   T_Commit_Count := 0;
   V_Commit_Count := 0;
   DEL_DATE       := P_DELIVERY_DATE;
   SUPPLIER_CODE  := P_DATA_SUPPLIER_CODE;
   CFW_PRICE_TYPE := P_CFW_TYPE;
   --
   pgmloc := 1020;
   --
   -- Get Language and User Initials
   --
   Language         := Constants.Get_User_Language;
   Initials         := Constants.Get_User_ID;
   Security_Pattern := 'IB';
   --
   --  Is it an Intra-Day?
   --
   pgmloc := 1030;
   INTRA_DAY := Supplier.IS_IT_INTRA_DAY
                   (SUPPLIER_CODE,
                    TO_CHAR(DEL_DATE, 'HH24MI'));
   IF INTRA_DAY
   THEN
      RAISE INTRA_DAY_PRICE_EXCH;
   END IF;
   --
   -- For all Securities
   --
   For All_Secs in Get_All_Securities Loop
      pgmloc           := 1040;
      Ticker           := All_Secs.Fri_Ticker;
      Sc_Code          := All_Secs.Source_Code;
      Currency         := All_Secs.Price_Currency;
      -- Get Current Row
      OPEN CUR_GET_SEC_INFO;
      FETCH CUR_GET_SEC_INFO INTO SP_VALUES;
      CLOSE CUR_GET_SEC_INFO;
      --
      -- Apply Carry Fwd
      --
      pgmloc := 1050;
      CFW_PRICE_OR_EXCH_APPLY;
      P_CFW_COUNT := P_CFW_Count + CFW_COUNT;
      P_STATUS    := STATUS;
      IF STATUS = 2
      THEN
         G_Msg_Code :=  G_Err_Code||'020';
         Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
         --
      END IF;
      -- Do I Commit?
      V_Commit_Count := V_Commit_Count + 1;
      IF P_Commit_Count > 0 AND
         V_Commit_Count >= P_Commit_Count
      THEN
         T_Commit_Count := T_Commit_Count + V_Commit_Count;
         -- Update Pricing Msgs - Counter #5 and #7
         Update Pricing_Msgs
            set Value_05    = P_Cfw_Count,
                Value_07    = T_Commit_Count,
                Last_Stamp  = Sysdate
          where Program_ID  = P_Program_ID_Prices
            and Date_of_msg = P_System_Date;
         Commit;
         --
         V_Commit_Count := 0;
      END IF;
   END LOOP;
--
--    Exceptions
--
EXCEPTION
WHEN INTRA_DAY_PRICE_EXCH THEN
   P_STATUS := 3;
   G_Msg_Code :=  G_Err_Code||'030';
   Manage_Messages.Get_Oasis_Message(
                 G_Msg_Code,
                 G_Delete_Flag,
                 P_Message);
   --
WHEN INVALID_TYPE THEN
   P_STATUS := 2;
   G_Msg_Code :=  G_Err_Code||'050';
   Manage_Messages.Get_Oasis_Message(
                 G_Msg_Code,
                 G_Delete_Flag,
                 P_Message);
   --
WHEN OTHERS THEN
   P_STATUS := 2;
   G_Msg_Code :=  G_Err_Code||'020';
   Manage_Messages.Get_Oasis_Message(
                 G_Msg_Code,
                 G_Delete_Flag,
                 P_Message);
   P_Message := P_Message||' pgmloc='||to_char(pgmloc);
   --
   RAISE;
--}}
END CFW_PRICE_ALL;
-----------------------------------------------------------------------
--
-- Nouvelle procedure qui met a jour une chaine de carry fwd commencant
-- par la rangee de prix la plus recente.  (avec son nouveau prix)
--
-- Cette procedure a ete modifie pour cree la rangee de prix
-- dans le cas des prix de fin de semaine ou de jour ferie
-- une fois que la rangee de prix est cree le reverse CFW pourra
-- etre applique
--
PROCEDURE CFW_REVERSE_PRICE_OR_EXCH
   (P_FRI_TICKER         IN  SECURITIES.FRI_TICKER%TYPE,
    P_SOURCE_CODE        IN  SOURCES.CODE%TYPE,
    P_PRICE_CURRENCY     IN  CURRENCIES.CURRENCY%TYPE,
    P_DELIVERY_DATE      IN  DATE,
    P_DATA_SUPPLIER_CODE IN  DATA_SUPPLIERS.CODE%TYPE,
    P_CFW_TYPE           IN  CHAR,
    P_CFW_COUNT         OUT  NUMBER,
    P_STATUS            OUT  NUMBER,
    P_MESSAGE           OUT  VARCHAR2)
IS
--{
Intra_Day            BOOLEAN;    -- si intra-day ou non
Language             LANGUAGES.CODE%TYPE;
Security_Type        SECURITY_TYPES.CODE%TYPE;
Frequency_Value      FREQUENCIES.VALUE%TYPE;
CFW_Required         BOOLEAN;
Intra_Day_Price_Exch EXCEPTION;
Not_On_File          EXCEPTION;
Invalid_Type         EXCEPTION;
Archived_Data        EXCEPTION;
Invalid_WE_Flag      EXCEPTION;
Cutoff_Date          DATE;
Arch_Date_Field      All_Tab_Columns.Column_Name%TYPE;
Table_Name           All_Tables.Table_Name%TYPE;
--
Accept_WE_Prices     YES_NO.Code%TYPE;
Holiday_Code         Holiday_Codes.Code%TYPE;
Temp_Buffer          CHAR(255);
--
New_Del_Date         DATE;
CURSOR Get_WE_Hldy_Flag
IS
    SELECT DSS.Accept_WE_Hldy_Prices_Flag
      FROM Data_Supply_Schedule DSS
     WHERE DSS.Data_Supplier_Code =   P_Data_Supplier_Code
       AND DSS.Delivery_Time      =   To_Char(P_Delivery_Date,'HH24MI');
--
CURSOR Does_WE_Rec_Exist (P_Delivery_Date DATE)
IS
    --mlz - 05nov2015 - do not take any chance
    --SELECT 'X'
    SELECT /*+ index(sp security_prices_x_01) */ 'X'
    --endmlz - 05nov2015
      FROM Security_Prices SP
     WHERE SP.Fri_Ticker         = P_Fri_Ticker
       AND SP.Data_Supplier_Code = P_Data_Supplier_Code
       AND SP.Price_Currency     = P_Price_Currency
       AND SP.Source_Code        = P_Source_Code
       AND SP.Delivery_Date      = P_Delivery_Date;
--
BEGIN
--{
   P_CFW_Count := 0;
--    On valide le type du prix
--
   pgmloc := 1060;
   IF (P_CFW_Type!='A') AND (P_CFW_Type!='B') AND (P_CFW_Type!='M')
   AND (P_CFW_Type!='P')
   THEN
      RAISE Invalid_Type;
   END IF;
--
   CFW_Count      := 0;
   P_Status       := 0;
   P_Message      := NULL;
   CFW_Price_Type := P_CFW_Type;
--
   Ticker         := P_FRI_Ticker;
   SC_Code        := P_Source_Code;
   Currency       := P_Price_Currency;
   Del_Date       := P_Delivery_Date;
   Supplier_Code  := P_Data_Supplier_Code;
--
   pgmloc := 1070;
--
   Initials := Constants.Get_User_Id;
   pgmloc := 1080;
   SELECT Security_Type_Code,
          Income_Frequency_Value
     INTO Security_Type,
          Frequency_Value
     FROM Securities
    WHERE Fri_Ticker = Ticker;
--
   pgmloc := 1090;
   Security_Pattern := Security.GET_SEC_TYPE_PATTERN
                             (Security_Type,
                              Frequency_Value);
--
   Intra_Day := Supplier.Is_It_Intra_Day(SUPPLIER_CODE,
                                            TO_CHAR(DEL_DATE,
                                                          'HH24MI'));
--
   IF Intra_Day
   THEN
      RAISE Intra_Day_Price_Exch;
   END IF;
   --
   IF Security_Pattern = EXCH_CODE
   THEN
      pgmloc := 1100;
      OPEN Cur_Get_Exch_Info;
      FETCH Cur_Get_Exch_Info INTO ER_Values;
      --
      IF Cur_Get_Exch_Info%NOTFOUND
      THEN
         CLOSE Cur_Get_Exch_Info;
         RAISE Not_On_File;
      END IF;
      CLOSE Cur_Get_Exch_Info;
      --
      Table_Name := 'EXCHANGE_RATES';
--    SSI - 28OCT2005 - Check if the cutoff date is already fetched before
--                      fetching
      IF G_Exch_Arch_Cutoff IS NULL THEN
         Archive_Data.Get_Archive_Date( UPPER(Table_Name) ,NULL,'R',
                                       Arch_Date_Field,Cutoff_Date);
         G_Exch_Arch_Cutoff := Cutoff_Date;
      ELSE
         Cutoff_Date := G_Exch_Arch_Cutoff;
      END IF;
      --
      IF ER_Values.Price_Date <> TRUNC(Del_Date)
      THEN
         CFW_Required := TRUE;
         IF ER_Values.Price_Date <= Cutoff_Date
         THEN
            Raise Archived_Data;
         END IF;
         P_Temp_Date  := to_date(to_char(ER_Values.Price_Date,
                                          'dd-mon-yyyy') ||
                                          '000000',
                                          'dd-mon-yyyyHH24MISS');
      END IF;
   ELSE
      pgmloc := 1110;
      OPEN Cur_Get_Sec_Info;
      FETCH Cur_Get_Sec_Info INTO SP_Values;
      IF Cur_Get_Sec_Info%NOTFOUND
      THEN
         CLOSE Cur_Get_Sec_Info;
         RAISE Not_On_File;
      END IF;
      CLOSE Cur_Get_Sec_Info;
      --
      Table_Name := 'SECURITY_PRICES';
--    SSI - 28OCT2005 - Check if the cutoff date is already fetched before
--                      fetching
      IF G_Prc_Arch_Cutoff IS NULL THEN
         Archive_Data.Get_Archive_Date( UPPER(Table_Name) ,NULL,'R',
                                       Arch_Date_Field,Cutoff_Date);
         G_Prc_Arch_Cutoff := Cutoff_Date;
      ELSE
         Cutoff_Date := G_Prc_Arch_Cutoff;
      END IF;
      --
      IF CFW_Price_Type = 'A'
      THEN
         IF SP_Values.Ask_Date <> TRUNC(Del_Date)
         THEN
            CFW_Required := TRUE;
            IF SP_Values.Ask_Date <= Cutoff_Date
            THEN
               Raise Archived_Data;
            END IF;
            P_Temp_Date  := to_date(to_char(SP_Values.Ask_Date,
                                           'dd-mon-yyyy') ||
                                           '000000',
                                           'dd-mon-yyyyHH24MISS');
         END IF;
      END IF;
      --
      IF CFW_Price_Type = 'B'
      THEN
         IF SP_Values.Bid_Date <> TRUNC(Del_Date)
         THEN
            CFW_Required := TRUE;
            IF SP_Values.Bid_Date <= Cutoff_Date
            THEN
               Raise Archived_Data;
            END IF;
            P_Temp_Date  := to_date(to_char(SP_Values.Bid_Date,
                                           'dd-mon-yyyy') ||
                                           '000000',
                                           'dd-mon-yyyyHH24MISS');
         END IF;
      END IF;
      --
      IF CFW_Price_Type = 'M'
      THEN
         IF SP_Values.Mid_Date <> TRUNC(Del_Date)
         THEN
            CFW_Required := TRUE;
            IF SP_Values.Mid_Date <= Cutoff_Date
            THEN
               Raise Archived_Data;
            END IF;
            P_Temp_Date  := to_date(to_char(SP_Values.Mid_Date,
                                           'dd-mon-yyyy') ||
                                           '000000',
                                           'dd-mon-yyyyHH24MISS');
         END IF;
      END IF;
      IF CFW_Price_Type = 'P'
      THEN
         IF SP_Values.Price_Date <> TRUNC(Del_Date)
         THEN
            CFW_Required := TRUE;
            IF SP_Values.Price_Date <= Cutoff_Date
            THEN
               Raise Archived_Data;
            END IF;
            P_Temp_Date  := to_date(to_char(SP_Values.Price_Date,
                                           'dd-mon-yyyy') ||
                                           '000000',
                                           'dd-mon-yyyyHH24MISS');
         END IF;
      END IF;
   END IF;
--
--    on appelle la procedure pour faire le Carry Forward
   pgmloc := 1120;
   --
   IF CFW_Required
   THEN
      --
      -- Nous allons verifier premierement si le prix
      -- (price_date,ask_date,bid_date,mid_date) tombe sur un samedi
      -- dimanche, ou jour ferie, si oui est si Data_supply_Schedule
      -- Accept_WE_Hldy_Prices_Flag = 'Y' nous inserons la
      -- rangee
      --
      OPEN Get_WE_Hldy_Flag;
      FETCH Get_WE_Hldy_Flag INTO Accept_WE_Prices;
      CLOSE Get_WE_Hldy_Flag;
      --
      IF CFW_Price_Type = 'P'
      THEN
         Holiday_Code := Holiday.Is_It_Open(
              SP_Values.Price_Date,
              'SUPPLIER',
              Supplier_Code,
              to_char(Del_Date,'HH24MI')) ;
         --
         IF Holiday_Code IS NULL
         THEN
            Holiday_Code := Holiday.Is_It_Open(
               SP_Values.Price_Date,
              'SOURCE',
               SC_Code,
               NULL);
         END IF;
         --
         IF Holiday_Code IS NOT NULL
         THEN
            --
            -- Insert Record if record doesn't already exist
            --
            -- Nous voulons verifier le flag Accept_WE_Hldy_Prices ici
            -- si non une exception sera levee
            --
            -- Permettre de rentrer des prix pour le 02 Jan 2000
            -- Richard (02 Dec 1999) - Pour le Test Y2K
            --
            IF Accept_WE_Prices <> 'Y' AND
               to_char(SP_Values.Price_Date,'DD-Mon-YYYY') <>
                   '02-JAN-2000'
            THEN
               RAISE Invalid_WE_Flag;
            END IF;
-- Nous devons quand meme verifier si nous devons ou non inserer une rangee
-- si la date de prix n'existe pas encore.
         END IF ; -- WE or Holiday;
            New_Del_Date :=
               to_date(to_char(p_temp_date,'DD-MON-YYYY') || ' ' ||
                    to_char(del_date,'HH24MI'),'DD-MON-YYYY HH24MI');
            --
         WHILE TRUE LOOP
            OPEN Does_WE_Rec_Exist (New_Del_Date);
            FETCH Does_WE_Rec_Exist INTO Temp_Buffer;
            IF Does_WE_Rec_Exist%NOTFOUND
            THEN
               INSERT INTO Security_Prices
                   (Fri_Ticker,Source_Code,Price_Currency,Delivery_Date,
                    Data_Supplier_Code, Price_Date, Price, Volume, High,
                    Low,Open_Price,Yield,Board_Lot_Flag,Last_Trade_Time,
                    Number_Of_Trans,
                    Bid_Date,Bid,Bid_Yield,Ask_Date,Ask,Ask_Yield,
                    Mid_Date,Mid,Mid_Yield,Updated_Flag,Price_Origin_Code,
                    Last_User,Last_Stamp,
                    dirty_price,price_mod_duration,bid_dirty_price,
                    bid_mod_duration,ask_dirty_price,ask_mod_duration,
                    mid_dirty_price,mid_mod_duration,accrued_interest,
                    term_in_years,settlement_date,maturity_date,
                    maturity_type,index_ratio,rpb_factor,effective_date_factor,
                    next_chg_date_factor,benchmark_code,spread,spread_from_generic,
                    spread_from_libor,convexity,pvbp,average_life_date,market_cap
                  , IFRS_Code
                  , High_52w
                  , Low_52w
                  , Official_Close
                  , Trade_Count
                  , Close_Cumulative_Volume
                  , Close_Cumulative_Volume_Status
                  , Close_Cumulative_Volume_Date
                  )
               VALUES (Ticker, SC_Code, Currency, New_Del_Date,
                      Supplier_Code, SP_Values.Price_Date,
                      SP_Values.Price, SP_Values.Volume,SP_Values.High,
                      SP_Values.Low,SP_Values.Open_Price,
                      SP_Values.Yield,SP_Values.Board_Lot_Flag,
                      SP_Values.Last_Trade_Time,
                      SP_Values.Number_Of_Trans,
                      SP_Values.Bid_Date,SP_Values.Bid,SP_Values.Bid_Yield,
                      SP_Values.Ask_Date,SP_Values.Ask,SP_Values.Ask_Yield,
                      SP_Values.Mid_Date,SP_Values.Mid,SP_Values.Mid_Yield,
                      'Y','L',
                      Constants.Get_User_Id, SYSDATE,
                      SP_Values.dirty_price,SP_Values.price_mod_duration,
                      SP_Values.bid_dirty_price,SP_Values.bid_mod_duration,
                      SP_Values.ask_dirty_price,SP_Values.ask_mod_duration,
                      SP_Values.mid_dirty_price,SP_Values.mid_mod_duration,
                      SP_Values.accrued_interest,SP_Values.term_in_years,
                      SP_Values.settlement_date,SP_Values.maturity_date,
                      SP_Values.maturity_type,SP_Values.index_ratio,
                      SP_Values.rpb_factor,SP_Values.effective_date_factor,
                      SP_Values.next_chg_date_factor,SP_Values.benchmark_code,
                      SP_Values.spread,SP_Values.spread_from_generic,
                      SP_Values.spread_from_libor,SP_Values.convexity,
                      SP_Values.pvbp,SP_Values.average_life_date,
                      SP_Values.Market_Cap
                    , SP_Values.IFRS_Code
                    , SP_Values.High_52w
                    , SP_Values.Low_52w
                    , SP_Values.Official_Close
                    , SP_Values.Trade_Count
                    , SP_Values.Close_Cumulative_Volume
                    , SP_Values.Close_Cumulative_Volume_Status
                    , SP_Values.Close_Cumulative_Volume_Date
                    );
--          Get the next date based on the day we are open.
--          We are doing carry forward, not prices load.
            New_Del_Date := Holiday.Get_Next_Open_Date( TRUNC(New_Del_Date), 'HOLCLASS', NULL, 'STP' );
            New_Del_Date := to_date( to_char(New_Del_Date,'DD-MON-YYYY')
                                  || ' '
                                  || to_char(del_date,'HH24MI')
                                   , 'DD-MON-YYYY HH24MI');
            ELSE
               EXIT;
            END IF ; -- rec does not exist
            CLOSE Does_WE_Rec_Exist;
         END LOOP;
      END IF; -- Type = 'P'
      --
      CFW_REV_PRICE_OR_EXCH_APPLY;
      P_CFW_COUNT := CFW_COUNT;
      P_STATUS    := STATUS;
   END IF; -- CFW_Required
   --
   IF Status = 2
   THEN
      G_Msg_Code :=  G_Err_Code||'020';
      Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
   END IF;
--
-- Exceptions
--
EXCEPTION
WHEN INTRA_DAY_PRICE_EXCH THEN
   P_STATUS := 3;
   G_Msg_Code :=  G_Err_Code||'030';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
--
WHEN Invalid_WE_Flag
THEN
   P_STATUS := 5;
   G_Msg_Code :=  G_Err_Code||'060';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
--
WHEN Archived_Data
THEN
   P_STATUS := 4;
   G_Msg_Code :=  G_Err_Code||'070';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
WHEN NOT_ON_FILE THEN
   P_STATUS := 1;
   G_Msg_Code :=  G_Err_Code||'040';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
WHEN INVALID_TYPE THEN
   P_STATUS := 2;
   G_Msg_Code :=  G_Err_Code||'050';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
WHEN OTHERS THEN
   P_STATUS := 2;
   G_Msg_Code :=  G_Err_Code||'020';
   Manage_Messages.Get_Oasis_Message(
                    G_Msg_Code,
                    G_Delete_Flag,
                    P_Message);
   P_Message := P_Message||' pgmloc='||to_char(pgmloc);
   RAISE;
   --}}
END CFW_REVERSE_PRICE_OR_EXCH;
----------------------------------------------------------------------
--
--    La Procedure CFW_PRICE_OR_EXCH_APPLY va executer les Carry
--    Forwards de Ask, Bid, Mid, Price, et taux de change dependant du
--    CFW_TYPE.
--
PROCEDURE CFW_PRICE_OR_EXCH_APPLY IS
--{
--     Cette procedure va appliquer le carry forward sur ou le mid,
--    le bid, le ask, le prix, ou le taux de change
--
    PRICE_BUFFER       CHAR(255);
    EXCH_BUFFER        CHAR(255);
--
-- Ajout - JSL - 06 mai 2014
    Carry_Fwd_Applied  BOOLEAN;
-- Fin d'ajout - JSL - 06 mai 2014
--
--    Ce cursor va mettre un 'lock' sur les rangees necessaires
--    pour faire un carry forward d'un ASK
--
CURSOR CUR_GET_CFW_ASKS IS
-- Modification - JSL - 06 mai 2014
--  SELECT PRICE_BUFFER
-- Modification - JSL - 19 novembre 2015
--  SELECT *
    SELECT /*+ INDEX( SP SECURITY_PRICES_X_02) */ 
           *
-- Fin de modification - JSL - 19 novembre 2015
-- Fin de modification - JSL - 06 mai 2014
    FROM Security_Prices SP
    WHERE SP.FRI_TICKER         = TICKER
    AND   SP.SOURCE_CODE        = SC_CODE
    AND   SP.PRICE_CURRENCY     = CURRENCY
    AND   SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
    AND   SP.DELIVERY_DATE     >  DEL_DATE
    AND   (SP.ASK_DATE         <= DEL_DATE
     OR    SP.ASK_DATE         IS NULL)
-- Modification - JSL - 06 mai 2014
--  AND   Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
-- Fin de modification - JSL - 06 mai 2014
    FOR UPDATE OF SP.ASK_DATE,
                  SP.ASK,
                  SP.Ask_Yield,
                  SP.LAST_USER,
                  SP.LAST_STAMP,
                  SP.Ask_Dirty_Price,
                  SP.Ask_Mod_Duration
               ;
--
--    Ce cursor va metter un 'lock' sur les rangees necessaires
--    pour faire un carry forward d'un BID
--
CURSOR CUR_GET_CFW_BIDS IS
-- Modification - JSL - 06 mai 2014
--  SELECT PRICE_BUFFER
-- Modification - JSL - 19 novembre 2015
--  SELECT *
    SELECT /*+ INDEX( SP SECURITY_PRICES_X_02) */ 
           *
-- Fin de modification - JSL - 19 novembre 2015
-- Fin de modification - JSL - 06 mai 2014
    FROM Security_Prices SP
    WHERE SP.FRI_TICKER         = TICKER
    AND   SP.SOURCE_CODE        = SC_CODE
    AND   SP.PRICE_CURRENCY     = CURRENCY
    AND   SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
    AND   SP.DELIVERY_DATE     >  DEL_DATE
    AND   (SP.BID_DATE         <= DEL_DATE
     OR    SP.BID_DATE          IS NULL)
-- Modification - JSL - 06 mai 2014
--  AND    Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
-- Fin de modification - JSL - 06 mai 2014
    FOR UPDATE OF SP.BID_DATE,
                  SP.BID,
                  SP.Bid_Yield,
                  SP.LAST_USER,
                  SP.LAST_STAMP,
                  SP.Bid_Dirty_Price,
                  SP.Bid_Mod_Duration;
--
--    Ce cursor va mettre un 'lock' sur les rangees necessaires
--    pour faire un carry forward d'un MID
--
CURSOR CUR_GET_CFW_MIDS IS
-- Modification - JSL - 06 mai 2014
--  SELECT PRICE_BUFFER
-- Modification - JSL - 19 novembre 2015
--  SELECT *
    SELECT /*+ INDEX( SP SECURITY_PRICES_X_02) */ 
           *
-- Fin de modification - JSL - 19 novembre 2015
-- Fin de modification - JSL - 06 mai 2014
    FROM Security_Prices SP
    WHERE SP.FRI_TICKER         = TICKER
    AND   SP.SOURCE_CODE        = SC_CODE
    AND   SP.PRICE_CURRENCY     = CURRENCY
    AND   SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
    AND   SP.DELIVERY_DATE     >  DEL_DATE
    AND   (SP.MID_DATE         <= DEL_DATE
    OR    SP.MID_DATE            IS NULL)
-- Modification - JSL - 06 mai 2014
--  AND    Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
-- Fin de modification - JSL - 06 mai 2014
    FOR UPDATE OF SP.MID_DATE,
                  SP.MID,
                  SP.Mid_Yield,
                  SP.LAST_USER,
                  SP.LAST_STAMP,
                  SP.Mid_Dirty_Price,
                  SP.Mid_Mod_Duration;
--
--    Ce Cursor va mettre un 'lock' sur les rangees necessaires
--    pour faire un carry forward d'un prix
--
CURSOR CUR_GET_CFW_PRICES IS
-- Modification - JSL - 06 mai 2014
--  SELECT PRICE_BUFFER
-- Modification - JSL - 19 novembre 2015
--  SELECT *
    SELECT /*+ INDEX( SP SECURITY_PRICES_X_02) */ 
           *
-- Fin de modification - JSL - 19 novembre 2015
-- Fin de modification - JSL - 06 mai 2014
    FROM Security_Prices SP
    WHERE SP.FRI_TICKER         = TICKER
    AND   SP.SOURCE_CODE        = SC_CODE
    AND   SP.PRICE_CURRENCY     = CURRENCY
    AND   SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
    AND   SP.DELIVERY_DATE     >  DEL_DATE
    AND   (SP.PRICE_DATE       <= DEL_DATE
    OR    SP.PRICE_DATE        IS NULL)
-- Modification - JSL - 06 mai 2014
--  AND    Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
-- Fin de modification - JSL - 06 mai 2014
    FOR UPDATE OF SP.PRICE_DATE,
                  SP.PRICE,
                  SP.VOLUME,
                  SP.HIGH,
                  SP.LOW,
                  SP.OPEN_PRICE,
                  SP.YIELD,
                  SP.BOARD_LOT_FLAG,
                  SP.LAST_TRADE_TIME,
                  SP.NUMBER_OF_TRANS,
                  SP.LAST_USER,
                  SP.LAST_STAMP,
                  SP.Dirty_Price,
                  SP.Price_Mod_Duration,
                  SP.Market_Cap
                , SP.IFRS_Code
                , SP.High_52w
                , SP.Low_52w
                , SP.Official_Close
                , SP.Trade_Count
                , SP.Close_Cumulative_Volume
                , SP.Close_Cumulative_Volume_Status
                , SP.Close_Cumulative_Volume_Date
                ;
--
--    Ce Cursor va mettre un 'lock' sur les rangees necessaire
--    pour faire un Carry Forward d'un taux
--
CURSOR CUR_GET_CFW_EXCHANGES IS
    SELECT EXCH_BUFFER
    FROM EXCHANGE_RATES ER
    WHERE ER.FRI_TICKER         = TICKER
    AND   ER.SOURCE_CODE        = SC_CODE
    AND   ER.PRICE_CURRENCY     = CURRENCY
    AND   ER.DATA_SUPPLIER_CODE = SUPPLIER_CODE
    AND   ER.DELIVERY_DATE     >  DEL_DATE
    AND   ER.PRICE_DATE        <= DEL_DATE
    AND   Pricing.Validate_Supplier_Chain(ER.Data_Supplier_Code,
                               TO_CHAR(DEL_DATE,'HH24MI'),
                               TO_CHAR(ER.Delivery_Date,'HH24MI')) = 'Y'
    FOR UPDATE OF ER.PRICE_DATE,
                  ER.PRICE,
                  ER.ASK,
                  ER.LAST_USER,
                  ER.LAST_STAMP;
--
NO_CFWS    EXCEPTION;
--
BEGIN
--{
--    Carry Forward des taux de change
--
   IF SECURITY_PATTERN = EXCH_CODE THEN
      pgmloc := 1130;
      OPEN CUR_GET_CFW_EXCHANGES;
      FETCH CUR_GET_CFW_EXCHANGES INTO EXCH_BUFFER;
      IF CUR_GET_CFW_EXCHANGES%NOTFOUND THEN
         CLOSE CUR_GET_CFW_EXCHANGES;
         RAISE NO_CFWS;
      END IF;
--
      pgmloc := 1140;
      UPDATE EXCHANGE_RATES ER
         SET PRICE_DATE = ER_VALUES.PRICE_DATE,
             PRICE      = ER_VALUES.PRICE,
             ASK        = ER_VALUES.ASK,
             LAST_USER  = INITIALS,
             LAST_STAMP = SYSDATE
       WHERE ER.FRI_TICKER         = TICKER
         AND ER.SOURCE_CODE        = SC_CODE
         AND ER.PRICE_CURRENCY     = CURRENCY
         AND ER.DATA_SUPPLIER_CODE = SUPPLIER_CODE
         AND ER.DELIVERY_DATE     >  DEL_DATE
         AND Pricing.Validate_Supplier_Chain(
                               ER.Data_Supplier_Code,
                               TO_CHAR(DEL_DATE,'HH24MI'),
                               TO_CHAR(ER.Delivery_Date,'HH24MI')) = 'Y'
         AND ER.PRICE_DATE        <= DEL_DATE
         AND ( ER.PRICE_DATE      != ER_VALUES.PRICE_DATE
            OR NVL(ER.PRICE,-999) != ER_VALUES.PRICE
            OR NVL(ER.ASK  ,-999) != nvl(ER_VALUES.ASK,-999) );
--
       CFW_COUNT := SQL%ROWCOUNT;
       CLOSE CUR_GET_CFW_EXCHANGES;
--
   ELSE
--
--    Carry Forward des ASK
--
      IF CFW_PRICE_TYPE = 'A' THEN
         pgmloc := 1150;
-- Modification - JSL - 06 mai 2014
--       OPEN CUR_GET_CFW_ASKS;
--       FETCH CUR_GET_CFW_ASKS INTO PRICE_BUFFER;
--       IF CUR_GET_CFW_ASKS%NOTFOUND THEN
--          CLOSE CUR_GET_CFW_ASKS;
--          RAISE NO_CFWS;
--       END IF;
--
--       pgmloc := 1160;
--       UPDATE Security_Prices SP
--          SET ASK_DATE          = SP_VALUES.ASK_DATE,
--              ASK               = SP_VALUES.ASK,
--           -- Ajout - JSL - 23 avril 2007
--              ASK_YIELD         = SP_VALUES.ASK_YIELD,
--            --PRICE_ORIGIN_CODE = SP_VALUES.PRICE_ORIGIN_CODE,
--              LAST_USER         = INITIALS,
--              LAST_STAMP        = SYSDATE,
-- Araisbel 25JAN2008 2 New fields
--              ASK_DIRTY_PRICE   = SP_VALUES.ASK_DIRTY_PRICE,
--              ASK_MOD_DURATION  = SP_VALUES.ASK_MOD_DURATION
--        WHERE SP.FRI_TICKER         = TICKER
--          AND SP.SOURCE_CODE        = SC_CODE
--          AND SP.PRICE_CURRENCY     = CURRENCY
--          AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
--          AND SP.DELIVERY_DATE     > DEL_DATE
--          AND Pricing.Validate_Supplier_Chain(
--                             SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
--          AND ( SP.ASK_DATE        <= DEL_DATE
--             OR SP.ASK_DATE        IS NULL )
--         -- ajoute le NVL pour date de null
--         -- Marie (08mars96) req: 3176
--          AND ( to_char(SP.ASK_DATE) !=
--                   NVL(to_char(SP_VALUES.ASK_DATE),CHR(1))
--         -- Ajout - JSL - 23 avril 2007
--             OR NVL(SP.ASK_YIELD,-999) != NVL(SP_VALUES.ASK_YIELD,-999)
--             OR NVL(SP.ASK,-999)     != SP_VALUES.ASK
-- Richard 26JAN2008 2 New fields
--             OR NVL(SP.ASK_DIRTY_PRICE,-999)
--                  != NVL(SP_VALUES.ASK_DIRTY_PRICE,-999)
--             OR NVL(SP.ASK_MOD_DURATION,-999)
--                  != NVL(SP_VALUES.ASK_MOD_DURATION,-999) );
--
--
--       CFW_COUNT := SQL%ROWCOUNT;
--       CLOSE CUR_GET_CFW_ASKS;

         Carry_Fwd_Applied := FALSE;
         CFW_COUNT := 0;
         FOR X IN CUR_GET_CFW_ASKS
         LOOP
           IF Pricing.Validate_Supplier_Chain( X.Data_Supplier_Code
                                             , TO_CHAR( DEL_DATE, C_Time )
                                             , TO_CHAR( X.Delivery_Date, C_Time )
                                             ) = 'Y'
           THEN
             Carry_Fwd_Applied := TRUE;
           ELSE
             GOTO Next_Ask;
           END IF;

           IF to_char(X.ASK_DATE)          != NVL(to_char(SP_VALUES.ASK_DATE),CHR(1))
           OR NVL(X.ASK_YIELD,-999)        != NVL(SP_VALUES.ASK_YIELD,-999)
           OR NVL(X.ASK,-999)              != SP_VALUES.ASK
           OR NVL(X.ASK_DIRTY_PRICE,-999)  != NVL(SP_VALUES.ASK_DIRTY_PRICE,-999)
           OR NVL(X.ASK_MOD_DURATION,-999) != NVL(SP_VALUES.ASK_MOD_DURATION,-999)
           THEN
             NULL;
           ELSE
             GOTO Next_Ask;
           END IF;

           pgmloc := 1160;
           UPDATE Security_Prices SP
              SET ASK_DATE          = SP_VALUES.ASK_DATE,
                  ASK               = SP_VALUES.ASK,
                  ASK_YIELD         = SP_VALUES.ASK_YIELD,
                  LAST_USER         = INITIALS,
                  LAST_STAMP        = SYSDATE,
                  ASK_DIRTY_PRICE   = SP_VALUES.ASK_DIRTY_PRICE,
                  ASK_MOD_DURATION  = SP_VALUES.ASK_MOD_DURATION
            WHERE Current of CUR_GET_CFW_ASKS
                  ;
            CFW_Count := CFW_Count + SQL%ROWCOUNT;
<< Next_Ask >>
           NULL;
         END LOOP;
         IF NOT Carry_Fwd_Applied
         THEN
           RAISE NO_CFWS;
         END IF;
-- Fin de modification - JSL - 06 mai 2014
--
--    Carry Forwards des BID
--
      ELSIF CFW_PRICE_TYPE = 'B' THEN
         pgmloc := 1170;
-- Modification - JSL - 06 mai 2014
--       OPEN CUR_GET_CFW_BIDS;
--       FETCH CUR_GET_CFW_BIDS INTO PRICE_BUFFER;
--       IF CUR_GET_CFW_BIDS%NOTFOUND THEN
--          CLOSE CUR_GET_CFW_BIDS;
--          RAISE NO_CFWS;
--       END IF;
--
--       pgmloc := 1180;
--       UPDATE Security_Prices SP
--          SET BID_DATE          = SP_VALUES.BID_DATE,
--              BID               = SP_VALUES.BID,
--           -- Ajout - JSL - 23 avril 2007
--              BID_YIELD         = SP_VALUES.BID_YIELD,
--            --PRICE_ORIGIN_CODE = SP_VALUES.PRICE_ORIGIN_CODE,
--              LAST_USER         = INITIALS,
--              LAST_STAMP        = SYSDATE,
-- Araisbel 25JAN2008 2 New fields
--              BID_DIRTY_PRICE   = SP_VALUES.BID_DIRTY_PRICE,
--              BID_MOD_DURATION  = SP_VALUES.BID_MOD_DURATION
--        WHERE SP.FRI_TICKER         = TICKER
--          AND SP.SOURCE_CODE        = SC_CODE
--          AND SP.PRICE_CURRENCY     = CURRENCY
--          AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
--          AND SP.DELIVERY_DATE     >  DEL_DATE
--          AND ( SP.BID_DATE        <= DEL_DATE
--             OR SP.BID_DATE        IS NULL)
--          AND Pricing.Validate_Supplier_Chain(
--                             SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
--             -- ajoute le NVL pour date de null
--             -- Marie (08mars96) req: 3176
--          AND ( to_char(SP.BID_DATE) !=
--                NVL(to_char(SP_VALUES.BID_DATE),CHR(1))
--             -- Ajout - JSL - 23 avril 2007
--             OR NVL(SP.BID_YIELD,-999) != NVL(SP_VALUES.BID_YIELD,-999)
--             OR NVL(SP.BID,-999)     != SP_VALUES.BID
-- Richard 26JAN2008 2 New fields
--             OR NVL(SP.BID_DIRTY_PRICE,-999)
--                  != NVL(SP_VALUES.BID_DIRTY_PRICE,-999)
--             OR NVL(SP.BID_MOD_DURATION,-999)
--                  != NVL(SP_VALUES.BID_MOD_DURATION,-999) );
--
--       CFW_COUNT := SQL%ROWCOUNT;
--       CLOSE CUR_GET_CFW_BIDS;

         Carry_Fwd_Applied := FALSE;
         CFW_COUNT := 0;
         FOR X IN CUR_GET_CFW_BIDS
         LOOP
           IF Pricing.Validate_Supplier_Chain( X.Data_Supplier_Code
                                             , TO_CHAR( DEL_DATE, C_Time )
                                             , TO_CHAR( X.Delivery_Date, C_Time )
                                             ) = 'Y'
           THEN
             Carry_Fwd_Applied := TRUE;
           ELSE
             GOTO Next_Bid;
           END IF;

           IF to_char(X.BID_DATE)          != NVL(to_char(SP_VALUES.BID_DATE),CHR(1))
           OR NVL(X.BID_YIELD,-999)        != NVL(SP_VALUES.BID_YIELD,-999)
           OR NVL(X.BID,-999)              != SP_VALUES.BID
           OR NVL(X.BID_DIRTY_PRICE,-999)  != NVL(SP_VALUES.BID_DIRTY_PRICE,-999)
           OR NVL(X.BID_MOD_DURATION,-999) != NVL(SP_VALUES.BID_MOD_DURATION,-999)
           THEN
             NULL;
           ELSE
             GOTO Next_Bid;
           END IF;

           pgmloc := 1160;
           UPDATE Security_Prices SP
              SET BID_DATE          = SP_VALUES.BID_DATE,
                  BID               = SP_VALUES.BID,
                  BID_YIELD         = SP_VALUES.BID_YIELD,
                  LAST_USER         = INITIALS,
                  LAST_STAMP        = SYSDATE,
                  BID_DIRTY_PRICE   = SP_VALUES.BID_DIRTY_PRICE,
                  BID_MOD_DURATION  = SP_VALUES.BID_MOD_DURATION
            WHERE Current of CUR_GET_CFW_BIDS
                  ;
            CFW_Count := CFW_Count + SQL%ROWCOUNT;
<< Next_Bid >>
           NULL;
         END LOOP;
         IF NOT Carry_Fwd_Applied
         THEN
           RAISE NO_CFWS;
         END IF;
-- Fin de modification - JSL - 06 mai 2014
--
--    Carry Forward des MIDS
--
      ELSIF CFW_PRICE_TYPE = 'M' THEN
         pgmloc := 1190;
-- Modification - JSL - 06 mai 2014
--       OPEN CUR_GET_CFW_MIDS;
--       FETCH CUR_GET_CFW_MIDS INTO PRICE_BUFFER;
--       IF CUR_GET_CFW_MIDS%NOTFOUND THEN
--          CLOSE CUR_GET_CFW_MIDS;
--          RAISE NO_CFWS;
--       END IF;
--
--       pgmloc := 1200;
--       UPDATE Security_Prices SP
--          SET MID_DATE          = SP_VALUES.MID_DATE,
--              MID               = SP_VALUES.MID,
--           -- Ajout - JSL - 23 avril 2007
--              MID_YIELD         = SP_VALUES.MID_YIELD,
--            --PRICE_ORIGIN_CODE = SP_VALUES.PRICE_ORIGIN_CODE,
--              LAST_USER         = INITIALS,
--              LAST_STAMP        = SYSDATE,
-- Araisbel 25JAN2008 2 New fields
--              MID_DIRTY_PRICE   = SP_VALUES.MID_DIRTY_PRICE,
--              MID_MOD_DURATION  = SP_VALUES.MID_MOD_DURATION
--        WHERE SP.FRI_TICKER         = TICKER
--          AND SP.SOURCE_CODE        = SC_CODE
--          AND SP.PRICE_CURRENCY     = CURRENCY
--          AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
--          AND SP.DELIVERY_DATE     >  DEL_DATE
--          AND ( SP.MID_DATE        <= DEL_DATE
--             OR SP.MID_DATE        IS NULL)
--          AND Pricing.Validate_Supplier_Chain(
--                             SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
--             -- ajoute le NVL pour date de null
--             -- Marie (08mars96) req: 3176
--          AND ( to_char(SP.MID_DATE) !=
--                   NVL(to_char(SP_VALUES.MID_DATE),CHR(1))
--             -- Ajout - JSL - 23 avril 2007
--             OR NVL(SP.MID_YIELD,-999) != NVL(SP_VALUES.MID_YIELD,-999)
--             OR NVL(SP.MID,-999)     != SP_VALUES.MID
-- Richard 26JAN2008 2 New fields
--             OR NVL(SP.MID_DIRTY_PRICE,-999)
--                  != NVL(SP_VALUES.MID_DIRTY_PRICE,-999)
--             OR NVL(SP.MID_MOD_DURATION,-999)
--                  != NVL(SP_VALUES.MID_MOD_DURATION,-999) );
--
--       CFW_COUNT := SQL%ROWCOUNT;
--       CLOSE CUR_GET_CFW_MIDS;

         Carry_Fwd_Applied := FALSE;
         CFW_COUNT := 0;
         FOR X IN CUR_GET_CFW_MIDS
         LOOP
           IF Pricing.Validate_Supplier_Chain( X.Data_Supplier_Code
                                             , TO_CHAR( DEL_DATE, C_Time )
                                             , TO_CHAR( X.Delivery_Date, C_Time )
                                             ) = 'Y'
           THEN
             Carry_Fwd_Applied := TRUE;
           ELSE
             GOTO Next_Mid;
           END IF;

           IF to_char(X.MID_DATE)          != NVL(to_char(SP_VALUES.MID_DATE),CHR(1))
           OR NVL(X.MID_YIELD,-999)        != NVL(SP_VALUES.MID_YIELD,-999)
           OR NVL(X.MID,-999)              != SP_VALUES.MID
           OR NVL(X.MID_DIRTY_PRICE,-999)  != NVL(SP_VALUES.MID_DIRTY_PRICE,-999)
           OR NVL(X.MID_MOD_DURATION,-999) != NVL(SP_VALUES.MID_MOD_DURATION,-999)
           THEN
             NULL;
           ELSE
             GOTO Next_Mid;
           END IF;

           pgmloc := 1160;
           UPDATE Security_Prices SP
              SET MID_DATE          = SP_VALUES.MID_DATE,
                  MID               = SP_VALUES.MID,
                  MID_YIELD         = SP_VALUES.MID_YIELD,
                  LAST_USER         = INITIALS,
                  LAST_STAMP        = SYSDATE,
                  MID_DIRTY_PRICE   = SP_VALUES.MID_DIRTY_PRICE,
                  MID_MOD_DURATION  = SP_VALUES.MID_MOD_DURATION
            WHERE Current of CUR_GET_CFW_MIDS
                  ;
            CFW_Count := CFW_Count + SQL%ROWCOUNT;
<< Next_Mid >>
           NULL;
         END LOOP;
         IF NOT Carry_Fwd_Applied
         THEN
           RAISE NO_CFWS;
         END IF;
-- Fin de modification - JSL - 06 mai 2014
--
--    Carry Forward des Prix
--
      ELSIF CFW_PRICE_TYPE = 'P' THEN
         pgmloc := 1210;
-- Modification - JSL - 06 mai 2014
--       OPEN CUR_GET_CFW_PRICES;
--       FETCH CUR_GET_CFW_PRICES INTO PRICE_BUFFER;
--       IF CUR_GET_CFW_PRICES%NOTFOUND THEN
--          CLOSE CUR_GET_CFW_PRICES;
--          RAISE NO_CFWS;
--       END IF;
--
--         pgmloc := 1220;
--         UPDATE Security_Prices SP
--            SET PRICE_DATE         = SP_VALUES.PRICE_DATE,
--                PRICE              = SP_VALUES.PRICE,
--                VOLUME             = SP_VALUES.VOLUME,
--                HIGH               = SP_VALUES.HIGH,
--                LOW                = SP_VALUES.LOW,
--                OPEN_PRICE         = SP_VALUES.OPEN_PRICE,
--                YIELD              = SP_VALUES.YIELD,
--                BOARD_LOT_FLAG     = SP_VALUES.BOARD_LOT_FLAG,
--                LAST_TRADE_TIME    = SP_VALUES.LAST_TRADE_TIME,
--                NUMBER_OF_TRANS    = SP_VALUES.NUMBER_OF_TRANS,
--              --PRICE_ORIGIN_CODE  = SP_VALUES.PRICE_ORIGIN_CODE,
--                LAST_USER          = INITIALS,
--                LAST_STAMP         = SYSDATE,
---- Araisbel 25JAN2008 2 New fields - Fixed by Richard (6Jan2008)
--                DIRTY_PRICE        = SP_VALUES.DIRTY_PRICE,
--                PRICE_MOD_DURATION = SP_VALUES.PRICE_MOD_DURATION,
--                -- Richard (19Oct2009) - Market cap
--                MARKET_CAP         = SP_VALUES.MARKET_CAP
---- Ajout - JSL - 12 janvier 2010
--              , IFRS_Code          = SP_VALUES.IFRS_Code
---- Fin d'ajout - JSL - 12 janvier 2010
---- Ajout - RV  - 09 Aout 2011
--              , High_52w           = SP_VALUES.High_52W
--              , Low_52w            = SP_VALUES.Low_52W
---- Fin d'ajout - 09 Aout 2011
---- Ajout - JSL - 29 novembre 2011
--              , Official_Close     = SP_VALUES.Official_Close
--              , Trade_Count        = SP_VALUES.Trade_Count
--              , Close_Cumulative_Volume       =SP_VALUES.Close_Cumulative_Volume
--              , Close_Cumulative_Volume_Status=SP_VALUES.Close_Cumulative_Volume_Status
--              , Close_Cumulative_Volume_Date  =SP_VALUES.Close_Cumulative_Volume_Date
---- Fin d'ajout - JSL - 29 novembre 2011
--          WHERE SP.FRI_TICKER         = TICKER
--            AND SP.SOURCE_CODE        = SC_CODE
--            AND SP.PRICE_CURRENCY     = CURRENCY
--            AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
--            AND SP.DELIVERY_DATE     >  DEL_DATE
--            AND ( SP.PRICE_DATE      <= DEL_DATE
--               OR SP.PRICE_DATE      IS NULL)
--            AND Pricing.Validate_Supplier_Chain(
--                               SP.Data_Supplier_Code,
--                               TO_CHAR(DEL_DATE,'HH24MI'),
--                               TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
--           -- ajoute le NVL pour date de null
--             -- Marie (08mars96) req: 3176
--            AND ( to_char(SP.PRICE_DATE) !=
--                     NVL(to_char(SP_VALUES.PRICE_DATE),CHR(1))
--               OR NVL(SP.PRICE,-999)      != SP_VALUES.PRICE
--             -- ajoute NVL pour les champs NULL
--             -- Marie (12juin1996)
--               OR NVL(SP.VOLUME,-999)
--                    != NVL(SP_VALUES.VOLUME,-999)
--               OR NVL(SP.HIGH,-999)
--                    != NVL(SP_VALUES.HIGH,-999)
--               OR NVL(SP.LOW,-999)
--                    != NVL(SP_VALUES.LOW,-999)
--               OR NVL(SP.OPEN_PRICE,-999)
--                    != NVL(SP_VALUES.OPEN_PRICE,-999)
--               OR NVL(SP.YIELD,-999)
--                    != NVL(SP_VALUES.YIELD,-999)
--               OR NVL(SP.BOARD_LOT_FLAG,CHR(1))
--                    != NVL(SP_VALUES.BOARD_LOT_FLAG,CHR(1))
--               OR NVL(SP.NUMBER_OF_TRANS,-999)
--                    != NVL(SP_VALUES.NUMBER_OF_TRANS,-999)
--               OR NVL(SP.LAST_TRADE_TIME,-999)
--                    != NVL(SP_VALUES.LAST_TRADE_TIME,-999)
---- Richard 26JAN2008 2 New fields
--               OR NVL(SP.DIRTY_PRICE,-999)
--                    != NVL(SP_VALUES.DIRTY_PRICE,-999)
--               OR NVL(SP.PRICE_MOD_DURATION,-999)
--                    != NVL(SP_VALUES.PRICE_MOD_DURATION,-999)
---- Richard (19Oct2009) - Market cap
--               OR NVL(SP.MARKET_CAP,-999)
--                    != NVL(SP_VALUES.MARKET_CAP,-999)
---- Ajout - JSL - 12 janvier 2010
--               OR NVL(SP.IFRS_Code, 'NOT AVAILABLE' )
--                    != NVL(SP_VALUES.IFRS_Code, 'NOT AVAILABLE' )
---- Fin d'ajout - JSL - 12 janvier 2010
---- Ajout - RV  - 09 Aout 2011
--               OR NVL(SP.High_52w,-999)
--                    != NVL(SP_VALUES.High_52w,-999)
--               OR NVL(SP.Low_52w,-999)
--                    != NVL(SP_VALUES.Low_52w,-999)
---- Fin d'ajout - 09 Aout 2011
---- Ajout - JSL - 29 novembre 2011
--               OR NVL(SP.Official_Close,-999)
--                    != NVL(SP_VALUES.Official_Close,-999)
--               OR NVL( SP.Trade_Count,-999)
--                    != NVL( SP_VALUES.Trade_Count,-999)
--               OR NVL( SP.Close_Cumulative_Volume,-999)
--                    != NVL( SP_VALUES.Close_Cumulative_Volume,-999)
--               OR NVL( SP.Close_Cumulative_Volume_Status,-999)  
--                    != NVL( SP_VALUES.Close_Cumulative_Volume_Status,-999)
--               OR NVL( TO_CHAR(SP.Close_Cumulative_Volume_Date),CHR(1))
--                    != NVL( TO_CHAR(SP_VALUES.Close_Cumulative_Volume_Date),CHR(1))
---- Fin d'ajout - JSL - 29 novembre 2011
--               );
----
--         CFW_COUNT := SQL%ROWCOUNT;
--         CLOSE CUR_GET_CFW_PRICES;

         Carry_Fwd_Applied := FALSE;
         CFW_COUNT := 0;
         FOR X IN CUR_GET_CFW_Prices
         LOOP
           IF Pricing.Validate_Supplier_Chain( X.Data_Supplier_Code
                                             , TO_CHAR( DEL_DATE, C_Time )
                                             , TO_CHAR( X.Delivery_Date, C_Time )
                                             ) = 'Y'
           THEN
             Carry_Fwd_Applied := TRUE;
           ELSE
             GOTO Next_Prices;
           END IF;

           IF to_char(X.PRICE_DATE)               != NVL(to_char(SP_VALUES.PRICE_DATE),CHR(1))
           OR NVL(X.PRICE,-999)                   != SP_VALUES.PRICE
           OR NVL(X.VOLUME,-999)                  != NVL(SP_VALUES.VOLUME,-999)
           OR NVL(X.HIGH,-999)                    != NVL(SP_VALUES.HIGH,-999)
           OR NVL(X.LOW,-999)                     != NVL(SP_VALUES.LOW,-999)
           OR NVL(X.OPEN_PRICE,-999)              != NVL(SP_VALUES.OPEN_PRICE,-999)
           OR NVL(X.YIELD,-999)                   != NVL(SP_VALUES.YIELD,-999)
           OR NVL(X.BOARD_LOT_FLAG,CHR(1))        != NVL(SP_VALUES.BOARD_LOT_FLAG,CHR(1))
           OR NVL(X.NUMBER_OF_TRANS,-999)         != NVL(SP_VALUES.NUMBER_OF_TRANS,-999)
           OR NVL(X.LAST_TRADE_TIME,-999)         != NVL(SP_VALUES.LAST_TRADE_TIME,-999)
           OR NVL(X.DIRTY_PRICE,-999)             != NVL(SP_VALUES.DIRTY_PRICE,-999)
           OR NVL(X.PRICE_MOD_DURATION,-999)      != NVL(SP_VALUES.PRICE_MOD_DURATION,-999)
           OR NVL(X.MARKET_CAP,-999)              != NVL(SP_VALUES.MARKET_CAP,-999)
           OR NVL(X.IFRS_Code, 'NOT AVAILABLE' )  != NVL(SP_VALUES.IFRS_Code, 'NOT AVAILABLE' )
           OR NVL(X.High_52w,-999)                != NVL(SP_VALUES.High_52w,-999)
           OR NVL(X.Low_52w,-999)                 != NVL(SP_VALUES.Low_52w,-999)
           OR NVL(X.Official_Close,-999)          != NVL(SP_VALUES.Official_Close,-999)
           OR NVL(X.Trade_Count,-999)             != NVL( SP_VALUES.Trade_Count,-999)
           OR NVL(X.Close_Cumulative_Volume,-999) != NVL( SP_VALUES.Close_Cumulative_Volume,-999)
           OR NVL(X.Close_Cumulative_Volume_Status,-999)  
                    != NVL( SP_VALUES.Close_Cumulative_Volume_Status,-999)
           OR NVL( TO_CHAR(X.Close_Cumulative_Volume_Date),CHR(1))
                    != NVL( TO_CHAR(SP_VALUES.Close_Cumulative_Volume_Date),CHR(1))
           THEN
             NULL;
           ELSE
             GOTO Next_Prices;
           END IF;

           pgmloc := 1160;
           UPDATE Security_Prices SP
              SET PRICE_DATE         = SP_VALUES.PRICE_DATE,
                  PRICE              = SP_VALUES.PRICE,
                  VOLUME             = SP_VALUES.VOLUME,
                  HIGH               = SP_VALUES.HIGH,
                  LOW                = SP_VALUES.LOW,
                  OPEN_PRICE         = SP_VALUES.OPEN_PRICE,
                  YIELD              = SP_VALUES.YIELD,
                  BOARD_LOT_FLAG     = SP_VALUES.BOARD_LOT_FLAG,
                  LAST_TRADE_TIME    = SP_VALUES.LAST_TRADE_TIME,
                  NUMBER_OF_TRANS    = SP_VALUES.NUMBER_OF_TRANS,
                  LAST_USER          = INITIALS,
                  LAST_STAMP         = SYSDATE,
                  DIRTY_PRICE        = SP_VALUES.DIRTY_PRICE,
                  PRICE_MOD_DURATION = SP_VALUES.PRICE_MOD_DURATION,
                  MARKET_CAP         = SP_VALUES.MARKET_CAP
                , IFRS_Code          = SP_VALUES.IFRS_Code
                , High_52w           = SP_VALUES.High_52W
                , Low_52w            = SP_VALUES.Low_52W
                , Official_Close     = SP_VALUES.Official_Close
                , Trade_Count        = SP_VALUES.Trade_Count
                , Close_Cumulative_Volume       =SP_VALUES.Close_Cumulative_Volume
                , Close_Cumulative_Volume_Status=SP_VALUES.Close_Cumulative_Volume_Status
                , Close_Cumulative_Volume_Date  =SP_VALUES.Close_Cumulative_Volume_Date
            WHERE Current of CUR_GET_CFW_Prices
                  ;
            CFW_Count := CFW_Count + SQL%ROWCOUNT;
<< Next_Prices >>
           NULL;
         END LOOP;
         IF NOT Carry_Fwd_Applied
         THEN
           RAISE NO_CFWS;
         END IF;
-- Fin de modification - JSL - 06 mai 2014
      END IF;
   END IF;
   STATUS := 0 ; -- succes
--
EXCEPTION
WHEN NO_CFWS  THEN
   CFW_COUNT := 0;
   STATUS    := 0;
--
WHEN OTHERS THEN
   STATUS := 2;
   CFW_COUNT := 0;
   RAISE;
--}}
END CFW_PRICE_OR_EXCH_APPLY;
-----------------------------------------------------------------------
--
--    La Procedure CFW_REV_PRICE_OR_EXCH_APPLY va executer les Reverse Carry
--    Forwards de Ask, Bid, Mid, Price, et taux de change dependant du
--    CFW_TYPE.
--
PROCEDURE CFW_REV_PRICE_OR_EXCH_APPLY IS
--{
    PRICE_BUFFER       CHAR(255);
    EXCH_BUFFER        CHAR(255);
--
-- Ajout - JSL - 06 mai 2014
    Carry_Fwd_Applied  BOOLEAN;
-- Fin d'ajout - JSL - 06 mai 2014
--
--    Ce cursor va mettre un 'lock' sur les rangees necessaires
--    pour faire un carry forward d'un ASK
--
CURSOR  CUR_GET_CFW_ASKS IS
-- Modification - JSL - 06 mai 2014
--  SELECT PRICE_BUFFER
    --mlz - 05nov2015
    --SELECT *
    SELECT /*+ index(sp security_prices_x_02) */ *
    --endmlz - 05nov2015
-- Fin de modification - JSL - 06 mai 2014
      FROM Security_Prices SP
     WHERE SP.FRI_TICKER         = TICKER
       AND SP.SOURCE_CODE        = SC_CODE
       AND SP.PRICE_CURRENCY     = CURRENCY
       AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
       AND SP.DELIVERY_DATE     <  DEL_DATE
-- Modification - JSL - 06 mai 2014
--     AND Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
-- Fin de modification - JSL - 06 mai 2014
       AND SP.DELIVERY_DATE     >= P_Temp_Date
       AND SP.ASK_DATE          <= SP_VALUES.ASK_DATE
    FOR UPDATE OF SP.ASK_DATE,
                  SP.ASK,
                  SP.ASK_YIELD,
                  SP.LAST_USER,
                  SP.LAST_STAMP,
                  SP.ASK_DIRTY_PRICE,
                  SP.ASK_MOD_DURATION;
--
--    Ce cursor va metter un 'lock' sur les rangees necessaires
--    pour faire un carry forward d'un BID
--
CURSOR CUR_GET_CFW_BIDS IS
-- Modification - JSL - 06 mai 2014
--  SELECT PRICE_BUFFER
    --mlz - 05nov2015
    --SELECT *
    SELECT /*+ index(sp security_prices_x_02) */ *
    --endmlz - 05nov2015
-- Fin de modification - JSL - 06 mai 2014
      FROM Security_Prices SP
     WHERE SP.FRI_TICKER         = TICKER
       AND SP.SOURCE_CODE        = SC_CODE
       AND SP.PRICE_CURRENCY     = CURRENCY
       AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
       AND SP.DELIVERY_DATE     <  DEL_DATE
       AND SP.DELIVERY_DATE     >= P_Temp_Date
       AND SP.BID_DATE          <= SP_VALUES.BID_DATE
-- Modification - JSL - 06 mai 2014
--     AND Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
-- Fin de modification - JSL - 06 mai 2014
    FOR UPDATE OF SP.BID_DATE,
                  SP.BID,
                  SP.BID_YIELD,
                  SP.LAST_USER,
                  SP.LAST_STAMP,
                  SP.BID_DIRTY_PRICE,
                  SP.BID_MOD_DURATION;
--
--    Ce cursor va mettre un 'lock' sur les rangees necessaires
--    pour faire un carry forward d'un MID
--
CURSOR CUR_GET_CFW_MIDS IS
-- Modification - JSL - 06 mai 2014
--  SELECT PRICE_BUFFER
    --mlz - 05nov2015
    --SELECT *
    SELECT /*+ index(sp security_prices_x_02) */ *
    --endmlz - 05nov2015
-- Fin de modification - JSL - 06 mai 2014
      FROM Security_Prices SP
     WHERE SP.FRI_TICKER         = TICKER
       AND SP.SOURCE_CODE        = SC_CODE
       AND SP.PRICE_CURRENCY     = CURRENCY
       AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
       AND SP.DELIVERY_DATE     <  DEL_DATE
       AND SP.DELIVERY_DATE     >= P_Temp_Date
       AND SP.MID_DATE          <= SP_VALUES.MID_DATE
-- Modification - JSL - 06 mai 2014
--     AND  Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
-- Fin de modification - JSL - 06 mai 2014
    FOR UPDATE OF SP.MID_DATE,
                  SP.MID,
                  SP.MID_YIELD,
                  SP.LAST_USER,
                  SP.LAST_STAMP,
                  SP.MID_DIRTY_PRICE,
                  SP.MID_MOD_DURATION;
--
--    Ce Cursor va mettre un 'lock' sur les rangees necessaires
--    pour faire un carry forward d'un prix
--
CURSOR CUR_GET_CFW_PRICES IS
-- Modification - JSL - 06 mai 2014
--  SELECT PRICE_BUFFER
    --mlz - 05nov2015
    --SELECT *
    SELECT /*+ index(sp security_prices_x_02) */ *
    --endmlz - 05nov2015
-- Fin de modification - JSL - 06 mai 2014
      FROM Security_Prices SP
     WHERE SP.FRI_TICKER         = TICKER
       AND SP.SOURCE_CODE        = SC_CODE
       AND SP.PRICE_CURRENCY     = CURRENCY
       AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
       AND SP.DELIVERY_DATE     <  DEL_DATE
       AND SP.DELIVERY_DATE     >= P_Temp_Date
       AND SP.PRICE_DATE        <= SP_VALUES.PRICE_DATE
-- Modification - JSL - 06 mai 2014
--     AND Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
--                             TO_CHAR(DEL_DATE,'HH24MI'),
--                             TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
-- Fin de modification - JSL - 06 mai 2014
    FOR UPDATE OF SP.PRICE_DATE,
                  SP.PRICE,
                  SP.VOLUME,
                  SP.HIGH,
                  SP.LOW,
                  SP.OPEN_PRICE,
                  SP.YIELD,
                  SP.BOARD_LOT_FLAG,
                  SP.LAST_TRADE_TIME,
                  SP.NUMBER_OF_TRANS,
                  SP.LAST_USER,
                  SP.LAST_STAMP,
                  SP.DIRTY_PRICE,
                  SP.PRICE_MOD_DURATION,
                  SP.MARKET_CAP
                , SP.IFRS_Code
                , SP.High_52w
                , SP.Low_52w
                , Official_Close 
                , Trade_Count  
                , Close_Cumulative_Volume
                , Close_Cumulative_Volume_Status
                , Close_Cumulative_Volume_Date
                ;
--
--    Ce Cursor va mettre un 'lock' sur les rangees necessaire
--    pour faire un Carry Forward d'un taux
--
CURSOR CUR_GET_CFW_EXCHANGES IS
    SELECT EXCH_BUFFER
      FROM EXCHANGE_RATES ER
     WHERE ER.FRI_TICKER         = TICKER
       AND ER.SOURCE_CODE        = SC_CODE
       AND ER.PRICE_CURRENCY     = CURRENCY
       AND ER.DATA_SUPPLIER_CODE = SUPPLIER_CODE
       AND ER.DELIVERY_DATE     <  DEL_DATE
       AND ER.DELIVERY_DATE     >= P_Temp_Date
       AND ER.PRICE_DATE        <= ER_VALUES.PRICE_DATE
       AND Pricing.Validate_Supplier_Chain(ER.Data_Supplier_Code,
                               TO_CHAR(DEL_DATE,'HH24MI'),
                               TO_CHAR(ER.Delivery_Date,'HH24MI')) = 'Y'
    FOR UPDATE OF ER.PRICE_DATE,
                  ER.PRICE,
                  ER.ASK,
                  ER.LAST_USER,
                  ER.LAST_STAMP;
--
NO_CFWS    EXCEPTION;
--
BEGIN
--{
--    Carry Forward des taux de change
--
   IF SECURITY_PATTERN = EXCH_CODE THEN
      pgmloc := 1230;
      OPEN CUR_GET_CFW_EXCHANGES;
      FETCH CUR_GET_CFW_EXCHANGES INTO EXCH_BUFFER;
      IF CUR_GET_CFW_EXCHANGES%NOTFOUND THEN
         CLOSE CUR_GET_CFW_EXCHANGES;
         RAISE NO_CFWS;
      END IF;
--
      pgmloc := 1240;
      UPDATE EXCHANGE_RATES ER
         SET PRICE_DATE =  ER_VALUES.PRICE_DATE,
             PRICE      =  ER_VALUES.PRICE,
             ASK        =  ER_VALUES.ASK,
             LAST_USER  =  INITIALS,
             LAST_STAMP =  SYSDATE
       WHERE ER.FRI_TICKER         = TICKER
         AND ER.SOURCE_CODE        = SC_CODE
         AND ER.PRICE_CURRENCY     = CURRENCY
         AND ER.DATA_SUPPLIER_CODE = SUPPLIER_CODE
         AND ER.DELIVERY_DATE     <  DEL_DATE
         AND ER.DELIVERY_DATE     >= P_Temp_Date
         AND Pricing.Validate_Supplier_Chain(
                              ER.Data_Supplier_Code,
                              TO_CHAR(DEL_DATE,'HH24MI'),
                              TO_CHAR(ER.Delivery_Date,'HH24MI')) = 'Y'
         AND ER.PRICE_DATE        <= ER_VALUES.PRICE_DATE
         AND ( ER.PRICE_DATE      != ER_VALUES.PRICE_DATE
            OR NVL(ER.PRICE,-999) != ER_VALUES.PRICE
            OR NVL(ER.ASK  ,-999) != NVL(ER_VALUES.ASK,-999 ));
--
      CFW_COUNT := SQL%ROWCOUNT;
      CLOSE CUR_GET_CFW_EXCHANGES;
--
   ELSE
--
--    Carry Forward des ASK
--
      IF CFW_PRICE_TYPE = 'A' THEN
         pgmloc := 1250;
-- Modification - JSL - 06 mai 2014
--       OPEN CUR_GET_CFW_ASKS;
--       FETCH CUR_GET_CFW_ASKS INTO PRICE_BUFFER;
--       IF CUR_GET_CFW_ASKS%NOTFOUND THEN
--          CLOSE CUR_GET_CFW_ASKS;
--          RAISE NO_CFWS;
--       END IF;
--       --
--       pgmloc := 1260;
--       UPDATE Security_Prices SP
--          SET ASK_DATE         = SP_VALUES.ASK_DATE,
--              ASK              = SP_VALUES.ASK,
--           -- Ajout - JSL - 23 avril 2007
--              ASK_YIELD        = SP_VALUES.ASK_YIELD,
--              LAST_USER        = INITIALS,
--              LAST_STAMP       = SYSDATE,
-- Araisbel 25JAN2008 2 New fields
--              ASK_DIRTY_PRICE  = SP_VALUES.ASK_DIRTY_PRICE,
--              ASK_MOD_DURATION = SP_VALUES.ASK_MOD_DURATION
--        WHERE SP.FRI_TICKER         = TICKER
--          AND SP.SOURCE_CODE        = SC_CODE
--          AND SP.PRICE_CURRENCY     = CURRENCY
--          AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
--          AND SP.DELIVERY_DATE     <  DEL_DATE
--          AND Pricing.Validate_Supplier_Chain(
--                                SP.Data_Supplier_Code,
--                                TO_CHAR(DEL_DATE,'HH24MI'),
--                                TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
--          AND SP.DELIVERY_DATE     >= P_Temp_Date
--          AND SP.ASK_DATE          <= SP_VALUES.ASK_DATE;
--       --
--       CFW_COUNT := SQL%ROWCOUNT;
--       CLOSE CUR_GET_CFW_ASKS;

         Carry_Fwd_Applied := FALSE;
         CFW_COUNT := 0;
         FOR X IN CUR_GET_CFW_ASKS
         LOOP
           IF Pricing.Validate_Supplier_Chain( X.Data_Supplier_Code
                                             , TO_CHAR( DEL_DATE, C_Time )
                                             , TO_CHAR( X.Delivery_Date, C_Time )
                                             ) = 'Y'
           THEN
             Carry_Fwd_Applied := TRUE;
           ELSE
             GOTO Next_Ask;
           END IF;

           pgmloc := 1160;
           UPDATE Security_Prices SP
              SET ASK_DATE          = SP_VALUES.ASK_DATE,
                  ASK               = SP_VALUES.ASK,
                  ASK_YIELD         = SP_VALUES.ASK_YIELD,
                  LAST_USER         = INITIALS,
                  LAST_STAMP        = SYSDATE,
                  ASK_DIRTY_PRICE   = SP_VALUES.ASK_DIRTY_PRICE,
                  ASK_MOD_DURATION  = SP_VALUES.ASK_MOD_DURATION
            WHERE Current of CUR_GET_CFW_ASKS
                  ;
            CFW_Count := CFW_Count + SQL%ROWCOUNT;
<< Next_Ask >>
           NULL;
         END LOOP;
         IF NOT Carry_Fwd_Applied
         THEN
           RAISE NO_CFWS;
         END IF;
-- Fin de modification - JSL - 06 mai 2014
         --
--    Carry Forwards des BID
--
      ELSIF CFW_PRICE_TYPE = 'B' THEN
         pgmloc := 1270;
-- Modification - JSL - 06 mai 2014
--       OPEN CUR_GET_CFW_BIDS;
--       FETCH CUR_GET_CFW_BIDS INTO PRICE_BUFFER;
--       IF CUR_GET_CFW_BIDS%NOTFOUND THEN
--          CLOSE CUR_GET_CFW_BIDS;
--          RAISE NO_CFWS;
--       END IF;
--       --
--       pgmloc := 1280;
--       UPDATE Security_Prices SP
--          SET BID_DATE   = SP_VALUES.BID_DATE,
--              BID        = SP_VALUES.BID,
--           -- Ajout - JSL - 23 avril 2007
--              BID_YIELD  = SP_VALUES.BID_YIELD,
--              LAST_USER  = INITIALS,
--              LAST_STAMP = SYSDATE,
-- Araisbel 25JAN2008 2 New fields
--              BID_DIRTY_PRICE  = SP_VALUES.BID_DIRTY_PRICE,
--              BID_MOD_DURATION = SP_VALUES.BID_MOD_DURATION
--        WHERE SP.FRI_TICKER         = TICKER
--          AND SP.SOURCE_CODE        = SC_CODE
--          AND SP.PRICE_CURRENCY     = CURRENCY
--          AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
--          AND SP.DELIVERY_DATE     <  DEL_DATE
--          AND Pricing.Validate_Supplier_Chain(
--                                SP.Data_Supplier_Code,
--                                TO_CHAR(DEL_DATE,'HH24MI'),
--                                TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
--          AND SP.DELIVERY_DATE     >= P_Temp_Date
--          AND SP.BID_DATE          <= SP_VALUES.BID_DATE;
--       --
--       CFW_COUNT := SQL%ROWCOUNT;
--       CLOSE CUR_GET_CFW_BIDS;

         Carry_Fwd_Applied := FALSE;
         CFW_COUNT := 0;
         FOR X IN CUR_GET_CFW_BIDS
         LOOP
           IF Pricing.Validate_Supplier_Chain( X.Data_Supplier_Code
                                             , TO_CHAR( DEL_DATE, C_Time )
                                             , TO_CHAR( X.Delivery_Date, C_Time )
                                             ) = 'Y'
           THEN
             Carry_Fwd_Applied := TRUE;
           ELSE
             GOTO Next_Bid;
           END IF;

           pgmloc := 1160;
           UPDATE Security_Prices SP
              SET BID_DATE          = SP_VALUES.BID_DATE,
                  BID               = SP_VALUES.BID,
                  BID_YIELD         = SP_VALUES.BID_YIELD,
                  LAST_USER         = INITIALS,
                  LAST_STAMP        = SYSDATE,
                  BID_DIRTY_PRICE   = SP_VALUES.BID_DIRTY_PRICE,
                  BID_MOD_DURATION  = SP_VALUES.BID_MOD_DURATION
            WHERE Current of CUR_GET_CFW_BIDS
                  ;
            CFW_Count := CFW_Count + SQL%ROWCOUNT;
<< Next_Bid >>
           NULL;
         END LOOP;
         IF NOT Carry_Fwd_Applied
         THEN
           RAISE NO_CFWS;
         END IF;
-- Fin de modification - JSL - 06 mai 2014
         --
      --    Carry Forward des MIDS
      --
      ELSIF CFW_PRICE_TYPE = 'M' THEN
         pgmloc := 1290;
-- Modification - JSL - 06 mai 2014
--       OPEN CUR_GET_CFW_MIDS;
--       FETCH CUR_GET_CFW_MIDS INTO PRICE_BUFFER;
--       IF CUR_GET_CFW_MIDS%NOTFOUND THEN
--          CLOSE CUR_GET_CFW_MIDS;
--          RAISE NO_CFWS;
--       END IF;
--       --
--       pgmloc := 1300;
--       UPDATE Security_Prices SP
--          SET MID_DATE   = SP_VALUES.MID_DATE,
--              MID        = SP_VALUES.MID,
--           -- Ajout - JSL - 23 avril 2007
--              MID_YIELD  = SP_VALUES.MID_YIELD,
--              LAST_USER  = INITIALS,
--              LAST_STAMP = SYSDATE,
-- Araisbel 25JAN2008 2 New fields
--              MID_DIRTY_PRICE  = SP_VALUES.MID_DIRTY_PRICE,
--              MID_MOD_DURATION = SP_VALUES.MID_MOD_DURATION
--        WHERE SP.FRI_TICKER         = TICKER
--          AND SP.SOURCE_CODE        = SC_CODE
--          AND SP.PRICE_CURRENCY     = CURRENCY
--          AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
--          AND SP.DELIVERY_DATE     <  DEL_DATE
--          AND Pricing.Validate_Supplier_Chain(
--                                SP.Data_Supplier_Code,
--                                TO_CHAR(DEL_DATE,'HH24MI'),
--                                TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
--          AND SP.DELIVERY_DATE     >= P_Temp_Date
--          AND SP.MID_DATE          <= SP_VALUES.MID_DATE;
--       --
--       CFW_COUNT := SQL%ROWCOUNT;
--       CLOSE CUR_GET_CFW_MIDS;

         Carry_Fwd_Applied := FALSE;
         CFW_COUNT := 0;
         FOR X IN CUR_GET_CFW_MIDS
         LOOP
           IF Pricing.Validate_Supplier_Chain( X.Data_Supplier_Code
                                             , TO_CHAR( DEL_DATE, C_Time )
                                             , TO_CHAR( X.Delivery_Date, C_Time )
                                             ) = 'Y'
           THEN
             Carry_Fwd_Applied := TRUE;
           ELSE
             GOTO Next_Mid;
           END IF;

           pgmloc := 1160;
           UPDATE Security_Prices SP
              SET MID_DATE          = SP_VALUES.MID_DATE,
                  MID               = SP_VALUES.MID,
                  MID_YIELD         = SP_VALUES.MID_YIELD,
                  LAST_USER         = INITIALS,
                  LAST_STAMP        = SYSDATE,
                  MID_DIRTY_PRICE   = SP_VALUES.MID_DIRTY_PRICE,
                  MID_MOD_DURATION  = SP_VALUES.MID_MOD_DURATION
            WHERE Current of CUR_GET_CFW_MIDS
                  ;
            CFW_Count := CFW_Count + SQL%ROWCOUNT;
<< Next_Mid >>
           NULL;
         END LOOP;
         IF NOT Carry_Fwd_Applied
         THEN
           RAISE NO_CFWS;
         END IF;
-- Fin de modification - JSL - 06 mai 2014
         --
      --    Carry Forward des Prix
      --
      ELSIF CFW_PRICE_TYPE = 'P' THEN
         pgmloc := 1310;
-- Modification - JSL - 06 mai 2014
--       OPEN CUR_GET_CFW_PRICES;
--       FETCH CUR_GET_CFW_PRICES INTO PRICE_BUFFER;
--       IF CUR_GET_CFW_PRICES%NOTFOUND THEN
--          CLOSE CUR_GET_CFW_PRICES;
--          RAISE NO_CFWS;
--       END IF;
--       --
--       pgmloc := 1320;
--       UPDATE Security_Prices SP
--          SET PRICE_DATE         = SP_VALUES.PRICE_DATE,
--              PRICE              = SP_VALUES.PRICE,
--              VOLUME             = SP_VALUES.VOLUME,
--              HIGH               = SP_VALUES.HIGH,
--              LOW                = SP_VALUES.LOW,
--              OPEN_PRICE         = SP_VALUES.OPEN_PRICE,
--              YIELD              = SP_VALUES.YIELD,
--              BOARD_LOT_FLAG     = SP_VALUES.BOARD_LOT_FLAG,
--              LAST_TRADE_TIME    = SP_VALUES.LAST_TRADE_TIME,
--              NUMBER_OF_TRANS    = SP_VALUES.NUMBER_OF_TRANS,
--           -- Price Origin was not updated
--           -- For now, only IFIC,Fundserv are using that
--           -- Concept (Richard - 15 Mar 2004)
--              Price_Origin_Code  = 'C',
--           -- End of Modif (RV - 15 Mar 2004)
--              LAST_USER          = INITIALS,
--              LAST_STAMP         = SYSDATE,
-- Araisbel 25JAN2008 2 New fields
--              DIRTY_PRICE        = SP_VALUES.DIRTY_PRICE,
--              PRICE_MOD_DURATION = SP_VALUES.PRICE_MOD_DURATION,
-- Richard (19Oct2009) - Market cap
--              MARKET_CAP         = SP_VALUES.MARKET_CAP
-- Ajout - JSL - 12 janvier 2010
--            , IFRS_Code          = SP_VALUES.IFRS_Code
-- Fin d'ajout - JSL - 12 janvier 2010
-- Ajout - RV  - 09 Aout 2011
--            , High_52w           = SP_VALUES.High_52w
--            , Low_52w            = SP_VALUES.Low_52w
-- Fin d'ajout - 09 Aout 2011
-- Ajout - JSL - 29 novembre 2011
--            , Official_Close     = SP_VALUES.Official_Close
--            , Trade_Count        = SP_VALUES.Trade_Count
--            , Close_Cumulative_Volume       =SP_VALUES.Close_Cumulative_Volume
--            , Close_Cumulative_Volume_Status=SP_VALUES.Close_Cumulative_Volume_Status
--            , Close_Cumulative_Volume_Date  =SP_VALUES.Close_Cumulative_Volume_Date
-- Fin d'ajout - JSL - 29 novembre 2011
--        WHERE SP.FRI_TICKER         = TICKER
--          AND SP.SOURCE_CODE        = SC_CODE
--          AND SP.PRICE_CURRENCY     = CURRENCY
--          AND SP.DATA_SUPPLIER_CODE = SUPPLIER_CODE
--          AND SP.DELIVERY_DATE     <  DEL_DATE
--          AND Pricing.Validate_Supplier_Chain(
--                                SP.Data_Supplier_Code,
--                                TO_CHAR(DEL_DATE,'HH24MI'),
--                                TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
--          AND SP.DELIVERY_DATE     >= P_Temp_Date
--          AND SP.PRICE_DATE        <= SP_VALUES.PRICE_DATE;
--       --
--       CFW_COUNT := SQL%ROWCOUNT;
--       CLOSE CUR_GET_CFW_PRICES;

         Carry_Fwd_Applied := FALSE;
         CFW_COUNT := 0;
         FOR X IN CUR_GET_CFW_Prices
         LOOP
           IF Pricing.Validate_Supplier_Chain( X.Data_Supplier_Code
                                             , TO_CHAR( DEL_DATE, C_Time )
                                             , TO_CHAR( X.Delivery_Date, C_Time )
                                             ) = 'Y'
           THEN
             Carry_Fwd_Applied := TRUE;
           ELSE
             GOTO Next_Prices;
           END IF;

           pgmloc := 1160;
           UPDATE Security_Prices SP
              SET PRICE_DATE                     = SP_VALUES.PRICE_DATE
                , PRICE                          = SP_VALUES.PRICE
                , VOLUME                         = SP_VALUES.VOLUME
                , HIGH                           = SP_VALUES.HIGH
                , LOW                            = SP_VALUES.LOW
                , OPEN_PRICE                     = SP_VALUES.OPEN_PRICE
                , YIELD                          = SP_VALUES.YIELD
                , BOARD_LOT_FLAG                 = SP_VALUES.BOARD_LOT_FLAG
                , LAST_TRADE_TIME                = SP_VALUES.LAST_TRADE_TIME
                , NUMBER_OF_TRANS                = SP_VALUES.NUMBER_OF_TRANS
                , Price_Origin_Code              = 'C'
                , LAST_USER                      = INITIALS
                , LAST_STAMP                     = SYSDATE
                , DIRTY_PRICE                    = SP_VALUES.DIRTY_PRICE
                , PRICE_MOD_DURATION             = SP_VALUES.PRICE_MOD_DURATION
                , MARKET_CAP                     = SP_VALUES.MARKET_CAP
                , IFRS_Code                      = SP_VALUES.IFRS_Code
                , High_52w                       = SP_VALUES.High_52w
                , Low_52w                        = SP_VALUES.Low_52w
                , Official_Close                 = SP_VALUES.Official_Close
                , Trade_Count                    = SP_VALUES.Trade_Count
                , Close_Cumulative_Volume        = SP_VALUES.Close_Cumulative_Volume
                , Close_Cumulative_Volume_Status = SP_VALUES.Close_Cumulative_Volume_Status
                , Close_Cumulative_Volume_Date   = SP_VALUES.Close_Cumulative_Volume_Date
            WHERE Current of CUR_GET_CFW_Prices
                  ;
            CFW_Count := CFW_Count + SQL%ROWCOUNT;
<< Next_Prices >>
           NULL;
         END LOOP;
         IF NOT Carry_Fwd_Applied
         THEN
           RAISE NO_CFWS;
         END IF;
-- Fin de modification - JSL - 06 mai 2014
      END IF;
   END IF;
   STATUS := 0 ; -- succes
--
EXCEPTION
WHEN NO_CFWS  THEN
   CFW_COUNT := 0;
   STATUS    := 4;
--
WHEN OTHERS THEN
   STATUS := 2;
   CFW_COUNT := 0;
   RAISE;
--}}
END CFW_REV_PRICE_OR_EXCH_APPLY;
------------------------------------------------------------------------
--
--  Cette Procedure sert a faire les Carry Forwards des Courbes de
--  Rendement: Yield_Input_By_Months et Yield_Input_By_Dates
--
PROCEDURE CFW_YIELD_CURVES
       (P_DATA_SUPPLIER_CODE   IN  DATA_SUPPLIERS.CODE%TYPE,
        P_DELIVERY_DATE        IN  DATE,
        P_DELIVERY_TIME        IN  VARCHAR2,
        P_COMMIT_COUNT         IN  NUMBER,
        P_RESTART_FLAG         IN  VARCHAR2,
        P_RESTART_DATE         IN  DATE     := NULL,
        P_FROM_SCREEN          IN  VARCHAR2 := NULL)
IS
--{
-- Declaration de variables
--
Max_Delivery_Date          DATE;
temp_pgm_id                OPS_Scripts.Program_Id%TYPE;
Handled_Raise              BOOLEAN := FALSE;
--
CURSOR Cur_Get_Cfw_Date_Mth
IS
   SELECT MAX(Delivery_Date)
     FROM Yield_Input_By_Months YI,
          Yield_Curves          YC
    WHERE YI.Yield_Curve_Code    = YC.Code
      AND Delivery_Date          < P_Restart_Date
      AND YC.Data_Supplier_Code  =  P_Data_Supplier_Code;
--
CURSOR Cur_Get_Cfw_Date_Dt
IS
   SELECT MAX(Delivery_Date)
     FROM Yield_Input_By_Dates  YI,
          Yield_Curves          YC
    WHERE YI.Yield_Curve_Code    = YC.Code
      AND Delivery_Date          < P_Restart_Date
      AND YC.Data_Supplier_Code  =  P_Data_Supplier_Code;
--
CURSOR Cur_Rows_Before_Restart_Mth (
   P_Restart_Date       DATE,
   P_Data_Supplier_Code Data_Suppliers.Code%TYPE)
IS
  SELECT 'X'
    FROM Yield_Input_By_Months YI,
         Yield_Curves          YC,
         Data_Supply_Schedule DSS
   WHERE YI.Delivery_Date        > P_Restart_Date
     AND YI.Yield_Curve_Code     = YC.Code
     AND YC.Data_Supplier_Code   = P_Data_Supplier_Code
     AND DSS.Data_Supplier_Code  =  YC.Data_Supplier_Code
     AND DSS.Delivery_Time       =  to_char(YI.delivery_date,'hh24mi')
     AND DSS.Delivery_Time       <> P_Delivery_Time
     AND DSS.CF_Delivery_Time    =  P_Delivery_Time
  UNION
  SELECT DISTINCT 'X'
    FROM Yield_Input_By_Months YI,
         Yield_Curves          YC,
         Data_Supply_Schedule DSS
   WHERE YI.Delivery_Date       >  (P_Restart_Date+1)
     AND YI.Yield_Curve_Code    = YC.Code
     AND YC.Data_Supplier_Code   = P_Data_Supplier_Code
     AND DSS.Data_Supplier_Code =  YC.Data_Supplier_Code
     AND DSS.Delivery_Time      =  to_char(YI.delivery_date,'hh24mi')
     AND DSS.CF_Delivery_Time   =  P_Delivery_Time;
--
CURSOR Cur_Rows_Before_Restart_Dt (
   P_Restart_Date       DATE,
   P_Data_Supplier_Code Data_Suppliers.Code%TYPE)
IS
  SELECT 'X'
    FROM Yield_Input_By_Dates  YI,
         Yield_Curves          YC,
         Data_Supply_Schedule DSS
   WHERE YI.Delivery_Date        > P_Restart_Date
     AND YI.Yield_Curve_Code     = YC.Code
     AND YC.Data_Supplier_Code   = P_Data_Supplier_Code
     AND DSS.Data_Supplier_Code  =  YC.Data_Supplier_Code
     AND DSS.Delivery_Time       =  to_char(YI.delivery_date,'hh24mi')
     AND DSS.Delivery_Time       <> P_Delivery_Time
     AND DSS.CF_Delivery_Time    =  P_Delivery_Time
  UNION
  SELECT DISTINCT 'X'
    FROM Yield_Input_By_Dates  YI,
         Yield_Curves          YC,
         Data_Supply_Schedule DSS
   WHERE YI.Delivery_Date       >  (P_Restart_Date+1)
     AND YI.Yield_Curve_Code    = YC.Code
     AND YC.Data_Supplier_Code   = P_Data_Supplier_Code
     AND DSS.Data_Supplier_Code =  YC.Data_Supplier_Code
     AND DSS.Delivery_Time      =  to_char(YI.delivery_date,'hh24mi')
     AND DSS.CF_Delivery_Time   =  P_Delivery_Time;
--
-- The Successfull_Flag = Y must be the one to be fetched
-- This is the only known success in Oasis
--
CURSOR Cur_Previous_CFW_Done
IS
   SELECT PM.Successfull_Flag
     FROM Pricing_Msgs      PM
    WHERE PM.Program_Id       = Temp_Pgm_Id
      AND PM.Date_of_Prices   = TRUNC(Max_Delivery_Date)
      AND PM.Date_Of_Msg      =
          (SELECT MAX(PM2.Date_of_Msg)
           FROM  Pricing_Msgs      PM2,
                 Successfull_Flags SF2
           WHERE PM2.Program_id       = PM.Program_id
            AND  PM2.Date_of_Prices   = PM.Date_of_Prices
            AND  PM2.Successfull_Flag = SF2.Code
            AND  SF2.Not_Run_Yet_Flag = 'N' );
--
-- Cursor Pour aller chercher les rangees a Carry Forward par mois
--
CURSOR Cur_Cfws_By_Month IS
      SELECT    YI.Yield_Curve_Code,
                YI.Month_Number,
                YI.Yield,
                YI.ASK_Yield
        FROM    Yield_Input_By_Months  YI,
                Yield_Known_Months     YKM,
                Yield_Curves           YC
       WHERE    YKM.Yield_Curve_Code   = YI.Yield_Curve_Code
         AND    YKM.Month_Number       = YI.Month_Number
         AND    YC.Code                = YI.Yield_Curve_Code
         AND    YC.Data_Supplier_Code  = P_Data_Supplier_Code
     -- Carrying Forward Active Curves Only
         AND    YC.Active_Flag         = 'Y'
         AND    YI.Delivery_Date       = Max_Delivery_Date
      Order by YI.Yield_Curve_Code,
               YI.Delivery_Date,
               YI.Month_Number;
--
-- Cursor Pour aller chercher les rangees a Carry Forward par dates
-- Pour les rangee ou le premier yield date est plus petit
-- que le p_delivery_date
--
CURSOR Cur_Cfws_By_Dates IS
      SELECT    YI.Yield_Curve_Code,
                YI.Delivery_Date,
                YI.Yield_Date,
                YI.Yield,
                YI.ASK_Yield
               ,YC.YCS_Flag
        FROM    Yield_Input_By_Dates   YI,
                Yield_Curves           YC
       WHERE    YC.Code                = YI.Yield_Curve_Code
         AND    YC.Data_Supplier_Code  = P_Data_Supplier_Code
         AND    YC.Active_Flag         = 'Y'
         AND    YI.Delivery_Date       = Max_Delivery_Date
      Order by YI.Yield_Curve_Code,
               YI.Delivery_Date,
               YI.Yield_Date;
--
-- Ce Cursor va aller verifier si des mois ont ete rajoutes dans
-- Yield_Known_Months, si oui il faudra faire un CFW avec un rendement
-- de 0
--
CURSOR Cur_Get_Other_Months IS
      SELECT  YKM.Yield_Curve_Code,
              YKM.Month_Number
        FROM  Yield_Known_Months YKM,
              Yield_Curves       YC
       WHERE  YC.Active_Flag         = 'Y'
         AND  YC.Code                = YKM.Yield_Curve_Code
         AND  YC.Data_Supplier_Code  =  P_Data_Supplier_Code
         AND  NOT EXISTS
              (
                Select 'X'
                FROM   Yield_Input_By_Months  YI2
                WHERE  YI2.Yield_Curve_Code   = YKM.Yield_Curve_Code
                  AND  YI2.Month_Number       = YKM.Month_Number
                  -- Months that were inexistant on previous site date
                  -- will be Inserted
                  AND  TRUNC(YI2.Delivery_Date) =
                                    Constants.Get_Previous_Site_Date
              );
--
-- Ce cursor va verifier si le yield date d'une rangee specifiee
-- dans Yield_Input_By_Dates est la premiere ou non
--
CURSOR Cur_Get_Min_Yield_Date(P_Code   Yield_Curves.Code%TYPE,
                              P_Date   DATE) IS
   SELECT MIN(Yield_Date)
     FROM Yield_Input_By_Dates IY
    WHERE IY.Yield_Curve_Code = P_Code
      AND IY.Delivery_Date    = P_Date;
--
-- Ce Cursor va aller chercher la deuxieme yield_date.  Ceci
-- est necessaire pour calculer le nombre de mois entre la
-- premiere et la deuxieme date pour former la nouvelle yield_date
-- pour la rangee Carry Forward
--
CURSOR Cur_Get_Second_Yield_Date(P_Code       Yield_Curves.Code%TYPE,
                                 P_Date       DATE,
                                 P_Yield_Date DATE) IS
   SELECT MIN(Yield_Date)
     FROM Yield_Input_By_Dates IY
    WHERE IY.Yield_Curve_Code = P_Code
      AND IY.Delivery_Date    = P_Date
      AND IY.Yield_Date       > P_Yield_Date;
--
-- Declaration des Variables
--
User_Id                    User_List.User_id%TYPE;
Curr_Program_Id            Ops_Scripts.Program_Id%TYPE;
Database_Code              Databases.Code%TYPE;
Database_Mode_Code         Database_Modes.Code%TYPE;
Min_Yield_Date             DATE;
Second_Yield_Date          DATE;
V_Delivery_Date            DATE;
V_CF_Delivery_Time         Data_Supply_Schedule.Delivery_Time%TYPE;
Old_Break_Status           Break_Points.Status%TYPE;
Last_Yield_Curve_Code      Yield_Curves.Code%TYPE := NULL;
--
Rows_Read                  NUMBER := 0 ;
Rows_Inserted              NUMBER := 0;
Rows_Rejected              NUMBER := 0;
Rows_Updated               NUMBER := 0;
Rows_Deleted               NUMBER := 0;
Records_Committed          NUMBER := 0;
--
pgmloc                     NUMBER;
Success                    VARCHAR2(1) := 'Y';
End_Of_Loop                BOOLEAN     := FALSE;
Mths_to_Add                NUMBER := 0;
Yield_Data                 Cur_Cfws_By_Month%ROWTYPE;
Yield_Data_Dt              Cur_Cfws_By_Dates%ROWTYPE;
Month_Data                 Cur_Get_Other_Months%ROWTYPE;
New_Yield_Date             DATE;
temp_first_run_date        DATE;
Restart_Date               DATE := NULL;
CFW_Date                   DATE := NULL;
temp_succ_flag             pricing_Msgs.Successfull_Flag%TYPE;
Temp_Buffer                CHAR(255);
Invalid_Price_Date         EXCEPTION;
No_Data                       EXCEPTION;
Bad_Previous_CF            EXCEPTION;
Cannot_Restart             EXCEPTION;
--
V_Prev_Curve               Yield_Curves.Code%TYPE;
--
BEGIN
--{
-- Va chercher le Program_id, si il est NULL ou que le data Supply
-- Code et Delivery Time ne sont pas valide, l'exception No_Data_Found
-- sera levee automatiquement
--
   pgmloc := 1330;
   SELECT SRP.Program_Id,
          DSS.CF_Delivery_Time
   INTO   Curr_Program_Id,
          V_CF_Delivery_Time
   FROM   Supplier_Running_Parms SRP ,
          Data_Supply_Schedule   DSS
   WHERE  SRP.Data_Supplier_Code  =  P_Data_Supplier_Code
   AND    SRP.Data_Supplier_Code  =  DSS.Data_Supplier_Code
   AND    SRP.Delivery_Time       =  DSS.Delivery_Time
   AND    SRP.Product_Code        =  'CFW'
   AND    SRP.Delivery_Time       =  P_Delivery_Time;
   --
   pgmloc := 1340;
   --
   -- Utilise package Constants
   User_Id       := Constants.Get_User_Id;
   G_Program_Id  := Curr_Program_Id;
   G_System_Date := SYSDATE;
   --
   Global_File_Updates.Initialize_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   Pricing_Msg_Rec.Msg_Text :=
     'Carry Forwards for Yields for ' || P_Data_Supplier_Code ||
      ', ' || TO_CHAR(P_Delivery_Date,'DD-MON-YYYY')  || ' '  ||
      P_Delivery_Time;
   --
   Pricing_Msg_Rec.Msg_Text_2       := 'Start of Program';
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   --
   IF P_Restart_Flag = 'Y'
   THEN
      Pricing_Msg_Rec.Date_of_Prices := P_Restart_Date;
   END IF;
   --
   Global_File_Updates.Update_Pricing_Msgs
      (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   COMMIT;
--
-- Cherche le dernier status de Break Points
--
   pgmloc := 1350;
   SELECT Status
   INTO   Old_Break_Status
   FROM   BREAK_POINTS BP
   WHERE  BP.Program_Id  = G_Program_Id;
--
   pgmloc := 1360;
--
   pgmloc := 1370;
--
-- si date n'est pas egal au last price date, on doit interrompre
-- l'execution
--
   IF Constants.G_Site_Date <> TRUNC(P_Delivery_Date)
   THEN
      Pricing_Msg_Rec.Err_Text   :=
        'Invalid Date Passed In.  Date Must be Equal ' ||
        'to the date in New Day';
      Pricing_Msg_Rec.Msg_Text_2 := 'Error while validating parameters';
      RAISE Invalid_Price_Date;
   END IF;
--
-- Ajout - JSL - 14 juillet 2015
-- Est-ce que l'on doit accepter un drp?
   Unchain_Flag := 'N';
   Pgmloc := 642;
   OPEN  Get_DRP_Info( P_Data_Supplier_Code );
   Pgmloc := 643;
   FETCH Get_DRP_Info INTO Unchain_Flag;
   Pgmloc := 644;
   CLOSE Get_DRP_Info;
-- Fin d'ajout - JSL - 14 juillet 2015
   --
   --  On set le V_Delivery_Date
   --
   V_Delivery_Date := TO_DATE(TO_CHAR(P_Delivery_Date,'DD-MON-YYYY') ||
                              ' '                                    ||
                              P_Delivery_Time,'DD-MON-YYYY HH24MI');
   --
   -- modifie pour utiliser le previous_price_date de Site_Controls
   -- au lieu de faire un max
   --
   IF P_Restart_Flag = 'N'
   THEN
      Restart_Date:= V_Delivery_Date;
      --
-- Ajout - JSL - 14 juillet 2015
      IF  Unchain_Flag = 'Y'
      AND ( Holiday.Is_It_Open( p_delivery_date, 'HOLCLASS', null, 'FRI' ) IS NOT NULL
         OR Holiday.Is_It_Open( p_delivery_date, 'SUPPLIER', P_Data_Supplier_Code, P_Delivery_Time ) IS NOT NULL
          )
      THEN
         Max_Delivery_Date :=
            to_date(to_char(Constants.G_Previous_Site_Date,'DD-MON-YYYY') ||
              ' ' || P_Delivery_Time,'DD-MON-YYYY HH24MI');
      ELSE
-- Fin d'ajout - JSL - 14 juillet 2015
         IF V_CF_Delivery_Time < P_Delivery_Time
         THEN
            Max_Delivery_Date :=
               to_date(to_char(Constants.G_Site_Date,'DD-MON-YYYY') ||
                 ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
         ELSE
               Max_Delivery_Date :=
                  to_date(to_char(Constants.G_Previous_Site_Date,'DD-MON-YYYY') ||
                    ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
         END IF;
-- Ajout - JSL - 14 juillet 2015
      END IF;
-- Fin d'ajout - JSL - 14 juillet 2015
      --
   ELSE
      --
      IF P_Restart_Date = Constants.G_Site_Date
      THEN
         CFW_Date := Constants.G_Previous_Site_Date;
      ELSE
         OPEN Cur_Get_Cfw_Date_Mth;
         FETCH Cur_Get_Cfw_Date_Mth INTO CFW_Date;
         CLOSE Cur_Get_Cfw_Date_Mth;
         --
         IF CFW_Date IS NULL
         THEN
            OPEN Cur_Get_Cfw_Date_Dt;
            FETCH Cur_Get_Cfw_Date_Dt INTO CFW_Date;
            CLOSE Cur_Get_Cfw_Date_Dt;
         END IF;
      END IF;
      --
      Restart_Date :=
        TO_DATE(TO_CHAR(P_Restart_Date,'DD-MON-YYYY')  || ' ' ||
        P_Delivery_Time,'DD-MON-YYYY HH24MI');
      --
-- Ajout - JSL - 14 juillet 2015
      IF  Unchain_Flag = 'Y'
      AND ( Holiday.Is_It_Open( p_delivery_date, 'HOLCLASS', null, 'FRI' ) IS NOT NULL
         OR Holiday.Is_It_Open( p_delivery_date, 'SUPPLIER', P_Data_Supplier_Code, P_Delivery_Time ) IS NOT NULL
          )
      THEN
         Max_Delivery_Date :=
            to_date(to_char(CFW_Date,'DD-MON-YYYY') ||
              ' ' || P_Delivery_Time,'DD-MON-YYYY HH24MI');
      ELSE
-- Fin d'ajout - JSL - 14 juillet 2015
         IF V_CF_Delivery_Time < P_Delivery_Time
         THEN
            Max_Delivery_Date :=
               to_date(to_char(P_Restart_Date,'DD-MON-YYYY') ||
               ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
         ELSE
            Max_Delivery_Date :=
               to_date(to_char(CFW_Date,'DD-MON-YYYY') ||
               ' ' || V_CF_Delivery_Time,'DD-MON-YYYY HH24MI');
         END IF;
-- Ajout - JSL - 14 juillet 2015
      END IF;
-- Fin d'ajout - JSL - 14 juillet 2015
   END IF;
   pgmloc := 1380;
--
   --
   -- Nous devons maintenant s'assurer que le CFWD de la date
   -- Max_Delivery_Date a ete execute
   --
   pgmloc := 1390;
   SELECT SRP.Program_Id , SRP.First_Run_Date
     INTO Temp_Pgm_Id, Temp_First_Run_date
     FROM Supplier_Running_Parms SRP
    WHERE SRP.Data_Supplier_Code = P_Data_Supplier_Code
      AND SRP.Delivery_Time      = V_CF_Delivery_Time
      AND SRP.Product_Code       = 'CFW';
  --
   IF Temp_First_Run_Date != P_Delivery_Date THEN
      OPEN Cur_Previous_CFW_Done;
      FETCH Cur_Previous_CFW_Done INTO Temp_Succ_Flag;
      IF Cur_Previous_CFW_Done%FOUND
      THEN
         CLOSE Cur_Previous_CFW_Done;
         IF Temp_Succ_Flag != 'Y'
         THEN
            Raise Bad_Previous_cf;
         END IF;
      ELSE
         CLOSE Cur_Previous_CFW_Done;
         Raise Bad_Previous_cf;
      END IF;
   END IF;
   pgmloc := 1400;
   --
   -- On verifie si c'est un restart, si oui,on enleve les rangees
   --
   pgmloc := 1410;
   END_OF_LOOP := FALSE ;
   --
   IF P_Restart_Flag = 'Y' OR Old_Break_Status = 'FRESTART'
   OR Old_Break_Status = 'PRESTART'
   THEN
      IF P_Restart_Flag = 'Y'
      THEN
         --
         -- Nous devons s'assurer qu'il n'y a plus de rangee
         -- dans Yield_Input_By_Months plus recente que le restart date
         --
         OPEN Cur_Rows_Before_Restart_Mth(Restart_Date,
                                          P_Data_Supplier_Code);
         FETCH Cur_Rows_Before_Restart_Mth INTO Temp_Buffer;
         IF Cur_Rows_Before_Restart_Mth%FOUND
         THEN
            CLOSE Cur_Rows_Before_Restart_Mth;
            Pricing_Msg_Rec.Err_Text:=
              'Cannot Restart Carry Fwd,Yields exist past restart date';
            RAISE Cannot_Restart;
         END IF;
         CLOSE Cur_Rows_Before_Restart_Mth;
      ELSE
         Restart_Date :=
             TO_DATE(TO_CHAR(Constants.G_Site_Date,'DD-MON-YYYY')  ||
             ' ' || P_Delivery_Time,'DD-MON-YYYY HH24MI');
      END IF;
      --
      WHILE NOT (end_of_loop) LOOP
         DELETE FROM Yield_Input_By_Months YI
         WHERE  Delivery_Date = Restart_Date
           AND  exists(
                  SELECT 'X'
                  FROM  Yield_Curves          YC
                  WHERE YC.Data_Supplier_Code = P_Data_Supplier_Code
                    AND YI.Yield_Curve_Code   = YC.Code)
           AND  ROWNUM  <= P_Commit_Count;
--
         Rows_Deleted := Rows_Deleted + SQL%ROWCOUNT;
         IF SQL%ROWCOUNT < P_Commit_Count THEN
            end_of_loop := TRUE;
         END IF;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Global_File_Updates.Update_Pricing_Msgs( Pricing_Msg_Rec.Program_Id
                                                , Pricing_Msg_Rec.Date_of_Msg
                                                , FALSE
                                                , Pricing_Msg_Rec
                                                );
         COMMIT;
      END LOOP;
   END IF;
--
   pgmloc := 1420;
   END_OF_LOOP := FALSE ;
   IF P_Restart_Flag = 'Y' OR Old_Break_Status = 'FRESTART'
   OR Old_Break_Status = 'PRESTART'
   THEN
      IF P_Restart_Flag = 'Y'
      THEN
         --
         -- Nous devons s'assurer qu'il n'y a plus de rangee
         -- dans Yield_Input_By_Dates plus recente que le restart date
         --
         OPEN Cur_Rows_Before_Restart_Dt(Restart_Date,
                                         P_Data_Supplier_Code);
         FETCH Cur_Rows_Before_Restart_Dt INTO Temp_Buffer;
         IF Cur_Rows_Before_Restart_Dt%FOUND
         THEN
            CLOSE Cur_Rows_Before_Restart_Dt;
            Pricing_Msg_Rec.Err_Text:=
              'Cannot Restart Carry Fwd,Yields exist past restart date';
            RAISE Cannot_Restart;
         END IF;
         CLOSE Cur_Rows_Before_Restart_Dt;
      ELSE
         Restart_Date :=
            TO_DATE(TO_CHAR(Constants.G_Site_Date,'DD-MON-YYYY')  ||
            ' ' || P_Delivery_Time,'DD-MON-YYYY HH24MI');
      END IF;
      --
      WHILE NOT (end_of_loop) LOOP
         DELETE FROM Yield_Input_By_Dates YI
         WHERE  Delivery_Date = Restart_Date
           AND  exists(
                  SELECT 'X'
                  FROM  Yield_Curves           YC
                  WHERE YI.Yield_Curve_Code   = YC.Code
                  AND   YC.Data_Supplier_Code  = P_Data_Supplier_Code)
           AND    ROWNUM                       <= P_Commit_Count;
--
         Rows_Deleted := Rows_Deleted + SQL%ROWCOUNT;
         IF SQL%ROWCOUNT < P_Commit_Count THEN
            end_of_loop := TRUE;
         END IF;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Global_File_Updates.Update_Pricing_Msgs( Pricing_Msg_Rec.Program_Id
                                                , Pricing_Msg_Rec.Date_of_Msg
                                                , FALSE
                                                , Pricing_Msg_Rec
                                                );
         COMMIT;
      END LOOP;
--
      LOOP
         DELETE FROM Yield_Curves_Price_Dates PD
         WHERE  Delivery_Date = Restart_Date
           AND  exists(
                  SELECT 'X'
                  FROM  Yield_Curves           YC
                  WHERE PD.Yield_Curve_Code   = YC.Code
                  AND   YC.Data_Supplier_Code  = P_Data_Supplier_Code)
           AND    ROWNUM                       <= P_Commit_Count;
--
         EXIT WHEN SQL%ROWCOUNT = 0;
         Rows_Deleted := Rows_Deleted + SQL%ROWCOUNT;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Global_File_Updates.Update_Pricing_Msgs( Pricing_Msg_Rec.Program_Id
                                                , Pricing_Msg_Rec.Date_of_Msg
                                                , FALSE
                                                , Pricing_Msg_Rec
                                                );
         COMMIT;
      END LOOP;
--
      LOOP
         DELETE FROM yield_curves_not_loaded
         WHERE DATA_SUPPLIER_CODE = P_DATA_SUPPLIER_CODE
           AND   DELIVERY_DATE = Restart_Date
           AND    ROWNUM <= P_Commit_Count;
--
         EXIT WHEN SQL%ROWCOUNT = 0;
         Rows_Deleted := Rows_Deleted + SQL%ROWCOUNT;
         Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
         Global_File_Updates.Update_Pricing_Msgs( Pricing_Msg_Rec.Program_Id
                                                , Pricing_Msg_Rec.Date_of_Msg
                                                , FALSE
                                                , Pricing_Msg_Rec
                                                );
         COMMIT;
      END LOOP;
   END IF;
--
-- On va a travers les rangees selectionnees de Yield_Input_By_Months
--
   pgmloc := 1430;
--
   FOR Yield_Data IN Cur_Cfws_By_Month  LOOP
--
      Rows_Read := Rows_Read + 1;
      INSERT INTO Yield_Input_By_Months
                  (Yield_Curve_Code,
                   Delivery_Date,
                   Month_Number,
                   Yield,
                   Ask_Yield,
                   Updated_Flag,
                   Last_User,
                   Last_Stamp)
      SELECT       Yield_Data.Yield_Curve_Code,
                   Restart_Date,
                   Yield_Data.Month_Number,
                   Yield_Data.Yield,
                   Yield_Data.Ask_Yield,
                   'N',
                   Constants.Get_User_Id,
                   SYSDATE
        FROM       Dual
       WHERE       NOT EXISTS
                   (
                    SELECT  'X'
                    FROM    Yield_Input_By_Months YI
                    WHERE   Yield_Data.Yield_Curve_Code =
                            YI.Yield_Curve_Code
                      AND   Yield_Data.Month_Number     =
                            YI.Month_Number
                      AND   YI.Delivery_Date            =
                            Restart_Date
                    );
--
      IF SQL%ROWCOUNT > 0
      THEN
         Rows_Inserted        := Rows_Inserted+SQL%ROWCOUNT;
         pgmloc := 1440;
         Records_Committed    :=  Records_Committed + SQL%ROWCOUNT;
         IF Records_Committed >= P_Commit_Count
         THEN
            Pricing_Msg_Rec.Value_01 :=  Rows_Read;
            Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
            Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
            Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
            --
            Global_File_Updates.Update_Pricing_Msgs
              (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
            COMMIT;
            Records_Committed := 0;
         END IF;
      ELSE
         pgmloc := 1450;
         Rows_Rejected        := Rows_Rejected + 1;
      END IF;
   END LOOP;
--
   pgmloc := 1460;
--
-- on va chercher si il y a des nouvelles rangees dans Yield_Known_
-- Months
--
   FOR Month_Data IN Cur_Get_Other_Months  LOOP
--
      Rows_Read := Rows_Read + 1;
--
      INSERT INTO Yield_Input_By_Months
                  (Yield_Curve_Code,
                   Delivery_Date,
                   Month_Number,
                   Yield,
                   Ask_Yield,
                   Updated_Flag,
                   Last_User,
                   Last_Stamp)
      SELECT       Month_Data.Yield_Curve_Code,
                   Restart_Date,
                   Month_Data.Month_Number,
                   0,
                   0,
                   'N',
                   Constants.Get_User_Id,
                   SYSDATE
        FROM       Dual
       WHERE       NOT EXISTS
                   (
                    SELECT  'X'
                    FROM    Yield_Input_By_Months YI
                    WHERE   Month_Data.Yield_Curve_Code =
                            YI.Yield_Curve_Code
                      AND   Month_Data.Month_Number     =
                            YI.Month_Number
                      AND   YI.Delivery_Date            =
                            Restart_Date
                    );
--
      IF SQL%ROWCOUNT > 0
      THEN
         Rows_Inserted         := Rows_Inserted+SQL%ROWCOUNT;
         pgmloc := 1470;
         Records_Committed     :=  Records_Committed + SQL%ROWCOUNT;
         IF Records_Committed  >= P_Commit_Count
          THEN
             pgmloc := 1480;
             Pricing_Msg_Rec.Value_01 :=  Rows_Read;
             Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
             Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
             Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
             Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
             --
             Global_File_Updates.Update_Pricing_Msgs
                (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
             --
             COMMIT;
             Records_Committed := 0;
         END IF;
      ELSE
         pgmloc := 1490;
         Rows_Rejected        := Rows_Rejected + 1;
      END IF;
   END LOOP;
--
-- On selectionne les rangee de Yield_Input_By_Dates
--
   V_Prev_Curve := NULL;
   FOR Yield_Data_Dt IN Cur_Cfws_By_Dates  LOOP
--
      Rows_Read := Rows_Read + 1;
--
-- on doit verifier si la yield_date est la premiere, si oui
-- on doit verifier si elle est plus petite que le V_Delivery_Date
-- si oui la difference (nombre de mois) entre la premiere et
-- la deuxieme date doit etre ajoute au yield date pour former le
-- yield date de la nouvelle rangee (le Carry Forward)
--
      pgmloc := 1520;
      New_Yield_Date:=Yield_Data_Dt.Yield_Date;
      --
      -- Le Carry Fwd se fait seulement dans le cas que le Yield_Date
      -- est dans le futur (ou le present)
      --
      IF  New_Yield_Date >= TRUNC(Restart_Date)
      THEN
         INSERT INTO Yield_Input_By_Dates
                     (Yield_Curve_Code,
                      Delivery_Date,
                      Yield_Date,
                      Yield,
                      Ask_Yield,
                      Updated_Flag,
                      Last_User,
                      Last_Stamp)
         SELECT       Yield_Data_Dt.Yield_Curve_Code,
                      Restart_Date,
                      New_Yield_Date,
                      Yield_Data_Dt.Yield,
                      Yield_Data_Dt.Ask_Yield,
                      'N',
                      Constants.Get_User_Id,
                      SYSDATE
           FROM       Dual
          WHERE       NOT EXISTS
                      (
                       SELECT  'X'
                       FROM    Yield_Input_By_Dates YI
                          WHERE   Yield_Data_Dt.Yield_Curve_Code =
                               YI.Yield_Curve_Code
                         AND   New_Yield_Date                 =
                               YI.Yield_Date
                         AND   YI.Delivery_Date               =
                               Restart_Date
                       );
--
         IF SQL%ROWCOUNT > 0
         THEN
            Rows_Inserted     := Rows_Inserted+SQL%ROWCOUNT;
            pgmloc := 1530;
            Records_Committed :=  Records_Committed + SQL%ROWCOUNT;
            IF Records_Committed      >= P_Commit_Count
            THEN
               pgmloc := 1540;
               Pricing_Msg_Rec.Value_01 :=  Rows_Read;
               Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
               Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
               Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
               Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
               --
               Global_File_Updates.Update_Pricing_Msgs
                  (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
               --
               COMMIT;
               Records_Committed := 0;
            END IF;
         ELSE
            Rows_Rejected        := Rows_Rejected + 1;
         END IF;
      ELSE
         pgmloc := 1550;
         Rows_Rejected        := Rows_Rejected + 1;
      END IF;
--
-- CFW curve in Yield_Curves_Price_Dates
      IF  Yield_Data_Dt.YCS_Flag = 'Y' AND
          (NVL(V_Prev_Curve,'#NULLYIELD#') <> Yield_Data_Dt.Yield_Curve_Code)
      THEN
         INSERT INTO Yield_Curves_Price_Dates
                      (Yield_Curve_Code,
                       Delivery_Date,
                       Last_Price_Date,
                       Data_Supplier_Code,
                       Last_User,
                       Last_Stamp)
         SELECT       PD.Yield_Curve_Code,
                      Restart_Date,
                      Last_Price_Date,
                      PD.Data_Supplier_Code,
                      Constants.Get_User_Id,
                      SYSDATE
         FROM Yield_Curves_Price_Dates PD
         WHERE PD.Yield_Curve_Code = Yield_Data_Dt.Yield_Curve_Code
         AND   PD.Data_Supplier_Code = P_Data_Supplier_Code
         AND   PD.Delivery_Date      = Yield_Data_Dt.Delivery_Date
         AND   NOT EXISTS (SELECT 1
                           FROM Yield_Curves_Price_Dates PD1
                           WHERE PD1.Yield_Curve_Code = Yield_Data_Dt.Yield_Curve_Code
                           AND   PD1.Data_Supplier_Code = P_Data_Supplier_Code
                           AND   PD1.Delivery_Date      = restart_date);
      --
         IF SQL%ROWCOUNT > 0
         THEN
            Rows_Inserted     := Rows_Inserted+SQL%ROWCOUNT;
            pgmloc := 1530;
            Records_Committed :=  Records_Committed + SQL%ROWCOUNT;
            IF Records_Committed      >= P_Commit_Count
            THEN
               pgmloc := 1540;
               Pricing_Msg_Rec.Value_01 :=  Rows_Read;
               Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
               Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
               Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
               Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
            --
               Global_File_Updates.Update_Pricing_Msgs
                  (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
            --
               COMMIT;
               Records_Committed := 0;
            END IF;
         ELSE
            Rows_Rejected        := Rows_Rejected + 1;
         END IF;
     END IF;  -- YCS_FLag = Y'
     V_Prev_Curve := Yield_Data_Dt.Yield_Curve_Code;
   END LOOP;
--
-- nous mettons a jour le Last_Run_Date de supplier_running_parms
--
   UPDATE Supplier_Running_Parms SRP
      SET Last_Run_Date  = sysdate
    WHERE SRP.Program_Id = Curr_Program_Id;
--
-- Au cas ou Rows_Inserted et Rows_Updates = 0, nous devons quand meme
-- indiquer que le programme a termine
--
   pgmloc     := 1560;
   --
   Pricing_Msg_Rec.Successfull_Flag  := 'Y';
   -- Si aucune rangee a ete lu, on veut faire un raise pour avoir
   -- le message approprie
   IF rows_read = 0
   THEN
      RAISE No_Data;
   END IF;
   --
   IF (Rows_Inserted = 0) AND (Rows_Updated = 0)
   THEN
      pgmloc    := 1570;
      Pricing_Msg_Rec.Msg_Text_2 :=
           'WARNING - Carry Forward was already DONE';
   ELSE
      pgmloc    := 1580;
      Pricing_Msg_Rec.Msg_Text_2 :=
       'Carry Forward Yield Curves completed ';
      --
      IF P_Restart_Flag = 'Y'
      THEN
         Pricing_Msg_Rec.Msg_Text_2 := Pricing_Msg_Rec.Msg_Text_2 ||
            'Restarted for ' || to_char(P_Restart_Date,'DD-MON-YYYY');
      END IF;
      --
   END IF;
--
   pgmloc := 1590;
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
--
EXCEPTION
--
WHEN Cannot_Restart
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
--
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN Invalid_Price_Date
THEN
   --
   -- si la date qui a ete passee comme parametre n'est pas egale a
   -- la date last_price_date de site_controls
   --
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
   --
WHEN No_Data
THEN
   Pricing_Msg_Rec.Remarks  := 'No data to apply carry forward on';
   Pricing_Msg_Rec.Successfull_Flag := 'Y';
   --
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
-- Le dernier carry forward n'a pas fonctionne. On arrete pour permettre
-- une verification.
--
WHEN Bad_Previous_Cf
THEN
   Pricing_Msg_Rec.Successfull_Flag := 'N';
   Pricing_Msg_Rec.Msg_Text    :=
     'Previous CF '                                         ||
      to_char(Max_Delivery_Date,'ddmonyyyy')     ||
     ' didn''t work.Create a rec succ Y in '                ||
     'Prcg Msg';
   --
   Pricing_Msg_Rec.Msg_Text_2  :=
     'OR Change the Dates in Site_Controls (Take a copy of the table)';
   --
   Pricing_Msg_Rec.Err_Text    :=
     'Recompile the proc and run the Carry Fwd of '     ||
      to_char(Max_Delivery_Date,'ddmonyyyy')     ||  ' '  ||
      V_CF_Delivery_Time;
   --
   Pricing_Msg_Rec.Value_01 :=  Rows_Read;
   Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
   Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
   Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
   Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
   --
   Global_File_Updates.Update_Pricing_Msgs
     (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
   --
   COMMIT;
-- Ajout - JSL - 10 septembre 2014
   Send_Email( Pricing_Msg_Rec );
-- Fin d'ajout - JSL - 10 septembre 2014
   -- Nous voulons que la procedure plante avec un message
   -- d'erreur
   IF p_from_screen = 'N' OR p_from_screen is NULL
   THEN
      RAISE;
   END IF;
   Handled_Raise := TRUE;
--
WHEN OTHERS
THEN
   --
   -- Pout toute autre exceptions on choisit le message d'ORACLE et
   -- son Code
   --
   IF NOT(Handled_Raise)
   THEN
      Pricing_Msg_Rec.Successfull_Flag := 'N';
      Pricing_Msg_Rec.Err_Text    := SUBSTR(SQLERRM(SQLCODE),1,255);
      Pricing_Msg_Rec.Msg_Text_2  :=
         'Unhandled Exception Occurred at pgmloc: ' || TO_CHAR(pgmloc);
      --
      Pricing_Msg_Rec.Value_01 :=  Rows_Read;
      Pricing_Msg_Rec.Value_02 :=  Rows_Inserted;
      Pricing_Msg_Rec.Value_03 :=  Rows_Updated;
      Pricing_Msg_Rec.Value_04 :=  Rows_Rejected;
      Pricing_Msg_Rec.Value_06 :=  Rows_Deleted;
      --
      Global_File_Updates.Update_Pricing_Msgs
        (G_Program_Id, G_System_Date, TRUE, Pricing_Msg_Rec);
--
      COMMIT;
   END IF;
-- Ajout - JSL - 10 septembre 2014
   IF NOT(Handled_Raise)
   THEN
      -- We may be doing a rollback in send email...
      Send_Email( Pricing_Msg_Rec );
   END IF;
-- Fin d'ajout - JSL - 10 septembre 2014
--
-- pour SQL*PLUS
--
   RAISE;
--}}
END CFW_Yield_Curves;
----------------------------------------------------------------------
PROCEDURE Can_I_Delete_Chain
   (P_Fri_Ticker         IN  Securities.Fri_Ticker%TYPE,
    P_Source_Code        IN  Sources.Code%TYPE,
    P_Price_Currency     IN  Currencies.Currency%TYPE,
    P_Delivery_Date      IN  DATE,
    P_Data_Supplier_Code IN  Data_Suppliers.Code%TYPE,
    P_Delete_Chain       OUT VARCHAR2,
    P_Status             OUT NUMBER,
    P_Message            OUT VARCHAR2)
IS
--{
-- Cette procedure va determiner si un delete d'une chaine de CFW
-- est possible ou non
--
CURSOR Base_Row_Details
IS
   SELECT SP.Price_Date,
          SP.Bid_Date,
          SP.Mid_Date,
          SP.Ask_Date
     FROM Security_Prices SP
    WHERE SP.Fri_Ticker         = P_Fri_Ticker
      AND SP.Source_Code        = P_Source_Code
      AND SP.Price_Currency     = P_Price_Currency
      AND SP.Delivery_Date      = P_Delivery_Date
      AND SP.Data_Supplier_Code = P_Data_Supplier_Code;
--
Base_Row     Base_Row_Details%ROWTYPE;
--
CURSOR Get_CFW_Chain
IS
-- Modification - JSL - 19 novembre 2015
-- SELECT SP.Price_Date,
   SELECT /*+ INDEX( SP SECURITY_PRICES_X_02 ) */
          SP.Price_Date,
-- Fin de modification - JSL - 19 novembre 2015
          SP.Bid_Date,
          SP.Mid_Date,
          SP.Ask_Date
     FROM Security_Prices SP
    WHERE SP.Fri_Ticker         = P_Fri_Ticker
      AND SP.Source_Code        = P_Source_Code
      AND SP.Price_Currency     = P_Price_Currency
      AND SP.Delivery_Date      > P_Delivery_Date
      AND SP.Data_Supplier_Code = P_Data_Supplier_Code
      AND Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
                               TO_CHAR(P_Delivery_Date,'HH24MI'),
                               TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
      AND (NVL(to_char(SP.Price_Date,'dd-mon-yyyy'), CHR(1)) =
           NVL(to_char(Base_Row.Price_Date,'dd-mon-yyyy'),CHR(1))
      OR   NVL(to_char(SP.Bid_Date,'dd-mon-yyyy'), CHR(1)) =
           NVL(to_char(Base_Row.Bid_Date,'dd-mon-yyyy'),CHR(1))
      OR   NVL(to_char(SP.Ask_Date,'dd-mon-yyyy'), CHR(1)) =
           NVL(to_char(Base_Row.Ask_Date,'dd-mon-yyyy'),CHR(1))
      OR   NVL(to_char(SP.Mid_Date,'dd-mon-yyyy'), CHR(1)) =
           NVL(to_char(Base_Row.Mid_Date,'dd-mon-yyyy'),CHR(1)))
      FOR UPDATE;
--
Invalid_pricing_Row     EXCEPTION;
Pricing_Row_Not_Current EXCEPTION;
Cannot_Delete           EXCEPTION;
Current_Date            DATE;
--
BEGIN
--{
   P_Delete_Chain := 'Y';
   P_Status       := 0;
   P_Message      := NULL;
   Current_Date   := TRUNC(P_Delivery_Date);
   pgmloc         := 1600;
   --
   -- Nous voulons premierement s'assurer que la rangee de
   -- base n'est pas elle meme un cfw pour une des dates.
   --
   OPEN Base_Row_Details;
   FETCH Base_Row_Details INTO Base_Row;
   IF Base_Row_Details%NOTFOUND
   THEN
      pgmloc := 1610;
      CLOSE Base_Row_Details;
      RAISE Invalid_Pricing_Row;
   END IF;
   pgmloc := 1620;
   CLOSE Base_Row_Details;
   --
   IF NVL(Base_Row.Price_Date,Current_Date) <> Current_Date OR
      NVL(Base_Row.Ask_Date,Current_Date)   <> Current_Date OR
      NVL(Base_Row.Bid_Date,Current_Date)   <> Current_Date OR
      NVL(Base_Row.Mid_Date,Current_Date)   <> Current_Date
   THEN
      pgmloc := 1630;
      RAISE Pricing_Row_Not_Current;
   END IF;
   --
   pgmloc := 1640;
   FOR CFW_Chain IN Get_CFW_Chain
   LOOP
      IF  NVL(to_char(CFW_Chain.Price_Date,'dd-mon-yyyy'),CHR(1))  <>
          NVL(to_char(Base_Row.Price_Date ,'dd-mon-yyyy'),CHR(1))
      THEN
         RAISE Cannot_Delete;
      ELSIF  NVL(to_char(CFW_Chain.Bid_Date,'dd-mon-yyyy'),CHR(1))  <>
          NVL(to_char(Base_Row.Bid_Date ,'dd-mon-yyyy'),CHR(1))
      THEN
         RAISE Cannot_Delete;
      ELSIF  NVL(to_char(CFW_Chain.Ask_Date,'dd-mon-yyyy'),CHR(1))  <>
          NVL(to_char(Base_Row.Ask_Date ,'dd-mon-yyyy'),CHR(1))
      THEN
         RAISE Cannot_Delete;
      ELSIF  NVL(to_char(CFW_Chain.Mid_Date,'dd-mon-yyyy'),CHR(1))  <>
          NVL(to_char(Base_Row.Mid_Date ,'dd-mon-yyyy'),CHR(1))
      THEN
         RAISE Cannot_Delete;
      END IF;
   END LOOP;
   --
EXCEPTION
WHEN Invalid_Pricing_Row
THEN
   P_Delete_Chain := 'N';
   P_Status  := 2;
   P_Message := 'Pricing Row does not exist';
--
WHEN Cannot_Delete
THEN
   P_Delete_Chain := 'N';
   P_Status  := 2;
   P_Message := 'Cannot delete chain. Bid/Mid/Ask/Price date fields '||
                'must match.';
--
WHEN Pricing_Row_Not_Current
THEN
   P_Delete_Chain := 'N';
   P_Status  := 2;
   P_Message := 'Pricing Row is not current';
--
WHEN Others
THEN
   P_Delete_Chain := 'N';
   P_Status  := 2;
   P_Message := 'Processing Error at pgmloc = ' || to_char(pgmloc);
--}}
END Can_I_Delete_Chain;
--
----------------------------------------------------------------------
PROCEDURE Delete_Chain
   (P_Fri_Ticker         IN  Securities.Fri_Ticker%TYPE,
    P_Source_Code        IN  Sources.Code%TYPE,
    P_Price_Currency     IN  Currencies.Currency%TYPE,
    P_Delivery_Date      IN  DATE,
    P_Data_Supplier_Code IN  Data_Suppliers.Code%TYPE,
    P_Rows_Deleted       OUT NUMBER,
    P_Status             OUT NUMBER,
    P_Message            OUT VARCHAR2)
IS
--{
-- Cette procedure va enlever toutes les rangees qui sont CFW
-- sur la rangee de base que l'usager veut enlever
-- Cette logique ne s'applique pas au rangees Intra-Day.
--
CURSOR Base_Row_Details
IS
   SELECT SP.Price_Date,
          SP.Bid_Date,
          SP.Mid_Date,
          SP.Ask_Date
     FROM Security_Prices SP
    WHERE SP.Fri_Ticker         = P_Fri_Ticker
      AND SP.Source_Code        = P_Source_Code
      AND SP.Price_Currency     = P_Price_Currency
      AND SP.Delivery_Date      = P_Delivery_Date
      AND SP.Data_Supplier_Code = P_Data_Supplier_Code;
--
Base_Row     Base_Row_Details%ROWTYPE;
Rows_Deleted NUMBER;
--
CURSOR Get_CFW_Chain
IS
-- Modification - JSL - 19 novembre 2015
-- SELECT SP.Price_Date,
   SELECT /*+ INDEX( SP SECURITY_PRICES_X_02 ) */
          SP.Price_Date,
-- Fin de modification - JSL - 19 novembre 2015
          SP.Bid_Date,
          SP.Mid_Date,
          SP.Ask_Date
     FROM Security_Prices SP
    WHERE SP.Fri_Ticker         = P_Fri_Ticker
      AND SP.Source_Code        = P_Source_Code
      AND SP.Price_Currency     = P_Price_Currency
      AND SP.Delivery_Date      > P_Delivery_Date
      AND SP.Data_Supplier_Code = P_Data_Supplier_Code
      AND Pricing.Validate_Supplier_Chain(SP.Data_Supplier_Code,
                               TO_CHAR(P_Delivery_Date,'HH24MI'),
                               TO_CHAR(SP.Delivery_Date,'HH24MI')) = 'Y'
      AND (NVL(to_char(SP.Price_Date,'dd-mon-yyyy'), CHR(1)) =
           NVL(to_char(Base_Row.Price_Date,'dd-mon-yyyy'),CHR(1))
      OR   NVL(to_char(SP.Bid_Date,'dd-mon-yyyy'), CHR(1)) =
           NVL(to_char(Base_Row.Bid_Date,'dd-mon-yyyy'),CHR(1))
      OR   NVL(to_char(SP.Ask_Date,'dd-mon-yyyy'), CHR(1)) =
           NVL(to_char(Base_Row.Ask_Date,'dd-mon-yyyy'),CHR(1))
      OR   NVL(to_char(SP.Mid_Date,'dd-mon-yyyy'), CHR(1)) =
           NVL(to_char(Base_Row.Mid_Date,'dd-mon-yyyy'),CHR(1)))
      FOR UPDATE;
--
Invalid_pricing_Row     EXCEPTION;
Pricing_Row_Not_Current EXCEPTION;
Cannot_Delete           EXCEPTION;
Current_Date            DATE;
--
BEGIN
--{
   Rows_Deleted   := 0 ;
   P_Rows_Deleted := 0 ;
   P_Status       := NULL;
   P_Message      := NULL;
   Current_Date   := TRUNC(P_Delivery_Date);
   pgmloc         := 1650;
   --
   -- Nous voulons premierement s'assurer que la rangee de
   -- base n'est pas elle meme un cfw pour une des dates.
   --
   OPEN Base_Row_Details;
   FETCH Base_Row_Details INTO Base_Row;
   IF Base_Row_Details%NOTFOUND
   THEN
      pgmloc := 1660;
      CLOSE Base_Row_Details;
      RAISE Invalid_Pricing_Row;
   END IF;
   pgmloc := 1670;
   CLOSE Base_Row_Details;
   --
   IF NVL(Base_Row.Price_Date,Current_Date) <> Current_Date OR
      NVL(Base_Row.Ask_Date,Current_Date)   <> Current_Date OR
      NVL(Base_Row.Bid_Date,Current_Date)   <> Current_Date OR
      NVL(Base_Row.Mid_Date,Current_Date)   <> Current_Date
   THEN
      pgmloc := 1680;
      RAISE Pricing_Row_Not_Current;
   END IF;
   --
   pgmloc := 1690;
   FOR CFW_Chain IN Get_CFW_Chain
   LOOP
      IF  NVL(to_char(CFW_Chain.Price_Date,'dd-mon-yyyy'),CHR(1))  <>
          NVL(to_char(Base_Row.Price_Date ,'dd-mon-yyyy'),CHR(1))
      THEN
         RAISE Cannot_Delete;
      ELSIF  NVL(to_char(CFW_Chain.Bid_Date,'dd-mon-yyyy'),CHR(1))  <>
          NVL(to_char(Base_Row.Bid_Date ,'dd-mon-yyyy'),CHR(1))
      THEN
         RAISE Cannot_Delete;
      ELSIF  NVL(to_char(CFW_Chain.Ask_Date,'dd-mon-yyyy'),CHR(1))  <>
          NVL(to_char(Base_Row.Ask_Date ,'dd-mon-yyyy'),CHR(1))
      THEN
         RAISE Cannot_Delete;
      ELSIF  NVL(to_char(CFW_Chain.Mid_Date,'dd-mon-yyyy'),CHR(1))  <>
          NVL(to_char(Base_Row.Mid_Date ,'dd-mon-yyyy'),CHR(1))
      THEN
         RAISE Cannot_Delete;
      ELSE
         DELETE Security_Prices SP
          WHERE CURRENT OF Get_CFW_Chain;
         Rows_Deleted := Rows_Deleted + 1;
      END IF;
   END LOOP;
   P_Rows_Deleted := Rows_Deleted;
   --
EXCEPTION
WHEN Invalid_Pricing_Row
THEN
   P_Status  := 2;
   P_Message := 'Pricing Row does not exist';
--
WHEN Cannot_Delete
THEN
   P_Status  := 2;
   P_Message := 'Cannot delete chain. Bid/Mid/Ask/Price date fields '||
                'must match.';
--
WHEN Pricing_Row_Not_Current
THEN
   P_Status  := 2;
   P_Message := 'Pricing Row is not current';
--
WHEN Others
THEN
   P_Status  := 2;
   P_Message := 'Processing Error at pgmloc = ' || to_char(pgmloc);
--   RAISE;
--}}
END Delete_Chain;
--
-----------------------------------------------------------------------
FUNCTION Get_Base_CFW_Chain
   (P_Data_Supplier_Code IN Data_Suppliers.Code%TYPE,
    P_Delivery_Time      IN Data_Supply_Schedule.Delivery_Time%TYPE)
RETURN VARCHAR2
IS
--{
-- Cette Fonction le delivery time qui constitut la base d'une chaine
-- de CFW
--
CURSOR Validate_Parameters
IS
   SELECT 'X'
     FROM Data_Supply_Schedule DSS
    WHERE DSS.Data_Supplier_Code = P_Data_Supplier_Code
      AND DSS.Delivery_Time      = P_Delivery_Time;
--
CURSOR Get_CF_Del_Time (
   P_Delivery_Time Data_Supply_Schedule.Delivery_Time%TYPE)
IS
   SELECT CF_Delivery_Time
     FROM Data_Supply_Schedule DSS
    WHERE DSS.Data_Supplier_Code = P_Data_Supplier_Code
      AND DSS.Delivery_Time      = P_Delivery_Time;
--
CF_Delivery_Time      Data_Supply_Schedule.Delivery_Time%TYPE;
CF_Base_Del_Time      Data_Supply_Schedule.Delivery_Time%TYPE;
W_Del_Time            Data_Supply_Schedule.Delivery_Time%TYPE;
Temp_Buffer           CHAR(255);
--
BEGIN
   --{
   OPEN  Validate_Parameters;
   FETCH Validate_Parameters INTO Temp_Buffer;
   IF Validate_Parameters%NOTFOUND
   THEN
      CLOSE Validate_Parameters;
      RAISE No_Data_Found;
   END IF;
   CLOSE Validate_Parameters;
   --
   OPEN  Get_CF_Del_Time(P_Delivery_Time);
   FETCH Get_CF_Del_Time INTO CF_Delivery_Time;
   CLOSE Get_CF_Del_Time;
   --
   IF CF_Delivery_Time >= P_Delivery_Time
   THEN
      CF_Base_Del_Time := P_Delivery_Time;
   ELSE
      LOOP
         W_Del_Time := CF_Delivery_Time;
         OPEN Get_CF_Del_Time(W_Del_Time);
         FETCH Get_CF_Del_Time INTO CF_Delivery_Time;
         CLOSE Get_CF_Del_Time;
      EXIT WHEN CF_Delivery_Time >= W_Del_Time;
      END LOOP;
      CF_Base_Del_Time := W_Del_Time;
   END IF;
   RETURN CF_Base_Del_Time;
--
EXCEPTION
WHEN No_Data_Found
THEN
   CF_Base_Del_Time  := NULL;
   RETURN CF_Base_Del_Time;
--}}
END Get_Base_CFW_Chain;
-----------------------------------------------------------------------
--
FUNCTION IS_IT_FIRST_CFW
   (P_Data_Supplier_Code  IN  Data_Suppliers.Code%TYPE,
    P_Delivery_Time       IN  Data_Supply_Schedule.Delivery_Time%TYPE)
--
RETURN VARCHAR2
IS
--{
   -- Cette Fonction determine si un cfw pour un fournisseur / del time
   -- est le premier de la journee (retourne 'Y' ou 'N')
   --
   CURSOR Later_CFW
   IS
      SELECT 'X'
        FROM Supplier_Running_Parms SRP,
             Data_Supply_Schedule   DS
       WHERE SRP.Data_Supplier_Code = P_Data_Supplier_Code
         AND SRP.Delivery_Time      = P_Delivery_Time
         AND SRP.Product_Code       = 'CFW'
         AND DS.Data_Supplier_Code  = P_Data_Supplier_Code
         AND DS.Delivery_Time       = P_Delivery_Time
         AND DS.Delivery_Time      <= DS.CF_Delivery_Time ;
   --
   Temp_Buffer  CHAR(255);
   First_CFW    VARCHAR2(1) := 'N';
   --
BEGIN
   --{
   OPEN  Later_CFW;
   FETCH Later_CFW INTO Temp_Buffer;
   IF Later_CFW%FOUND
   THEN
      First_CFW := 'Y';
   END IF;
   CLOSE Later_CFW;
   RETURN First_CFW;
--}}
END Is_It_First_CFW;
END carry_fwd;
/
show error
